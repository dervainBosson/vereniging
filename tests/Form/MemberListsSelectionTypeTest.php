<?php

/*
 * Copyright (C) 2022 by Luc Hamers <vereniging@hamers.de>
 *
 * This file is part of Vereniging.
 *
 * Vereniging is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Vereniging is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Vereniging.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Tests\Form\MemberListsSelectionTypeTest;

use App\Form\MemberListsSelectionType;
use App\Repository\CommitteeRepository;
use App\Repository\MembershipTypeRepository;
use App\Repository\StatusRepository;
use App\Repository\UserRoleRepository;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * Class ValueFormTypeTest
 */
class MemberListsSelectionTypeTest extends WebTestCase
{
    /**
     * Test call to buildForm method. For this test, the FormBuildInterface is mocked.
     */
    public function testBuildForm(): void
    {
        // Mock the FormBuilder
        $builder = $this->getMockBuilder('\Symfony\Component\Form\FormBuilderInterface')
                        ->disableOriginalConstructor()
                        ->getMock();

        // Expect builder to be called for all data fields except the id.
        $builder->expects($this->exactly(7))
                ->method('add')
                ->withConsecutive(
                    [$this->equalTo('status'), 'Symfony\Bridge\Doctrine\Form\Type\EntityType', [
                        'class' => 'App\Entity\Status',
                        'expanded' => true,
                        'multiple' => true,
                        'data' => [null],
                        'query_builder' => function (StatusRepository $repository) {
                            return ($repository->createFindAllWithMembershipNumberQueryBuilder('en'));
                        },
                        ],
                    ],
                    [$this->equalTo('membershipType'), 'Symfony\Bridge\Doctrine\Form\Type\EntityType', [
                        'class' => 'App\Entity\MembershipType',
                        'expanded' => true,
                        'multiple' => true,
                        'query_builder' => function (MembershipTypeRepository $repository) {
                            return $repository->createFindByGroupMembershipQueryBuilder('en', false);
                        },
                        ],
                    ],
                    [$this->equalTo('committee'), 'Symfony\Bridge\Doctrine\Form\Type\EntityType', [
                        'class' => 'App\Entity\Committee',
                        'expanded' => true,
                        'multiple' => true,
                        'query_builder' => function (CommitteeRepository $repository) {
                            return $repository->createFindAllOrderedQueryBuilder('en');
                        },
                        ],
                    ],
                    [$this->equalTo('userRole'), 'Symfony\Bridge\Doctrine\Form\Type\EntityType', [
                        'class' => 'App\Entity\UserRole',
                        'expanded' => true,
                        'multiple' => true,
                        'query_builder' => function (UserRoleRepository $repository) {
                            return $repository->createFindAllOrderedQueryBuilder('en');
                        },
                        ],
                    ],
                    [$this->equalTo('email'), 'Symfony\Component\Form\Extension\Core\Type\ChoiceType', [
                        'choices' => [
                            'All' => 'all',
                            'Only with email' => 'with',
                            'Only without email' => 'without',
                            'Only without email in group' => 'without group',
                            ],
                        'empty_data'  => null,
                        'expanded' => true,
                        'multiple' => false,
                        'data' => 'all',
                        ],
                    ],
                    [$this->equalTo('debt'), 'Symfony\Component\Form\Extension\Core\Type\ChoiceType', [
                        'choices' => [
                            'All' => 'all',
                            'Only with outstanding debt' => 'with_debt',
                            'Only without outstanding debt' => 'without_dept',
                        ],
                        'empty_data'  => null,
                        'expanded' => true,
                        'multiple' => false,
                        'data' => 'all',
                        ],
                    ],
                    [$this->equalTo('others'), 'App\Form\MemberListsSelectionOthersType'],
                )
        ->willReturn($builder);

        $selectionForm = new MemberListsSelectionType();
        $selectionForm->buildForm($builder, ['selectedStatus' => null, 'currentLocale' => 'en', 'statusWithMembershipNumberOnly' => false]);
    }


    /**
     * Test call to the method configureOptions, where the data class is set.
     */
    public function testConfigureOptions(): void
    {
        // Mock the OptionsResolver
        $resolver = $this->getMockBuilder('\Symfony\Component\OptionsResolver\OptionsResolver')
                         ->disableOriginalConstructor()
                         ->getMock();

        // Expect builder to be called for all data fields except the id.
        $resolver->expects($this->exactly(1))
                 ->method('setDefaults')
            ->withConsecutive([[
                     'csrf_protection' => false,
                     'selectedStatus' => null,
                     'currentLocale' => null,
                     'statusWithMembershipNumberOnly' => null,
            ], ]);

        $selectionForm = new MemberListsSelectionType();
        $selectionForm->configureOptions($resolver);
    }
}
