<?php

/*
 * Copyright (C) 2022 by Luc Hamers <vereniging@hamers.de>
 *
 * This file is part of Vereniging.
 *
 * Vereniging is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Vereniging is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Vereniging.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Tests\Form;

use App\Entity\ChangeType;
use App\Entity\MemberEntry;
use App\Form\EmailNotificationsCollection;
use App\Tests\TestCase;

/**
 * Class EmailNotificationCollectionTest
 */
class EmailNotificationCollectionTest extends TestCase
{
    /**
     * @inheritDoc
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->loadAllFixtures();
    }


    /**
     * Test if the member selection by status and role form can be submitted.
     */
    public function testConstruct(): void
    {
        /** @var MemberEntry[] $memberEntries */
        $memberEntries = $this->getEntityManager()->getRepository(MemberEntry::class)
                                                  ->findBy(['firstName' => ['Firstname1', 'Firstname2']]);

        $emailNotificationCollection = new EmailNotificationsCollection($memberEntries);
        $this->assertEquals(2, $emailNotificationCollection->getMemberEntries()->count());
    }


    /**
     * Test the addList and removeList methods
     */
    public function testAddRemove(): void
    {
        /** @var MemberEntry $memberEntry */
        $memberEntry = $this->getEntityManager()->getRepository(MemberEntry::class)
                                                           ->findOneBy(['firstName' => 'Firstname1']);

        // Add an email notifications since these are not defined in the fixtures.
        /** @var ChangeType $changeType */
        $changeType = $this->getEntityManager()->getRepository(ChangeType::class)->findOneBy(['changeType' => 'error']);
        $memberEntry->addEmailNotification($changeType);
        $this->getEntityManager()->flush();

        $emailNotificationCollection = new EmailNotificationsCollection([]);
        $this->assertEquals(0, $emailNotificationCollection->getMemberEntries()->count());

        // Check if the element is added to the internal list
        $emailNotificationCollection->addMemberEntry($memberEntry);
        $this->assertEquals(1, $emailNotificationCollection->getMemberEntries()->count());

        // Check that the element cannot be added twice
        $emailNotificationCollection->addMemberEntry($memberEntry);
        $this->assertEquals(1, $emailNotificationCollection->getMemberEntries()->count());

        // Check if the element is removed from the internal list
        $emailNotificationCollection->removeMemberEntry($memberEntry);
        $this->assertEquals(0, $emailNotificationCollection->getMemberEntries()->count());

        // Check that the element cannot be removed twice
        $emailNotificationCollection->removeMemberEntry($memberEntry);
        $this->assertEquals(0, $emailNotificationCollection->getMemberEntries()->count());

        // Check that a member with an empty email notification list name is not added
        $memberEntry->removeEmailNotification($changeType);
        $this->getEntityManager()->flush();

        $emailNotificationCollection->addMemberEntry($memberEntry);
        $this->assertEquals(0, $emailNotificationCollection->getMemberEntries()->count());
    }
}
