<?php

/*
 * Copyright (C) 2022 by Luc Hamers <vereniging@hamers.de>
 *
 * This file is part of Vereniging.
 *
 * Vereniging is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Vereniging is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Vereniging.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Tests;

use Liip\FunctionalTestBundle\Test\WebTestCase as LiipWebTestCase;
use Throwable;

/**
 * This extends the Liip WebTestCase with some functions needed by all functional tests
 */
class TestCase extends LiipWebTestCase
{
    use TestCaseTrait;


    /**
     * @inheritDoc
     */
    public function setUp(): void
    {
        self::bootKernel();
        $this->setupMutual();
        parent::setUp();
    }


    /**
     * @inheritDoc
     */
    public function tearDown():void
    {
        $this->tearDownMutual();
        parent::tearDown();
    }


    /**
     * @inheritDoc
     *
     * @param Throwable $t
     *
     * @throws Throwable
     */
    protected function onNotSuccessfulTest(Throwable $t): void
    {
        $this->writeErrorFile($t->getMessage(), $t->getTraceAsString());

        parent::onNotSuccessfulTest($t);
    }


    /**
     * @param string $errorMessage
     * @param string $trace
     */
    protected function writeErrorFile(string $errorMessage, string $trace): void
    {
        $testClass = get_class($this);
        $testName = $this->getName();

        // Generate a file name containing the test file name and the test name, e.g.
        // App_Tests_Controller_MyControllerTest___testDefault.html
        $fileName = str_replace('\\', '_', "$testClass"."___$testName.html");
        $content = "Error message: $errorMessage\nFailing test: $testClass::$testName\nStacktrace:\n$trace";
        file_put_contents(self::$logDir."//$fileName", $content);
    }


    /**
     * Check if the passed class is registered correctly at the service container.
     *
     * @param string $class
     */
    protected function checkContainerRegistration(string $class): void
    {
        $service = $this->getContainer()->get($class);

        $this->assertNotNull($service);
        $this->assertInstanceOf($class, $service);
    }
}
