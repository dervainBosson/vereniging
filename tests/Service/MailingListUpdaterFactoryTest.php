<?php

/*
 * Copyright (C) 2022 by Luc Hamers <vereniging@hamers.de>
 *
 * This file is part of Vereniging.
 *
 * Vereniging is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Vereniging is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Vereniging.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Tests\Service;

use App\Service\MailingListUpdateInterface;
use App\Service\MailingListUpdaterFactory;
use App\Service\MailManUpdater;
use App\Service\MajorDomoUpdater;
use App\Tests\TestCase;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Component\Mailer\MailerInterface;

/**
 * Class MailingListUpdaterFactoryTest
 */
class MailingListUpdaterFactoryTest extends TestCase
{
    /**
     * Test if the factory correctly returns a mail man updater
     *
     * @throws Exception
     */
    public function testMailMan(): void
    {
        $mailerMock = $this->createMock(MailerInterface::class);
        $managerMock = $this->createMock(EntityManagerInterface::class);

        $mailManUpdater = MailingListUpdaterFactory::createMailingListUpdater($managerMock, $mailerMock, '', 'MailMan');
        $this->assertInstanceOf(MailManUpdater::class, $mailManUpdater);
    }


    /**
     * Test if the factory correctly returns a majordomo updater
     *
     * @throws Exception
     */
    public function testMajorDomo(): void
    {
        $mailerMock = $this->createMock(MailerInterface::class);
        $managerMock = $this->createMock(EntityManagerInterface::class);

        $majorDomoUpdater = MailingListUpdaterFactory::createMailingListUpdater($managerMock, $mailerMock, '', 'MajorDomo');
        $this->assertInstanceOf(MajorDomoUpdater::class, $majorDomoUpdater);
    }


    /**
     * Test if the correct exception is thrown when an invalid type is passed
     *
     * @throws Exception
     */
    public function testTypeNotFoundException(): void
    {
        $mailerMock = $this->createMock(MailerInterface::class);
        $managerMock = $this->createMock(EntityManagerInterface::class);

        $this->expectException(Exception::class);
        $this->expectExceptionMessage('Type "Bla" not supported');

        MailingListUpdaterFactory::createMailingListUpdater($managerMock, $mailerMock, '', 'Bla');
    }


    /**
     * Test if the service is registered correctly in the service container
     */
    public function testServiceRegistration(): void
    {
        $this->checkContainerRegistration(MailingListUpdateInterface::class);
    }
}
