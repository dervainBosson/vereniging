<?php

/*
 * Copyright (C) 2022 by Luc Hamers <vereniging@hamers.de>
 *
 * This file is part of Vereniging.
 *
 * Vereniging is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Vereniging is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Vereniging.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Tests\Service;

use App\Service\AddRemoveFormGenerator;
use App\Tests\TestCase;
use Symfony\Component\Form\FormFactory;

/**
 * Class AddRemoveFormGeneratorTest
 */
class AddRemoveFormGeneratorTest extends TestCase
{
    private FormFactory $formBuilderMock;


    /**
     * @inheritDoc
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->loadAllFixtures();
        // Create a stub for the SomeClass class.
        $this->formBuilderMock = $this->getMockBuilder('Symfony\Component\Form\FormFactory')
                                      ->disableOriginalConstructor()
                                      ->getMock();
        $this->formBuilderMock->expects($this->any())
                               ->method('create')
                               ->willReturn(new FormMock());
    }


    /**
     * Test creating the status form.
     */
    public function testCreateStatusForm(): void
    {
        $generator = new AddRemoveFormGenerator($this->formBuilderMock, $this->getEntityManager(), 'en');
        $widget = $generator->CreateShowAddDeleteWidget("Status", "Status caption", null);
        $this->checkDefaultSettings($widget, 'testCreateStatusFormAfterSubmitWrongFormType');
    }


    /**
     * Test create the country form.
     */
    public function testCreateCountryForm(): void
    {
        $generator = new AddRemoveFormGenerator($this->formBuilderMock, $this->getEntityManager(), 'en');
        $widget = $generator->CreateShowAddDeleteWidget("Country", "Country caption", null);
        $this->assertArrayHasKey('articles', $widget);
        $this->assertArrayHasKey('form', $widget);
        $this->assertArrayHasKey('caption', $widget);
        $this->assertArrayHasKey('object_type', $widget);

        $this->assertEquals($widget['form'], 'Test');
        $this->assertEquals($widget['caption'], 'Country caption');
        $this->assertEquals($widget['object_type'], 'Country');
        $this->assertCount(2, $widget['articles']);
        $this->assertEquals($widget['articles'][0]->getCountry(), 'Germany');
        $this->assertEquals($widget['articles'][1]->getCountry(), 'Netherlands');
    }



    /**
     * Test creating the status form after the form has been submitted.
     */
    public function testCreateStatusFormAfterSubmit(): void
    {
        $generator = new AddRemoveFormGenerator($this->formBuilderMock, $this->getEntityManager(), 'en');
        $form = array();
        $form['type'] = 'Status';
        $form['form'] = new FormMock('my_form');
        $widget = $generator->CreateShowAddDeleteWidget("Status", "Status caption", $form);
        $this->assertArrayHasKey('articles', $widget);
        $this->assertArrayHasKey('form', $widget);
        $this->assertArrayHasKey('caption', $widget);
        $this->assertArrayHasKey('object_type', $widget);

        // On submit, the passed form has to be added to the widget
        $this->assertEquals('my_form', $widget['form']);
        $this->assertEquals('Status caption', $widget['caption']);
        $this->assertEquals('Status', $widget['object_type']);
        $this->assertCount(3, $widget['articles']);
        $this->assertEquals('Former member', $widget['articles'][0]->getStatus());
        $this->assertEquals('Future member', $widget['articles'][1]->getStatus());
        $this->assertEquals('Member', $widget['articles'][2]->getStatus());
    }



    /**
     * Test what happens when a wrong form type is submitted.
     */
    public function testCreateStatusFormAfterSubmitWrongFormType(): void
    {
        $generator = new AddRemoveFormGenerator($this->formBuilderMock, $this->getEntityManager(), 'en');
        $form = array();
        $form['type'] = 'not status';
        $form['form'] = new FormMock('my_form');
        $widget = $generator->CreateShowAddDeleteWidget("Status", "Status caption", $form);
        $this->checkDefaultSettings($widget, 'testCreateStatusFormAfterSubmitWrongFormType');
    }


    /**
     * @param *      $widget
     * @param string $testName
     */
    private function checkDefaultSettings($widget, string $testName): void
    {
        $this->assertArrayHasKey('articles', $widget, $testName);
        $this->assertArrayHasKey('form', $widget, $testName);
        $this->assertArrayHasKey('caption', $widget, $testName);
        $this->assertArrayHasKey('object_type', $widget, $testName);

        $this->assertEquals('Test', $widget['form'], $testName);
        $this->assertEquals('Status caption', $widget['caption'], $testName);
        $this->assertEquals('Status', $widget['object_type'], $testName);
        $this->assertCount(3, $widget['articles'], $testName);
        $this->assertEquals('Former member', $widget['articles'][0]->getStatus(), $testName);
        $this->assertEquals('Future member', $widget['articles'][1]->getStatus(), $testName);
        $this->assertEquals('Member', $widget['articles'][2]->getStatus(), $testName);
    }
}
