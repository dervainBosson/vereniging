<?php

/*
 * Copyright (C) 2022 by Luc Hamers <vereniging@hamers.de>
 *
 * This file is part of Vereniging.
 *
 * Vereniging is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Vereniging is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Vereniging.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Tests\Service;

use App\Service\Html2Latex;
use App\Tests\TestCase;

/**
 * Test the conversion from html to latex.
 *
 * Class Html2LatexTest
 */
class Html2LatexTest extends TestCase
{
    private Html2Latex $converter;


    /**
     * Construct the html to latex converter, which is the object under test.
     */
    public function setUp(): void
    {
        $this->converter = new Html2Latex();
    }


    /**
     * Test converting a table to latex.
     */
    public function testTablesBasic(): void
    {
        $htmlTables  = "<table><tbody><tr><td>Cell one</td><td>Cell two</td></tr><tr><td>Cell three</td><td>Cell four</td></tr></tbody></table>";
        $htmlTables .= "\nbla bla\n<table><tbody><tr><td>Cell five</td><td>Cell six</td></tr><tr><td>Cell seven</td><td>Cell eight</td></tr></tbody></table>";
        $expected  = '\begin{longtable}{l l }'."\n";
        $expected .= 'Cell one & Cell two\\\\ '."\n";
        $expected .= 'Cell three & Cell four\\\\ '."\n";
        $expected .= '\end{longtable}'."\n\n\n";
        $expected .= 'bla bla'."\n";
        $expected .= '\begin{longtable}{l l }'."\n";
        $expected .= 'Cell five & Cell six\\\\ '."\n";
        $expected .= 'Cell seven & Cell eight\\\\ '."\n";
        $expected .= '\end{longtable}'."\n\n";
        $this->assertEquals($expected, $this->converter->convert($htmlTables));
    }


    /**
     * Test converting tables with alignment and width information into latex.
     */
    public function testTablesWithFormatting()
    {
        $htmlTables  = "<table border=\"0\">\n";
        $htmlTables .= "<tr>\n<th>Cell one</th>\n<td style=\"text-align: left;\">Cell two\n</td>\n<td style=\"width: 15cm;\">Cell three</td>\n</tr>\n";
        $htmlTables .= '</table>';
        $htmlTables .= '<table border="1">';
        $htmlTables .= '<tr><td style="text-align: center;">Cell four</td><td style="text-align: right;">Cell five</td></tr>';
        $htmlTables .= '</table>';
        $htmlTables .= '<table style="width: 15cm; height: 18px;">';
        $htmlTables .= '<tr><td style="width: 15cm; text-align: center;">Cell six</td><td style="width: 15cm; height: 18px;">Cell seven</td></tr>';
        $htmlTables .= '</table>';
        $htmlTables .= '<table style="width: 15cm; height: 18px;">';
        $htmlTables .= '<tr><td style="text-align: center;">Cell eight</td><td style="text-align: right;">Cell nine</td></tr>';
        $htmlTables .= '</table>';
        $expected  = '\begin{longtable}{l l p{15cm} }'."\n";
        $expected .= 'Cell one & Cell two'."\n".' & Cell three\\\\ '."\n";
        $expected .= '\end{longtable}'."\n\n";
        $expected .= '\begin{longtable}{c r }'."\n";
        $expected .= 'Cell four & Cell five\\\\ '."\n";
        $expected .= '\end{longtable}'."\n\n";
        $expected .= '\begin{longtable}{p{15cm} p{15cm} }'."\n";
        $expected .= 'Cell six & Cell seven\\\\ '."\n";
        $expected .= '\end{longtable}'."\n\n";
        $expected .= '\begin{longtable}{c r }'."\n";
        $expected .= 'Cell eight & Cell nine\\\\ '."\n";
        $expected .= '\end{longtable}'."\n\n";

        $this->assertEquals($expected, $this->converter->convert($htmlTables));
    }


    /**
     * Check if converting URLs is done correctly
     */
    public function testReplaceUrl(): void
    {
        $result = "test1 \href{http://www.heise.de}{heise.de} test2";

        // Simple test without title
        $this->assertEquals($result, $this->converter->convert("test1 <a href=\"http://www.heise.de\">heise.de</a> test2"));

        // Add title -> result must be the same
        $this->assertEquals(
            $result,
            $this->converter->convert("test1 <a title=\"My title\" href=\"http://www.heise.de\">heise.de</a> test2")
        );

        // Add target -> result must be the same
        $this->assertEquals(
            $result,
            $this->converter->convert("test1 <a href=\"http://www.heise.de\" target=\"_blank\">heise.de</a> test2")
        );

        // Test if https is used correctly
        $result = "test1 \href{https://www.heise.de}{heise.de} test2";
        $this->assertEquals($result, $this->converter->convert("test1 <a href=\"https://www.heise.de\">heise.de</a> test2"));

        // URL without http -> result must be the same
        $this->assertEquals($result, $this->converter->convert("test1 <a href=\"www.heise.de\">heise.de</a> test2"));
        $this->assertEquals(
            $result,
            $this->converter->convert("test1 <a title=\"My title\" href=\"www.heise.de\">heise.de</a> test2")
        );
        $this->assertEquals(
            $result,
            $this->converter->convert("test1 <a title=\"My title\" href=\"www.heise.de\" target=\"_blank\">heise.de</a> test2")
        );

        // Test if mailto is used correctly
        $result = "test1 \href{mailto:test@test.com}{test@test.com} test2";
        $this->assertEquals($result, $this->converter->convert("test1 <a href=\"mailto:test@test.com\">test@test.com</a> test2"));
    }


    /**
     * Test if double line breaks are removed correctly.
     */
    public function testRemoveDoubleNewLines(): void
    {
        // Remove two newlines with only spaces between them
        $this->assertEquals("test \\newline  test test", $this->converter->convert("<p>test <br /><br />test test"));
        // \newline is not allowed at the end of a line
        $this->assertEquals("test test", $this->converter->convert("<p>test test<br />"));
        // Check if this also works with a non breaking space
        $this->assertEquals(
            "test \\newline  test test",
            $this->converter->convert("<p>test <br />&nbsp;<br /><br />test test")
        );
        // \newline is not allowed at the start of a line
        $this->assertEquals(" test test", $this->converter->convert("<p><br />test test"));
        $this->assertEquals("test\n\n  test test\n\n", $this->converter->convert("<p>test</p><p><br />test test</p>"));
    }


    /**
     * Test all replacements when using pictures.
     */
    public function testReplacePicture(): void
    {
        // Width 20% -> set size to 0.2 * textwidth
        $expected = "\includegraphics[width=0.2\\textwidth]{exports/test.jpg}";
        $this->assertEquals($expected, $this->converter->convert("<img src=\"exports/test.jpg\" alt=\"\" width=\"20%\" />"));

        $expected = "\includegraphics[width=1\\textwidth]{exports/test.jpg}";
        // Picture without size -> set size to textwidth
        $this->assertEquals($expected, $this->converter->convert("<img src=\"exports/test.jpg\" />"));
        // Picture with alt attribute, which is to be ignored
        $this->assertEquals($expected, $this->converter->convert("<img src=\"exports/test.jpg\" alt=\"\" />"));
        // Width in pixels, not % -> set size to textwidth
        $this->assertEquals($expected, $this->converter->convert("<img src=\"exports/test.jpg\" alt=\"\" width=\"200\" />"));
        // Width 120% -> set size to textwidth
        $this->assertEquals($expected, $this->converter->convert("<img src=\"exports/test.jpg\" alt=\"\" width=\"120%\" />"));
        // Width -20% -> set size to textwidth
        $this->assertEquals($expected, $this->converter->convert("<img src=\"exports/test.jpg\" alt=\"\" width=\"-20%\" />"));

        // Center picture
        $this->assertEquals(
            "\begin{center}$expected".'\end{center}',
            $this->converter->convert("<img style=\"display: block; margin-left: auto; margin-right: auto;\" src=\"exports/test.jpg\"/>")
        );

        // Picture as float left (text is wrapped around the image)
        $this->assertEquals(
            '\parpic[l]{'.$expected.'}',
            $this->converter->convert("<img style=\"float: left;\" src=\"exports/test.jpg\"/>")
        );

        // Picture as float right (text is wrapped around the image)
        $this->assertEquals(
            '\parpic[r]{'.$expected.'}',
            $this->converter->convert("<img style=\"float: right;\" src=\"exports/test.jpg\"/>")
        );
    }


    /**
     * When there are quotes within a text block, then these have to be replaced by latex style quotes.
     */
    public function testReplaceQuotes(): void
    {
        // Quotes have to be replaced within a paragraph
        $this->assertEquals("test1 „test2 test3“ test4\n\n", $this->converter->convert('<p>test1 "test2 test3" test4</p>'));
        // Quotes have to be replaced within a list
        $this->assertEquals(
            "\item test1 „test2 test3“ test4",
            $this->converter->convert('<li>test1 "test2 test3" test4</li>')
        );
        // Quotes have to be replaced within a table (since this is no complete table, no other replacements are made)
        $this->assertEquals(
            "<td>test1 „test2 test3“ test4</td>",
            $this->converter->convert('<td>test1 "test2 test3" test4</td>')
        );

        // Now check if quotes at other locations are left unchanged
        $this->assertEquals(
            '<some_tag style="some_style">test</some_tag>',
            $this->converter->convert('<some_tag style="some_style">test</some_tag>')
        );
    }


    /**
     * Test replacements of the alignments (center, left, right)
     */
    public function testReplaceAlignments(): void
    {
        // Replace center style
        $this->assertEquals(
            '\begin{center}Test1 test2\end{center}',
            $this->converter->convert('<p style="text-align: center;">Test1 test2</p>')
        );
        // Replace left style
        $this->assertEquals(
            '\begin{flushleft}Test1 test2\end{flushleft}',
            $this->converter->convert('<p style="text-align: left;">Test1 test2</p>')
        );
        // Replace right style
        $this->assertEquals(
            '\begin{flushright}Test1 test2\end{flushright}',
            $this->converter->convert('<p style="text-align: right;">Test1 test2</p>')
        );

        // When another style is found, then it is removed, i.e. ignored.
        $this->assertEquals("Test1 test2\n\n", $this->converter->convert('<p style="some_style;">Test1 test2</p>'));
    }


    /**
     * Test replacement of the euro symbol, including the replacement of the spaces by a small non-breaking space
     */
    public function testEuroSymbolReplacement(): void
    {
        // Number is in front of the euro symbol
        $this->assertEquals('test1 10\,\euro{} test2', $this->converter->convert('test1 10 &euro; test2'));
        // Same as before, but now with several spaces
        $this->assertEquals('test1 10\,\euro{} test2', $this->converter->convert('test1 10      &euro; test2'));
        // Number is behind the euro symbol
        $this->assertEquals('test \euro{}\,10 test', $this->converter->convert('test &euro; 10 test'));
        // Same as before, but now with several spaces
        $this->assertEquals('test \euro{}\,10 test', $this->converter->convert('test &euro;      10 test'));
    }


    /**
     * Test replacements of formatting tags in the text
     */
    public function testFormattingReplacements(): void
    {
        // Bold text
        $this->assertEquals(
            'test1 \textbf{test2 test3} test4',
            $this->converter->convert('test1 <strong>test2 test3</strong> test4')
        );
        // Italic text
        $this->assertEquals(
            'test1 \textit{test2 test3} test4',
            $this->converter->convert('test1 <em>test2 test3</em> test4')
        );
        // Subscript text
        $this->assertEquals('test1 $_{test2 test3}$ test4', $this->converter->convert('test1 <sub>test2 test3</sub> test4'));
        // Superscript text
        $this->assertEquals('test1 $^{test2 test3}$ test4', $this->converter->convert('test1 <sup>test2 test3</sup> test4'));
    }


    /**
     * Test replacements of the numbered and unnumbered lists
     */
    public function testLists(): void
    {
        // Numbered list without line feeds
        $this->assertEquals(
            '\begin{enumerate}[noitemsep]\item test1\item test2\end{enumerate}',
            $this->converter->convert('<ol><li>test1</li><li>test2</li></ol>')
        );
        // Numbered list with linefeeds
        $this->assertEquals(
            "\\begin{enumerate}[noitemsep]\n\\item test1\n\\item test2\n\\end{enumerate}\n",
            $this->converter->convert("<ol>\n<li>test1</li>\n<li>test2</li>\n</ol>\n")
        );
        // Unnumbered list without linefeeds
        $this->assertEquals(
            '\begin{itemize}[noitemsep]\item test1\item test2\end{itemize}',
            $this->converter->convert('<ul><li>test1</li><li>test2</li></ul>')
        );
        // Unnumbered list with linefeeds
        $this->assertEquals(
            "\\begin{itemize}[noitemsep]\n\\item test1\n\\item test2\n\\end{itemize}\n",
            $this->converter->convert("<ul>\n<li>test1</li>\n<li>test2</li>\n</ul>\n")
        );

        // Unnumbered list with numbered sub list
        $this->assertEquals(
            '\begin{itemize}[noitemsep]\item test1\item \begin{enumerate}[noitemsep]\item test2_1\item test2_2\end{enumerate}\end{itemize}',
            $this->converter->convert("<ul><li>test1</li><li><ol><li>test2_1</li><li>test2_2</li></ol></li></ul>")
        );

        // Numbered list with unnumbered sub list
        $this->assertEquals(
            '\begin{enumerate}[noitemsep]\item test1\item \begin{itemize}[noitemsep]\item test2_1\item test2_2\end{itemize}\end{enumerate}',
            $this->converter->convert("<ol><li>test1</li><li><ul><li>test2_1</li><li>test2_2</li></ul></li></ol>")
        );
    }


    /**
     * Test the replacement of spaces, paragraphs, line breaks and page breaks
     */
    public function testSpacesAndBreaks(): void
    {
        // Paragraphs
        $this->assertEquals(
            "First paragraph\n\nSecond paragraph\n\n",
            $this->converter->convert('<p>First paragraph</p><p>Second paragraph</p>')
        );
        // Breaks
        $this->assertEquals(
            "First paragraph\\newline \nSecond paragraph",
            $this->converter->convert("First paragraph<br />\nSecond paragraph")
        );
        $this->assertEquals(
            "First paragraph\\newline Second paragraph",
            $this->converter->convert("First paragraph<br />Second paragraph")
        );
        // Page break
        $this->assertEquals(
            "First page\\newpage Second page",
            $this->converter->convert("First page<!--newpage-->Second page")
        );
        // Non breaking space
        $this->assertEquals("Test1~test2", $this->converter->convert("Test1&nbsp;test2"));
    }


    /**
     * Test conversion of special characters
     */
    public function testSpecialCharacters(): void
    {
        // Html entities
        $this->assertEquals(
            'öäüÖÄÜß"\&<>'."'",
            $this->converter->convert('&ouml;&auml;&uuml;&Ouml;&Auml;&Uuml;&szlig;&quot;&amp;&lt;&gt;&acute;')
        );
        // Special characters
        $this->assertEquals('$^{\circ}$ $^2$', $this->converter->convert('&deg; ²'));
        $this->assertEquals('\euro{}', $this->converter->convert('&euro;'));
        $this->assertEquals('\_', $this->converter->convert('&lowbar;'));
    }



    /**
     * Test if the service is registered correctly in the service container
     */
    public function testServiceRegistration(): void
    {
        $this->checkContainerRegistration(Html2Latex::class);
    }
}
