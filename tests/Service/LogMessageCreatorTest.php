<?php

/*
 * Copyright (C) 2022 by Luc Hamers <vereniging@hamers.de>
 *
 * This file is part of Vereniging.
 *
 * Vereniging is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Vereniging is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Vereniging.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Tests\Service;

use App\Entity\ComplexLogEntryDoubleDataField;
use App\Entity\MemberEntry;
use App\Service\EmailNotificationSender;
use App\Service\LogMessageCreator;
use App\Tests\TestCase;
use Exception;
use Symfony\Component\Security\Core\Security;

/**
 * Class LogMessageCreatorTest
 */
class LogMessageCreatorTest extends TestCase
{
    /**
     * @inheritDoc
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->loadAllFixtures();
    }


    /**
     * Test if the exceptions for a missing logged-in user is thrown correctly
     */
    public function testCreateLogEntryUserNotSetException(): void
    {
        $notificationMock = $this->createMock(EmailNotificationSender::class);
        $securityMock = $this->createMock(Security::class);
        $securityMock->method('getUser')->willReturn(null);
        $logCreator = new LogMessageCreator($this->getEntityManager(), $notificationMock, $securityMock);

        try {
            $logCreator->createLogEntry('', [], null);
        } catch (\Exception $e) {
            $this->assertEquals('Logged in user is not set!', $e->getMessage());

            return;
        }

        $this->fail('Expected exception not thrown');
    }


    /**
     * Test if the exceptions for an unknown change type is thrown correctly
     */
    public function testCreateLogEntryUnknownChangeTypeException(): void
    {
        $notificationMock = $this->createMock(EmailNotificationSender::class);
        $securityMock = $this->createMock(Security::class);
        $securityMock->method('getUser')->willReturn($this->getFixtureReference('member_entry_last_name1'));
        $logCreator = new LogMessageCreator($this->getEntityManager(), $notificationMock, $securityMock);

        try {
            $logCreator->createLogEntry('bla', '', null);
        } catch (Exception $e) {
            $this->assertEquals('Change type bla not found!', $e->getMessage());

            return;
        }

        $this->fail('Expected exception not thrown');
    }


    /**
     * Test if the log entry is created correctly, when the changed by user is set via the security context and directly
     *
     * @throws Exception
     */
    public function testCreateLogEntryWithUserSetDirectly(): void
    {
        /** @var MemberEntry $securityUser */
        $securityUser = $this->getFixtureReference('member_entry_last_name1');
        /** @var MemberEntry $logUser */
        $logUser = $this->getFixtureReference('member_entry_last_name3');

        $this->createAndCheckLogMessage($securityUser, $logUser, $logUser);
    }


    /**
     * Test if the log entry is created correctly, when the changed by user is set via the security context.
     *
     * @throws Exception
     */
    public function testCreateLogEntryWithUserFromSecurityContext(): void
    {
        /** @var MemberEntry $securityUser */
        $securityUser = $this->getFixtureReference('member_entry_last_name1');

        $this->createAndCheckLogMessage($securityUser, $securityUser, null);
    }


    /**
     * Test if the log entry is created correctly user the special method for messages without a logged-in user.
     *
     * @throws Exception
     */
    public function testCreateLogEntryWithoutLoggedInUser(): void
    {
        $notificationMock = $this->createMock(EmailNotificationSender::class);
        $securityMock = $this->createMock(Security::class);
        $securityMock->method('getUser')->willReturn(null);
        $logCreator = new LogMessageCreator($this->getEntityManager(), $notificationMock, $securityMock);

        $message = 'Login failed';
        $logMessage = $logCreator->createLogEntryWithoutLoggedInUser('login error', $message);

        $this->assertNull($logMessage->getChangedByMember());
        $this->assertNull($logMessage->getChangesOnMember());
        $this->assertEquals('Login error', $logMessage->getChangeType()->getChangeType());
        $this->assertEquals($message, $logMessage->getLogentry());
        $this->assertNull($logMessage->getId());

        // Check if the log message is stored in the database correctly (i.e. if the persist for this entity is already
        // called)
        $this->getEntityManager()->flush();
        $this->assertNotNull($logMessage->getId());
    }


    /**
     * Test if creating the log entry sends an email
     *
     * @throws Exception
     */
    public function testSendEmailNotification(): void
    {
        /** @var MemberEntry $member */
        $member = $this->getFixtureReference('member_entry_last_name1');

        $notificationMock = $this->createMock(EmailNotificationSender::class);
        $notificationMock->expects($this->exactly(1))
                         ->method('sendEmailNotification');
        $securityMock = $this->createMock(Security::class);
        $securityMock->method('getUser')->willReturn($member);
        $logCreator = new LogMessageCreator($this->getEntityManager(), $notificationMock, $securityMock);

        $content = new ComplexLogEntryDoubleDataField();
        $content->setMainMessage('message');
        $content->addDataField('status', ['old value', 'new value']);
        $logCreator->createLogEntry('system settings change', $content, $member);
    }


    /**
     * Test disabling sending email notifications really stops sending emails
     *
     * @throws Exception
     */
    public function testDisableEmailNotification(): void
    {
        /** @var MemberEntry $member */
        $member = $this->getFixtureReference('member_entry_last_name1');

        $notificationMock = $this->createMock(EmailNotificationSender::class);
        $notificationMock->expects($this->exactly(1))
                         ->method('sendEmailNotification');
        $securityMock = $this->createMock(Security::class);
        $securityMock->method('getUser')->willReturn($member);

        $content = new ComplexLogEntryDoubleDataField();
        $content->setMainMessage('message');
        $content->addDataField('status', ['old value', 'new value']);

        $logCreator = new LogMessageCreator($this->getEntityManager(), $notificationMock, $securityMock);
        $logCreator->disableEmailNotifications();

        $logCreator->createLogEntry('system settings change', $content, $member);
        $logCreator->enableEmailNotifications();
        $logCreator->createLogEntry('system settings change', $content, $member);
    }


    /**
     * Test if the service is registered correctly in the service container
     */
    public function testServiceRegistration(): void
    {
        $this->checkContainerRegistration(LogMessageCreator::class);
    }


    /**
     * @param MemberEntry      $securityUser
     * @param MemberEntry      $expectedChangedBy
     * @param MemberEntry|null $logUser
     *
     * @throws Exception
     */
    private function createAndCheckLogMessage(MemberEntry $securityUser, MemberEntry $expectedChangedBy, ?MemberEntry $logUser): void
    {
        /** @var MemberEntry $member2 */
        $member2 = $this->getFixtureReference('member_entry_last_name2');
        $notificationMock = $this->createMock(EmailNotificationSender::class);
        $securityMock = $this->createMock(Security::class);
        $securityMock->method('getUser')->willReturn($securityUser);
        $logCreator = new LogMessageCreator($this->getEntityManager(), $notificationMock, $securityMock);
        $logCreator->setUser($logUser);

        $content = new ComplexLogEntryDoubleDataField();
        $content->setMainMessage('message');
        $content->addDataField('status', ['old value', 'new value']);
        $logMessage = $logCreator->createLogEntry('system settings change', $content, $member2);

        $this->assertEquals($expectedChangedBy->getFirstName(), $logMessage->getChangedByMember()->getFirstName());
        $this->assertEquals($member2->getFirstName(), $logMessage->getChangesOnMember()->getFirstName());
        $this->assertEquals('System settings change', $logMessage->getChangeType()->getChangeType());
        $this->assertEquals($content->getDataAsArray(), $logMessage->getComplexLogEntry()->getDataAsArray());
        $this->assertNull($logMessage->getId());

        // Check if the log message is stored in the database correctly (i.e. if the persist for this entity is already
        // called)
        $this->getEntityManager()->persist($securityUser);
        $this->getEntityManager()->flush();
        $this->assertNotNull($logMessage->getId());
    }

}
