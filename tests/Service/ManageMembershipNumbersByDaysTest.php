<?php

/*
 * Copyright (C) 2022 by Luc Hamers <vereniging@hamers.de>
 *
 * This file is part of Vereniging.
 *
 * Vereniging is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Vereniging is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Vereniging.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Tests\Service;

use App\Entity\MemberEntry;
use App\Entity\MembershipNumber;
use App\Service\LogMessageCreator;
use App\Service\ManageMembershipNumbersByDays;
use App\Tests\TestCase;
use DateTime;
use Exception;

/**
 * Class MembershipNumberEnumeratorTest
 */
class ManageMembershipNumbersByDaysTest extends TestCase
{
    /**
     * @inheritDoc
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->loadAllFixtures();
        $this->setLoggedInUserForLogging();
    }


    /**
     * Test getting new membership numbers
     *
     * @throws Exception
     */
    public function testAddMembershipNumber(): void
    {
        $enumerator = new ManageMembershipNumbersByDays(4, $this->getEntityManager());

        // Get a membership number for a member which has group members. This should return the membership number 1
        /** @var MemberEntry $member1 */
        $member1 = $this->getFixtureReference('member_entry_last_name1');
        $enumerator->addMembershipNumber($member1);
        $this->assertEquals(1, $member1->getMembershipNumber()->getMembershipNumber());
        $this->assertNull($member1->getMembershipNumber()->getDateRemoved());

        // Get an old membership number of which the blocking time has expired
        /** @var MemberEntry $member4 */
        $member4 = $this->getFixtureReference('member_entry_last_name4');

        $enumerator->addMembershipNumber($member4);
        $this->assertEquals(10, $member4->getMembershipNumber()->getMembershipNumber());
        $this->assertNull($member4->getMembershipNumber()->getDateRemoved());
        // This checks if the member entries collection in the membership number is updated correctly
        $this->assertEquals(1, $member4->getMembershipNumber()->getMemberEntries()->count());

        // For the next test the current membership number has to be saved to the database to clear the date_removed.
        $this->getEntityManager()->flush();

        // Get a new membership number, since there are no numbers left to recycle
        $enumerator->addMembershipNumber($member4);
        $this->assertEquals(21, $member4->getMembershipNumber()->getMembershipNumber());
        $this->assertNull($member4->getMembershipNumber()->getDateRemoved());

        // When there are no membership numbers in the database, a new number has to be created with the index 1.
        $this->clearAllMembershipNumbers();
        $enumerator->addMembershipNumber($member4);
        $this->assertEquals(1, $member4->getMembershipNumber()->getMembershipNumber());
        $this->assertNull($member4->getMembershipNumber()->getDateRemoved());

        // Test if cascade persist is set correctly
        $this->getEntityManager()->flush();
        $this->getEntityManager()->refresh($member4);
        $this->assertEquals(1, $member4->getMembershipNumber()->getMembershipNumber());
        $this->assertNull($member4->getMembershipNumber()->getDateRemoved());
    }


    /**
     * Test removing a membership number from a member entry.
     *
     * @throws Exception
     */
    public function testRemoveMembershipNumber(): void
    {
        /** @var MemberEntry $member1 */
        $member1 = $this->getFixtureReference('member_entry_last_name1');
        /** @var LogMessageCreator $logMessageCreator */
        $logMessageCreator = $this->getContainer()->get(LogMessageCreator::class);
        $logMessageCreator->setUser($member1);

        $enumerator = new ManageMembershipNumbersByDays(4, $this->getEntityManager());

        // Remove the membership number for a member which has no group members. This should remove the number and mark
        // it as reusable in the database.
        /** @var MemberEntry $member3 */
        $member3 = $this->getFixtureReference('member_entry_last_name3');
        $enumerator->removeMembershipNumber($member3);
        $this->getEntityManager()->flush();
        $this->assertNull($member3->getMembershipNumber());
        $membershipNumber3 = $this->getEntityManager()->getRepository(MembershipNumber::class)->find(3);
        $this->assertEquals(3, $membershipNumber3->getMembershipNumber());

        // Timestamps can differ up to 2 seconds to copy with slow test systems.
        $this->assertEqualsWithDelta(new DateTime(), $membershipNumber3->getDateRemoved(), 2);
        $this->assertCount(0, $membershipNumber3->getMemberEntries());

        // Trying to remove a membership number from a member1 which doesn't have a number should not throw an exception
        /** @var MemberEntry $member4 */
        $member4 = $this->getFixtureReference('member_entry_last_name4');
        $enumerator->removeMembershipNumber($member4);
        $this->assertNull($member4->getMembershipNumber());

        // Remove the membership number for a member1 which has group members should only remove the number from the
        // member1 entry, but not set the membership number entry to reusable.
        $membershipNumber1 = $member1->getMembershipNumber();
        $enumerator->removeMembershipNumber($member1);
        $this->assertNull($member1->getMembershipNumber());
        $this->assertEquals(3, $membershipNumber1->getMemberEntries()->count());

        // Now remove the membership number for the other members of the group and check if the number is set for reuse.
        /** @var MemberEntry $member10 */
        $member10 = $this->getFixtureReference('member_entry_last_name10');
        /** @var MemberEntry $member11 */
        $member11 = $this->getFixtureReference('member_entry_last_name11');
        /** @var MemberEntry $member12 */
        $member12 = $this->getFixtureReference('member_entry_last_name12');
        $enumerator->removeMembershipNumber($member10);
        $enumerator->removeMembershipNumber($member11);
        $this->assertNull($member10->getMembershipNumber());
        $this->assertNull($member11->getMembershipNumber());
        $this->assertCount(1, $member12->getMembershipNumber()->getMemberEntries());
        $enumerator->removeMembershipNumber($member12);
        $this->assertNull($member3->getMembershipNumber());
        /** @var MembershipNumber $membershipNumber1 */
        $membershipNumber1 = $this->getEntityManager()->getRepository(MembershipNumber::class)
                                                      ->find(1);
        $this->assertEquals(1, $membershipNumber1->getMembershipNumber());
        $this->assertLessThanOrEqual(1, $membershipNumber1->getDateRemoved()->diff(new DateTime())->format("%S"));
        $this->assertEquals(0, $membershipNumber1->getMemberEntries()->count());
    }


    /**
     * Test if the service is registered correctly in the service container
     */
    public function testServiceRegistration(): void
    {
        $this->checkContainerRegistration(ManageMembershipNumbersByDays::class);
    }


    /**
     * Clear all membership numbers from the database
     */
    private function clearAllMembershipNumbers(): void
    {
        $allMembers = $this->getEntityManager()->getRepository(MemberEntry::class)->findAll();
        foreach ($allMembers as $member) {
            $member->setMembershipNumber();
        }
        $allMembershipNumbers = $this->getEntityManager()->getRepository(MembershipNumber::class)->findAll();
        foreach ($allMembershipNumbers as $membershipNumber) {
            $this->getEntityManager()->remove($membershipNumber);
        }
        $this->getEntityManager()->flush();
    }
}
