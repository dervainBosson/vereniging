<?php

/*
 * Copyright (C) 2022 by Luc Hamers <vereniging@hamers.de>
 *
 * This file is part of Vereniging.
 *
 * Vereniging is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Vereniging is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Vereniging.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Tests\Service;

use App\Entity\Status;
use App\Service\ListTypeManager;
use App\Service\LogMessageCreator;
use App\Tests\TestCase;
use Exception;
use Symfony\Component\Form\FormFactoryInterface;

/**
 * Class ListTypeManagerTest
 */
class ListTypeManagerTest extends TestCase
{
    /**
     * @inheritDoc
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->loadAllFixtures();
    }


    /**
     * Test the generateHeaders method. This should distinguish between entities which have a translated main value and
     * entities that don't.
     */
    public function testGenerateHeaders(): void
    {
        // Use the status entity, which has to be translated. The default locale English mus tbe in first place.
        $manager = $this->createClassUnderTest();
        $expected = ["status|en", "status|de", "status|es", "status|nl", "hasMembershipNumber"];
        $this->assertEquals($expected, $manager->generateHeaders('Status'));

        // Use the status entity, which has to be translated. The default locale Spanish mus tbe in first place.
        $manager = $this->createClassUnderTest('es');
        $expected = ["status|es", "status|de", "status|en", "status|nl", "hasMembershipNumber"];
        $this->assertEquals($expected, $manager->generateHeaders('Status'));

        // Use the title entity, which has no translations. The default locale doesn't matter
        $manager = $this->createClassUnderTest();
        $expected = ["title", "afterName"];
        $this->assertEquals($expected, $manager->generateHeaders('Title'));
    }


    /**
     * test reading the locales
     */
    public function testGetLocales(): void
    {
        // Use English as the default locale
        $manager = $this->createClassUnderTest();
        $expected = ["en", "de", "es", "nl"];
        $this->assertEquals($expected, $manager->getLocales());

        // Use Spanish as default locale
        $manager = $this->createClassUnderTest('es');
        $expected = ["es", "de", "en", "nl"];
        $this->assertEquals($expected, $manager->getLocales());
    }


    /**
     * Test reading the extra fields from an entity
     *
     * @throws Exception
     */
    public function testGetExtraFields(): void
    {
        $manager = $this->createClassUnderTest();
        $manager->generateHeaders('phoneNumberType');
        $expected = ['phoneNumberType_de', 'phoneNumberType_es', 'phoneNumberType_nl'];
        $this->assertEquals($expected, $manager->getExtraFields());

        $manager->generateHeaders('title');
        $this->assertEquals([], $manager->getExtraFields());
    }


    /**
     * Test reading the main field from an entity
     *
     * @throws Exception
     */
    public function testGetMainField(): void
    {
        $manager = $this->createClassUnderTest();
        $manager->generateHeaders('phoneNumberType');
        $this->assertEquals('phoneNumberType', $manager->getMainFieldName());

        $manager->generateHeaders('title');
        $this->assertEquals('title', $manager->getMainFieldName());
    }


    /**
     * test reading the locales
     *
     * @throws Exception
     */
    public function testGetForms(): void
    {
        $manager = $this->createClassUnderTest();
        $manager->generateHeaders('status');
        $form = $manager->generateForm();
        $this->assertCount(3, $form->getData()->getMembers());

        // It is only possible here to test the entity form fields. The additional fields for the translations are
        // missing. We will test those in the controller. So we will just check here that the correct entities are used
        // in the form.
        /** @var Status[] $members */
        $members = $form->getData()->getMembers();
        $this->assertEquals('Former member', $members[0]);
        $this->assertEquals('Future member', $members[1]);
        $this->assertEquals('Member', $members[2]);
    }


    /**
     * Test if the service is registered correctly
     */
    public function testServiceContainerRegistration(): void
    {
        $this->checkContainerRegistration(ListTypeManager::class);
    }


    /**
     * Generate a ListTypeManager object
     *
     * @param string $defaultLocale Use this to create the object with a different default locale
     *
     * @return ListTypeManager
     */
    private function createClassUnderTest(string $defaultLocale = 'en'): ListTypeManager
    {
        /** @var FormFactoryInterface $formFactory */
        $formFactory = $this->getContainer()->get(FormFactoryInterface::class);

        return new ListTypeManager($formFactory, $this->getEntityManager(), $defaultLocale);
    }
}
