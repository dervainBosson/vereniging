<?php

/*
 * Copyright (C) 2022 by Luc Hamers <vereniging@hamers.de>
 *
 * This file is part of Vereniging.
 *
 * Vereniging is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Vereniging is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Vereniging.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Tests\Service;

use App\Entity\MailingList;
use App\Service\MailingListUpdateInterface;
use App\Service\MajorDomoUpdater;
use App\Tests\TestCase;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;

/**
 * Class MajorDomoUpdaterTest
 */
class MajorDomoUpdaterTest extends TestCase
{
    /**
     * @inheritDoc
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->loadAllFixtures();
    }


    /**
     * Test adding and removing email addresses.
     */
    public function testAddRemoveEmailAddress(): void
    {
        /** @var MailingList $mailingList */
        $mailingList = $this->getFixtureReference('mailinglist1');

        // Create a mock for the mailer.
        $mailerMock = $this->createMock(MailerInterface::class);

        /** @var MailingListUpdateInterface $majorDomoUpdater */
        $majorDomoUpdater = new MajorDomoUpdater($mailerMock, 'sender@test.com');

        $mailerMock->expects($this->exactly(2))
            ->method('send')
            ->withConsecutive(
                [
                    $this->callback(function (Email $expected) {
                        $body = 'approve password1 subscribe MailingList_1 me@mail.com';

                        return $this->checkMailMessage('sender@test.com', 'mailinglist1@mail.org', 'Update mailinglist "MailingList_1"', $body, $expected);
                    }),
                ],
                [
                    $this->callback(function (Email $expected) {
                        $body = 'approve password1 unsubscribe MailingList_1 me@mail.com';

                        return $this->checkMailMessage('sender@test.com', 'mailinglist1@mail.org', 'Update mailinglist "MailingList_1"', $body, $expected);
                    }),
                    ]
            );

        $majorDomoUpdater->addEmailAddress($mailingList, 'me@mail.com');
        $majorDomoUpdater->removeEmailAddress($mailingList, 'me@mail.com');
    }


    /**
     * Test the getters
     */
    public function testGetter(): void
    {
        // Create a mock for the mailer.
        $mailerMock = $this->createMock(MailerInterface::class);

        $majorDomoUpdater = new MajorDomoUpdater($mailerMock, 'sender@test.com');
        $this->assertCount(3, $majorDomoUpdater->getFieldNames(true));
        $this->assertEquals(['List name', 'Management email', 'Management password'], $majorDomoUpdater->getFieldNames(true));
        $this->assertCount(3, $majorDomoUpdater->getFieldNames(false));
        $this->assertEquals(['List name', 'Management email', 'Management password'], $majorDomoUpdater->getFieldNames(false));
    }


    /**
     * Check the content of an email message.
     *
     * @param string $from    Expected from address
     * @param string $to      Expected to address
     * @param string $subject Expected subject
     * @param string $body    Expected body
     * @param Email  $message Message to compare to
     *
     * @return bool true when the message is correct
     */
    private function checkMailMessage(string $from, string $to, string $subject, string $body, Email $message): bool
    {
        if ($message->getFrom()[0]->getAddress() !== $from) {
            return false;
        }

        if ($message->getTo()[0]->getAddress() !== $to) {
            return false;
        }

        if ($message->getSubject() !== $subject) {
            return false;
        }

        if ($message->getTextBody() !== $body) {
            return false;
        }

        return true;
    }
}
