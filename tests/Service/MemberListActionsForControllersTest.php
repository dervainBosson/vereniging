<?php

/*
 * Copyright (C) 2022 by Luc Hamers <vereniging@hamers.de>
 *
 * This file is part of Vereniging.
 *
 * Vereniging is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Vereniging is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Vereniging.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Tests\Service;

use App\Entity\Address;
use App\Entity\AddressType;
use App\Entity\Country;
use App\Entity\LogfileEntry;
use App\Entity\MailingList;
use App\Entity\MemberAddressMap;
use App\Entity\MemberEntry;
use App\Entity\MembershipFeeTransaction;
use App\Entity\MembershipNumber;
use App\Entity\PhoneNumber;
use App\Entity\PhoneNumberType;
use App\Entity\SerialLetter;
use App\Entity\SerialLetterContent;
use App\Entity\Status;
use App\Service\LogEventsDisabler;
use App\Service\LogMessageCreator;
use App\Service\MailingListUpdateInterface;
use App\Service\ManageMembershipNumberInterface;
use App\Service\MemberListActionsForControllers;
use App\Service\SerialEmailGenerator;
use App\Service\SerialLetterGenerator;
use App\Tests\TestCase;
use DateTime;
use Exception;
use PHPUnit\Framework\MockObject\MockObject;
use Symfony\Component\Mime\Email;
use Throwable;

/**
 * Class MemberListActionsForControllersTest
 */
class MemberListActionsForControllersTest extends TestCase
{
    private MockObject $mailingListUpdaterMock;

    /**
     * @inheritDoc
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->loadAllFixtures();
        LogfileEntry::prepareTranslation($this->getEntityManager(), $this->getTranslator(), 'en');
    }


    /**
     * Test changing user status
     *
     * @throws Exception
     */
    public function testChangeMemberStatus(): void
    {
        $this->clearAllLogFiles();
        $this->setLoggedInUserForLogging();

        $actions = $this->getMemberListActionsForControllers();

        // Check response for empty member list
        $response = $actions->changeMemberStatus([], 0);
        $this->assertEquals(204, $response->getStatusCode());
        $this->assertEquals('', $response->getContent());

        // Check is status change works for some members
        $repo = $this->getEntityManager()->getRepository(MemberEntry::class);
        $members = $repo->findBy(['firstName' => ['Firstname1', 'Firstname2', 'Firstname3']]);
        $memberIds = [];
        foreach ($members as $member) {
            $memberIds[] = $member->getId();
        }
        /** @var Status $formerMemberStatus */
        $formerMemberStatus = $this->getFixtureReference('status_former_member');
        $response = $actions->changeMemberStatus($memberIds, $formerMemberStatus->getId());
        $this->assertEquals(204, $response->getStatusCode());
        $this->assertEquals('status updated', $response->getContent());
        foreach ($members as $member) {
            $this->getEntityManager()->refresh($member);
            $this->assertEquals($formerMemberStatus, $member->getStatus());
        }

        $logMessages = $this->getAllLogs(false);
        $this->assertCount(3, $logMessages);

        for ($i = 0; $i < count($members); $i++) {
            $this->assertEquals(['status' => ['Member', 'Former member']], $logMessages[$i]->getComplexLogEntry()->getDataAsArray());
            $this->asserttrue(in_array($logMessages[$i]->getChangesOnMember()->getId(), $memberIds));
        }
    }


    /**
     * Test generating serial letters
     *
     * @throws Exception
     */
    public function testCreateSerialLetters(): void
    {
        $this->clearAllLogFiles();
        $this->setLoggedInUserForLogging();

        $actions = $this->getMemberListActionsForControllers();

        $response = $actions->createSerialLetterAction([], 0);
        $this->assertEquals(204, $response->getStatusCode());
        $this->assertEquals('', $response->getContent());

        /** @var MemberEntry $member */
        $member = $this->getFixtureReference('member_entry_last_name1');
        /** @var SerialLetter $serialLetter1 */
        $serialLetter1 = $this->referenceRepository->getReference('serial_letter1');

        // When at least one member is selected, a pdf should be generated
        $response = $actions->createSerialLetterAction([$member->getId()], $serialLetter1->getId());

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertStringContainsString('max-age=0', $response->headers->get('cache-control'));
        // The file is roughly about 32kiB
        $this->assertGreaterThan(30, $response->headers->get('content-length')/1024);
        $this->assertLessThan(35, $response->headers->get('content-length')/1024);
        $this->assertCount(1, $this->getAllLogs());
        $this->assertEquals('Generated serial letters (en: Serial letter1; nl: Seriebrief1) for selected members', $this->getAllLogs()[0]->getLogentry());

        // Now select all members and generate the serial letter
        $this->clearAllLogFiles();
        $members = $this->getEntityManager()->getRepository(MemberEntry::class)->findAll();
        $memberIds = [];
        foreach ($members as $member) {
            $memberIds[] = $member->getId();
        }

        $response = $actions->createSerialLetterAction($memberIds, $serialLetter1->getId());

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertStringContainsString('max-age=0', $response->headers->get('cache-control'));
        // The file is roughly about 39kiB
        $this->assertGreaterThan(35, $response->headers->get('content-length')/1024);
        $this->assertLessThan(45, $response->headers->get('content-length')/1024);
    }


    /**
     * Test generating serial letters
     *
     * @throws Exception
     */
    public function testCreateSerialLettersWithError(): void
    {
        $actions = $this->getMemberListActionsForControllers();

        $members = $this->getEntityManager()->getRepository(MemberEntry::class)->findAll();
        $memberIds = [];
        foreach ($members as $member) {
            $memberIds[] = $member->getId();
        }

        // Now check the reaction to an error during pdf generation. To do this, change the content of the first serial
        // letter to "$".
        /** @var SerialLetterContent $serialLetter1 */
        $serialLetter1 = $this->getFixtureReference('serial_letter_content11');
        $serialLetter1->setContent('$');
        $this->getEntityManager()->flush();
        $response = $actions->createSerialLetterAction($memberIds, $serialLetter1->getSerialLetter()->getId());

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertEquals('attachment;filename="letter.log"', $response->headers->get('content-disposition'));
    }


    /**
     * Test generating serial emails
     *
     * @throws Exception
     * @throws Throwable
     */
    public function testCreateSerialEmails(): void
    {
        $this->clearAllLogFiles();
        $this->setLoggedInUserForLogging();

        $actions = $this->getMemberListActionsForControllers();

        $response = $actions->createSerialEmailAction([], 0, 0);
        $this->assertEquals(204, $response->getStatusCode());
        $this->assertEquals('', $response->getContent());

        /** @var MemberEntry $member */
        $member = $this->getFixtureReference('member_entry_last_name1');
        /** @var SerialLetter $serialLetter1 */
        $serialLetter1 = $this->referenceRepository->getReference('serial_letter1');

        $actions->createSerialEmailAction([1], 1, 3);

        $this->assertEmailCount(1);
        /** @var Email $email */
        $email = $this->getMailerMessage(0);
        $this->assertEmailHeaderSame($email, 'To', 'firstname1@email.com');
        $this->assertEquals("serial email 1 content: Dear Mr. Lastname1, \nThis is a serial letter1.\nKind regards,\n<table><tr><td>Dipl.-Ing. Firstname1 Lastname1</td><td>Firstname5 Lastname5</td></tr><tr><td>Board - Chairman</td><td></td></tr></table>", $email->getHtmlBody());
        $this->assertEquals("serial email 1 content: Dear Mr. Lastname1, \nThis is a serial letter1.\nKind regards,\nDipl.-Ing. Firstname1 Lastname1Firstname5 Lastname5Board - Chairman", $email->getTextBody());
        $this->assertCount(1, $this->getAllLogs());
        $this->assertEquals('Generated serial emails (en: Serial letter1; nl: Seriebrief1) with email template (en: Serial email; nl: Serie-email; de: Serien-E-Mail) for selected members', $this->getAllLogs()[0]->getLogentry());

        // Now select all members and send the serial email
        $this->clearAllLogFiles();
        $members = $this->getEntityManager()->getRepository(MemberEntry::class)->findAll();
        $memberIds = [];
        foreach ($members as $member) {
            $memberIds[] = $member->getId();
        }

        $actions->createSerialEmailAction($memberIds, 1, 4);
        $this->assertEmailCount(12);
        /** @var Email[] $messages */
        $messages = $this->getMailerMessages();
        // Remove the message from the first test
        array_splice($messages, 0, 1);
        foreach ($messages as $message) {
            $this->assertEmailTextBodyContains($message, 'serial email 2 content:');
        }
    }


    /**
     * Test generating membership fee transactions.
     *
     * @throws Exception
     */
    public function testGenerateMembershipFeeTransactions(): void
    {
        $this->clearAllLogFiles();
        $this->setLoggedInUserForLogging();

        $actions = $this->getMemberListActionsForControllers();
        $response = $actions->generateMembershipFeeTransactions([]);
        $this->assertEquals(204, $response->getStatusCode());
        $this->assertEquals('', $response->getContent());

        /** @var MembershipNumber $number1 */
        $number1 = $this->getFixtureReference('membership_number_1');
        /** @var MembershipNumber $number2 */
        $number2 = $this->getFixtureReference('membership_number_2');
        $response = $actions->generateMembershipFeeTransactions([
            $number1->getMembershipNumber(),
            $number2->getMembershipNumber(),
        ]);
        $this->assertEquals(204, $response->getStatusCode());
        $this->assertEquals('membership fee requests generated', $response->getContent());
        $this->assertEqualsWithDelta(124, $number1->getOutstandingDebt(), 0.1);
        $this->assertEqualsWithDelta(44, $number2->getOutstandingDebt(), 0.1);

        $members = array_merge($number1->getMemberEntries()->toArray(), $number2->getMemberEntries()->toArray());
        $logMessages = $this->getAllLogs(false);
        $this->assertCount(count($members), $logMessages);
        $i = 0;
        /** @var MemberEntry $member */
        foreach ($members as $member) {
            $fee = number_format($member->getMembershipType()->getMembershipFee(), 2);
            $number = $member->getMembershipNumber()->getMembershipNumber();
            $expected = "Created contribution claim of $fee for membership number $number";
            $this->assertEquals($expected, $logMessages[$i]->getLogentry());
            $this->assertEquals($member, $logMessages[$i++]->getChangesOnMember());
        }
    }


    /**
     * Test generating membership fee transactions.
     *
     * @throws Exception
     */
    public function testCloseMembershipFeeTransactions(): void
    {
        $this->clearAllLogFiles();
        $this->setLoggedInUserForLogging();

        $actions = $this->getMemberListActionsForControllers();
        $response = $actions->closeMembershipFeeTransactions([]);
        $this->assertEquals(204, $response->getStatusCode());
        $this->assertEquals('', $response->getContent());

        /** @var MembershipNumber $number1 */
        $number1 = $this->getFixtureReference('membership_number_1');
        /** @var MembershipNumber $number2 */
        $number2 = $this->getFixtureReference('membership_number_2');
        $response = $actions->closeMembershipFeeTransactions([
            $number1->getMembershipNumber(),
            $number2->getMembershipNumber(),
        ]);
        $this->assertEquals(204, $response->getStatusCode());
        $this->assertEquals('membership fee requests paid', $response->getContent());
        $this->assertEqualsWithDelta(52, $number1->getOutstandingDebt(), 0.1);
        $this->assertEqualsWithDelta(0, $number2->getOutstandingDebt(), 0.1);

        $members = array_merge($number1->getMemberEntries()->toArray(), $number2->getMemberEntries()->toArray());
        $logMessages = $this->getAllLogs(false);
        $this->assertCount(count($members), $logMessages);
        $i = 0;
        /** @var MemberEntry $member */
        foreach ($members as $member) {
            $number = $member->getMembershipNumber()->getMembershipNumber();
            if (2 === $number) {
                $fee = '20.00';
            } else {
                $fee = '36.00';
            }
            $expected = "Closed contribution claim from Nov 28, 2020 of $fee for membership number $number";
            $this->assertEquals($expected, $logMessages[$i]->getLogentry());
            $this->assertEquals($member, $logMessages[$i++]->getChangesOnMember());
        }

        $response = $actions->closeMembershipFeeTransactions([
            $number1->getMembershipNumber(),
            $number2->getMembershipNumber(),
        ]);
        $this->assertEquals(204, $response->getStatusCode());
        $this->assertEquals('membership fee requests paid', $response->getContent());
        $this->assertEqualsWithDelta(0, $number1->getOutstandingDebt(), 0.1);
        $this->assertEqualsWithDelta(0, $number2->getOutstandingDebt(), 0.1);
    }


    /**
     * Test generating membership fee transactions.
     *
     * @throws Exception
     */
    public function testSetUseDirectDebit(): void
    {
        $this->clearAllLogFiles();
        $this->setLoggedInUserForLogging();

        $actions = $this->getMemberListActionsForControllers();
        $response = $actions->setUseDirectDebit([], true);
        $this->assertEquals(204, $response->getStatusCode());
        $this->assertEquals('', $response->getContent());

        /** @var MembershipNumber $number1 */
        $number1 = $this->getFixtureReference('membership_number_1');
        /** @var MembershipNumber $number2 */
        $number2 = $this->getFixtureReference('membership_number_2');
        /** @var MembershipNumber $number3 */
        $number3 = $this->getFixtureReference('membership_number_3');
        $this->assertTrue($number1->getUseDirectDebit());
        $this->assertFalse($number2->getUseDirectDebit());
        $this->assertFalse($number3->getUseDirectDebit());

        // Set use direct debit for membership numbers 1 and 2. This only changes the settings for number 2
        $response = $actions->setUseDirectDebit([
            $number1->getMembershipNumber(),
            $number2->getMembershipNumber(),
        ], true);
        $this->assertEquals(204, $response->getStatusCode());
        $this->assertEquals('set memberships to use direct debit', $response->getContent());
        $this->assertTrue($number1->getUseDirectDebit());
        $this->assertTrue($number2->getUseDirectDebit());
        $this->assertFalse($number3->getUseDirectDebit());

        $logMessages = $this->getAllLogs(false);
        $this->assertCount(1, $logMessages);
        $expected = "Use direct debit for membership number 2";
        $this->assertEquals($expected, $logMessages[0]->getLogentry());
        $this->assertEquals($number2->getMemberEntries()[0], $logMessages[0]->getChangesOnMember());

        // Remove use direct debit for membership numbers 1, 2 and 4. This changes the settings for numbers 1 and 2
        $this->clearAllLogFiles();
        $response = $actions->setUseDirectDebit([
            $number1->getMembershipNumber(),
            $number2->getMembershipNumber(),
            $number3->getMembershipNumber(),
        ], false);
        $this->assertEquals(204, $response->getStatusCode());
        $this->assertEquals('set memberships to not use direct debit', $response->getContent());
        $this->assertFalse($number1->getUseDirectDebit());
        $this->assertFalse($number2->getUseDirectDebit());
        $this->assertFalse($number3->getUseDirectDebit());

        /** @var MemberEntry[] $members */
        $members = array_merge($number1->getMemberEntries()->toArray(), $number2->getMemberEntries()->toArray());
        $logMessages = $this->getAllLogs(false);
        $this->assertCount(5, $logMessages);
        for ($i = 0; $i < count($members); $i++) {
            $expected = "Don't use direct debit for membership number ".$members[$i]->getMembershipNumber()->getMembershipNumber();
            $this->assertEquals($expected, $logMessages[$i]->getLogentry());
            $this->assertEquals($members[$i], $logMessages[$i]->getChangesOnMember());
        }
    }


    /**
     * Test the generateMembershipFeeTransactionLogs method
     *
     * @throws Exception;
     */
    public function testGenerateMembershipFeeTransactionLogs(): void
    {
        $this->clearAllLogFiles();
        $this->setLoggedInUserForLogging();

        $actions = $this->getMemberListActionsForControllers();
        /** @var MembershipNumber $number2 */
        $number2 = $this->getFixtureReference('membership_number_2');

        // Create new membership fee transaction
        $fee = new MembershipFeeTransaction();
        $fee->setCreateDate(new DateTime('2019-1-1'));
        $fee->setAmount(20);
        $fee->setMembershipNumber($number2);

        $actions->generateMembershipFeeTransactionLogs(null, $fee->toArray());

        // Close the membership fee transaction
        $before = $fee->toArray();
        $fee->setCloseDate(new DateTime('2020-2-2'));
        $actions->generateMembershipFeeTransactionLogs($before, $fee->toArray());

        // Delete membership fee transaction
        $actions->generateMembershipFeeTransactionLogs($before, null);

        // Change content
        $before = $fee->toArray();
        $fee->setAmount(21);
        $fee->setCreateDate(new DateTime('2019-2-2'));
        $fee->setCloseDate(new DateTime('2020-3-3'));
        $actions->generateMembershipFeeTransactionLogs($before, $fee->toArray());

        // Check if an empty close date is OK
        $fee->setCloseDate(null);
        $actions->generateMembershipFeeTransactionLogs($before, $fee->toArray());

        // Check that content without changes does not generate a log message
        $actions->generateMembershipFeeTransactionLogs($before, $before);

        // Save the created log messages to the database
        $this->getEntityManager()->flush();
        $logMessages = $this->getAllLogs(false);
        $this->assertCount(5, $logMessages);
        $expected = "Created contribution claim of 20.00 for membership number ".$number2->getMembershipNumber();
        $this->assertEquals($expected, $logMessages[0]->getLogentry());
        $expected = "Closed contribution claim from Jan 1, 2019 of 20.00 for membership number 2";
        $this->assertEquals($expected, $logMessages[1]->getLogentry());
        $expected = "Deleted contribution claim from Jan 1, 2019 of 20.00 for membership number 2";
        $this->assertEquals($expected, $logMessages[2]->getLogentry());
        $expected = '{"numberOfDataFields":2,"mainMessage":"Updated %classname% entry %stringIdentifier%|membershipFeeTransaction,","dataArray":{"Membership number":[2,2],"Amount":["20.00","21.00"],"Opened":["Jan 1, 2019","Feb 2, 2019"],"Closed":["Feb 2, 2020","Mar 3, 2020"]}}';
        $this->assertEquals($expected, $logMessages[3]->getLogentry());
        $expected = '{"numberOfDataFields":2,"mainMessage":"Updated %classname% entry %stringIdentifier%|membershipFeeTransaction,","dataArray":{"Membership number":[2,2],"Amount":["20.00","21.00"],"Opened":["Jan 1, 2019","Feb 2, 2019"],"Closed":["Feb 2, 2020","-"]}}';
        $this->assertEquals($expected, $logMessages[4]->getLogentry());
    }


    /**
     * This tests if a member is deleted correctly. To do this, several database entries are created for member6, to be
     * able to test if all the foreign key relationships are also deleted or modified correctly.
     *
     * @throws Exception
     */
    public function testDeleteMember1(): void
    {
        $this->prepareDeleteData();

        // Check that the database has been prepared correctly
        $this->assertCount(8, $this->getAllLogs());

        $this->assertCount(9, $this->getEntityManager()->getRepository(MemberAddressMap::class)->findAll());
        $this->assertCount(7, $this->getEntityManager()->getRepository(Address::class)->findAll());
        $this->assertCount(1, $this->getEntityManager()->getRepository(Address::class)->findBy(['zip' => 'new zip']));
        $this->assertCount(6, $this->getEntityManager()->getRepository(PhoneNumber::class)->findAll());
        $this->assertCount(1, $this->getEntityManager()->getRepository(PhoneNumber::class)->findBy(['phoneNumber' => '123']));
        $membership5 = $this->getEntityManager()->getRepository(MembershipNumber::class)->findOneBy(['membershipNumber' => 5]);
        $this->assertNull($membership5->getDateRemoved());
        $this->assertCount(1, $membership5->getMembershipFeeTransactions());

        /** @var MemberEntry $member1 */
        $member1 = $this->getFixtureReference('member_entry_last_name1');
        /** @var MemberEntry $member6 */
        $member6 = $this->getEntityManager()->getRepository(MemberEntry::class)->findOneBy(['firstName' => 'Firstname6']);
        // Delete member6, assign its log messages to member1
        $actions = $this->getMemberListActionsForControllers();

        /** @var MailingList $list1 */
        $list1 = $this->getFixtureReference('mailinglist1');

        $this->mailingListUpdaterMock->expects($this->once())
            ->method('removeEmailAddress')
            ->with($this->equalTo($list1, $member6))
        ;

        $actions->deleteMember($member6, $member1);

        // Check that member6 is really deleted
        $this->assertCount(0, $this->getEntityManager()->getRepository(MemberEntry::class)->findBy(['firstName' => 'Firstname6']));

        // Check the remaining log messages. These are messages created by member6, which should now be assigned to
        // member1
        $logs = $this->getAllLogs();
        $this->assertCount(4, $logs);
        $this->assertEquals('Deleted user with database identifier 6', $logs[0]->getLogentry());
        $this->assertEquals($member1->getId(), $logs[1]->getChangedByMember()->getId());
        $this->assertEquals($member1->getId(), $logs[2]->getChangedByMember()->getId());
        $this->assertEquals($member1->getId(), $logs[3]->getChangedByMember()->getId());

        // Check that the new address has been deleted.
        $this->assertCount(8, $this->getEntityManager()->getRepository(MemberAddressMap::class)->findAll());
        $this->assertCount(6, $this->getEntityManager()->getRepository(Address::class)->findAll());
        $this->assertCount(0, $this->getEntityManager()->getRepository(Address::class)->findBy(['zip' => 'new zip']));

        // Check that the new phone number has been deleted.
        $this->assertCount(5, $this->getEntityManager()->getRepository(PhoneNumber::class)->findAll());
        $this->assertCount(0, $this->getEntityManager()->getRepository(PhoneNumber::class)->findBy(['phoneNumber' => '123']));

        // Check that the serial letter signers have been removed
        /** @var SerialLetter $serialLetter */
        $serialLetter = $this->getFixtureReference('serial_letter1');
        $this->assertNull($serialLetter->getFirstSignatureMember());
        $this->assertNull($serialLetter->getSecondSignatureMember());

        // Check that the membership number has been closed and all membership fee transactions have been deleted
        $membership5 = $this->getEntityManager()->getRepository(MembershipNumber::class)->findOneBy(['membershipNumber' => 5]);
        $this->assertNotNull($membership5->getDateRemoved());
        $this->assertCount(0, $membership5->getMembershipFeeTransactions());
    }


    /**
     * Test if deleting a member works correctly when this is member of a group:
     * The deleted member is the only group member and the group change is the only one in the log message
     *
     * @throws Exception
     */
    public function testDeleteMembersWithGroups1(): void
    {
        $this->setLoggedInUserForLogging();
        $this->clearAllLogFiles();

        /** @var MemberEntry $member6 */
        $member6 = $this->getFixtureReference('member_entry_last_name6');
        /** @var MemberEntry $member7 */
        $member7 = $this->getFixtureReference('member_entry_last_name7');
        $member6->addGroupMember($member7);
        $this->getEntityManager()->flush();

        /** @var MemberEntry $member1 */
        $member1 = $this->getFixtureReference('member_entry_last_name1');
        /** @var MemberEntry $member6 */
        $member6 = $this->getEntityManager()->getRepository(MemberEntry::class)->findOneBy(['firstName' => 'Firstname6']);
        // Delete member6, assign its log messages to member1
        $actions = $this->getMemberListActionsForControllers();
        $actions->deleteMember($member6, $member1);
        $logs = $this->getAllLogs();
        $this->assertCount(1, $logs);
        $this->assertEquals('Deleted user with database identifier 6', $logs[0]->getLogentry());
    }


    /**
     * Test if deleting a member works correctly when this is member of a group:
     * The deleted member is the only group member and the group change is not the only one in the log message
     *
     * @throws Exception
     */
    public function testDeleteMembersWithGroups2(): void
    {
        $this->setLoggedInUserForLogging();
        $this->clearAllLogFiles();

        /** @var MemberEntry $member6 */
        $member6 = $this->getFixtureReference('member_entry_last_name6');
        /** @var MemberEntry $member7 */
        $member7 = $this->getFixtureReference('member_entry_last_name7');
        $member6->addGroupMember($member7);
        $member7->setEmail('new email');
        $this->getEntityManager()->flush();

        /** @var MemberEntry $member1 */
        $member1 = $this->getFixtureReference('member_entry_last_name1');
        /** @var MemberEntry $member6 */
        $member6 = $this->getEntityManager()->getRepository(MemberEntry::class)->findOneBy(['firstName' => 'Firstname6']);
        // Delete member6, assign its log messages to member1
        $actions = $this->getMemberListActionsForControllers();
        $actions->deleteMember($member6, $member1);
        $logs = $this->getAllLogs();
        $this->assertCount(2, $logs);
        $this->assertEquals('Deleted user with database identifier 6', $logs[0]->getLogentry());
        $this->assertEquals(['email' => ["firstname7@email.com", "new email"]], $logs[1]->getComplexLogEntry()->getDataAsArray());
    }


    /**
     * Test if deleting a member works correctly when this is member of a group:
     * The deleted member is not the only group member
     *
     * @throws Exception
     */
    public function testDeleteMembersWithGroups3(): void
    {
        $this->setLoggedInUserForLogging();
        $this->clearAllLogFiles();

        /** @var MemberEntry $member5 */
        $member5 = $this->getFixtureReference('member_entry_last_name5');
        /** @var MemberEntry $member6 */
        $member6 = $this->getFixtureReference('member_entry_last_name6');
        /** @var MemberEntry $member7 */
        $member7 = $this->getFixtureReference('member_entry_last_name7');
        /** @var MemberEntry $member8 */
        $member8 = $this->getFixtureReference('member_entry_last_name8');
        $member6->addGroupMember($member5);
        $member6->addGroupMember($member7);
        $member6->addGroupMember($member8);
        $this->getEntityManager()->flush();

        /** @var MemberEntry $member1 */
        $member1 = $this->getFixtureReference('member_entry_last_name1');
        /** @var MemberEntry $member6 */
        $member6 = $this->getEntityManager()->getRepository(MemberEntry::class)->findOneBy(['firstName' => 'Firstname6']);
        // Delete member6, assign its log messages to member1
        $actions = $this->getMemberListActionsForControllers();
        $actions->deleteMember($member6, $member1);

        $logs = $this->getAllLogs();
        $this->assertCount(4, $logs);
        $this->assertEquals('Deleted user with database identifier 6', $logs[0]->getLogentry());
        $this->assertEquals(['Membership group members' => ["-", "Firstname5 Lastname5, Firstname7 Lastname7"]], $logs[1]->getComplexLogEntry()->getDataAsArray(false));
        $this->assertEquals(['Membership group members' => ["-", "Firstname5 Lastname5, Firstname8 Lastname8"]], $logs[2]->getComplexLogEntry()->getDataAsArray(false));
        $this->assertEquals(['Membership group members' => ["-", "Firstname7 Lastname7, Firstname8 Lastname8"]], $logs[3]->getComplexLogEntry()->getDataAsArray(false));
    }


    /**
     * Test if deleting a member also deletes the logs when this member is a serial letter signer
     *
     * @throws Exception
     */
    public function testDeleteMembersWithSerialLetterSigners(): void
    {
        $this->setLoggedInUserForLogging();
        $this->clearAllLogFiles();

        /** @var MemberEntry $member6 */
        $member6 = $this->getFixtureReference('member_entry_last_name6');
        /** @var MemberEntry $member7 */
        $member7 = $this->getFixtureReference('member_entry_last_name7');

        /** @var SerialLetter $serialLetter */
        $serialLetter = $this->getFixtureReference('serial_letter1');
        $serialLetter->setFirstSignatureMember($member6);
        $serialLetter->setSecondSignatureMember($member6);
        $serialLetter->setLastUpdatedBy($member7);
        $this->getEntityManager()->flush();
        $serialLetter->setFirstSignatureMember();
        $serialLetter->setSecondSignatureMember();
        $this->getEntityManager()->flush();

        /** @var MemberEntry $member1 */
        $member1 = $this->getFixtureReference('member_entry_last_name1');
        /** @var MemberEntry $member6 */
        $member6 = $this->getEntityManager()->getRepository(MemberEntry::class)->findOneBy(['firstName' => 'Firstname6']);
        // Delete member6, assign its log messages to member1
        $actions = $this->getMemberListActionsForControllers();
        $actions->deleteMember($member6, $member1);

        $logs = $this->getAllLogs();
        $this->assertCount(2, $logs);
        $this->assertEquals('Deleted user with database identifier 6', $logs[0]->getLogentry());
        $this->assertEquals(['firstSignatureMember' => ["Firstname1 Lastname1", "-"], 'secondSignatureMember' => ["Firstname5 Lastname5", "-"]], $logs[1]->getComplexLogEntry()->getDataAsArray(false));
    }

    /**
     * Test if the service is registered correctly in the service container
     */
    public function testServiceRegistration(): void
    {
        $this->checkContainerRegistration(MemberListActionsForControllers::class);
    }


    /**
     * This creates the log messages and prepares the other database objects needed to test the deletion of a member.
     * LogfileEntry - changedOn = member 6 - changeType = member data change
     * LogfileEntry - changedOn = member 6 - changeType = member settings change
     * LogfileEntry - changesBy = member 6 - changeType = login
     * LogfileEntry - changesBy = member 6 - changeType = member data change
     * LogfileEntry - changesBy = member 6 - changeType = member settings change
     * Address
     * MemberAddressMap - memberEntry = member 6
     * PhoneNumber - memberEntry = member 6
     * SerialLetter1 - firstSignatureMember = member 6
     * SerialLetter1 - secondSignatureMember = member 6
     * SerialLetter1 - lastUpdatedBy = member 6
     * MembershipFee
     *
     * @throws Exception
     */
    private function prepareDeleteData(): void
    {
        $this->clearAllLogFiles();

        // Add log messages for changes on member6
        /** @var LogMessageCreator $logCreater */
        $logCreator = $this->getContainer()->get(LogMessageCreator::class);
        /** @var MemberEntry $member1 */
        $member1 = $this->getFixtureReference('member_entry_last_name1');
        /** @var MemberEntry $member6 */
        $member6 = $this->getFixtureReference('member_entry_last_name6');
        $this->setLoggedInUserForLogging();
        $logCreator->createLogEntry('member data change', 'Member data change by member1 on member6', $member6);
        $logCreator->createLogEntry('member settings change', 'Member settings change by member1 on member6', $member6);

        // Add log messages by member6
        $this->setLoggedInUserForLogging(false, $member6);
        $logCreator->createLogEntry('login', 'Login message by member6');
        $logCreator->createLogEntry('member data change', 'Member data change by member6 on member1', $member1);
        $logCreator->createLogEntry('member settings change', 'Member settings change by member6 on member1', $member1);

        // Add an address to member6
        /** @var AddressType $addressType */
        $addressType = $this->getFixtureReference('address_type_private');
        /** @var Country $country */
        $country = $this->getFixtureReference('country_germany');
        $address = new Address();
        $address->setAddressType($addressType);
        $address->setCountry($country);
        $address->setAddress('new address');
        $address->setCity('new city');
        $address->setZip('new zip');
        $addressMap = new MemberAddressMap();
        $addressMap->setAddress($address);
        $addressMap->setMemberEntry($member6);
        $addressMap->setIsMainAddress(true);

        // Add a phone number to member6
        /** @var PhoneNumberType $phoneNumberType */
        $phoneNumberType = $this->getFixtureReference('phone_number_type_home');
        $phoneNumber = new PhoneNumber();
        $phoneNumber->setMemberEntry($member6);
        $phoneNumber->setPhoneNumber('123');
        $phoneNumber->setPhoneNumberType($phoneNumberType);
        $this->getEntityManager()->persist($phoneNumber);

        // Set member6 as signature and change member for serial letter 1
        /** @var SerialLetter $serialLetter */
        $serialLetter = $this->getFixtureReference('serial_letter1');
        $serialLetter->setFirstSignatureMember($member6);
        $serialLetter->setSecondSignatureMember($member6);
        $serialLetter->setLastUpdatedBy($member6);

        // Add a membership fee transaction to the membership number
        /** @var MembershipNumber $membership6 */
        $membership6 = $this->getFixtureReference('membership_number_5');
        $fee = new MembershipFeeTransaction();
        $fee->setAmount(0);
        $fee->setMembershipNumber($membership6);
        $this->getEntityManager()->persist($fee);

        /** @var MailingList $list1 */
        $list1 = $this->getFixtureReference('mailinglist1');
        $member6->addMailingList($list1);

        $this->getEntityManager()->flush();
        $this->setLoggedInUserForLogging();
    }


    /**
     * Create the object to be tested in this class
     *
     * @return MemberListActionsForControllers
     */
    private function getMemberListActionsForControllers(): MemberListActionsForControllers
    {
        $this->mailingListUpdaterMock = $this->createMock(MailingListUpdateInterface::class);

        return new MemberListActionsForControllers(
            $this->getEntityManager(),
            $this->getContainer()->get(SerialLetterGenerator::class),
            $this->getContainer()->get(LogMessageCreator::class),
            $this->getContainer()->get(LogEventsDisabler::class),
            $this->getContainer()->get(ManageMembershipNumberInterface::class),
            $this->mailingListUpdaterMock,
            $this->getContainer()->get(SerialEmailGenerator::class)
        );
    }
}
