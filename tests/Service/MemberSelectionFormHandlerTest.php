<?php

/*
 * Copyright (C) 2022 by Luc Hamers <vereniging@hamers.de>
 *
 * This file is part of Vereniging.
 *
 * Vereniging is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Vereniging is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Vereniging.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Tests\Service;

use App\Service\MemberSelectionFormHandler;
use App\Tests\TestCase;
use Exception;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Form\FormView;

/**
 * Class MemberSelectionFormHandlerTest
 */
class MemberSelectionFormHandlerTest extends TestCase
{
    /**
     * @inheritDoc
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->loadAllFixtures();
    }


    /**
     * Test if the form is generated correctly
     */
    public function testGenerateForm(): void
    {
        /** @var FormFactoryInterface $formFactory */
        $formFactory = $this->getContainer()->get(FormFactoryInterface::class);
        $formHandler = new MemberSelectionFormHandler($this->getEntityManager(), $formFactory);
        $form = $formHandler->generateForm();
        $this->assertNotNull($form);
        // There should be 7 form fields
        $this->assertEquals(7, $form->count());
        // Since it is not possible to access the fields in the form (all private data), convert to form view to check
        // the data fields.
        $formView = $form->createView();

        // Check the order of the entries
        $expectedLabels = [
            'status' => ['Former member', 'Future member', 'Member'],
            'membershipType' => ['Family member', 'Group member', 'Regular member', 'Student member'],
            'committee' => ['Activities', 'Board', 'Public relations'],
            'userRole' => ['Member administrator', 'Serial letter author', 'Serial letter signer', 'System administrator', 'Treasurer', 'User'],
            'email' => ['All', 'Only with email', 'Only without email', 'Only without email in group'],
            'debt' => ['All', 'Only with outstanding debt', 'Only without outstanding debt'],
            'others' => ['', ''],
        ];
        $this->checkFormFields($expectedLabels, $formView);
        // No data is preselected, so the value field should be empty
        $this->assertEquals([''], $formView->children['status']->vars['value']);

        // Now generate a form where the status member is preselected. This is found in the value variable.
        $memberStatus = $this->getFixtureReference('status_member');
        $formView = $formHandler->generateForm(true)->createView();
        $this->assertEquals([$memberStatus->getId()], $formView->children['status']->vars['value']);

        $this->checkFormFields($expectedLabels, $formView);
    }


    /**
     * Test the setOption method
     *
     * @throws Exception
     */
    public function testSetOption(): void
    {
        $formFactory = $this->getContainer()->get(FormFactoryInterface::class);
        $formHandler = new MemberSelectionFormHandler($this->getEntityManager(), $formFactory);

        try {
            $formHandler->setOption('bla', null);
            $this->fail('Expected exception is not thrown!');
        } catch (Exception $e) {
            $this->assertEquals('Trying to set key bla which does not exists!', $e->getMessage());
        }

        $formHandler->setOption('omitFields', ['debt', 'email'])
                    ->setOption('statusWithMembershipNumberOnly', true)
                    ->setOption('currentLocale', 'de');
        $form = $formHandler->generateForm();
        $formView = $form->createView();

        $expectedLabels = [
            'status' => ['Member'],
            'membershipType' => ['Family member', 'Group member', 'Regular member', 'Student member'],
            'committee' => ['Public relations', 'Activities', 'Board'],
            'userRole' => ['User', 'Member administrator', 'Treasurer', 'Serial letter author', 'Serial letter signer', 'System administrator'],
            'others' => ['', ''],
        ];
        $this->checkFormFields($expectedLabels, $formView);
    }


    /**
     * Test if the service is registered correctly in the service container
     */
    public function testServiceRegistration(): void
    {
        $this->checkContainerRegistration(MemberSelectionFormHandler::class);
    }


    /**
     * Check if the form has the correct form fields by checking the labels.
     *
     * @param array    $expectedLabels
     * @param FormView $formView
     */
    private function checkFormFields(array $expectedLabels, FormView $formView): void
    {
        $this->assertCount(count($expectedLabels), $formView->children);

        foreach ($expectedLabels as $fieldName => $labels) {
            $this->assertCount(count($labels), $formView->children[$fieldName]->children);
            $index = 0;
            foreach ($formView->children[$fieldName]->children as $child) {
                $this->assertEquals(
                    $labels[$index],
                    $child->vars['label'],
                    "Order of fields for $fieldName is wrong on position $index"
                );
                $index++;
            }
        }
    }
}
