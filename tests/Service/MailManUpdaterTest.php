<?php

/*
 * Copyright (C) 2022 by Luc Hamers <vereniging@hamers.de>
 *
 * This file is part of Vereniging.
 *
 * Vereniging is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Vereniging is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Vereniging.  If not, see <https://www.gnu.org/licenses/>.
 */

// The namespace must be identical to that of MailManUpdater.php for the function replacement of get_file_contents to
// work
namespace App\Tests\Service;

use App\Entity\MailingList;
use App\Service\MailingListUpdateInterface;
use App\Service\MailManUpdater;
use App\Tests\TestCase;

/**
 * Class MailManUpdaterTest
 */
class MailManUpdaterTest extends TestCase
{
    /** @var string|bool */
    public static $fileGetContentsReturnValue;
    public static string $fileGetContentsUrl;


    /**
     * This method is passed to the mail man object to replace the function file_get_contents. The call just stores the
     * call parameter (so it can be checked later on). It returns a value, which is previously set in a global variable.
     *
     * @param string $filename
     *
     * @return string|bool
     */
    public static function fileGetContents(string $filename)
    {
        self::$fileGetContentsUrl = $filename;

        return self::$fileGetContentsReturnValue;
    }


    /**
     * @inheritDoc
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->loadAllFixtures();
    }


    /**
     * Test adding and removing email addresses.
     */
    public function testAddRemoveEmailAddress(): void
    {
        /** @var MailingList $mailingList */
        $mailingList = $this->getFixtureReference('mailinglist1');
        // In the default fixtures, the domain address is an email, This is changed here for this test.
        $mailingList->setManagementEmail('http://lists.test.com');

        /** @var MailingListUpdateInterface $mailManUpdater */
        $mailManUpdater = new MailManUpdater($this->getEntityManager(), [MailManUpdaterTest::class, 'fileGetContents']);

        // First add an email address to the list
        self::$fileGetContentsReturnValue = 'Successfully subscribed me@mail.com';
        self::$fileGetContentsUrl = '';
        $mailManUpdater->addEmailAddress($mailingList, 'me@mail.com');
        $expected  = 'http://lists.test.com/mailman/admin/MailingList_1/members/';
        $expected .= 'add?subscribe_or_invite=0&subscribees=me@mail.com&adminpw=password1';
        $this->assertEquals($expected, self::$fileGetContentsUrl);

        // Then remove an email address from the list
        self::$fileGetContentsReturnValue = 'Successfully Unsubscribed me@mail.com';
        self::$fileGetContentsUrl = '';
        $mailManUpdater->removeEmailAddress($mailingList, 'me@mail.com');
        $expected  = 'http://lists.test.com/mailman/admin/MailingList_1/members/';
        $expected .= 'remove?send_unsub_ack_to_this_batch=0&send_unsub_notifications_to_list_owner=1&unsubscribees=me@mail.com&adminpw=password1';
        $this->assertEquals($expected, self::$fileGetContentsUrl);
    }


    /**
     * Test adding and removing email addresses, where only the error behaviour is checked.
     */
    public function testAddRemoveEmailAddressWithErrors(): void
    {
        /** @var MailingList $mailingList */
        $mailingList = $this->getFixtureReference('mailinglist1');
        // In the default fixtures, the domain address is an email, This is changed here for this test.
        $mailingList->setManagementEmail('http://lists.test.com');

        $mailManUpdater = new MailManUpdater($this->getEntityManager(), [MailManUpdaterTest::class, 'fileGetContents']);

        // Check errors when adding to the list
        try {
            self::$fileGetContentsReturnValue = false;
            $mailManUpdater->addEmailAddress($mailingList, 'me@mail.com');
            $this->fail("Expected exception didn't occur");
        } catch (\Exception $e) {
            $expected  = 'Calling the url "http://lists.test.com/mailman/admin/MailingList_1/members/';
            $expected .= 'add?subscribe_or_invite=0&subscribees=me@mail.com&adminpw=password1" didn\'t work!';
            $this->assertEquals($expected, $e->getMessage());
        }
        try {
            self::$fileGetContentsReturnValue = '';
            $mailManUpdater->addEmailAddress($mailingList, 'me@mail.com');
            $this->fail("Expected exception didn't occur");
        } catch (\Exception $e) {
            $expected  = 'Adding email address me@mail.com with command "http://lists.test.com/mailman/admin/MailingList_1/members/';
            $expected .= 'add?subscribe_or_invite=0&subscribees=me@mail.com&adminpw=password1" failed!';
            $this->assertEquals($expected, $e->getMessage());
        }

        // Check errors when removing from the list
        try {
            self::$fileGetContentsReturnValue = false;
            $mailManUpdater->removeEmailAddress($mailingList, 'me@mail.com');
            $this->fail("Expected exception didn't occur");
        } catch (\Exception $e) {
            $expected  = 'Calling the url "http://lists.test.com/mailman/admin/MailingList_1/members/';
            $expected .= 'remove?send_unsub_ack_to_this_batch=0&send_unsub_notifications_to_list_owner=1&unsubscribees=me@mail.com&adminpw=password1" didn\'t work!';
            $this->assertEquals($expected, $e->getMessage());
        }
        try {
            self::$fileGetContentsReturnValue = '';
            $mailManUpdater->removeEmailAddress($mailingList, 'me@mail.com');
            $this->fail("Expected exception didn't occur");
        } catch (\Exception $e) {
            $expected  = 'Removing email address me@mail.com with command "http://lists.test.com/mailman/admin/MailingList_1/members/';
            $expected .= 'remove?send_unsub_ack_to_this_batch=0&send_unsub_notifications_to_list_owner=1&unsubscribees=me@mail.com&adminpw=password1" failed!';
            $this->assertEquals($expected, $e->getMessage());
        }
    }


    /**
     * Test the getters
     */
    public function testGetter(): void
    {
        $mailManUpdater = new MailManUpdater($this->getEntityManager(), [MailManUpdaterTest::class, 'fileGetContents']);
        $this->assertCount(4, $mailManUpdater->getFieldNames(false));
        $this->assertEquals(['List name', 'Server domain name', 'Management URL',  'Management password'], $mailManUpdater->getFieldNames(false));
        $this->assertCount(3, $mailManUpdater->getFieldNames(true));
        $this->assertEquals(['List name', 'Server domain name', 'Management password'], $mailManUpdater->getFieldNames(true));
    }
}
