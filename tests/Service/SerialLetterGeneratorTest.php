<?php

/*
 * Copyright (C) 2022 by Luc Hamers <vereniging@hamers.de>
 *
 * This file is part of Vereniging.
 *
 * Vereniging is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Vereniging is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Vereniging.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Service;

use App\Entity\CompanyInformation;
use App\Entity\Language;
use App\Entity\MemberEntry;
use App\Entity\SerialLetter;
use App\Entity\SerialLetterContent;
use App\Service\Html2Latex;
use App\Service\LogMessageCreator;
use App\Service\SerialLetterGenerator;
use App\Tests\TestCase;
use Exception;

/**
 * Replace the php exec function by our own function to be able to define its behaviour. This will only work in our
 * current namespace (i.e. Core\SerialLetterBundle\Services). The call to this function is passed to the exec function
 * of the test class.
 *
 * @param string     $command
 * @param array|null $output
 * @param null       $returnVal
 *
 * @return mixed
 *
 * @codingStandardsIgnoreStart
 */
function exec(string $command, array &$output = null, &$returnVal = null)
{
    return SerialLetterGeneratorTest::exec($command, $output, $returnVal);
}
// @codingStandardsIgnoreEnd

//SerialLetterGeneratorTest::$execReturnValue = 0;


/**
 * Class SerialLetterGeneratorTest
 */
class SerialLetterGeneratorTest extends TestCase
{
    private Html2Latex $html2Latex;
    private LogMessageCreator $logMessageCreator;
    private static string $template;
    private static string $background;
    private string $projectDir;
    private static array $neededVariables;
    private static array $replacementVariables;
    private static string $pdfLatexBin;
    public static int $execReturnValue = 0;


    /**
     * When the php exec function is called anywhere within the current namespace, the call is redirected to this
     * method. Here the default is to pass the call to the phps real exec function. By setting the static variable
     * self::$execReturnValue to another value than 0, no call is executed and this return value is used. This can be
     * used to "mock" calls to the exec function.
     *
     * @param string     $command
     * @param array|null $output
     * @param int|null   $returnVal
     *
     * @return mixed
     */
    public static function exec(string $command, array &$output = null, ?int &$returnVal = null)
    {
        $returnString = '';
        if (0 === self::$execReturnValue) {
            $returnString = \exec($command, $output, $returnVal);
        } else {
            $returnVal = self::$execReturnValue;
        }

        return $returnString;
    }


    /**
     * Setup the test environment, i.e. load fixture data.
     */
    public static function setUpBeforeClass(): void
    {
        // Create empty template file to be used by several tests.
        self::$template = '/tmp/test.tex';
        self::$background = 'templates/SerialLetter/background.pdf';
        file_put_contents(self::$template, '');
        self::$neededVariables = ['return_address', 'background', 'template_path', 'address', 'language', 'title',
            'content', 'footer', 'supported_languages',
        ];
        self::$replacementVariables = [
            'opening', 'signers', '-',
            'name', 'firstName', 'title', 'gender', 'birthday', 'preferredLanguage', '-',
            'email', 'phoneNumbers', 'postalAddresses', 'mailingListSubscriptions', '-',
            'companyName', 'companyLocation', 'companyDescription', 'companyFunction', 'companyHomepage', '-',
            'userName', 'userRole', 'membershipNumber', 'memberSince', 'membershipEndRequested', 'userStatus', 'membershipType', 'membershipFee', 'groupMembers', 'committeeMemberships',
        ];
        self::$pdfLatexBin = "/usr/bin/pdflatex";
        self::$execReturnValue = 0;
        parent::setUpBeforeClass();
    }


    /**
     * @inheritDoc
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->loadAllFixtures();

        $this->html2Latex = $this->getContainer()->get(Html2Latex::class);
        $this->logMessageCreator = $this->getContainer()->get(LogMessageCreator::class);
        $this->projectDir = $this->getContainer()->get('kernel')->getProjectDir();
        $this->setLoggedInUserForLogging();
    }


    /**
     * This is called after each test case.
     */
    public function tearDown(): void
    {
        // Ensure that the original exec function is back in place
        self::$execReturnValue = 0;
        parent::tearDown();
    }


    /**
     * @inheritdoc
     */
    public static function tearDownAfterClass(): void
    {
        // Remove template file
        unlink(self::$template);
    }


    /**
     * Test the reaction of the constructor to wrong file paths
     */
    public function testConstructorWithWrongFilePaths(): void
    {
        $this->clearAllLogFiles();

        // Template file is empty
        try {
            new SerialLetterGenerator($this->html2Latex, $this->getEntityManager(), $this->getTranslator(), $this->logMessageCreator, [], '');
            $this->fail('Expected exception was not thrown');
        } catch (Exception $e) {
            $this->assertEquals('Latex template file must be set.', $e->getMessage());
        }

        // Template file is not found
        try {
            new SerialLetterGenerator($this->html2Latex, $this->getEntityManager(), $this->getTranslator(), $this->logMessageCreator, ['template' => 'bla'], '');
            $this->fail('Expected exception was not thrown');
        } catch (Exception $e) {
            $this->assertStringContainsString('Latex template file "/bla" does not exist', $e->getMessage());
        }

        // Background file is not found
        try {
            new SerialLetterGenerator($this->html2Latex, $this->getEntityManager(), $this->getTranslator(), $this->logMessageCreator, ['template' => self::$template, 'background' => 'bla'], '');
            $this->fail('Expected exception was not thrown');
        } catch (Exception $e) {
            $this->assertStringContainsString('Background image file "/bla" does not exist', $e->getMessage());
        }

        // Temp directory is not writable
        try {
            new SerialLetterGenerator($this->html2Latex, $this->getEntityManager(), $this->getTranslator(), $this->logMessageCreator, ['template' => self::$template, 'background' => self::$background, 'tempDirectory' => '/' ], $this->projectDir);
            $this->fail('Expected exception was not thrown');
        } catch (Exception $e) {
            $this->assertEquals('Cannot write to temp directory /.', $e->getMessage());
        }

        $logMessages = $this->getAllLogs();
        $this->assertEquals(4, count($logMessages));
        $this->assertEquals('Error', $logMessages[0]->getChangeType()->getChangeType());
        $this->assertEquals('Error', $logMessages[1]->getChangeType()->getChangeType());
        $this->assertEquals('Error', $logMessages[2]->getChangeType()->getChangeType());
        $this->assertEquals('Cannot write to temp directory /.', $logMessages[0]->getLogentry());
        $this->assertStringContainsString('Background image file "/bla" does not exist', $logMessages[1]->getLogentry());
        $this->assertEquals('Latex template file "/bla" does not exist.', $logMessages[2]->getLogentry());
        $this->assertEquals('Latex template file must be set.', $logMessages[3]->getLogentry());
    }


    /**
     * Test the reaction of the constructor to a template file in which the needed variables are missing. For this test,
     * the image file is set to the same path, since its content is not checked and it has no relevance in this test.
     *
     * @throws Exception
     */
    public function testConstructorWithMissingVariables(): void
    {
        $this->clearAllLogFiles();
        $file = self::$template;

        // Loop through all the needed variables. Start with an empty file and check the first variable. Then write the
        // first variable and check the second one, and so on.
        foreach (self::$neededVariables as $neededVariable) {
            // Template file is empty
            try {
                new SerialLetterGenerator($this->html2Latex, $this->getEntityManager(), $this->getTranslator(), $this->logMessageCreator, ['template' => $file, 'background' => self::$background, 'tempDirectory' => '/tmp', 'pdfLatexBin' => self::$pdfLatexBin], $this->projectDir);
                $this->fail('Expected exception was not thrown');
            } catch (Exception $e) {
                $this->assertEquals(
                    'Variable $'.$neededVariable.'$ is missing in LaTeX template file '.self::$template.'.',
                    $e->getMessage()
                );
            }

            file_put_contents(self::$template, '$'.$neededVariable."$\n", FILE_APPEND);
        }

        $logMessages = array_reverse($this->getAllLogs());
        $this->assertEquals(count(self::$neededVariables), count($logMessages));
        foreach (self::$neededVariables as $index => $neededVariable) {
            $this->assertEquals('Error', $logMessages[$index]->getChangeType()->getChangeType());
            $this->assertEquals('Variable $'.$neededVariable.'$ is missing in LaTeX template file /tmp/test.tex.', $logMessages[$index]->getLogentry());
        }

        // Now all variables have been written to the file, so the object should be created without an exception.
        new SerialLetterGenerator($this->html2Latex, $this->getEntityManager(), $this->getTranslator(), $this->logMessageCreator, ['template' => $file, 'background' => self::$background, 'tempDirectory' => '/tmp', 'pdfLatexBin' => self::$pdfLatexBin], $this->projectDir);
    }


    /**
     * Test if the constructor replaces all needed variables.
     *
     * @throws Exception
     */
    public function testConstructorVariableReplacements(): void
    {
        // Create empty template file
        $this->generateVariableFile();

        $letterGenerator = $this->createSerialLetterGenerator("replaced\n footer", "replaced\n return address");

        $data = $letterGenerator->getCurrentData();
        $this->assertStringContainsString("0 replaced\\\\ return address\n", $data);
        $this->assertStringContainsString("1 $this->projectDir/templates/SerialLetter/background.pdf\n", $data);
        $this->assertStringContainsString("2 /tmp\n", $data);
        $this->assertStringContainsString("3 \$address$\n", $data);
        $this->assertStringContainsString("4 \$language$\n", $data);
        $this->assertStringContainsString("5 \$title$\n", $data);
        $this->assertStringContainsString("6 \$content$\n", $data);
        $this->assertStringContainsString("7 replaced\\\\ footer\n", $data);
        $this->assertStringContainsString("8 dutch,english\n", $data);
    }


    /**
     * This tests the getNeededVariables and the getReplacementVariables methods. The intention behind this test is to
     * inform that the unit test has to be changed whenever a new variable is added to the lists in the class.
     */
    public function testGetNeededVariables(): void
    {
        $this->assertEquals(self::$neededVariables, SerialLetterGenerator::getNeededVariables(), 'New template variable was added, change test accordingly');
        $this->assertEquals(self::$replacementVariables, SerialLetterGenerator::getReplacementVariables(), 'New replacement variable was added, change test accordingly');
    }


    /**
     * Test if the check for a working pdflatex installation works correctly
     */
    public function testCheckPdfLatex(): void
    {
        $this->clearAllLogFiles();

        // This disables the call to the real php exec function and set its return value to 1 to simulate a call to a
        // general error.
        self::$execReturnValue = 1;

        try {
            $this->createSerialLetterGenerator();

            $this->fail('Expected exception did not occur!');
        } catch (Exception $e) {
            $this->assertEquals('pdflatex cannot be called; reason: general error.', $e->getMessage());
        }

        // This disables the call to the real php exec function and set its return value to 126 to simulate a call to a
        // pdflatex which cannot be executed.
        self::$execReturnValue = 126;

        try {
            $this->createSerialLetterGenerator();

            $this->fail('Expected exception did not occur!');
        } catch (Exception $e) {
            $this->assertEquals('pdflatex cannot be called; reason: cannot execute: permissions?.', $e->getMessage());
        }

        // This disables the call to the real php exec function and set its return value to 2 to simulate pdflatex
        // returning an unknown error code.
        self::$execReturnValue = 2;

        try {
            $this->createSerialLetterGenerator();

            $this->fail('Expected exception did not occur!');
        } catch (Exception $e) {
            $this->assertEquals('pdflatex cannot be called; reason: return value 2.', $e->getMessage());
        }

        // This enables the call to the real php exec function
        self::$execReturnValue = 0;

        try {
            new SerialLetterGenerator(
                $this->html2Latex,
                $this->getEntityManager(),
                $this->getTranslator(),
                $this->logMessageCreator,
                [
                    'template' => self::$template,
                    'background' => self::$background,
                    'tempDirectory' => '/tmp',
                    'pdfLatexBin' => '',
                    'footer' => "replaced\n footer",
                    'returnAddress' => "replaced\n return address",
                ],
                $this->projectDir
            );

            $this->fail('Expected exception did not occur!');
        } catch (Exception $e) {
            $this->assertEquals('pdflatex cannot be called; reason: command not found.', $e->getMessage());
        }

        $logMessages = $this->getAllLogs();
        $this->assertEquals(4, count($logMessages));
        $this->assertEquals('Error', $logMessages[0]->getChangeType()->getChangeType());
        $this->assertEquals('Error', $logMessages[1]->getChangeType()->getChangeType());
        $this->assertEquals('Error', $logMessages[2]->getChangeType()->getChangeType());
        $this->assertEquals('Error', $logMessages[3]->getChangeType()->getChangeType());
        $this->assertEquals('pdflatex cannot be called; reason: command not found.', $logMessages[0]->getLogentry());
        $this->assertEquals('pdflatex cannot be called; reason: return value 2.', $logMessages[1]->getLogentry());
        $this->assertEquals('pdflatex cannot be called; reason: cannot execute: permissions?.', $logMessages[2]->getLogentry());
        $this->assertEquals('pdflatex cannot be called; reason: general error.', $logMessages[3]->getLogentry());
    }


    /**
     * Test creating a pdf preview
     *
     * @throws Exception
     */
    public function testGeneratePdfPreview(): void
    {
        $this->generateVariableFile();

        $letterGenerator = $this->createSerialLetterGenerator();

        // Add some special chars to the title so check if there are handles correctly
        $content = $this->getFixtureReference('serial_letter_content11');
        $content->setLetterTitle($content->getLetterTitle().'€°äöüÖÄÜß&_');

        /** @var Language $languageEnglish */
        $languageEnglish = $this->getFixtureReference('language_english');
        /** @var SerialLetter $letter */
        $letter = $this->getFixtureReference('serial_letter1');
        /** @var MemberEntry $member1 */
        $member1 = $this->getFixtureReference('member_entry_last_name1');

        $letterGenerator->generatePdfPreview($letter, $languageEnglish, $member1, true);

        $data = $letterGenerator->getCurrentData();

        $this->assertStringContainsString("0 \n", $data);
        $this->assertStringContainsString("1 $this->projectDir/templates/SerialLetter/background.pdf", $data);
        $this->assertStringContainsString("2 /tmp\n", $data);
        $this->assertStringContainsString("3 ~\n", $data);
        $this->assertStringContainsString("4 english\n", $data);
        $this->assertStringContainsString('5 Serial letter1\,\euro{}$^{\circ}$äöüÖÄÜß\&\_'."\n", $data);
        $this->assertStringContainsString("6 Dear ladies and gentlemen,\nThis is a serial letter1.\n", $data);
        $this->assertStringContainsString("7 \n", $data);
        $this->assertStringContainsString("8 dutch,english\n", $data);
        $this->assertStringContainsString("9 Dear ladies and gentlemen,\n", $data);
        $this->assertStringNotContainsString("10 <span class=\"mceNonEditable\" style=\"\" data-variable=\"signers\">bla</span>\n", $data);
        $this->assertMatchesRegularExpression('#\n10\s\X*(Dipl.-Ing. Firstname1 Lastname1)#', $data);
    }


    /**
     * This method tests if the variable replacement in serial letters works correctly.
     *
     * @throws Exception
     */
    public function testVariableReplacementWithGeneratePdfPreview(): void
    {
        $this->generateVariableFile();
        $letterGenerator = $this->createSerialLetterGenerator();

        /** @var SerialLetter $letter */
        $letter = $this->getFixtureReference('serial_letter1');
        /** @var Language $language */
        $language = $this->getFixtureReference('language_dutch');
        /** @var MemberEntry $member1 */
        $member1 = $this->getFixtureReference('member_entry_last_name1');
        $content = $letter->getContent($language);

        $text = '';
        // Write all known variables to the template file
        $variableCounter = 1;
        foreach (SerialLetterGenerator::REPLACEMENT_VARIABLES as $neededVariable) {
            if ('-' !== $neededVariable) {
                $text .= "#$variableCounter <span class=\"mceNonEditable\" style=\"\" data-variable=\"$neededVariable\">bla</span> ";
                $variableCounter++;
            }
        }
        $content->setContent($text.'#end');

        $letterGenerator->generatePdfPreview($letter, $language, $member1, true);

        $this->assertStringContainsString('#1 Geachte dames en heren, #2', $letterGenerator->getCurrentData());
        $this->assertStringContainsString('#2 {\begin{tabularx}{\textwidth}{@{}XX@{}}Dipl.-Ing. Firstname1 Lastname1 & Firstname5 Lastname5 \\\\ Bestuur - Voorzitter &  \\\\ \end{tabularx}} #3', $letterGenerator->getCurrentData());
        $this->assertStringContainsString('#3 Lastname1 #4', $letterGenerator->getCurrentData());
        $this->assertStringContainsString('#4 Firstname1 #5', $letterGenerator->getCurrentData());
        $this->assertStringContainsString('#5 Dipl.-Ing. #6', $letterGenerator->getCurrentData());
        $this->assertStringContainsString('#6 Man #7', $letterGenerator->getCurrentData());
        $this->assertStringContainsString('#7 07.02.1974 #8', $letterGenerator->getCurrentData());
        $this->assertStringContainsString('#8 Engels #9', $letterGenerator->getCurrentData());
        $this->assertStringContainsString('#9 firstname1@email.com #10', $letterGenerator->getCurrentData());
        $this->assertStringContainsString('#10 \hspace{-0.5em}\begin{tabular}[t]{ l  l } Werk: & 2876543210 \\\\ Werk: & 9876543210 \\\\ Thuis: & 0123456789 \\\\ Mobiel: & 121212 \\\\ \end{tabular} #11', $letterGenerator->getCurrentData());
        $this->assertStringContainsString('#11 \hspace{-0.5em}\begin{tabular}[t]{ l  l  l  l  l } Thuisadres: & MyAddress1 & MyZip1 & MyCity1 & Duitsland \\\\ Firma-adres: & MyAddress2 & MyZip2 & MyCity2 & Nederland \\\\ \end{tabular} #12', $letterGenerator->getCurrentData());
        $this->assertStringContainsString('#12 MailingList\_1, MailingList\_3 #13', $letterGenerator->getCurrentData());
        $this->assertStringContainsString('#13 My Company #14', $letterGenerator->getCurrentData());
        $this->assertStringContainsString('#14 My city #15', $letterGenerator->getCurrentData());
        $this->assertStringContainsString('#15 My description #16', $letterGenerator->getCurrentData());
        $this->assertStringContainsString('#16 My function #17', $letterGenerator->getCurrentData());
        $this->assertStringContainsString('#17 http://company.com #18', $letterGenerator->getCurrentData());
        $this->assertStringContainsString('#18 firstname1.lastname1 #19', $letterGenerator->getCurrentData());
        $this->assertStringContainsString('#19 Systeemadministrator #20', $letterGenerator->getCurrentData());
        $this->assertStringContainsString('#20 1 #21', $letterGenerator->getCurrentData());
        $this->assertStringContainsString('#21 2016 #22', $letterGenerator->getCurrentData());
        $this->assertStringContainsString('#22 Nee #23', $letterGenerator->getCurrentData());
        $this->assertStringContainsString('#23 Lid #24', $letterGenerator->getCurrentData());
        $this->assertStringContainsString('#24 Familielid #25', $letterGenerator->getCurrentData());
        $this->assertStringContainsString('#25 36,00 #26', $letterGenerator->getCurrentData());
        $this->assertStringContainsString('#26 Firstname1 Lastname1, Firstname10 Lastname10, Firstname11 Lastname1, Firstname12 Lastname12 #27', $letterGenerator->getCurrentData());
        $this->assertStringContainsString('#27 \hspace{-0.5em}\begin{tabular}[t]{ l  l } Bestuur: & Voorzitter \\\\ Bestuur: & Vicevoorzitter \\\\ Bestuur: & Lid \\\\ Activiteiten: & Lid \\\\ Public relations: & Lid \\\\ \end{tabular} #end', $letterGenerator->getCurrentData());

        // Now check that the variable replacement also works when generating multiple letters instead of the preview
        // function.
        try {
            $letterGenerator = $this->createSerialLetterGenerator('', '', 'templates/SerialLetter/template.tex');

            /** @var MemberEntry[] $members */
            $members = $this->getEntityManager()->getRepository(MemberEntry::class)->findBy([]);
            $letterGenerator->generatePdfLetters($letter, $members, true);
        } catch (Exception $e) {
            $this->fail('Unexpected exeption '.$e->getMessage());
        }

        $this->assertStringContainsString('#19 Ledenadministrator #20', $letterGenerator->getCurrentData());
        $this->assertStringContainsString('#19 Gebruiker #20', $letterGenerator->getCurrentData());
    }


    /**
     * Test creating a pdf preview in another language to test the translations
     *
     * @throws Exception
     */
    public function testGeneratePdfPreviewWithTranslations(): void
    {
        $this->generateVariableFile();

        $letterGenerator = $this->createSerialLetterGenerator();

        /** @var SerialLetter $letter */
        $letter = $this->getFixtureReference('serial_letter1');
        /** @var Language $language */
        $language = $this->getFixtureReference('language_dutch');
        /** @var MemberEntry $member1 */
        $member1 = $this->getFixtureReference('member_entry_last_name1');

        $letterGenerator->generatePdfPreview($letter, $language, $member1, true);

        $data = $letterGenerator->getCurrentData();
        $this->assertStringContainsString("0 \n", $data);
        $this->assertStringContainsString("1 $this->projectDir/templates/SerialLetter/background.pdf", $data);
        $this->assertStringContainsString("2 /tmp\n", $data);
        $this->assertStringContainsString("3 ~\n", $data);
        $this->assertStringContainsString("4 dutch\n", $data);
        $this->assertStringContainsString('5 Seriebrief1'."\n", $data);
        $this->assertStringContainsString("6 Geachte dames en heren,\nDit is een seriebrief1.\n", $data);
        $this->assertStringContainsString("7 \n", $data);
        $this->assertStringContainsString("8 dutch,english\n", $data);
        $this->assertStringContainsString("9 Geachte dames en heren,\n", $data);
        $this->assertStringNotContainsString("10 <span class=\"mceNonEditable\" style=\"\" data-variable=\"signers\">bla</span>\n", $data);
        $this->assertMatchesRegularExpression('#\n10\s\X*(Dipl.-Ing. Firstname1 Lastname1)#', $data);
        $this->assertMatchesRegularExpression('#\n10\s\X*(Bestuur - Voorzitter)#', $data);
    }


    /**
     * Test creating a pdf preview
     *
     * @throws Exception
     */
    public function testSignatureInGeneratePdfPreview(): void
    {
        $this->generateVariableFile();

        $letterGenerator = $this->createSerialLetterGenerator();

        /** @var Language $language */
        $language = $this->getFixtureReference('language_english');
        /** @var SerialLetter $letter */
        $letter = $this->getFixtureReference('serial_letter1');
        /** @var MemberEntry $member1 */
        $member1 = $this->getFixtureReference('member_entry_last_name1');

        // Check with one signature member
        $letterGenerator->generatePdfPreview($letter, $language, $member1, true);
        $data = $letterGenerator->getCurrentData();
        $this->assertStringNotContainsString("10 <span class=\"mceNonEditable\" style=\"\" data-variable=\"signers\">bla</span>\n", $data);
        // Search after \n 10: member 1 including the titles must appear
        $this->assertMatchesRegularExpression('#\n10\s\X*(Dipl.-Ing. Firstname1 Lastname1)#', $data);

        // Check with two signature members
        $letterGenerator = $this->createSerialLetterGenerator();
        /** @var MemberEntry $member2 */
        $member2 = $this->getFixtureReference('member_entry_last_name2');
        $boardChairman = $this->getFixtureReference('committee_function_map_board_chairman');
        $boardMember = $this->getFixtureReference('committee_function_map_board_member');
        $letter->setSecondSignatureMember($member2);
        $letter->setFirstSignatureMembersFunction($boardChairman);
        $letter->setSecondSignatureMembersFunction($boardMember);
        $letterGenerator->generatePdfPreview($letter, $language, $member1, true);
        $data = $letterGenerator->getCurrentData();
        $this->assertStringNotContainsString("10 <span class=\"mceNonEditable\" style=\"\" data-variable=\"signers\">bla</span>\n", $data);
        // Search between \n 4 and \n 5: both names including the titles and the function for member1 must appear
        $this->assertMatchesRegularExpression('#\n10\s\X*(Dipl.-Ing. Firstname1 Lastname1 & Firstname2 Lastname2 M.A.)#', $data);
        $this->assertMatchesRegularExpression('#\n10\s\X*(Board - Chairman & Board - Member)#', $data);

        // Check without titles
        $letterGenerator = $this->createSerialLetterGenerator();
        $letter->setSignatureUseTitle(false);
        $letterGenerator->generatePdfPreview($letter, $language, $member1, true);
        $data = $letterGenerator->getCurrentData();
        $this->assertStringNotContainsString("10 <span class=\"mceNonEditable\" style=\"\" data-variable=\"signers\">bla</span>\n", $data);
        // Search between \n 4 and \n 5: both names without the titles must appear
        $this->assertMatchesRegularExpression('#\n10\s\X*(Firstname1 Lastname1 & Firstname2 Lastname2)#', $data);
        $this->assertStringNotContainsString('Dipl.-Ing.', $data);
        $this->assertStringNotContainsString('M.A.', $data);

        // Check without signature members
        $letterGenerator = $this->createSerialLetterGenerator();
        $letter->setFirstSignatureMember(null);
        $letter->setSecondSignatureMember(null);
        $letterGenerator->generatePdfPreview($letter, $language, $member1, true);
        $data = $letterGenerator->getCurrentData();
        $this->assertStringNotContainsString("10 <span class=\"mceNonEditable\" style=\"\" data-variable=\"signers\">bla</span>\n", $data);
        $this->assertStringNotContainsString('Firstname1 Lastname1', $data);
        $this->assertStringNotContainsString('Firstname2 Lastname2', $data);
    }


    /**
     * Test creating a pdf preview with errors. This also tests getting the pdflatex log file.
     *
     * @throws Exception
     */
    public function testGeneratePdfPreviewWithError(): void
    {
        // Create empty template file
        $this->generateVariableFile();

        $letterGenerator = $this->createSerialLetterGenerator();

        // Before calling pdflatex, a call to getLogFile should throw an exception.
        try {
            $letterGenerator->getLogFile();
            $this->fail('Expected exception was not thrown');
        } catch (Exception $e) {
            $this->assertEquals('Log file doesn\'t exist, pdflatex has not yet been called.', $e->getMessage());
        }

        $this->clearAllLogFiles();

        /** @var Language $languageEnglish */
        $languageEnglish = $this->getFixtureReference('language_english');
        /** @var SerialLetter $letter */
        $letter = $this->getFixtureReference('serial_letter1');
        /** @var MemberEntry $member1 */
        $member1 = $this->getFixtureReference('member_entry_last_name1');

        // Calling generatePdfPreview with the test file will fail.
        try {
            $letterGenerator->generatePdfPreview($letter, $languageEnglish, $member1);
            $this->fail('Expected exception was not thrown');
        } catch (Exception $e) {
            $this->assertEquals('pdflatex run failed. Call getLogFile() to get information why.', $e->getMessage());
        }

        // Log file should start with the error line.
        $logFile = file($letterGenerator->getLogFile());
        $this->assertStringContainsString('LaTeX Error: Missing \begin{document}', $logFile[0]);

        // Now try to create an error where the pdf writer cannot write to the directory. Since this is checked by the
        // constructor, we will have to remove the writing rights after construction
        $dirName = '/tmp/serialLetterTest'.rand(1, 1000);
        mkdir($dirName);
        chmod($dirName, '0755');
        $letterGenerator = $this->createSerialLetterGenerator('', '', '', $dirName);

        // Now generate a pdf preview without writing permissions to the directory
        chmod($dirName, '0555');
        // Calling generatePdfPreview with the test file will fail.
        try {
            $letterGenerator->generatePdfPreview($letter, $languageEnglish, $member1);
            $this->fail('Expected exception was not thrown');
        } catch (Exception $e) {
            $this->assertStringContainsString('failed to open stream: Permission denied', $e->getMessage());
        }
        chmod($dirName, '0755');
        rmdir($dirName);

        $logMessage = $this->getAllLogs()[0];
        $this->assertEquals('Error', $logMessage->getChangeType()->getChangeType());
        $this->assertStringContainsString('failed to open stream: Permission denied', $logMessage->getLogentry());
    }


    /**
     * Test creating a pdf preview
     */
    public function testRealPdf(): void
    {
        try {
            $letterGenerator = $this->createSerialLetterGenerator("My organisation\nGermany", "My organisation • Germany • Fax.: +49 123 456789\n www.domain.com • test@domain.com", 'templates/SerialLetter/template.tex');

            /** @var Language $language */
            $language = $this->getFixtureReference('language_english');
            /** @var SerialLetter $letter */
            $letter = $this->getFixtureReference('serial_letter1');
            /** @var MemberEntry $member1 */
            $member1 = $this->getFixtureReference('member_entry_last_name1');

            $file = $letterGenerator->generatePdfPreview($letter, $language, $member1);

            $this->assertFileExists($file);
        } catch (Exception $e) {
            $this->fail('Unexpected exeption '.$e->getMessage());
        }
    }


    /**
     * This tests generating a file with multiple serial letters for several letters. Each letter is loaded with the
     * preferred language of the member. For member1, the preferred language is German. Since there is no German letter,
     * for this member the letter in the first language (English) is loaded.
     */
    public function testGenerateSerialLetters(): void
    {
        $data = '';
        try {
            $letterGenerator = $this->createSerialLetterGenerator("My organisation\nGermany", "My organisation • Germany • Fax.: +49 123 456789\n www.domain.com • test@domain.com", 'templates/SerialLetter/template.tex');

            // To verify a bug fix: it crashed the latex run when an ampersand is part of the company name, change the
            // company name of company 2.
            /** @var CompanyInformation $company2 */
            $company2 = $this->getFixtureReference('company2');
            $company2->setName('My Company2 &amp; Company 3');

            // Read all members except member 12 (last member in the list). This is needed to test that member 12, who
            // is a member of member1 group, is omitted.
            $members = $this->getEntityManager()->getRepository(MemberEntry::class)->findBy([], [], 11);

            // To test if a missing first name (which is set to dash) is handled correctly (should be replaced by Mr.
            // or Ms.), remove FIrstname9
            $members[8]->setFirstName('-');

            /** @var SerialLetter $letter */
            $letter = $this->getFixtureReference('serial_letter1');
            $letterGenerator->generatePdfLetters($letter, $members, true);
            $data = $letterGenerator->getCurrentData();
        } catch (Exception $e) {
            $this->fail('Unexpected exeption '.$e->getMessage());
        }

        $this->assertEquals(10, substr_count($data, '\begin{letter}'));
        $this->assertEquals(8, substr_count($data, '\selectlanguage{dutch}'));
        // There is one English and one German member. Since there is no German serial letter, this member will also get
        // the serial letter in english.
        $this->assertEquals(2, substr_count($data, '\selectlanguage{english}'));
        $this->assertEquals(0, substr_count($data, '\selectlanguage{german}'));
        $this->assertEquals(8, substr_count($data, 'Seriebrief1'));
        $this->assertEquals(2, substr_count($data, 'Serial letter1'));
        $this->assertEquals(8, substr_count($data, 'Dit is een seriebrief1.'));
        $this->assertEquals(2, substr_count($data, 'This is a serial letter1.'));
        $this->assertEquals(1, substr_count($data, 'Dear Mr. Lastname1, dear Mr. Lastname10,'));
        $this->assertEquals(1, substr_count($data, 'Dear Ms. Lastname2,'));
        $this->assertEquals(1, substr_count($data, 'Geachte heer Lastname3,'));
        $this->assertEquals(8, substr_count($data, 'Met vriendelijke groeten,'));
        $this->assertEquals(2, substr_count($data, 'Kind regards,'));
        $this->assertEquals(11, substr_count($data, 'Dipl.-Ing. Firstname1 Lastname1'));
        // Member 1, 10 and 12 are in the same group and share the same address. Member 11 is also part of the group,
        // but has a different address. However, member 12 must be omitted since he is not in the selection list.
        $this->assertEquals(1, substr_count($data, 'Dipl.-Ing. Firstname1 Lastname1\\\\Firstname10 Lastname10\\\\MyAddress1\\\\MyZip1 MyCity1\\\\Germany'));
        $this->assertEquals(1, substr_count($data, 'Dhr. Lastname9\\\\MyAddress1\\\\MyZip1 MyCity1\\\\Germany'));
        $this->assertEquals(1, substr_count($data, 'My Company2 \& Company 3\\\\Firstname11 Lastname1\\\\MyAddress2\\\\MyZip2 MyCity2\\\\Netherlands'));
    }


    /**
     * Test special handling of members with:
     * - missing first name
     * - gender diverse
     * - gender none
     */
    public function testGenerateSerialLettersSpecialNaming(): void
    {
        $data = '';
        try {
            $letterGenerator = $this->createSerialLetterGenerator("My organisation\nGermany", "My organisation • Germany • Fax.: +49 123 456789\n www.domain.com • test@domain.com", 'templates/SerialLetter/template.tex');

            /** @var Language $languageGerman */
            $languageGerman = $this->getFixtureReference('language_german');

            /** @var SerialLetter $letter */
            $letter = $this->getFixtureReference('serial_letter1');

            // Add german content for some test cases
            $opening = '<span class="mceNonEditable" style="" data-variable="opening"></span>'."\n";
            $signers = '<span class="mceNonEditable" style="" data-variable="signers"></span>';
            $contentDe = new SerialLetterContent();
            $letter->addSerialLetterContent($contentDe);
            $contentDe->setLanguage($languageGerman);
            $contentDe->setContent("{$opening}Dies ist Serienbrief3\nKind regards,\n$signers");
            $contentDe->setLetterTitle('Serienbrief3');

            $members = $this->getEntityManager()->getRepository(MemberEntry::class)->findBy([], [], 7);

            // Test if a missing first name is handled correctly for male and female
            $members[0]->setFirstName('-');
            $members[0]->setGender('m');
            $members[1]->setFirstName('-');
            $members[1]->setGender('f');
            $members[2]->setFirstName('-');
            $members[2]->setGender('f');

            // Test if diverse is handled correctly (use German to test use of male/female form in "dear")
            $members[3]->setFirstName('-');
            $members[3]->setGender('d');
            $members[3]->setPreferredLanguage($languageGerman);
            $members[4]->setGender('d');
            $members[4]->setPreferredLanguage($languageGerman);

            // Test if gender none is handled correctly (use German to test use of male/female form in "dear")
            $members[5]->setFirstName('-');
            $members[5]->setGender('n');
            $members[5]->setPreferredLanguage($languageGerman);
            $members[6]->setGender('n');
            $members[6]->setPreferredLanguage($languageGerman);

            // To also check if the name is also correct in the address field, add an address to all 5 members
            foreach ($members as $member) {
                $member->addMemberAddressMap($members[0]->getMemberAddressMaps()[0]);
            }
            $letterGenerator->generatePdfLetters($letter, $members, true);
            $data = $letterGenerator->getCurrentData();
        } catch (Exception $e) {
            $this->fail('Unexpected exeption '.$e->getMessage());
        }

        $this->assertEquals(7, substr_count($data, '\begin{letter}'));
        // No first name, use Mr. (here with a title)
        $this->assertEquals(1, substr_count($data, '{Mr. Dipl.-Ing. Lastname1\\'));
        $this->assertEquals(1, substr_count($data, 'Dear Mr. Lastname1,'));
        // No first name, use Ms. (here with a title)
        $this->assertEquals(1, substr_count($data, '{Frau Lastname2 M.A.\\'));
        $this->assertEquals(1, substr_count($data, 'Sehr geehrte Frau Lastname2,'));
        // No first name, use Ms. in Dutch (here without a title)
        $this->assertEquals(1, substr_count($data, '{Mevr. Lastname3\\'));
        $this->assertEquals(1, substr_count($data, 'Geachte mevrouw Lastname3,'));
        // Diverse without first name
        $this->assertEquals(1, substr_count($data, '{Lastname4\\'));
        $this->assertEquals(1, substr_count($data, 'Sehr geehrte(r) Lastname4,'));
        // Diverse with first name (in German, to check if the universal "dear" is used)
        $this->assertEquals(1, substr_count($data, '{Firstname5 Lastname5\\'));
        $this->assertEquals(1, substr_count($data, 'Sehr geehrte(r) Firstname5 Lastname5'));
        // None without first name
        $this->assertEquals(1, substr_count($data, '{Lastname6\\'));
        $this->assertEquals(1, substr_count($data, 'Lastname6'));
        // This is true for Lastname6 and Lastname7.
        $this->assertEquals(2, substr_count($data, 'Sehr geehrte Damen und Herren,'));
        // None with first name (in German, to check if the universal "dear" is used)
        $this->assertEquals(1, substr_count($data, '{Firstname7 Lastname7\\'));
        $this->assertEquals(1, substr_count($data, 'Firstname7 Lastname7'));
    }


    /**
     * Test if the service is registered correctly in the service container
     */
    public function testServiceRegistration(): void
    {
        $this->checkContainerRegistration(SerialLetterGenerator::class);
    }


    /**
     * Create a text file which contains all variables which will be replaced by GenerateSerialLetter. The file then
     * looks as follows:
     * 0 $return_address$
     * 1 $background$
     * 2 $template_path$
     * 3 $address$
     * 4 $language$
     * 5 $title$
     * 6 $content$
     * 7 $footer$
     * 8 $supported_languages$
     * 9 <span class="mceNonEditable" style="" data-variable="opening">bla</span>
     * 10 <span class="mceNonEditable" style="" data-variable="signers">bla</span>
     */
    private function generateVariableFile(): void
    {
        // Create empty template file
        file_put_contents(self::$template, '');

        // Write all known variables to the file
        foreach (self::$neededVariables as $key => $neededVariable) {
            file_put_contents(self::$template, $key.' $'.$neededVariable."$\n", FILE_APPEND);
        }
        $counter = count(self::$neededVariables);
        file_put_contents(self::$template, "$counter <span class=\"mceNonEditable\" style=\"\" data-variable=\"opening\">bla</span>\n", FILE_APPEND);
        $counter++;
        file_put_contents(self::$template, "$counter <span class=\"mceNonEditable\" style=\"\" data-variable=\"signers\">bla</span>\n", FILE_APPEND);
    }


    /**
     * Generate a working serial letter generator
     *
     * @param string $footer
     * @param string $returnAddress
     * @param string $template
     * @param string $tempDirectory
     *
     * @return SerialLetterGenerator
     *
     * @throws Exception
     */
    private function createSerialLetterGenerator(string $footer = '', string $returnAddress = '', string $template = '', string $tempDirectory = '/tmp'): SerialLetterGenerator
    {
        return new SerialLetterGenerator(
            $this->html2Latex,
            $this->getEntityManager(),
            $this->getTranslator(),
            $this->logMessageCreator,
            [
                'template' => $template === '' ? self::$template : $template,
                'background' => self::$background,
                'tempDirectory' => $tempDirectory,
                'pdfLatexBin' => self::$pdfLatexBin,
                'footer' => $footer,
                'returnAddress' => $returnAddress,
            ],
            $this->projectDir
        );
    }
}
