<?php

/*
 * Copyright (C) 2022 by Luc Hamers <vereniging@hamers.de>
 *
 * This file is part of Vereniging.
 *
 * Vereniging is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Vereniging is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Vereniging.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Tests\Service;

use App\Service\HtmlDiffer;
use App\Tests\TestCase;

/**
 * Class HtmlDifferTest
 */
class HtmlDifferTest extends TestCase
{
    /**
     * Test if the differ finds the difference in normal text
     */
    public function testDifferWithText(): void
    {
        $differ = new HtmlDiffer();

        // plain text without diffs
        $this->assertEquals([], $differ->diff("Lorem ipsum", "Lorem ipsum"));

        // plain text with diffs
        $this->assertEquals(["Lorem ipsum", "Lorem ipsum dolor"], $differ->diff("Lorem ipsum", "Lorem ipsum dolor"));
    }


    /**
     * Test if differ finds the difference in html text
     */
    public function testDifferWithHtml(): void
    {
        $differ = new HtmlDiffer();

        // html without diffs
        $this->assertEquals([], $differ->diff("<p>Lorem ipsum</p>", "<p>Lorem ipsum</p>"));

        // Diff should be case sensitive
        $this->assertEquals(['<p>Lorem <del>ipsum</del></p>', '<p>Lorem <ins>Ipsum</ins></p>'], $differ->diff("<p>Lorem ipsum</p>", "<p>Lorem Ipsum</p>"));

        // html with diffs in one line
        $old = '<p>Lorem ipsum dolor sit amet, consectetur elit dolor ipsum.</p>';
        $new = '<p>Lorem ipsum sit amet, consectetur adipiscing elit.</p>';
        $expected = [
            "<p>Lorem ipsum <del>dolor</del> sit amet, consectetur <del>elit dolor ipsum.</del></p>",
            "<p>Lorem ipsum sit amet, consectetur <ins>adipiscing elit.</ins></p>",
        ];
        $this->assertEquals($expected, $differ->diff($old, $new));

        // html with multiple lines. There are changes in the first and third block
        $old = '<p>This is a test</p><p>Maecenas porta diam non odio mollis viverra.</p><p>ullam erat, congue in tortor vitae, dignissim rutrum dui.</p><p>Quisque placerat auctor neque non mattis.</p>';
        $new = '<p>This is the test</p><p>Maecenas porta diam non odio mollis viverra.</p><p>ullam lacus erat, congue in tortor, dignissim rutrum dui.</p><p>Quisque placerat auctor neque non mattis.</p>';
        $expected = [
            "<p>This is <del>a</del> test</p>\n<p>ullam erat, congue in tortor <del>vitae</del>, dignissim rutrum dui.</p>",
            "<p>This is <ins>the</ins> test</p>\n<p>ullam <ins>lacus</ins> erat, congue in tortor, dignissim rutrum dui.</p>",
        ];
        $this->assertEquals($expected, $differ->diff($old, $new));

        // html with multiple lines. One block is missing, one is new
        $old = '<p>Lorem ipsum dolor sit amet, consectetur elit.</p><p>Maecenas porta diam non odio mollis viverra.</p><p>ullam lacus erat, congue in tortor vitae, dignissim rutrum dui.</p>';
        $new = '<p>Lorem ipsum dolor sit amet, consectetur elit.</p><p>ullam lacus erat, congue in tortor vitae, dignissim rutrum dui.</p><p>Quisque placerat auctor neque non mattis.</p>';
        $expected = [
            "<del><p>Maecenas porta diam non odio mollis viverra.</p></del>\n-",
            "-\n<ins><p>Quisque placerat auctor neque non mattis.</p></ins>",
        ];
        $this->assertEquals($expected, $differ->diff($old, $new));

        // Check if block entities are split up into multiple lines
        $old = '<p>Lorem</p><table>ipsum</table><tr>dolor</tr><td>sit</td>';
        $new = '<p>Lorem1</p><table>ipsum1</table><tr>dolor1</tr><td>sit1</td>';
        $expected = [
            "<p><del>Lorem</del></p>\n<table><del>ipsum</del></table>\n<tr><del>dolor</del></tr>\n<td><del>sit</del></td>",
            "<p><ins>Lorem1</ins></p>\n<table><ins>ipsum1</ins></table>\n<tr><ins>dolor1</ins></tr>\n<td><ins>sit1</ins></td>",
        ];
        $this->assertEquals($expected, $differ->diff($old, $new));

        // Special test to prevent strange behaviour in inserted tables in combination with non braking spaces.
        $old = '<p>&nbsp;</p> <p>&nbsp;</p>';
        $new = '<table> <tr> <td>bla</td> </tr> </table> <p>&nbsp;</p> <p>&nbsp;</p>';
        $expected = [
            "<del><p></del><del>&nbsp;</del><del></p></del>\n-\n-\n-",
            "<ins><table> <tr> <td>bla</td></ins>\n<ins> </tr></ins>\n<ins> </table></ins>\n<ins> <p>&nbsp;</p></ins>",
        ];
        $this->assertEquals($expected, $differ->diff($old, $new));
    }


    /**
     * Test if the service is registered correctly in the service container
     */
    public function testServiceRegistration(): void
    {
        $this->checkContainerRegistration(HtmlDiffer::class);
    }
}
