<?php

/*
 * Copyright (C) 2022 by Luc Hamers <vereniging@hamers.de>
 *
 * This file is part of Vereniging.
 *
 * Vereniging is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Vereniging is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Vereniging.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Tests\Service;

use App\Service\LogEventsDisabler;
use App\Tests\TestCase;

/**
 * Class LogEventsDisablerTest
 */
class LogEventsDisablerTest extends TestCase
{
    /**
     * Test complete log events disabler class
     */
    public function testLogEventsDisabler(): void
    {
        $disabler = new LogEventsDisabler();

        $this->assertFalse($disabler->lifeCycleEventsAreDisabled());
        $disabler->disableLifecycleEvents();
        $this->assertTrue($disabler->lifeCycleEventsAreDisabled());
        $disabler->enableLifecycleEvents();
        $this->assertFalse($disabler->lifeCycleEventsAreDisabled());
    }


    /**
     * Test if the service is registered correctly in the service container
     */
    public function testServiceRegistration(): void
    {
        $this->checkContainerRegistration(LogEventsDisabler::class);
    }
}
