<?php

/*
 * Copyright (C) 2022 by Luc Hamers <vereniging@hamers.de>
 *
 * This file is part of Vereniging.
 *
 * Vereniging is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Vereniging is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Vereniging.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Tests\Service;

use App\Entity\EmailTemplate;
use App\Entity\Language;
use App\Entity\MemberEntry;
use App\Entity\SerialLetter;
use App\Service\LogMessageCreator;
use App\Service\SerialEmailGenerator;
use App\Service\SerialLetterGenerator;
use App\Tests\TestCase;
use Symfony\Component\Mailer\Exception\TransportException;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Mailer\MailerInterface;

/**
 *
 */
class SerialEmailGeneratorTest extends TestCase
{
    /**
     * @inheritDoc
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->loadAllFixtures();
        $this->setLoggedInUserForLogging();
    }


    /**
     * This is called after each test case.
     */
    public function tearDown(): void
    {
        parent::tearDown();
    }

    /**
     * Test sending serial emails
     *
     * @throws TransportExceptionInterface
     */
    public function testSendEmails(): void
    {
        $members = $this->getEntityManager()->getRepository(MemberEntry::class)->findAll();

        // Test the special handling of group members:
        // * if a group member has no email address or the same address as other members, only one email for them is
        //   sent.
        // * if a group member has its own email address, this one is not part of the groups email
        $members[10]->setEmail(null);
        $members[11]->setEmail('firstname1@email.com');

        // To test if a missing first name (which is set to dash) is handled correctly (should be replaced by Mr.
        // or Ms.), remove Firstname9
        $members[8]->setFirstName('-');

        /** @var SerialLetter $letter */
        $letter = $this->getFixtureReference('serial_letter1');
        // We are not really sending emails here, just checking their content. So we use a mock.
        $symfonyMailer = $this->createMock(MailerInterface::class);
        $symfonyMailer->expects($this->exactly(9))
                      ->method('send');
        $logMessageCreatorMock = $this->createMock(LogMessageCreator::class);

        // Special test to see how UTF8 and html entities are handled in the subject
        ($letter->getContent('en'))
                ->setLetterTitle('Title with special chars öäß&');

        $emailTemplate = new EmailTemplate();
        $emailTemplate->setContent('<p>Lorem</p>'.EmailTemplate::HTML_CONTENT_VARIABLE.'<p>ipsum</p>');
        $emailTemplate->addNewTSenderAddressTranslation('en', 'test-en@email.com');
        $emailTemplate->addNewTSenderAddressTranslation('nl', 'test-nl@email.com');
        $sendGenerator = new SerialEmailGenerator($this->getEntityManager(), $symfonyMailer, $this->getTranslator(), $logMessageCreatorMock, 'vereninging@example.com');
        $sendEmails = $sendGenerator->sendSerialEmails($letter, $members, $emailTemplate);

        // Remove the second member, since it doesn's have an email address and therefor it will not receive an email
        array_splice($members, 1, 1);

        // Since there are some special case to test, we first create the default content and then change the special
        // content after this block
        $signers = '<table><tr><td>Dipl.-Ing. Firstname1 Lastname1</td><td>Firstname5 Lastname5</td></tr><tr><td>Bestuur - Voorzitter</td><td></td></tr></table><p>ipsum</p>';
        $expectedEmails = [];
        foreach ($members as $member) {
            $expected['from'] = 'test-nl@email.com';
            $expected['to'] = $member->getEmail();
            $expected['body'] = "<p>Lorem</p>Geachte heer {$member->getName()}, \nDit is een seriebrief1.\nMet vriendelijke groeten,\n$signers";
            $expected['subject'] = 'Seriebrief1';
            $expectedEmails[] = $expected;
        }

        // Remove member 11 and 12, since they will be in the group email for member 1
        array_splice($expectedEmails, 9);
        $expectedEmails[0]['from'] = 'test-en@email.com';
        // Member 8 is female
        $expectedEmails[6]['body'] = "<p>Lorem</p>Geachte mevrouw Lastname8, \nDit is een seriebrief1.\nMet vriendelijke groeten,\n$signers";
        $signers = '<table><tr><td>Dipl.-Ing. Firstname1 Lastname1</td><td>Firstname5 Lastname5</td></tr><tr><td>Board - Chairman</td><td></td></tr></table><p>ipsum</p>';
        // Member 1 has two more members in the group (actually 3, bis Firstname10 has his own email address)
        $expectedEmails[0]['body'] = "<p>Lorem</p>Dear Mr. Lastname1, dear Mr. Lastname1, dear Mr. Lastname12, \nThis is a serial letter1.\nKind regards,\n$signers";
        // Member 1 has a special subject
        $expectedEmails[0]['subject'] = 'Title with special chars öäß&';

        // Now check the content of the emails
        foreach ($expectedEmails as $key => $expectedEmail) {
            $email = $sendEmails[$key];
            $this->assertCount(1, $email->getTo(), "To address count is wrong in email $key");
            $this->assertEquals($expectedEmail['to'], $email->getTo()[0]->getAddress(), "To address is wrong in email $key");
            $this->assertCount(1, $email->getFrom(), "From address count is wrong in email $key");
            $this->assertEquals($expectedEmail['from'], $email->getFrom()[0]->getAddress(), "From address is wrong in email $key");
            $this->assertEquals($expectedEmail['body'], $email->getHtmlBody(), "Body is wrong in email $key");
            $this->assertEquals($expectedEmail['subject'], $email->getSubject(), "Subject is wrong in email $key");
        }
    }


    /**
     * Test the error handling when email sending fails
     *
     * @return void
     *
     * @throws TransportExceptionInterface
     */
    public function testSendWithError(): void
    {
        /** @var MemberEntry $member1 */
        $member1 = $this->getFixtureReference('member_entry_last_name1');
        /** @var SerialLetter $letter */
        $letter = $this->getFixtureReference('serial_letter1');

        // CHeck if a transport exception is handled correctly: it must write a log message and rethrow
        $symfonyMailer = $this->createMock(MailerInterface::class);
        $symfonyMailer->method('send')->willThrowException(new TransportException('Bad smtp'));

        $logMessageCreator = $this->getContainer()->get(LogMessageCreator::class);
        $this->clearAllLogFiles();

        $sendGenerator = new SerialEmailGenerator($this->getEntityManager(), $symfonyMailer, $this->getTranslator(), $logMessageCreator, 'vereninging@example.com');

        $emailTemplate = new EmailTemplate();
        $emailTemplate->setSenderAddress('test@email.com');

        try {
            $sendGenerator->sendSerialEmails($letter, [$member1], $emailTemplate);
            $this->fail('Expected TransportException was not thrown!');
        } catch (TransportException $e) {
            $this->assertEquals('Bad smtp', $e->getMessage());
        }

        $logMessages = $this->getAllLogs();
        $this->assertCount(1, $logMessages);
        $this->assertEquals("Sending serial email failed with the following error: Bad smtp", $logMessages[0]->getLogentry());
    }


    /**
     * Test if variables are replaced in serial letters
     *
     * @return void
     *
     * @throws TransportExceptionInterface
     */
    public function testVariableReplacement(): void
    {
        /** @var MemberEntry $member1 */
        $member1 = $this->getFixtureReference('member_entry_last_name1');
        /** @var Language $language */
        $language = $this->getFixtureReference('language_dutch');
        $member1->setPreferredLanguage($language);


        /** @var SerialLetter $letter */
        $letter = $this->getFixtureReference('serial_letter1');
        // We are not really sending emails here, just checking their content. So we use a mock.
        $symfonyMailer = $this->createMock(MailerInterface::class);
        $symfonyMailer->expects($this->once())
            ->method('send');
        $logMessageCreatorMock = $this->createMock(LogMessageCreator::class);

        $template = '';
        // Write all known variables to the template file
        $variableCounter = 0;
        foreach (SerialLetterGenerator::REPLACEMENT_VARIABLES as $neededVariable) {
            if ('-' !== $neededVariable) {
                $template .= "<p>#$variableCounter <span class=\"mceNonEditable\" style=\"\" data-variable=\"$neededVariable\">bla</span> </p>\n";
                $variableCounter++;
            }
        }
        $template .= "#end\n";
        $template .= EmailTemplate::HTML_CONTENT_VARIABLE;

        $emailTemplate = new EmailTemplate();
        $emailTemplate->setContent($template);
        $emailTemplate->addNewTSenderAddressTranslation('nl', 'test@email.com');

        $sendGenerator = new SerialEmailGenerator($this->getEntityManager(), $symfonyMailer, $this->getTranslator(), $logMessageCreatorMock, 'vereninging@example.com');
        $sendEmails = $sendGenerator->sendSerialEmails($letter, [$member1], $emailTemplate);

        $emailBody = $sendEmails[0]->getHtmlBody();
        $this->assertStringContainsString("<p>#0 Geachte heer Lastname1,  </p>\n<p>#1", $emailBody);
        $this->assertStringContainsString("<p>#1 <table><tr><td>Dipl.-Ing. Firstname1 Lastname1</td><td>Firstname5 Lastname5</td></tr><tr><td>Bestuur - Voorzitter</td><td></td></tr></table> </p>\n<p>#2", $emailBody);
        $this->assertStringContainsString("<p>#2 Lastname1 </p>\n<p>#3", $emailBody);
        $this->assertStringContainsString("<p>#3 Firstname1 </p>\n<p>#4", $emailBody);
        $this->assertStringContainsString("<p>#4 Dipl.-Ing. </p>\n<p>#5", $emailBody);
        $this->assertStringContainsString("<p>#5 Man </p>\n<p>#6", $emailBody);
        $this->assertStringContainsString("<p>#6 07.02.1974 </p>\n<p>#7", $emailBody);
        $this->assertStringContainsString("<p>#7 Nederlands </p>\n<p>#8", $emailBody);
        $this->assertStringContainsString("<p>#8 firstname1@email.com </p>\n<p>#9", $emailBody);
        $this->assertStringContainsString("<p>#9 <table><tr><td>Werk:</td><td>2876543210</td></tr><tr><td>Werk:</td><td>9876543210</td></tr><tr><td>Thuis:</td><td>0123456789</td></tr><tr><td>Mobiel:</td><td>121212</td></tr></table> </p>\n<p>#10", $emailBody);
        $this->assertStringContainsString("<p>#10 <table><tr><td>Thuisadres:</td><td>MyAddress1</td><td>MyZip1</td><td>MyCity1</td><td>Duitsland</td></tr><tr><td>Firma-adres:</td><td>MyAddress2</td><td>MyZip2</td><td>MyCity2</td><td>Nederland</td></tr></table> </p>\n<p>#11", $emailBody);
        $this->assertStringContainsString("<p>#11 MailingList&lowbar;1, MailingList&lowbar;3 </p>\n<p>#12", $emailBody);
        $this->assertStringContainsString("<p>#12 My Company </p>\n<p>#13", $emailBody);
        $this->assertStringContainsString("<p>#13 My city </p>\n<p>#14", $emailBody);
        $this->assertStringContainsString("<p>#14 My description </p>\n<p>#15", $emailBody);
        $this->assertStringContainsString("<p>#15 My function </p>\n<p>#16", $emailBody);
        $this->assertStringContainsString("<p>#16 http://company.com </p>\n<p>#17", $emailBody);
        $this->assertStringContainsString("<p>#17 firstname1.lastname1 </p>\n<p>#18", $emailBody);
        $this->assertStringContainsString("<p>#18 Systeemadministrator </p>\n<p>#19", $emailBody);
        $this->assertStringContainsString("<p>#19 1 </p>\n<p>#20", $emailBody);
        $this->assertStringContainsString("<p>#20 2016 </p>\n<p>#21", $emailBody);
        $this->assertStringContainsString("<p>#21 Nee </p>\n<p>#22", $emailBody);
        $this->assertStringContainsString("<p>#22 Lid </p>\n<p>#23", $emailBody);
        $this->assertStringContainsString("<p>#23 Familielid </p>\n<p>#24", $emailBody);
        $this->assertStringContainsString("<p>#24 36,00 </p>\n<p>#25", $emailBody);
        $this->assertStringContainsString("<p>#25 Firstname1 Lastname1, Firstname10 Lastname10, Firstname11 Lastname1, Firstname12 Lastname12 </p>\n<p>#26", $emailBody);
        $this->assertStringContainsString("<p>#26 <table><tr><td>Bestuur:</td><td>Voorzitter</td></tr><tr><td>Bestuur:</td><td>Vicevoorzitter</td></tr><tr><td>Bestuur:</td><td>Lid</td></tr><tr><td>Activiteiten:</td><td>Lid</td></tr><tr><td>Public relations:</td><td>Lid</td></tr></table> </p>\n#end", $emailBody);
    }
}
