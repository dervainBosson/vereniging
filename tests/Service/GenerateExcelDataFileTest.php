<?php

/*
 * Copyright (C) 2022 by Luc Hamers <vereniging@hamers.de>
 *
 * This file is part of Vereniging.
 *
 * Vereniging is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Vereniging is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Vereniging.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Tests\Service;

use App\Service\ExcelDataFormats;
use App\Service\GenerateExcelDataFile;
use App\Tests\TestCase;
use PhpOffice\PhpSpreadsheet\Exception;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

/**
 * Test service class generate excel data file.
 *
 * Class GenerateExcelDataFileTest
 */
class GenerateExcelDataFileTest extends TestCase
{
    /**
     * @inheritDoc
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->loadAllFixtures();
    }


    /**
     * Test the create Excel file method
     */
    public function testCreateExcelFileWithoutLoggedInUser(): void
    {
        $testObject = new GenerateExcelDataFile($this->getEntityManager());

        try {
            $testObject->createExcelFile([]);
        } catch (\Exception $e) {
            $this->assertEquals('Logged in user is not set!', $e->getMessage());

            return;
        }

        $this->fail('Expected exception not thrown');
    }


    /**
     * Test the create Excel file method
     *
     * @throws Exception
     */
    public function testCreateExcelFile(): void
    {
        $members[] = $this->getFixtureReference('member_entry_last_name1');

        $testObject = new GenerateExcelDataFile($this->getEntityManager());
        $testObject->setLoggedInUser($members[0]);

        $excelFile = null;
        try {
            $excelFile = $testObject->createExcelFile($members);
        } catch (\Exception $e) {
            echo $e->getMessage();
        }

        $writer = IOFactory::createWriter($excelFile, "Xlsx");
        $writer->save("/tmp/test.xlsx");

        // Check document properties
        $this->assertEquals($members[0]->getCompleteName(), $excelFile->getProperties()->getCreator());
        $this->assertEquals($members[0]->getCompleteName(), $excelFile->getProperties()->getLastModifiedBy());
        $this->assertEquals("Vereniging data export", $excelFile->getProperties()->getTitle());
        $this->assertEquals("Vereniging data export", $excelFile->getProperties()->getDescription());
        $this->assertEquals("Vereniging data export", $excelFile->getProperties()->getSubject());

        $sheet = $excelFile->getActiveSheet();

        // Check static headers
        $this->checkStaticHeaders($sheet);

        // Check company information
        $column = count(ExcelDataFormats::STATIC_HEADERS);
        $headerInfo = ExcelDataFormats::DYNAMIC_HEADERS['companyInformation'];
        $column = $this->checkDynamicHeaders(1, $headerInfo, $sheet, $column, true);

        // Phone numbers
        $headerInfo = ExcelDataFormats::DYNAMIC_HEADERS['phoneNumbers'];
        $column = $this->checkDynamicHeaders(4, $headerInfo, $sheet, $column);

        // Addresses
        $headerInfo = ExcelDataFormats::DYNAMIC_HEADERS['addresses'];
        $column = $this->checkDynamicHeaders(2, $headerInfo, $sheet, $column);

        // Committee functions
        $headerInfo = ExcelDataFormats::DYNAMIC_HEADERS['committees'];
        $column = $this->checkDynamicHeaders(5, $headerInfo, $sheet, $column);

        // Group members
        $headerInfo = ExcelDataFormats::DYNAMIC_HEADERS['groupMembers'];
        $this->checkDynamicHeaders(3, $headerInfo, $sheet, $column);

        unlink("/tmp/test.xlsx");
    }


    /**
     * Test the create Excel file method with a member without connected fields
     *
     * @throws Exception
     */
    public function testCreateExcelFileWithoutConnectedFields(): void
    {
        $members[] = $this->getFixtureReference('member_entry_last_name3');

        $testObject = new GenerateExcelDataFile($this->getEntityManager());
        $testObject->setLoggedInUser($members[0]);

        $excelFile = $testObject->createExcelFile($members);
        $sheet = $excelFile->getActiveSheet();

        // Check static headers
        $this->checkStaticHeaders($sheet);

        $this->assertEquals('', $sheet->getCell('S1')->getValue());
    }


    /**
     * Test if the member data is written correctly
     *
     * @throws Exception
     */
    public function testCreateExcelFileWithMemberData(): void
    {
        $members[] = $this->getFixtureReference('member_entry_last_name1');
        $members[] = $this->getFixtureReference('member_entry_last_name2');
        $members[] = $this->getFixtureReference('member_entry_last_name3');

        $testObject = new GenerateExcelDataFile($this->getEntityManager());
        $testObject->setLoggedInUser($members[0]);

        $excelFile = $testObject->createExcelFile($members);
        $sheet = $excelFile->getActiveSheet();

        // Check static headers
        $this->checkStaticHeaders($sheet);

//        $writer = IOFactory::createWriter($excelFile, "Xlsx");
//        $writer->save("/tmp/test.xlsx");

        // Last name
        $this->assertEquals('Lastname1', $sheet->getCell('A3')->getValue());
        $this->assertEquals('Lastname2', $sheet->getCell('A4')->getValue());
        $this->assertEquals('Lastname3', $sheet->getCell('A5')->getValue());

        // First name
        $this->assertEquals('Firstname1', $sheet->getCell('B3')->getValue());
        $this->assertEquals('Firstname2', $sheet->getCell('B4')->getValue());
        $this->assertEquals('Firstname3', $sheet->getCell('B5')->getValue());

        // Gender
        $this->assertEquals('m', $sheet->getCell('C3')->getValue());
        $this->assertEquals('f', $sheet->getCell('C4')->getValue());
        $this->assertEquals('m', $sheet->getCell('C5')->getValue());

        // Title
        $this->assertEquals('Dipl.-Ing.', $sheet->getCell('D3')->getValue());
        $this->assertEquals('M.A.', $sheet->getCell('D4')->getValue());
        $this->assertEquals(null, $sheet->getCell('D5')->getValue());

        // Birth date
        $this->assertEquals('1974-02-07 00:00:00', $sheet->getCell('E3')->getValue());
        $this->assertEquals(null, $sheet->getCell('E4')->getValue());
        $this->assertEquals(null, $sheet->getCell('E5')->getValue());

        // Preferred language
        $this->assertEquals('English', $sheet->getCell('F3')->getValue());
        $this->assertEquals('German', $sheet->getCell('F4')->getValue());
        $this->assertEquals('Dutch', $sheet->getCell('F5')->getValue());

        // Email
        $this->assertEquals('firstname1@email.com', $sheet->getCell('G3')->getValue());
        $this->assertEquals(null, $sheet->getCell('G4')->getValue());
        $this->assertEquals('firstname3@email.com', $sheet->getCell('G5')->getValue());

        // Membership number
        $this->assertEquals(1, $sheet->getCell('H3')->getValue());
        $this->assertEquals(2, $sheet->getCell('H4')->getValue());
        $this->assertEquals(3, $sheet->getCell('H5')->getValue());

        // Use direct debit
        $this->assertEquals(1, $sheet->getCell('I3')->getValue());
        $this->assertEquals(0, $sheet->getCell('I4')->getValue());
        $this->assertEquals(0, $sheet->getCell('I5')->getValue());

        // Membership end requested
        $this->assertEquals(0, $sheet->getCell('J3')->getValue());
        $this->assertEquals(0, $sheet->getCell('J4')->getValue());
        $this->assertEquals(1, $sheet->getCell('J5')->getValue());

        // Member since
        $this->assertEquals(2016, $sheet->getCell('K3')->getValue());
        $this->assertEquals(2015, $sheet->getCell('K4')->getValue());
        $this->assertEquals(2015, $sheet->getCell('K5')->getValue());

        // User status
        $this->assertEquals('Member', $sheet->getCell('L3')->getValue());
        $this->assertEquals('Member', $sheet->getCell('L4')->getValue());
        $this->assertEquals('Member', $sheet->getCell('L5')->getValue());

        // Membership type
        $this->assertEquals('Family member', $sheet->getCell('M3')->getValue());
        $this->assertEquals('Regular member', $sheet->getCell('M4')->getValue());
        $this->assertEquals('Regular member', $sheet->getCell('M5')->getValue());

        // User role
        $this->assertEquals('System administrator', $sheet->getCell('N3')->getValue());
        $this->assertEquals('User', $sheet->getCell('N4')->getValue());
        $this->assertEquals('Member administrator', $sheet->getCell('N5')->getValue());

        // Company name
        $this->assertEquals('My Company', $sheet->getCell('O3')->getValue());
        $this->assertEquals(null, $sheet->getCell('O4')->getValue());
        $this->assertEquals(null, $sheet->getCell('O5')->getValue());

        // Company city
        $this->assertEquals('My city', $sheet->getCell('P3')->getValue());
        $this->assertEquals(null, $sheet->getCell('P4')->getValue());
        $this->assertEquals(null, $sheet->getCell('P5')->getValue());

        // Company description
        $this->assertEquals('My description', $sheet->getCell('Q3')->getValue());
        $this->assertEquals(null, $sheet->getCell('Q4')->getValue());
        $this->assertEquals(null, $sheet->getCell('Q5')->getValue());

        // Company function
        $this->assertEquals('My function', $sheet->getCell('R3')->getValue());
        $this->assertEquals(null, $sheet->getCell('R4')->getValue());
        $this->assertEquals(null, $sheet->getCell('R5')->getValue());

        // Company URL
        $this->assertEquals('http://company.com', $sheet->getCell('S3')->getValue());
        $this->assertEquals(null, $sheet->getCell('S4')->getValue());
        $this->assertEquals(null, $sheet->getCell('S5')->getValue());

        // Phone number 1
        $this->assertEquals('Home', $sheet->getCell('T3')->getValue());
        $this->assertEquals('Company', $sheet->getCell('T4')->getValue());
        $this->assertEquals(null, $sheet->getCell('T5')->getValue());

        $this->assertEquals('0123456789', $sheet->getCell('U3')->getValue());
        $this->assertEquals('2876543210', $sheet->getCell('U4')->getValue());
        $this->assertEquals(null, $sheet->getCell('U5')->getValue());

        // Phone number 2
        $this->assertEquals('Company', $sheet->getCell('V3')->getValue());
        $this->assertEquals(null, $sheet->getCell('V4')->getValue());
        $this->assertEquals(null, $sheet->getCell('V5')->getValue());

        $this->assertEquals('9876543210', $sheet->getCell('W3')->getValue());
        $this->assertEquals(null, $sheet->getCell('W4')->getValue());
        $this->assertEquals(null, $sheet->getCell('W5')->getValue());

        // Phone number 3
        $this->assertEquals('Mobile', $sheet->getCell('X3')->getValue());
        $this->assertEquals(null, $sheet->getCell('X4')->getValue());
        $this->assertEquals(null, $sheet->getCell('X5')->getValue());

        $this->assertEquals('121212', $sheet->getCell('Y3')->getValue());
        $this->assertEquals(null, $sheet->getCell('Y4')->getValue());
        $this->assertEquals(null, $sheet->getCell('Y5')->getValue());

        // Phone number 4
        $this->assertEquals('Company', $sheet->getCell('Z3')->getValue());
        $this->assertEquals(null, $sheet->getCell('Z4')->getValue());
        $this->assertEquals(null, $sheet->getCell('Z5')->getValue());

        $this->assertEquals('2876543210', $sheet->getCell('AA3')->getValue());
        $this->assertEquals(null, $sheet->getCell('AA4')->getValue());
        $this->assertEquals(null, $sheet->getCell('AA5')->getValue());

        // Address 1
        $this->assertEquals('yes', $sheet->getCell('AB3')->getValue());
        $this->assertEquals(null, $sheet->getCell('AB4')->getValue());
        $this->assertEquals(null, $sheet->getCell('AB5')->getValue());

        $this->assertEquals('Home address', $sheet->getCell('AC3')->getValue());
        $this->assertEquals(null, $sheet->getCell('AC4')->getValue());
        $this->assertEquals(null, $sheet->getCell('AC5')->getValue());

        $this->assertEquals('MyAddress1', $sheet->getCell('AD3')->getValue());
        $this->assertEquals(null, $sheet->getCell('AD4')->getValue());
        $this->assertEquals(null, $sheet->getCell('AD5')->getValue());

        $this->assertEquals('MyZip1', $sheet->getCell('AE3')->getValue());
        $this->assertEquals(null, $sheet->getCell('AE4')->getValue());
        $this->assertEquals(null, $sheet->getCell('AE5')->getValue());

        $this->assertEquals('MyCity1', $sheet->getCell('AF3')->getValue());
        $this->assertEquals(null, $sheet->getCell('AF4')->getValue());
        $this->assertEquals(null, $sheet->getCell('AF5')->getValue());

        $this->assertEquals('Germany', $sheet->getCell('AG3')->getValue());
        $this->assertEquals(null, $sheet->getCell('AG4')->getValue());
        $this->assertEquals(null, $sheet->getCell('AG5')->getValue());

        // Address 2
        $this->assertEquals('no', $sheet->getCell('AH3')->getValue());
        $this->assertEquals(null, $sheet->getCell('AH4')->getValue());
        $this->assertEquals(null, $sheet->getCell('AH5')->getValue());

        $this->assertEquals('Company address', $sheet->getCell('AI3')->getValue());
        $this->assertEquals(null, $sheet->getCell('AI4')->getValue());
        $this->assertEquals(null, $sheet->getCell('AI5')->getValue());

        $this->assertEquals('MyAddress2', $sheet->getCell('AJ3')->getValue());
        $this->assertEquals(null, $sheet->getCell('AJ4')->getValue());
        $this->assertEquals(null, $sheet->getCell('AJ5')->getValue());

        $this->assertEquals('MyZip2', $sheet->getCell('AK3')->getValue());
        $this->assertEquals(null, $sheet->getCell('AK4')->getValue());
        $this->assertEquals(null, $sheet->getCell('AK5')->getValue());

        $this->assertEquals('MyCity2', $sheet->getCell('AL3')->getValue());
        $this->assertEquals(null, $sheet->getCell('AL4')->getValue());
        $this->assertEquals(null, $sheet->getCell('AL5')->getValue());

        $this->assertEquals('Netherlands', $sheet->getCell('AM3')->getValue());
        $this->assertEquals(null, $sheet->getCell('AM4')->getValue());
        $this->assertEquals(null, $sheet->getCell('AM5')->getValue());

        // Committee function 1
        $this->assertEquals('Board', $sheet->getCell('AN3')->getValue());
        $this->assertEquals('Activities', $sheet->getCell('AN4')->getValue());
        $this->assertEquals(null, $sheet->getCell('AN5')->getValue());

        $this->assertEquals('Chairman', $sheet->getCell('AO3')->getValue());
        $this->assertEquals('Chairman', $sheet->getCell('AO4')->getValue());
        $this->assertEquals(null, $sheet->getCell('AO5')->getValue());

        // Committee function 2
        $this->assertEquals('Board', $sheet->getCell('AP3')->getValue());
        $this->assertEquals('Public relations', $sheet->getCell('AP4')->getValue());
        $this->assertEquals(null, $sheet->getCell('AP5')->getValue());

        $this->assertEquals('Vice chairman', $sheet->getCell('AQ3')->getValue());
        $this->assertEquals('Member', $sheet->getCell('AQ4')->getValue());
        $this->assertEquals(null, $sheet->getCell('AQ5')->getValue());

        // Committee function 3
        $this->assertEquals('Board', $sheet->getCell('AR3')->getValue());
        $this->assertEquals(null, $sheet->getCell('AR4')->getValue());
        $this->assertEquals(null, $sheet->getCell('AR5')->getValue());

        $this->assertEquals('Member', $sheet->getCell('AS3')->getValue());
        $this->assertEquals(null, $sheet->getCell('AS4')->getValue());
        $this->assertEquals(null, $sheet->getCell('AS5')->getValue());

        // Committee function 5
        $this->assertEquals('Activities', $sheet->getCell('AT3')->getValue());
        $this->assertEquals(null, $sheet->getCell('AT4')->getValue());
        $this->assertEquals(null, $sheet->getCell('AT5')->getValue());

        $this->assertEquals('Member', $sheet->getCell('AU3')->getValue());
        $this->assertEquals(null, $sheet->getCell('AU4')->getValue());
        $this->assertEquals(null, $sheet->getCell('AU5')->getValue());

        // Committee function 5
        $this->assertEquals('Public relations', $sheet->getCell('AV3')->getValue());
        $this->assertEquals(null, $sheet->getCell('AV4')->getValue());
        $this->assertEquals(null, $sheet->getCell('AV5')->getValue());

        $this->assertEquals('Member', $sheet->getCell('AW3')->getValue());
        $this->assertEquals(null, $sheet->getCell('AW4')->getValue());
        $this->assertEquals(null, $sheet->getCell('AW5')->getValue());

        $this->assertEquals('Lastname10', $sheet->getCell('AX3')->getValue());
        $this->assertEquals(null, $sheet->getCell('AX4')->getValue());
        $this->assertEquals(null, $sheet->getCell('AX5')->getValue());

        $this->assertEquals('Firstname10', $sheet->getCell('AY3')->getValue());
        $this->assertEquals(null, $sheet->getCell('AY4')->getValue());
        $this->assertEquals(null, $sheet->getCell('AY5')->getValue());

        $this->assertEquals('firstname10@email.com', $sheet->getCell('AZ3')->getValue());
        $this->assertEquals(null, $sheet->getCell('AZ4')->getValue());
        $this->assertEquals(null, $sheet->getCell('AZ5')->getValue());

        $this->assertEquals('Lastname1', $sheet->getCell('BA3')->getValue());
        $this->assertEquals(null, $sheet->getCell('BA4')->getValue());
        $this->assertEquals(null, $sheet->getCell('BA5')->getValue());

        $this->assertEquals('Firstname11', $sheet->getCell('BB3')->getValue());
        $this->assertEquals(null, $sheet->getCell('BB4')->getValue());
        $this->assertEquals(null, $sheet->getCell('BB5')->getValue());

        $this->assertEquals('firstname11@email.com', $sheet->getCell('BC3')->getValue());
        $this->assertEquals(null, $sheet->getCell('BC4')->getValue());
        $this->assertEquals(null, $sheet->getCell('BC5')->getValue());

        $this->assertEquals('Lastname12', $sheet->getCell('BD3')->getValue());
        $this->assertEquals(null, $sheet->getCell('BD4')->getValue());
        $this->assertEquals(null, $sheet->getCell('BD5')->getValue());

        $this->assertEquals('Firstname12', $sheet->getCell('BE3')->getValue());
        $this->assertEquals(null, $sheet->getCell('BE4')->getValue());
        $this->assertEquals(null, $sheet->getCell('BE5')->getValue());

        $this->assertEquals('firstname12@email.com', $sheet->getCell('BF3')->getValue());
        $this->assertEquals(null, $sheet->getCell('BF4')->getValue());
        $this->assertEquals(null, $sheet->getCell('BF5')->getValue());
    }


    /**
     * Test if the service is registered correctly in the service container
     */
    public function testServiceRegistration(): void
    {
        $this->checkContainerRegistration(GenerateExcelDataFile::class);
    }


    /**
     * Check all fixed headers
     *
     * @param Worksheet $sheet
     */
    private function checkStaticHeaders(Worksheet $sheet): void
    {
        foreach (ExcelDataFormats::STATIC_HEADERS as $column => $header) {
            $this->assertEquals($header, $sheet->getCell($this->translateCell($column, 1))->getValue());
        }
    }


    /**
     * This translates an integer column-row-value (e.g. 1, 1) into a user readable value (e.g. A2).
     *
     * @param int $column Zero-based column
     * @param int $row    One-based row
     *
     * @return string Cell name
     */
    private function translateCell(int $column, int $row): string
    {
        // Columns up to 25 have just one character (e.g. A4)
        if ($column < 26) {
            return (chr(ord('A') + $column).$row);
        }

        $first = floor($column / 26) - 1;
        $second = $column % 26;

        return chr(ord('A') + $first).chr(ord('A') + $second).$row;
    }


    /**
     * Check dynamic headers of one type, e.g. phone numbers
     *
     * @param int       $numberOfEntries Expected number of entries of this type.
     * @param array     $headerInfo      Header info from ExcelDataFormats::DYNAMIC_HEADERS
     * @param Worksheet $sheet           Work sheet to check
     * @param int       $column          Column where to start checking
     * @param bool      $suppressCounter When true, the main header in row 1 is expected to have no couter.
     *
     * @return int
     */
    private function checkDynamicHeaders(int $numberOfEntries, array $headerInfo, Worksheet $sheet, int $column, bool $suppressCounter = false): int
    {
        for ($number = 1; $number <= $numberOfEntries; ++$number) {
            $header = $headerInfo['header'];
            if (!$suppressCounter) {
                $header .= " $number";
            }
            $this->assertEquals($header, $sheet->getCell($this->translateCell($column, 1))->getValue());
            foreach ($headerInfo['fields'] as $field) {
                $this->assertEquals($field, $sheet->getCell($this->translateCell($column, 2))->getValue());
                ++$column;
            }
        }

        return $column;
    }
}
