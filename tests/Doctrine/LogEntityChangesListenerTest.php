<?php

/*
 * Copyright (C) 2022 by Luc Hamers <vereniging@hamers.de>
 *
 * This file is part of Vereniging.
 *
 * Vereniging is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Vereniging is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Vereniging.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Tests\Doctrine;

use App\Entity\Address;
use App\Entity\AddressType;
use App\Entity\Committee;
use App\Entity\CommitteeFunction;
use App\Entity\CompanyInformation;
use App\Entity\Country;
use App\Entity\Language;
use App\Entity\LogfileEntry;
use App\Entity\MailingList;
use App\Entity\MemberAddressMap;
use App\Entity\MemberEntry;
use App\Entity\MembershipNumber;
use App\Entity\MembershipType;
use App\Entity\PhoneNumber;
use App\Entity\PhoneNumberType;
use App\Entity\SerialLetter;
use App\Entity\SerialLetterContent;
use App\Entity\SerialLetterTemplate;
use App\Entity\Status;
use App\Entity\Title;
use App\Entity\UserRole;
use App\Tests\TestCase;
use Exception;

/**
 * Class LogEntityChangesListenerTest
 */
class LogEntityChangesListenerTest extends TestCase
{
    /**
     * @inheritDoc
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->loadAllFixtures();
        $this->setLoggedInUserForLogging();
        LogfileEntry::prepareTranslation($this->getEntityManager(), $this->getTranslator(), 'en');
    }


    /**
     * Check if address type entity changes are logged correctly
     *
     * @throws Exception
     */
    public function testAddressType(): void
    {
        $this->clearAllLogFiles();
        $entityManager = $this->getEntityManager();

        // Create
        $newAddressType = new AddressType();
        $newAddressType->setAddressType('new address type');
        $newAddressType->setUseCompanyName(false);
        $entityManager->persist($newAddressType);
        $entityManager->flush();


        // Change
        $newAddressType->setAddressType("changed address type");
        $entityManager->flush();

        // Delete
        $entityManager->remove($newAddressType);
        $entityManager->flush();

        // Get the last 3 log messages
        $logMessages = $this->checkDefaultLogMessageSettings('System settings change');
        $this->assertEquals(3, count($logMessages));

        // Reverse order because of DESC!
        $this->checkComplexLogMessage($logMessages[2], 'Created new entry of type addresstype ', ["address type" => ["new address type"]]);
        $this->checkComplexLogMessage($logMessages[1], 'Updated entry of type addresstype ', ["address type" => ["new address type", "changed address type"]]);
        $this->checkComplexLogMessage($logMessages[0], 'Deleted entry of type addresstype ', ["address type" => ["changed address type"]]);
    }


    /**
     * Check if committee entity changes are logged correctly
     *
     * @throws Exception
     */
    public function testCommittee(): void
    {
        $this->clearAllLogFiles();
        $entityManager = $this->getEntityManager();

        // Create
        $newCommittee = new Committee();
        $newCommittee->setCommitteeName('new committee');
        $entityManager->persist($newCommittee);
        $entityManager->flush();

        // Change
        $newCommittee->setCommitteeName('changed committee');
        $entityManager->flush();

        // Delete
        $entityManager->remove($newCommittee);
        $entityManager->flush();

        // Get the last 3 log message
        $logMessages = $this->checkDefaultLogMessageSettings('System settings change');
        $this->assertEquals(3, count($logMessages));

        // Reverse order because of DESC!
        $this->checkComplexLogMessage($logMessages[2], 'Created new entry of type committee ', ["committee name" => ["new committee"]]);
        $this->checkComplexLogMessage($logMessages[1], 'Updated entry of type committee ', ["committee name" => ["new committee", "changed committee"]]);
        $this->checkComplexLogMessage($logMessages[0], 'Deleted entry of type committee ', ["committee name" => ["changed committee"]]);
    }


    /**
     * Check if country entity changes are logged correctly
     *
     * @throws Exception
     */
    public function testCountry(): void
    {
        $this->clearAllLogFiles();
        $entityManager = $this->getEntityManager();

        // Create
        $newCountry = new Country();
        $newCountry->setCountry('new country');
        $entityManager->persist($newCountry);
        $entityManager->flush();

        // Change
        $newCountry->setCountry("changed country");
        $entityManager->flush();

        // Delete
        $entityManager->remove($newCountry);
        $entityManager->flush();

        // Get the last 3 log messages
        $logMessages = $this->checkDefaultLogMessageSettings('System settings change');
        $this->assertEquals(3, count($logMessages));

        // Reverse order because of DESC!
        $this->checkComplexLogMessage($logMessages[2], 'Created new entry of type country ', ["country" => ["new country"]]);
        $this->checkComplexLogMessage($logMessages[1], 'Updated entry of type country ', ["country" => ["new country", "changed country"]]);
        $this->checkComplexLogMessage($logMessages[0], 'Deleted entry of type country ', ["country" => ["changed country"]]);
    }


    /**
     * Check if committee function entity changes are logged correctly
     *
     * @throws Exception
     */
    public function testFunction(): void
    {
        $this->clearAllLogFiles();
        $entityManager = $this->getEntityManager();

        // Create
        $newFunction = new CommitteeFunction();
        $newFunction->setCommitteeFunctionName('new function');
        $entityManager->persist($newFunction);
        $entityManager->flush();

        // Change
        $newFunction->setCommitteeFunctionName('changed function');
        $entityManager->flush();

        // Delete
        $entityManager->remove($newFunction);
        $entityManager->flush();

        // Get the last 3 log messages
        $logMessages = $this->checkDefaultLogMessageSettings('System settings change');
        $this->assertEquals(3, count($logMessages));

        // Reverse order because of DESC!
        $this->checkComplexLogMessage($logMessages[2], 'Created new entry of type committeefunction ', ["committee function name" => ["new function"]]);
        $this->checkComplexLogMessage($logMessages[1], 'Updated entry of type committeefunction ', ["committee function name" => ["new function", "changed function"]]);
        $this->checkComplexLogMessage($logMessages[0], 'Deleted entry of type committeefunction ', ["committee function name" => ["changed function"]]);
    }


    /**
     * Check if language entity changes are logged correctly
     *
     * @throws Exception
     */
    public function testLanguage(): void
    {
        $this->clearAllLogFiles();
        $entityManager = $this->getEntityManager();

        // Create
        $newLanguage = new Language();
        $newLanguage->setLanguage('new language');
        $newLanguage->setLocale('NL');
        $newLanguage->setFlagImage('nl.png');
        $newLanguage->setUseForSerialLetters(false);
        $entityManager->persist($newLanguage);
        $entityManager->flush();

        // Change
        $newLanguage->setLanguage("changed language");
        $entityManager->flush();

        // Delete
        $entityManager->remove($newLanguage);
        $entityManager->flush();

        // Get the last 3 log messages
        $logMessages = $this->checkDefaultLogMessageSettings('System settings change');
        $this->assertEquals(3, count($logMessages));

        // Reverse order because of DESC!
        $this->checkComplexLogMessage($logMessages[2], 'Created new entry of type language ', ["language" => ["new language"]]);
        $this->checkComplexLogMessage($logMessages[1], 'Updated entry of type language ', ["language" => ["new language", "changed language"]]);
        $this->checkComplexLogMessage($logMessages[0], 'Deleted entry of type language ', ["language" => ["changed language"]]);
    }


    /**
     * Check if mailing list entity changes are logged correctly
     *
     * @throws Exception
     */
    public function testMailingList(): void
    {
        $this->clearAllLogFiles();
        $entityManager = $this->getEntityManager();

        // Create
        $newMailingList = new MailingList();
        $newMailingList->setListName('new mailing list');
        $newMailingList->setManagementEmail('new email');
        $newMailingList->setManagementPassword('new password');
        $entityManager->persist($newMailingList);
        $entityManager->flush();

        // Change
        $newMailingList->setListName("changed mailing list");
        $newMailingList->setManagementEmail("changed email");
        $entityManager->flush();

        // Changing the password should not create a log message
        $newMailingList->setManagementPassword("changed password");
        $entityManager->flush();

        // Delete
        $entityManager->remove($newMailingList);
        $entityManager->flush();

        // Get the last 3 log messages
        $logMessages = $this->checkDefaultLogMessageSettings('System settings change');
        $this->assertEquals(3, count($logMessages));

        // Reverse order because of DESC!
        $this->checkComplexLogMessage($logMessages[2], 'Created new entry of type mailing list new mailing list', ["list name" => ["new mailing list"]]);
        $this->checkComplexLogMessage($logMessages[1], 'Updated entry of type mailing list changed mailing list', ["list name" => ["new mailing list", "changed mailing list"], "management email" => ["new email", "changed email"]]);
        $this->checkComplexLogMessage($logMessages[0], 'Deleted entry of type mailing list changed mailing list', ["list name" => ["changed mailing list"]]);
    }


    /**
     * Check if membership type entity changes are logged correctly
     *
     * @throws Exception
     */
    public function testMembershipType(): void
    {
        $this->clearAllLogFiles();
        $entityManager = $this->getEntityManager();

        // Create
        $newMembershipType = new MembershipType();
        $newMembershipType->setType('new type');
        $newMembershipType->setGroupMembership(false);
        $newMembershipType->setMembershipFee(30);
        $newMembershipType->setDefaultGroupMembershipType(false);
        $newMembershipType->setDefaultNewMembershipType(false);
        $entityManager->persist($newMembershipType);
        $entityManager->flush();

        // Change
        $newMembershipType->setType('changed type');
        $newMembershipType->setGroupMembership(true);
        $newMembershipType->setMembershipFee(35);
        $newMembershipType->setDefaultGroupMembershipType(true);
        $newMembershipType->setDefaultNewMembershipType(true);
        $entityManager->flush();

        // Delete
        $entityManager->remove($newMembershipType);
        $entityManager->flush();

        // Get the last 7 log messages
        $logMessages = $this->checkDefaultLogMessageSettings('System settings change');
        $this->assertEquals(7, count($logMessages));

        // Reverse order because of DESC!
        $this->checkComplexLogMessage($logMessages[6], 'Created new entry of type membershiptype new type', ["Membership type" => ["new type"]]);
        $this->checkComplexLogMessage($logMessages[5], 'Updated entry of type membershiptype changed type', ["Membership type" => ["new type", "changed type"], "contribution claim" => [30, 35], "group membership" => ["no", "yes"], "default group membership" => ["no", "yes"], "default new membership" => ["no", "yes"]]);
        $this->checkComplexLogMessage($logMessages[4], 'Updated entry of type membershiptype Family member', ["default group membership" => ["yes", "no"]]);
        $this->checkComplexLogMessage($logMessages[3], 'Updated entry of type membershiptype Regular member', ["default new membership" => ["yes", "no"]]);
        $this->checkComplexLogMessage($logMessages[2], 'Updated entry of type membershiptype Family member', ["default group membership" => ["no", "yes"], "default new membership" => ["no", "yes"]]);
        $this->checkComplexLogMessage($logMessages[1], 'Updated entry of type membershiptype changed type', ["default group membership" => ["yes", "no"], "default new membership" => ["yes", "no"]]);
        $this->checkComplexLogMessage($logMessages[0], 'Deleted entry of type membershiptype changed type', ["Membership type" => ["changed type"]]);
    }


    /**
     * Check if phone number type entity changes are logged correctly
     *
     * @throws Exception
     */
    public function testPhoneNumberType(): void
    {
        $this->clearAllLogFiles();
        $entityManager = $this->getEntityManager();

        // Create
        $newPhoneNumberType = new PhoneNumberType();
        $newPhoneNumberType->setPhoneNumberType('new phone number type');
        $entityManager->persist($newPhoneNumberType);
        $entityManager->flush();

        // Change
        $newPhoneNumberType->setPhoneNumberType("changed phone number type");
        $entityManager->flush();

        // Delete
        $entityManager->remove($newPhoneNumberType);
        $entityManager->flush();

        // Get the last 3 log messages
        $logMessages = $this->checkDefaultLogMessageSettings('System settings change');
        $this->assertEquals(3, count($logMessages));

        // Reverse order because of DESC!
        $this->checkComplexLogMessage($logMessages[2], 'Created new entry of type phonenumbertype ', ["phone number type" => ["new phone number type"]]);
        $this->checkComplexLogMessage($logMessages[1], 'Updated entry of type phonenumbertype ', ["phone number type" => ["new phone number type", "changed phone number type"]]);
        $this->checkComplexLogMessage($logMessages[0], 'Deleted entry of type phonenumbertype ', ["phone number type" => ["changed phone number type"]]);
    }


    /**
     * Check if status entity changes are logged correctly
     *
     * @throws Exception
     */
    public function testStatus(): void
    {
        $this->clearAllLogFiles();
        $entityManager = $this->getEntityManager();

        // Create
        $newStatus = new Status();
        $newStatus->setStatus('new status');
        $newStatus->setHasMembershipNumber(false);
        $entityManager->persist($newStatus);
        $entityManager->flush();

        // Change
        $newStatus->setStatus("changed status");
        $entityManager->flush();

        // Delete
        $entityManager->remove($newStatus);
        $entityManager->flush();

        // Get the last 3 log messages
        $logMessages = $this->checkDefaultLogMessageSettings('System settings change');
        $this->assertEquals(3, count($logMessages));

        // Reverse order because of DESC!
        $this->checkComplexLogMessage($logMessages[2], 'Created new entry of type status ', ["status" => ["new status"]]);
        $this->checkComplexLogMessage($logMessages[1], 'Updated entry of type status ', ["status" => ["new status", "changed status"]]);
        $this->checkComplexLogMessage($logMessages[0], 'Deleted entry of type status ', ["status" => ["changed status"]]);
    }


    /**
     * Check if serial letter entity changes are logged correctly
     *
     * @throws Exception
     */
    public function testSerialLetter(): void
    {
        $this->clearAllLogFiles();
        $entityManager = $this->getEntityManager();

        // Create
        $newLetter = new SerialLetter();
        $newLetter->setSignatureUseTitle(false);
        $newLetter->setLastUpdatedBy($this->getFixtureReference('member_entry_last_name1'));
        $newLetter->setFirstSignatureMember($this->getFixtureReference('member_entry_last_name2'));
        $newLetter->setSecondSignatureMember(($this->getFixtureReference('member_entry_last_name3')));
        $newLetter->setFirstSignatureMembersFunction($this->getFixtureReference('committee_function_map_board_chairman'));
        $newLetter->setSecondSignatureMembersFunction($this->getFixtureReference('committee_function_map_board_vice_chairman'));
        $newLetterContent = new SerialLetterContent();
        $newLetter->addSerialLetterContent($newLetterContent);
        $newLetterContent->setSerialLetter($newLetter);
        $newLetterContent->setLanguage($this->getFixtureReference('language_german'));
        $newLetterContent->setLetterTitle('Test title');
        $newLetterContent->setContent('<p>Test content</p>');


        $entityManager->persist($newLetter);
        $entityManager->persist($newLetterContent);
        $entityManager->flush();

        // Change
        $newLetter->setSignatureUseTitle(true);
        $newLetter->setLastUpdatedBy($this->getFixtureReference('member_entry_last_name4'));
        $newLetter->setFirstSignatureMember($this->getFixtureReference('member_entry_last_name5'));
        $newLetter->setSecondSignatureMember(($this->getFixtureReference('member_entry_last_name6')));
        $newLetter->setFirstSignatureMembersFunction($this->getFixtureReference('committee_function_map_board_member'));
        $newLetter->setSecondSignatureMembersFunction($this->getFixtureReference('committee_function_map_activities_chairman'));
        $newLetterContent->setLetterTitle('Test title1');
        $newLetterContent->setContent('<p>Test extra content</p>');
        $entityManager->flush();

        // Delete
        $entityManager->remove($newLetter);
        $entityManager->flush();

        // Get the last 3 log messages
        $logMessages = $this->checkDefaultLogMessageSettings('Serial letter change');
        $this->assertEquals(3, count($logMessages));

        // Reverse order because of DESC!
        $this->checkComplexLogMessage($logMessages[2], 'Created new entry of type serial letter ', ["Title German" => ["Test title"]]);
        $expectedData = [
            // This will also test ist html messages use the correct diffing algorithm
            "Content German" => ["Test content", "Test <ins>extra</ins> content"],
            "First signing member" => ["Firstname2 Lastname2", "Firstname5 Lastname5"],
            "Second signing member" => ["Firstname3 Lastname3", "Firstname6 Lastname6"],
            "Title German" => ["Test title", "Test title1"],
            "Use titles in signature" => ["no", "yes"],
        ];

        $this->checkComplexLogMessage($logMessages[1], 'Updated entry of type serial letter ', $expectedData);
        $this->checkComplexLogMessage($logMessages[0], 'Deleted entry of type serial letter ', ["Title German" => ["Test title1"]]);
    }


    /**
     * Check if title entity changes are logged correctly
     *
     * @throws Exception
     */
    public function testTitle(): void
    {
        $this->clearAllLogFiles();
        $entityManager = $this->getEntityManager();

        // Create
        $newTitle = new Title();
        $newTitle->setTitle('new title');
        $newTitle->setAfterName(false);
        $entityManager->persist($newTitle);
        $entityManager->flush();

        // Change
        $newTitle->setTitle("changed title");
        $entityManager->flush();

        // Delete
        $entityManager->remove($newTitle);
        $entityManager->flush();

        // Get the last 3 log messages
        $logMessages = $this->checkDefaultLogMessageSettings('System settings change');
        $this->assertEquals(3, count($logMessages));

        // Reverse order because of DESC!
        $this->checkComplexLogMessage($logMessages[2], 'Created new entry of type title ', ["title" => ["new title"]]);
        $this->checkComplexLogMessage($logMessages[1], 'Updated entry of type title ', ["title" => ["new title", "changed title"]]);
        $this->checkComplexLogMessage($logMessages[0], 'Deleted entry of type title ', ["title" => ["changed title"]]);
    }


    /**
     * Check if changes to the direct fields of the member entry entity are logged correctly
     *
     * @throws Exception
     */
    public function testMemberEntryDirectFields(): void
    {
        $this->clearAllLogFiles();
        $entityManager = $this->getEntityManager();
        /** @var Language $languageDE */
        $languageDE = $this->getFixtureReference('language_german');
        /** @var Language $languageNL */
        $languageNL = $this->getFixtureReference('language_dutch');
        /** @var Status $statusMember */
        $statusMember = $this->getFixtureReference('status_member');
        /** @var Status $statusFutureMember */
        $statusFutureMember = $this->getFixtureReference('status_future_member');
        /** @var MembershipType $membershipType1 */
        $membershipType1 = $this->getFixtureReference('membership_type_regular');
        /** @var MembershipType $membershipType2 */
        $membershipType2 = $this->getFixtureReference('membership_type_family');
        /** @var UserRole $userRole1 */
        $userRole1 = $this->getFixtureReference('user_role_role_user');
        /** @var UserRole $userRole2 */
        $userRole2 = $this->getFixtureReference('user_role_role_member_administrator');
        /** @var Title $title1 */
        $title1 = $this->getFixtureReference('title3');
        /** @var Title $title2 */
        $title2 = $this->getFixtureReference('title1');
        $membershipNumber = new MembershipNumber();
        $membershipNumber->setMembershipNumber(123);
        $membershipNumber->setUseDirectDebit(false);

        // Create
        $newMember = new MemberEntry();
        $newMember->setFirstName('new first name');
        $newMember->setName('new name');
        $newMember->setGender('f');
        $newMember->setMembershipEndRequested(false);
        $newMember->setPreferredLanguage($languageDE);
        $newMember->setStatus($statusMember);
        $newMember->setMembershipType($membershipType1);
        $newMember->setMembershipNumber($membershipNumber);
        $newMember->setUserRole($userRole1);
        $newMember->setTitle($title1);
        $entityManager->persist($newMember);
        $entityManager->flush();

        // Change
        $newMember->setName("changed name");
        $newMember->setGender('m');
        $newMember->setMembershipEndRequested(true);
        $newMember->setPreferredLanguage($languageNL);
        $newMember->setStatus($statusFutureMember);
        $newMember->setMembershipType($membershipType2);
        $newMember->setUserRole($userRole2);
        $newMember->setTitle($title2);
        $entityManager->flush();

        $newMember->getMembershipNumber()->setUseDirectDebit(true);
        $entityManager->flush();

        // Get the last 2 log messages
        $logMessages = $this->checkDefaultLogMessageSettings('Member data change', false);
        $expectedData = [
            "first name" => ["new first name"],
            "last name" => ["new name"],
            "gender" => ["Female"],
            "Membership number" => ['123'],
        ];

        // Reverse order because of DESC!
        $this->checkComplexLogMessage($logMessages[1], 'Created new entry of type member ', $expectedData);
        $expectedData = [
            "Benutzerrolle" => ["Benutzer", "Mitgliederadministrator"],
            "Bevorzugte Sprache" => ["Deutsch", "Niederländisch"],
            "Geschlecht" => ["Weiblich", "Männlich"],
            "Mitgliedschaftsende beantragt" => ["nein", "ja"],
            "Nachname" => ["new name", "changed name"],
            "Status" => ["Mitglied", "Zukünftiges Mitglied"],
            "Titel" => ['Dr.', 'Dipl.-Ing.'],
            "Typ der Mitgliedschaft" => ["Reguläres Mitglied", "Familienmitglied"],
        ];
        // Set the language to read the log messages to German to check if the fields are translated corrected. This is
        // to prevent a regression (bug #80)
        LogfileEntry::prepareTranslation($this->getEntityManager(), $this->getTranslator(), 'de');
        $this->checkComplexLogMessage($logMessages[0], 'Eintrag vom Typ Mitglied aktualisiert ', $expectedData);
    }


    /**
     * Check if changes to the phone numbers are logged correctly
     *
     * @throws Exception
     */
    public function testMemberEntryPhoneNumbers(): void
    {
        $this->clearAllLogFiles();
        $entityManager = $this->getEntityManager();

        /** @var MemberEntry $member1 */
        $member1 = $this->getFixtureReference('member_entry_last_name1');
        /** @var PhoneNumberType $typeMobile */
        $typeMobile = $this->getFixtureReference('phone_number_type_mobile');
        $phoneNumber = $member1->getPhoneNumbers()[0];
        $phoneNumber->setPhoneNumber('1234');
        $typeHome = $phoneNumber->getPhoneNumberType();
        $phoneNumber->setPhoneNumberType($typeMobile);

        $entityManager->persist($member1);
        $entityManager->flush();

        $member1->removePhoneNumber($phoneNumber);
        $entityManager->persist($member1);
        $entityManager->flush();

        $phoneNumber = new PhoneNumber();
        $phoneNumber->setPhoneNumberType($typeHome);
        $phoneNumber->setPhoneNumber('123');
        $member1->addPhoneNumber($phoneNumber);
        $entityManager->flush();

        $logMessages = $this->checkDefaultLogMessageSettings('Member data change', false);
        $this->assertEquals(3, count($logMessages));

        // Reverse order because of DESC!
        $this->checkComplexLogMessage($logMessages[2], 'Updated entry of type phonenumber ', ["phone number" => ["0123456789", "1234"], "phone number type" => ["Home", "Mobile"]]);
        $this->checkComplexLogMessage($logMessages[1], 'Deleted entry of type phonenumber ', ["phone number" => ["1234"], "phone number type" => ["Mobile"]]);
        $this->checkComplexLogMessage($logMessages[0], 'Created new entry of type phonenumber ', ["phone number" => ["123"], "phone number type" => ["Home"]]);
    }


    /**
     * Check if changes to the address fields are logged correctly
     *
     * @throws Exception
     */
    public function testMemberEntryAddresses(): void
    {
        $this->clearAllLogFiles();
        $entityManager = $this->getEntityManager();

        /** @var MemberEntry $member1 */
        $member1 = $this->getFixtureReference('member_entry_last_name1');
        /** @var Country $countryNL */
        $countryNL = $this->getFixtureReference('country_netherlands');
        /** @var AddressType $typeCompany */
        $typeCompany = $this->getFixtureReference('address_type_company');
        $address = $member1->getMemberAddressMaps()[0]->getAddress();
        $address->setAddress('changed address');
        $address->setZip('cha zip');
        $address->setCity('changed city');
        $address->setCountry($countryNL);
        $address->setAddressType($typeCompany);
        $member1->getMemberAddressMaps()[0]->setIsMainAddress(false);
        $member1->getMemberAddressMaps()[1]->setIsMainAddress(true);

        $entityManager->flush();

        $member1->removeMemberAddressMap($member1->getMemberAddressMaps()[0]);
        $entityManager->persist($member1);
        $entityManager->flush();

        $address = new Address();
        $address->setAddress('new address');
        $address->setZip('new zip');
        $address->setCity('new city');
        $address->setCountry($countryNL);
        $address->setAddressType($typeCompany);
        $addressMap = new MemberAddressMap();
        $addressMap->setAddress($address);
        $addressMap->setIsMainAddress(true);
        $member1->addMemberAddressMap($addressMap);
        $entityManager->flush();

        $logMessages = $this->checkDefaultLogMessageSettings('Member data change', false);
        $this->assertEquals(3, count($logMessages));

        // Reverse order because of DESC!
        $this->checkComplexLogMessage($logMessages[2], 'Updated entry of type address ', ["address" => ["MyAddress1", "changed address"], "address type" => ["Home address", "Company address"], "zip" => ["MyZip1", "cha zip"], "city" => ["MyCity1", "changed city"], "country" => ["Germany", "Netherlands"], "is main address" => ["yes", "no"]]);
        $this->checkComplexLogMessage($logMessages[1], 'Deleted entry of type address ', ["address" => ["changed address"], "address type" => ["Company address"], "zip" => ["cha zip"], "city" => ["changed city"], "country" => ["Netherlands"], "is main address" => ["no"]]);
        $this->checkComplexLogMessage($logMessages[0], 'Created new entry of type address ', ["address" => ["new address"], "address type" => ["Company address"], "zip" => ["new zip"], "city" => ["new city"], "country" => ["Netherlands"], "is main address" => ["yes"]]);
    }


    /**
     * Check if changes to the company information are logged correctly
     *
     * @throws Exception
     */
    public function testMemberEntryCompanyInformation(): void
    {
        $this->clearAllLogFiles();
        $entityManager = $this->getEntityManager();

        /** @var MemberEntry $member */
        $member = $entityManager->getRepository(MemberEntry::class)->findOneBy(['firstName' => 'Firstname1']);
        $company = $member->getCompanyInformation();

        // Change
        $company->setName('changed company');
        $company->setCity('changed city');
        $company->setDescription('changed description');
        $company->setFunctionDescription('changed function');
        $company->setUrl('changed url');
        $entityManager->flush();

        // Delete
        $entityManager->remove($company);
        $entityManager->flush();

        // Create
        $company = new CompanyInformation();
        $company->setName('new company');
        $company->setCity('new city');
        $company->setDescription('new description');
        $company->setFunctionDescription('new function');
        $company->setUrl('new url');
        $member->setCompanyInformation($company);
        $entityManager->persist($member);
        $entityManager->flush();

        $logMessages = $this->checkDefaultLogMessageSettings('Member data change', false);
        $this->assertEquals(3, count($logMessages));

        // Reverse order because of DESC!
        $this->checkComplexLogMessage($logMessages[2], 'Updated entry of type company information ', ["company name" => ["My Company", "changed company"], "city" => ["My city", "changed city"], "function" => ["My function", "changed function"], "description" => ["My description", "changed description"], "url" => ["http://company.com", "changed url"]]);
        $this->checkComplexLogMessage($logMessages[1], 'Deleted entry of type company information ', ["company name" => ["changed company"]]);
        $this->checkComplexLogMessage($logMessages[0], 'Created new entry of type company information ', ["company name" => ["new company"]]);
    }



    /**
     * Check if changes to the serial letter templates are logged correctly
     *
     * @throws Exception
     */
    public function testSerialLetterTemplate(): void
    {
        $this->clearAllLogFiles();
        $entityManager = $this->getEntityManager();

        $template = $this->getFixtureReference('serial_letter_template11');
        $template->setTemplateContent('original content');
        $entityManager->persist($template);
        $entityManager->flush();
        $this->clearAllLogFiles();

        // Change
        /** @var SerialLetterTemplate $template */
        $template->setLanguage($this->getFixtureReference('language_german'));
        $template->setTemplateContent('changed content');
        $template->setTemplateName('changed template name');
        $entityManager->flush();

        // Delete
        $entityManager->remove($template);
        $entityManager->flush();

        // Create
        $template = new SerialLetterTemplate();
        $template->setLanguage($this->getFixtureReference('language_dutch'));
        $template->setTemplateContent('new content');
        $template->setTemplateName('new template name');
        $entityManager->persist($template);
        $entityManager->flush();

        $logMessages = $this->checkDefaultLogMessageSettings('System settings change', false);
        $this->assertEquals(3, count($logMessages));

        // Reverse order because of DESC!
        $this->checkComplexLogMessage($logMessages[2], 'Updated entry of type serial letter template ', ["content" => ["original content", "changed content"], "language" => ["English", "German"], "name" => ["Day program", "changed template name"]]);
        $this->checkComplexLogMessage($logMessages[1], 'Deleted entry of type serial letter template ', ["name" => ["changed template name"]]);
        $this->checkComplexLogMessage($logMessages[0], 'Created new entry of type serial letter template ', ["name" => ["new template name"]]);
    }


    /**
     * @param string $changeType
     * @param bool   $checkChangesOnMember
     *
     * @return array|LogfileEntry[]
     *
     * @throws Exception
     */
    private function checkDefaultLogMessageSettings(string $changeType, bool $checkChangesOnMember = true): array
    {
        $logMessages = $this->getAllLogs();
        foreach ($logMessages as $logMessage) {
            $this->assertEquals('Firstname1', $logMessage->getChangedByMember()->getFirstName());
            $this->assertEquals($changeType, $logMessage->getChangeType()->getChangeType());
            if ($checkChangesOnMember) {
                $this->assertNull($logMessage->getChangesOnMember());
            }
        }

        return $logMessages;
    }


    /**
     * @param LogfileEntry $logEntry
     * @param string       $expectedMessage
     * @param array        $expectedData
     */
    private function checkComplexLogMessage(LogfileEntry $logEntry, string $expectedMessage, array $expectedData): void
    {
        $complexLogEntry = $logEntry->getComplexLogEntry();
        $this->assertEquals($expectedMessage, $complexLogEntry->getMainMessage());
        $this->assertEquals($expectedData, $complexLogEntry->getDataAsArray());
    }
}
