<?php

/*
 * Copyright (C) 2022 by Luc Hamers <vereniging@hamers.de>
 *
 * This file is part of Vereniging.
 *
 * Vereniging is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Vereniging is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Vereniging.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Tests;

use App\Entity\LogfileEntry;
use App\Entity\MemberEntry;
use App\Service\LogMessageCreator;
use Doctrine\Common\DataFixtures\Executor\AbstractExecutor;
use Doctrine\Common\DataFixtures\ReferenceRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Query\ResultSetMapping;
use Symfony\Component\Translation\Translator;
use Liip\TestFixturesBundle\Services\DatabaseToolCollection;
use Liip\TestFixturesBundle\Services\DatabaseTools\AbstractDatabaseTool;

/**
 * This bundles all methods needed for both integration and functional tests, i.e. for the TestCase and the WebTestCase.
 *
 * Trait TestCaseTrait
 */
trait TestCaseTrait
{
    protected static string $logDir;
    protected ReferenceRepository $referenceRepository;
    private AbstractDatabaseTool $databaseTool;


    /**
     * Do some initialisations which can be used in the setup methods
     */
    public function setupMutual(): void
    {
        $this->useForeignKeys(true);
        $this->databaseTool = static::getContainer()->get(DatabaseToolCollection::class)->get();
    }


    /**
     * @inheritDoc
     */
    public static function setUpBeforeClass(): void
    {
        self::$logDir = getcwd()."/var/tests";
        if (!is_dir(self::$logDir)) {
            mkdir(self::$logDir, 0777, true);
        }
    }


    /**
     * Things that must be done in the teardown method
     */
    public function tearDownMutual(): void
    {
        LogMessageCreator::resetUser();
    }


    /**
     * Return the doctrine entity manager
     *
     * @return EntityManagerInterface
     */
    protected function getEntityManager(): EntityManagerInterface
    {
        return $this->getContainer()->get('doctrine.orm.entity_manager');
    }


    /**
     * @return Translator
     */
    protected function getTranslator()
    {
        return $this->getContainer()->get('translator');
    }


    /**
     * Use this method to load certian fixture classes.
     *
     * @param array $fixtures
     */
    protected function loadFixtures(array $fixtures): void
    {
        $this->useForeignKeys(false);
        $this->databaseTool->loadFixtures($fixtures);
        $this->useForeignKeys(true);
    }


    /**
     * This loads all the fixtures defined in the project, including ordering them, e.g. by the
     * DependentFixtureInterface
     *
     * @param string[] $groups Filter fixtures by group name.
     *
     * @return AbstractExecutor|null
     */
    protected function loadAllFixtures(array $groups = ['default']): ?AbstractExecutor
    {
        $this->useForeignKeys(false);
        $fixtures = $this->databaseTool->loadAllFixtures($groups);
        $this->referenceRepository = $fixtures->getReferenceRepository();
        $this->useForeignKeys(true);

        return $fixtures;
    }


    /**
     * Return an object defined in the fixtures by reference.
     *
     * @param string $reference
     *
     * @return object|null
     */
    protected function getFixtureReference(string $reference): ?object
    {
        if ($this->referenceRepository) {
            return $this->referenceRepository->getReference($reference);
        }

        return null;
    }


    /**
     * Set Firstname1 as currently logging user, i.e. the user which is used for generating log files.
     *
     * @param bool             $reset        When true, the change-by member for log messages is set to null.
     * @param MemberEntry|null $loggedInUser Set user which creates the log messages
     *
     * @return object|null Member with name Firstname1 Lastname1.
     */
    protected function setLoggedInUserForLogging(bool $reset = false, ?MemberEntry $loggedInUser = null): ?MemberEntry
    {
        if ((!$reset) && (!$loggedInUser)) {
            $loggedInUser = $this->getEntityManager()->getRepository(MemberEntry::class)->findOneBy(['firstName' => 'Firstname1']);
        }
        /** @var LogMessageCreator $logMessageCreator */
        $logMessageCreator = $this->getContainer()->get(LogMessageCreator::class);
        $logMessageCreator->setUser($reset ? null : $loggedInUser);

        return $loggedInUser;
    }


    /**
     * Remove all log messages from the database.
     */
    protected function clearAllLogFiles(): void
    {
        $logEntries = $this->getEntityManager()->getRepository(LogfileEntry::class)->findAll();
        foreach ($logEntries as $logEntry) {
            $this->getEntityManager()->remove($logEntry);
        }
        $this->getEntityManager()->flush();
    }


    /**
     * @param bool $orderDescending When true, the log entries are returned last message first
     *
     * @return LogfileEntry[]
     */
    protected function getAllLogs(bool $orderDescending = true): array
    {
        $order = $orderDescending ? 'DESC' : 'ASC';

        return $this->getEntityManager()->getRepository(LogfileEntry::class)->findBy([], ['id' => $order]);
    }


    /**
     * By default, the foreign key checks are disabled when using sqlite and Doctrine. This prevents cascading
     * deletes, which are managed by the foreign keys. To be able to use the these, the foreign key checks must be
     * enabled manually. This method can enable or disable these checks
     *
     * @param bool $enable
     */
    private function useForeignKeys(bool $enable): void
    {
        if ($enable) {
            $setting = 'ON';
        } else {
            $setting = 'OFF';
        }
        $this->getEntityManager()
            ->createNativeQuery("PRAGMA foreign_keys = $setting;", new ResultSetMapping())
            ->execute();
    }
}
