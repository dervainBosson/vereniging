<?php

/*
 * Copyright (C) 2022 by Luc Hamers <vereniging@hamers.de>
 *
 * This file is part of Vereniging.
 *
 * Vereniging is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Vereniging is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Vereniging.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Tests\Entity;

use App\Entity\EmailTemplate;
use App\Tests\TestCase;
use Exception;

/**
 * Class PhoneNumberTypeTest
 */

class EmailTemplateTest extends TestCase
{
    /**
     * @inheritDoc
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->loadAllFixtures();
        $this->setLoggedInUserForLogging();
    }


    /**
     * Test the special handling of name setting
     */
    public function testSetTemplateName(): void
    {
        $template = new EmailTemplate();
        $this->assertEquals('', $template->getTemplateName());
        $template->setTemplateName('default name');
        $this->assertEquals('default name', $template->getTemplateName());
        // Add a translation for the default language. This has to be returned when the getter is called
        $template->addNewTranslation('en', 'test1');
        $this->assertEquals('test1', $template->getTemplateName());
        $this->assertEquals('default name', $template->getTemplateName(null, true));
        $template->addNewTranslation('de', 'test2');
        $this->assertEquals('test2', $template->getTemplateName('de'));

        // When changing the name, then the default translation must also be changed
        $template->setTemplateName('test3');
        $this->assertEquals('test3', $template->getTemplateName('en'));
    }


    /**
     * Test the special handling of sender address
     */
    public function testSetSenderAddress(): void
    {
        $template = new EmailTemplate();
        $this->assertEquals('', $template->getSenderAddress());
        $template->setSenderAddress('test@email.com');
        $this->assertEquals('test@email.com', $template->getSenderAddress());
        // Add a translation for the default language. This has to be returned when the getter is called
        $template->addNewTSenderAddressTranslation('en', 'test-en@email.com');
        $this->assertEquals('test-en@email.com', $template->getSenderAddress());
        $this->assertEquals('test@email.com', $template->getSenderAddress(null, true));
        $template->addNewTSenderAddressTranslation('de', 'test-de@email.com');
        $this->assertEquals('test-de@email.com', $template->getSenderAddress('de'));

        // When changing the sender address, then the default translation must also be changed
        $template->setSenderAddress('test-new@email.com');
        $this->assertEquals('test-new@email.com', $template->getSenderAddress('en'));
    }


    /**
     * Test the special handling of type setting
     */
    public function testSetTemplateType(): void
    {
        $template = new EmailTemplate();
        $this->assertEquals('serial email', $template->getTemplateType());
        try {
            $template->setTemplateType('some type');
            $this->fail("Expected exception did not occur");
        } catch (Exception $e) {
            $this->assertEquals('Template type "some type" not allowed, only values ["serial email","login data","change message"] are allowed!', $e->getMessage());
        }

        $template->setTemplateType('change message');
        $this->assertEquals('change message', $template->getTemplateType());
    }


    /**
     * Test the overloaded findAll method
     */
    public function testFindAll(): void
    {
        $templates = $this->getEntityManager()->getRepository(EmailTemplate::class)->findAll();
        $this->assertCount(4, $templates);
        $this->assertEquals('Change message', $templates[0]->getTemplateName());
        $this->assertEquals('Login data', $templates[1]->getTemplateName());
        $this->assertEquals('New email', $templates[2]->getTemplateName());
        $this->assertEquals('Serial email', $templates[3]->getTemplateName());
    }



    /**
     * Test reading the change template
     */
    public function testGetChangeTemplate(): void
    {
        $template = $this->getEntityManager()->getRepository(EmailTemplate::class)->readChangeTemplate();
        $expected = 'change content: '.EmailTemplate::HTML_CONTENT_VARIABLE;
        $this->assertEquals($expected, $template->getContent());
    }


    /**
     * Test reading the login template
     */
    public function testGetLoginTemplate(): void
    {
        $template = $this->getEntityManager()->getRepository(EmailTemplate::class)->readLoginTemplate();
        $expected = '<p>login content: </p>'.EmailTemplate::HTML_CONTENT_VARIABLE;
        $this->assertEquals($expected, $template->getContent());
    }


    /**
     * Test reading the serial email templates
     */
    public function testGetEmailTemplates(): void
    {
        $templates = $this->getEntityManager()->getRepository(EmailTemplate::class)->getEmailTemplates();
        $this->assertCount(2, $templates);
        $expected = 'serial email 1 content: '.EmailTemplate::HTML_CONTENT_VARIABLE;
        $this->assertEquals($expected, $templates[1]->getContent());
        $expected = 'serial email 2 content: '.EmailTemplate::HTML_CONTENT_VARIABLE;
        $this->assertEquals($expected, $templates[0]->getContent());
    }


    /**
     * Test the getTemplateNames method
     *
     * @return void
     */
    public function testGetTemplateNames(): void
    {
        $template1 = $this->getEntityManager()->getRepository(EmailTemplate::class)->find(1);

        $names = $template1->getTemplateNames();
        $this->assertEquals('en: Change message; nl: Veranderingsbericht; de: Änderungsnachricht', $names);
    }
}
