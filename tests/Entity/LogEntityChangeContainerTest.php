<?php

/*
 * Copyright (C) 2022 by Luc Hamers <vereniging@hamers.de>
 *
 * This file is part of Vereniging.
 *
 * Vereniging is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Vereniging is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Vereniging.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Tests\Entity;

use App\Entity\AbstractComplexLogEntryBase;
use App\Entity\LogEntityChangeContainer;
use App\Entity\LogEntryDataField;
use App\Entity\Status;
use App\Tests\TestCase;
use Exception;

/**
 * Test for the log entity change container class.
 *
 */
class LogEntityChangeContainerTest extends TestCase
{
    /**
     * @inheritDoc
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->loadAllFixtures();
        $this->setLoggedInUserForLogging();
    }


    /**
     * Test all the getters and setters
     */
    public function testGettersSetters(): void
    {
        $testObject = new LogEntityChangeContainer();
        $this->assertNull($testObject->getChangesOnMemberId());
        $this->assertEquals(0, count($testObject->getContent()));
        $this->assertEquals('', $testObject->getStringIdentifier());

        $this->initialiseDefaultObject($testObject);

        $this->checkDefaultObject($testObject);
        $this->assertCount(2, $testObject->getContent());
    }


    /**
     * Test the merge method when the old object is empty (i.e. when the object is created)
     *
     * @throws Exception
     */
    public function testMergeWithEmptyThis(): void
    {
        $old = new LogEntityChangeContainer();
        $new = new LogEntityChangeContainer();
        $this->initialiseDefaultObject($new);

        /** @var AbstractComplexLogEntryBase $diff */
        $diff = $old->diffObjects($new);

        // We expect the extended value to be removed and the short value to return an array where the untranslated
        // content is the only field.
        $this->assertCount(1, $diff->getDataAsArray());
        $this->assertEquals(1, $diff->getNumberOfDataFields());
        $this->assertEquals(['name' => ['nameValue']], $diff->getDataAsArray());
    }


    /**
     * Test the merge method when the new object is empty (i.e. when the object is deleted)
     *
     * @throws Exception
     */
    public function testMergeWithEmptyOther(): void
    {
        $old = new LogEntityChangeContainer();
        $new = new LogEntityChangeContainer();
        $this->initialiseDefaultObject($old);

        /** @var AbstractComplexLogEntryBase $diff */
        $diff = $old->diffObjects($new);

        // We expect the extended value to be removed and the short value to return an array where the untranslated
        // content is the only field.
        $this->assertCount(1, $diff->getDataAsArray());
        $this->assertEquals(1, $diff->getNumberOfDataFields());
        $this->assertEquals(['name' => ['nameValue']], $diff->getDataAsArray());
    }


    /**
     * Test the merge method when both objects contain data
     *
     * @throws Exception
     */
    public function testMergeWithCompleteObjects(): void
    {
        $old = new LogEntityChangeContainer();
        $new = new LogEntityChangeContainer();
        $this->initialiseDefaultObject($old);
        $this->initialiseDefaultObject($new);

        /** @var AbstractComplexLogEntryBase $diff */
        $diff = $old->diffObjects($new);

        // Both objects are the same
        $this->assertCount(0, $diff->getDataAsArray());

        // Change both the short content and the extended content
        $old->addContentItem('additional key', false, 'additional value');
        $new->addContentItem('additional key', false, 'additional value');
        $new->addContentItem('name', false, 'new nameValue');
        $new->addContentItem('key', true, 'new value');
        /** @var AbstractComplexLogEntryBase $diff */
        $diff = $old->diffObjects($new);
        $this->assertCount(2, $diff->getDataAsArray());
        $this->assertEquals(2, $diff->getNumberOfDataFields());
        $this->assertEquals(['name' => ['nameValue', 'new nameValue'], 'key' => ['value', 'new value']], $diff->getDataAsArray());
    }


    /**
     * @throws Exception
     */
    public function testDiffWithLogEntryDataFields(): void
    {
        $old = new LogEntityChangeContainer();
        $new = new LogEntityChangeContainer();
        $this->initialiseDefaultObject($old);
        $this->initialiseDefaultObject($new);

        $status = $this->getEntityManager()->getRepository(Status::class)->findBy([
            'status' => ['member', 'future_member'],
        ]);

        $old->addContentItem('gender', false, new LogEntryDataField('Male', true));
        $new->addContentItem('gender', false, new LogEntryDataField('Female', true));
        $old->addContentItem('status', false, new LogEntryDataField($status[0], true));
        $new->addContentItem('status', false, new LogEntryDataField($status[1], true));
        $diff = $old->diffObjects($new);
        $this->assertCount(2, $diff->getDataAsArray());
        $this->assertEquals(2, $diff->getNumberOfDataFields());
        $this->assertEquals(['gender' => ['Male', 'Female'], 'status' => ['Member', 'Future member']], $diff->getDataAsArray());
    }


    /**
     * @param LogEntityChangeContainer $testObject
     */
    private function initialiseDefaultObject(LogEntityChangeContainer $testObject): void
    {
        $testObject->setClassName(get_class($this));
        $testObject->setIndex(5);
        $testObject->setChangesOnMemberId(6);
        $testObject->setChangeType('change type');
        $testObject
            ->addContentItem('name', false, 'nameValue')
            ->addContentItem('key', true, 'value');
        $testObject->setStringIdentifier('identifier');
    }


    /**
     * @param LogEntityChangeContainer $testObject
     */
    private function checkDefaultObject(LogEntityChangeContainer $testObject): void
    {
        $this->assertEquals('logentitychangecontainertest', $testObject->getClassName());
        $this->assertEquals(5, $testObject->getIndex());
        $this->assertEquals(6, $testObject->getChangesOnMemberId());
        $this->assertEquals('change type', $testObject->getChangeType());
        $this->assertEquals('identifier', $testObject->getStringIdentifier());
    }
}
