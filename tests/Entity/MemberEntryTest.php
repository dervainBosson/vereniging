<?php

/*
 * Copyright (C) 2022 by Luc Hamers <vereniging@hamers.de>
 *
 * This file is part of Vereniging.
 *
 * Vereniging is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Vereniging is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Vereniging.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Tests\Entity;

use App\Entity\Address;
use App\Entity\AddressType;
use App\Entity\Country;
use App\Entity\MailingList;
use App\Entity\MemberEntry;
use App\Entity\MemberAddressMap;
use App\Entity\MembershipType;
use App\Entity\PhoneNumber;
use App\Entity\PhoneNumberType;
use App\Tests\TestCase;

/**
 * Class MemberEntryTest
 */
class MemberEntryTest extends TestCase
{
    /**
     * @inheritDoc
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->loadAllFixtures();
        $this->setLoggedInUserForLogging();
    }


    /**
     * Test gender getter because this can return a short and a long form of the gender.
     */
    public function testGetGender(): void
    {
        /** @var memberEntry $memberEntry */
        $memberEntry = $this->getFixtureReference('member_entry_last_name1');

        $this->assertEquals('m', $memberEntry->getGender());
        $this->assertEquals('m', $memberEntry->getGender(false));
        $this->assertEquals('Male', $memberEntry->getGender(true));
    }


    /**
     * Test adding phone number entities to a member entry.
     */
    public function testAddPhoneNumbers(): void
    {
        /** @var memberEntry $memberEntry */
        $memberEntry = $this->getFixtureReference('member_entry_last_name1');

        /** @var PhoneNumberType $mobileType */
        $mobileType = $this->getFixtureReference('phone_number_type_mobile');
        $phoneNumber = new PhoneNumber();
        $phoneNumber->setPhoneNumber('121212');
        $phoneNumber->setPhoneNumberType($mobileType);
        // addPhoneNumber should set the member entry in $phoneNumber.
        $memberEntry->addPhoneNumber($phoneNumber);
        // Adding the same phone number for the second time should do no harm
        $memberEntry->addPhoneNumber($phoneNumber);

        $this->getEntityManager()->persist($phoneNumber);
        $this->getEntityManager()->flush();

        $memberEntry = $this->getFixtureReference('member_entry_last_name1');
        $this->assertEquals(5, count($memberEntry->getPhoneNumbers()));
        $this->assertEquals("121212", $memberEntry->getPhoneNumbers()[4]->getPhoneNumber());

        // Check if the phone number can be removed
        $memberEntry->removePhoneNumber($phoneNumber);
        $this->assertEquals(4, count($memberEntry->getPhoneNumbers()));
        // Check that remove the same number again does no harm
        $memberEntry->removePhoneNumber($phoneNumber);
        $this->assertEquals(4, count($memberEntry->getPhoneNumbers()));
    }


    /**
     *  Test adding addresses
     */
    public function testAddRemoveAddress(): void
    {
        /** @var memberEntry $memberEntry */
        $memberEntry = $this->getFixtureReference('member_entry_last_name1');

        $this->assertEquals(2, count($memberEntry->getMemberAddressMaps()));
        $address = new Address();
        $address->setAddress("my address1");
        $address->setZip("my zip1");
        $address->setCity("my city1");
        /** @var Country $country */
        $country = $this->getFixtureReference('country_netherlands');
        $address->setCountry($country);
        /** @var AddressType $addressType */
        $addressType = $this->getFixtureReference('address_type_private');
        $address->setAddressType($addressType);

        $addressMap = new MemberAddressMap();
        $addressMap->setAddress($address);
        $addressMap->setMemberEntry($memberEntry);
        $addressMap->setIsMainAddress(false);
        $this->getEntityManager()->persist($addressMap);
        $this->getEntityManager()->flush();

        $this->assertEquals(3, count($memberEntry->getMemberAddressMaps()));

        // Now remove the address map.
        $this->getEntityManager()->remove($addressMap);
        $this->getEntityManager()->flush();

        $this->assertEquals(2, count($memberEntry->getMemberAddressMaps()));
    }


    /**
     * Test if the addresses are returned correctly from the member entry.
     */
    public function testGetAddresses(): void
    {
        // First add a second address where main address is set. When calling findAll, this new address should come back
        // first because here the main address flag is set.
        /** @var memberEntry $memberEntry */
        $memberEntry = $this->getFixtureReference('member_entry_last_name1');
        //$this->addAddress($memberEntry, true);
        $addresses = $memberEntry->getAddressesAsArray();
        $this->assertEquals(2, count($addresses));
        $this->assertEquals('MyAddress1', $addresses[0]['address']);
        $this->assertEquals('MyZip1', $addresses[0]['zip']);
        $this->assertEquals('MyCity1', $addresses[0]['city']);
        $this->assertEquals('Germany', $addresses[0]['country']);
        $this->assertEquals('Home address', $addresses[0]['type']);
        $this->assertTrue($addresses[0]['is_main_address']);
        $this->assertEquals('MyAddress2', $addresses[1]['address']);
        $this->assertEquals('MyZip2', $addresses[1]['zip']);
        $this->assertEquals('MyCity2', $addresses[1]['city']);
        $this->assertEquals('Netherlands', $addresses[1]['country']);
        $this->assertEquals('Company address', $addresses[1]['type']);
        $this->assertArrayHasKey('company', $addresses[1]);
        $this->assertEquals('My Company', $addresses[1]['company']);
        $this->assertFalse($addresses[1]['is_main_address']);

        // Now set the company to null. This should remove the company name from the address
        $company = $memberEntry->getCompanyInformation();
        $this->getEntityManager()->remove($company);
        $this->getEntityManager()->flush();
        $this->getEntityManager()->refresh($memberEntry);
        $memberEntry = $this->getFixtureReference('member_entry_last_name1');
        $this->assertNull($memberEntry->getCompanyInformation());
        $addresses = $memberEntry->getAddressesAsArray();
        $this->assertEquals(2, count($addresses));
        $this->assertEquals('Company address', $addresses[1]['type']);
        $this->assertArrayNotHasKey('company', $addresses[1]);


        // Now test this method with the added indexes.
        $indexes = array();
        $addresses = $memberEntry->getAddressesAsArray(true);
        foreach ($addresses as $key => $address) {
            $indexes[] = array('map_id' => $key, 'address_id' => $address['address_id']);
        }
        $this->assertEquals(2, count($addresses));
        $this->assertEquals('MyAddress1', $addresses[$indexes[0]['map_id']]['address']);
        $this->assertEquals('MyZip1', $addresses[$indexes[0]['map_id']]['zip']);
        $this->assertEquals('MyCity1', $addresses[$indexes[0]['map_id']]['city']);
        $this->assertEquals('Germany', $addresses[$indexes[0]['map_id']]['country']);
        $this->assertEquals('Home address', $addresses[$indexes[0]['map_id']]['type']);
        $this->assertTrue($addresses[$indexes[0]['map_id']]['is_main_address']);
        $this->assertEquals($indexes[0]['map_id'], $addresses[$indexes[0]['map_id']]['map_id']);
        $this->assertEquals($indexes[0]['address_id'], $addresses[$indexes[0]['map_id']]['address_id']);
        $this->assertEquals('MyAddress2', $addresses[$indexes[1]['map_id']]['address']);
        $this->assertEquals('MyZip2', $addresses[$indexes[1]['map_id']]['zip']);
        $this->assertEquals('MyCity2', $addresses[$indexes[1]['map_id']]['city']);
        $this->assertEquals('Netherlands', $addresses[$indexes[1]['map_id']]['country']);
        $this->assertEquals('Company address', $addresses[$indexes[1]['map_id']]['type']);
        $this->assertFalse($addresses[$indexes[1]['map_id']]['is_main_address']);
        $this->assertEquals($indexes[1]['map_id'], $addresses[$indexes[1]['map_id']]['map_id']);
        $this->assertEquals($indexes[1]['address_id'], $addresses[$indexes[1]['map_id']]['address_id']);

        $addresses = $this->getEntityManager()->getRepository(MemberAddressMap::class)->findBy(['memberEntry' => $memberEntry]);
        $this->getEntityManager()->remove($addresses[0]);
        $this->getEntityManager()->remove($addresses[1]);
        $this->getEntityManager()->flush();

        $this->assertTrue($memberEntry->getAddressesAsArray() === array());
    }


    /**
     * Test reading the mail address
     */
    public function testGetMainAddress(): void
    {
        /** @var memberEntry $memberEntry */
        $memberEntry = $this->getFixtureReference('member_entry_last_name1');
        $address = $memberEntry->getMainAddress();
        $this->assertEquals('MyAddress1', $address['address']);
        $this->assertEquals('MyZip1', $address['zip']);
        $this->assertEquals('MyCity1', $address['city']);
        $this->assertEquals('Germany', $address['country']);
        $this->assertArrayNotHasKey('company', $address);

        // Member 11 has a company address, so the company key must be present
        $memberEntry = $this->getFixtureReference('member_entry_last_name11');
        $address = $memberEntry->getMainAddress();
        $this->assertEquals('MyAddress2', $address['address']);
        $this->assertEquals('MyZip2', $address['zip']);
        $this->assertEquals('MyCity2', $address['city']);
        $this->assertEquals('Netherlands', $address['country']);
        $this->assertEquals('My Company2', $address['company']);

        // Member 3 has no address
        $memberEntry = $this->getFixtureReference('member_entry_last_name3');
        $address = $memberEntry->getMainAddress();
        $this->assertNull($address);
    }


    /**
     * Test reading the phone numbers from the member entries.
     */
    public function testGetPhoneNumbersAsArray(): void
    {
        /** @var memberEntry $memberEntry */
        $memberEntry = $this->getFixtureReference('member_entry_last_name1');

        $indexes = array();
        $phoneNumbers = $memberEntry->getPhoneNumbers();
        foreach ($phoneNumbers as $number) {
            $indexes[] = $number->getId();
        }
        $phoneNumbers = $memberEntry->getPhoneNumbersAsArray();
        $this->assertEquals($phoneNumbers[$indexes[0]]['phone_number'], '2876543210');
        $this->assertEquals($phoneNumbers[$indexes[0]]['type'], 'Company');
        $this->assertEquals($phoneNumbers[$indexes[0]]['phone_number_id'], $indexes[0]);
        $this->assertEquals($phoneNumbers[$indexes[1]]['phone_number'], '9876543210');
        $this->assertEquals($phoneNumbers[$indexes[1]]['type'], 'Company');
        $this->assertEquals($phoneNumbers[$indexes[1]]['phone_number_id'], $indexes[1]);
        $this->assertEquals($phoneNumbers[$indexes[2]]['phone_number'], '0123456789');
        $this->assertEquals($phoneNumbers[$indexes[2]]['type'], 'Home');
        $this->assertEquals($phoneNumbers[$indexes[2]]['phone_number_id'], $indexes[2]);
        $this->assertEquals($phoneNumbers[$indexes[3]]['phone_number'], '121212');
        $this->assertEquals($phoneNumbers[$indexes[3]]['type'], 'Mobile');
        $this->assertEquals($phoneNumbers[$indexes[3]]['phone_number_id'], $indexes[3]);
    }


    /**
     * This function tests adding and removing members to the many2many relationship for change types.
     */
    public function testAddRemoveChangeTypes(): void
    {
        $changeType = $this->getFixtureReference('change_type_member_data_change');
        $member = $this->getFixtureReference('member_entry_last_name1');

        $member->addEmailNotification($changeType);
        $this->getEntityManager()->flush();
        $this->assertCount(1, $member->getEmailNotifications());
        $this->assertEquals('Member data change', $member->getEmailNotifications()[0]);

        // Check if adding a second time doesn't do any harm
        $member->addEmailNotification($changeType);
        $this->getEntityManager()->flush();
        $this->assertCount(1, $member->getEmailNotifications());
        $this->assertEquals('Member data change', $member->getEmailNotifications()[0]);

        // Check if removing the change type works correctly
        $member->removeEmailNotification($changeType);
        $this->getEntityManager()->flush();
        $this->assertCount(0, $member->getEmailNotifications());

        // Check if removing a second time doesn't do any harm
        $member->removeEmailNotification($changeType);
        $this->getEntityManager()->flush();
        $this->assertCount(0, $member->getEmailNotifications());
    }


    /**
     * Test if the salt for the password encryption is returned correctly.
     */
    public function testGetSalt(): void
    {
        /** @var memberEntry $memberEntry */
        $memberEntry = $this->getFixtureReference('member_entry_last_name1');

        $this->assertNull($memberEntry->getSalt());
    }


    /**
     * Test setting the password, which uses the event subscriber HashPasswordListener.
     */
    public function testPasswordHandling(): void
    {
        /** @var memberEntry $memberEntry */
        $memberEntry = $this->getFixtureReference('member_entry_last_name1');

        // Change password
        $password = "my secret";
        $memberEntry->setPlainPassword($password);
        $this->assertEquals($password, $memberEntry->getPlainPassword());
        $this->getEntityManager()->persist($memberEntry);
        $this->getEntityManager()->flush();

        $this->assertTrue(password_verify($password, $memberEntry->getPassword()));
        $this->assertEquals('$2y$04$', substr($memberEntry->getPassword(), 0, 7));
        $this->assertEmpty($memberEntry->getPlainPassword());

        // Check if eraseCredentials works correctly.
        $memberEntry->setPlainPassword("password");
        $memberEntry->eraseCredentials();
        $this->assertEmpty($memberEntry->getPlainPassword());

        // Return to previous state.
        $this->getEntityManager()->refresh($memberEntry);
    }


    /**
     * Test if the roles are returned correctly.
     */
    public function testGetRoles(): void
    {
        /** @var memberEntry $memberEntry */
        $memberEntry = $this->getFixtureReference('member_entry_last_name1');
        $this->assertContains('ROLE_VIEW_DASHBOARD', $memberEntry->getRoles());
        $this->assertContains('ROLE_EDIT_OTHER_USERS', $memberEntry->getRoles());
        $this->assertContains('ROLE_MANAGE_LIST_VALUES', $memberEntry->getRoles());

        $memberEntry = $this->getEntityManager()->getRepository(MemberEntry::class)
                                           ->findOneBy(['username' => 'firstname2.lastname2']);
        $this->assertContains('ROLE_VIEW_DASHBOARD', $memberEntry->getRoles());
    }


    /**
     * Test both the getUser and getCompleteName methods at the same time, since the input data for both tests is the
     * same.
     */
    public function testGetUserAndGetCompleteName(): void
    {
        $member = new MemberEntry();

        // Easiest case
        $member->setFirstName("FirstName");
        $member->setName("LastName");
        $this->assertEquals("firstname.lastname", $member->getUsername());
        $this->assertEquals("FirstName LastName", $member->getCompleteName());

        // Test if the user name is generated at a call to setFirstName And TO setLastName
        $member->setFirstName("FirstName1");
        $this->assertEquals("firstname1.lastname", $member->getUsername());
        $member->setName("LastName1");
        $this->assertEquals("firstname1.lastname1", $member->getUsername());
        $this->assertEquals("FirstName1 LastName1", $member->getCompleteName());

        // Case with a dot in the name
        $member->setFirstName("FirstName N.");
        $member->setName("LastName");
        $this->assertEquals("firstname.n.lastname", $member->getUsername());
        $this->assertEquals("FirstName N. LastName", $member->getCompleteName());

        // Case with multiple dots in the name
        $member->setFirstName("FirstName N.....");
        $member->setName("LastName");
        $this->assertEquals("firstname.n.lastname", $member->getUsername());
        $this->assertEquals("FirstName N..... LastName", $member->getCompleteName());

        // Case spaces at the beginning and end of the name
        $member->setFirstName(" FirstName ");
        $member->setName(" LastName ");
        $this->assertEquals("firstname.lastname", $member->getUsername());
        $this->assertEquals("FirstName LastName", $member->getCompleteName());

        // Case with a comma in the name: the part after the comma has to be witten first
        $member->setFirstName("FirstName");
        $member->setName("LastName, van den");
        $this->assertEquals("firstname.van.den.lastname", $member->getUsername());
        $this->assertEquals("FirstName van den LastName", $member->getCompleteName());

        // Case with multiple commas in the name: the parts have to written in reverse order
        $member->setFirstName("FirstName");
        $member->setName("LastName, den, van ");
        $this->assertEquals("firstname.van.den.lastname", $member->getUsername());
        $this->assertEquals("FirstName van den LastName", $member->getCompleteName());

        // Check if non-ascii-chars at the beginning of a name work correctly for the user name
        $member->setFirstName('Öabc');
        $member->setName('Üdef');
        $this->assertEquals("öabc.üdef", $member->getUsername());
        $this->assertEquals("Öabc Üdef", $member->getCompleteName());

        // Now check if the academic titles are inserted correctly
        /** @var memberEntry $member */
        $member = $this->getFixtureReference('member_entry_last_name1');
        $this->assertEquals('Dipl.-Ing. Firstname1 Lastname1', $member->getCompleteName(true));
        $this->assertEquals('Firstname1 Lastname1', $member->getCompleteName(false));
        $this->assertEquals('Firstname1 Lastname1', $member->getCompleteName());

        /** @var memberEntry $member */
        $member = $this->getFixtureReference('member_entry_last_name2');
        $this->assertEquals('Firstname2 Lastname2 M.A.', $member->getCompleteName(true));
        $this->assertEquals('Firstname2 Lastname2', $member->getCompleteName(false));
        $this->assertEquals('Firstname2 Lastname2', $member->getCompleteName());

        // Check if the unset first name (is stored as '-') is omitted
        /** @var memberEntry $member */
        $member = $this->getFixtureReference('member_entry_last_name1');
        $member->setFirstName('-');
        $this->assertEquals('Dipl.-Ing. Lastname1', $member->getCompleteName(true));
        $this->assertEquals('Lastname1', $member->getCompleteName(false));
        $member->setFirstName('Firstname1');
    }


    /**
     * Test adding and removing mailing lists to and from a member entry..
     */
    public function testAddRemoveMailingLists(): void
    {
        /** @var MemberEntry $member */
        $member = $this->getFixtureReference('member_entry_last_name2');
        /** @var MailingList $list */
        $list = $this->getEntityManager()->getRepository(MailingList::class)->findOneBy(['listName' => 'MailingList_2']);

        // Add entry to the member entry side
        $member->addMailingList($list);
        $this->getEntityManager()->persist($member);
        $this->getEntityManager()->flush();
        $this->assertEquals(1, $list->getMemberEntries()->count());
        $this->assertEquals(1, $member->getMailingLists()->count());

        // Add the same entry a second time. This should not blow up
        $member->addMailingList($list);
        $this->getEntityManager()->persist($member);
        $this->getEntityManager()->flush();
        $this->assertEquals(1, $member->getMailingLists()->count());
        $this->assertEquals(1, $list->getMemberEntries()->count());

        // Remove entry from the list side
        $member->removeMailingList($list);
        $this->getEntityManager()->persist($member);
        $this->getEntityManager()->flush();
        $this->assertEquals(0, $list->getMemberEntries()->count());
        $this->assertEquals(0, $member->getMailingLists()->count());

        // Remove the same entry a second time. This should not blow up.
        $member->removeMailingList($list);
        $this->getEntityManager()->persist($member);
        $this->getEntityManager()->flush();
        $this->assertEquals(0, $list->getMemberEntries()->count());
        $this->assertEquals(0, $member->getMailingLists()->count());
    }


    /**
     * Test serialize and deserialize methods
     */
    public function testSerialization(): void
    {
        /** @var memberEntry $memberEntry */
        $memberEntry = $this->getFixtureReference('member_entry_last_name1');

        $memberString = $memberEntry->serialize();

        $otherMember = new MemberEntry();
        $otherMember->unserialize($memberString);

        $this->assertEquals($memberEntry->getId(), $otherMember->getId());
        $this->assertEquals($memberEntry->getPassword(), $otherMember->getPassword());
        $this->assertEquals($memberEntry->getUsername(), $otherMember->getUsername());
    }


    /**
     * Test adding and removing members to the member group.
     */
    public function testAddRemoveGroupMembers(): void
    {
        /** @var memberEntry $memberEntry1 */
        $memberEntry1 = $this->getFixtureReference('member_entry_last_name1');
        /** @var memberEntry $memberEntry2 */
        $memberEntry2 = $this->getFixtureReference('member_entry_last_name2');
        /** @var memberEntry $memberEntry10 */
        $memberEntry10 = $this->getFixtureReference('member_entry_last_name10');
        /** @var memberEntry $memberEntry11 */
        $memberEntry11 = $this->getFixtureReference('member_entry_last_name11');
        /** @var memberEntry $memberEntry12 */
        $memberEntry12 = $this->getFixtureReference('member_entry_last_name12');
        $this->checkGroup(false);

        // Add member2 to the group of member1. Both members should be in each others group.
        $memberEntry1->addGroupMember($memberEntry2);
        $this->checkGroup(true);

        // Check if this is saved to the database correctly
        $this->getEntityManager()->flush();
        $this->getEntityManager()->refresh($memberEntry1);
        $this->getEntityManager()->refresh($memberEntry2);
        $this->getEntityManager()->refresh($memberEntry10);
        $this->getEntityManager()->refresh($memberEntry11);
        $this->getEntityManager()->refresh($memberEntry12);
        $this->checkGroup(true);

        // Check that a member is not added twice to the group
        $memberEntry1->addGroupMember($memberEntry2);
        $memberEntry2->addGroupMember($memberEntry1);
        $this->checkGroup(true);

        // Removing itself should not do anything
        $memberEntry1->removeGroupMember($memberEntry1);
        $memberEntry2->removeGroupMember($memberEntry2);
        $this->checkGroup(true);

        // Removing a member from the group should remove it on both sides
        $memberEntry1->removeGroupMember($memberEntry2);
        $this->checkGroup(false);

        // Check if this is saved to the database correctly
        $this->getEntityManager()->flush();
        $this->getEntityManager()->refresh($memberEntry1);
        $this->getEntityManager()->refresh($memberEntry2);
        $this->getEntityManager()->refresh($memberEntry10);
        $this->getEntityManager()->refresh($memberEntry11);
        $this->getEntityManager()->refresh($memberEntry12);
        $this->checkGroup(false);
    }


    /**
     * Test if the event listener updates the membership type for all group members, when a group membership is
     * selected.
     */
    public function testMembershipTypeOnGroupMembers(): void
    {
        /** @var memberEntry $memberEntry */
        $memberEntry = $this->getFixtureReference('member_entry_last_name1');

        /** @var MembershipType $groupMembershipType */
        $groupMembershipType = $this->getFixtureReference('membership_type_family');
        /** @var MembershipType $newMembershipType */
        $newMembershipType = $this->getFixtureReference('membership_type_regular');

        // Set membership type for the current member to a group membership type. This should also change the membership
        // type for all other members in the group.
        $memberEntry->setMembershipType($groupMembershipType);
        $this->getEntityManager()->persist($memberEntry);
        $this->getEntityManager()->flush();
        $this->getEntityManager()->refresh($memberEntry);
        $this->assertEquals(3, count($memberEntry->getGroupMembers()));
        $this->assertEquals($groupMembershipType, $memberEntry->getMembershipType());
        $this->assertEquals($groupMembershipType, $memberEntry->getGroupMembers()[0]->getMembershipType());
        $this->assertEquals($groupMembershipType, $memberEntry->getGroupMembers()[1]->getMembershipType());
        $this->assertEquals($groupMembershipType, $memberEntry->getGroupMembers()[2]->getMembershipType());

        // Set the membership type for the current member to a non-group membership type. This should only change the
        // membership type for the current member, but not for the group members.
        $memberEntry->setMembershipType($newMembershipType);
        $this->getEntityManager()->persist($memberEntry);
        $this->getEntityManager()->flush();
        $this->getEntityManager()->refresh($memberEntry);
        $this->assertEquals(3, count($memberEntry->getGroupMembers()));
        $this->assertEquals($newMembershipType, $memberEntry->getMembershipType());
        $this->assertEquals($groupMembershipType, $memberEntry->getGroupMembers()[0]->getMembershipType());
        $this->assertEquals($groupMembershipType, $memberEntry->getGroupMembers()[1]->getMembershipType());
        $this->assertEquals($groupMembershipType, $memberEntry->getGroupMembers()[2]->getMembershipType());
    }


    /**
     * Test writing and reading picture data.
     */
    public function testSetReadPicture(): void
    {
        $memberEntry = new MemberEntry();
        $this->assertNull($memberEntry->getPicture());
        $memberEntry->setPicture('picture');
        $this->assertEquals('picture', $memberEntry->getPicture());
    }


    /**
     * Check of the group with four members is correct
     *
     * @param bool $groupOfFour When set, the group has 4 members
     */
    private function checkGroup(bool $groupOfFour): void
    {
        /** @var memberEntry $memberEntry1 */
        $memberEntry1 = $this->getFixtureReference('member_entry_last_name1');
        /** @var memberEntry $memberEntry2 */
        $memberEntry2 = $this->getFixtureReference('member_entry_last_name2');
        /** @var memberEntry $memberEntry10 */
        $memberEntry10 = $this->getFixtureReference('member_entry_last_name10');
        /** @var memberEntry $memberEntry11 */
        $memberEntry11 = $this->getFixtureReference('member_entry_last_name11');
        /** @var memberEntry $memberEntry12 */
        $memberEntry12 = $this->getFixtureReference('member_entry_last_name12');

        if ($groupOfFour) {
            $this->assertEquals(4, $memberEntry1->getGroupMembers()->count());
            $this->assertEquals(4, $memberEntry10->getGroupMembers()->count());
            $this->assertEquals(4, $memberEntry11->getGroupMembers()->count());
            $this->assertEquals(4, $memberEntry12->getGroupMembers()->count());
            $this->assertEquals(4, $memberEntry2->getGroupMembers()->count());
        } else {
            $this->assertEquals(3, $memberEntry1->getGroupMembers()->count());
            $this->assertEquals(3, $memberEntry10->getGroupMembers()->count());
            $this->assertEquals(3, $memberEntry11->getGroupMembers()->count());
            $this->assertEquals(3, $memberEntry12->getGroupMembers()->count());
            $this->assertEquals(0, $memberEntry2->getGroupMembers()->count());
        }
        $this->assertTrue($memberEntry1->getGroupMembers()->contains($memberEntry10));
        $this->assertTrue($memberEntry1->getGroupMembers()->contains($memberEntry11));
        $this->assertTrue($memberEntry1->getGroupMembers()->contains($memberEntry12));

        if ($groupOfFour) {
            $this->assertTrue($memberEntry1->getGroupMembers()->contains($memberEntry2));
            $this->assertTrue($memberEntry2->getGroupMembers()->contains($memberEntry1));
            $this->assertTrue($memberEntry2->getGroupMembers()->contains($memberEntry10));
            $this->assertTrue($memberEntry2->getGroupMembers()->contains($memberEntry11));
            $this->assertTrue($memberEntry2->getGroupMembers()->contains($memberEntry12));
        }
    }
}
