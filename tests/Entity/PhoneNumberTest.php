<?php

/*
 * Copyright (C) 2022 by Luc Hamers <vereniging@hamers.de>
 *
 * This file is part of Vereniging.
 *
 * Vereniging is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Vereniging is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Vereniging.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Tests\Entity;

use App\Entity\MemberEntry;
use App\Entity\PhoneNumber;
use App\Tests\TestCase;

/**
 * Test for the address type entity.
 *
 */
class PhoneNumberTest extends TestCase
{
    /**
     * @inheritDoc
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->loadAllFixtures();
    }


    /**
     * Test toArray method.
     */
    public function testToArray(): void
    {
        /** @var PhoneNumber $number1 */
        $number1 = $this->getEntityManager()->getRepository(PhoneNumber::class)->findOneBy(['phoneNumber' => "0123456789"]);
        /** @var PhoneNumber $number2 */
        $number2 = $this->getEntityManager()->getRepository(PhoneNumber::class)->findOneBy(['phoneNumber' => "9876543210"]);
        $numberArray1 = $number1->toArray();
        $numberArray2 = $number2->toArray();
        $this->assertArrayHasKey("phone_number", $numberArray1);
        $this->assertArrayHasKey("phone_number", $numberArray2);
        $this->assertArrayHasKey("type", $numberArray1);
        $this->assertArrayHasKey("type", $numberArray2);

        $this->assertEquals("0123456789", $numberArray1["phone_number"]);
        $this->assertEquals("9876543210", $numberArray2["phone_number"]);
        $this->assertEquals("Home", $numberArray1["type"]);
        $this->assertEquals("Company", $numberArray2["type"]);
    }


    /**
     * Test reading the member entry
     */
    public function testGetMemberEntry(): void
    {
        /** @var PhoneNumber $number1 */
        $number1 = $this->getEntityManager()->getRepository(PhoneNumber::class)->findOneBy(['phoneNumber' => "0123456789"]);
        /** @var PhoneNumber $number2 */
        $number2 = $this->getEntityManager()->getRepository(PhoneNumber::class)->findOneBy(['phoneNumber' => "9876543210"]);

        $member1 = $this->getFixtureReference('member_entry_last_name1');

        $this->assertEquals($member1->getId(), $number1->getMemberEntry()->getId());
        $this->assertEquals($member1->getId(), $number2->getMemberEntry()->getId());
    }


    /**
     * Test toArray method.
     */
    public function testFindMemberPhoneNumbersOrderedByType(): void
    {
        /** @var MemberEntry $member1 */
        $member1 = $this->getFixtureReference('member_entry_last_name1');
        /** @var PhoneNumber[] $phoneNumbers */
        $phoneNumbers = $this->getEntityManager()->getRepository(PhoneNumber::class)
                                                 ->findMemberPhoneNumbersOrderedByType($member1);

        // Check if only the phone numbers for this member are read
        $this->assertEquals(4, count($phoneNumbers));
        // Check that all phone numbers are ordered by type
        $this->assertEquals($phoneNumbers[0]->getPhoneNumberType()->getPhoneNumberType(), "Company");
        $this->assertEquals($phoneNumbers[1]->getPhoneNumberType()->getPhoneNumberType(), "Company");
        $this->assertEquals($phoneNumbers[2]->getPhoneNumberType()->getPhoneNumberType(), "Home");
        $this->assertEquals($phoneNumbers[3]->getPhoneNumberType()->getPhoneNumberType(), "Mobile");
        // Check that in case of multiple entries for one type, the entries are ordered by phone number
        $this->assertEquals($phoneNumbers[0]->getPhoneNumber(), "2876543210");
        $this->assertEquals($phoneNumbers[1]->getPhoneNumber(), "9876543210");
    }
}
