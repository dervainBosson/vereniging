<?php

/*
 * Copyright (C) 2022 by Luc Hamers <vereniging@hamers.de>
 *
 * This file is part of Vereniging.
 *
 * Vereniging is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Vereniging is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Vereniging.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Tests\Entity;

use App\Entity\Language;
use App\Tests\TestCase;

/**
 * Class LanguageTest
 */
class LanguageTest extends TestCase
{
    /**
     * @inheritDoc
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->loadAllFixtures();
        $this->setLoggedInUserForLogging();
    }


    /**
     * Test getters which are not tested elsewhere.
     */
    public function testGetters(): void
    {
        /** @var Language $language */
        $language = $this->getEntityManager()->getRepository(Language::class)->findOneBy(['language' => 'German']);
        $this->assertNotNull($language->getId());
        $this->assertEquals('de.png', $language->getFlagImage());
        $this->assertEquals('de', $language->getLocale());
    }


    /**
     * Test the inUse method.
     */
    public function testIsInUse(): void
    {
        $language = $this->getEntityManager()->getRepository(Language::class)->findOneBy(['language' => 'German']);
        $this->assertTrue($language->isInUse());
        $language = $this->getEntityManager()->getRepository(Language::class)->findOneBy(['language' => 'Dutch']);
        $this->assertTrue($language->isInUse());
        $language = $this->getEntityManager()->getRepository(Language::class)->findOneBy(['language' => 'English']);
        $this->assertTrue($language->isInUse());
        $language = $this->getEntityManager()->getRepository(Language::class)->findOneBy(['language' => 'Spanish']);
        $this->assertFalse($language->isInUse());
    }


    /**
     * Test the getSlug method.
     */
    public function testGetSetBabelName(): void
    {
        $language = $this->getEntityManager()->getRepository(Language::class)->findOneBy(['language' => 'German']);
        $this->assertEquals('ngerman', $language->getBabelName());
        $language = $this->getEntityManager()->getRepository(Language::class)->findOneBy(['language' => 'Dutch']);
        $this->assertEquals('dutch', $language->getBabelName());
        $language = $this->getEntityManager()->getRepository(Language::class)->findOneBy(['language' => 'English']);
        $this->assertEquals('english', $language->getBabelName());
        $language = $this->getEntityManager()->getRepository(Language::class)->findOneBy(['language' => 'Spanish']);
        $this->assertEquals('spanish', $language->getBabelName());
    }


    /**
     * Test if the findAll method in the repository class has been overloaded to return the values ordered ascending.
     */
    public function testFindAll(): void
    {
        $languages = $this->getEntityManager()->getRepository(Language::class)->findAll();
        $this->assertCount(4, $languages);
        $this->assertEquals('Dutch', $languages[0]->getLanguage());
        $this->assertEquals('English', $languages[1]->getLanguage());
        $this->assertEquals('German', $languages[2]->getLanguage());
        $this->assertEquals('Spanish', $languages[3]->getLanguage());
    }


    /**
     * Test if the findAllForSerialLetters method in the repository class returns the correct values ordered ascending.
     */
    public function testFindAllForSerialLetter(): void
    {
        $languages = $this->getEntityManager()->getRepository(Language::class)->findAllForSerialLetters();
        $this->assertCount(2, $languages);
        $this->assertEquals('Dutch', $languages[0]->getLanguage());
        $this->assertEquals('English', $languages[1]->getLanguage());
    }


    /**
     * Test the find all locales method in the repository
     */
    public function testFindAllLocales(): void
    {
        $locales = $this->getEntityManager()->getRepository(Language::class)->findAllLocales('en');
        $this->assertCount(4, $locales);
        $this->assertEquals(['en', 'de', 'es', 'nl'], $locales);

        $locales = $this->getEntityManager()->getRepository(Language::class)->findAllLocales('nl');
        $this->assertCount(4, $locales);
        $this->assertEquals(['nl', 'de', 'en', 'es'], $locales);

        // Use default locale which is not in the database
        $locales = $this->getEntityManager()->getRepository(Language::class)->findAllLocales('bla');
        $this->assertCount(5, $locales);
        $this->assertEquals(['bla', 'de', 'en', 'es', 'nl'], $locales);
    }


    /**
     * Check if translations for the phone number type table work correctly
     */
    public function testTranslation(): void
    {
        $language = $this->getEntityManager()->getRepository(Language::class)->findOneBy(['language' => 'German']);

        $this->assertEquals("German", $language->getLanguage());
        $this->assertEquals("German", $language);
        $this->assertEquals("German", $language->translate('en')->getLanguageTranslated());
        $this->assertEquals("Duits", $language->translate('nl')->getLanguageTranslated());
        $this->assertEquals("Deutsch", $language->translate('de')->getLanguageTranslated());

        $language->addNewTranslation('es', 'German in Spanish');
        $this->getEntityManager()->flush();
        $this->getEntityManager()->refresh($language);
        $this->assertEquals('German in Spanish', $language->translate('es')->getLanguageTranslated());
    }


    /**
     * Test if a default translation is created when a new object is created without one.
     */
    public function testDefaultTranslation(): void
    {
        $language = new Language();
        $language->setLanguage('Testlanguage');
        $language->setUseForSerialLetters(false);
        $language->setLocale('TS');
        $language->setFlagImage('');
        $this->getEntityManager()->persist($language);
        $this->getEntityManager()->flush();
        $id = $language->getId();
        $this->assertEquals('Testlanguage', $language->getLanguage());

        // Check if the translations are persisted correctly when reloading the object from the database
        $this->getEntityManager()->clear();
        $newType = $this->getEntityManager()->getRepository(Language::class)->findOneBy(['id' => $id]);
        $this->assertEquals('Testlanguage', $newType->getLanguage());
    }
}
