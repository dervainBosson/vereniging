<?php

/*
 * Copyright (C) 2022 by Luc Hamers <vereniging@hamers.de>
 *
 * This file is part of Vereniging.
 *
 * Vereniging is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Vereniging is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Vereniging.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Tests\Entity;

use App\Entity\Title;
use App\Tests\TestCase;

/**
 * Class TitleTest
 */
class TitleTest extends TestCase
{
    /**
     * @inheritDoc
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->loadAllFixtures();
    }


    /**
     * Test the inUse method.
     */
    public function testIsInUse(): void
    {
        $title = $this->getFixtureReference('title1');
        $this->assertTrue($title->isInUse());
        $title = $this->getFixtureReference('title2');
        $this->assertTrue($title->isInUse());
        $title = $this->getFixtureReference('title3');
        $this->assertFalse($title->isInUse());
    }


    /**
     * Test if the findAll method in the repository class has been overloaded to return the values ordered ascending.
     */
    public function testFindAll(): void
    {
        $titles = $this->getEntityManager()->getRepository(Title::class)->findAll();
        $this->assertEquals("Dipl.-Ing.", $titles[0]->getTitle());
        $this->assertEquals("Dr.", $titles[1]->getTitle());
        $this->assertEquals("M.A.", $titles[2]->getTitle());
    }
}
