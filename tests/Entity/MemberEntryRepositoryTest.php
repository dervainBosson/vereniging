<?php

/*
 * Copyright (C) 2022 by Luc Hamers <vereniging@hamers.de>
 *
 * This file is part of Vereniging.
 *
 * Vereniging is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Vereniging is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Vereniging.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Tests\Entity;

use App\Entity\ChangeType;
use App\Entity\MemberEntry;
use App\Entity\Status;
use App\Tests\TestCase;
use Doctrine\ORM\QueryBuilder;
use Exception;

/**
 * Test for the member entry repository
 *
 */
class MemberEntryRepositoryTest extends TestCase
{
    /**
     * @inheritDoc
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->loadAllFixtures();
        $this->setLoggedInUserForLogging();
    }


    /**
     * Test the method to get all members with the same last name as the current member.
     */
    public function testCreateFindAllWithSameAddressesAndLastNameQueryBuilder(): void
    {
        /** @var MemberEntry $member1 */
        $member1 = $this->getFixtureReference('member_entry_last_name1');

        /** @var QueryBuilder $qb */
        $qb = $this->getEntityManager()->getRepository(MemberEntry::class)
                   ->createFindAllWithSameAddressesAndLastNameQueryBuilder($member1);
        $members = $qb->getQuery()->execute();
        $this->checkMembersInArray([11, 10, 12, 9], $members);
    }


    /**
     * Test the method to get all members with a selected user role
     */
    public function testCreateFindAllWithRoleQueryBuilder(): void
    {
        $repo = $this->getEntityManager()->getRepository(MemberEntry::class);
        // Test a user role from the system administrators
        /** @var QueryBuilder $qb */
        $qb = $repo->createFindAllWithRoleQueryBuilder('ROLE_MANAGE_PASSWORDS');
        $members = $qb->getQuery()->execute();
        $this->checkMembersInArray([1], $members);

        // Test a user role from the member administrators
        $qb = $repo->createFindAllWithRoleQueryBuilder('ROLE_SIGN_SERIAL_LETTERS');
        $members = $qb->getQuery()->execute();
        $this->checkMembersInArray([1, 3], $members);

        // Test if additional members can be added
        $additionalMembers[] = $members[0]->getId();
        $additionalMembers[] = $repo->findOneBy(['firstName' => 'Firstname5'])->getId();
        $qb = $repo->createFindAllWithRoleQueryBuilder('ROLE_SIGN_SERIAL_LETTERS', $additionalMembers);
        $members = $qb->getQuery()->execute();
        $this->checkMembersInArray([1, 3, 5], $members);
    }


    /**
     * Test findByIdOrderedByCountryAndGender
     */
    public function testFindByIdOrderedByCountryAndGender(): void
    {
        $repo = $this->getEntityManager()->getRepository(MemberEntry::class);
        // /First test for an empty id list
        $members = $repo->findByIdOrderedByCountryAndGender([]);
        $this->checkMembersInArray([], $members);

        // Second select some members and check the number and order. These are from several countries and with several
        // genders.
        $members = $repo->findBy(['firstName' => ['Firstname12', 'Firstname7', 'Firstname1', 'Firstname8']]);
        $ids = [];
        foreach ($members as $member) {
            $ids[] = $member->getId();
        }

        $members = $repo->findByIdOrderedByCountryAndGender($ids);
        $this->checkMembersInArray([1, 12, 8, 7], $members);
    }


    /**
     * Test findByIdOrderedByName
     */
    public function testFindByIdOrderedByName(): void
    {
        $repo = $this->getEntityManager()->getRepository(MemberEntry::class);
        // /First test for an empty id list
        $members = $repo->findByIdOrderedByName([]);
        $this->checkMembersInArray([], $members);

        $members = $repo->findBy(['firstName' => ['Firstname11', 'Firstname8', 'Firstname1', 'Firstname7']]);
        $ids = [];
        foreach ($members as $member) {
            $ids[] = $member->getId();
        }

        $members = $repo->findByIdOrderedByName($ids);
        $this->checkMembersInArray([1, 11, 7, 8], $members);
    }


    /**
     * The overloaded findBy method.
     */
    public function testFindByCriteria(): void
    {
        $repo = $this->getEntityManager()->getRepository(MemberEntry::class);
        // When no search criteria are passed, find no members
        $members = $repo->findByCriteria([]);
        $this->checkMembersInArray([], $members);

        // Except one, all members have the status 'member', 'former_member' is not used.
        $memberStatus = $this->getEntityManager()->getRepository(Status::class)->findBy(['status' => ['member', 'former_member']]);
        $members = $repo->findByCriteria(['status' => $memberStatus]);
        // Check order: the entries are first ordered by last name (i.e. Firstname 1 and 11 have the same last name!),
        // then by first name.
        $this->checkMembersInArray([1, 11, 10, 12, 2, 3, 5, 6, 7, 8, 9], $members);

        // Check if the exclude field works correct
        $members = $repo->findByCriteria(['status' => $memberStatus, 'exclude' => [$members[0]->getId(), $members[1]->getId()]]);
        $this->checkMembersInArray([10, 12, 2, 3, 5, 6, 7, 8, 9], $members);
        $members = $repo->findByCriteria(['status' => $memberStatus, 'exclude' => $members]);
        $this->checkMembersInArray([1, 11], $members);
        $members = $repo->findByCriteria(['exclude' => [1000]]);
        $this->checkMembersInArray([1, 11, 10, 12, 2, 3, 4, 5, 6, 7, 8, 9], $members);
        $members = $repo->findByCriteria(['exclude' => []]);
        $this->checkMembersInArray([1, 11, 10, 12, 2, 3, 4, 5, 6, 7, 8, 9], $members);

        // To search for committee members, the member ids are passed in.
        $member12 = $repo->findBy(['firstName' => ['Firstname1', 'Firstname2']]);
        $ids = [$member12[0]->getId(), $member12[1]->getId()];
        $members = $repo->findByCriteria(['id' => $ids]);
        $this->checkMembersInArray([1, 2], $members);

        // Find members, regardless of having or not having an email address. When there is no other search criterion,
        // this should return an empty list.
        $members = $repo->findByCriteria(['email' => 'all', 'directDebit' => 'all']);
        $this->checkMembersInArray([], $members);

        // Check if returning members regardless of their email address works correctly
        $members = $repo->findByCriteria(['status' => $memberStatus, 'email' => 'all']);
        $this->checkMembersInArray([1, 11, 10, 12, 2, 3, 5, 6, 7, 8, 9], $members);

        // Now add member2 to the group of member1, which HAS an email address. This is needed to distinguish between
        // both without email address options
        $members = $repo->findBy(['firstName' => ['Firstname1', 'Firstname2', 'Firstname3']]);
        $members[0]->addGroupMember($members[1]);
        // To be able to check the email selection, remove the email address of member3
        $members[2]->setEmail(null);
        $this->getEntityManager()->flush();
        // Check if returning members with an email address works correctly
        $members = $repo->findByCriteria(['status' => $memberStatus, 'email' => 'with']);
        $this->checkMembersInArray([1, 11, 10, 12, 5, 6, 7, 8, 9], $members);

        // Check if returning members without an email address works correctly
        $members = $repo->findByCriteria(['status' => $memberStatus, 'email' => 'without']);
        $this->checkMembersInArray([2, 3], $members);

        // Check if returning members without an email address in the group works correctly
        $members = $repo->findByCriteria(['status' => $memberStatus, 'email' => 'without group']);
        $this->checkMembersInArray([3], $members);

        // Check if membership end requested is used correctly
        $members = $repo->findByCriteria(['membershipEndRequested' => true]);
        $this->checkMembersInArray([3], $members);

        // Check members with and without debt
        $members = $repo->findByCriteria(['status' => $memberStatus, 'debt' => 'without_debt']);
        $this->checkMembersInArray([5, 6, 7, 8, 9], $members);

        $members = $repo->findByCriteria(['status' => $memberStatus, 'debt' => 'with_debt']);
        $this->checkMembersInArray([1, 11, 10, 12, 2, 3], $members);
    }


    /**
     * Test the query builder which finds all members which have email notifications (change types) defined
     */
    public function testCreateFindAllEmailNotificationsQueryBuilder(): void
    {
        $repo = $this->getEntityManager()->getRepository(MemberEntry::class);
        // First add some email notifications since these are not defined in the fixtures.
        $members = $repo->findby(['firstName' => ['Firstname1', 'Firstname2']]);
        $changeType = $this->getEntityManager()->getRepository(ChangeType::class)->findOneBy(['changeType' => 'error']);
        foreach ($members as $member) {
            $member->addEmailNotification($changeType);
        }
        $this->getEntityManager()->flush();

        // Now do the actual test
        $members = $repo->createFindAllWithEmailNotificationsQueryBuilder()
                        ->getQuery()
                        ->execute();
        $this->checkMembersInArray([1, 2], $members);

        // Test when passing additional member ids to the query
        $member3 = $repo->findOneBy(['firstName' => 'Firstname3']);
        $members = $repo->createFindAllWithEmailNotificationsQueryBuilder([$member3->getId()])
                        ->getQuery()
                        ->execute();
        $this->checkMembersInArray([1, 2, 3], $members);
    }


    /**
     * Test the findAllByStatus method
     */
    public function testFindAllByStatus(): void
    {
        $repo = $this->getEntityManager()->getRepository(MemberEntry::class);
        $members = $repo->findByStatus('member');
        $this->checkMembersInArray([1, 11, 10, 12, 2, 3, 5, 6, 7, 8, 9], $members);

        $members = $repo->findByStatus('former_member');
        $this->checkMembersInArray([], $members);

        $members = $repo->findByStatus('future_member');
        $this->checkMembersInArray([4], $members);
    }


    /**
     * Test the findMemberIdsByMembershipNumbers method
     */
    public function testFindMemberIdsByMembershipNumbers(): void
    {
        $repo = $this->getEntityManager()->getRepository(MemberEntry::class);

        // Check that an empty list is returned when we pass an empty list
        $this->assertCount(0, $repo->findMemberIdsByMembershipNumbers([]));

        // Check the result for a membership number with multiple members
        /** @var MemberEntry[] $members */
        $members = $this->getEntityManager()->getRepository(MemberEntry::class)->findBy([], ['id' => 'ASC']);
        $result = $repo->findMemberIdsByMembershipNumbers([$members[0]->getMembershipNumber()->getMembershipNumber()]);
        $this->assertCount(4, $result);
        $expected[] = $members[0]->getId();
        $expected[] = $members[9]->getId();
        $expected[] = $members[10]->getId();
        $expected[] = $members[11]->getId();
        $this->assertEquals($expected, $result);

        // Check the order of the result
        $result = $repo->findMemberIdsByMembershipNumbers([
            $members[8]->getMembershipNumber()->getMembershipNumber(),
            $members[5]->getMembershipNumber()->getMembershipNumber(),
            $members[0]->getMembershipNumber()->getMembershipNumber(),
        ]);
        $this->assertCount(6, $result);
        $expected = [];
        $expected[] = $members[0]->getId();
        $expected[] = $members[5]->getId();
        $expected[] = $members[8]->getId();
        $expected[] = $members[9]->getId();
        $expected[] = $members[10]->getId();
        $expected[] = $members[11]->getId();
        $this->assertEquals($expected, $result);
    }


    /**
     * Test getCountsPerStatus
     *
     * @throws Exception
     */
    public function testGetCountsPerStatus(): void
    {
        $repo = $this->getEntityManager()->getRepository(MemberEntry::class);

        $counts = $repo->getCountsPerStatus();

        $this->assertCount(2, $counts);
        $this->assertEquals(['Member' => 11, 'Future member' => 1], $counts);
    }


     /**
     * Test getCountsPerStatus
      *
      * @throws Exception
     */
    public function testGetCountsPerLanguage(): void
    {
        $repo = $this->getEntityManager()->getRepository(MemberEntry::class);

        $counts = $repo->getCountsPerLanguage();

        $this->assertCount(3, $counts);
        $this->assertEquals(['Dutch' => 10, 'English' => 1, 'German' => 1], $counts);
    }


    /**
     * Test getCountsPerStatus
     *
     * @throws Exception
     */
    public function testGetCountsPerMemberSince(): void
    {
        $repo = $this->getEntityManager()->getRepository(MemberEntry::class);

        $counts = $repo->getCountsPerMemberSince();

        $this->assertCount(3, $counts);
        $this->assertEquals(['2015' => 9, '2016' => 1, '' => 1], $counts);
    }


    /**
     * Test getCountsPerStatus
     *
     * @throws Exception
     */
    public function testGetCountsPerUseDirectDebit(): void
    {
        $repo = $this->getEntityManager()->getRepository(MemberEntry::class);

        $counts = $repo->getCountsPerUseDirectDebit();

        $this->assertCount(2, $counts);
        $this->assertEquals(['No' => 7, 'Yes' => 4], $counts);
    }


    /**
     * Check if the expected member are in the array by looking at the firstnames.
     *
     * @param int[]         $expectedNumbers List of expected firstname numbers
     * @param MemberEntry[] $foundMembers    List of found members
     */
    private function checkMembersInArray(array $expectedNumbers, array $foundMembers): void
    {
        $this->assertCount(count($expectedNumbers), $foundMembers);

        $index = 0;
        foreach ($foundMembers as $member) {
            $this->assertEquals('Firstname'.$expectedNumbers[$index++], $member->getFirstName());
        }
    }
}
