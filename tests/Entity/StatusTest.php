<?php

/*
 * Copyright (C) 2022 by Luc Hamers <vereniging@hamers.de>
 *
 * This file is part of Vereniging.
 *
 * Vereniging is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Vereniging is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Vereniging.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Tests\Entity;

use App\Entity\Status;
use App\Tests\TestCase;

/**
 * Class StatusTest
 */
class StatusTest extends TestCase
{
    /**
     * @inheritDoc
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->loadAllFixtures();
        $this->setLoggedInUserForLogging();
    }


    /**
     * Test the inUse method.
     */
    public function testIsInUse(): void
    {
        /** @var Status $status */
        $status = $this->getFixtureReference('status_member');
        $this->assertTrue($status->isInUse());
        $status = $this->getFixtureReference('status_future_member');
        $this->assertTrue($status->isInUse());
        $status = $this->getFixtureReference('status_former_member');
        $this->assertFalse($status->isInUse());
    }


    /**
     * Test if the findAll method in the repository class has been overloaded to return the values ordered ascending.
     */
    public function testFindAll(): void
    {
        $states = $this->getEntityManager()->getRepository(Status::class)->findAll();
        $this->assertEquals("Former member", $states[0]->getStatus());
        $this->assertEquals("Future member", $states[1]->getStatus());
        $this->assertEquals("Member", $states[2]->getStatus());
    }


    /**
     * Check if translations for the status table work correctly
     */
    public function testTranslation(): void
    {
        $status = $this->getFixtureReference('status_member');

        $this->assertEquals("Member", $status->getStatus());
        $this->assertEquals("Member", $status);
        $this->assertEquals("Member", $status->translate('en')->getStatusTranslated());
        $this->assertEquals("Lid", $status->translate('nl')->getStatusTranslated());
        $this->assertEquals("Mitglied", $status->translate('de')->getStatusTranslated());

        $status->addNewTranslation('es', 'Member Spanish');
        $this->getEntityManager()->flush();
        $this->getEntityManager()->refresh($status);
        $this->assertEquals('Member Spanish', $status->translate('es')->getStatusTranslated());
    }


    /**
     * Test if a default translation is created when a new object is created without one.
     */
    public function testDefaultTranslation(): void
    {
        $status = new Status();
        $status->setStatus('Testtype');
        $status->setHasMembershipNumber(false);
        $this->getEntityManager()->persist($status);
        $this->getEntityManager()->flush();
        $id = $status->getId();
        $this->assertEquals('Testtype', $status->getStatus());

        // Check if the translations are persisted correctly when reloading the object from the database
        $this->getEntityManager()->clear();
        $newType = $this->getEntityManager()->getRepository(Status::class)->findOneBy(['id' => $id]);
        $this->assertEquals('Testtype', $newType->getStatus());
    }


    /**
     * Test the findAllWithMembershipNumberQueryBuilder method
     */
    public function testCreateFindAllWithMembershipNumberQueryBuilder(): void
    {
        $states = $this->getEntityManager()->getRepository(Status::class)
            ->createFindAllWithMembershipNumberQueryBuilder('en')
            ->getQuery()->execute();

        $this->assertCount(3, $states);
        $this->assertEquals('Former member', $states[0]);
        $this->assertEquals('Future member', $states[1]);
        $this->assertEquals('Member', $states[2]);

        $states = $this->getEntityManager()->getRepository(Status::class)
            ->createFindAllWithMembershipNumberQueryBuilder('en', true)
            ->getQuery()->execute();

        $this->assertCount(1, $states);
        $this->assertEquals('Member', $states[0]);
    }
}
