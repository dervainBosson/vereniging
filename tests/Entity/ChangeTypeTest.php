<?php

/*
 * Copyright (C) 2022 by Luc Hamers <vereniging@hamers.de>
 *
 * This file is part of Vereniging.
 *
 * Vereniging is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Vereniging is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Vereniging.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Tests\Entity;

use App\Entity\ChangeType;
use App\Entity\MemberEntry;
use App\Tests\TestCase;

/**
 * Test for the change type entity.
 *
 */
class ChangeTypeTest extends TestCase
{
    /**
     * @inheritDoc
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->loadAllFixtures();
    }


    /**
     * This function tests adding and removing members to the many2many relationship.
     */
    public function testAddRemoveChangeTypes(): void
    {
        $changeType = $this->getEntityManager()->getRepository(ChangeType::class)
                                               ->findOneBy(['changeType' => "member data change"]);
        $member1 = $this->getEntityManager()->getRepository(MemberEntry::class)
                                            ->findOneBy(['firstName' => 'Firstname1']);
        $member2 = $this->getEntityManager()->getRepository(MemberEntry::class)
                                            ->findOneBy(['firstName' => 'Firstname2']);

        // Subscribe a member to the change type
        $changeType->addMemberEntry($member1);
        $this->getEntityManager()->flush();
        $this->assertCount(1, $member1->getEmailNotifications());
        $this->assertEquals('Member data change', $member1->getEmailNotifications()[0]);
        $this->assertCount(1, $changeType->getEmailNotificationMembers());
        $this->assertEquals('firstname1.lastname1', $changeType->getEmailNotificationMembers()[0]);

        // Check if adding a second time doesn't do any harm
        $changeType->addMemberEntry($member1);
        $this->getEntityManager()->flush();
        $this->assertCount(1, $member1->getEmailNotifications());
        $this->assertEquals('Member data change', $member1->getEmailNotifications()[0]);

        // Add a second member to the same change type
        $changeType->addMemberEntry($member2);
        $this->getEntityManager()->flush();
        $this->assertCount(1, $member1->getEmailNotifications());
        $this->assertCount(1, $member2->getEmailNotifications());
        $this->assertEquals('Member data change', $member1->getEmailNotifications()[0]);
        $this->assertEquals('Member data change', $member2->getEmailNotifications()[0]);
        $this->assertCount(2, $changeType->getEmailNotificationMembers());
        $this->assertEquals('firstname1.lastname1', $changeType->getEmailNotificationMembers()[0]);
        $this->assertEquals('firstname2.lastname2', $changeType->getEmailNotificationMembers()[1]);

        // Check if removing the change type works correctly
        $changeType->removeMemberEntry($member1);
        $this->getEntityManager()->flush();
        $this->assertCount(0, $member1->getEmailNotifications());
        $this->assertCount(1, $changeType->getEmailNotificationMembers());
        $this->assertEquals('firstname2.lastname2', $changeType->getEmailNotificationMembers()[1]);

        // Check if removing a second time doesn't do any harm
        $changeType->removeMemberEntry($member1);
        $this->getEntityManager()->flush();
        $this->assertCount(0, $member1->getEmailNotifications());
        $this->assertCount(1, $changeType->getEmailNotificationMembers());
        $this->assertEquals('firstname2.lastname2', $changeType->getEmailNotificationMembers()[1]);

        $changeType->removeMemberEntry($member2);
        $this->getEntityManager()->flush();
        $this->assertCount(0, $member2->getEmailNotifications());
        $this->assertCount(0, $changeType->getEmailNotificationMembers());
    }


    /**
     * Check if translations for the change type table work correctly
     */
    public function testTranslation(): void
    {
        $changeType = $this->getEntityManager()->getRepository(ChangeType::class)
                                          ->findOneBy(['changeType' => "member data change"]);

        $this->assertEquals("Member data change", $changeType->getChangeType());
        $this->assertEquals("Member data change", $changeType);
        $this->assertEquals("Member data change", $changeType->translate('en')->getChangeTypeTranslated());
        $this->assertEquals("Verandering ledengegevens", $changeType->translate('nl')->getChangeTypeTranslated());
        $this->assertEquals("Änderung Mitgliederdaten", $changeType->translate('de')->getChangeTypeTranslated());
    }


    /**
     * Test if a default translation is created when a new object is created without one.
     */
    public function testDefaultTranslation(): void
    {
        $changeType = new ChangeType();
        $changeType->setChangeType('Testtype');
        $this->getEntityManager()->persist($changeType);
        $this->getEntityManager()->flush();
        $id = $changeType->getId();
        $this->assertEquals('Testtype', $changeType->getChangeType());

        // Check if the translations are persisted correctly when reloading the object from the database
        $this->getEntityManager()->clear();
        $newType = $this->getEntityManager()->getRepository(ChangeType::class)->findOneBy(['id' => $id]);
        $this->assertEquals('Testtype', $newType->getChangeType());

        // Removed test type, so the table is as before this test
        $this->getEntityManager()->remove($newType);
        $this->getEntityManager()->flush();
    }


    /**
     * Test the create find all query builder from the repository class
     */
    public function testCreateFindAllQueryBuilder(): void
    {
        $changeTypes = $this->getEntityManager()->getRepository(ChangeType::class)
                                                ->createFindAllQueryBuilder()->getQuery()->execute();

        $this->assertCount(12, $changeTypes);
        $this->assertEquals('Error', $changeTypes[0]);
        $this->assertEquals('Export action', $changeTypes[1]);
        $this->assertEquals('Member data change', $changeTypes[6]);
        $this->assertEquals('System settings change', $changeTypes[11]);
    }
}
