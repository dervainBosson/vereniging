<?php

/*
 * Copyright (C) 2022 by Luc Hamers <vereniging@hamers.de>
 *
 * This file is part of Vereniging.
 *
 * Vereniging is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Vereniging is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Vereniging.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Tests\Entity;

use App\Entity\Language;
use App\Entity\LogfileEntry;
use App\Entity\MemberEntry;
use App\Entity\SerialLetter;
use App\Entity\SerialLetterContent;
use App\Repository\SerialLetterContentRepository;
use App\Tests\TestCase;
use DateTime;
use Exception;

/**
 * Class SerialLetterTest
 */
class SerialLetterTest extends TestCase
{
    /**
     * @inheritDoc
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->loadAllFixtures();
        $this->setLoggedInUserForLogging();
    }


    /**
     * Test if findAll orders the letters correctly. To check the order, set the letter created last to the oldest date.
     *
     * @throws Exception
     */
    public function testFindAll(): void
    {
        /** @var SerialLetter $letter2 */
        $letter2 = $this->getFixtureReference('serial_letter2');
        $letter2->setChangeDate(new DateTime('-3 days'));
        $this->getEntityManager()->flush();

        $letters = $this->getEntityManager()->getRepository(SerialLetter::class)->findAll();

        $langEN = $this->getEntityManager()->getRepository(Language::class)->findOneBy(['locale' => 'en']);
        $this->assertEquals('Serial letter1', $letters[0]->getTitle($langEN));
        $this->assertEquals('Serial letter3', $letters[1]->getTitle($langEN));
        $this->assertEquals('Serial letter2', $letters[2]->getTitle($langEN));
    }


    /**
     * Test if create date and change date are set automatically.
     *
     * @throws Exception
     */
    public function testCreateChangeDateUpdate(): void
    {
        $serialLetter = new SerialLetter();

        // Check that both date fields are empty on creation
        $this->assertNull($serialLetter->getCreateDate());
        $this->assertNull($serialLetter->getChangeDate());

        // Create and persist a serial letter object
        /** @var MemberEntry $member1 */
        $member1 = $this->getFixtureReference('member_entry_last_name1');
        $serialLetter->setSignatureUseTitle(false);
        $serialLetter->setFirstSignatureMember($member1);
        $serialLetter->setLastUpdatedBy($member1);

        $now = new DateTime('now');
        $this->getEntityManager()->persist($serialLetter);
        $this->getEntityManager()->flush();

        // Check that both time stamps are set
        $this->assertNotNull($serialLetter->getCreateDate());
        $this->assertNotNull($serialLetter->getChangeDate());

        // Check that both time stamps less than or equal to 1s after calling flush
        $this->assertLessThanOrEqual(1, $serialLetter->getCreateDate()->getTimestamp() - $now->getTimestamp());
        $this->assertLessThanOrEqual(1, $serialLetter->getChangeDate()->getTimestamp() - $now->getTimestamp());

        $origCreate = $serialLetter->getCreateDate();
        sleep(2);
        $serialLetter->setSignatureUseTitle(true);
        $this->getEntityManager()->flush();

        // The create time stamp must remain the same
        $this->assertEquals($origCreate, $serialLetter->getCreateDate());
        $this->assertLessThanOrEqual(3, $serialLetter->getChangeDate()->getTimestamp() - $origCreate->getTimestamp());
    }


    /**
     * Test if getTitle returns the title in the correct language.
     */
    public function testGetTitle(): void
    {
        /** @var SerialLetter $letter */
        $letter = $this->getFixtureReference('serial_letter1');

        $langEN = $this->getFixtureReference('language_english');
        $langNL = $this->getFixtureReference('language_dutch');
        $langDE = $this->getFixtureReference('language_german');
        $langES = $this->getFixtureReference('language_spanish');

        $this->assertEquals('Serial letter1', $letter->getTitle($langEN));
        $this->assertEquals('Seriebrief1', $letter->getTitle($langNL));
        $this->assertEquals('Serial letter1', $letter->getTitle($langDE));
        $this->assertEquals('Serial letter1', $letter->getTitle($langES));

        $letter = new SerialLetter();
        $this->assertEquals('', $letter->getTitle($langEN));
        $this->assertEquals('', $letter->getTitle($langNL));
        $this->assertEquals('', $letter->getTitle($langDE));
        $this->assertEquals('', $letter->getTitle($langES));
    }


    /**
     *
     */
    public function testGetTitles(): void
    {
        /** @var SerialLetter $letter */
        $letter = $this->getFixtureReference('serial_letter1');

        $this->assertEquals('en: Serial letter1; nl: Seriebrief1', $letter->getTitles());
    }


    /**
     * Check if the change date of the serial letter is updated when the content changes. To check this, the change date
     * of the serial letter is set back before executing the test.
     *
     * @throws Exception
     */
    public function testUpdateChangeDateForContent(): void
    {
        /** @var SerialLetterContent $content */
        $content = $this->getFixtureReference('serial_letter_content11');

        // Change the title
        $this->changeSerialLetterContentAndCheckChangeDate($content, 'setLetterTitle', 'Updated title');
        // Change the content
        $this->changeSerialLetterContentAndCheckChangeDate($content, 'setContent', 'Updated content');
        // Change the serial letter
        $this->changeSerialLetterContentAndCheckChangeDate($content, 'setSerialLetter', $content->getSerialLetter());
        // Change the language
        $this->changeSerialLetterContentAndCheckChangeDate($content, 'setLanguage', $content->getLanguage());
    }


    /**
     * Test if content objects can be added correctly.
     */
    public function testAddContent(): void
    {
        $letter = new SerialLetter();
        $letter->setSignatureUseTitle(false);
        /** @var MemberEntry $member1 */
        $member1 = $this->getFixtureReference('member_entry_last_name1');
        $letter->setLastUpdatedBy($member1);

        $languages = $this->getEntityManager()->getRepository(Language::class)->findAllForSerialLetters();
        $this->assertCount(0, $letter->getSerialLetterContents());

        foreach ($languages as $language) {
            $content = new SerialLetterContent();
            $letter->addSerialLetterContent($content);
            $content->setLanguage($language);
            $this->getEntityManager()->persist($content);
        }

        $this->getEntityManager()->flush();
        $this->assertCount(2, $letter->getSerialLetterContents());
    }


    /**
     * Test if the content is also deleted when the letter it points to is removed.
     */
    public function testRemoveLetter(): void
    {
        /** @var SerialLetterContentRepository $contentRepo */
        $contentRepo = $this->getEntityManager()->getRepository(SerialLetterContent::class);
        /** @var SerialLetter $letter */
        $letter = $contentRepo->findOneBy(['letterTitle' => 'Serial letter3'])->getSerialLetter();

        // Removing the serial letter should also remove the content entries which point to this letter.
        $this->getEntityManager()->remove($letter);
        $this->getEntityManager()->flush();

        $content = $contentRepo->findOneBy(['letterTitle' => 'Serial letter3']);
        $this->assertNull($content);
        $content = $contentRepo->findOneBy(['letterTitle' => 'Seriebrief3']);
        $this->assertNull($content);
    }


    /**
     * Test if changes to a serial letter are logged correctly
     */
    public function testChangeListener(): void
    {
        $this->clearAllLogFiles();

        // Create
        $letter = new SerialLetter();
        $letter->setSignatureUseTitle(false);
        /** @var MemberEntry $member1 */
        $member1 = $this->getFixtureReference('member_entry_last_name1');
        /** @var MemberEntry $member2 */
        $member2 = $this->getFixtureReference('member_entry_last_name2');

        $letter->setLastUpdatedBy($member1);
        $letter->setFirstSignatureMember($member1);
        $letter->setSecondSignatureMember($member2);

        /** @var Language[] $languages */
        $languages = $this->getEntityManager()->getRepository(Language::class)->findAllForSerialLetters();
        $this->assertCount(0, $letter->getSerialLetterContents());

        foreach ($languages as $language) {
            $content = new SerialLetterContent();
            $letter->addSerialLetterContent($content);
            $content->setLanguage($language);
            $content->setLetterTitle('title '.$language->getLocale());
            $content->setContent('content '.$language->getLocale());
            $this->getEntityManager()->persist($content);
        }

        $this->getEntityManager()->flush();

        // Change
        $letter->setSignatureUseTitle(true);
        $letter->setLastUpdatedBy($member1);
        $letter->setFirstSignatureMember($member2);
        $letter->setSecondSignatureMember($member1);
        /** @var SerialLetterContent $content */
        foreach ($languages as $language) {
            $content = $letter->getContent($language);
            $content->setLetterTitle('new '.$content->getLetterTitle());
            $content->setContent('new '.$content->getContent());
        }
        $this->getEntityManager()->flush();

        // Delete
        $this->getEntityManager()->remove($letter);
        $this->getEntityManager()->flush();

        $logMessages = $this->getAllLogs();
        $this->assertEquals(3, count($logMessages));

        // Reverse order because of DESC!
        $this->checkComplexLogMessage($logMessages[2], 'Created new entry of type serial letter ', [
            'Title English' => ['title en'],
            'Title Dutch' => ['title nl'],

        ]);
        $this->checkComplexLogMessage($logMessages[1], 'Updated entry of type serial letter ', [
            'Title English' => ['title en', 'new title en'],
            'Title Dutch' => ['title nl', 'new title nl'],
            'Content English' => ['content en', 'new content en'],
            'Content Dutch' => ['content nl', 'new content nl'],
            'First signing member' => ['Firstname1 Lastname1', 'Firstname2 Lastname2'],
            'Second signing member' => ['Firstname2 Lastname2', 'Firstname1 Lastname1'],
            'Use titles in signature' => ['no', 'yes'],
        ]);
        $this->checkComplexLogMessage($logMessages[0], 'Deleted entry of type serial letter ', [
            'Title English' => ['new title en'],
            'Title Dutch' => ['new title nl'],

        ]);
    }


    /**
     * Test if changes to a serial letter are logged correctly when there is html content, which is handled in a special
     * way.
     */
    public function testChangeListenerWithHtml(): void
    {
        // Add some html content including variables to the serial letter
        /** @var SerialLetterContent $content */
        $content = $this->getFixtureReference('serial_letter_content11');
        $htmlContent = '<p>Second sentence with the variables ';
        $htmlContent .= '<span class="mceNonEditable" style="border: thin dotted grey;" data-variable="name">name</span> and ';
        $htmlContent .= '<span class="mceNonEditable" style="border: thin dotted grey;" data-variable="gender">gender</span> </p>';
        $htmlContent .= "<p style=\"bla\">Some text</p>";
        $content->setContent($htmlContent);
        $this->getEntityManager()->flush();
        $this->clearAllLogFiles();

        // Now do some changes to this text to test if the changes are shown correctly in the logs.
        $htmlContent = '<p>Second sentence with the variables ';
        $htmlContent .= '<span class="mceNonEditable" style="border: thin dotted grey;" data-variable="firstName">firstName</span> and ';
        $htmlContent .= '<span class="mceNonEditable" style="border: thin dotted grey;" data-variable="gender">gender</span> </p>';
        $htmlContent .= "<p style=\"bla\">Changed text</p>";
        $content->setContent($htmlContent);
        $content->setLetterTitle('bla');
        $this->getEntityManager()->flush();

        $logMessages = $this->getAllLogs();
        $this->assertEquals(1, count($logMessages));

        $expected = [
            'Content English' => [
                "Second sentence with the variables {{ <del>name</del> }} and {{ gender }} \n<del>Some</del> text",
                "Second sentence with the variables {{ <ins>firstName</ins> }} and {{ gender }} \n<ins>Changed</ins> text",
            ],
            'Title English' => ['Serial letter1', 'bla'],
        ];
        $this->checkComplexLogMessage($logMessages[0], 'Updated entry of type serial letter ', $expected);
    }


    /**
     * Check that changing any of the content fields also updates the change date of the referenced serial letter.
     *
     * @param SerialLetterContent $content Serial letter content to change
     * @param string              $method  Method name (i.e. set...) to call for the change
     * @param mixed               $value   Value to pass to the setter method.
     *
     * @throws Exception
     */
    private function changeSerialLetterContentAndCheckChangeDate(SerialLetterContent $content, string $method, $value): void
    {
        // Set change date of the current serial letter to 3 days in the past to be able to check updating the date to
        // today.
        $content->getSerialLetter()->setChangeDate(new DateTime('-3 days'));
        $this->getEntityManager()->persist($content->getSerialLetter());
        $this->getEntityManager()->flush();

        $content->$method($value);
        $this->getEntityManager()->flush();
        $this->getEntityManager()->refresh($content->getSerialLetter());
        $dateToday = new DateTime('now');
        $dateToday->setTime(0, 0);
        $this->assertEquals($dateToday, $content->getSerialLetter()->getChangeDate());
    }


    /**
     * @param LogfileEntry $logEntry
     * @param string       $expectedMessage
     * @param array        $expectedData
     */
    private function checkComplexLogMessage(LogfileEntry $logEntry, string $expectedMessage, array $expectedData): void
    {
        LogfileEntry::prepareTranslation($this->getEntityManager(), $this->getTranslator(), 'en');
        $complexLogEntry = $logEntry->getComplexLogEntry();
        $this->assertEquals($expectedMessage, $complexLogEntry->getMainMessage());
        $this->assertEquals($expectedData, $complexLogEntry->getDataAsArray());
    }
}
