<?php

/*
 * Copyright (C) 2022 by Luc Hamers <vereniging@hamers.de>
 *
 * This file is part of Vereniging.
 *
 * Vereniging is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Vereniging is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Vereniging.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Tests\Entity;

use App\Entity\CommitteeFunction;
use App\Tests\TestCase;

/**
 * Class CommitteeFunctionTest
 */
class CommitteeFunctionTest extends TestCase
{
    /**
     * @inheritDoc
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->loadAllFixtures();
        $this->setLoggedInUserForLogging();
    }


    /**
     * Test all getters and setters
     */
    public function testGettersSetters(): void
    {
        $committeeFunction = new CommitteeFunction();
        $committeeFunction->setCommitteeFunctionName('name');

        $this->assertEquals('name', $committeeFunction->getCommitteeFunctionName());
        $this->assertEquals('name', $committeeFunction);
        $this->assertNull($committeeFunction->getId());

        $this->getEntityManager()->persist($committeeFunction);
        $this->getEntityManager()->flush();
        $this->assertEquals('name', $committeeFunction->getCommitteeFunctionName());
        $this->assertNotNull($committeeFunction->getId());

        $this->getEntityManager()->remove($committeeFunction);
        $this->getEntityManager()->flush();
    }


    /**
     * Check if translations for the committee function table work correctly
     */
    public function testTranslation(): void
    {
        $function = $this->getEntityManager()->getRepository(CommitteeFunction::class)
                                             ->findOneBy(['committeeFunctionName' => "chairman"]);

        $this->assertEquals("Chairman", $function->getCommitteeFunctionName());
        $this->assertEquals("Chairman", $function);
        $this->assertEquals("Chairman", $function->translate('en')->getCommitteeFunctionNameTranslated());
        $this->assertEquals("Voorzitter", $function->translate('nl')->getCommitteeFunctionNameTranslated());
        $this->assertEquals("Vorsitzender", $function->translate('de')->getCommitteeFunctionNameTranslated());
    }


    /**
     * Test if a default translation is created when a new object is created without one.
     */
    public function testDefaultTranslation(): void
    {
        $function = new CommitteeFunction();
        $function->setCommitteeFunctionName('Test');
        $this->getEntityManager()->persist($function);
        $this->getEntityManager()->flush();
        $id = $function->getId();
        $this->assertEquals('Test', $function->getCommitteeFunctionName());

        // Check if the translations are persisted correctly when reloading the object from the database
        $this->getEntityManager()->clear();
        $newFunction = $this->getEntityManager()->getRepository(CommitteeFunction::class)->findOneBy(['id' => $id]);
        $this->assertEquals('Test', $newFunction->getCommitteeFunctionName());
    }
}
