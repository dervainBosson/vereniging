<?php

/*
 * Copyright (C) 2022 by Luc Hamers <vereniging@hamers.de>
 *
 * This file is part of Vereniging.
 *
 * Vereniging is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Vereniging is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Vereniging.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Tests\Entity;

use App\Entity\Address;
use App\Entity\MemberAddressMap;
use App\Entity\MemberEntry;
use App\Repository\MemberAddressMapRepository;
use App\Tests\TestCase;

/**
 * Class MemberAddressMapTest
 */
class MemberAddressMapTest extends TestCase
{
    /**
     * @inheritDoc
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->loadAllFixtures();
        $this->setLoggedInUserForLogging();
    }


    /**
     * Test all combinations of setting the main postal address.
     */
    public function testSettingMainAddress(): void
    {
        /** @var MemberEntry $member1 */
        $member1 = $this->getFixtureReference('member_entry_last_name1');
        $this->getEntityManager()->remove($member1->getMemberAddressMaps()[1]);
        $this->getEntityManager()->flush();

        // Before all, check if a safety mechanism for preventing a null pointer dereferencing works correctly
        $memberAddressMap = new MemberAddressMap();
        $memberAddressMap->ensureSingleMainAddressAdd();

        // Read the single address entry that exists. Here the main address flag must be set (it is not set by the
        // fixture, but by the lifecycle callback on first creation).
        /** @var MemberAddressMap[] $addresses */
        $addresses = $this->getMember1Addresses();

        $this->assertEquals(1, count($addresses));
        $this->assertTrue($addresses[0]->getIsMainAddress());

        // Now create a new entry where the main address flag is not set. Here the flag should not be set by the
        // lifecycle callback.
        $mapEntry1 = $this->addAddress($member1, false);
        // Check that adding the same member entry twice doesn't do any harm
        $mapEntry1->setMemberEntry($member1);

        /** @var MemberAddressMapRepository $repo */
        $repo = $this->getEntityManager()->getRepository(MemberAddressMap::class);
        $addresses = $repo->findBy(['memberEntry' => $member1]);
        $this->assertEquals(2, count($addresses));
        $this->assertTrue($addresses[0]->getIsMainAddress());
        $this->assertFalse($addresses[1]->getIsMainAddress());
        $this->assertFalse($mapEntry1->getIsMainAddress());

        // Now delete this entry. This should also not change anything on the other entry
        $this->getEntityManager()->remove($mapEntry1);
        $this->getEntityManager()->flush();
        $addresses = $repo->findBy(['memberEntry' => $member1]);
        $this->assertTrue(count($addresses) === 1);
        $this->assertTrue($addresses[0]->getIsMainAddress());

        // Now add an entry where the main address flag is set. This should delete the existing flag of the first entry
        // and keep that of the second.
        $mapEntry2 = $this->addAddress($member1, true);
        $addresses = $repo->findBy(['memberEntry' => $member1]);
        $this->assertEquals(2, count($addresses));
        $this->assertFalse($addresses[0]->getIsMainAddress());
        $this->assertTrue($addresses[1]->getIsMainAddress());
        $this->assertTrue($mapEntry2->getIsMainAddress());

        // When the main address entry is removed, then the main address flag should be set on the first other address
        // in the list.
        $this->getEntityManager()->remove($mapEntry2);
        $this->getEntityManager()->flush();
        $addresses = $repo->findBy(['memberEntry' => $member1]);
        $this->assertTrue(count($addresses) === 1);
        $this->assertTrue($addresses[0]->getIsMainAddress());

        // Check of the post remove callback doesn't create an error when the last entry is deleted.
        $mapEntry = $repo->findOneBy(['memberEntry' => $member1]);
        $this->getEntityManager()->remove($mapEntry);
        $this->getEntityManager()->flush();
    }


    /**
     * Generate and add an address entry
     *
     * @param MemberEntry $memberEntry
     * @param boolean     $mainAddress
     *
     * @return MemberAddressMap
     */
    private function addAddress(MemberEntry $memberEntry, bool $mainAddress): MemberAddressMap
    {
        $mapEntry = new MemberAddressMap();
        $newAddress = new Address();
        $newAddress->setAddress('new address');
        $newAddress->setZip('zip');
        $newAddress->setCity('new city');
        $newAddress->setAddressType($this->getFixtureReference("address_type_company"));
        $newAddress->setCountry($this->getFixtureReference("country_netherlands"));

        $mapEntry->setAddress($newAddress);
        $mapEntry->setMemberEntry($memberEntry);
        $mapEntry->setIsMainAddress($mainAddress);
        $this->getEntityManager()->persist($mapEntry);
        $this->getEntityManager()->flush();

        return ($mapEntry);
    }


    /**
     * @return array
     */
    private function getMember1Addresses(): array
    {
        $member1 = $this->getFixtureReference('member_entry_last_name1');
        $this->getEntityManager()->refresh($member1);
        $addresses = $this->getEntityManager()->getRepository(MemberAddressMap::class)
                                              ->findBy(['memberEntry' => $member1]);

        return $addresses;
    }
}
