<?php

/*
 * Copyright (C) 2022 by Luc Hamers <vereniging@hamers.de>
 *
 * This file is part of Vereniging.
 *
 * Vereniging is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Vereniging is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Vereniging.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Tests\Entity;

use App\Entity\Committee;
use App\Entity\CommitteeFunction;
use App\Entity\CommitteeFunctionMap;
use App\Entity\SerialLetter;
use App\Entity\SerialLetterContent;
use App\Tests\TestCase;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Class CommitteeFunctionMapTest
 */
class CommitteeFunctionMapTest extends TestCase
{
    /**
     * @inheritDoc
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->loadAllFixtures();
        $this->setLoggedInUserForLogging();
    }


    /**
     * Test getters and setters
     */
    public function testGet(): void
    {
        /** @var CommitteeFunction $function */
        $function = $this->getEntityManager()->getRepository(CommitteeFunction::class)->findOneBy([
            'committeeFunctionName' => 'chairman',
        ]);

        /** @var Committee $committee */
        $committee = $this->getEntityManager()->getRepository(Committee::class)->findOneBy([
            'committeeName' => 'board',
        ]);

        $committeeFunction = $this->getEntityManager()->getRepository(CommitteeFunctionMap::class)
                                  ->findOneBy(['committee' => $committee, 'committeeFunction' => $function]);
        $this->assertEquals('Chairman', $committeeFunction->getCommitteeFunction());
        $this->assertEquals('Board', $committeeFunction->getCommittee());
        $this->assertNotNull($committeeFunction->getId());
    }


    /**
     * Test deleting functions, committees and mapping entries and check if the orphan removal works correctly.
     */
    public function testOrphanRemoval(): void
    {
        // Delete a function and check, if all the mapping entries are deleted
        /** @var CommitteeFunction $function */
        $function = $this->getEntityManager()->getRepository(CommitteeFunction::class)
                                             ->findOneBy(['committeeFunctionName' => 'chairman']);
        // Check if the committee function map in the serial letter referencing this map entry is set to null
        /** @var SerialLetter $letter */
        $letter = $this->getEntityManager()->getRepository(SerialLetterContent::class)
                                           ->findOneBy(['letterTitle' => 'Serial letter1'])
                                           ->getSerialLetter();
        $this->assertNotNull($letter->getFirstSignatureMembersFunction());

        $committeeFunctions = $this->getEntityManager()->getRepository(CommitteeFunctionMap::class)->findAll();
        $this->assertEquals(7, count($committeeFunctions));
        $this->getEntityManager()->remove($function);
        $this->getEntityManager()->remove($letter->getFirstSignatureMembersFunction());
        $this->getEntityManager()->flush();
        $committeeFunctions = $this->getEntityManager()->getRepository(CommitteeFunctionMap::class)->findAll();
        $this->assertEquals(4, count($committeeFunctions));
        $this->getEntityManager()->refresh($letter);
        $this->assertNull($letter->getFirstSignatureMembersFunction());

        // Delete a committee and check, if all the mapping entries are deleted
        /** @var Committee $committee */
        $committee = $this->getEntityManager()->getRepository(Committee::class)->findOneBy([
            'committeeName' => 'board',
        ]);
        $this->getEntityManager()->remove($committee);
        $this->getEntityManager()->flush();
        $committeeFunctions = $this->getEntityManager()->getRepository(CommitteeFunctionMap::class)->findAll();
        $this->assertEquals(2, count($committeeFunctions));
    }


    /**
     *
     */
    public function testFindAll(): void
    {
        $committeeFunctionMaps = $this->getEntityManager()->getRepository(CommitteeFunctionMap::class)->findAll();
        $mapStrings = [];
        foreach ($committeeFunctionMaps as $committeeFunctionMap) {
            $mapStrings[] = (string) $committeeFunctionMap;
        }

        $expected  = '["Activities - Chairman","Activities - Member","Board - Chairman","Board - Member","Board - Vice chairman",';
        $expected .= '"Public relations - Chairman","Public relations - Member"]';
        $this->assertEquals($expected, json_encode($mapStrings));
    }


    /**
     * Test the method findOneByCommitteeFunction
     */
    public function testFindOneByCommitteeFunction(): void
    {
        $repository = $this->getEntityManager()->getRepository(CommitteeFunctionMap::class);
        $committeeFunctionMap = $repository->findOneByCommitteeFunction('');
        $this->assertNull($committeeFunctionMap);

        $committeeFunctionMap = $repository->findOneByCommitteeFunction('board member');
        $this->assertNull($committeeFunctionMap);

        $committeeFunctionMap = $repository->findOneByCommitteeFunction('board - member');
        $this->assertEquals('Board - Member', (string) $committeeFunctionMap);

        $committeeFunctionMap = $repository->findOneByCommitteeFunction('board - member - chairman');
        $this->assertNull($committeeFunctionMap);
    }


    /**
     * Test the __tostring method
     */
    public function testToString(): void
    {
        $committeeFunctionMaps = $this->getEntityManager()->getRepository(CommitteeFunctionMap::class)->findAll();

        $this->assertEquals(7, count($committeeFunctionMaps));
        $this->assertEquals("Activities - Chairman", (string) $committeeFunctionMaps[0]);
        $this->assertEquals("Activities - Member", (string) $committeeFunctionMaps[1]);
        $this->assertEquals("Board - Chairman", (string) $committeeFunctionMaps[2]);
        $this->assertEquals("Board - Member", (string) $committeeFunctionMaps[3]);
        $this->assertEquals("Board - Vice chairman", (string) $committeeFunctionMaps[4]);
        $this->assertEquals("Public relations - Chairman", (string) $committeeFunctionMaps[5]);
        $this->assertEquals("Public relations - Member", (string) $committeeFunctionMaps[6]);
    }


    /**
     * Test the getOrderedCommitteeFunctions method
     */
    public function testGetOrderedCommitteeFunctions(): void
    {
        $committeeFunctions = $this->getEntityManager()->getRepository(CommitteeFunctionMap::class)->getOrderedCommitteeFunctions(new ArrayCollection());
        $this->assertCount(0, $committeeFunctions);

        $searchMaps = new ArrayCollection();
        $searchMaps->add($this->getFixtureReference('committee_function_map_board_vice_chairman'));
        $searchMaps->add($this->getFixtureReference('committee_function_map_activities_member'));
        $searchMaps->add($this->getFixtureReference('committee_function_map_activities_chairman'));
        $searchMaps->add($this->getFixtureReference('committee_function_map_board_chairman'));
        $committeeFunctions = $this->getEntityManager()->getRepository(CommitteeFunctionMap::class)->getOrderedCommitteeFunctions($searchMaps);
        $this->assertCount(2, $committeeFunctions);
        $expected = [
            'Activities' => ['Chairman', 'Member'],
            'Board' => ['Chairman', 'Vice chairman'],
        ];
        $this->assertEquals($expected, $committeeFunctions);
    }
}
