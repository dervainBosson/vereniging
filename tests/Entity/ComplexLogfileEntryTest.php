<?php

/*
 * Copyright (C) 2022 by Luc Hamers <vereniging@hamers.de>
 *
 * This file is part of Vereniging.
 *
 * Vereniging is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Vereniging is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Vereniging.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Tests\Entity;

use App\Entity\AbstractComplexLogEntryBase;
use App\Entity\ComplexLogEntryDoubleDataField;
use App\Entity\ComplexLogEntrySingleDataField;
use App\Entity\LogEntryDataField;
use App\Tests\TestCase;
use Exception;
use InvalidArgumentException;

/**
 * Test for the address type entity.
 *
 */
class ComplexLogfileEntryTest extends TestCase
{
    /**
     * @inheritDoc
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->loadAllFixtures();
    }


    /**
     * Test working with complex log entries with a single data field
     */
    public function testEntryWithSingleDataField(): void
    {
        $testObject = new ComplexLogEntrySingleDataField();

        $testObject->setMainMessage('Login succeeded');
        $this->assertEquals('Login succeeded', $testObject->getMainMessage());
        $this->assertEquals(1, $testObject->getNumberOfDataFields());
        $this->assertEquals([], $testObject->getDataAsArray());

        $testObject->addDataField('my key1', ['my value1']);
        $testObject->addDataField('my key2', ['my value2']);
        $this->assertEquals(['my key1' => ['my value1'], 'my key2' => ['my value2']], $testObject->getDataAsArray());

        // Try if we can change the content when we get the data object
        $data = &$testObject->getDataAsArray(false);
        $data['my key1'] = ['my value3'];
        $this->assertEquals(['my key1' => ['my value3'], 'my key2' => ['my value2']], $testObject->getDataAsArray());
    }


    /**
     * Test working with complex log entries with a double data field
     */
    public function testEntryWithDoubleDataField(): void
    {
        $testObject = new ComplexLogEntryDoubleDataField();

        $testObject->setMainMessage('Login succeeded');
        $this->assertEquals('Login succeeded', $testObject->getMainMessage());
        $this->assertEquals(2, $testObject->getNumberOfDataFields());
        $this->assertEquals([], $testObject->getDataAsArray());

        $testObject->addDataField('my key1', ['my value1', 'my value2']);
        $testObject->addDataField('my key2', ['my value3', 'my value4']);
        $this->assertEquals(['my key1' => ['my value1', 'my value2'], 'my key2' => ['my value3', 'my value4']], $testObject->getDataAsArray());

        // Try if we can change the content when we get the data object
        $data = &$testObject->getDataAsArray(false);
        $data['my key1'] = ['my value5', 'my value6'];
        $this->assertEquals(['my key1' => ['my value5', 'my value6'], 'my key2' => ['my value3', 'my value4']], $testObject->getDataAsArray());
    }


    /**
     * Test removing of data field entries
     */
    public function testRemoveDataField(): void
    {
        // Test removing from a single data field object
        $testObject = new ComplexLogEntrySingleDataField();
        try {
            $testObject->removeDataField('search key');
            $this->fail("Expected exception did not occur");
        } catch (Exception $e) {
            $this->assertEquals("Array key 'search key' not found in data array!", $e->getMessage());
        }

        $testObject->addDataField('my key1', ['my value1']);
        $testObject->addDataField('my key2', ['my value2']);

        $testObject->removeDataField('my key1');
        $this->assertEquals(['my key2' => ['my value2']], $testObject->getDataAsArray());

        // Test removing from a double data field object
        $testObject = new ComplexLogEntryDoubleDataField();
        try {
            $testObject->removeDataField('search key');
            $this->fail("Expected exception did not occur");
        } catch (Exception $e) {
            $this->assertEquals("Array key 'search key' not found in data array!", $e->getMessage());
        }

        $testObject->addDataField('my key1', ['my value1', 'my value2']);
        $testObject->addDataField('my key2', ['my value3', 'my value4']);

        $testObject->removeDataField('my key1');
        $this->assertEquals(['my key2' => ['my value3', 'my value4']], $testObject->getDataAsArray());
    }


    /**
     * Test if the main message is translated correctly
     */
    public function testTranslationOfMainMessage(): void
    {
        $testObject = new ComplexLogEntrySingleDataField();
        $this->translationOfMainMessageTests($testObject);

        $testObject = new ComplexLogEntryDoubleDataField();
        $this->translationOfMainMessageTests($testObject);
    }


    /**
     * Test if all fields are translated correctly.
     */
    public function testTranslationsWithSingleDataField(): void
    {
        $testObject = new ComplexLogEntrySingleDataField();
        $testObject->prepareTranslation($this->getEntityManager(), $this->getTranslator(), 'en');

        // Now check if the data field keys are translated and then are ordered alphabetically
        $testObject->addDataField('gender', ['my value1']);
        $testObject->addDataField('companyinformation', ['my value2']);
        // Capitals should not be ordered to the top
        $testObject->addDataField('Membership number', ['my value2']);
        $this->assertEquals(['company information' => ['my value2'], 'gender' => ['my value1'], 'Membership number' => ['my value2']], $testObject->getDataAsArray());
        // Now check if the data is translated to German and ordered according to the German translations
        $testObject->prepareTranslation($this->getEntityManager(), $this->getTranslator(), 'de');
        $this->assertEquals(['Firmeninformation' => ['my value2'], 'Geschlecht' => ['my value1'], 'Mitgliedsnummer' => ['my value2']], $testObject->getDataAsArray());

        // Check if the data field is translated correctly
        $testObject = new ComplexLogEntrySingleDataField();
        $testObject->addDataField('gender', [new LogEntryDataField('Male', true)]);
        $testObject->prepareTranslation($this->getEntityManager(), $this->getTranslator(), 'en');
        $this->assertEquals(['gender' => ['Male']], $testObject->getDataAsArray());
        $testObject->prepareTranslation($this->getEntityManager(), $this->getTranslator(), 'de');
        $this->assertEquals(['Geschlecht' => ['Männlich']], $testObject->getDataAsArray());
    }


    /**
     * Test if all fields are translated correctly.
     */
    public function testTranslationsWithDoubleDataFieldS(): void
    {
        $testObject = new ComplexLogEntryDoubleDataField();
        $testObject->prepareTranslation($this->getEntityManager(), $this->getTranslator(), 'en');

        // Now check if the data field keys are translated and then are ordered alphabetically
        $testObject->addDataField('gender', ['my value1', 'my value2']);
        $testObject->addDataField('companyinformation', ['my value3', 'my value4']);
        $this->assertEquals(['company information' => ['my value3', 'my value4'], 'gender' => ['my value1', 'my value2']], $testObject->getDataAsArray());
        // Now check if the data is translated to German and ordered according to the German translations
        $testObject->prepareTranslation($this->getEntityManager(), $this->getTranslator(), 'de');
        $this->assertEquals(['Firmeninformation' => ['my value3', 'my value4'], 'Geschlecht' => ['my value1', 'my value2']], $testObject->getDataAsArray());

        // Check if the data field is translated correctly
        $testObject = new ComplexLogEntryDoubleDataField();
        $testObject->addDataField('gender', [new LogEntryDataField('Male', true), new LogEntryDataField('Female', true)]);
        $testObject->prepareTranslation($this->getEntityManager(), $this->getTranslator(), 'en');
        $this->assertEquals(['gender' => ['Male', 'Female']], $testObject->getDataAsArray());
        $testObject->prepareTranslation($this->getEntityManager(), $this->getTranslator(), 'de');
        $this->assertEquals(['Geschlecht' => ['Männlich', 'Weiblich']], $testObject->getDataAsArray());
    }


    /**
     * Test of the number of data fields is checked
     */
    public function testEntryWithSingleDataFieldWithException(): void
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage('Data contains 2 fields, should contain 1!');

        $testObject = new ComplexLogEntrySingleDataField();
        $testObject->addDataField('my key1', ['my value1', 'my faulty second value']);
    }


    /**
     * Test of the number of data fields is checked
     */
    public function testEntryWithDoubleDataFieldWithException(): void
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage('Data contains 1 fields, should contain 2!');

        $testObject = new ComplexLogEntryDoubleDataField();
        $testObject->addDataField('my key1', ['my value1']);
    }


    /**
     * Test if the object can be serialised correctly
     *
     * @throws Exception
     */
    public function testSerialisationWithSingleDataField(): void
    {
        $testObject = new ComplexLogEntrySingleDataField();

        // Check if the object is serialised correctly
        $testObject->setMainMessage('Login succeeded');
        $testObject->addDataField('my key', ['my value']);
        $this->assertEquals('{"numberOfDataFields":1,"mainMessage":"Login succeeded","dataArray":{"my key":["my value"]}}', $testObject->serialize());

        // Check if the object initialisation with the serialised object works correctly
        $serializedObject = new ComplexLogEntrySingleDataField($testObject->serialize());
        $this->assertEquals('Login succeeded', $serializedObject->getMainMessage());
        $this->assertEquals(['my key' => ['my value']], $serializedObject->getDataAsArray());
        $this->assertCount(1, $serializedObject->getDataAsArray());

        // Check if serialisation with the data field object works correctly
        $testObject = new ComplexLogEntrySingleDataField();
        $testObject->addDataField('gender', [new LogEntryDataField('Male', true)]);
        $this->assertEquals('{"numberOfDataFields":1,"mainMessage":"","dataArray":{"gender":[{"className":null,"dbIndex":null,"defaultString":"Male","translate":true}]}}', $testObject->serialize());

        $serializedObject = new ComplexLogEntrySingleDataField($testObject->serialize());
        $serializedObject->prepareTranslation($this->getEntityManager(), $this->getTranslator(), 'en');
        $this->assertEquals(['gender' => ['Male']], $serializedObject->getDataAsArray());
        $serializedObject->prepareTranslation($this->getEntityManager(), $this->getTranslator(), 'de');
        $this->assertEquals(['Geschlecht' => ['Männlich']], $serializedObject->getDataAsArray());
    }


    /**
     * Test if the object can be serialised correctly
     *
     * @throws Exception
     */
    public function testSerialisationWithDoubleDataField(): void
    {
        $testObject = new ComplexLogEntryDoubleDataField();

        // Check if the object is serialised correctly
        $testObject->setMainMessage('Login succeeded');
        $testObject->addDataField('my key', ['my value1', 'my value2']);
        $this->assertEquals('{"numberOfDataFields":2,"mainMessage":"Login succeeded","dataArray":{"my key":["my value1","my value2"]}}', $testObject->serialize());

        // Check if the object initialisation with the serialised object works correctly
        $serializedObject = new ComplexLogEntryDoubleDataField($testObject->serialize());
        $this->assertEquals('Login succeeded', $serializedObject->getMainMessage());
        $this->assertEquals(['my key' => ['my value1', 'my value2']], $serializedObject->getDataAsArray());
        $this->assertCount(1, $serializedObject->getDataAsArray());

        // Check if serialisation with the data field object works correctly
        $testObject = new ComplexLogEntryDoubleDataField();
        $testObject->addDataField('gender', [new LogEntryDataField('Male', true), new LogEntryDataField('Female', true)]);
        $this->assertEquals('{"numberOfDataFields":2,"mainMessage":"","dataArray":{"gender":[{"className":null,"dbIndex":null,"defaultString":"Male","translate":true},{"className":null,"dbIndex":null,"defaultString":"Female","translate":true}]}}', $testObject->serialize());

        $serializedObject = new ComplexLogEntryDoubleDataField($testObject->serialize());
        $serializedObject->prepareTranslation($this->getEntityManager(), $this->getTranslator(), 'en');
        $this->assertEquals(['gender' => ['Male', 'Female']], $serializedObject->getDataAsArray());
        $serializedObject->prepareTranslation($this->getEntityManager(), $this->getTranslator(), 'de');
        $this->assertEquals(['Geschlecht' => ['Männlich', 'Weiblich']], $serializedObject->getDataAsArray());
    }


    /**
     * Test if the correct object is returned depending on the number of data fields.
     */
    public function testSerialisationFactory(): void
    {
        $testObject = new ComplexLogEntrySingleDataField();
        $testObject->addDataField('key', ['bla']);
        $serialisedObject = AbstractComplexLogEntryBase::createComplexLogEntry($testObject->serialize());
        $this->assertEquals('App\Entity\ComplexLogEntrySingleDataField', get_class($serialisedObject));

        $testObject = new ComplexLogEntryDoubleDataField();
        $testObject->addDataField('key', ['bla', 'bla']);
        $serialisedObject = AbstractComplexLogEntryBase::createComplexLogEntry($testObject->serialize());
        $this->assertEquals('App\Entity\ComplexLogEntryDoubleDataField', get_class($serialisedObject));
    }


    /**
     * Test if the factory throws the expected exception when the json string is corrupted
     */
    public function testSerialisationFactoryWithExceptionCorruptJsonData(): void
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage('Object cannot be created, json data is corrupted!');

        AbstractComplexLogEntryBase::createComplexLogEntry('bla');
    }


    /**
     * Test if the factory throws the expected exception when the serialised object is no AbstractComplexLogEntryBase.
     */
    public function testSerialisationFactoryWithExceptionWrongObject1(): void
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage('Object cannot be created, json data doesn\'t contain the correct object!');

        AbstractComplexLogEntryBase::createComplexLogEntry(json_encode('bla'));
    }


    /**
     * Test if the factory throws the expected exception when the serialised object is no AbstractComplexLogEntryBase.
     */
    public function testSerialisationFactoryWithExceptionWrongObject2(): void
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage('Object cannot be created, json data doesn\'t contain the correct object!');

        AbstractComplexLogEntryBase::createComplexLogEntry(json_encode(['numberOfDataFields1' => 1, 'mainMessage' => 'bla', 'dataArray' => []]));
    }


    /**
     * Test if the factory throws the expected exception when the serialised object is no AbstractComplexLogEntryBase.
     */
    public function testSerialisationFactoryWithExceptionWrongObject3(): void
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage('Object cannot be created, json data doesn\'t contain the correct object!');

        AbstractComplexLogEntryBase::createComplexLogEntry(json_encode(['numberOfDataFields' => 1, 'mainMessage1' => 'bla', 'dataArray' => []]));
    }


    /**
     * Test if the factory throws the expected exception when the serialised object is no AbstractComplexLogEntryBase.
     */
    public function testSerialisationFactoryWithExceptionWrongObject4(): void
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage('Object cannot be created, json data doesn\'t contain the correct object!');

        AbstractComplexLogEntryBase::createComplexLogEntry(json_encode(['numberOfDataFields' => 1, 'mainMessage' => 'bla', 'dataArray1' => []]));
    }


    /**
     * Test if the factory throws the expected exception when the serialised object is no AbstractComplexLogEntryBase.
     */
    public function testSerialisationFactoryWithExceptionWrongObjectNumberOfDataFields(): void
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage('Object cannot be created with number of data fields of 3!');

        AbstractComplexLogEntryBase::createComplexLogEntry(json_encode(['numberOfDataFields' => 3, 'mainMessage' => 'bla', 'dataArray' => []]));
    }


    /**
     * Test if corrupted json data generates the correct exception
     */
    public function testObjectCreationWithSingleDataFieldWithCorruptedJsonException(): void
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage('Object cannot be created, json data is corrupted!');

        new ComplexLogEntrySingleDataField('faulty data');
    }


    /**
     * Test if corrupted json data generates the correct exception
     */
    public function testObjectCreationWithDoubleDataFieldWithCorruptedJsonException(): void
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage('Object cannot be created, json data is corrupted!');

        new ComplexLogEntrySingleDataField('faulty data');
    }


    /**
     * Test if corrupted json data generates the correct exception
     */
    public function testObjectCreationWithSingleDataFieldWithIncorrectColumnCountException(): void
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage('Object cannot be created, json data contains 2 fields, should contain 1!');

        new ComplexLogEntrySingleDataField('{"mainMessage":"Login succeeded","dataArray":{"my key1":["my value1","my value2"],"my key2":["my value3","my value4"]}}');
    }


    /**
     * Test if corrupted json data generates the correct exception
     */
    public function testObjectCreationWithDoubleDataFieldWithIncorrectColumnCountException(): void
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage('Object cannot be created, json data contains 1 fields, should contain 2!');

        new ComplexLogEntryDoubleDataField('{"mainMessage":"Login succeeded","dataArray":{"my key1":["my value1"],"my key2":["my value2"]}}');
    }


    /**
     * Test if the main message is translated correctly
     *
     * @param AbstractComplexLogEntryBase $testObject
     */
    private function translationOfMainMessageTests(AbstractComplexLogEntryBase $testObject): void
    {
        $testObject->setMainMessage('Login succeeded');

        $testObject->prepareTranslation($this->getEntityManager(), $this->getTranslator(), 'en');
        // Check if the message is translated correctly into English, which is default locale
        $this->assertEquals('Login succeeded', $testObject->getMainMessage());
        // Now check if the message is translated to German
        $testObject->prepareTranslation($this->getEntityManager(), $this->getTranslator(), 'de');
        $this->assertEquals('Anmeldung erfolgreich', $testObject->getMainMessage());

        // For entity changes, there are main messages in a special format. Check if these get processed correctly
        $testObject->setMainMessage('Created new %classname% entry %stringIdentifier%|mailinglist,(my new mailinglist)');
        $this->assertEquals('Neuer Eintrag vom Typ Emailverteiler erzeugt (my new mailinglist)', $testObject->getMainMessage());
        // And the same test for English
        $testObject->prepareTranslation($this->getEntityManager(), $this->getTranslator(), 'en');
        $this->assertEquals('Created new entry of type mailing list (my new mailinglist)', $testObject->getMainMessage());
    }
}
