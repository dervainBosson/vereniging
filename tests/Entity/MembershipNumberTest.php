<?php

/*
 * Copyright (C) 2022 by Luc Hamers <vereniging@hamers.de>
 *
 * This file is part of Vereniging.
 *
 * Vereniging is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Vereniging is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Vereniging.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Tests\Entity;

use App\Entity\MembershipNumber;
use App\Tests\Service\LogEventsDisabler;
use App\Tests\TestCase;
use DateInterval;
use DateTimeImmutable;
use Exception;

/**
 * Test the membership number entity
 */
class MembershipNumberTest extends TestCase
{
    /**
     * @inheritDoc
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->loadAllFixtures();
        $this->setLoggedInUserForLogging();
    }


    /**
     * Test the reading the outstanding debt
     */
    public function testGetOutstandingDebt(): void
    {
        /** @var MembershipNumber $membershipNumber1 */
        $membershipNumber1 = $this->getFixtureReference('membership_number_1');
        $this->assertEqualsWithDelta(88, $membershipNumber1->getOutstandingDebt(), 0.5);

        $membershipNumber2 = $this->getFixtureReference('membership_number_5');
        $this->assertEqualsWithDelta(0, $membershipNumber2->getOutstandingDebt(), 0.5);
    }


    /**
     * Test if getMembershipFeeTransactions returns the transactions in the right order
     */
    public function testGetMembershipFeeTransactions(): void
    {
        /** @var MembershipNumber $membershipNumber */
        $membershipNumber = $this->getFixtureReference('membership_number_1');
        $transactions = $membershipNumber->getMembershipFeeTransactions();
        $this->assertCount(3, $transactions);
        $compareDate = new DateTimeImmutable();
        foreach ($transactions as $transaction) {
            $this->assertLessThan($compareDate, $transaction->getCreateDate());
            $compareDate = $transaction->getCreateDate();
        }
    }


    /**
     * Test getLastTransactionDate
     *
     * @throws Exception
     */
    public function testGetLastTransactionDate(): void
    {
        /** @var MembershipNumber $membershipNumber */
        $membershipNumber = $this->getFixtureReference('membership_number_1');
        // Read the date of the newest open transaction, which should return the create date. We set the create date of
        // this transaction here by hand, to be able to check against it in this test
        $compareDate = (new DateTimeImmutable())->modify("- 1 day");
        $membershipNumber->getMembershipFeeTransactions()[0]->setCreateDate($compareDate);
        $this->assertEquals($compareDate, $membershipNumber->getLastTransactionDate());

        // Now close the transaction, to return today as last transaction date
        $compareDate = new DateTimeImmutable();
        $membershipNumber->getMembershipFeeTransactions()[1]->setCloseDate($compareDate);
        $this->assertEquals($compareDate, $membershipNumber->getLastTransactionDate());

        // Read the last date from a number without transactions, which should return null
        /** @var MembershipNumber $membershipNumber */
        $membershipNumber = $this->getFixtureReference('membership_number_5');
        $this->assertNull($membershipNumber->getLastTransactionDate());
    }
}
