<?php

/*
 * Copyright (C) 2022 by Luc Hamers <vereniging@hamers.de>
 *
 * This file is part of Vereniging.
 *
 * Vereniging is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Vereniging is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Vereniging.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Tests\Entity;

use App\Entity\ChangeType;
use App\Entity\ComplexLogEntryDoubleDataField;
use App\Entity\LogfileEntry;
use App\Entity\MemberEntry;
use App\Tests\TestCase;
use DateTime;
use Exception;

/**
 * Test for the address type entity.
 *
 */
class LogfileEntryTest extends TestCase
{
    private ?MemberEntry $defaultMemberEntry;
    private ?ChangeType $defaultChangeType;


    /**
     * @inheritDoc
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->loadAllFixtures();
        $this->defaultChangeType = $this->getFixtureReference('change_type_member_data_change');
        $this->defaultMemberEntry = $this->getFixtureReference('member_entry_last_name1');
    }


    /**
     * Test auto timestamping. The date field is set automatically on saving the object
     */
    public function testSetTimestamp(): void
    {
        $previousTimestamp = new DateTime("now");
        $logfileEntry = new LogfileEntry();
        $logfileEntry->setChangedByMember($this->defaultMemberEntry);
        $logfileEntry->setChangeType($this->defaultChangeType);
        $logfileEntry->setLogentry("Test entry");

        $this->getEntityManager()->persist($logfileEntry);
        $this->getEntityManager()->flush();

        // Storage date must be later than start of test
        $this->assertGreaterThanOrEqual($previousTimestamp, $logfileEntry->getDate());
        // Storage date must be before this assert
        $this->assertGreaterThanOrEqual($logfileEntry->getDate(), new DateTime('now'));

        // Now check if the date is updated when the entry is updated. This should not be the case
        $oldTimestamp = $logfileEntry->getDate();
        sleep(1);
        $logfileEntry->setLogentry("Test entry1");
        $this->getEntityManager()->persist($logfileEntry);
        $this->getEntityManager()->flush();
        $this->assertEquals($oldTimestamp, $logfileEntry->getDate());
    }


    /**
     * Test storing and reading complex log entries
     */
    public function testComplexLogentry(): void
    {
        $logfileEntry = new LogfileEntry();
        $logfileEntry->setChangedByMember($this->defaultMemberEntry);
        $logfileEntry->setChangeType($this->defaultChangeType);
        $this->assertFalse($logfileEntry->isComplexLogentry());
        $this->assertNull($logfileEntry->getComplexLogEntry());
        $complexEntry = new ComplexLogEntryDoubleDataField();
        $complexEntry->addDataField('Name', ['old name', 'new name']);
        $complexEntry->addDataField('First name', ['old first name', 'new first name']);
        $logfileEntry->setLogentry($complexEntry);
        $this->getEntityManager()->persist($logfileEntry);
        $this->getEntityManager()->flush();
        $entryId = $logfileEntry->getId();

        $expected  = '{"numberOfDataFields":2,"mainMessage":"","dataArray":{"Name":["old name","new name"],';
        $expected .=  '"First name":["old first name","new first name"]}}';
        $this->assertEquals($expected, $logfileEntry->getLogentry());
        $this->assertTrue($logfileEntry->isComplexLogentry());

        // Now load back the complex entry and check if the complex flag is set correctly
        $logfileEntry1 = $this->getEntityManager()->getRepository(LogfileEntry::class)->find($entryId);
        $this->assertEquals($expected, $logfileEntry1->getLogentry());
        $this->assertTrue($logfileEntry1->isComplexLogentry());

        // Check if writing a simple message will clear the complex flag
        $logfileEntry->setLogentry("Bla");
        $this->assertFalse($logfileEntry->isComplexLogentry());
        // Check if writing a complex message directly will enable the flag.
        $logfileEntry->setLogentry($expected);
        $this->assertTrue($logfileEntry->isComplexLogentry());
    }


    /**
     * Test if the complex column count returns the correct number
     */
    public function testComplexEntryColumnCount(): void
    {
        $logfileEntry = new LogfileEntry();
        $logfileEntry->setChangedByMember($this->defaultMemberEntry);
        $logfileEntry->setChangeType($this->defaultChangeType);

        $this->assertEquals(0, $logfileEntry->getComplexColumnCount());
        $complexEntry = new ComplexLogEntryDoubleDataField();
        $complexEntry->addDataField('Name', ['old name', 'new name']);
        $complexEntry->addDataField('First name', ['old first name', 'new first name']);
        $logfileEntry->setLogentry($complexEntry);
        $this->assertEquals(2, $logfileEntry->getComplexColumnCount());
        $this->getEntityManager()->persist($logfileEntry);
        $this->getEntityManager()->flush();
    }


    /**
     * Test if the data row count is calculated correctly
     */
    public function testDataRowCount(): void
    {
        $logfileEntry = new LogfileEntry();
        $logfileEntry->setChangedByMember($this->defaultMemberEntry);
        $logfileEntry->setChangeType($this->defaultChangeType);

        // Test initialisation
        $this->assertEquals(0, $logfileEntry->getDataRowCount());

        // Test if the value is updated on set
        $logfileEntry->setLogentry('');
        $this->assertEquals(1, $logfileEntry->getDataRowCount());

        // Test if the value is still correct after storing it to the database
        $this->getEntityManager()->persist($logfileEntry);
        $this->getEntityManager()->flush();
        $this->getEntityManager()->refresh($logfileEntry);
        $this->assertEquals(1, $logfileEntry->getDataRowCount());

        // Test of the value is calculated correctly for a complex log entry on set
        $complexEntry = new ComplexLogEntryDoubleDataField();
        $complexEntry->addDataField('Name', ['old name', 'new name']);
        $complexEntry->addDataField('First name', ['old first name', 'new first name']);
        $logfileEntry->setLogentry($complexEntry);
        $this->assertEquals(3, $logfileEntry->getDataRowCount());

        // And now also after storing it to the database
        $this->getEntityManager()->persist($logfileEntry);
        $this->getEntityManager()->flush();
        // Set the data row count to 1 to check if the post load event initialises the value correctly
        $logfileEntry->setLogentry('');
        $this->getEntityManager()->refresh($logfileEntry);
        $this->assertEquals(3, $logfileEntry->getDataRowCount());

        // Test if the value is calculated correctly when the complex log value contains multiple lines (i.e. on serial
        // letter changes)
        $complexEntry->addDataField('Name', ["old name1\nold name2", "new name1\nnew name2"]);
        $complexEntry->addDataField('First name', ['old first name', 'new first name']);
        $logfileEntry->setLogentry($complexEntry);
        $this->assertEquals(4, $logfileEntry->getDataRowCount());
    }


    /**
     * Test of the values are translated correctly
     */
    public function testTranslation(): void
    {
        $logfileEntry = new LogfileEntry();
        $complexEntry = new ComplexLogEntryDoubleDataField();
        $complexEntry->addDataField('Name', ['old name', 'new name']);
        $complexEntry->addDataField('First name', ['old first name', 'new first name']);
        $logfileEntry->setLogentry($complexEntry);

        // Test the translation of complex entries without translator (this should do nothing to the key names)
        $this->assertEquals(['Name' => ['old name', 'new name'], 'First name' => ['old first name', 'new first name']], $logfileEntry->getComplexLogEntry()->getDataAsArray());
        LogfileEntry::prepareTranslation($this->getEntityManager(), $this->getTranslator(), 'nl');
        // Test the same WITH translator
        $this->assertEquals(['Naam' => ['old name', 'new name'], 'Voornaam' => ['old first name', 'new first name']], $logfileEntry->getComplexLogEntry()->getDataAsArray());

        // Test the translation of simple log messages with variables
        $logfileEntry->setLogentry('Created new %classname% entry %stringIdentifier%|My class name, identifier');
        $this->assertEquals('Nieuw element van type My class name ingevoegd  identifier', $logfileEntry->getLogentry());

        // Test the translation of simple log messages with variables, when there is a comma in the content of the
        // variable (which has to be escaped in the parameter)
        $logfileEntry->setLogentry('Created new %classname% entry %stringIdentifier%|My \,class name, identifier');
        $this->assertEquals('Nieuw element van type My ,class name ingevoegd  identifier', $logfileEntry->getLogentry());
    }


    /**
     * Test the findAllByParameters method in the repository class.
     *
     * @throws Exception
     */
    public function testFindAllByParameters1(): void
    {
        $repo = $this->getEntityManager()->getRepository(LogfileEntry::class);

        // Test maxCount
        $this->assertEquals(10, count($repo->findAllByParameters(10, null, null, null, null, null)));
        $this->assertEquals(34, count($repo->findAllByParameters(50, null, null, null, null, null)));

        // Test changeTyp
        $logfileEntries = $repo->findAllByParameters(10, $this->defaultChangeType, null, null, null, null);
        $this->assertEquals(10, count($logfileEntries));
        /** @var LogfileEntry $logfileEntry */
        foreach ($logfileEntries as $logfileEntry) {
            $this->assertEquals('Member data change', $logfileEntry->getChangeType()->getChangeType());
        }

        // Test daysPast
        $logfileEntries = $repo->findAllByParameters(100, null, 31, null, null, null);
        foreach ($logfileEntries as $logfileEntry) {
            $this->assertGreaterThanOrEqual((new DateTime())->modify("-31 day")->modify('-2 minute'), $logfileEntry->getDate());
        }

        // Test searchByWhom
        $logfileEntries = $repo->findAllByParameters(100, null, null, null, '1', null);
        $this->assertEquals(6, count($logfileEntries));
        foreach ($logfileEntries as $logfileEntry) {
            $this->assertStringContainsString('firstname1.lastname1', $logfileEntry->getChangedByMember()->getUsername());
        }
        $logfileEntries = $repo->findAllByParameters(100, null, null, null, '2', null);
        $this->assertEquals(28, count($logfileEntries));
        foreach ($logfileEntries as $logfileEntry) {
            $this->assertStringContainsString('firstname2.lastname2', $logfileEntry->getChangedByMember()->getUsername());
        }
    }


    /**
     * Test the findAllByParameters method in the repository class.
     *
     * @throws Exception
     */
    public function testFindAllByParameters2(): void
    {
        $repo = $this->getEntityManager()->getRepository(LogfileEntry::class);

        // Test searchOnWho
        $logfileEntries = $repo->findAllByParameters(100, null, null, null, null, '1');
        $this->assertEquals(28, count($logfileEntries));
        /** @var LogfileEntry $logfileEntry */
        foreach ($logfileEntries as $logfileEntry) {
            $this->assertStringContainsString('firstname1.lastname1', $logfileEntry->getChangesOnMember()->getUsername());
        }
        $logfileEntries = $repo->findAllByParameters(100, null, null, null, null, '2');
        $this->assertEquals(0, count($logfileEntries));
        foreach ($logfileEntries as $logfileEntry) {
            $this->assertStringContainsString('firstname2.lastname2', $logfileEntry->getChangesOnMember()->getUsername());
        }

        // Test searchWhat
        $logfileEntries = $repo->findAllByParameters(100, null, null, '5', null, null);
        $this->assertEquals(3, count($logfileEntries));
        foreach ($logfileEntries as $logfileEntry) {
            $this->assertStringContainsString('5', $logfileEntry->getLogentry());
        }
        $logfileEntries = $repo->findAllByParameters(100, null, null, '0', null, null);
        $this->assertEquals(3, count($logfileEntries));
        foreach ($logfileEntries as $logfileEntry) {
            $this->assertStringContainsString('0', $logfileEntry->getLogentry());
        }

        // Test if the oldest entry is at the bottom
        $logfileEntries = $repo->findAllByParameters(100, null, 31, null, null, null);
        $lastDate = (new DateTime())->modify("+1 day");
        foreach ($logfileEntries as $logfileEntry) {
            $this->assertLessThanOrEqual($lastDate, $logfileEntry->getDate());
            $lastDate = $logfileEntry->getDate();
        }
    }


    /**
     * Test if setting and reading of the editable flag works correctly
     */
    public function testEditable(): void
    {
        $logfileEntry = new LogfileEntry();
        // This is needed, because by default, the field is null and it is set to false in the php class.
        $this->assertFalse($logfileEntry->isEditable());
        $logfileEntry->setEditable(true);
        $this->assertTrue($logfileEntry->isEditable());
    }
}
