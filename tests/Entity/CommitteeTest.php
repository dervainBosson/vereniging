<?php

/*
 * Copyright (C) 2022 by Luc Hamers <vereniging@hamers.de>
 *
 * This file is part of Vereniging.
 *
 * Vereniging is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Vereniging is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Vereniging.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Tests\Entity;

use App\Entity\Committee;
use App\Entity\CommitteeFunction;
use App\Entity\CommitteeFunctionMap;
use App\Tests\TestCase;

/**
 * Class CommitteeTest
 */
class CommitteeTest extends TestCase
{
    /**
     * @inheritDoc
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->loadAllFixtures();
        $this->setLoggedInUserForLogging();
    }


    /**
     * Test all getters and setters
     */
    public function testGettersSetters(): void
    {
        $committee = new Committee();
        $committee->setCommitteeName('name');

        $this->assertEquals('name', $committee->getCommitteeName());
        $this->assertEquals('name', $committee);
        $this->assertNull($committee->getId());

        $this->getEntityManager()->persist($committee);
        $this->getEntityManager()->flush();
        $this->assertEquals('name', $committee->getCommitteeName());
        $this->assertNotNull($committee->getId());

        $this->getEntityManager()->remove($committee);
        $this->getEntityManager()->flush();
    }


    /**
     * Test for the findAll method in the repo class, which orders by committee name.
     */
    public function testFindAll(): void
    {
        $committees = $this->getEntityManager()->getRepository(Committee::class)->findAll();
        $this->assertEquals(3, count($committees));
        $this->assertEquals('Activities', $committees[0]);
        $this->assertEquals('Board', $committees[1]);
        $this->assertEquals('Public relations', $committees[2]);
    }


    /**
     * Test adding and removing functions to the one-to-many side of the map.
     */
    public function testAddRemoveFunction(): void
    {
        /** @var CommitteeFunction $function */
        $function = $this->getEntityManager()->getRepository(CommitteeFunction::class)
                                             ->findOneBy(['committeeFunctionName' => 'vice chairman']);
        /** @var Committee $committee */
        $committee = $this->getEntityManager()->getRepository(Committee::class)
                                              ->findOneBy(['committeeName' => 'activities']);

        // Create a new mapping entry, where only the function is set
        $committeeFunctionMap = new CommitteeFunctionMap();
        $committeeFunctionMap->setCommitteeFunction($function);
        $committee->addCommitteeFunctionMap($committeeFunctionMap);
        // This tests the cascade persist in the committee entity
        $this->getEntityManager()->persist($committee);
        $this->getEntityManager()->flush();

        // Read back the mapping entry. Here the committee should have been set
        $mappingCheck = $this->getEntityManager()->getRepository(CommitteeFunctionMap::class)
                                                 ->findOneBy(['committeeFunction' => $function, 'committee' => $committee]);
        $this->assertNotNull($mappingCheck);
        $this->assertEquals($committee, $mappingCheck->getCommittee());
        $this->assertEquals($function, $mappingCheck->getCommitteeFunction());

        // Add the same again and check, if this is ignored
        $committeeFunctionMapDuplicate = new CommitteeFunctionMap();
        $committeeFunctionMapDuplicate->setCommitteeFunction($function);
        $committee->addCommitteeFunctionMap($committeeFunctionMapDuplicate);
        $this->getEntityManager()->persist($committee);
        $this->getEntityManager()->flush();

        // Read back the mapping entry. Here the committee should have been set
        $mappingChecks = $this->getEntityManager()->getRepository(CommitteeFunctionMap::class)
                                                  ->findBy(['committeeFunction' => $function, 'committee' => $committee]);
        $this->assertNotNull($mappingChecks);
        $this->assertEquals(1, count($mappingChecks));

        // Remove mapping entry from committee. This should also remove the mapping entry from the database.
        $committeeFunctionMap = new CommitteeFunctionMap();
        $committeeFunctionMap->setCommitteeFunction($function);
        $committee->removeCommitteeFunctionMap($committeeFunctionMap);
        $this->getEntityManager()->persist($committee);
        $this->getEntityManager()->flush();
        // Read back the mapping entry. This should not be found
        $mappingChecks = $this->getEntityManager()->getRepository(CommitteeFunctionMap::class)
                                                  ->findBy(['committeeFunction' => $function, 'committee' => $committee]);
        $this->assertEquals(0, count($mappingChecks));

        // Remove a second time. This should do nothing.
        $committeeFunctionMapDuplicate = new CommitteeFunctionMap();
        $committeeFunctionMapDuplicate->setCommitteeFunction($function);
        $committee->removeCommitteeFunctionMap($committeeFunctionMapDuplicate);
        $this->getEntityManager()->persist($committee);
        $this->getEntityManager()->flush();
    }


    /**
     * Test getAllCommitteeInformation method.
     */
    public function testGetAllCommitteeInformation(): void
    {
        $committees = $this->getEntityManager()->getRepository(Committee::class)->getAllCommitteeInformation();

        // Check if all three committees are present
        $this->assertEquals(3, count($committees));
        $this->assertArrayHasKey('Board', $committees);
        $this->assertArrayHasKey('Activities', $committees);
        $this->assertArrayHasKey('Public relations', $committees);

        // Check if every committee has the needed functions
        $this->assertEquals(3, count($committees['Board']));
        $this->assertArrayHasKey('Chairman', $committees['Board']);
        $this->assertArrayHasKey('Vice chairman', $committees['Board']);
        $this->assertArrayHasKey('Member', $committees['Board']);
        $this->assertEquals(2, count($committees['Activities']));
        $this->assertArrayHasKey('Chairman', $committees['Activities']);
        $this->assertArrayHasKey('Member', $committees['Activities']);
        $this->assertEquals(2, count($committees['Public relations']));
        $this->assertArrayHasKey('Chairman', $committees['Public relations']);
        $this->assertArrayHasKey('Member', $committees['Public relations']);

        // Check if for every function the correct member with its user name is entered or an empty array is attached
        $this->assertEquals(1, count($committees['Board']['Chairman']));
        $this->assertArrayHasKey('Firstname1 Lastname1', $committees['Board']['Chairman']);
        $this->assertEquals($committees['Board']['Chairman']['Firstname1 Lastname1'], 'firstname1.lastname1');
        $this->assertEquals(1, count($committees['Board']['Vice chairman']));
        $this->assertEquals(1, count($committees['Board']['Member']));

        $this->assertEquals(1, count($committees['Activities']['Chairman']));
        $this->assertArrayHasKey('Firstname2 Lastname2', $committees['Activities']['Chairman']);
        $this->assertEquals('firstname2.lastname2', $committees['Activities']['Chairman']['Firstname2 Lastname2']);
        $this->assertEquals(1, count($committees['Activities']['Member']));
        $this->assertArrayHasKey('Firstname1 Lastname1', $committees['Activities']['Member']);
        $this->assertEquals('firstname1.lastname1', $committees['Activities']['Member']['Firstname1 Lastname1']);

        $this->assertEquals(0, count($committees['Public relations']['Chairman']));
        $this->assertEquals(2, count($committees['Public relations']['Member']));
        $this->assertArrayHasKey('Firstname1 Lastname1', $committees['Public relations']['Member']);
        $this->assertEquals('firstname1.lastname1', $committees['Public relations']['Member']['Firstname1 Lastname1']);
        $this->assertArrayHasKey('Firstname2 Lastname2', $committees['Public relations']['Member']);
        $this->assertEquals('firstname2.lastname2', $committees['Public relations']['Member']['Firstname2 Lastname2']);

        // Do some of the same checks, but now with academic titles.
        $committees = $this->getEntityManager()->getRepository(Committee::class)->getAllCommitteeInformation(true);
        $this->assertEquals('firstname2.lastname2', $committees['Activities']['Chairman']['Firstname2 Lastname2 M.A.']);
        $this->assertEquals('firstname1.lastname1', $committees['Activities']['Member']['Dipl.-Ing. Firstname1 Lastname1']);
    }


    /**
     * Check if translations for the committee table work correctly
     */
    public function testTranslation(): void
    {
        $committee = $this->getEntityManager()->getRepository(Committee::class)
                                              ->findOneBy(['committeeName' => "board"]);

        $this->assertEquals("Board", $committee->getCommitteeName());
        $this->assertEquals("Board", $committee);
        $this->assertEquals("Board", $committee->translate('en')->getCommitteeNameTranslated());
        $this->assertEquals("Bestuur", $committee->translate('nl')->getCommitteeNameTranslated());
        $this->assertEquals("Vorstand", $committee->translate('de')->getCommitteeNameTranslated());
    }


    /**
     * Test if a default translation is created when a new object is created without one.
     */
    public function testDefaultTranslation(): void
    {
        $committee = new Committee();
        $committee->setCommitteeName('Test');
        $this->getEntityManager()->persist($committee);
        $this->getEntityManager()->flush();
        $id = $committee->getId();
        $this->assertEquals('Test', $committee->getCommitteeName());

        // Check if the translations are persisted correctly when reloading the object from the database
        $this->getEntityManager()->clear();
        $newCommittee = $this->getEntityManager()->getRepository(Committee::class)->findOneBy(['id' => $id]);
        $this->assertEquals('Test', $newCommittee->getCommitteeName());
    }


    /**
     * Test the createFindAllOrderedQueryBuilder method
     */
    public function testCreateFindAllOrderedQueryBuilder(): void
    {
        $committees = $this->getEntityManager()->getRepository(Committee::class)
            ->createFindAllOrderedQueryBuilder('en')
            ->getQuery()->execute();

        /** @var Committee[] $committees */
        $this->assertCount(3, $committees);
        $this->assertEquals('Activities', $committees[0]->getCommitteeName());
        $this->assertEquals('Board', $committees[1]->getCommitteeName());
        $this->assertEquals('Public relations', $committees[2]->getCommitteeName());
    }
}
