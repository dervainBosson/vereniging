<?php

/*
 * Copyright (C) 2022 by Luc Hamers <vereniging@hamers.de>
 *
 * This file is part of Vereniging.
 *
 * Vereniging is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Vereniging is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Vereniging.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Tests\Entity;

use App\Entity\MailingList;
use App\Entity\MemberEntry;
use App\Tests\TestCase;
use Doctrine\ORM\QueryBuilder;

/**
 * Class MailingListTest
 */
class MailingListTest extends TestCase
{
    /**
     * @inheritDoc
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->loadAllFixtures();
        $this->setLoggedInUserForLogging();
    }


    /**
     * Test writing and reading data.
     */
    public function testSetData(): void
    {
        $list = new MailingList();
        $list->setListName("new list");
        $list->setManagementPassword("my password");
        $list->setManagementEmail("list@email.org");
        $this->getEntityManager()->persist($list);
        $this->getEntityManager()->flush();

        $this->getEntityManager()->refresh($list);
        $this->assertGreaterThan(0, $list->getId());
        $this->assertEquals('new list', $list->getListName());
        $this->assertEquals('my password', $list->getManagementPassword());
        $this->assertEquals('list@email.org', $list->getManagementEmail());
    }


    /**
     * Test adding and removing member entries to and from a mailing list.
     */
    public function testAddRemoveMemberEntries(): void
    {
        /** @var MemberEntry $member */
        $member = $this->getEntityManager()->getRepository(MemberEntry::class)
                                      ->findOneBy(['username' => 'firstname2.lastname2']);
        /** @var MailingList $list */
        $list = $this->getEntityManager()->getRepository(MailingList::class)
                                    ->findOneBy(['listName' => 'MailingList_2']);

        // Add entry to the list side
        $list->addMemberEntry($member);
        $this->getEntityManager()->persist($list);
        $this->getEntityManager()->flush();
        $this->assertEquals(1, $list->getMemberEntries()->count());
        $this->assertEquals(1, $member->getMailingLists()->count());

        // Add the same entry a second time. This should not blow up
        $list->addMemberEntry($member);
        $this->getEntityManager()->persist($list);
        $this->getEntityManager()->flush();
        $this->assertEquals(1, $member->getMailingLists()->count());
        $this->assertEquals(1, $list->getMemberEntries()->count());

        // Remove entry from the list side
        $list->removeMemberEntry($member);
        $this->getEntityManager()->persist($list);
        $this->getEntityManager()->flush();
        $this->assertEquals(0, $list->getMemberEntries()->count());
        $this->assertEquals(0, $member->getMailingLists()->count());

        // Remove the same entry a second time. This should not blow up.
        $list->removeMemberEntry($member);
        $this->getEntityManager()->persist($list);
        $this->getEntityManager()->flush();
        $this->assertEquals(0, $list->getMemberEntries()->count());
        $this->assertEquals(0, $member->getMailingLists()->count());
    }


    /**
     *
     */
    public function testFindAllQuery(): void
    {
        /** @var QueryBuilder $query */
        $query = $this->getEntityManager()->getRepository(MailingList::class)
                                            ->createFindAllQueryBuilder();
        $mailingLists = $query->getQuery()->execute();
        $this->assertCount(3, $mailingLists);
        $this->assertEquals('MailingList_1', $mailingLists[0]);
        $this->assertEquals('MailingList_2', $mailingLists[1]);
        $this->assertEquals('MailingList_3', $mailingLists[2]);
    }
}
