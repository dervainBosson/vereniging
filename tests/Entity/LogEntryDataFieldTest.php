<?php

/*
 * Copyright (C) 2022 by Luc Hamers <vereniging@hamers.de>
 *
 * This file is part of Vereniging.
 *
 * Vereniging is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Vereniging is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Vereniging.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Tests\Entity;

use App\Entity\LogEntryDataField;
use App\Entity\Status;
use App\Tests\TestCase;
use Exception;
use InvalidArgumentException;

/**
 * Test for the log entry data field entity
 */
class LogEntryDataFieldTest extends TestCase
{
    /**
     * @inheritDoc
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->loadAllFixtures();
    }


    /**
     * Test if the default value is returned
     *
     * @throws Exception
     */
    public function testGetDefaultString(): void
    {
        $testObject = new LogEntryDataField('default value', false);
        $this->assertEquals('default value', $testObject);

        $testObject = new LogEntryDataField('default value', true);
        $this->assertEquals('default value', $testObject);

        $status = new Status();
        $status->setStatus('my test status');
        $testObject = new LogEntryDataField($status, false);
        $this->assertEquals('my test status', $testObject);

        $testObject = new LogEntryDataField($status, true);
        $this->assertEquals('my test status', $testObject);
    }


    /**
     * Test if the right exception is thrown when the object is created with a faulty data type.
     *
     * @throws Exception
     */
    public function testWrongDatatypeException(): void
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage('Value 0 is neither an object nor a string!');

        new LogEntryDataField(0.0, false);
    }


    /**
     * Test if the database object is hydrated correctly
     *
     * @throws Exception
     */
    public function testEntityHydration(): void
    {
        $status = $this->getEntityManager()->getRepository(Status::class)->findOneBy([
            'status' => 'member',
        ]);
        $testObject = new LogEntryDataField($status, false);
        // Read value back without hydration, i.e. check default value
        $this->assertEquals('Member', $testObject);
        // Read back value after hydration with the default locale, without translation
        $testObject->prepareTranslation($this->getEntityManager(), $this->getTranslator(), 'de');
        $this->assertEquals('Member', (string) $testObject);

        // Now do the same WITH the translation flag set
        $testObject = new LogEntryDataField($status, true);
        // Read value back without hydration, i.e. check default value
        $this->assertEquals('Member', $testObject);
        // Set translation prerequisites and check if the value is translated correctly
        $testObject->prepareTranslation($this->getEntityManager(), $this->getTranslator(), 'de');
        $this->assertEquals('Mitglied', (string) $testObject);
    }


    /**
     * Test if the plain strings are translated correctly.
     *
     * @throws Exception
     */
    public function testStringTranslation(): void
    {
        $testObject = new LogEntryDataField('Female', false);
        // Test when the translator is not set
        $this->assertEquals('Female', $testObject);
        // Set translation prerequisites and check if the value is still not translated
        $testObject->prepareTranslation($this->getEntityManager(), $this->getTranslator(), 'de');
        $this->assertEquals('Female', $testObject);

        $testObject = new LogEntryDataField('Female', true);
        // Test when the translator is not set
        $this->assertEquals('Female', $testObject);
        // Set translation prerequisites and check if the value is translated correctly
        $testObject->prepareTranslation($this->getEntityManager(), $this->getTranslator(), 'de');
        $this->assertEquals('Weiblich', $testObject);
    }


    /**
     * Test if the object is serialised correctly
     *
     * @throws Exception
     */
    public function testSerialisation(): void
    {
        $testObject = new LogEntryDataField('Female', false);
        $this->assertEquals('{"className":null,"dbIndex":null,"defaultString":"Female","translate":false}', json_encode($testObject));

        $status = $this->getEntityManager()->getRepository(Status::class)->findOneBy([
            'status' => 'member',
        ]);
        $status->setCurrentLocale('en');
        $testObject = new LogEntryDataField($status, true);
        $this->assertEquals('{"className":"App\\\\Entity\\\\Status","dbIndex":'.$status->getId().',"defaultString":"Member","translate":true}', json_encode($testObject));

        $newObject = new LogEntryDataField(json_encode($testObject));
        $this->assertEquals('Member', (string) $newObject);
        $newObject->prepareTranslation($this->getEntityManager(), $this->getTranslator(), 'de');
        $this->assertEquals('Mitglied', $newObject);
    }


    /**
     * Test if construction throws the correct exception when initialising with a json string AND translate is set
     */
    public function testSerialisationWithExceptionTranslateSet(): void
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage('Translate must be null when passing a json string!');

        new LogEntryDataField('{"className":null,"dbIndex":null,"defaultString":"Female","translate":false}', true);
    }


    /**
     * Test if construction throws the correct exception when initialising with a json string AND the json string is not
     * correct
     */
    public function testSerialisationWithExceptionJsonInvalid(): void
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage('Parameter dbIndex is missing in json string!');

        new LogEntryDataField('{"className":null,"dbIndex1":null,"defaultString":"Female","translate":false}');
    }
}
