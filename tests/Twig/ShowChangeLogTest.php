<?php

/*
 * Copyright (C) 2022 by Luc Hamers <vereniging@hamers.de>
 *
 * This file is part of Vereniging.
 *
 * Vereniging is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Vereniging is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Vereniging.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Tests\Twig;

use App\Tests\TestCase;
use App\Twig\ShowChangeLog;
use Exception;

/**
 * Class ShowChangeLogTest
 */
class ShowChangeLogTest extends TestCase
{
    private string $temporaryFile = '';


    /**
     * @inheritDoc
     */
    public function setUp(): void
    {
        $this->temporaryFile = '/tmp/'.uniqid('change_log_test_').'.yaml';
        parent::setUp();
    }


    /**
     * @inheritDoc
     */
    public function tearDown(): void
    {
        unlink($this->temporaryFile);
        parent::tearDown();
    }


    /**
     * Test if file location check works correctly
     */
    public function testFileChecks(): void
    {
        try {
            new ShowChangeLog('', 'a');

            $this->fail('Expected exception not thrown.');
        } catch (Exception $e) {
            $this->assertEquals('Cannot load change log yaml file /a!', $e->getMessage());
        }

        file_put_contents($this->temporaryFile, '%');
        try {
            new ShowChangeLog('', $this->temporaryFile);

            $this->fail('Expected exception not thrown.');
        } catch (Exception $e) {
            $this->assertEquals('The reserved indicator "%" cannot start a plain scalar; you need to quote the scalar at line 1 (near "%").', $e->getMessage());
        }
    }


    /**
     * Test the output with an empty file
     *
     * @throws Exception
     */
    public function testInitWithEmptyFile(): void
    {
        file_put_contents($this->temporaryFile, '');
        $testObject = new ShowChangeLog('', $this->temporaryFile);

        $this->assertEquals('-', $testObject->showChangeLog());
    }


    /**
     * Test the output with an empty file
     *
     * @throws Exception
     */
    public function testNormalOutput(): void
    {
        $yaml = <<<yaml
        '0.1':
            migration:
                - First entry
                - Second entry
            data:
                - ['10', 1.1.2020, '52', 'Fixed bug 1']
                - ['20', 1.2.2020, '13', 'Fixed bug 2']
                - ['30', 2.2.2020, '', 'Fixed bug 3']
                - ['40', 3.3.2020, '21', 'Fixed bug 4']
        '0.3':
            data:
                - ['50', 1.4.2020, '15', 'Fixed bug 5']
                - ['60', 2.4.2020, '', 'Fixed bug 6']
            
                - ['70', 23.4.2020, '', 'Fixed bug 7']
        yaml;
        file_put_contents($this->temporaryFile, $yaml);
        $testObject = new ShowChangeLog('', $this->temporaryFile);
        $changeLog = $testObject->showChangeLog();
        $this->assertStringContainsString('<h4>Version 0.1.10</h4>', $changeLog);
        $this->assertStringContainsString('<h4>Version 0.3.50</h4>', $changeLog);
        $this->assertStringContainsString('<h6>Update information:</h6>', $changeLog);
        $this->assertEquals(1, substr_count($changeLog, '<h6>'));
        $this->assertEquals(1, substr_count($changeLog, '<h6>'));
        $this->assertEquals(2, substr_count($changeLog, '<li>'));
        $this->assertStringContainsString('<li>First entry</li>', $changeLog);
        $this->assertStringContainsString('<li>Second entry</li>', $changeLog);
        $this->assertEquals(2, substr_count($changeLog, '<table>'));
        $this->assertEquals(2, substr_count($changeLog, '<tr><th>Buildnumber&nbsp;&nbsp;</th><th>Date</th><th>&nbsp;&nbsp;Issue</th><th>Change</th></tr>'));
        $this->assertStringContainsString('>10</td>', $changeLog);
        $this->assertStringContainsString('>01.01.2020</td>', $changeLog);
        $this->assertStringContainsString('(#52)', $changeLog);
        $this->assertStringContainsString('<td>Fixed bug 1</td>', $changeLog);
    }
}
