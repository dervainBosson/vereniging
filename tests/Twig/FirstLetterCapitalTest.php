<?php

/*
 * Copyright (C) 2022 by Luc Hamers <vereniging@hamers.de>
 *
 * This file is part of Vereniging.
 *
 * Vereniging is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Vereniging is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Vereniging.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Tests\Twig;

use App\Twig\FirstLetterCapital;
use PHPUnit\Framework\TestCase;

/**
 * Class FirstLetterCapitalTest
 */
class FirstLetterCapitalTest extends TestCase
{
    /**
     * Test if the twig filter first letter capital works correctly
     */
    public function testTwigFilter(): void
    {
        $filter = new FirstLetterCapital();

        $this->assertEquals('Test Bla', $filter->firstLetterCapital('test Bla'));
    }
}
