<?php

/*
 * Copyright (C) 2022 by Luc Hamers <vereniging@hamers.de>
 *
 * This file is part of Vereniging.
 *
 * Vereniging is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Vereniging is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Vereniging.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Tests\Controller\Dashboard;

use App\Entity\CommitteeFunctionMap;
use App\Entity\MemberEntry;
use App\Entity\SerialLetter;
use App\Entity\SerialLetterContent;
use App\Service\SerialLetterGenerator;
use App\Tests\WebTestCase;
use DateTime;
use Exception;

/**
 * Class SerialLetterControllerTest
 */
class SerialLetterControllerTest extends WebTestCase
{
    private $serialLetterId;


    /**
     * Setup the test environment, i.e. load fixture data.
     *
     * @throws Exception
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->loadAllFixtures();
        $this->serialLetterId = $this->referenceRepository->getReference('serial_letter_content11')->getSerialLetter()->getId();
    }


    /**
     * Test if there is a link on the dashboard page to the new member page.
     *
     * @throws Exception
     */
    public function testLinkOnDashboardPage(): void
    {
        $this->checkLinkOnDashboardPage($this, 'Serial letters', 'Select serial letter', '/dashboard');
    }


    /**
     * Test the index controller.
     *
     * @throws Exception
     */
    public function testIndex(): void
    {
        $client = $this->getMyClient();
        $crawler = $client->request('GET', '/serial_letters/select');

        // Check the page title
        $this->assertCount(1, $crawler->filter('title:contains("Select serial letter")'));

        // Check the navigation links
        $this->assertCount(1, $crawler->filter('a.navbar-brand:contains("the Vereniging member system dashboard")'));
        $this->assertCount(1, $crawler->filter('a.nav-link:contains("Select serial letter")'));

        // Check the table header
        $this->assertCount(1, $crawler->filter('th:contains("Title")'));
        $this->assertCount(1, $crawler->filter('th:contains("Created on")'));
        $this->assertCount(1, $crawler->filter('th:contains("Last changed on")'));
        $this->assertCount(1, $crawler->filter('th:contains("Last changed by")'));

        // Check if the serial letters are all shown with the correct information
        /** @var SerialLetter $serialLetter1 */
        $today = $this->referenceRepository->getReference('serial_letter1')->getChangeDate();
        $this->assertCount(1, $crawler->filter('td:contains("Serial letter1")'));
        $this->assertCount(1, $crawler->filter('td:contains("Serial letter2")'));
        $this->assertCount(1, $crawler->filter('td:contains("Serial letter3")'));
        $this->assertCount(6, $crawler->filter('td:contains("'.$today->format('Y-m-d').'")'));
        $this->assertCount(2, $crawler->filter('td:contains("Firstname1 Lastname1")'));
        $this->assertCount(1, $crawler->filter('td:contains("Firstname2 Lastname2")'));
        $this->assertCount(3, $crawler->filter('td a'));
    }


    /**
     * Test if a new letter is created correctly. A new letter object is stored in the database containing titles in two
     * languages.
     *
     * @throws Exception
     */
    public function testEmptyLetterCreation(): void
    {
        $client = $this->getMyClient();
        $client->followRedirects(true);

        // Call the new serial letter page to insert a 4th (empty) serial letter.
        $crawler = $client->request('GET', '/serial_letters/new');

        // Check the titles
        $this->assertCount(1, $crawler->filter('input[value="New serial letter"]'));
        $this->assertCount(1, $crawler->filter('input[value="Nieuwe seriebrief"]'));

        // Check that all form fields are empty
        $this->assertEmpty($crawler->filter('textarea')->text());
        $this->assertEmpty($crawler->filter('input[type="text"]')->text());
        $this->assertCount(0, $crawler->filter('option[selected="selected"]'));
        $this->assertCount(0, $crawler->filter('input[checked="checked"]'));

        // Write both titles and save
        $buttonCrawlerNode = $crawler->selectButton('Generate PDF preview - English');
        $form = $buttonCrawlerNode->form();
        // Set titles to see if the form can be saved with changes
        $form['serial_letter[serialLetterContents][0][letterTitle]'] = "title1";
        $form['serial_letter[serialLetterContents][1][letterTitle]'] = "title2";
        $client->submit($form);

        // Call the select page, which should show the 4th letter.
        $crawler = $client->request('GET', '/serial_letters/select');
        $today = new DateTime('now');
        // There should be 4 letters
        $this->assertCount(8, $crawler->filter('td:contains("'.$today->format('Y-m-d').'")'));
    }


    /**
     * Test if the forms page for editing serial letters are shown correctly.
     *
     * @throws Exception
     */
    public function testShowEdit(): void
    {
        $client = $this->getMyClient();

        // Call the edit serial letter page
        $crawler = $client->request('GET', '/serial_letters/edit/'.$this->serialLetterId);

        // Check the page title
        $this->assertCount(1, $crawler->filter('title:contains("Edit serial letter")'));

        // Check the navigation links
        $this->assertCount(1, $crawler->filter('a.navbar-brand:contains("the Vereniging member system dashboard")'));
        $this->assertCount(1, $crawler->filter('a.nav-link:contains("Select serial letter")'));
        $this->assertCount(1, $crawler->filter('a.nav-link:contains("Edit serial letter")'));

        // Check if tinymce is used
        $this->assertCount(1, $crawler->filter('script:contains("tinymce.init")'));
        $this->assertCount(1, $crawler->filter('script:contains("selector: \'textarea.en\'")'));
        $this->assertCount(1, $crawler->filter('script:contains("selector: \'textarea.nl\'")'));

        // Check if all the form elements are present
        // Check the titles
        $this->assertCount(1, $crawler->filter('input[value="Serial letter1"]'));
        $this->assertCount(1, $crawler->filter('input[value="Seriebrief1"]'));
        // Check the signature selects:
        //   two member selects with three members (2 via the role, one because it is the second signer of letter1) and
        //   one empty option each.
        //   additionally there are 6 function options for the first member and one (empty) for the second.
        $this->assertCount(15, $crawler->filter('option'));
        $this->assertCount(4, $crawler->filter('option[value=""]'));
        $this->assertCount(2, $crawler->filter('option:contains("firstname1.lastname1")'));
        $this->assertCount(2, $crawler->filter('option:contains("firstname3.lastname3")'));
        $this->assertCount(2, $crawler->filter('option:contains("firstname5.lastname5")'));
        $this->assertCount(1, $crawler->filter('option:contains("Activities - Member")'));
        $this->assertCount(1, $crawler->filter('option:contains("Board - Chairman")'));
        $this->assertCount(1, $crawler->filter('option:contains("Board - Member")'));
        $this->assertCount(1, $crawler->filter('option:contains("Board - Vice chairman")'));
        $this->assertCount(1, $crawler->filter('option:contains("Public relations - Member")'));
        // Check the text areas, which are displayed as tinymce editor windows.
        $this->assertCount(1, $crawler->filter('textarea:contains("This is a serial letter1.")'));
        $this->assertCount(1, $crawler->filter('textarea:contains("Dit is een seriebrief1.")'));
        // Check for the use academic titles checkbox
        $this->assertCount(1, $crawler->filter('label[for="serial_letter_signatureUseTitle"]:contains("Use academic titles")'));
        $this->assertCount(1, $crawler->filter('input[type="checkbox"][checked="checked"]'));
        // Check the order of the content forms. The dutch form should be first, the english one last (alphabetic order)
        $this->assertStringContainsString('Dit is een seriebrief1.', $crawler->filter('textarea')->first()->html());
        $this->assertStringContainsString('This is a serial letter1.', $crawler->filter('textarea')->last()->html());

        // Check if all the buttons are shown
        $this->assertEquals('Generate PDF preview - Dutch', $crawler->filter('button[type="submit"][name="previewDutch"]')->text());
        $this->assertEquals('Generate PDF preview - English', $crawler->filter('button[type="submit"][name="previewEnglish"]')->text());
    }


    /**
     * Test if the forms page for editing serial letters shows the variables menu
     *
     * @throws Exception
     */
    public function testShowTemplates(): void
    {
        $client = $this->getMyClient();

        // Call the edit serial letter page
        $crawler = $client->request('GET', '/serial_letters/edit/'.$this->serialLetterId);
        // There are 25 variables which can be added
        $this->assertStringContainsString('Activity list', $crawler->html());
        $this->assertStringContainsString('Activiteitenlijst', $crawler->html());
    }


    /**
     * Test if the forms page for editing serial letters shows the variables menu
     *
     * @throws Exception
     */
    public function testShowEditVariables(): void
    {
        $client = $this->getMyClient();

        // Call the edit serial letter page
        $crawler = $client->request('GET', '/serial_letters/edit/'.$this->serialLetterId);
        // There are 27 variables which can be added, and the opening and signers variable are already in the template
        $this->assertEquals(58, substr_count($crawler->html(), 'mceNonEditable'));
        $this->assertEquals(58, substr_count($crawler->html(), 'data-variable'));
        // Check if the variables in the menu are translated
        $this->assertEquals(2, substr_count($crawler->html(), 'data-variable="firstName">first name'));

        // Check if the translated variables in the text are stored as non-translated ones in the database
        $buttonCrawlerNode = $crawler->selectButton('Generate PDF preview - English');
        $form = $buttonCrawlerNode->form();
        // Write the variable preferredLanguage with the German translation. This also contains the variables in dutch
        // for name and first name to prevent regression (a bug in the controller messed up the first names backward
        // translation, it changed "voornaam" to "voorname")
        // There was another bug, where plain text in html tags was translated back (e.g. <p>Bevorzugte Sprache</p> to
        // <p>preferredLanguage</p>
        $form['serial_letter[serialLetterContents][0][content]'] = 'bla <span class="mceNonEditable" style="border: thin dotted grey;" data-variable="preferredLanguage">Bevorzugte Sprache</span><span class="mceNonEditable" style="border: thin dotted grey;" data-variable="name">naam</span><span class="mceNonEditable" style="border: thin dotted grey;" data-variable="firstName">voornaam</span> <p>Bevorzugte Sprache</p>';
        $client->submit($form);
        // Now read back the letter content from the database. Instead of "Bevorzugte Sprache", this must contain
        // "preferredLanguage" as value of the span field.
        /** @var SerialLetter $letter */
        $letter = $this->getEntityManager()->getRepository(SerialLetter::class)->find($this->serialLetterId);
        $this->getEntityManager()->refresh($letter->getContent('en'));
        $this->assertStringContainsString('data-variable="preferredLanguage">preferredLanguage</span>', $letter->getContent('en')->getContent());
        $this->assertStringContainsString('data-variable="name">name</span>', $letter->getContent('en')->getContent());
        $this->assertStringContainsString('data-variable="firstName">firstName</span>', $letter->getContent('en')->getContent());
        $this->assertStringContainsString('<p>Bevorzugte Sprache</p>', $letter->getContent('en')->getContent());

        // Now check if the variable is translated back to English when the variable is shown back in the editor
        $crawler = $client->request('GET', '/serial_letters/edit/'.$this->serialLetterId);
        // Now check if the variable name are translated back when the serial letter reloads.
        // $crawler->html() escapes the html characters like < and >, so use &gt; and &lt;.
        $this->assertStringContainsString('data-variable="preferredLanguage"&gt;preferred language&lt;/span&gt;', $crawler->html(), 'Variable preferredLanguage is not translated back when loading the page.');
    }


     /**
     * Test if the serial letters edit page creates pdf previews correctly. For this test, click the preview button and
     * check if the returned file is really a pdf file. We check some parameters here (content type, file size), but we
     * do not check the PDF itself.
      *
      * @throws Exception
      */
    public function testGeneratePreview(): void
    {
        $client = $this->getMyClient();
        $this->clearAllLogFiles();

        $crawler = $client->request('GET', '/serial_letters/edit/'.$this->serialLetterId);
        $buttonCrawlerNode = $crawler->selectButton('previewDutch');
        $form = $buttonCrawlerNode->form();

        // Click the preview button under normal circumstances.
        $client->submit($form);
        $this->assertEquals('application/pdf', $client->getResponse()->headers->get('content_type'));
        $this->assertStringContainsString('max-age=0', $client->getResponse()->headers->get('cache-control'));
        // The file is roughly about 32kiB
        $this->assertGreaterThan(30, $client->getResponse()->headers->get('content-length')/1024);
        $this->assertLessThan(35, $client->getResponse()->headers->get('content-length')/1024);

        $logFiles = $this->getAllLogs();
        $this->assertCount(1, $logFiles);
    }


    /**
     * Test if clicking the preview button returns an error log instead of a pdf file in case there is an error during
     * the pdflatex run. To simulate this, we replace the app.generate_serial_letter service by a mock which we can
     * control. When the controller calls generatePdfPreview, this generates an exception. In the controller there then
     * will also be a call to getLogFile, so we also have to mock that method.
     */
    public function testGeneratePreviewWithError(): void
    {
        // Create the log file
        $testLogFile = '/tmp/SerialLetterControllerTest_'.date('Ymd_His').'.log';
        file_put_contents($testLogFile, 'test');

        // Create the mock to replace the serial letter generator service.
        $generatorMock = $this->createMock(SerialLetterGenerator::class);
        // Configure the mock to return an exception on preview generation.
        $generatorMock->method('generatePdfPreview')
                      ->will($this->throwException(new Exception('')));
        // Configure the mock to return the path to the previously created log file.
        $generatorMock->method('getLogFile')
                      ->willReturn($testLogFile);

        // Inject the mock into the container
        $client = $this->getMyClient();
        $container = $client->getContainer();
        $container->set('App\Service\SerialLetterGenerator', $generatorMock);

        // Call the controller without any serial letter data, but with the English preview button clicked.
        $client->request('POST', '/serial_letters/edit/'.$this->serialLetterId, [
            // This is the clicked button
            'previewEnglish' => 1,
            'serial_letter' => [],
        ]);

        $this->assertEquals('text/plain', $client->getResponse()->headers->get('content_type'));
        $this->assertStringContainsString('max-age=0', $client->getResponse()->headers->get('cache-control'));
        $this->assertEquals(4, $client->getResponse()->headers->get('content-length'));
        // Cleanup temporary log file.
        unlink($testLogFile);
    }


    /**
     * Test if the serial letters edit page saves its data correctly.
     *
     * @throws Exception
     */
    public function testSubmitEdit(): void
    {
        $client = $this->getMyClient();

        $entityManager = $this->getEntityManager();
        // Select serial letter 2, because this has been changed by user2. After submit, the changed by user should be
        // user1.
        /** @var SerialLetterContent $content */
        $content = $entityManager->getRepository(SerialLetterContent::class)
                                 ->findOneBy(['letterTitle' => 'Serial letter2']);
        // Set the change date of the serial letter to some time in the past to check if the date is updated correctly
        // by the submit.
        $content->getSerialLetter()->setChangeDate(new DateTime('-3 days'));
        $entityManager->persist($content->getSerialLetter());
        $entityManager->flush();

        $letterId = $content->getSerialLetter()->getId();
        $crawler = $client->request('GET', "/serial_letters/edit/$letterId");
        $buttonCrawlerNode = $crawler->selectButton('Generate PDF preview - English');
        $form = $buttonCrawlerNode->form();
        // Change all options to see if the form can be saved with changes
        $form['serial_letter[serialLetterContents][0][letterTitle]'] = "title1";
        $form['serial_letter[serialLetterContents][1][letterTitle]'] = "title2";
        $form['serial_letter[serialLetterContents][0][content]'] = "content1";
        $form['serial_letter[serialLetterContents][1][content]'] = "content2";
        $form['serial_letter[signatureUseTitle]']->tick();
        $form['serial_letter[firstSignatureMember]']->select("");
        $member3 = $entityManager->getRepository(MemberEntry::class)->findOneBy(['firstName' => 'Firstname3']);
        $form['serial_letter[secondSignatureMember]']->select($member3->getId());

        $client->submit($form);

        $crawler = $client->request('GET', "/serial_letters/edit/$letterId");

        // Check if all the changes have been changed
        // Check the titles
        $this->assertCount(1, $crawler->filter('input[value="title1"]'));
        $this->assertCount(1, $crawler->filter('input[value="title2"]'));
        // Check the signature selects:Two member selects with two members and one empty option each.0
        $this->assertCount(0, $crawler->filter('option[selected="selected"]:contains("firstname1.lastname1")'));
        $this->assertCount(1, $crawler->filter('option[selected="selected"]:contains("firstname3.lastname3")'));
        // Check the text areas, which are displayed as tinymce editor windows.
        $this->assertCount(1, $crawler->filter('textarea:contains("content1")'));
        $this->assertCount(1, $crawler->filter('textarea:contains("content2")'));
        // Check for the use academic titles checkbox
        $this->assertCount(1, $crawler->filter('input[type="checkbox"][checked="checked"]'));

        // Check if the change date for the serial letter has been updated correctly.
        $content = $entityManager->getRepository(SerialLetterContent::class)
                                 ->findOneBy(['letterTitle' => 'title1']);
        $dateToday = new DateTime('now');
        $dateToday->setTime(0, 0);
        $entityManager->refresh($content->getSerialLetter());
        /** @var SerialLetter $letter */
        $letter = $content->getSerialLetter();
        $this->assertEquals($dateToday, $letter->getChangeDate());
        $this->assertEquals('Firstname1', $letter->getLastUpdatedBy()->getFirstName());
    }


    /**
     * Test if the serial letters edit page saves its signature data correctly.
     *
     * @throws Exception
     */
    public function testSubmitEditSignature(): void
    {
        // Remove the first signature member. Javascript is not working here, so the second member and function are also
        // present.
        $client = $this->getMyClient();
        $crawler = $client->request('GET', '/serial_letters/edit/'.$this->serialLetterId);
        $buttonCrawlerNode = $crawler->selectButton('Generate PDF preview - English');
        $form = $buttonCrawlerNode->form();
        $form['serial_letter[firstSignatureMember]']->select("");
        $form['serial_letter[secondSignatureMember]']->select("");
        $client->submit($form);

        $crawler = $client->request('GET', "/serial_letters/edit/".$this->serialLetterId);

        // Check if both signature members and all functions have been removed
        $this->assertCount(0, $crawler->filter('option[selected="selected"]'));
        // There should be 2 member selects with 3 options and 2 function select both with an empty option.
        $this->assertCount(8, $crawler->filter('option'));
        $this->assertCount(4, $crawler->filter('option[value=""]'));
        $this->assertCount(2, $crawler->filter('option:contains("firstname1.lastname1")'));
        $this->assertCount(2, $crawler->filter('option:contains("firstname3.lastname3")'));

        // Now set member 3 as second signing member (first member stays empty). To see any difference, elect member3 as
        // board member first. Because the entity manager has been reset by the last request, we must reload the
        // following entities from the database and the reference repository
        $member3 = $this->getEntityManager()->getRepository(MemberEntry::class)->findOneBy(['firstName' => 'Firstname3']);
        $boardChairman = $this->referenceRepository->getReference('committee_function_map_board_chairman');
        // This ensures we reload the board chairman from the database.
        $boardChairman = $this->getEntityManager()->getRepository(CommitteeFunctionMap::class)->find($boardChairman->getId());
        $member3->addCommitteeFunctionMap($boardChairman);
        $this->getEntityManager()->flush();

        $crawler = $client->request('GET', '/serial_letters/edit/'.$this->serialLetterId);
        $buttonCrawlerNode = $crawler->selectButton('Generate PDF preview - English');
        $form = $buttonCrawlerNode->form();
        $form['serial_letter[firstSignatureMember]']->select($member3->getId());
        $form['serial_letter[secondSignatureMember]']->select($member3->getId());
        $client->submit($form);
        $crawler = $client->request('GET', "/serial_letters/edit/".$this->serialLetterId);
        // Check the signature selects: both signature members should be member3.
        $this->assertCount(0, $crawler->filter('option[selected="selected"]:contains("firstname1.lastname1")'));
        $this->assertCount(2, $crawler->filter('option[selected="selected"]:contains("firstname3.lastname3")'));
        // There should be 2 member selects with 3 options and 2 function select both with an empty option and a
        // board - chairman option.
        $this->assertCount(10, $crawler->filter('option'));
        $this->assertCount(4, $crawler->filter('option[value=""]'));
        $this->assertCount(2, $crawler->filter('option:contains("firstname1.lastname1")'));
        $this->assertCount(2, $crawler->filter('option:contains("firstname3.lastname3")'));
        $this->assertCount(2, $crawler->filter('option:contains("Board - Chairman")'));
    }
}
