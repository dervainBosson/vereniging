<?php

/*
 * Copyright (C) 2022 by Luc Hamers <vereniging@hamers.de>
 *
 * This file is part of Vereniging.
 *
 * Vereniging is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Vereniging is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Vereniging.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Tests\Controller\Dashboard;

use App\Entity\MemberEntry;
use App\Entity\Title;
use App\Tests\WebTestCase;
use Exception;

/**
 * Test the logfile controller.
 *
 */
class LogfileControllerTest extends WebTestCase
{
    /**
     * Setup the test environment, i.e. load fixture data.
     *
     * @throws Exception
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->loadAllFixtures();
    }


    /**
     * Test if there is a link on the dashboard page to the new member page.
     *
     * @throws Exception
     */
    public function testLinkOnDashboardPage(): void
    {
        $this->checkLinkOnDashboardPage($this, 'Log file', 'Log file', '/dashboard');
    }


    /**
     * Test the main dashboard page.
     *
     * @throws Exception
     */
    public function testIndex(): void
    {
        $client = $this->getMyClient();

        $crawler = $client->request('GET', '/dashboard/logfile');

        // Check the page title
        $this->assertEquals(1, $crawler->filter('title:contains("Log file")')->count());

        // Check the navigation links
        $this->assertEquals(1, $crawler->filter('a.navbar-brand:contains("the Vereniging member system dashboard")')->count());
        $this->assertEquals(1, $crawler->filter('a.nav-link:contains("Log file")')->count());

        // Check frame
        $this->assertEquals(1, $crawler->filter('h6.card-header:contains("Log file entry list")')->count());

        // Check the table headings
        $this->assertEquals(1, $crawler->filter('th:contains("When")')->count());
        $this->assertEquals(1, $crawler->filter('th:contains("By whom")')->count());
        $this->assertEquals(1, $crawler->filter('th:contains("On who")')->count());
        $this->assertEquals(1, $crawler->filter('th:contains("What")')->count());

        // Check the number of logfile entries in the list.
        $this->assertEquals(10, $crawler->filter('td:contains("Logentry")')->count());

        // Check the widgets to control the logfile entries in the list
        // First the time range selection
        $this->assertEquals(1, $crawler->filter('select#logfile_entry_selection_dateRange')->count());
        $this->assertEquals(1, $crawler->filter('select#logfile_entry_selection_dateRange option:contains("Last day")')->count());
        $this->assertEquals(1, $crawler->filter('select#logfile_entry_selection_dateRange option:contains("Last week")')->count());
        $this->assertEquals(1, $crawler->filter('select#logfile_entry_selection_dateRange option:contains("Last month")')->count());
        $this->assertEquals(1, $crawler->filter('select#logfile_entry_selection_dateRange option:contains("Last year")')->count());

        // Look for the search fields for by whom and on who
        $this->assertEquals(1, $crawler->filter('input#logfile_entry_selection_searchByWhom')->count());
        $this->assertEquals(1, $crawler->filter('input#logfile_entry_selection_searchOnWho')->count());

        // Look for change type selector
        $this->assertEquals(1, $crawler->filter('select#logfile_entry_selection_changeType')->count());
        $this->assertEquals(1, $crawler->filter('select#logfile_entry_selection_changeType option:contains("Member data change")')->count());
        $this->assertEquals(1, $crawler->filter('select#logfile_entry_selection_changeType option:contains("Member settings change")')->count());
        $this->assertEquals(1, $crawler->filter('select#logfile_entry_selection_changeType option:contains("Member free text")')->count());
        $this->assertEquals(1, $crawler->filter('select#logfile_entry_selection_changeType option:contains("Member added")')->count());
        $this->assertEquals(1, $crawler->filter('select#logfile_entry_selection_changeType option:contains("Member moved")')->count());
        $this->assertEquals(1, $crawler->filter('select#logfile_entry_selection_changeType option:contains("System settings change")')->count());
        $this->assertEquals(1, $crawler->filter('select#logfile_entry_selection_changeType option:contains("Import action")')->count());
        $this->assertEquals(1, $crawler->filter('select#logfile_entry_selection_changeType option:contains("Export action")')->count());

        // Look for the search field for what
        $this->assertEquals(1, $crawler->filter('input#logfile_entry_selection_searchWhat')->count());

        // Look for number limit selector
        $this->assertEquals(1, $crawler->filter('select#logfile_entry_selection_numberLimit')->count());
        $this->assertEquals(2, $crawler->filter('select#logfile_entry_selection_numberLimit option:contains("10")')->count());
        $this->assertEquals(1, $crawler->filter('select#logfile_entry_selection_numberLimit option:contains("50")')->count());
        $this->assertEquals(1, $crawler->filter('select#logfile_entry_selection_numberLimit option:contains("100")')->count());
    }


    /**
     * Test sending the limit number filter to the controller
     *
     * @throws Exception
     */
    public function testSelectionsLimitNumber(): void
    {
        $client = $this->getMyClient();

        $crawler = $client->request('GET', '/dashboard/logfile');
        $form = $crawler->filter("#selection_form")->form();
        $form['logfile_entry_selection[numberLimit]']->select(100);
        $crawler = $client->submit($form);
        $this->assertEquals(34, $crawler->filter('td[style*="white-space:nowrap"]')->count());

        $crawler = $client->request('GET', '/dashboard/logfile');
        $form = $crawler->filter("#selection_form")->form();
        $form['logfile_entry_selection[numberLimit]']->select(50);
        $crawler = $client->submit($form);
        $this->assertEquals(34, $crawler->filter('td[style*="white-space:nowrap"]')->count());

        $crawler = $client->request('GET', '/dashboard/logfile');
        $form = $crawler->filter("#selection_form")->form();
        $form['logfile_entry_selection[numberLimit]']->select(10);
        $crawler = $client->submit($form);
        $this->assertEquals(10, $crawler->filter('td[style*="white-space:nowrap"]')->count());
    }


    /**
     * Test sending the date limit filter to the controller
     *
     * @throws Exception
     */
    public function testSelectionsLimitDate(): void
    {
        $client = $this->getMyClient();

        $crawler = $client->request('GET', '/dashboard/logfile');
        $form = $crawler->filter("#selection_form")->form();
        $form['logfile_entry_selection[dateRange]']->select(1);
        $form['logfile_entry_selection[numberLimit]']->select(50);
        $crawler = $client->submit($form);
        $this->assertEquals(24, $crawler->filter('td[style*="white-space:nowrap"]')->count());

        $crawler = $client->request('GET', '/dashboard/logfile');
        $form = $crawler->filter("#selection_form")->form();
        $form['logfile_entry_selection[dateRange]']->select(7);
        $form['logfile_entry_selection[numberLimit]']->select(50);
        $crawler = $client->submit($form);
        $this->assertEquals(26, $crawler->filter('td[style*="white-space:nowrap"]')->count());

        $crawler = $client->request('GET', '/dashboard/logfile');
        $form = $crawler->filter("#selection_form")->form();
        $form['logfile_entry_selection[dateRange]']->select(31);
        $form['logfile_entry_selection[numberLimit]']->select(50);
        $crawler = $client->submit($form);
        $this->assertEquals(29, $crawler->filter('td[style*="white-space:nowrap"]')->count());

        $crawler = $client->request('GET', '/dashboard/logfile');
        $form = $crawler->filter("#selection_form")->form();
        $form['logfile_entry_selection[dateRange]']->select(365);
        $form['logfile_entry_selection[numberLimit]']->select(50);
        $crawler = $client->submit($form);
        $this->assertEquals(32, $crawler->filter('td[style*="white-space:nowrap"]')->count());

        $crawler = $client->request('GET', '/dashboard/logfile');
        $form = $crawler->filter("#selection_form")->form();
        $form['logfile_entry_selection[dateRange]']->select('');
        $form['logfile_entry_selection[numberLimit]']->select(50);
        $crawler = $client->submit($form);
        $this->assertEquals(34, $crawler->filter('td[style*="white-space:nowrap"]')->count());
    }


    /**
     * Test sending the type limit filter to the controller
     *
     * @throws Exception
     */
    public function testSelectionsLimitType(): void
    {
        $client = $this->getMyClient();

        $crawler = $client->request('GET', '/dashboard/logfile');
        $form = $crawler->filter("#selection_form")->form();
        $memberDataChange = $this->referenceRepository->getReference('change_type_member_data_change');
        $form['logfile_entry_selection[changeType]']->select($memberDataChange->getId());
        $form['logfile_entry_selection[numberLimit]']->select(50);
        $crawler = $client->submit($form);
        $this->assertEquals(18, $crawler->filter('td[style*="white-space:nowrap"]')->count());

        $crawler = $client->request('GET', '/dashboard/logfile');
        $form = $crawler->filter("#selection_form")->form();
        $memberDataChange = $this->referenceRepository->getReference('change_type_system_settings_change');
        $form['logfile_entry_selection[changeType]']->select($memberDataChange->getId());
        $form['logfile_entry_selection[numberLimit]']->select(50);
        $crawler = $client->submit($form);
        $this->assertEquals(16, $crawler->filter('td[style*="white-space:nowrap"]')->count());

        $crawler = $client->request('GET', '/dashboard/logfile');
        $form = $crawler->filter("#selection_form")->form();
        $memberDataChange = $this->referenceRepository->getReference('change_type_member_settings_change');
        $form['logfile_entry_selection[changeType]']->select($memberDataChange->getId());
        $form['logfile_entry_selection[numberLimit]']->select(50);
        $crawler = $client->submit($form);
        $this->assertEquals(0, $crawler->filter('td[style*="white-space:nowrap"]')->count());
    }


    /**
     * Test sending the search call by whom to the controller
     *
     * @throws Exception
     */
    public function testSearchByWhom(): void
    {
        $client = $this->getMyClient();

        $crawler = $client->request('GET', '/dashboard/logfile');
        $form = $crawler->filter("#selection_form")->form();
        $form['logfile_entry_selection[searchByWhom]'] = '1';
        $form['logfile_entry_selection[numberLimit]']->select(50);
        $crawler = $client->submit($form);
        $this->assertEquals(6, $crawler->filter('td[style*="white-space:nowrap"]')->count());
        $this->assertEquals(6, $crawler->filter('td:contains("Firstname1 Lastname1")')->count());
        $this->assertEquals(0, $crawler->filter('td:contains("Firstname2 Lastname2")')->count());
    }


    /**
     * Test sending the search call on who to the controller
     *
     * @throws Exception
     */
    public function testSearchOnWho(): void
    {
        $client = $this->getMyClient();

        $crawler = $client->request('GET', '/dashboard/logfile');
        $form = $crawler->filter("#selection_form")->form();
        $form['logfile_entry_selection[searchOnWho]'] = '1';
        $form['logfile_entry_selection[numberLimit]']->select(50);
        $crawler = $client->submit($form);
        $this->assertEquals(28, $crawler->filter('td[style*="white-space:nowrap"]')->count());

        $crawler = $client->request('GET', '/dashboard/logfile');
        $form = $crawler->filter("#selection_form")->form();
        $form['logfile_entry_selection[searchOnWho]'] = '2';
        $form['logfile_entry_selection[numberLimit]']->select(50);
        $crawler = $client->submit($form);
        $this->assertEquals(0, $crawler->filter('td[style*="white-space:nowrap"]')->count());
    }


    /**
     * Test sending the search call what to the controller
     *
     * @throws Exception
     */
    public function testSearchWhat(): void
    {
        $client = $this->getMyClient();

        $crawler = $client->request('GET', '/dashboard/logfile');
        $form = $crawler->filter("#selection_form")->form();
        $form['logfile_entry_selection[searchWhat]'] = '0';
        $form['logfile_entry_selection[numberLimit]']->select(50);
        $crawler = $client->submit($form);
        $this->assertEquals(3, $crawler->filter('td[style*="white-space:nowrap"]')->count());

        $crawler = $client->request('GET', '/dashboard/logfile');
        $form = $crawler->filter("#selection_form")->form();
        $form['logfile_entry_selection[searchWhat]'] = 'Logentry 1';
        $form['logfile_entry_selection[numberLimit]']->select(50);
        $crawler = $client->submit($form);
        $this->assertEquals(11, $crawler->filter('td[style*="white-space:nowrap"]')->count());

        $crawler = $client->request('GET', '/dashboard/logfile');
        $form = $crawler->filter("#selection_form")->form();
        $form['logfile_entry_selection[searchWhat]'] = '100';
        $form['logfile_entry_selection[numberLimit]']->select(50);
        $crawler = $client->submit($form);
        $this->assertEquals(0, $crawler->filter('td[style*="white-space:nowrap"]')->count());
    }


    /**
     * There was a bug in logging a title change, which crashed the log output. This tests reproduces this to prevent it
     * from happening again.
     */
    public function testAddTitle(): void
    {
        $this->setLoggedInUserForLogging();
        $member3 = $this->getEntityManager()->getRepository(MemberEntry::class)->findOneBy(['firstName' => 'Firstname3']);
        $title = $this->getEntityManager()->getRepository(Title::class)->findOneBy(['title' => 'Dr.']);
        $member3->setTitle($title);

        $this->getEntityManager()->flush();
        $client = $this->getMyClient();
        $crawler = $client->request('GET', '/dashboard/logfile');
        $this->assertEquals(1, $crawler->filter('td:contains("Dr.")')->count());
    }
}
