<?php

/*
 * Copyright (C) 2022 by Luc Hamers <vereniging@hamers.de>
 *
 * This file is part of Vereniging.
 *
 * Vereniging is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Vereniging is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Vereniging.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Tests\Controller\Dashboard;

use App\Entity\Committee;
use App\Entity\MemberEntry;
use App\Entity\Status;
use App\Entity\UserRole;
use App\Service\SerialLetterGenerator;
use App\Tests\WebTestCase;
use Exception;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Component\DomCrawler\Crawler;
use Symfony\Component\HttpKernel\HttpKernelBrowser;
use Symfony\Component\Mime\Email;

/**
 * Test the member list controller.
 *
 */
class MemberListsControllerTest extends WebTestCase
{
    /**
     * Setup the test environment, i.e. load fixture data.
     *
     * @throws Exception
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->loadAllFixtures();
    }


    /**
     * Test if there is a link on the dashboard page to the new member page.
     *
     * @throws Exception
     */
    public function testLinkOnDashboardPage(): void
    {
        $this->checkLinkOnDashboardPage($this, 'Member lists', 'Member lists', '/dashboard');
    }


    /**
     * Test the main list view for the member lists as member administrator. Most tests for this are done in the
     * MemberSelectionFormHandlerTest. Just check some settings
     *
     * @throws Exception
     */
    public function testIndexAsMemberAdministrator(): void
    {
        // To prevent a regression when a committee without members is selected, we have to add a committee before
        // calling the page.
        $this->setLoggedInUserForLogging();
        $newCommittee = new Committee();
        $newCommittee->setCommitteeName('test');
        $newCommittee->translate('nl')->setCommitteeNameTranslated('Test');

        $this->getEntityManager()->persist($newCommittee);
        $this->getEntityManager()->flush();

        $client = $this->getMyClient('firstname3.lastname3');

        $crawler = $this->checkDefaultListElements($client);
        $this->assertEquals(11, $crawler->filter('a[href^="/member"]')->count());
        $this->assertEquals(1, $crawler->filter('h6.card-header:contains("Filters")')->count());

        // Submit form with all checkboxes cleared --> the header should be "None", the content empty
        $form = $crawler->filter("#selection_form")->form();
        // Remove member (first entry in Dutch view)
        $form['member_lists_selection[status]'][0]->untick();
        $crawler = $client->submit($form);
        $this->assertEquals(1, $crawler->filter('div.card-body:contains("No member entries found")')->count());

        // Test to check if a committee without members generates an sql error (bug fix)
        $form['member_lists_selection[committee]'][3]->tick();
        $crawler = $client->submit($form);
        $this->assertEquals(1, $crawler->filter('div.card-body:contains("No member entries found")')->count());

        // Submit form with both status checkboxes for member and future member selected (first and last entry in Dutch)
        // --> all members should appear.
        $form['member_lists_selection[status]'][0]->tick();
        $form['member_lists_selection[status]'][2]->tick();
        $crawler = $client->submit($form);
        $this->assertEquals(12, $crawler->filter('a[href^="/member"]')->count());

        // Submit form with all members using direct debit (Firstname3)
        $form['member_lists_selection[others][membershipEndRequested]']->tick();
        $crawler = $client->submit($form);
        $this->assertEquals(1, $crawler->filter('a[href^="/member"]')->count());

        // Submit form with all members marked to end their membership (Firstname1 plus all group members)
        $form['member_lists_selection[others][membershipEndRequested]']->untick();
        $form['member_lists_selection[others][directDebit]']->select('with');
        $crawler = $client->submit($form);
        $this->assertEquals(4, $crawler->filter('a[href^="/member"]')->count());

        // Submit form with all members using direct debit (Firstname1 plus all group members)
        $form['member_lists_selection[others][directDebit]']->select('without');
        $crawler = $client->submit($form);
        $this->assertEquals(7, $crawler->filter('a[href^="/member"]')->count());
    }


    /**
     * Test the main list view for the member lists as member without administrative rights.
     *
     * @throws Exception
     */
    public function testIndexAsMember(): void
    {
        $client = $this->getMyClient('firstname2.lastname2');

        $crawler = $this->checkDefaultListElements($client);
        $this->assertEquals(1, $crawler->filter('a[href^="/member"]')->count());
    }


    /**
     * Test the main list view for the member lists as member without administrative rights.
     *
     * @throws Exception
     */
    public function testCreateSerialLettersAsMemberCheckElements(): void
    {
        $client = $this->getMyClient('firstname2.lastname2');

        $crawler = $this->checkDefaultListElements($client);
        $this->assertEquals(0, $crawler->filter('form[id="selection_form"]')->count());
        $this->assertEquals(0, $crawler->filter('input[type="checkbox"][name="member[]"]')->count());
        $this->assertEquals(0, $crawler->filter('input[type="checkbox"][id="checkAll"]')->count());
        $this->assertEquals(0, $crawler->filter('select[name="letter"]')->count());
        $this->assertEquals(0, $crawler->filter('option')->count());
        $this->assertEquals(0, $crawler->filter('button[type="submit"]')->count());
    }


    /**
     * Test of the action select fields are shown correctly
     *
     * @throws Exception
     */
    public function testShowActionSelect(): void
    {
        $client = $this->getMyClient();
        $crawler = $client->request('GET', '/dashboard/memberlists');

        // First start with the rights to generate serial letters, send serial emails and to change the member's status.
        // There should be a form, checkboxes in front of the members and a submit button. There also should be an
        // action select form and the serial letter, serial email and status selects (serial email and status invisible)
        $this->checkForMainFormElements($crawler, true);
        $this->checkForActionSelect($crawler, true, ['Generate serial letter', 'Send serial emails', 'Change members status to']);
        $this->checkForSelect($crawler, 'letter_select', 'inline', true, 3);
        $this->checkForSelect($crawler, 'emailtemplate_select', 'none', true, 2);
        $this->checkForSelect($crawler, 'status_select', 'none', true, 3);

        // Now remove the role which is needed to change the member's status. This should remove the change status
        // option and the status select invisible
        $this->changeRoles('ROLE_MANAGE_MEMBER_CLUB_SETTINGS', null);
        $crawler = $this->getMyClient()->request('GET', '/dashboard/memberlists');
        $this->checkForMainFormElements($crawler, true);
        $this->checkForActionSelect($crawler, true, ['Generate serial letter', 'Send serial emails', ]);
        $this->checkForSelect($crawler, 'letter_select', 'inline', true, 3);
        $this->checkForSelect($crawler, 'emailtemplate_select', 'none', true, 2);
        $this->checkForSelect($crawler, 'status_select', 'none', false, 3);

        // Disable serial letter role, enable the role for the status changes
        $this->changeRoles('ROLE_CREATE_SERIAL_LETTERS', 'ROLE_MANAGE_MEMBER_CLUB_SETTINGS');
        $crawler = $this->getMyClient()->request('GET', '/dashboard/memberlists');
        $this->checkForMainFormElements($crawler, true);
        $this->checkForActionSelect($crawler, true, ['Send serial emails', 'Change members status to']);
        $this->checkForSelect($crawler, 'letter_select', 'none', false, 3);
        $this->checkForSelect($crawler, 'emailtemplate_select', 'none', true, 2);
        $this->checkForSelect($crawler, 'status_select', 'inline', true, 3);

        // Disable serial email role, enable the role for the serial letters
        $this->changeRoles('ROLE_SEND_SERIAL_EMAILS', 'ROLE_CREATE_SERIAL_LETTERS');
        $crawler = $client->request('GET', '/dashboard/memberlists');
        $this->checkForMainFormElements($crawler, true);
        $this->checkForActionSelect($crawler, true, ['Generate serial letter', 'Change members status to']);
        $this->checkForSelect($crawler, 'letter_select', 'inline', true, 3);
        $this->checkForSelect($crawler, 'emailtemplate_select', 'none', false, 2);
        $this->checkForSelect($crawler, 'status_select', 'none', true, 3);

        // Disable all three roles
        $this->changeRoles('ROLE_CREATE_SERIAL_LETTERS', null);
        $this->changeRoles('ROLE_MANAGE_MEMBER_CLUB_SETTINGS', null);
        $crawler = $client->request('GET', '/dashboard/memberlists');
        $this->checkForMainFormElements($crawler, false);
        $this->checkForActionSelect($crawler, false, []);
        $this->checkForSelect($crawler, 'letter_select', 'none', false, 3);
        $this->checkForSelect($crawler, 'status_select', 'inline', false, 3);
        $this->assertEquals(0, $crawler->filter('input[type="hidden"][name="action_select"][value="serial_letter"]')->count());
        $this->assertEquals(0, $crawler->filter('input[type="hidden"][name="action_select"][value="change_status"]')->count());
    }


    /**
     * Test generating serial letters
     *
     * @throws Exception
     */
    public function testCreateSerialLetters(): void
    {
        $client = $this->getMyClient();
        $crawler = $this->checkDefaultListElements($client);

        // Submit without any selected members, the response should be empty, the responds code is 204 (no content)
        $form = $crawler->filter("#action_selection")->form();
        $crawler = $client->submit($form);
        $this->assertEquals(204, $client->getResponse()->getStatusCode());
        $this->assertNull($crawler->getNode(0));

        // When at least one member is selected, a pdf should be generated
        $form['member'][0]->tick();
        $client->submit($form);
        // The file is roughly about 32kiB
        $this->checkSerialLetter($client, 32, [1]);

        // Now select all members in the list. This should generate a bigger pdf, which has more pages.
        for ($i = 0; $i < 11; ++$i) {
            $form['member'][$i]->tick();
        }
        $client->submit($form);
        // The file is roughly about 39kiB
        $this->checkSerialLetter($client, 39, [2, 3, 5, 6, 12, 9, 8, 1, 7]);
    }


    /**
     * Test generating serial letters
     *
     * @throws Exception
     */
    public function testCreateSerialLettersWithError(): void
    {
        $client = $this->getMyClient();
        $crawler = $client->request('GET', '/dashboard/memberlists');

        // Submit without any selected members, the response should be empty, the responds code is 204 (no content)
        $form = $crawler->filter("#action_selection")->form();

        // Now select all members in the list. This should generate a bigger pdf, which has more pages.
        for ($i = 0; $i < 11; ++$i) {
            $form['member'][$i]->tick();
        }

        // Now check the reaction to an error during pdf generation. To do this, change the content of the first serial
        // letter to "$".
        $serialLetter1 = $this->referenceRepository->getReference('serial_letter_content11');
        $serialLetter1->setContent('$');
        $this->getEntityManager()->flush();
        $client->submit($form);

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertEquals('text/x-tex', $client->getResponse()->headers->get('content_type'));
        $this->assertEquals('attachment;filename="letter.log"', $client->getResponse()->headers->get('content-disposition'));
    }


    /**
     * Test generating serial emails
     *
     * @throws Exception
     */
    public function testCreateSerialEmails(): void
    {
        $client = $this->getMyClient();
        $crawler = $this->checkDefaultListElements($client);

        // Submit without any selected members, the response should be empty, the responds code is 204 (no content)
        $form = $crawler->filter("#action_selection")->form();
        $form['action_select']->select('serial_email');
        $letter2 = $this->getFixtureReference('serial_letter2');
        $form['letter_select']->select($letter2->getId());
        $template4 = $this->getFixtureReference('email_template4');
        $form['emailtemplate_select']->select($template4->getId());

        $crawler = $client->submit($form);
        $this->assertEquals(204, $client->getResponse()->getStatusCode());
        $this->assertNull($crawler->getNode(0));

        // When at least one member is selected, a pdf should be generated
        $form['member'][0]->tick();
        $client->submit($form);
        $this->assertEmailCount(1);
        /** @var Email $email */
        $email = $this->getMailerMessage(0);
        $this->assertEmailHeaderSame($email, 'To', 'firstname1@email.com');
        $this->assertEquals("serial email 2 content: Dear Mr. Lastname1, \nThis is a serial letter2.\nKind regards,\n<table><tr><td>Firstname2 Lastname2</td><td></td></tr><tr><td></td><td></td></tr></table>", $email->getHtmlBody());
        $this->assertEquals("serial email 2 content: Dear Mr. Lastname1, \nThis is a serial letter2.\nKind regards,\nFirstname2 Lastname2", $email->getTextBody());

        // Now select all members in the list. This should generate a bigger pdf, which has more pages.
        for ($i = 0; $i < 11; ++$i) {
            $form['member'][$i]->tick();
        }
        $client->submit($form);

        $this->assertEmailCount(10);
    }


    /**
     * Test changing the members status
     *
     * @throws Exception
     */
    public function testChangeStatus(): void
    {
        $client = $this->getMyClient();
        $crawler = $this->checkDefaultListElements($client);

        // Submit without any selected members, the response should be empty, the responds code is 204 (no content)
        $form = $crawler->filter("#action_selection")->form();
        $form['action_select']->select('change_status');
        $futureMembers = $this->getEntityManager()->getRepository(Status::class)->findOneBy(['status' => 'future_member']);
        $form['status_select']->select($futureMembers->getId());

        $crawler = $client->submit($form);
        $this->assertEquals(204, $client->getResponse()->getStatusCode());
        $this->assertNull($crawler->getNode(0));

        // When at least one member is selected, the status of this member should be changed to future member
        $form['member'][0]->tick();
        $client->submit($form);
        $this->assertEquals(204, $client->getResponse()->getStatusCode());
        $member1 = $this->getEntityManager()->getRepository(MemberEntry::class)->findOneBy(['firstName' => 'Firstname1']);
        $this->getEntityManager()->refresh($member1);
        $this->assertEquals($futureMembers->getStatus(), $member1->getStatus()->getStatus());

        // Change some more members
        $form['member'][0]->tick();
        $form['member'][5]->tick();
        $form['member'][9]->tick();
        $formerMembers = $this->getEntityManager()->getRepository(Status::class)->findOneBy(['status' => 'former_member']);
        $form['status_select']->select($formerMembers->getId());
        $client->submit($form);
        $this->assertEquals(204, $client->getResponse()->getStatusCode());
        $members = $this->getEntityManager()->getRepository(MemberEntry::class)->findBy(['firstName' => ['Firstname1', 'Firstname3', 'Firstname8']]);
        foreach ($members as $member) {
            $this->getEntityManager()->refresh($member);
            $this->assertEquals($formerMembers->getStatus(), $member->getStatus()->getStatus());
        }
    }


    /**
     * @param HttpKernelBrowser $client
     *
     * @return Crawler
     */
    private function checkDefaultListElements(HttpKernelBrowser $client): Crawler
    {
        $crawler = $client->request('GET', '/dashboard/memberlists');

        $this->assertEquals(1, $crawler->filter('h6.card-header:contains("Entry list")')->count());
        $this->assertEquals(11, $crawler->filter('td:contains("Firstname")')->count());
        // Check the counter in front of the members
        for ($i = 2; $i <= 9; ++$i) {
            $this->assertEquals(1, $crawler->filter('td.loop:contains("'.$i.'")')->count(), "loop $i");
        }
        $this->assertEquals(3, $crawler->filter('td.loop:contains("1")')->count());
        $this->assertEquals(1, $crawler->filter('td.loop:contains("10")')->count());
        $this->assertEquals(1, $crawler->filter('td.loop:contains("11")')->count());

        $this->assertEquals(0, $crawler->filter('h6.card-header:contains("None")')->count());
        // Check if the table headers are all there
        $this->assertEquals(1, $crawler->filter('div.card-body th:contains("Title")')->count());
        $this->assertEquals(1, $crawler->filter('div.card-body th:contains("First name")')->count());
        $this->assertEquals(1, $crawler->filter('div.card-body th:contains("Last name")')->count());
        $this->assertEquals(1, $crawler->filter('div.card-body th:contains("Email")')->count());
        $this->assertEquals(1, $crawler->filter('div.card-body th:contains("Phone")')->count());
        $this->assertEquals(2, $crawler->filter('div.card-body th:contains("Address")')->count());
        $this->assertEquals(2, $crawler->filter('div.card-body th:contains("Type")')->count());
        $this->assertEquals(1, $crawler->filter('div.card-body th:contains("Number")')->count());
        $this->assertEquals(1, $crawler->filter('div.card-body th:contains("Zip")')->count());
        $this->assertEquals(1, $crawler->filter('div.card-body th:contains("City")')->count());
        $this->assertEquals(1, $crawler->filter('div.card-body th:contains("Country")')->count());
        // Check the content of the member data
        $this->assertEquals(1, $crawler->filter('div.card-body td:contains("Dipl.-Ing.")')->count());
        // The following 2 expect 2 because it finds Firstname1, Firstname10, Firstname11 and Firstname12.
        $this->assertEquals(4, $crawler->filter('div.card-body td:contains("Firstname1")')->count());
        $this->assertEquals(4, $crawler->filter('div.card-body td:contains("Lastname1")')->count());
        $this->assertEquals(1, $crawler->filter('div.card-body td:contains("firstname1@email.com")')->count());
        $this->assertEquals(2, $crawler->filter('div.card-body td:contains("Company:")')->count());
        $this->assertEquals(1, $crawler->filter('div.card-body td:contains("0123456789")')->count());
        $this->assertEquals(1, $crawler->filter('div.card-body td:contains("9876543210")')->count());
        $this->assertEquals(7, $crawler->filter('div.card-body td:contains("Home")')->count());
        $this->assertEquals(4, $crawler->filter('div.card-body td:contains("MyAddress1")')->count());
        $this->assertEquals(4, $crawler->filter('div.card-body td:contains("MyZip1")')->count());
        $this->assertEquals(4, $crawler->filter('div.card-body td:contains("MyCity1")')->count());
        $this->assertEquals(4, $crawler->filter('div.card-body td:contains("Germany")')->count());
        $this->assertEquals(4, $crawler->filter('div.card-body td:contains("Netherlands")')->count());
        $this->assertEquals(1, $crawler->filter('div.card-body td:contains("M.A.")')->count());
        $this->assertEquals(1, $crawler->filter('div.card-body td:contains("Firstname2")')->count());
        $this->assertEquals(1, $crawler->filter('div.card-body td:contains("Lastname2")')->count());
        $this->assertEquals(13, $crawler->filter('div.card-body td:contains(" -")')->count());
        $this->assertEquals(1, $crawler->filter('a[href$="member/firstname2.lastname2"]')->count());

        return $crawler;
    }


    /**
     * Add or remove a role from the system administrator
     *
     * @param string|null $removeRole
     * @param string|null $addRole
     *
     * @throws Exception
     */
    private function changeRoles(?string $removeRole, ?string $addRole): void
    {
        $userRole = $this->getEntityManager()->getRepository(UserRole::class)->findOneBy(['userRole' => 'System administrator']);
        $roles = $userRole->getSystemRoles();
        if ($removeRole) {
            unset($roles[array_search($removeRole, $roles)]);
        }
        if ($addRole) {
            $roles[] = $addRole;
        }
        $userRole->setSystemRoles($roles);
        $this->getEntityManager()->flush();
        $this->getMyClient();
    }


    /**
     * @param Crawler $crawler
     * @param bool    $available
     */
    private function checkForMainFormElements(Crawler $crawler, bool $available): void
    {
        $this->assertEquals($available * 1, $crawler->filter('form[id="action_selection"]')->count());
        $this->assertEquals($available * 1, $crawler->filter('button[type="submit"]')->count());
        $this->assertEquals($available * 11, $crawler->filter('input[type="checkbox"][name="member[]"]')->count());
        $this->assertEquals($available * 1, $crawler->filter('input[type="checkbox"][id="checkAll"]')->count());
    }


    /**
     * @param Crawler $crawler
     * @param bool    $available
     * @param array   $options
     */
    private function checkForActionSelect(Crawler $crawler, bool $available, array $options): void
    {
        $this->assertEquals($available * 1, $crawler->filter('select[name="action_select"]')->count());
        $this->assertEquals($available * count($options), $crawler->filter('select[name="action_select"]>option')->count());
        foreach ($options as $option) {
            $this->assertEquals($available * 1, $crawler->filter('select[name="action_select"]>option:contains("'.$option.'")')->count());
        }
    }


    /**
     * @param Crawler $crawler
     * @param string  $selectName
     * @param string  $display
     * @param bool    $available
     * @param int     $optionCount
     */
    private function checkForSelect(Crawler $crawler, string $selectName, string $display, bool $available, int $optionCount): void
    {
        $this->assertEquals($available * 1, $crawler->filter('select[name="'.$selectName.'"]')->count());
        $this->assertEquals($available * 1, $crawler->filter('select[name="'.$selectName.'"][style="display: '.$display.'"]')->count());
        $this->assertEquals($available * $optionCount, $crawler->filter('select[name="'.$selectName.'"]>option')->count());
    }


    /**
     * Check if the generated serial letter is correct
     *
     * @param KernelBrowser $client
     * @param int           $expectedSize    Expected file size in kiB
     * @param array         $expectedMembers List of expected members, i.e. [2, 3] for Lastname2 and Lastname3. When
     *                                       there are multiple names in the letter, enter the last one in the opening
     *                                       line.
     */
    private function checkSerialLetter(KernelBrowser $client, int $expectedSize, array $expectedMembers): void
    {
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertEquals('application/pdf', $client->getResponse()->headers->get('content_type'));
        $this->assertStringContainsString('max-age=0', $client->getResponse()->headers->get('cache-control'));
        $this->assertGreaterThan($expectedSize - 3, $client->getResponse()->headers->get('content-length') / 1024);
        $this->assertLessThan($expectedSize + 3, $client->getResponse()->headers->get('content-length') / 1024);
        /** @var SerialLetterGenerator $serialLetterGenerator */
        $serialLetterGenerator = $this->getContainer()->get(SerialLetterGenerator::class);

        $this->assertEquals(count($expectedMembers), substr_count($serialLetterGenerator->getCurrentData(), 'opening'));
        foreach ($expectedMembers as $expectedMember) {
            $this->assertMatchesRegularExpression(
                "/opening.*\n.*Lastname$expectedMember, .*\n/",
                $serialLetterGenerator->getCurrentData(),
                "Expected member Firstname$expectedMember not found!"
            );
        }
    }
}
