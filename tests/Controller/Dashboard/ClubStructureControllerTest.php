<?php

/*
 * Copyright (C) 2022 by Luc Hamers <vereniging@hamers.de>
 *
 * This file is part of Vereniging.
 *
 * Vereniging is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Vereniging is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Vereniging.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Tests\Controller\Dashboard;

use App\Tests\WebTestCase;
use Exception;

/**
 * Test for the club structure controller.
 *
 */
class ClubStructureControllerTest extends WebTestCase
{
    /**
     * Setup the test environment, i.e. load fixture data.
     *
     * @throws Exception
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->loadAllFixtures();
    }


    /**
     * Test if there is a link on the dashboard page to the new member page.
     *
     * @throws Exception
     */
    public function testLinkOnDashboardPage(): void
    {
        $this->checkLinkOnDashboardPage($this, 'Club structure', 'Club structure', '/dashboard');
    }


    /**
     * Test the index controller.
     *
     * @throws Exception
     */
    public function testIndex(): void
    {
        $client = $this->getMyClient();

        $crawler = $client->request('GET', '/dashboard/clubstructure');

        $this->assertEquals(1, $crawler->filter('title:contains("Club structure")')->count());
        $this->assertEquals(1, $crawler->filter('h6.card-header:contains("Board")')->count());
        $this->assertEquals(1, $crawler->filter('h6.card-header:contains("Activities")')->count());
        $this->assertEquals(1, $crawler->filter('h6.card-header:contains("Public relations")')->count());

        $this->assertEquals(3, $crawler->filter('td:contains("Chairman")')->count());
        $this->assertEquals(1, $crawler->filter('td:contains("Vice chairman")')->count());
        $this->assertEquals(3, $crawler->filter('td:contains("Member")')->count());

        $this->assertEquals(5, $crawler->filter('a[href$="member/firstname1.lastname1"]')->count());
        $this->assertEquals(5, $crawler->filter('a:contains("Dipl.-Ing. Firstname1 Lastname1")')->count());
        $this->assertEquals(2, $crawler->filter('a[href$="member/firstname2.lastname2"]')->count());
        $this->assertEquals(2, $crawler->filter('a:contains("Firstname2 Lastname2 M.A.")')->count());
    }


    /**
     * Test if links are disabled when the user doesn't have the right to edit other members.
     *
     * @throws Exception
     */
    public function testLinkDisable(): void
    {
        $client = $this->getMyClient('firstname2.lastname2');

        $crawler = $client->request('GET', '/dashboard/clubstructure');
        // All links should be absent
        $this->assertEquals(0, $crawler->filter('a[href$="member/firstname1.lastname1"]')->count());
        $this->assertEquals(0, $crawler->filter('a:contains("Dipl.-Ing. Firstname1 Lastname1")')->count());
        $this->assertEquals(0, $crawler->filter('a[href$="member/firstname2.lastname2"]')->count());
        $this->assertEquals(0, $crawler->filter('a:contains("Firstname2 Lastname2 M.A.")')->count());

        $this->assertEquals(5, $crawler->filter('td:contains("Dipl.-Ing. Firstname1 Lastname1")')->count());
        $this->assertEquals(2, $crawler->filter('td:contains("Firstname2 Lastname2 M.A.")')->count());
    }
}
