<?php

/*
 * Copyright (C) 2022 by Luc Hamers <vereniging@hamers.de>
 *
 * This file is part of Vereniging.
 *
 * Vereniging is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Vereniging is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Vereniging.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Tests\Controller\Dashboard;

use App\Tests\WebTestCase;
use Exception;

/**
 * Test the default dashboard controller.
 *
 */
class DefaultControllerTest extends WebTestCase
{
    /**
     * @inheritDoc
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->loadAllFixtures();
    }


    /**
     * Test the main dashboard page as system administrator with full access.
     *
     * @throws Exception
     */
    public function testIndexAsMemberAdministrator(): void
    {
        $client = $this->getMyClient('firstname3.lastname3');
        $crawler = $client->request('GET', '/dashboard');

        $this->assertEquals(1, $crawler->filter('a.nav-link:contains("Administration")')->count());

        $this->assertEquals(1, $crawler->filter('h6.card-header:contains("Information")')->count());
        $this->assertEquals(1, $crawler->filter('h6.card-header:contains("Tools")')->count());

        $this->assertEquals(1, $crawler->filter('html:contains("My data")')->count());
        $this->assertEquals(1, $crawler->filter('html:contains("Version")')->count());
        $this->assertEquals(1, $crawler->filter('html:contains("Member lists")')->count());
        $this->assertEquals(1, $crawler->filter('html:contains("Club structure")')->count());
        $this->assertEquals(1, $crawler->filter('html:contains("Company information")')->count());
        $this->assertEquals(1, $crawler->filter('html:contains("Serial letters")')->count());
        $this->assertEquals(1, $crawler->filter('html:contains("Statistics")')->count());
        $this->assertEquals(1, $crawler->filter('html:contains("Log file")')->count());
        $this->assertEquals(1, $crawler->filter('html:contains("New user")')->count());
        // Check if the link to edit my data is correctly to the first member.
        $this->assertStringContainsString("member/firstname3.lastname3", $crawler->selectLink('My data')->link()->getUri());
    }


    /**
     * Test the main dashboard page as member with limited access.
     *
     * @throws Exception
     */
    public function testIndexAsMember(): void
    {
        $client = $this->getMyClient('firstname2.lastname2');

        $crawler = $client->request('GET', '/dashboard');

        $this->assertEquals(0, $crawler->filter('a.nav-link:contains("Administration")')->count());

        $this->assertEquals(1, $crawler->filter('h6.card-header:contains("Information")')->count());
        $this->assertEquals(0, $crawler->filter('h6.card-header:contains("Tools")')->count());

        $this->assertEquals(1, $crawler->filter('html:contains("My data")')->count());
        $this->assertEquals(1, $crawler->filter('html:contains("Member lists")')->count());
        $this->assertEquals(1, $crawler->filter('html:contains("Club structure")')->count());
        $this->assertEquals(1, $crawler->filter('html:contains("Company information")')->count());
        $this->assertEquals(0, $crawler->filter('html:contains("Serial letters")')->count());
        $this->assertEquals(0, $crawler->filter('html:contains("Statistics")')->count());
        $this->assertEquals(0, $crawler->filter('html:contains("Log file")')->count());
        $this->assertEquals(0, $crawler->filter('html:contains("Create new user")')->count());
        // Check if the link to edit my data is correctly to the first member.
        $this->assertStringContainsString("member/firstname2.lastname2", $crawler->selectLink('My data')->link()->getUri());
    }


    /**
     * Test the about page.
     *
     * @throws Exception
     */
    public function testAboutPage(): void
    {
        $client = $this->getMyClient();

        $crawler = $client->request('GET', '/dashboard/about');
        $this->assertEquals(1, $crawler->filter('h6.card-header:contains("About this software")')->count());
        $this->assertEquals(1, $crawler->filter('h6.card-header:contains("Technical information")')->count());
        $this->assertEquals(1, $crawler->filter('h6.card-header:contains("Development history")')->count());
    }
}
