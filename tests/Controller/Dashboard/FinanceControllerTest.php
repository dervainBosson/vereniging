<?php

/*
 * Copyright (C) 2022 by Luc Hamers <vereniging@hamers.de>
 *
 * This file is part of Vereniging.
 *
 * Vereniging is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Vereniging is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Vereniging.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Tests\Controller\Dashboard;

use App\Entity\MemberEntry;
use App\Entity\Status;
use App\Service\SerialLetterGenerator;
use App\Tests\WebTestCase;
use DateTime;
use Exception;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Component\DomCrawler\Crawler;
use Symfony\Component\DomCrawler\Form;

/**
 * Test the finance controller.
 *
 */
class FinanceControllerTest extends WebTestCase
{
    /**
     * Setup the test environment, i.e. load fixture data.
     *
     * @throws Exception
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->loadAllFixtures();
    }


    /**
     * Test if there is a link on the dashboard page to the new member page.
     *
     * @throws Exception
     */
    public function testLinkOnDashboardPage(): void
    {
        $this->checkLinkOnDashboardPage($this, 'Manage finances', 'Manage finances', '/dashboard/manage_finances');
    }


    /**
     * Test the manage finance page.
     *
     * @throws Exception
     */
    public function testIndex(): void
    {
        $client = $this->getMyClient();
        $crawler = $client->request('GET', '/dashboard/manage_finances');

        // Check the page title
        $this->assertEquals(1, $crawler->filter('title:contains("Manage finances")')->count());

        // Check the navigation links
        $this->assertEquals(1, $crawler->filter('a.navbar-brand:contains("the Vereniging member system dashboard")')->count());
        $this->assertEquals(1, $crawler->filter('a.nav-link:contains("Manage finances")')->count());

        // Check frame
        $this->assertEquals(1, $crawler->filter('h6.card-header:contains("Membership list")')->count());

        $expectedHeadings = ['', 'Membership number', 'Members', 'Membership type', 'Contribution claim', 'Use direct debit', 'Outstanding debt', 'Transactions', '', 'Amount', 'Opened', 'Closed'];
        $this->checkTableHeaders($expectedHeadings, $crawler, '#action_selection');

        // Check the number of membership number entries in the list.
        $this->assertEquals(9, $crawler->filter('#action_selection input[type="checkbox"]')->count());

        // Check if the correct widgets have been selected. The content of the widgets is already checked by
        // MemberSelectionFormHandlerTest, so we do not have to check them here.
        $expectedWidgets = ['User status', 'Membership type', 'Email address', 'Debt', 'Others'];
        $this->checkContentAsArray($expectedWidgets, $crawler, '#selection_form div.col-md-2 legend');

        $this->checkMembershipTable($crawler);
    }


    /**
     * Test if the selections show the correct entries
     *
     * @throws Exception
     */
    public function testSelections(): void
    {
        $client = $this->getMyClient();
        $crawler = $client->request('GET', '/dashboard/manage_finances');

        // Submit form with all checkboxes cleared --> the content should be empty
        $form = $crawler->filter("#selection_form")->form();
        $form['member_lists_selection[status]'][0]->untick();
        $crawler = $client->submit($form);
        $this->assertEquals(1, $crawler->filter('div.card-body:contains("No member entries found")')->count());

        // Select members with membership end requested
        $form['member_lists_selection[others][membershipEndRequested]']->tick();
        $expectedLines = [
            ['', '3', 'Firstname3 Lastname3', 'Regular member', '€24.00', 'no', '€24.00', '€24.00Details', 'Nov 29, 2020', '-'],
        ];
        $this->checkMembershipNumberEntries($client, $form, $expectedLines);

        // Select regular members
        $form['member_lists_selection[others][membershipEndRequested]']->untick();
        $form['member_lists_selection[membershipType][2]']->tick();
        $expectedLines = [
            ['', '2', 'Firstname2 Lastname2', 'Regular member', '€24.00', 'no', '€20.00', '€20.00€24.00Details', 'Nov 28, 2020Nov 20, 2020', '-Nov 25, 2020'],
            ['', '3', 'Firstname3 Lastname3', 'Regular member', '€24.00', 'no', '€24.00', '€24.00Details', 'Nov 29, 2020', '-'],
            ['', '4', 'Firstname5 Lastname5', 'Regular member', '€24.00', 'no', '-', '-'],
            ['', '5', 'Firstname6 Lastname6', 'Regular member', '€24.00', 'no', '-', '-'],
            ['', '6', 'Firstname7 Lastname7', 'Regular member', '€24.00', 'no', '-', '-'],
            ['', '8', 'Firstname9 Lastname9', 'Regular member', '€24.00', 'no', '-', '-'],
            ['', '20', 'Firstname8 Lastname8', 'Regular member', '€24.00', 'no', '-', '-'],
        ];

        $this->checkMembershipNumberEntries($client, $form, $expectedLines);
    }


    /**
     * The the serial letter action for the finance table
     *
     * @throws Exception
     */
    public function testSerialLetterAction(): void
    {
        $client = $this->getMyClient();
        $crawler = $client->request('GET', '/dashboard/manage_finances');

        // Submit without any selected members, the response should be empty, the responds code is 204 (no content)
        $form = $crawler->filter("#action_selection")->form();
        $crawler = $client->submit($form);
        $this->assertEquals(204, $client->getResponse()->getStatusCode());
        $this->assertNull($crawler->getNode(0));

        // Select members 2, 3 and 5
        $form['membershipNumber'][1]->tick();
        $form['membershipNumber'][2]->tick();
        $form['membershipNumber'][3]->tick();
        $client->submit($form);
        $this->checkSerialLetter($client, 32, [2, 3, 5]);
    }


    /**
     * Test changing the members status
     *
     * @throws Exception
     */
    public function testChangeStatus(): void
    {
        $client = $this->getMyClient();
        $crawler = $client->request('GET', '/dashboard/manage_finances');

        // Submit without any selected members, the response should be empty, the responds code is 204 (no content)
        $form = $crawler->filter("#action_selection")->form();
        $form['action_select']->select('change_status');
        $futureMembers = $this->getEntityManager()->getRepository(Status::class)->findOneBy(['status' => 'future_member']);
        $form['status_select']->select($futureMembers->getId());

        $crawler = $client->submit($form);
        $this->assertEquals(204, $client->getResponse()->getStatusCode());
        $this->assertNull($crawler->getNode(0));

        // Select the first membership number. This should change the status of members 1, 10, 11 and 12
        $form['membershipNumber'][0]->tick();
        $client->submit($form);
        $this->assertEquals(204, $client->getResponse()->getStatusCode());
        $members = $this->getEntityManager()->getRepository(MemberEntry::class)->findBy(['firstName' => ['Firstname1', 'Firstname10', 'Firstname11', 'Firstname12']]);
        /** @var MemberEntry $member */
        foreach ($members as $member) {
            $this->getEntityManager()->refresh($member);
            $this->assertEquals($futureMembers->getStatus(), $member->getStatus()->getStatus());
        }
    }


    /**
     * Test creating membership fee transactions
     *
     * @throws Exception
     */
    public function testGenerateMembershipFeeTransactions(): void
    {
        $client = $this->getMyClient();
        $crawler = $client->request('GET', '/dashboard/manage_finances');

        // Submit without any selected members, the response should be empty, the responds code is 204 (no content)
        $form = $crawler->filter("#action_selection")->form();
        $form['action_select']->select('generate_transactions');

        $crawler = $client->submit($form);
        $this->assertEquals(204, $client->getResponse()->getStatusCode());
        $this->assertNull($crawler->getNode(0));

        // Select the first and second membership number. This should change the status of members 1, 2, 10, 11 and 12
        $form['membershipNumber'][0]->tick();
        $form['membershipNumber'][1]->tick();
        $client->submit($form);

        $crawler = $client->request('GET', '/dashboard/manage_finances');
        $todayString = (new DateTime())->format('M j, Y');

        $changedLines = [
            0 => ['', '1', 'Firstname1 Lastname1 Firstname10 Lastname10 Firstname11 Lastname1 Firstname12 Lastname12', 'Family member', '€36.00', 'yes', '€124.00', '€36.00€52.00Details', $todayString.'Nov 29, 2020', '--'],
            1 => ['', '2', 'Firstname2 Lastname2', 'Regular member', '€24.00', 'no', '€44.00', '€24.00€20.00Details', $todayString.'Nov 28, 2020', "--"],
        ];
        $this->checkMembershipTable($crawler, $changedLines);
    }


    /**
     * Test closing membership fee transactions
     *
     * @throws Exception
     */
    public function testCloseMembershipFeeTransactions(): void
    {
        $client = $this->getMyClient();
        $crawler = $client->request('GET', '/dashboard/manage_finances');

        // Submit without any selected members, the response should be empty, the responds code is 204 (no content)
        $form = $crawler->filter("#action_selection")->form();
        $form['action_select']->select('transactions_paid');

        $crawler = $client->submit($form);
        $this->assertEquals(204, $client->getResponse()->getStatusCode());
        $this->assertNull($crawler->getNode(0));

        // Select the first and second membership number. This should change the status of members 1, 2, 10, 11 and 12
        $form['membershipNumber'][0]->tick();
        $form['membershipNumber'][1]->tick();
        $client->submit($form);

        $crawler = $client->request('GET', '/dashboard/manage_finances');
        $todayString = (new DateTime())->format('M j, Y');

        $changedLines = [
            0 => ['', '1', 'Firstname1 Lastname1 Firstname10 Lastname10 Firstname11 Lastname1 Firstname12 Lastname12', 'Family member', '€36.00', 'yes', '€52.00', '€52.00€36.00Details', 'Nov 29, 2020Nov 28, 2020', "-$todayString"],
            1 => ['', '2', 'Firstname2 Lastname2', 'Regular member', '€24.00', 'no', '-', '€20.00€24.00Details', 'Nov 28, 2020Nov 20, 2020', $todayString.'Nov 25, 2020'],
        ];
        $this->checkMembershipTable($crawler, $changedLines);
    }


    /**
     * Test changing the direct debit settings
     *
     * @throws Exception
     */
    public function testChangeDirectDebitSettings(): void
    {
        $client = $this->getMyClient();
        $crawler = $client->request('GET', '/dashboard/manage_finances');

        // Submit without any selected members, the response should be empty, the responds code is 204 (no content)
        $form = $crawler->filter("#action_selection")->form();
        $form['action_select']->select('set_use_direct_debit');

        $crawler = $client->submit($form);
        $this->assertEquals(204, $client->getResponse()->getStatusCode());
        $this->assertNull($crawler->getNode(0));

        // Select the second and third membership number. This should change the status of members 2 and 3
        $form['membershipNumber'][1]->tick();
        $form['membershipNumber'][2]->tick();
        $client->submit($form);

        $client->request('GET', '/dashboard/manage_finances');
        $members = $this->getEntityManager()->getRepository(MemberEntry::class)->findBy(['firstName' => ['Firstname2', 'Firstname3']]);
        /** @var MemberEntry $member */
        foreach ($members as $member) {
            $this->getEntityManager()->refresh($member);
            $this->assertTrue($member->getMembershipNumber()->getUseDirectDebit());
        }

        // Select the first, second and third membership number. This should change the status of members 2 and 3
        $form['action_select']->select('remove_use_direct_debit');
        $form['membershipNumber'][0]->tick();
        $form['membershipNumber'][1]->tick();
        $form['membershipNumber'][2]->tick();
        $client->submit($form);

        $client->request('GET', '/dashboard/manage_finances');
        $members = $this->getEntityManager()->getRepository(MemberEntry::class)->findBy(['firstName' => ['Firstname1', 'Firstname2', 'Firstname3', 'Firstname10', 'Firstname11', 'Firstname12']]);
        /** @var MemberEntry $member */
        foreach ($members as $member) {
            $this->getEntityManager()->refresh($member);
            $this->assertFalse($member->getMembershipNumber()->getUseDirectDebit());
        }
    }


    /**
     * Test if the members have links to edit them
     *
     * @throws Exception
     */
    public function testLinkToMembers(): void
    {
        $client = $this->getMyClient();
        $crawler = $client->request('GET', '/dashboard/manage_finances');
        $link = $crawler->selectLink('Firstname2 Lastname2')->link();
        $crawler = $client->click($link);

        $this->assertPageTitleContains('View and edit data');
        $this->assertCount(1, $crawler->filter('#club_data div.col-sm-10:contains("firstname2.lastname2")'));
    }


    /**
     * Test if the details link on a membership number links to the correct transaction edit page
     *
     * @throws Exception
     */
    public function testLinkToTransactions(): void
    {
        $client = $this->getMyClient();
        $crawler = $client->request('GET', '/dashboard/manage_finances');
        $link = $crawler->filter('a[href$="edit_transactions/2"]')->link();
        $crawler = $client->click($link);

        $this->assertPageTitleContains('Edit contribution claims');
        $this->assertCount(1, $crawler->filter('h6:contains("Contribution claims for membership number 2 (Firstname2 Lastname2)")'));
    }


    /**
     * Test if the transactions edit page is shown correctly
     *
     * @throws Exception
     */
    public function testShowEditTransactions(): void
    {
        $client = $this->getMyClient();
        $crawler = $client->request('GET', '/dashboard/manage_finances/edit_transactions/2');

        // Check headers
        $this->checkTableHeaders(['', 'Amount', 'Opened', 'Closed'], $crawler, "#transaction-form");

        // Check buttons
        $this->assertCount(1, $crawler->filter('tbody > tr > td > a:contains("Add contribution claim")'));
        $this->assertCount(1, $crawler->filter('button[type="submit"]:contains("Save")'));
        $this->assertCount(1, $crawler->filter('a[href$="manage_finances"]:contains("Cancel")'));

        // Check if the javascript classes used for jquery are present
        $this->assertCount(1, $crawler->filter('tbody.js-transaction-table'));
        $this->assertCount(1, $crawler->filter('tbody[data-prototype]'));
        $this->assertCount(1, $crawler->filter('tbody[data-index="2"]'));
        $this->assertCount(2, $crawler->filter('tbody > tr.js-transaction-item'));
        $this->assertCount(2, $crawler->filter('tbody > tr > td > a.js-remove-transaction'));

        // Check if the input fields are present
        $this->assertCount(1, $crawler->filter('tr#transaction_0>td input[type="text"][value="20.00"]'));
        $this->assertCount(1, $crawler->filter('tr#transaction_1>td input[type="text"][value="24.00"]'));
        $this->checkDateFormWidget(new DateTime('2020-11-28'), 'transaction_0', 'createDate', $crawler);
        $this->checkDateFormWidget(new DateTime('2020-11-20'), 'transaction_1', 'createDate', $crawler);
        $this->checkDateFormWidget(new DateTime('2020-11-25'), 'transaction_1', 'closeDate', $crawler);
        $this->assertCount(0, $crawler->filter('tr#transaction_0>td select[name$="[closeDate][month]"] option[selected="selected"]'));
        $this->assertCount(0, $crawler->filter('tr#transaction_0>td select[name$="[closeDate][day]"] option[selected="selected"]'));
        $this->assertCount(0, $crawler->filter('tr#transaction_0>td select[name$="[closeDate][year]"] option[selected="selected"]'));

        // Check if cancel works. This should show the membership list without any changes
        $link = $crawler->selectLink('Cancel')->link();
        $crawler = $client->click($link);

        $this->checkMembershipTable($crawler);
    }


    /**
     * Edit transactions: change content
     *
     * @throws Exception
     */
    public function testEditTransactionsChange(): void
    {
        $this->clearAllLogFiles();
        $client = $this->getMyClient();

        // Change some values
        $crawler = $client->request('GET', '/dashboard/manage_finances/edit_transactions/2');
        $buttonCrawlerNode = $crawler->selectButton('Save');
        $form = $buttonCrawlerNode->form();
        $form['form_collection[members][0][amount]'] = 22.5;
        $form['form_collection[members][0][createDate][month]'] = 1;
        $form['form_collection[members][0][createDate][day]'] = 1;
        $form['form_collection[members][0][createDate][year]'] = 2019;
        $form['form_collection[members][0][closeDate][month]'] = 2;
        $form['form_collection[members][0][closeDate][day]'] = 2;
        $form['form_collection[members][0][closeDate][year]'] = 2020;

        $client->followRedirects(true);
        $crawler = $client->submit($form);

        $changedLines = [
            1 => ['', '2', 'Firstname2 Lastname2', 'Regular member', '€24.00', 'no', '-', '€24.00€22.50Details', 'Nov 20, 2020Jan 1, 2019', 'Nov 25, 2020Feb 2, 2020'],
        ];
        $this->checkMembershipTable($crawler, $changedLines);
        $logMessages = $this->getAllLogs();
        $this->assertCount(2, $logMessages);
        $this->assertEquals("Closed contribution claim from Nov 28, 2020 of 20.00 for membership number 2", $logMessages[0]->getLogentry());
        $this->assertEquals('{"numberOfDataFields":2,"mainMessage":"Updated %classname% entry %stringIdentifier%|membershipFeeTransaction,","dataArray":{"Membership number":[2,2],"Amount":["20.00","22.50"],"Opened":["Nov 28, 2020","Jan 1, 2019"],"Closed":["Feb 2, 2020","Feb 2, 2020"]}}', $logMessages[1]->getLogentry());
    }


    /**
     * Edit transactions: remove content
     *
     * @throws Exception
     */
    public function testEditTransactionsRemove(): void
    {
        $this->clearAllLogFiles();
        $client = $this->getMyClient();

        // Change some values
        $crawler = $client->request('GET', '/dashboard/manage_finances/edit_transactions/2');
        $buttonCrawlerNode = $crawler->selectButton('Save');
        $form = $buttonCrawlerNode->form();
        unset($form['form_collection[members][0]']);

        $client->followRedirects(true);
        $crawler = $client->submit($form);

        $changedLines = [
            1 => ['', '2', 'Firstname2 Lastname2', 'Regular member', '€24.00', 'no', '-', '€24.00Details', 'Nov 20, 2020', 'Nov 25, 2020'],
        ];
        $this->checkMembershipTable($crawler, $changedLines);
        $logMessages = $this->getAllLogs();
        $this->assertCount(1, $logMessages);
        $this->assertEquals("Deleted contribution claim from Nov 28, 2020 of 20.00 for membership number 2", $logMessages[0]->getLogentry());
    }


    /**
     * Edit transactions: add content
     *
     * @throws Exception
     */
    public function testEditTransactionsAdd(): void
    {
        $this->clearAllLogFiles();
        $client = $this->getMyClient();

        // Change some values
        $crawler = $client->request('GET', '/dashboard/manage_finances/edit_transactions/2');
        $buttonCrawlerNode = $crawler->selectButton('Save');
        $form = $buttonCrawlerNode->form();
        $data = $form->getPhpValues();
        $today = new DateTime();
        $data['form_collection']['members'][] = [
            'amount' => 55,
            'createDate' => [
                'month' => $today->format('n'),
                'day' => $today->format('j'),
                'year' => $today->format('Y'),
            ],
        ];

        $client->followRedirects(true);
        $crawler = $client->request('POST', '/dashboard/manage_finances/edit_transactions/2', $data);

        $todayString = (new DateTime())->format('M j, Y');

        $changedLines = [
            1 => ['', '2', 'Firstname2 Lastname2', 'Regular member', '€24.00', 'no', '€75.00', '€55.00€20.00Details', $todayString.'Nov 28, 2020', '--'],
        ];
        $this->checkMembershipTable($crawler, $changedLines);
        $logMessages = $this->getAllLogs();
        $this->assertCount(1, $logMessages);
        $this->assertEquals('Created contribution claim of 55.00 for membership number 2', $logMessages[0]->getLogentry());
    }


    /**
     * @param KernelBrowser $client        Client to submit to
     * @param Form          $form          Form to submit
     * @param array[]       $expectedLines Lines with the texts which are expected
     */
    private function checkMembershipNumberEntries(KernelBrowser $client, Form $form, array $expectedLines): void
    {
        $crawler = $client->submit($form);
        $this->assertCount(count($expectedLines) + 1, $crawler->filter('#action_selection input[type="checkbox"]'));

        $lineCounter = 3;
        foreach ($expectedLines as $expectedLine) {
            $this->checkContentAsArray($expectedLine, $crawler, "#action_selection table tr:nth-child($lineCounter) td", " in line $lineCounter");
            $lineCounter++;
        }
    }


    /**
     * Check if the generated serial letter is correct
     *
     * @param KernelBrowser $client
     * @param int           $expectedSize    Expected file size in kiB
     * @param array         $expectedMembers List of expected members, i.e. [2, 3] for Lastname2 and Lastname3. When
     *                                       there are multiple names in the letter, enter the last one in the opening
     *                                       line.
     */
    private function checkSerialLetter(KernelBrowser $client, int $expectedSize, array $expectedMembers): void
    {
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertEquals('application/pdf', $client->getResponse()->headers->get('content_type'));
        $this->assertStringContainsString('max-age=0', $client->getResponse()->headers->get('cache-control'));
        $this->assertGreaterThan($expectedSize - 3, $client->getResponse()->headers->get('content-length') / 1024);
        $this->assertLessThan($expectedSize + 3, $client->getResponse()->headers->get('content-length') / 1024);
        /** @var SerialLetterGenerator $serialLetterGenerator */
        $serialLetterGenerator = $this->getContainer()->get(SerialLetterGenerator::class);

        $this->assertEquals(count($expectedMembers), substr_count($serialLetterGenerator->getCurrentData(), 'opening'));
        foreach ($expectedMembers as $expectedMember) {
            $this->assertMatchesRegularExpression(
                "/opening.*\n.*Lastname$expectedMember, .*\n/",
                $serialLetterGenerator->getCurrentData(),
                "Expected member Firstname$expectedMember not found!"
            );
        }
    }


    /**
     * Search for a date form (which contains a month, a day and a year) and check if the selected date matches the
     * expected days in the past
     *
     * @param DateTime $date       Defines the expected date
     * @param string   $rowId      html id of the tr element
     * @param string   $selectName Name of the select fields (i.e. [name][month], [name][day]
     * @param Crawler  $crawler
     */
    private function checkDateFormWidget(DateTime $date, string $rowId, string $selectName, Crawler $crawler): void
    {
        $month = $date->format('M');
        $day = $date->format('j');
        $year = $date->format('Y');
        $this->assertCount(1, $crawler->filter('tr#'.$rowId.'>td select[name$="['.$selectName.'][month]"] option[selected="selected"]:contains("'.$month.'")'));
        $this->assertCount(1, $crawler->filter('tr#'.$rowId.'>td select[name$="['.$selectName.'][day]"] option[selected="selected"]:contains("'.$day.'")'));
        $this->assertCount(1, $crawler->filter('tr#'.$rowId.'>td select[name$="['.$selectName.'][year]"] option[selected="selected"]:contains("'.$year.'")'));
    }


    /**
     * Check the finance table
     *
     * @param Crawler    $crawler
     * @param array|null $changedContent Pass if some of the lines are expected to be changed.
     */
    private function checkMembershipTable(Crawler $crawler, array $changedContent = null): void
    {
        $expectedLines = [
            [
                '',
                '1',
                'Firstname1 Lastname1 Firstname10 Lastname10 Firstname11 Lastname1 Firstname12 Lastname12',
                'Family member',
                '€36.00',
                'yes',
                '€88.00',
                '€52.00€36.00Details',
                "Nov 29, 2020Nov 28, 2020",
                '--',
            ],
            [
                '',
                '2',
                'Firstname2 Lastname2',
                'Regular member',
                '€24.00',
                'no',
                '€20.00',
                '€20.00€24.00Details',
                "Nov 28, 2020Nov 20, 2020",
                "-Nov 25, 2020",
            ],
            [
                '',
                '3',
                'Firstname3 Lastname3',
                'Regular member',
                '€24.00',
                'no',
                '€24.00',
                '€24.00Details',
                "Nov 29, 2020",
                '-',
            ],
            ['', '4', 'Firstname5 Lastname5', 'Regular member', '€24.00', 'no', '-', '-'],
            ['', '5', 'Firstname6 Lastname6', 'Regular member', '€24.00', 'no', '-', '-'],
            ['', '6', 'Firstname7 Lastname7', 'Regular member', '€24.00', 'no', '-', '-'],
            ['', '8', 'Firstname9 Lastname9', 'Regular member', '€24.00', 'no', '-', '-'],
            ['', '20', 'Firstname8 Lastname8', 'Regular member', '€24.00', 'no', '-', '-'],
        ];

        if ($changedContent) {
            foreach ($changedContent as $index => $content) {
                $expectedLines[$index] = $content;
            }
        }

        $lineCounter = 3;
        foreach ($expectedLines as $expectedLine) {
            $this->checkContentAsArray(
                $expectedLine,
                $crawler,
                "#action_selection table tr:nth-child($lineCounter) td",
                " in line $lineCounter"
            );
            $lineCounter++;
        }
    }
}
