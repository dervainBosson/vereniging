<?php

/*
 * Copyright (C) 2022 by Luc Hamers <vereniging@hamers.de>
 *
 * This file is part of Vereniging.
 *
 * Vereniging is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Vereniging is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Vereniging.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Tests\Controller\Admin;

use App\Entity\LogfileEntry;
use App\Entity\MemberEntry;
use Exception;
use Symfony\Component\DomCrawler\Crawler;
use Symfony\Component\Mime\Email;
use Symfony\Component\PasswordHasher\Hasher\PasswordHasherFactory;

/**
 * Test class for the password controller.
 *
 */
class PasswordControllerTest extends TestWithMemberSelection
{
    /**
     * @inheritDoc
     */
    public function setUp(): void
    {
        parent::setUp();
    }


    /**
     * Test if there is a link on the dashboard page to the new member page.
     *
     * @throws Exception
     */
    public function testLinkOnDashboardPage(): void
    {
        $this->checkLinkOnDashboardPage($this, 'Manage passwords', 'Manage passwords', '/administration');
    }


    /**
     * Test if the selection form on this page works correctly
     *
     * @throws Exception
     */
    public function testSelectionForm(): void
    {
        $this->checkMemberSelectionForm('/administration/manage_passwords');
    }


    /**
     * Test the index controller.
     *
     * @throws Exception
     */
    public function testIndex(): void
    {
        $client = $this->getMyClient();

        $crawler = $client->request('GET', '/administration/manage_passwords');

        // Check the page title
        $this->assertEquals(1, $crawler->filter('title:contains("Administration")')->count());
        $this->assertEquals(1, $crawler->filter('title:contains("Manage passwords")')->count());

        // Check the navigation links
        $this->assertEquals(1, $crawler->filter('a.navbar-brand:contains("the Vereniging member system dashboard")')->count());
        $this->assertEquals(2, $crawler->filter('a.nav-link:contains("Administration")')->count());
        $this->assertEquals(1, $crawler->filter('a.nav-link:contains("Manage passwords")')->count());

        // Check the table headings
        $this->assertEquals(1, $crawler->filter('th:contains("First name")')->count());
        $this->assertEquals(1, $crawler->filter('th:contains("Last name")')->count());
        $this->assertEquals(1, $crawler->filter('th:contains("Email")')->count());
        $this->assertEquals(1, $crawler->filter('th:contains("Password has")')->count());
        $this->assertEquals(1, $crawler->filter('th:contains("been set")')->count());
        $this->assertEquals(1, $crawler->filter('th:contains("Send or update password")')->count());

        $this->checkTableLine($crawler, 'Firstname1', true, false, true, false);
        $this->checkTableLine($crawler, 'Firstname11', false, true, false, false);
        $this->checkTableLine($crawler, 'Firstname10', false, true, false, false);
        $this->checkTableLine($crawler, 'Firstname12', false, true, false, false);
        $this->checkTableLine($crawler, 'Firstname2', true, false, false, true);
        $this->checkTableLine($crawler, 'Firstname3', true, false, true, true);
        $this->checkTableLine($crawler, 'Firstname5', false, true, false, false);
        $this->checkTableLine($crawler, 'Firstname6', false, true, false, false);
        $this->checkTableLine($crawler, 'Firstname7', false, true, false, false);
        $this->checkTableLine($crawler, 'Firstname8', false, true, false, false);
        $this->checkTableLine($crawler, 'Firstname9', false, true, false, false);

        // Check the number of buttons in the table
        $this->assertEquals(2, $crawler->filter('a:contains("Send new password")')->count());
        $this->assertEquals(8, $crawler->filter('a:contains("Send password")')->count());
        $this->assertEquals(2, $crawler->filter('a:contains("Revoke access")')->count());
    }


    /**
     * Test the show action.
     *
     * @throws Exception
     */
    public function testShow(): void
    {
        $client = $this->getMyClient();

        $crawler = $client->request('GET', '/administration/manage_passwords');
        $form = $crawler->filter('#selection_form')->form();
        // Submit with default settings (status = member and user rule is not set)
        $crawler = $client->submit($form);
        $this->assertEquals(11, $crawler->filter('tr:contains("Firstname")')->count());

        // Add future member status. This should increase the number of members from 11 to 12.
        $form['member_lists_selection[status]'][1]->tick();
        $crawler = $client->submit($form);
        $this->assertEquals(12, $crawler->filter('tr:contains("Firstname")')->count());
    }


    /**
     * Test sending a new password.
     *
     * @throws Exception
     */
    public function testGenerateAndSendNewPassword(): void
    {
        $client = $this->getMyClient();

        $entityManager = $this->getEntityManager();
        /** @var MemberEntry $member */
        $member = $entityManager->getRepository(MemberEntry::class)->findOneBy(['name' => 'Lastname5']);
        $memberId = $member->getId();
        $this->assertNull($member->getPassword());

        // Generate a new password for Lastname5
        $crawler = $client->request('GET', "/administration/manage_passwords/new_password/$memberId");
        // Now there should be a flash message
        $this->assertStringContainsString('Changed password for Firstname5 Lastname5 and send an email with the new credentials to firstname5@email.com', $crawler->text(null, true));
        // The member now should have a password.
        $member = $entityManager->getRepository(MemberEntry::class)->find($member->getId());
        $entityManager->refresh($member);
        $this->assertNotNull($member->getPassword());

        $this->assertEmailCount(1);
        /** @var Email $email */
        $email = $this->getMailerMessage();
        // Check email settings
        $this->assertEmailHeaderSame($email, 'from', 'sender-login-nl@email.com');
        $this->assertEmailHeaderSame($email, 'to', 'firstname5@email.com');
        $this->assertEmailHeaderSame($email, 'subject', 'Nieuwe aanmeldgegevens voor Het Vereniging Ledensysteem');

        // The email with the login data is to be translated into Dutch. Since we do not know the password, we will
        // check the html content before and after the password.
        $expectedBeforePassword = <<<eof
            <p>login content: </p>
            <p>Geachte Firstname5 Lastname5,</p>
            <p>Uw aanmeldgegevens voor de pagina <a href="http://myserver.com/">Het Vereniging Ledensysteem</a> zijn veranderd:</p>
            <table>
                <tr><td>Gebruikersnaam:</td><td>firstname5.lastname5</td></tr>
                <tr><td>Wachtwoord:</td><td>
            eof;
        $expectedAfterPassword = <<<eof
            </td></tr>
            </table>
            <p>U kunt uw wachtwoord na het aanmelden veranderen in de afdeling "Mijn gegevens". Heeft U nog vragen, neem dan contact op met uw systeemadministrator door op deze email te antwoorden.</p>
            <p>Met vriendelijke groeten,</p>
            <p>Uw systeemadministrator</p>
            eof;


        // Check if the body text before and after the password is correct
        $this->assertEmailHtmlBodyContains($email, $expectedBeforePassword);
        $this->assertEmailHtmlBodyContains($email, $expectedAfterPassword);
        $this->assertEmailTextBodyContains($email, strip_tags($expectedBeforePassword));
        $this->assertEmailTextBodyContains($email, strip_tags($expectedAfterPassword));

        // Check if the password in the email is identical to that in the database, by checking of the user could login
        // with this password.
        $bodyCrawler = new Crawler($email->getHtmlBody());
        $password = $bodyCrawler->filter('td:contains("Wachtwoord")')->siblings()->text();
        /** @var PasswordHasherFactory $hasherService */
        $hasherService = $this->getContainer()->get('security.password_hasher_factory');
        $hasher = $hasherService->getPasswordHasher($member);
        $this->assertTrue($hasher->verify($member->getPassword(), $password, $member->getSalt()));

        // Check if the password change has been logged
        $logMessage = $entityManager->getRepository(LogfileEntry::class)->findOneBy([], ['id' => 'DESC']);
        $this->assertEquals('Changed password', $logMessage->getLogentry());
    }


    /**
     * Test Revoking the access.
     *
     * @throws Exception
     */
    public function testRevoke(): void
    {
        $client = $this->getMyClient();

        $entityManager = $this->getEntityManager();
        /** @var MemberEntry $member */
        $member = $entityManager->getRepository(MemberEntry::class)->findOneBy(['name' => 'Lastname3']);
        $memberId = $member->getId();
        $this->assertNotNull($member->getPassword());

        // Revoke password for Lastname3
        $crawler = $client->request('GET', "/administration/manage_passwords/revoke_password/$memberId");
        // Now there should be a flash message
        $this->assertStringContainsString('Revoked password for Firstname3 Lastname3.', $crawler->text(null, true));
        // The member now should have a password.
        $member = $entityManager->getRepository(MemberEntry::class)->find($member->getId());
        $entityManager->refresh($member);
        $this->assertNull($member->getPassword());

        // Check if the password change has been logged
        $logMessage = $entityManager->getRepository(LogfileEntry::class)->findOneBy([], ['id' => 'DESC']);
        $this->assertEquals('Revoked password', $logMessage->getLogentry());
    }


    /**
     * @param Crawler $crawler           Crawler to the current page
     * @param string  $memberName        Members name to look for
     * @param bool    $passwordAvailable True when for this member a password has been set
     * @param bool    $sendPassword      True when the button to send a password is available
     * @param bool    $sendNewPassword   True when the button to send a new password is available
     * @param bool    $revoke            True when the revoke-button is available
     */
    private function checkTableLine(Crawler $crawler, string $memberName, bool $passwordAvailable, bool $sendPassword, bool $sendNewPassword, bool $revoke): void
    {
        $rowCrawler = $crawler->filter("td:contains('$memberName')")->siblings();
        $member = $this->getEntityManager()->getRepository(MemberEntry::class)->findOneBy(['firstName' => $memberName]);

        $buttonCount = (int) $sendPassword + (int) $sendNewPassword + (int) $revoke;
        // Check the number of buttons
        $this->assertEquals($buttonCount, $rowCrawler->filter('td span[class*=btn-outline-secondary]')->count());
        // Check if the link points to the correct member id
        $this->assertEquals($buttonCount, $rowCrawler->filter('a[href*='.$member->getId().']')->count());
        // Check the text of the buttons
        $this->assertEquals((int) $sendPassword, $rowCrawler->filter('td span:contains("Send password")')->count());
        $this->assertEquals((int) $sendNewPassword, $rowCrawler->filter('td span:contains("Send new password")')->count());
        $this->assertEquals((int) $revoke, $rowCrawler->filter('td span:contains("Revoke access")')->count());
        // Check if the password available check symbol is shown
        $this->assertEquals((int) $passwordAvailable, $rowCrawler->filter('td span.fa-check')->count());
    }
}
