<?php

/*
 * Copyright (C) 2022 by Luc Hamers <vereniging@hamers.de>
 *
 * This file is part of Vereniging.
 *
 * Vereniging is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Vereniging is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Vereniging.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Tests\Controller\Admin;

use App\Tests\WebTestCase;
use Exception;

/**
 * Test for the list controller.
 *
 */
class ListControllerTest extends WebTestCase
{
    /**
     * @inheritDoc
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->loadAllFixtures();
    }


    /**
     * Test if there is a link on the dashboard page to the new member page.
     *
     * @throws Exception
     */
    public function testLinkOnDashboardPage(): void
    {
        $this->checkLinkOnDashboardPage($this, 'Manage list values', 'Manage list values', '/administration');
    }


    /**
     * Test the index controller.
     *
     * @throws Exception
     */
    public function testIndex(): void
    {
        $client = $this->getMyClient();
        $crawler = $client->request('GET', '/administration/manage_lists');

        // Check if all sections are present
        $this->assertEquals(1, $crawler->filter('h6.card-header:contains("User status types")')->count());
        $this->assertEquals(1, $crawler->filter('h6.card-header:contains("Academic titles")')->count());
        $this->assertEquals(1, $crawler->filter('h6.card-header:contains("Member languages")')->count());
        $this->assertEquals(1, $crawler->filter('h6.card-header:contains("Address types")')->count());
        $this->assertEquals(1, $crawler->filter('h6.card-header:contains("Countries")')->count());
        $this->assertEquals(1, $crawler->filter('h6.card-header:contains("Types of phone numbers")')->count());

        // Check if the status sections contains the translations for the locales
        $statusCrawler = $crawler->filter('div#status_body');
        $this->assertEquals(1, $statusCrawler->filter('th:contains("Status (en)")')->count());
        $this->assertEquals(1, $statusCrawler->filter('th:contains("Status (de)")')->count());
        $this->assertEquals(1, $statusCrawler->filter('th:contains("Status (es)")')->count());
        $this->assertEquals(1, $statusCrawler->filter('th:contains("Status (nl)")')->count());
        $this->assertEquals(1, $statusCrawler->filter('th:contains("Status has membership number")')->count());
        $this->assertEquals(1, $statusCrawler->filter('tr:contains("Member") td:contains("Member")')->count());
        $this->assertEquals(1, $statusCrawler->filter('tr:contains("Member") td:contains("Mitglied")')->count());
        $this->assertEquals(1, $statusCrawler->filter('tr:contains("Member") td:contains("Lid")')->count());

        // Check if the javascript call to change to edit mode is present
        $this->assertEquals(1, $crawler->filter('a#edit_status[href="/administration/manage_lists_edit/status"]')->count());
        $this->assertEquals(1, $crawler->filter('script:contains("$(\'#edit_status\').on(\'click\'")')->count());


        // Check that there is a doAjax method and that it is in in the globar script area
        $this->assertEquals(1, $crawler->filter('script:contains("function doAjax")')->count());
        $this->assertEquals(0, $statusCrawler->filter('script:contains("function doAjax")')->count());

        // Check if the lock symbols are shown in the right lines
        $this->assertEquals(0, $statusCrawler->filter('tr:contains("Former member") span.fas.fa-lock')->count());
        $this->assertEquals(1, $statusCrawler->filter('tr:contains("Future member") span.fas.fa-lock')->count());
        $this->assertEquals(1, $statusCrawler->filter('tr:contains("Member") span.fas.fa-lock')->count());

        // Check that the translation fields are not generated for the titles, which do not have translations
        $titleCrawler = $crawler->filter('div#title_body');
        $this->assertEquals(2, $titleCrawler->filter('th:contains("Title")')->count());
        $this->assertEquals(1, $titleCrawler->filter('th:contains("Title after name")')->count());
        $this->assertEquals(0, $titleCrawler->filter('th:contains("Title (en)")')->count());
        $this->assertEquals(0, $titleCrawler->filter('th:contains("Title (de)")')->count());
        $this->assertEquals(0, $titleCrawler->filter('th:contains("Title (es)")')->count());
        $this->assertEquals(0, $titleCrawler->filter('th:contains("Title (nl)")')->count());
    }


    /**
     * Test of the edit form is shown correctly
     *
     * @throws Exception
     */
    public function testShowEditForm(): void
    {
        $client = $this->getMyClient();
        $crawler = $client->request('GET', '/administration/manage_lists');
        $link = $crawler->filter('a#edit_status')->link();
        $crawler = $client->click($link);

        // Check the table headers
        $this->assertEquals(1, $crawler->filter('h6.card-header:contains("User status types")')->count());
        $this->assertEquals(1, $crawler->filter('th:contains("Status (en)")')->count());
        $this->assertEquals(1, $crawler->filter('th:contains("Status (de)")')->count());
        $this->assertEquals(1, $crawler->filter('th:contains("Status (es)")')->count());
        $this->assertEquals(1, $crawler->filter('th:contains("Status (nl)")')->count());
        $this->assertEquals(1, $crawler->filter('th:contains("Status has membership number")')->count());

        // Check the content of the text fields
        $this->assertEquals(1, $crawler->filter('tr#status_0 input[value="Former member"]')->count());
        $this->assertEquals(1, $crawler->filter('tr#status_0 input[value="Ehemaliges Mitglied"]')->count());
        $this->assertEquals(1, $crawler->filter('tr#status_0 input[value="Oudlid"]')->count());

        // Check if the lock symbols are shown in the right lines
        $this->assertEquals(0, $crawler->filter('tr#status_0 span.fas.fa-lock')->count());
        $this->assertEquals(1, $crawler->filter('tr#status_1 span.fas.fa-lock')->count());
        $this->assertEquals(1, $crawler->filter('tr#status_2 span.fas.fa-lock')->count());

        // Check if the delete symbols are shown in the right lines
        $this->assertEquals(1, $crawler->filter('tr#status_0 a span.far.fa-trash-alt')->count());
        $this->assertEquals(0, $crawler->filter('tr#status_1 a span.far.fa-trash-alt')->count());
        $this->assertEquals(0, $crawler->filter('tr#status_2 a span.far.fa-trash-alt')->count());

        // Test if there is an add and a delete button
        $this->assertEquals(1, $crawler->filter('button#save_status_data:contains("Save")')->count());
        $this->assertEquals(1, $crawler->filter('button#reset_status_data:contains("Cancel")')->count());
    }


    /**
     * Test if an entry can be deleted
     *
     * @throws Exception
     */
    public function testChangeEntries(): void
    {
        $client = $this->getMyClient();
        // This should show input fields for the personal data fields
        $crawler = $client->request('GET', '/administration/manage_lists_edit/status');
        $buttonCrawlerNode = $crawler->selectButton('Save');
        $form = $buttonCrawlerNode->form();

        // The string "Former member" was also used for the Spanish translation. Now overwrite that, so the string only
        // appears once after commit
        $form['form_collection[members][0][status_es]'] = "test";
        $crawler = $client->submit($form);
        $this->assertEquals(1, $crawler->filter('td:contains("Former member")')->count());
        $this->assertEquals(1, $crawler->filter('td:contains("test")')->count());

        // Test the reaction to a main empty value: This should be ignored
        $form['form_collection[members][0][status]'] = "";
        $crawler = $client->submit($form);
        $this->assertEquals(1, $crawler->filter('td:contains("Former member")')->count());
        $this->assertEquals(1, $crawler->filter('td:contains("test")')->count());

        // Delete top and bottom row
        $crawler = $client->request('GET', '/administration/manage_lists_edit/status');
        $buttonCrawlerNode = $crawler->selectButton('Save');
        $form = $buttonCrawlerNode->form();
        unset($form['form_collection[members][0]']);
        $crawler = $client->submit($form);
        $this->assertEquals(0, $crawler->filter('td:contains("Former member")')->count());
        $this->assertEquals(0, $crawler->filter('td:contains("test")')->count());

        // Add row
        $crawler = $client->request('GET', '/administration/manage_lists_edit/status');
        $buttonCrawlerNode = $crawler->selectButton('Save');
        $form = $buttonCrawlerNode->form();

        $data = $form->getPhpValues();
        $data["form_collection"]["members"][] = [
            "status" => "New status",
            "hasMemberShipNumber" => "1",
        ];

        $crawler = $client->request('POST', '/administration/manage_lists_edit/status', $data);

        // Since we are missing the translations, all four values have the same English value.
        $this->assertEquals(1, $crawler->filter('td:contains("New status")')->count());
    }


    /**
     * Test case for the cancel action
     *
     * @throws Exception
     */
    public function testCancel(): void
    {
        $client = $this->getMyClient();
        $crawler = $client->request('GET', '/administration/manage_lists_reset/status');

        $this->assertEquals(1, $crawler->filter('h6.card-header:contains("User status types")')->count());

        // Check some of the field values
        $this->assertEquals(1, $crawler->filter('th:contains("Status (en)")')->count());
        $this->assertEquals(1, $crawler->filter('th:contains("Status (de)")')->count());
        $this->assertEquals(1, $crawler->filter('tr:contains("Member") td:contains("Member")')->count());
        $this->assertEquals(1, $crawler->filter('tr:contains("Member") td:contains("Mitglied")')->count());
        $this->assertEquals(1, $crawler->filter('tr:contains("Member") td:contains("Lid")')->count());

        // Make sure we only get the status table back
        $this->assertEquals(0, $crawler->filter('h6.card-header:contains("Academic titles")')->count());
        $this->assertEquals(0, $crawler->filter('h6.card-header:contains("Member languages")')->count());
        $this->assertEquals(0, $crawler->filter('h6.card-header:contains("Address types")')->count());
        $this->assertEquals(0, $crawler->filter('h6.card-header:contains("Countries")')->count());
        $this->assertEquals(0, $crawler->filter('h6.card-header:contains("Types of phone numbers")')->count());
    }
}
