<?php

/*
 * Copyright (C) 2022 by Luc Hamers <vereniging@hamers.de>
 *
 * This file is part of Vereniging.
 *
 * Vereniging is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Vereniging is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Vereniging.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Tests\Controller\Admin;

use App\Entity\EmailTemplate;
use App\Tests\WebTestCase;
use Exception;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Component\DomCrawler\Crawler;

/**
 * Test class for the email templates controller.
 *
 */
class EmailTemplatesControllerTest extends WebTestCase
{
    /**
     * @inheritDoc
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->loadAllFixtures();
    }


    /**
     * Test if there is a link on the dashboard page to the new member page.
     *
     * @throws Exception
     */
    public function testLinkOnDashboardPage(): void
    {
        $this->checkLinkOnDashboardPage($this, 'Manage email templates', 'Manage email templates', '/administration');
    }


    /**
     * Test the index controller as system administrator with full access.
     *
     * @throws Exception
     */
    public function testIndexAsSystemAdministrator(): void
    {
        $client = $this->getMyClient();

        $crawler = $this->checkDefaultPageItems($client);

        $expectedSymbols = [
            [],
            ['fa-edit'],
            ['fa-edit'],
            ['fa-trash-alt', 'fa-edit'],
            ['fa-trash-alt', 'fa-edit'],
        ];
        $this->checkTableForTexts($expectedSymbols, $crawler, "#template_table");
    }


    /**
     * Test the index controller as member administrator with limited access.
     *
     * @throws Exception
     */
    public function testIndexAsMemberAdministrator(): void
    {
        $client = $this->getMyClient('firstname3.lastname3');

        $crawler = $this->checkDefaultPageItems($client);
        $expectedSymbols = [
            [],
            ['fa-lock'],
            ['fa-lock'],
            ['fa-trash-alt', 'fa-edit'],
            ['fa-trash-alt', 'fa-edit'],
        ];
        $this->checkTableForTexts($expectedSymbols, $crawler, "#template_table");
    }


    /**
     * Test the index controller as normal member with no access to the administration section
     *
     * @throws Exception
     */
    public function testIndexAsMember(): void
    {
        $client = $this->getMyClient('firstname2.lastname2');

        $crawler = $client->request('GET', '/administration');

        $this->assertEquals(1, $crawler->filter('title:contains("Access Denied")')->count());
        $this->assertEquals(1, $crawler->filter('h1:contains("Access Denied")')->count());
    }


    /**
     * Test deleting a template and if after that no delete is possible anymore, because only one email template is left
     *
     * @throws Exception
     */
    public function testDelete(): void
    {
        $client = $this->getMyClient();
        $crawler = $client->request('GET', '/administration/manage_email_templates');
        $link = $crawler->filter('tr.js-template-item:contains("New email") a.js-remove-template')->attr('href');
        // Click the delete link of the "Invitation" template
        $crawler = $client->request('GET', $link);
        $this->assertCount(1, $crawler->filter('tr.js-template-item:contains("Change message")'));
        $this->assertCount(1, $crawler->filter('tr.js-template-item:contains("Login data")'));
        $this->assertCount(0, $crawler->filter('tr.js-template-item:contains("New email")'));
        $this->assertCount(1, $crawler->filter('tr.js-template-item:contains("Serial email")'));
    }


    /**
     * Test if the edit form for the templates works correctly.
     *
     * @throws Exception
     */
    public function testEditShow(): void
    {
        $client = $this->getMyClient();

        $crawler = $client->request('GET', '/administration/manage_email_templates');
        $link = $crawler->filter('tr.js-template-item:contains("New email") a.js-edit-template')->attr('href');
        $crawler = $client->request('GET', $link);
        // Check header
        $this->assertStringContainsString("Template (\"New email\")", $crawler->filter('h6')->text());
        // Check form fields and their content
        $this->assertCount(1, $crawler->filter('div.row:contains("Template name (en)") input[value="New email"]'));
        $this->assertCount(1, $crawler->filter('div.row:contains("Template name (de)") input[value="Neue E-Mail"]'));
        $this->assertCount(0, $crawler->filter('div.row:contains("Template name (es)") input[value]'));
        $this->assertCount(1, $crawler->filter('div.row:contains("Template name (nl)") input[value="Nieuwe email"]'));
        $this->assertCount(1, $crawler->filter('div.row:contains("Content") textarea:contains("email content")'));
        // Check if the buttons are available
        $this->assertEquals('Save', $crawler->filter('button[type="submit"]')->text());
        $this->assertEquals('Cancel', $crawler->filter('button[type="reset"]')->text());
    }


    /**
     * Test if the edit form for the templates works correctly.
     *
     * @throws Exception
     */
    public function testEditSubmit(): void
    {
        $client = $this->getMyClient();

        $crawler = $client->request('GET', '/administration/manage_email_templates');
        $link = $crawler->filter('tr.js-template-item:contains("New email") a.js-edit-template')->attr('href');

        // Submit correct values
        $crawler = $client->request('GET', $link);
        $form = $crawler->selectButton('Save')->form();
        $form['email_template[templateName_en]'] = "new name";
        $form['email_template[templateName_de]'] = "neuer Name";
        $form['email_template[templateContent]'] = "<b>test</b>>";
        $crawler = $client->submit($form);
        $this->assertEquals('OK', $crawler->text());
        $crawler = $client->request('GET', '/administration/manage_email_templates');
        $expectedLines = [
            ['', 'Change message', 'Change message', 'Änderungsnachricht', '', 'Veranderingsbericht', 'sender-change-en@email.com', 'sender-change-de@email.com', '', 'sender-change-nl@email.com'],
            ['', 'Login data', 'Login data', 'Anmeldedaten', '', 'Aanmeldgegevens', 'sender-login-en@email.com', 'sender-login-de@email.com', '', 'sender-login-nl@email.com'],
            ['', 'Serial email', 'new name', 'neuer Name', '', 'Nieuwe email', 'sender-serial2-en@email.com', 'sender-serial2-de@email.com', '', 'sender-serial2-nl@email.com'],
            ['', 'Serial email', 'Serial email', 'Serien-E-Mail', '', 'Serie-email', 'sender-serial1-en@email.com', 'sender-serial1-de@email.com', '', 'sender-serial1-nl@email.com'],
        ];
        $lineCounter = 1;
        foreach ($expectedLines as $expectedLine) {
            $this->checkContentAsArray($expectedLine, $crawler, "#template_table table tr:nth-child($lineCounter) td", " in line $lineCounter");
            $lineCounter++;
        }
    }


    /**
     * Test the validation of the email address
     *
     * @return void
     *
     * @throws Exception
     */
    public function testEditSubWithError(): void
    {
        // Missing email address
        $this->checkEmailValidation("new name", true);
        // Just brackets, without sender name
        $this->checkEmailValidation("<test@email.com>", true);
        // As above, but with spaces
        $this->checkEmailValidation("     <test@email.com>", true);
        // Correct address without space
        $this->checkEmailValidation("bla<test@email.com>", false);
        // Correct address with space
        $this->checkEmailValidation("bla <test@email.com>", false);
        // Address without sender name
        $this->checkEmailValidation("test@email.com", false);
    }


    /**
     * Test if new templates can be added
     *
     * @throws Exception
     */
    public function testNew(): void
    {
        $client = $this->getMyClient();

        $crawler = $client->request('GET', '/administration/manage_email_templates');
        $link = $crawler->filter('a.js-template-add')->attr('href');

        // Submit correct values
        $crawler = $client->request('GET', $link);

        $this->assertStringContainsString("Template (\"\")", $crawler->filter('h6')->text());
        // Check form fields
        $this->assertCount(2, $crawler->filter('div.row:contains("Template name (en)") input'));
        $this->assertCount(2, $crawler->filter('div.row:contains("Template name (de)") input'));
        $this->assertCount(2, $crawler->filter('div.row:contains("Template name (es)") input'));
        $this->assertCount(2, $crawler->filter('div.row:contains("Template name (nl)") input'));
        $this->assertCount(1, $crawler->filter('div.row:contains("Content") textarea'));

        // Form fields should not contain content
        $this->assertStringNotContainsString('value', $crawler->filter('div.row:contains("Template name (en)") input')->ancestors()->html());
        $this->assertStringNotContainsString('value', $crawler->filter('div.row:contains("Template name (de)") input')->ancestors()->html());
        $this->assertStringNotContainsString('value', $crawler->filter('div.row:contains("Template name (es)") input')->ancestors()->html());
        $this->assertStringNotContainsString('value', $crawler->filter('div.row:contains("Template name (nl)") input')->ancestors()->html());
        $this->assertEquals('&lt;span class="mceNonEditable" style="border: thin dotted grey;" data-variable="email content"&gt;email content&lt;/span&gt;', $crawler->filter('div.row:contains("Content") textarea')->html());

        $form = $crawler->selectButton('Save')->form();
        $form['email_template[templateName_en]'] = "new name";
        $form['email_template[templateName_de]'] = "neuer Name";
        $form['email_template[templateContent]'] = "<b>test</b>>";
        $crawler = $client->submit($form);
        $this->assertEquals('OK', $crawler->text());
        $crawler = $client->request('GET', '/administration/manage_email_templates');
        $expectedLines = [
            ['', 'Change message', 'Change message', 'Änderungsnachricht', '', 'Veranderingsbericht', 'sender-change-en@email.com', 'sender-change-de@email.com', '', 'sender-change-nl@email.com'],
            ['', 'Login data', 'Login data', 'Anmeldedaten', '', 'Aanmeldgegevens', 'sender-login-en@email.com', 'sender-login-de@email.com', '', 'sender-login-nl@email.com'],
            ['', 'Serial email', 'New email', 'Neue E-Mail', '', 'Nieuwe email', 'sender-serial2-en@email.com', 'sender-serial2-de@email.com', '', 'sender-serial2-nl@email.com'],
            ['', 'Serial email', 'new name', 'neuer Name', '', '', '', '', '', ''],
        ];
        $lineCounter = 1;
        foreach ($expectedLines as $expectedLine) {
            $this->checkContentAsArray($expectedLine, $crawler, "#template_table table tr:nth-child($lineCounter) td", " in line $lineCounter");
            $lineCounter++;
        }
    }


    /**
     * Check if the content variable is translated correctly on rendering and when it is stored back in the database.
     *
     * @throws Exception
     */
    public function testTranslation(): void
    {
        $client = $this->getMyClient();
        $crawler = $client->request('GET', '/administration/manage_email_templates/edit/1', ['_locale' => 'de']);

        // Check that the content variable is shown in German. The shown variable end with </span>, i.e. &lt;/span&gt;
        $this->assertStringContainsString('E-Mail-Inhalt&lt;/span&gt;', $crawler->filter('textarea')->html());

        // Now store this content, the variable should be translated before storing it
        $form = $crawler->selectButton('Speichern')->form();
        $form['email_template[templateContent]'] = "<b>test</b>".$form['email_template[templateContent]']->getValue();

        $client->submit($form);

        /** @var EmailTemplate $template */
        $template = $this->getEntityManager()->getRepository(EmailTemplate::class)->find(1);
        // Check that the content variable is shown in English.
        $expectedContent = '<b>test</b>change content: <span class="mceNonEditable" style="border: thin dotted grey;" data-variable="email content">email content</span>';
        $this->assertEquals($expectedContent, $template->getContent());
    }


    /**
     * Check if the content variable is added automatically if it is not present in the form submitted by the user
     *
     * @throws Exception
     */
    public function testContentVariableAdd(): void
    {
        $client = $this->getMyClient();
        $crawler = $client->request('GET', '/administration/manage_email_templates/edit/1');

        // Now store this content, the variable should be added before storing
        $form = $crawler->selectButton('Save')->form();
        $form['email_template[templateContent]'] = "<b>test</b>";

        $client->submit($form);

        /** @var EmailTemplate $template */
        $template = $this->getEntityManager()->getRepository(EmailTemplate::class)->find(1);
        // Check that the content variable is shown in English.
        $expectedContent = '<b>test</b><span class="mceNonEditable" style="border: thin dotted grey;" data-variable="email content">email content</span>';
        $this->assertEquals($expectedContent, $template->getContent());
    }


    /**
     * @param KernelBrowser $client
     *
     * @return Crawler
     */
    private function checkDefaultPageItems(KernelBrowser $client): Crawler
    {
        $crawler = $client->request('GET', '/administration/manage_email_templates');

        // Check the page title
        $this->assertEquals(1, $crawler->filter('title:contains("Administration")')->count());
        $this->assertEquals(1, $crawler->filter('title:contains("Manage email templates")')->count());

        // Check the navigation links
        $this->assertEquals(1, $crawler->filter('a.navbar-brand:contains("the Vereniging member system dashboard")')->count());
        $this->assertEquals(2, $crawler->filter('a.nav-link:contains("Administration")')->count());
        $this->assertEquals(1, $crawler->filter('a.nav-link:contains("Manage email templates")')->count());

        // Check the table headings
        $headings = ["", "Type", "Template name (en)", "Template name (de)", "Template name (es)", "Template name (nl)", "Sender address (en)", "Sender address (de)", "Sender address (es)", "Sender address (nl)"];
        $this->checkTableHeaders($headings, $crawler, '#template_table');

        $expectedLines = [
            ['', 'Change message', 'Change message', 'Änderungsnachricht', '', 'Veranderingsbericht', 'sender-change-en@email.com', 'sender-change-de@email.com', '', 'sender-change-nl@email.com'],
            ['', 'Login data', 'Login data', 'Anmeldedaten', '', 'Aanmeldgegevens', 'sender-login-en@email.com', 'sender-login-de@email.com', '', 'sender-login-nl@email.com'],
            ['', 'Serial email', 'New email', 'Neue E-Mail', '', 'Nieuwe email', 'sender-serial2-en@email.com', 'sender-serial2-de@email.com', '', 'sender-serial2-nl@email.com'],
            ['', 'Serial email', 'Serial email', 'Serien-E-Mail', '', 'Serie-email', 'sender-serial1-en@email.com', 'sender-serial1-de@email.com', '', 'sender-serial1-nl@email.com'],
        ];
        $lineCounter = 1;
        foreach ($expectedLines as $expectedLine) {
            $this->checkContentAsArray($expectedLine, $crawler, "#template_table table tr:nth-child($lineCounter) td", " in line $lineCounter");
            $lineCounter++;
        }

        return $crawler;
    }


    /**
     * @param string $emailAddress Email address to check
     * @param bool   $expectError  True if an error is expected

     * @throws Exception
     */
    private function checkEmailValidation(string $emailAddress, bool $expectError): void
    {
        $client = $this->getMyClient();
        $crawler = $client->request('GET', '/administration/manage_email_templates/edit');
        $form = $crawler->selectButton('Save')->form();
        $form['email_template[templateName_en]'] = "new name";
        $form['email_template[senderAddress_en]'] = $emailAddress;
        $crawler = $client->submit($form);
        if ($expectError) {
            $errorMessage = 'The entered email address is invalid. The address must look like ';
            $this->assertStringContainsString($errorMessage, $crawler->filter('span.form-error-message')->html());
        } else {
            $this->assertEquals('OK', $crawler->text());
        }
    }
}
