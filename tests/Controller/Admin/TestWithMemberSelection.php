<?php

/*
 * Copyright (C) 2022 by Luc Hamers <vereniging@hamers.de>
 *
 * This file is part of Vereniging.
 *
 * Vereniging is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Vereniging is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Vereniging.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Tests\Controller\Admin;

use App\Entity\Committee;
use App\Entity\MembershipType;
use App\Entity\Status;
use App\Entity\UserRole;
use App\Tests\WebTestCase;
use Exception;
use Symfony\Component\DomCrawler\Crawler;

/**
 * Class TestWithMemberSelection
 */
class TestWithMemberSelection extends WebTestCase
{
    private array $statusLookup = [];
    private array $membershipTypeLookup = [];
    private array $committeeLookup = [];
    private array $userRoleLookup = [];

    /**
     * @inheritDoc
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->loadAllFixtures();
    }


    /**
     * Check if the member selection form works correctly
     *
     * @param string $url URL for which the form is checked.
     *
     * @throws Exception
     */
    public function checkMemberSelectionForm(string $url): void
    {
        $this->checkIndexMemberTable($url);

        $url = "$url/show";
        $this->initArray(Status::class, $this->statusLookup);
        $this->initArray(MembershipType::class, $this->membershipTypeLookup);
        $this->initArray(Committee::class, $this->committeeLookup);
        $this->initArray(UserRole::class, $this->userRoleLookup);

        $this->checkStatusSelection($url);
        $this->checkMembershipTypeSelection($url);
        $this->checkCommitteeSelection($url);
        $this->checkUserRoleSelection($url);
        $this->checkEmailSelection($url);
        $this->checkMembershipEndRequested($url);
    }


    /**
     * Check the member table on the index page.
     *
     * @param string $url URL of the page in which the member selection is embedded.
     *
     * @throws Exception
     */
    private function checkIndexMemberTable(string $url): void
    {
        $crawler = $this->getMyClient()->request('GET', $url);

        // Check if the selection form is correct
        $this->assertEquals(1, $crawler->filter('h6.card-header:contains("Filters")')->count());
        $this->assertEquals(1, $crawler->filter('div form#selection_form:contains("User status")')->count());
        $this->assertEquals(1, $crawler->filter('div#member_lists_selection_status:contains("Member")')->count());
        $this->assertEquals(1, $crawler->filter('div#member_lists_selection_status:contains("Future member")')->count());
        $this->assertEquals(1, $crawler->filter('div#member_lists_selection_status:contains("Former member")')->count());

        $this->assertEquals(1, $crawler->filter('div form#selection_form:contains("Membership type")')->count());
        $this->assertEquals(1, $crawler->filter('div form#selection_form:contains("Regular member")')->count());
        $this->assertEquals(1, $crawler->filter('div form#selection_form:contains("Student member")')->count());
        $this->assertEquals(1, $crawler->filter('div form#selection_form:contains("Family member")')->count());
        $this->assertEquals(1, $crawler->filter('div form#selection_form:contains("Group member")')->count());

        $this->assertEquals(1, $crawler->filter('div form#selection_form:contains("Committee")')->count());
        $this->assertEquals(1, $crawler->filter('div form#selection_form:contains("Board")')->count());
        $this->assertEquals(1, $crawler->filter('div form#selection_form:contains("Activities")')->count());
        $this->assertEquals(1, $crawler->filter('div form#selection_form:contains("Public relations")')->count());

        $this->assertEquals(1, $crawler->filter('div form#selection_form:contains("User role")')->count());
        $this->assertEquals(1, $crawler->filter('div form#selection_form:contains("User")')->count());
        $this->assertEquals(1, $crawler->filter('div form#selection_form:contains("Serial letter signer")')->count());
        $this->assertEquals(1, $crawler->filter('div form#selection_form:contains("Serial letter author")')->count());
        $this->assertEquals(1, $crawler->filter('div form#selection_form:contains("Member administrator")')->count());
        $this->assertEquals(1, $crawler->filter('div form#selection_form:contains("System administrator")')->count());

        $this->assertEquals(1, $crawler->filter('div form#selection_form:contains("Email address")')->count());
        $this->assertEquals(1, $crawler->filter('div form#selection_form:contains("All")')->count());
        $this->assertEquals(1, $crawler->filter('div form#selection_form:contains("Only with email")')->count());
        $this->assertEquals(1, $crawler->filter('div form#selection_form:contains("Only without email")')->count());
        $this->assertEquals(1, $crawler->filter('div form#selection_form:contains("Only without email in group")')->count());

        $this->assertEquals(1, $crawler->filter('div form#selection_form:contains("Others")')->count());
        $this->assertEquals(1, $crawler->filter('div form#selection_form:contains("Membership end requested")')->count());

        // Check if the member selection worked correctly, i.e. if members 1 to 10 are present, member 4 excluded.
        $this->CheckMembersPresence($crawler, [1, 2, 3, 5, 6, 7, 8, 9, 10, 11, 12], "testIndex member selection");

        //Check if the first member also has a last name and email addres
        $this->assertEquals(4, $crawler->filter('td:contains("Lastname1")')->count());
        $this->assertEquals(1, $crawler->filter('td:contains("firstname1@email.com")')->count());

        // Check how many filter columns there are
        $this->assertEquals(6, $crawler->filter('#selection_form div.col-md-2')->count());
    }


    /**
     * @param Crawler $crawler
     * @param int[]   $memberNumbersExpected
     * @param string  $testCase
     */
    private function checkMembersPresence(Crawler $crawler, array $memberNumbersExpected, string $testCase): void
    {
        if (0 === count($memberNumbersExpected)) {
            $this->assertEquals(1, $crawler->filter('div.card-body:contains("No member entries found")')->count(), "$testCase - no member selected");

            return;
        }

        $this->assertEquals(count($memberNumbersExpected), $crawler->filter('tr')->count() - 1, "$testCase - number of members doesn't match");

        // When member10 has to be searched, then a special handling for member1 has to be applied, since member1 is
        // also contained in the member10 string.
        $noMembersEqualGreaterThan10 = 0;
        if (in_array(10, $memberNumbersExpected)) {
            ++$noMembersEqualGreaterThan10;
        }
        if (in_array(11, $memberNumbersExpected)) {
            ++$noMembersEqualGreaterThan10;
        }
        if (in_array(12, $memberNumbersExpected)) {
            ++$noMembersEqualGreaterThan10;
        }

        foreach ($memberNumbersExpected as $memberNumber) {
            $message = "$testCase - $memberNumber";
            if (1 === $memberNumber && $noMembersEqualGreaterThan10) {
                $this->assertEquals($noMembersEqualGreaterThan10 + 1, $crawler->filter('td:contains("Firstname1")')->count(), $message);
            } else {
                $this->assertEquals(1, $crawler->filter('td:contains("Firstname'.$memberNumber.'")')->count(), $message);
            }
        }
    }


    /**
     * Check if the member selection by status works correctly
     *
     * @param string $url URL of the post route for the controller to be tested.
     *
     * @throws Exception
     */
    private function checkStatusSelection(string $url): void
    {
        $client = $this->getMyClient();

        // Don't select anything
        $crawler = $client->request('POST', $url, [
            'member_lists_selection' => [
                'email' => 'all',
                'others' => ['directDebit' => 'all'],
            ],
        ]);
        $this->CheckMembersPresence($crawler, [], "No member selected");

        // Select a status with members
        $crawler = $client->request('POST', $url, [
            'member_lists_selection' => [
                'status' => [$this->statusLookup['Former member']],
                'email' => 'all',
                'others' => ['membershipEndRequested' => false, 'directDebit' => 'all'],
            ],
        ]);
        $this->CheckMembersPresence($crawler, [], "Former member selected");

        // Select future members
        $crawler = $client->request('POST', $url, [
            'member_lists_selection' => [
                'status' => [$this->statusLookup['Future member']],
                'email' => 'all',
                'others' => ['directDebit' => 'all'],
            ],
        ]);
        $this->CheckMembersPresence($crawler, [4], "Future member selected");

        // Select member status
        $crawler = $client->request('POST', $url, [
            'member_lists_selection' => [
                'status' => [$this->statusLookup['Member']],
                'email' => 'all',
                'others' => ['directDebit' => 'all'],
            ],
        ]);
        $this->CheckMembersPresence($crawler, [1, 2, 3, 5, 6, 7, 8, 9, 10, 11, 12], "Member selected");

        // Select all
        $crawler = $client->request('POST', $url, [
            'member_lists_selection' => [
                'status' => [$this->statusLookup['Member'], $this->statusLookup['Future member']],
                'email' => 'all',
                'others' => ['directDebit' => 'all'],
            ],
        ]);
        $this->CheckMembersPresence($crawler, [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12], "Member and future member selected");
    }


    /**
     * Check if the member selection by membership type works correctly
     *
     * @param string $url URL of the post route for the controller to be tested.
     *
     * @throws Exception
     */
    private function checkMembershipTypeSelection(string $url): void
    {
        $client = $this->getMyClient();

        // Select members which have the regular member membership type.
        $crawler = $client->request('POST', $url, [
            'member_lists_selection' => [
                'membershipType' => [$this->membershipTypeLookup['Regular member']],
                'email' => 'all',
                'others' => ['directDebit' => 'all'],
            ],
        ]);
        $this->CheckMembersPresence($crawler, [2, 3, 4, 5, 6, 7, 8, 9], "Regular member selected");

        // Select members which have the student member membership type.
        $crawler = $client->request('POST', $url, [
            'member_lists_selection' => [
                'membershipType' => [$this->membershipTypeLookup['Student member']],
                'email' => 'all',
                'others' => ['directDebit' => 'all'],
            ],
        ]);
        $this->CheckMembersPresence($crawler, [], "Student member selected");

        // Select members which have the family member membership type.
        $crawler = $client->request('POST', $url, [
            'member_lists_selection' => [
                'membershipType' => [$this->membershipTypeLookup['Family member']],
                'email' => 'all',
                'others' => ['directDebit' => 'all'],
            ],
        ]);
        $this->CheckMembersPresence($crawler, [1, 10, 11, 12], "Family member selected");

        // Select members which have the regular or family member membership type.
        $crawler = $client->request('POST', $url, [
            'member_lists_selection' => [
                'membershipType' => [$this->membershipTypeLookup['Regular member'], $this->membershipTypeLookup['Family member']],
                'email' => 'all',
                'others' => ['directDebit' => 'all'],
            ],
        ]);
        $this->CheckMembersPresence($crawler, [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12], "Regular and family member selected");
    }


    /**
     * Check if the member selection by committee type works correctly
     *
     * @param string $url URL of the post route for the controller to be tested.
     *
     * @throws Exception
     */
    private function checkCommitteeSelection(string $url): void
    {
        $client = $this->getMyClient();

        // Select members which are member of the board.
        $crawler = $client->request('POST', $url, [
            'member_lists_selection' => [
                'committee' => [$this->committeeLookup['Board']],
                'email' => 'all',
                'others' => ['directDebit' => 'all'],
            ],
        ]);
        $this->CheckMembersPresence($crawler, [1], "Board selected");

        // Select members which are member of the activities committee.
        $crawler = $client->request('POST', $url, [
            'member_lists_selection' => [
                'committee' => [$this->committeeLookup['Activities']],
                'email' => 'all',
                'others' => ['directDebit' => 'all'],
            ],
        ]);
        $this->CheckMembersPresence($crawler, [1, 2], "Activities selected");

        // Select members which are member of the board and the activities committee.
        $crawler = $client->request('POST', $url, [
            'member_lists_selection' => [
                'committee' => [$this->committeeLookup['Board'], $this->committeeLookup['Activities']],
                'email' => 'all',
                'others' => ['directDebit' => 'all'],
            ],
        ]);
        $this->CheckMembersPresence($crawler, [1, 2], "Board and activities selected");
    }


    /**
     * Check if the member selection by user role works correctly
     *
     * @param string $url URL of the post route for the controller to be tested.
     *
     * @throws Exception
     */
    private function checkUserRoleSelection(string $url): void
    {
        $client = $this->getMyClient();

        // Select members which have the user role. This should return 7 members
        $crawler = $client->request('POST', $url, [
            'member_lists_selection' => [
                'userRole' => [$this->userRoleLookup['User']],
                'email' => 'all',
                'others' => ['directDebit' => 'all'],
            ],
        ]);
        $this->CheckMembersPresence($crawler, [2, 4, 5, 6, 7, 8, 9, 10, 11, 12], "User selected");

        // Select members which have the member administrator role. This should return 1 member
        $crawler = $client->request('POST', $url, [
            'member_lists_selection' => [
                'userRole' => [$this->userRoleLookup['Member administrator']],
                'email' => 'all',
                'others' => ['directDebit' => 'all'],
            ],
        ]);
        $this->CheckMembersPresence($crawler, [3], "Member administrator selected");

        // Select members which have the system administrator role. This should return 1 member
        $crawler = $client->request('POST', $url, [
            'member_lists_selection' => [
                'userRole' => [$this->userRoleLookup['System administrator']],
                'email' => 'all',
                'others' => ['directDebit' => 'all'],
            ],
        ]);
        $this->CheckMembersPresence($crawler, [1], "System administrator selected");

        // Select member and system administrator
        $crawler = $client->request('POST', $url, [
            'member_lists_selection' => [
                'userRole' => [$this->userRoleLookup['Member administrator'], $this->userRoleLookup['System administrator']],
                'email' => 'all',
                'others' => ['directDebit' => 'all'],
            ],
        ]);
        $this->CheckMembersPresence($crawler, [1, 3], "System administrator selected");
    }


    /**
     * Check if the member selection by email works correctly
     *
     * @param string $url URL of the post route for the controller to be tested.
     *
     * @throws Exception
     */
    private function checkEmailSelection(string $url): void
    {
        $client = $this->getMyClient();

        // Select members independent of their email address
        $crawler = $client->request('POST', $url, [
            'member_lists_selection' => [
                'status' => [$this->statusLookup['Member']],
                'email' => 'all',
                'others' => ['directDebit' => 'all'],
            ],
        ]);
        $this->CheckMembersPresence($crawler, [1, 2, 3, 5, 6, 7, 8, 9, 10, 11, 12], "All email selected");

        // Select members with an email address
        $crawler = $client->request('POST', $url, [
            'member_lists_selection' => [
                'status' => [$this->statusLookup['Member']],
                'email' => 'with',
                'others' => ['directDebit' => 'all'],
            ],
        ]);
        $this->CheckMembersPresence($crawler, [1, 3, 5, 6, 7, 8, 9, 10, 11, 12], "With email selected");

        // Select members without an email address
        $crawler = $client->request('POST', $url, [
            'member_lists_selection' => [
                'status' => [$this->statusLookup['Member']],
                'email' => 'without',
                'others' => ['directDebit' => 'all'],
            ],
        ]);
        $this->CheckMembersPresence($crawler, [2], "Without email selected");
    }


    /**
     * Check if the member selection by membership end requested works correctly
     *
     * @param string $url URL of the post route for the controller to be tested.
     *
     * @throws Exception
     */
    private function checkMembershipEndRequested(string $url): void
    {
        $client = $this->getMyClient();

        // Select members independent of their email address
        $crawler = $client->request('POST', $url, [
            'member_lists_selection' => [
                'email' => 'all',
                'others' => ['membershipEndRequested' => true, 'directDebit' => 'all'],
            ],
        ]);
        $this->CheckMembersPresence($crawler, [3], "Membership end request selected");
    }


    /**
     * Initialise an associative array where the status name is the key and the id is the content.
     *
     * @param string $class Class name, e.g. Status::class
     * @param array  $array Array which is to be initialised, e.g. $this->statusArray
     */
    private function initArray(string $class, array &$array): void
    {
        $entities = $this->getEntityManager()->getRepository($class)->findAll();
        foreach ($entities as $entity) {
            $array[(string) $entity] = $entity->getId();
        }
    }
}
