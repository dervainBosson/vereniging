<?php

/*
 * Copyright (C) 2022 by Luc Hamers <vereniging@hamers.de>
 *
 * This file is part of Vereniging.
 *
 * Vereniging is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Vereniging is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Vereniging.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Tests\Controller\Admin;

use App\Entity\UserRole;
use App\Tests\WebTestCase;
use Exception;

/**
 * Test class for the user rights controller.
 *
 */
class UserRoleControllerTest extends WebTestCase
{
    /**
     * @inheritDoc
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->loadAllFixtures();
    }


    /**
     * Test if there is a link on the dashboard page to the new member page.
     *
     * @throws Exception
     */
    public function testLinkOnDashboardPage(): void
    {
        $this->checkLinkOnDashboardPage($this, 'Manage user roles', 'Manage user roles', '/administration');
    }


    /**
     * Test the index controller.
     *
     * @throws Exception
     */
    public function testIndex(): void
    {
        $client = $this->getMyClient();
        $crawler = $client->request('GET', '/administration/manage_user_roles');

        // Check the page title
        $this->assertEquals(1, $crawler->filter('title:contains("Administration")')->count());
        $this->assertEquals(1, $crawler->filter('title:contains("Manage user roles")')->count());

        // Check the navigation links
        $this->assertEquals(1, $crawler->filter('a.navbar-brand:contains("the Vereniging member system dashboard")')->count());
        $this->assertEquals(2, $crawler->filter('a.nav-link:contains("Administration")')->count());
        $this->assertEquals(1, $crawler->filter('a.nav-link:contains("Manage user roles")')->count());

        // Check the table headings
        $headings = ["Role", "User", "Serial letter signer", "Serial letter author", "Member administrator", "Treasurer", "System administrator"];
        $this->checkTableHeaders($headings, $crawler, '#user-role-table');

        // Check the number of check symbols for certain roles
        $this->assertEquals(6, $crawler->filter('td:contains("View dashboard")')->siblings()->filter('td span')->count());
        $this->assertEquals(3, $crawler->filter('td:contains("Edit other users")')->siblings()->filter('td span')->count());
        $this->assertEquals(3, $crawler->filter('td:contains("View administration dashboard")')->siblings()->filter('td span')->count());
        $this->assertEquals(3, $crawler->filter('td:contains("Manage list values")')->siblings()->filter('td span')->count());
        $this->assertEquals(2, $crawler->filter('td:contains("Manage finances")')->siblings()->filter('td span')->count());
        $this->assertEquals(1, $crawler->filter('td:contains("Manage user roles")')->siblings()->filter('td span')->count());
        $this->assertEquals(1, $crawler->filter('td:contains("Manage user rights")')->siblings()->filter('td span')->count());
    }


    /**
     * The user roles are ordered by the number of system roles. This class tests if this also works when two roles have
     * the same number of system roles.
     *
     * @throws Exception
     */
    public function testIndexWithAdditionalRoles(): void
    {
        // First add a new role which has the same system roles as the serial letter signer role
        $newRole = new UserRole();
        $newRole->setUserRole('Serial letter signer');
        $newRole->setDefaultRole(false);
        /** @var UserRole $letterSignerRole */
        $letterSignerRole = $this->getFixtureReference('user_role_role_serial_letter_signer');
        $newRole->setSystemRoles($letterSignerRole->getSystemRoles());
        $newRole->translate('en')->setUserRoleTranslated('New role');
        $this->getEntityManager()->persist($newRole);
        $this->getEntityManager()->flush();

        $client = $this->getMyClient();
        $crawler = $client->request('GET', '/administration/manage_user_roles');

        // Check the table headings
        $headings = ["Role", "User", "New role", "Serial letter signer", "Serial letter author", "Member administrator", "Treasurer", "System administrator"];
        $this->checkTableHeaders($headings, $crawler, '#user-role-table');
    }
}
