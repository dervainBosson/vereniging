<?php

/*
 * Copyright (C) 2022 by Luc Hamers <vereniging@hamers.de>
 *
 * This file is part of Vereniging.
 *
 * Vereniging is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Vereniging is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Vereniging.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Tests\Controller\Admin;

use App\Entity\Committee;
use App\Entity\CommitteeFunction;
use App\Entity\LogfileEntry;
use App\Tests\WebTestCase;
use Exception;

/**
 * Test class for the committee controller.
 *
 */
class CommitteeControllerTest extends WebTestCase
{
    /**
     * @inheritDoc
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->loadAllFixtures();
    }


    /**
     * Test if there is a link on the dashboard page to the new member page.
     */
    public function testLinkOnDashboardPage(): void
    {
        $this->checkLinkOnDashboardPage($this, 'Manage committees', 'Manage committees', '/administration');
    }


    /**
     * Test the index controller.
     *
     * @throws Exception
     */
    public function testIndex(): void
    {
        $client = $this->getMyClient();

        $crawler = $client->request('GET', '/administration/manage_committees');
        // Check the page title
        $this->assertEquals(1, $crawler->filter('title:contains("Administration")')->count());
        $this->assertEquals(1, $crawler->filter('title:contains("Manage committees")')->count());

        // Check the navigation links
        $this->assertEquals(1, $crawler->filter('a.navbar-brand:contains("the Vereniging member system dashboard")')->count());
        $this->assertEquals(2, $crawler->filter('a.nav-link:contains("Administration")')->count());
        $this->assertEquals(1, $crawler->filter('a.nav-link:contains("Manage committees")')->count());

        // Check content panels
        $this->assertEquals(1, $crawler->filter('h6.card-header:contains("Committees")')->count());
        $this->assertEquals(1, $crawler->filter('h6.card-header:contains("Committee functions")')->count());
        $this->assertEquals(1, $crawler->filter('h6.card-header:contains("Link between committees and functions")')->count());

        // Check the value table for committees and functions
        $this->assertEquals(1, $crawler->filter('td:contains("Board")')->count());
        $this->assertEquals(1, $crawler->filter('td:contains("Activities")')->count());
        $this->assertEquals(3, $crawler->filter('td:contains("Public relations")')->count());
        $this->assertEquals(1, $crawler->filter('td:contains("Chairman")')->count());
        $this->assertEquals(1, $crawler->filter('td:contains("Vice chairman")')->count());
        $this->assertEquals(1, $crawler->filter('td:contains("Member")')->count());

        // Check translations
        $this->assertEquals(1, $crawler->filter('td:contains("Board")')->siblings()->filter('td:contains("Vorstand")')->count());
        $this->assertEquals(1, $crawler->filter('td:contains("Board")')->siblings()->filter('td:contains("Bestuur")')->count());
        $this->assertEquals(1, $crawler->filter('td:contains("Activities")')->siblings()->filter('td:contains("Veranstaltungen")')->count());
        $this->assertEquals(1, $crawler->filter('td:contains("Activities")')->siblings()->filter('td:contains("Activiteiten")')->count());
        $this->assertEquals(1, $crawler->filter('td:contains("Chairman")')->siblings()->filter('td:contains("Vorsitzender")')->count());
        $this->assertEquals(1, $crawler->filter('td:contains("Chairman")')->siblings()->filter('td:contains("Voorzitter")')->count());
        $this->assertEquals(1, $crawler->filter('td:contains("Vice chairman")')->siblings()->filter('td:contains("Stellvertretender Vorsitzender")')->count());
        $this->assertEquals(1, $crawler->filter('td:contains("Vice chairman")')->siblings()->filter('td:contains("Vicevoorzitter")')->count());
        $this->assertEquals(1, $crawler->filter('td:contains("Member")')->siblings()->filter('td:contains("Mitglied")')->count());
        $this->assertEquals(1, $crawler->filter('td:contains("Member")')->siblings()->filter('td:contains("Lid")')->count());


        // Check the number of check symbols per row in the links table
        $this->assertEquals(3, $crawler->filter('th:contains("Board")')->siblings()->filter('td span.fa-check')->count());
        $this->assertEquals(2, $crawler->filter('th:contains("Activities")')->siblings()->filter('td span.fa-check')->count());
        $this->assertEquals(2, $crawler->filter('th:contains("Public relations")')->siblings()->filter('td span.fa-check')->count());

        // Check some javascripts
        $this->assertEquals(1, $crawler->filter('script:contains("function doAjax")')->count());
        $this->assertEquals(1, $crawler->filter('script:contains("#edit_committee\').on(\'click")')->count());
        $this->assertEquals(1, $crawler->filter('script:contains("#reset_committee_data\').on(\'click")')->count());
        $this->assertEquals(1, $crawler->filter('script:contains("#edit_committeefunction\').on(\'click")')->count());
        $this->assertEquals(1, $crawler->filter('script:contains("#reset_committeefunction_data\').on(\'click")')->count());
        $this->assertEquals(1, $crawler->filter('script:contains("#edit_committee_links\').on(\'click")')->count());
        $this->assertEquals(1, $crawler->filter('script:contains("#reset_committee_link_data\').on(\'click")')->count());

        // Check ajax ankers
        $this->assertEquals(1, $crawler->filter('div#committee_content')->count());
        $this->assertEquals(1, $crawler->filter('div#committeefunction_content')->count());
        $this->assertEquals(1, $crawler->filter('div#committee_function_link_content')->count());
    }


    /**
     * Test the index controller when the showOnlyContent flag is set.
     *
     * @throws Exception
     */
    public function testIndexWithShowOnlyContentTable(): void
    {
        $client = $this->getMyClient();

        $crawler = $client->request('GET', '/administration/manage_committees/1');

        // Check the page title
        $this->assertEquals(0, $crawler->filter('title:contains("Administration")')->count());
        $this->assertEquals(0, $crawler->filter('title:contains("Manage committees")')->count());

        // Check the navigation links
        $this->assertEquals(0, $crawler->filter('a.navbar-brand:contains("the Vereniging member system dashboard")')->count());
        $this->assertEquals(0, $crawler->filter('a.nav-link:contains("Administration")')->count());
        $this->assertEquals(0, $crawler->filter('a.nav-link:contains("Manage committees")')->count());

        // Check content panel
        $this->assertEquals(1, $crawler->filter('h6.card-header:contains("Link between committees and functions")')->count());

        // Check the number of check symbols per row
        $this->assertEquals(3, $crawler->filter('th:contains("Board")')->siblings()->filter('td span.fa-check')->count());
        $this->assertEquals(2, $crawler->filter('th:contains("Activities")')->siblings()->filter('td span.fa-check')->count());
        $this->assertEquals(2, $crawler->filter('th:contains("Public relations")')->siblings()->filter('td span.fa-check')->count());
    }


    /**
     * Test editing the committee values.
     *
     * @throws Exception
     */
    public function testEditCommitteeValues(): void
    {
        $client = $this->getMyClient();

        $crawler = $client->request('GET', '/administration/manage_committees_edit_values/committee');
        $this->assertEquals(1, $crawler->filter('h6.card-header:contains("Committees")')->count());

        // Check if both buttons are available
        $this->assertEquals(1, $crawler->filter('button#save_committee_data[type="submit"]:contains("Save")')->count());
        $this->assertEquals(1, $crawler->filter('button#reset_committee_data[type="reset"]:contains("Cancel")')->count());

        // Check the value table for committees and functions. These should all be text inputs
        $this->assertEquals(1, $crawler->filter('input[type="text"][value*="Board"]')->count());
        $this->assertEquals(1, $crawler->filter('input[type="text"][value*="Activities"]')->count());
        $this->assertEquals(3, $crawler->filter('input[type="text"][value*="Public relations"]')->count());

        // Check some translations
        $this->assertEquals(1, $crawler->filter('input[type="text"][value*="Vorstand"]')->count());
        $this->assertEquals(1, $crawler->filter('input[type="text"][value*="Activiteiten"]')->count());

        // Check ajax ankers
        $this->assertEquals(3, $crawler->filter('a[class~="js-remove-committee"]')->count());
    }


    /**
     * Test editing the committee function values.
     *
     * @throws Exception
     */
    public function testEditCommitteeFunctionValues(): void
    {
        $client = $this->getMyClient();

        $crawler = $client->request('GET', '/administration/manage_committees_edit_values/committeefunction');
        $this->assertEquals(1, $crawler->filter('h6.card-header:contains("Committee functions")')->count());

        // Check if both buttons are available
        $this->assertEquals(1, $crawler->filter('button#save_committeefunction_data[type="submit"]:contains("Save")')->count());
        $this->assertEquals(1, $crawler->filter('button#reset_committeefunction_data[type="reset"]:contains("Cancel")')->count());

        // Check the value table for committees and functions. These should all be text inputs
        $this->assertEquals(1, $crawler->filter('input[type="text"][value*="Chairman"]')->count());
        $this->assertEquals(1, $crawler->filter('input[type="text"][value*="Vice chairman"]')->count());
        $this->assertEquals(1, $crawler->filter('input[type="text"][value*="Member"]')->count());

        // Check some translations
        $this->assertEquals(1, $crawler->filter('input[type="text"][value*="Voorzitter"]')->count());
        $this->assertEquals(1, $crawler->filter('input[type="text"][value*="Mitglied"]')->count());

        // Check ajax ankers
        $this->assertEquals(3, $crawler->filter('a[class~="js-remove-committeefunction"]')->count());
    }


    /**
     * Test editing the committee values.
     *
     * @throws Exception
     */
    public function testCommitCommitteeValues(): void
    {
        $client = $this->getMyClient();

        $crawler = $client->request('POST', '/administration/manage_committees_edit_values/committee');
        $buttonCrawlerNode = $crawler->selectButton('Save');
        $form = $buttonCrawlerNode->form();

        $form['form_collection[members][0][committeeName]'] = "Activities1";
        $form['form_collection[members][0][committeeName_de]'] = "Veranstaltungen1";
        $form['form_collection[members][0][committeeName_nl]'] = "Activiteiten1";
        $crawler = $client->submit($form);

        $this->assertEquals(1, $crawler->filter('td:contains("Activities1")')->count());
        $this->assertEquals(1, $crawler->filter('td:contains("Activities1")')->siblings()->filter('td:contains("Veranstaltungen1")')->count());
        $this->assertEquals(1, $crawler->filter('td:contains("Activities1")')->siblings()->filter('td:contains("Activiteiten1")')->count());

        // Test the reaction to setting a main empty value: This should be ignored
        $form['form_collection[members][0][committeeName]'] = "";
        $crawler = $client->submit($form);
        $this->assertEquals(1, $crawler->filter('td:contains("Activities1")')->count());

        // Remove activities
        unset($form['form_collection[members][0]']);
        $crawler = $client->submit($form);
        $this->assertEquals(0, $crawler->filter('td:contains("Activities1")')->count());

        unset($form['form_collection[members][1]']);
        unset($form['form_collection[members][2]']);
        $client->submit($form);
        $crawler = $client->request('GET', '/administration/manage_committees');
        $this->assertEquals(0, $crawler->filter('td:contains("Board")')->count());
        $this->assertEquals(0, $crawler->filter('td:contains("Activities1")')->count());
        $this->assertEquals(0, $crawler->filter('td:contains("Public relations")')->count());
        $this->assertEquals(1, $crawler->filter('td:contains("Chairman")')->count());
        $this->assertEquals(1, $crawler->filter('td:contains("Vice chairman")')->count());
        $this->assertEquals(1, $crawler->filter('td:contains("Member")')->count());

        // Check content panels
        $this->assertEquals(1, $crawler->filter('h6.card-header:contains("Committees")')->count());
        $this->assertEquals(1, $crawler->filter('h6.card-header:contains("Committee functions")')->count());
        $this->assertEquals(0, $crawler->filter('h6.card-header:contains("Link between committees and functions")')->count());

        // Test adding values
        $crawler = $client->request('POST', '/administration/manage_committees_edit_values/committee');
        $buttonCrawlerNode = $crawler->selectButton('Save');
        $form = $buttonCrawlerNode->form();
        $data = $form->getPhpValues();
        $data["form_collection"]["members"][] = [
            "committeeName" => "New committee",
        ];
        $client->request('POST', '/administration/manage_committees_edit_values/committee', $data);
        $crawler = $client->request('GET', '/administration/manage_committees');
        $this->assertEquals(0, $crawler->filter('td:contains("Board")')->count());
        $this->assertEquals(0, $crawler->filter('td:contains("Activities1")')->count());
        $this->assertEquals(0, $crawler->filter('td:contains("Public relations")')->count());
        $this->assertEquals(1, $crawler->filter('td:contains("New committee")')->count());
        $this->assertEquals(1, $crawler->filter('td:contains("Chairman")')->count());
        $this->assertEquals(1, $crawler->filter('td:contains("Vice chairman")')->count());
        $this->assertEquals(1, $crawler->filter('td:contains("Member")')->count());
        // Check content panels
        $this->assertEquals(1, $crawler->filter('h6.card-header:contains("Committees")')->count());
        $this->assertEquals(1, $crawler->filter('h6.card-header:contains("Committee functions")')->count());
        $this->assertEquals(1, $crawler->filter('h6.card-header:contains("Link between committees and functions")')->count());

        // Test adding an empty value
        $crawler = $client->request('POST', '/administration/manage_committees_edit_values/committee');
        $buttonCrawlerNode = $crawler->selectButton('Save');
        $form = $buttonCrawlerNode->form();
        $data = $form->getPhpValues();
        $data["form_collection"]["members"][] = [
            "committeeName" => "",
        ];
        $crawler = $client->request('POST', '/administration/manage_committees_edit_values/committee', $data);
        $this->assertCount(1, $crawler->filter('tr.js-committee-item'));
    }


    /**
     * Test editing the committee function values.
     *
     * @throws Exception
     */
    public function testCommitCommitteeFunctionValues(): void
    {
        $client = $this->getMyClient();

        $crawler = $client->request('POST', '/administration/manage_committees_edit_values/committeefunction');
        $buttonCrawlerNode = $crawler->selectButton('Save');
        $form = $buttonCrawlerNode->form();

        // The string "Former member" was also used for the Spanish translation. Now overwrite that, so the string only
        // appears once after commit
        $form['form_collection[members][0][committeeFunctionName]'] = "Chairman1";
        $form['form_collection[members][0][committeeFunctionName_de]'] = "Vorsitzender1";
        $form['form_collection[members][0][committeeFunctionName_nl]'] = "Voorzitter1";
        $crawler = $client->submit($form);

        $this->assertEquals(1, $crawler->filter('td:contains("Chairman1")')->count());
        $this->assertEquals(1, $crawler->filter('td:contains("Chairman1")')->siblings()->filter('td:contains("Vorsitzender1")')->count());
        $this->assertEquals(1, $crawler->filter('td:contains("Chairman1")')->siblings()->filter('td:contains("Voorzitter1")')->count());

        // Test the reaction to a main empty value: This should be ignored
        $form['form_collection[members][0][committeeFunctionName]'] = "";
        $crawler = $client->submit($form);
        $this->assertEquals(1, $crawler->filter('td:contains("Chairman1")')->count());

        // Remove chairman
        unset($form['form_collection[members][0]']);
        $crawler = $client->submit($form);
        $this->assertEquals(0, $crawler->filter('td:contains("Chairman1")')->count());

        unset($form['form_collection[members][1]']);
        unset($form['form_collection[members][2]']);
        $client->submit($form);
        $crawler = $client->request('GET', '/administration/manage_committees');
        $this->assertEquals(1, $crawler->filter('td:contains("Board")')->count());
        $this->assertEquals(1, $crawler->filter('td:contains("Activities")')->count());
        $this->assertEquals(3, $crawler->filter('td:contains("Public relations")')->count());
        $this->assertEquals(0, $crawler->filter('td:contains("Chairman1")')->count());
        $this->assertEquals(0, $crawler->filter('td:contains("Vice chairman")')->count());
        $this->assertEquals(0, $crawler->filter('td:contains("Member")')->count());

        // Check content panels
        $this->assertEquals(1, $crawler->filter('h6.card-header:contains("Committees")')->count());
        $this->assertEquals(1, $crawler->filter('h6.card-header:contains("Committee functions")')->count());
        $this->assertEquals(0, $crawler->filter('h6.card-header:contains("Link between committees and functions")')->count());

        // Test adding values
        $crawler = $client->request('POST', '/administration/manage_committees_edit_values/committeefunction');
        $buttonCrawlerNode = $crawler->selectButton('Save');
        $form = $buttonCrawlerNode->form();
        $data = $form->getPhpValues();
        $data["form_collection"]["members"][] = [
            "committeeFunctionName" => "New function",
        ];
        $client->request('POST', '/administration/manage_committees_edit_values/committeefunction', $data);
        $crawler = $client->request('GET', '/administration/manage_committees');
        $this->assertEquals(1, $crawler->filter('td:contains("Board")')->count());
        $this->assertEquals(1, $crawler->filter('td:contains("Activities")')->count());
        $this->assertEquals(3, $crawler->filter('td:contains("Public relations")')->count());
        $this->assertEquals(1, $crawler->filter('td:contains("New function")')->count());
        $this->assertEquals(0, $crawler->filter('td:contains("Chairman1")')->count());
        $this->assertEquals(0, $crawler->filter('td:contains("Vice chairman")')->count());
        $this->assertEquals(0, $crawler->filter('td:contains("Member")')->count());
        // Check content panels
        $this->assertEquals(1, $crawler->filter('h6.card-header:contains("Committees")')->count());
        $this->assertEquals(1, $crawler->filter('h6.card-header:contains("Committee functions")')->count());
        $this->assertEquals(1, $crawler->filter('h6.card-header:contains("Link between committees and functions")')->count());
    }


    /**
     * Test calling the cancel action
     *
     * @throws Exception
     */
    public function testCancelValues(): void
    {
        $client = $this->getMyClient();
        $crawler = $client->request('POST', '/administration/manage_committees_cancel_values/committee');
        // Check the value table for committees and functions
        $this->assertEquals(1, $crawler->filter('td:contains("Board")')->count());
        $this->assertEquals(0, $crawler->filter('input[type="text"][value*="Board"]')->count());
        $this->assertEquals(1, $crawler->filter('td:contains("Activities")')->count());
        $this->assertEquals(0, $crawler->filter('input[type="text"][value*="Activities"]')->count());
        $this->assertEquals(3, $crawler->filter('td:contains("Public relations")')->count());
        $this->assertEquals(0, $crawler->filter('input[type="text"][value*="Public relations"]')->count());

        $crawler = $client->request('POST', '/administration/manage_committees_cancel_values/committeefunction');
        $this->assertEquals(1, $crawler->filter('td:contains("Chairman")')->count());
        $this->assertEquals(0, $crawler->filter('input[type="text"][value*="Chairman"]')->count());
        $this->assertEquals(1, $crawler->filter('td:contains("Vice chairman")')->count());
        $this->assertEquals(0, $crawler->filter('input[type="text"][value*="Vice chairman"]')->count());
        $this->assertEquals(1, $crawler->filter('td:contains("Member")')->count());
        $this->assertEquals(0, $crawler->filter('input[type="text"][value*="Member"]')->count());
    }


    /**
     * Test the edit links action without submitting data.
     *
     * @throws Exception
     */
    public function testEditLinksShow(): void
    {
        $client = $this->getMyClient();

        $crawler = $client->request('GET', '/administration/manage_committees_edit_links');
        $this->assertEquals(1, $crawler->filter('h6.card-header:contains("Link between committees and functions")')->count());

        // Check if the checkboxes are there and the corrent number of flags is set.
        $this->assertEquals(3, $crawler->filter('th:contains("Board")')->siblings()->filter('td input[type=checkbox][checked=checked]')->count());
        $this->assertEquals(2, $crawler->filter('th:contains("Activities")')->siblings()->filter('td input[type=checkbox][checked=checked]')->count());
        $this->assertEquals(2, $crawler->filter('th:contains("Public relations")')->siblings()->filter('td input[type=checkbox][checked=checked]')->count());

        // Check if both buttons are available
        $this->assertEquals(1, $crawler->filter('button#save_committee_link_data[type="submit"]:contains("Save")')->count());
        $this->assertEquals(1, $crawler->filter('button#reset_committee_link_data[type="reset"]:contains("Cancel")')->count());
    }


    /**
     * Test the edit action.
     *
     * @throws Exception
     */
    public function testEditCommitLinks(): void
    {
        $client = $this->getMyClient();

        $committees = $this->getEntityManager()->getRepository(Committee::class)->findAll();
        $functions = $this->getEntityManager()->getRepository(CommitteeFunction::class)->findAll();

        // Set all checkboxes for the first committee
        foreach ($functions as $function) {
            $data['committee_function_form']['committee_'.$committees[0]->getId()][] = $function->getId();
        }
        $crawler = $client->request('POST', '/administration/manage_committees_edit_links', $data);
        $this->assertEquals(3, $crawler->filter('th:contains("Activities")')->siblings()->filter('td span.fa-check')->count());
        $this->assertEquals(0, $crawler->filter('th:contains("Board")')->siblings()->filter('td span.fa-check')->count());
        $this->assertEquals(0, $crawler->filter('th:contains("Public relations")')->siblings()->filter('td span.fa-check')->count());

        $logMessages = $this->getEntityManager()->getRepository(LogfileEntry::class)->findBy([], ['id' => 'DESC'], 6);
        foreach ($logMessages as $logMessage) {
            $this->assertEquals('Firstname1', $logMessage->getChangedByMember()->getFirstName());
            $this->assertEquals('System settings change', $logMessage->getChangeType()->getChangeType());
        }
        $this->assertEquals('Added function Vice chairman to committee Activities', $logMessages[5]->getLogentry());
        $this->assertEquals('Removed function Chairman from committee Board', $logMessages[4]->getLogentry());
        $this->assertEquals('Removed function Member from committee Board', $logMessages[3]->getLogentry());
        $this->assertEquals('Removed function Vice chairman from committee Board', $logMessages[2]->getLogentry());
        $this->assertEquals('Removed function Chairman from committee Public relations', $logMessages[1]->getLogentry());
        $this->assertEquals('Removed function Member from committee Public relations', $logMessages[0]->getLogentry());

        // Test special handler for when no check boxes are set.
        $crawler = $client->request('POST', '/administration/manage_committees_edit_links', []);
        $this->assertEquals(0, $crawler->filter('td span.fa-check')->count());
    }
}
