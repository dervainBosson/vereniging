<?php

/*
 * Copyright (C) 2022 by Luc Hamers <vereniging@hamers.de>
 *
 * This file is part of Vereniging.
 *
 * Vereniging is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Vereniging is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Vereniging.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Tests\Controller\Admin;

use App\Tests\WebTestCase;
use Exception;

/**
 * Class SerialLetterTemplateControllerTest
 */
class SerialLetterTemplateControllerTest extends WebTestCase
{
    /**
     * @inheritDoc
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->loadAllFixtures();
    }


    /**
     * Test if there is a link on the dashboard page to the new member page.
     *
     * @throws Exception
     */
    public function testLinkOnDashboardPage(): void
    {
        $this->checkLinkOnDashboardPage($this, 'Manage serial letter templates', 'Manage serial letter templates', '/administration');
    }


    /**
     * Test the index controller.
     *
     * @throws Exception
     */
    public function testIndex(): void
    {
        $client = $this->getMyClient();

        $crawler = $client->request('GET', '/administration/manage_serial_letter_templates');

        // Check the page title
        $this->assertStringContainsString('Administration', $crawler->filter('title')->text());
        $this->assertStringContainsString('Manage serial letter templates', $crawler->filter('title')->text());

        // Check the navigation links
        $this->assertStringContainsString('the Vereniging member system dashboard', $crawler->filter('a.navbar-brand')->text());
        $this->assertStringContainsString('Administration', $crawler->filter('a.nav-link')->text());
        $this->assertCount(1, $crawler->filter('a.nav-link:contains("Manage serial letter templates")'));

        // Check the list with the current templates
        $this->assertStringContainsString('Serial letter templates', $crawler->filter('h6.card-header')->text());
        $this->assertCount(1, $crawler->filter('h6:contains("Dutch")'));
        $this->assertCount(1, $crawler->filter('h6:contains("English")'));
        $this->assertCount(1, $crawler->filter('h6:contains("German")'));

        // Check the content items
        $this->assertCount(18, $crawler->filter('div.js-template-item>a'));
        $this->assertCount(1, $crawler->filter('div.js-template-item:contains("Uitnodiging")'));
        $this->assertCount(1, $crawler->filter('div.js-template-item:contains("Invitation")'));
        $this->assertCount(1, $crawler->filter('div.js-template-item:contains("Einladung")'));
        $this->assertCount(9, $crawler->filter('div.js-template-item>a.js-remove-template'));
        $this->assertCount(9, $crawler->filter('div.js-template-item>a.js-edit-template'));
    }


    /**
     * Test if the deletion of the templates works correctly.
     *
     * @throws Exception
     */
    public function testDelete(): void
    {
        $client = $this->getMyClient();

        $crawler = $client->request('GET', '/administration/manage_serial_letter_templates');
        $link = $crawler->filter('div.js-template-item:contains("Invitation")>a.js-remove-template')->attr('href');
        // Click the delete link of the "Invitation" template
        $client->request('GET', $link);
        $crawler = $client->request('GET', '/administration/manage_serial_letter_templates');
        $this->assertCount(1, $crawler->filter('div.js-template-item:contains("Uitnodiging")'));
        $this->assertCount(0, $crawler->filter('div.js-template-item:contains("Invitation")'));
        $this->assertCount(1, $crawler->filter('div.js-template-item:contains("Einladung")'));
    }


    /**
     * Test if the edit form for the templates works correctly.
     *
     * @throws Exception
     */
    public function testEditShow(): void
    {
        $client = $this->getMyClient();

        $crawler = $client->request('GET', '/administration/manage_serial_letter_templates');
        $link = $crawler->filter('div.js-template-item:contains("Activity list")>a.js-edit-template')->attr('href');
        $crawler = $client->request('GET', $link);
        // Check header
        $this->assertStringContainsString("Content (\"Activity list\")", $crawler->filter('h6')->text());
        // Check form fields and their content
        $this->assertCount(1, $crawler->filter('div.row:contains("Template name") input[value="Activity list"]'));
        $this->assertCount(1, $crawler->filter('div.row:contains("Language") option[selected="selected"]:contains("English")'));
        $this->assertCount(1, $crawler->filter('div.row:contains("Content") textarea:contains("26/7/2020")'));
        // Check if the buttons are available
        $this->assertEquals('Save', $crawler->filter('button[type="submit"]')->text());
        $this->assertEquals('Cancel', $crawler->filter('button[type="reset"]')->text());
    }


    /**
     * Test if the edit form for the templates works correctly.
     *
     * @throws Exception
     */
    public function testEditSubmit(): void
    {
        $client = $this->getMyClient();

        $crawler = $client->request('GET', '/administration/manage_serial_letter_templates');
        $link = $crawler->filter('div.js-template-item:contains("Activity list")>a.js-edit-template')->attr('href');

        // Submit correct values
        $crawler = $client->request('GET', $link);
        $form = $crawler->selectButton('Save')->form();
        $form['serial_letter_template[templateName]'] = "new name";
        $form['serial_letter_template[templateContent]'] = "new content";
        $form['serial_letter_template[language]']->select($this->getFixtureReference('language_dutch')->getId());
        $crawler = $client->submit($form);
        $this->assertEquals('OK', $crawler->text());
        $crawler = $client->request('GET', '/administration/manage_serial_letter_templates');
        $this->assertCount(0, $crawler->filter('div.js-template-item:contains("Activity list")'));
        $this->assertCount(1, $crawler->filter('div.js-template-item:contains("new name")'));

        // Submit empty values and check validation errors
        $crawler = $client->request('GET', $link);
        $form = $crawler->selectButton('Save')->form();
        $form['serial_letter_template[templateName]'] = "";
        $form['serial_letter_template[templateContent]'] = "";
        $crawler = $client->submit($form);
        $this->assertStringContainsString('This value should not be blank.', $crawler->filter('label:contains("Template name")')->html());
        $this->assertStringContainsString('This value should not be blank.', $crawler->filter('label:contains("Content")')->html());
    }


    /**
     * Test if new templates can be added
     *
     * @throws Exception
     */
    public function testNew(): void
    {
        $client = $this->getMyClient();

        $crawler = $client->request('GET', '/administration/manage_serial_letter_templates');
        $link = $crawler->filter('a.js-template-add')->attr('href');

        // Submit correct values
        $crawler = $client->request('GET', $link);

        $this->assertStringContainsString("Content (\"\")", $crawler->filter('h6')->text());
        // Check form fields
        $this->assertCount(1, $crawler->filter('div.row:contains("Template name") input'));
        $this->assertCount(1, $crawler->filter('div.row:contains("Language") select'));
        $this->assertCount(1, $crawler->filter('div.row:contains("Content") textarea'));
        // Form fields should not contain content
        $this->assertStringNotContainsString('value', $crawler->filter('div.row:contains("Template name") input')->ancestors()->html());
        $this->assertStringNotContainsString('selected', $crawler->filter('div.row:contains("Language") select')->ancestors()->html());
        $this->assertEquals('', $crawler->filter('div.row:contains("Content") textarea')->html());

        $form = $crawler->selectButton('Save')->form();
        $form['serial_letter_template[templateName]'] = "new name";
        $form['serial_letter_template[templateContent]'] = "new content";
        $form['serial_letter_template[language]']->select($this->getFixtureReference('language_dutch')->getId());
        $crawler = $client->submit($form);
        $this->assertEquals('OK', $crawler->text());
        $crawler = $client->request('GET', '/administration/manage_serial_letter_templates');
        $this->assertCount(1, $crawler->filter('div.js-template-item:contains("new name")'));
    }
}
