<?php

/*
 * Copyright (C) 2022 by Luc Hamers <vereniging@hamers.de>
 *
 * This file is part of Vereniging.
 *
 * Vereniging is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Vereniging is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Vereniging.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Tests\Controller\Admin;

use App\Tests\WebTestCase;
use Exception;

/**
 * Test calls to the import from excel controller
 *
 * Class ImportFromExcelControllerTest
 */
class ImportFromExcelControllerTest extends WebTestCase
{
    /**
     * @inheritDoc
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->loadAllFixtures();
    }


    /**
     * Test if there is a link on the dashboard page to the new member page.
     *
     * @throws Exception
     */
    public function testLinkOnDashboardPage(): void
    {
        $this->checkLinkOnDashboardPage($this, 'Import from Excel', 'Import from Excel', '/administration');
    }


    /**
     * Test the index controller.
     *
     * @throws Exception
     */
    public function testIndex(): void
    {
        $client = $this->getMyClient();

        $crawler = $client->request('GET', '/administration/import_excel');

        // Check the page title
        $this->assertEquals(1, $crawler->filter('title:contains("Administration")')->count());
        $this->assertEquals(1, $crawler->filter('title:contains("Import from Excel")')->count());

        // Check the navigation links
        $this->assertEquals(1, $crawler->filter('a.navbar-brand:contains("the Vereniging member system dashboard")')->count());
        $this->assertEquals(2, $crawler->filter('a.nav-link:contains("Administration")')->count());
        $this->assertEquals(1, $crawler->filter('a.nav-link:contains("Import from Excel")')->count());

        // Check if the file selection form is present
        $this->assertEquals(1, $crawler->filter('h6.card-header:contains("Select import file")')->count());
        $this->assertEquals(1, $crawler->filter('form[action="/administration/import_excel_check"]')->count());
        $this->assertEquals(1, $crawler->filter('input[type="file"]')->count());
        $this->assertEquals(1, $crawler->filter('button[type="submit"]')->count());
    }


    /**
     * Test submitting a pdf file, which should generate an error (at step 1).
     *
     * @throws Exception
     */
    public function testFileUploadWithErrorStep1(): void
    {
        $client = $this->getMyClient();

        $crawler = $client->request('GET', '/administration/import_excel');
        $form = $crawler->filter("#upload_form")->form();
        $form['file_upload[file]']->upload(dirname(__FILE__).'/ImportExcelFiles/empty.pdf');
        $crawler = $client->submit($form);
        $this->assertEquals(1, $crawler->filter('p:contains("Error while importing file: File empty.pdf is no excel file.")')->count());
    }


    /**
     * Now create an error in step 2: first transmit a correct file, check it, replace it by a broken file in the temp
     * directory and then submit step 2.
     *
     * @throws Exception
     */
    public function testFileUploadWithErrorStep2(): void
    {
        $client = $this->getMyClient();

        $crawler = $client->request('GET', '/administration/import_excel');
        $form = $crawler->filter("#upload_form")->form();
        $form['file_upload[file]']->upload(dirname(__FILE__).'/ImportExcelFiles/demo_import_file.xlsx');
        $crawler = $client->submit($form);

        $tmpDir = $_ENV['VERENIGING_TEMP_DIRECTORY'];
        $fileName = glob("$tmpDir/vereniging_import_file.*.xlsx");
        $this->assertCount(1, $fileName);
        exec('cp '.dirname(__FILE__).'/ImportExcelFiles/dynamic_data_required_address_type_missing_error.xlsx '.$fileName[0]);
        $form = $crawler->filter("#import-form")->form();
        $crawler = $client->submit($form);
        $this->assertEquals(1, $crawler->filter('p:contains("Error while importing file: Field Address type cannot be empty in cell AI3.")')->count());
    }


    /**
     * Test submitting a pdf file, which should generate an error.
     *
     * @throws Exception
     */
    public function testFileUploadWithValidFile(): void
    {
        $client = $this->getMyClient();

        $crawler = $client->request('GET', '/administration/import_excel');
        $form = $crawler->filter("#upload_form")->form();
        $form['file_upload[file]']->upload(dirname(__FILE__).'/ImportExcelFiles/demo_import_file.xlsx');
        $crawler = $client->submit($form);
        $this->assertEquals(1, $crawler->filter('p:contains("Import file is OK")')->count());
        $this->assertEquals(1, $crawler->filter('p:contains("It contains 12 member entries")')->count());
        $this->assertEquals(1, $crawler->filter('li:contains("1 company information field")')->count());
        $this->assertEquals(1, $crawler->filter('li:contains("4 phone number fields")')->count());
        $this->assertEquals(1, $crawler->filter('li:contains("2 address fields")')->count());
        $this->assertEquals(1, $crawler->filter('li:contains("5 committee function fields")')->count());
        $this->assertEquals(1, $crawler->filter('li:contains("3 group members")')->count());

        $this->assertEquals(1, $crawler->filter('li:contains("4 title values")')->count());
        $this->assertEquals(1, $crawler->filter('li:contains("Dr.-Ing.")')->count());
        $this->assertEquals(1, $crawler->filter('li:contains("Prof.-Dr.")')->count());

        $this->assertEquals(1, $crawler->filter('li:contains("3 function values without new values")')->count());

        $this->assertEquals(1, $crawler->filter('li:contains("9 committee function values")')->count());
        $this->assertEquals(1, $crawler->filter('li:contains("sports - chairman")')->count());
        $this->assertEquals(1, $crawler->filter('li:contains("sports - member")')->count());

        $this->assertEquals(1, $crawler->filter('form:contains("Import data")')->count());

        $tmpDir = $_ENV['VERENIGING_TEMP_DIRECTORY'];
        $numberOfImportFiles = count(glob("$tmpDir/vereniging_import_file.*.xlsx"));
        $this->assertEquals(1, $numberOfImportFiles);

        // Upload a file for the second time to test if the old files are deleted correctly
        $crawler = $client->request('GET', '/administration/import_excel');
        $form = $crawler->filter("#upload_form")->form();
        $form['file_upload[file]']->upload(dirname(__FILE__).'/ImportExcelFiles/demo_import_file.xlsx');
        $crawler = $client->submit($form);

        $numberOfImportFiles = count(glob("$tmpDir/vereniging_import_file.*.xlsx"));
        $this->assertEquals(1, $numberOfImportFiles);

        $form = $crawler->filter("#import-form")->form();
        $crawler = $client->submit($form);
        $this->assertEquals(1, $crawler->filter('p:contains("Imported 12 member entries")')->count());
    }
}
