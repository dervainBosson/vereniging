<?php

/*
 * Copyright (C) 2022 by Luc Hamers <vereniging@hamers.de>
 *
 * This file is part of Vereniging.
 *
 * Vereniging is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Vereniging is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Vereniging.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Tests\Controller\Admin;

use App\Tests\WebTestCase;
use Exception;

/**
 * Test class for the default administration controller.
 *
 */
class DefaultControllerTest extends WebTestCase
{
    /**
     * @inheritDoc
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->loadAllFixtures();
    }


    /**
     * Test if there is a link on the dashboard page to the new member page.
     *
     * @throws Exception
     */
    public function testLinkOnDashboardPage(): void
    {
        $this->checkLinkOnDashboardPage($this, 'Administration', 'Administration', '/dashboard');
    }


    /**
     * Test the index controller as system administrator with full access.
     *
     * @throws Exception
     */
    public function testIndexAsSystemAdministrator(): void
    {
        $client = $this->getMyClient();

        $crawler = $client->request('GET', '/administration');

        $this->assertEquals(1, $crawler->filter('title:contains("- Administration")')->count());
        $this->assertEquals(1, $crawler->filter('h6.card-header:contains("Member and club settings")')->count());
        $this->assertEquals(1, $crawler->filter('h6.card-header:contains("Email and serial letter settings")')->count());
        $this->assertEquals(1, $crawler->filter('h6.card-header:contains("Data handling")')->count());
        $this->assertEquals(1, $crawler->filter('h6.card-header:contains("System settings")')->count());

        $this->assertEquals(1, $crawler->filter('html:contains("Manage list values")')->count());
        $this->assertEquals(1, $crawler->filter('html:contains("Manage committees")')->count());
        $this->assertEquals(1, $crawler->filter('html:contains("Manage mailing lists")')->count());
        $this->assertEquals(1, $crawler->filter('html:contains("Manage membership types")')->count());

        $this->assertEquals(1, $crawler->filter('html:contains("Manage serial letter templates")')->count());
        $this->assertEquals(1, $crawler->filter('html:contains("Manage email templates")')->count());
        $this->assertEquals(1, $crawler->filter('html:contains("Manage email notifications")')->count());

        $this->assertEquals(1, $crawler->filter('html:contains("Import from Excel")')->count());
        $this->assertEquals(1, $crawler->filter('html:contains("System backup")')->count());
        $this->assertEquals(1, $crawler->filter('html:contains("Snapshot diffs")')->count());
        $this->assertEquals(1, $crawler->filter('html:contains("Export to Excel")')->count());
        $this->assertEquals(1, $crawler->filter('html:contains("Export to PDF")')->count());

        $this->assertEquals(1, $crawler->filter('html:contains("Manage user roles")')->count());
        $this->assertEquals(1, $crawler->filter('html:contains("Manage user rights")')->count());
        $this->assertEquals(1, $crawler->filter('html:contains("Manage passwords")')->count());
    }


    /**
     * Test the index controller as member administrator with limited access.
     *
     * @throws Exception
     */
    public function testIndexAsMemberAdministrator(): void
    {
        $client = $this->getMyClient('firstname3.lastname3');

        $crawler = $client->request('GET', '/administration');

        $this->assertEquals(1, $crawler->filter('title:contains("- Administration")')->count());
        $this->assertEquals(1, $crawler->filter('h6.card-header:contains("Member and club settings")')->count());
        $this->assertEquals(1, $crawler->filter('h6.card-header:contains("Email and serial letter settings")')->count());
        $this->assertEquals(1, $crawler->filter('h6.card-header:contains("Data handling")')->count());
        $this->assertEquals(0, $crawler->filter('h6.card-header:contains("System settings")')->count());

        $this->assertEquals(1, $crawler->filter('html:contains("Manage list values")')->count());
        $this->assertEquals(1, $crawler->filter('html:contains("Manage committees")')->count());
        $this->assertEquals(1, $crawler->filter('html:contains("Manage mailing lists")')->count());
        $this->assertEquals(1, $crawler->filter('html:contains("Manage membership types")')->count());

        $this->assertEquals(1, $crawler->filter('html:contains("Manage serial letter templates")')->count());
        $this->assertEquals(1, $crawler->filter('html:contains("Manage email templates")')->count());
        $this->assertEquals(0, $crawler->filter('html:contains("Manage email notifications")')->count());

        $this->assertEquals(0, $crawler->filter('html:contains("Import from Excel")')->count());
        $this->assertEquals(0, $crawler->filter('html:contains("System backup")')->count());
        $this->assertEquals(1, $crawler->filter('html:contains("Snapshot diffs")')->count());
        $this->assertEquals(1, $crawler->filter('html:contains("Export to Excel")')->count());
        $this->assertEquals(1, $crawler->filter('html:contains("Export to PDF")')->count());

        $this->assertEquals(0, $crawler->filter('html:contains("Manage user roles")')->count());
        $this->assertEquals(0, $crawler->filter('html:contains("Manage user rights")')->count());
        $this->assertEquals(0, $crawler->filter('html:contains("Manage passwords")')->count());
    }


    /**
     * Test the index controller as normal member with no access to the administration section
     *
     * @throws Exception
     */
    public function testIndexAsMember(): void
    {
        $client = $this->getMyClient('firstname2.lastname2');

        $crawler = $client->request('GET', '/administration');

        $this->assertEquals(1, $crawler->filter('title:contains("Access Denied")')->count());
        $this->assertEquals(1, $crawler->filter('h1:contains("Access Denied")')->count());
    }
}
