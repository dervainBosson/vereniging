<?php

/*
 * Copyright (C) 2022 by Luc Hamers <vereniging@hamers.de>
 *
 * This file is part of Vereniging.
 *
 * Vereniging is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Vereniging is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Vereniging.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Tests\Controller\Admin;

use App\Entity\MemberEntry;
use App\Entity\UserRole;
use Doctrine\ORM\ORMInvalidArgumentException;
use Exception;

/**
 * Test class for the user rights controller.
 *
 */
class UserRightsControllerTest extends TestWithMemberSelection
{
    /**
     * @inheritDoc
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->loadAllFixtures();
    }


    /**
     * Test if there is a link on the dashboard page to the new member page.
     *
     * @throws Exception
     */
    public function testLinkOnDashboardPage(): void
    {
        $this->checkLinkOnDashboardPage($this, 'Manage user rights', 'Manage user rights', '/administration');
    }


    /**
     * Test if the selection form on this page works correctly
     *
     * @throws Exception
     */
    public function testSelectionForm(): void
    {
        $this->checkMemberSelectionForm('/administration/manage_user_rights');
    }


    /**
     * Test the index controller.
     *
     * @throws Exception
     */
    public function testIndex(): void
    {
        $client = $this->getMyClient();

        $crawler = $client->request('GET', '/administration/manage_user_rights');

        // Check the page title
        $this->assertEquals(1, $crawler->filter('title:contains("Administration")')->count());
        $this->assertEquals(1, $crawler->filter('title:contains("Manage user rights")')->count());

        // Check the navigation links
        $this->assertEquals(1, $crawler->filter('a.navbar-brand:contains("the Vereniging member system dashboard")')->count());
        $this->assertEquals(2, $crawler->filter('a.nav-link:contains("Administration")')->count());
        $this->assertEquals(1, $crawler->filter('a.nav-link:contains("Manage user rights")')->count());

        // Check if all members have their own form
        $this->assertEquals(11, $crawler->filter('form.js-memberform-item')->count());

        // Check if for member 1 and 2 the correct user role is pre-selected.
        $entityManager = $this->getEntityManager();
        $member1 = $entityManager->getRepository(MemberEntry::class)->findOneBy(['username' => 'firstname1.lastname1']);
        $member2 = $entityManager->getRepository(MemberEntry::class)->findOneBy(['username' => 'firstname2.lastname2']);
        $selectId1 = 'member_form_'.$member1->getId().'_userRole';
        $selectId2 = 'member_form_'.$member2->getId().'_userRole';
        $this->assertEquals('System administrator', $crawler->filter('select#'.$selectId1.' option[selected="selected"]')->text());
        $this->assertEquals('User', $crawler->filter('select#'.$selectId2.' option[selected="selected"]')->text());

        // Check the table headings
        $this->assertEquals(1, $crawler->filter('th:contains("First name")')->count());
        $this->assertEquals(1, $crawler->filter('th:contains("Last name")')->count());
        $this->assertEquals(1, $crawler->filter('th:contains("Email")')->count());
        $this->assertEquals(1, $crawler->filter('th:contains("User role")')->count());
    }


    /**
     * Test the show action.
     *
     * @throws Exception
     */
    public function testShow(): void
    {
        $client = $this->getMyClient();

        $crawler = $client->request('GET', '/administration/manage_user_rights');
        $form = $crawler->filter('#selection_form')->form();
        // Submit with default settings (status = member and user rule is not set)
        $crawler = $client->submit($form);
        $this->assertEquals(11, $crawler->filter('tr:contains("Firstname")')->count());

        // Add future member status. This should increase the number of members from 11 to 12.
        $form['member_lists_selection[status]'][1]->tick();
        $crawler = $client->submit($form);
        $this->assertEquals(12, $crawler->filter('tr:contains("Firstname")')->count());
    }


    /**
     * Test the update action.
     *
     * @throws ORMInvalidArgumentException
     * @throws Exception
     */
    public function testUpdate(): void
    {
        $client = $this->getMyClient();

        $entityManager = $this->getEntityManager();
        $member2 = $entityManager->getRepository(MemberEntry::class)->findOneBy(['username' => 'firstname2.lastname2']);

        // Change role for member2 to user
        $client->request('POST', '/administration/manage_user_rights/update', [
            'member_form_'.$member2->getId() => [
                'userRole' => $this->getRolesArray()['System administrator'],
            ],
        ]);
        $entityManager->refresh($member2);
        $this->assertEquals('System administrator', $member2->getUserRole()->getUserRole());

        // Change role for member2 to system administrator
        $client->request('POST', '/administration/manage_user_rights/update', [
            'member_form_'.$member2->getId() => [
                'userRole' => $this->getRolesArray()['User'],
            ],
        ]);
        $member2 = $entityManager->getRepository(MemberEntry::class)->find($member2->getId());
        $entityManager->refresh($member2);
        $this->assertEquals('User', $member2->getUserRole()->getUserRole());
    }


    /**
     * Get an associative array where the user role name is the key and the id is the content.
     *
     * @return array
     */
    private function getRolesArray(): array
    {
        $entityManager = $this->getEntityManager();

        $roles = $entityManager->getRepository(UserRole::class)->findAll();

        $rolesArray = array();
        foreach ($roles as $role) {
            $rolesArray[$role->getUserRole()] = $role->getId();
        }

        return ($rolesArray);
    }
}
