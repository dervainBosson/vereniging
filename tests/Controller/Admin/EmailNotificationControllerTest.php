<?php

/*
 * Copyright (C) 2022 by Luc Hamers <vereniging@hamers.de>
 *
 * This file is part of Vereniging.
 *
 * Vereniging is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Vereniging is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Vereniging.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Tests\Controller\Admin;

use App\Entity\ChangeType;
use App\Entity\MemberEntry;
use App\Tests\WebTestCase;
use Doctrine\Common\Collections\Collection;
use Doctrine\Persistence\Mapping\MappingException;
use Exception;
use Symfony\Component\DomCrawler\Crawler;

/**
 * Test class for the mailing lists controller.
 *
 */
class EmailNotificationControllerTest extends WebTestCase
{
    private array $changeTypeIndexes;


    /**
     * @inheritDoc
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->loadAllFixtures();

        // Generate a lookup table so find the table index (nth-child) of a certain change type. Because the nth-child
        // operator is 1-based and the first three columns are used for the delete icon, first name and name, a 4 is
        // added to all indexes.
        $changeTypes = $this->getEntityManager()->getRepository(ChangeType::class)->findby([], ['changeType' => 'ASC']);
        $this->changeTypeIndexes = [];
        foreach ($changeTypes as $key => $changeType) {
            $this->changeTypeIndexes["$changeType"] = $key + 4;
        }
    }


    /**
     * Test if there is a link on the dashboard page to the new member page.
     *
     * @throws Exception
     */
    public function testLinkOnDashboardPage(): void
    {
        $this->checkLinkOnDashboardPage($this, 'Manage email notifications', 'Manage email notifications', '/administration');
    }


    /**
     * Test the index controller
     *
     * @throws Exception
     */
    public function testIndex(): void
    {
        $client = $this->getMyClient();

        $crawler = $client->request('GET', '/administration/manage_email_notifications');

        // Check the page title
        $this->assertEquals(1, $crawler->filter('title:contains("Administration")')->count());
        $this->assertEquals(1, $crawler->filter('title:contains("Manage email notifications")')->count());

        // Check the navigation links
        $this->assertEquals(1, $crawler->filter('a.navbar-brand:contains("the Vereniging member system dashboard")')->count());
        $this->assertEquals(2, $crawler->filter('a.nav-link:contains("Administration")')->count());
        $this->assertEquals(1, $crawler->filter('a.nav-link:contains("Manage email notifications")')->count());

        // Check the table headings
        $this->checkTableHeadings($crawler, false);
        $this->assertEquals(1, $crawler->filter('p:contains("No email notifications found")')->count());
        $members = $this->generateDefaultEmailNotifications();

        $crawler = $client->request('GET', '/administration/manage_email_notifications');

        // Check the table headings
        $this->checkTableHeadings($crawler, true);
        $this->assertEquals(0, $crawler->filter('p:contains("No email notifications found")')->count());

        // Check if all the expected members are present and if the number of checks is correct
        $this->checkMembersPresence($crawler, $members, "testIndex", false);

        // Check some javascripts
        $this->assertEquals(1, $crawler->filter('script:contains("function doAjax")')->count());
        $this->assertEquals(1, $crawler->filter('script:contains("#edit_email_notifications\').on(\'click")')->count());
        $this->assertEquals(1, $crawler->filter('script:contains("#reset_email_notifications_data\').on(\'click")')->count());

        // Check ajax anker
        $this->assertEquals(1, $crawler->filter('div#email_notifications_content')->count());
    }


    /**
     * Test the index controller when the showOnlyContent flag is set.
     *
     * @throws Exception
     */
    public function testIndexWithShowOnlyContentTable(): void
    {
        $client = $this->getMyClient();
        $this->generateDefaultEmailNotifications();

        $crawler = $client->request('GET', '/administration/manage_email_notifications/1');

        // Check the page title
        $this->assertEquals(0, $crawler->filter('title:contains("Administration")')->count());
        $this->assertEquals(0, $crawler->filter('title:contains("Manage email notifications")')->count());

        // Check the navigation links
        $this->assertEquals(0, $crawler->filter('a.navbar-brand:contains("the Vereniging member system dashboard")')->count());
        $this->assertEquals(0, $crawler->filter('a.nav-link:contains("Administration")')->count());
        $this->assertEquals(0, $crawler->filter('a.nav-link:contains("Manage email notifications")')->count());

        // Check the table headings
        $this->checkTableHeadings($crawler, true);
        $this->assertEquals(0, $crawler->filter('p:contains("No email notifications found")')->count());

        // Check if all the expected members are present and if the number of checks is correct
        /** @var MemberEntry[] $members */
        $members = $this->getEntityManager()->getRepository(MemberEntry::class)->findby(['firstName' => ['Firstname1', 'Firstname2', 'Firstname3']]);
        $this->checkMembersPresence($crawler, $members, "testIndexWithShowOnlyContentTable", false);
    }


    /**
     * Test the edit action without submitting data.
     *
     * @throws Exception
     */
    public function testEditShow(): void
    {
        $client = $this->getMyClient();
        $this->generateDefaultEmailNotifications();

        $crawler = $client->request('GET', '/administration/manage_email_notifications_edit');
        $this->assertEquals(1, $crawler->filter('h6.card-header:contains("Email notifications")')->count());

        // Check if the javascript classes used for jquery are present
        $this->assertEquals(1, $crawler->filter('tbody.js-email-notification-table')->count());
        $this->assertEquals(3, $crawler->filter('tr.js-email-notification-item')->count());
        $this->assertEquals(3, $crawler->filter('td a.js-remove-email-notification')->count());
        $this->assertEquals(1, $crawler->filter('td a.js-email-notification-add')->count());
        $this->assertEquals(1, $crawler->filter('tbody[data-prototype]')->count());
        $this->assertEquals(1, $crawler->filter('tbody[data-index=3]')->count());

        // Check some javascripts
        $this->assertEquals(1, $crawler->filter('script:contains("#edit_email_notifications\').on(\'click")')->count());
        $this->assertEquals(1, $crawler->filter('script:contains("#reset_email_notifications_data\').on(\'click")')->count());
        $this->assertEquals(1, $crawler->filter('script:contains("#save_email_notifications_data\').on(\'click")')->count());
        $this->assertEquals(1, $crawler->filter('script:contains("on(\'click\', \'.js-remove-email-notification\', function(e)")')->count());

        // Check if both buttons are available
        $this->assertEquals(1, $crawler->filter('button#save_email_notifications_data[type="submit"]:contains("Save")')->count());
        $this->assertEquals(1, $crawler->filter('button#reset_email_notifications_data[type="reset"]:contains("Cancel")')->count());

        // Check if the input fields are present
        // Check if all the expected members are present and if the number of checks is correct
        /** @var MemberEntry[] $members */
        $members = $this->getEntityManager()->getRepository(MemberEntry::class)->findby(['firstName' => ['Firstname1', 'Firstname2', 'Firstname3']]);
        $this->checkMembersPresence($crawler, $members, "testEditShow", true);
    }


    /**
     * Test the update action by selecting and deselecting checkboxes.
     *
     * @throws Exception
     */
    public function testEditCommitWithCheckboxes(): void
    {
        $this->generateDefaultEmailNotifications();

        // For Firstname1, remove Error and add Export action
        $tick = [[0, 1]];
        $untick = [[0, 0]];
        $crawler = $this->changeCheckboxes($untick, $tick);

        // findBy doesn't reload from the database, so clear the cache by hand to be able to check if the change is
        // saved correctly to the database
        $this->getEntityManager()->clear();
        /** @var MemberEntry[] $members */
        $members = $this->getEntityManager()->getRepository(MemberEntry::class)->findby(['firstName' => ['Firstname1', 'Firstname2', 'Firstname3']]);
        $this->checkMembersPresence($crawler, $members, "testEditCommitWithCheckboxes", false);

        // For Firstname2, remove Error: Firstname2 then has no checkboxes left and should be removed from the view
        $tick = [];
        $untick = [[1, 0]];
        $crawler = $this->changeCheckboxes($untick, $tick);

        $members = $this->getEntityManager()->getRepository(MemberEntry::class)->findby(['firstName' => ['Firstname1', 'Firstname3']]);
        $this->checkMembersPresence($crawler, $members, "testEditCommitWithCheckboxes", false);

        // For Firstname1, remove Export action and Member added: Firstname1 then has no checkboxes left and should be
        // removed from the view. Also remove Error from Firstname3
        $tick = [];
        $untick = [[0, 1], [0, 5], [1, 0]];
        $crawler = $this->changeCheckboxes($untick, $tick);

        $this->assertEquals(1, $crawler->filter('p:contains("No email notifications found")')->count());
    }


    /**
     * Test the update action by deleting or adding rows in the table, to simulate the javascript row delete and add.
     *
     * @throws Exception
     */
    public function testEditCommitWithDeletedAndAddedRows(): void
    {
        $client = $this->getMyClient();
        $members = $this->generateDefaultEmailNotifications();
        $crawler = $client->request('GET', '/administration/manage_email_notifications_edit');
        $this->checkMembersPresence($crawler, $members, "testEditCommitWithDeletedRows", true);

        // Submit only data for the first row, which is like deleting the second row via javascript
        $changeTypes = $members[0]->getEmailNotifications();
        $changeTypeIds = [];
        foreach ($changeTypes as $changeType) {
            $changeTypeIds[] = $changeType->getId();
        }
        $data['email_notifications_collection']['memberEntries'][0] =
            ['emailNotifications' => $changeTypeIds, 'id' => $members[0]->getId()];
        $data['email_notifications_collection']['empty_field'] = 'abcdef';
        $crawler = $client->request('POST', '/administration/manage_email_notifications_edit', $data);
        $members = [$members[0]];
        $this->checkMembersPresence($crawler, $members, "testEditCommitWithDeletedRows", false);

        // Submit no data, which is like deleting the first row via javascript
        $emptyData = [];
        $emptyData['email_notifications_collection']['empty_field'] = 'abcdef';
        $crawler = $client->request('POST', '/administration/manage_email_notifications_edit', $emptyData);
        $this->assertEquals(1, $crawler->filter('p:contains("No email notifications found")')->count());

        // Now add the data from the first test
        $crawler = $client->request('POST', '/administration/manage_email_notifications_edit', $data);
        $members = [$members[0]];
        $this->checkMembersPresence($crawler, $members, "testEditCommitWithDeletedRows", false);
    }


    /**
     * Test the update action by deleting or adding rows in the table, to simulate the javascript row delete and add. In
     * this test, adding and removing is done at the same time since this will create different problems to the form
     * system. These tests were added to simulate bugs which were found with the first implementation.
     *
     * @throws Exception
     */
    public function testEditCommitWithDeletedAndAddedRowsInOneCommit(): void
    {
        $client = $this->getMyClient();
        $members = $this->generateDefaultEmailNotifications();
        $crawler = $client->request('GET', '/administration/manage_email_notifications_edit');
        $this->checkMembersPresence($crawler, $members, "testEditCommitWithDeletedAndAddedRowsInOneCommit", true);

        $changeTypes = $members[0]->getEmailNotifications();
        $changeTypeIds = [];
        foreach ($changeTypes as $changeType) {
            $changeTypeIds[] = $changeType->getId();
        }

//        // First remove and then add the same member
//        $data['email_notifications_collection']['memberEntries'][0] =
//            ['emailNotifications' => $changeTypeIds, 'id' => $members[0]->getId()];
//        // Array index is 3 (should be 1, when the member is only changed). This happens, when the second member is
//        // removed and then added again via javascript.
//        $data['email_notifications_collection']['memberEntries'][3] =
//            ['emailNotifications' => $changeTypeIds, 'id' => $members[1]->getId()];
//        $data['email_notifications_collection']['memberEntries'][2] =
//            ['emailNotifications' => $changeTypeIds, 'id' => $members[2]->getId()];
//        $data['email_notifications_collection']['empty_field'] = 'abcdef';
//        $crawler = $client->request('POST', '/administration/manage_email_notifications_edit', $data);
//
//        EntityTestHelper::getEntityManager()->clear();
//        $members = $this->entityManager->getRepository('CoreDataBundle:MemberEntry')->findby(['firstName' => ['Firstname1', 'Firstname2', 'Firstname3']]);
//        $this->checkMembersPresence($crawler, $members, "testEditCommitWithDeletedRows", false);
//
//        // Now remove one member and add another one. There must be at least 3 members for this.
//        $crawler = $client->request('GET', '/administration/manage_email_notifications_edit');
//        $this->checkMembersPresence($crawler, $members, "testEditCommitWithDeletedAndAddedRowsInOneCommit", true);

        /** @var MemberEntry[] $members */
        $members = $this->getEntityManager()->getRepository(MemberEntry::class)->findby(['firstName' => ['Firstname1', 'Firstname3', 'Firstname4']]);
        $data['email_notifications_collection']['memberEntries'][0] =
            ['emailNotifications' => $changeTypeIds, 'id' => $members[0]->getId()];
        $data['email_notifications_collection']['memberEntries'][3] =
            ['emailNotifications' => $changeTypeIds, 'id' => $members[2]->getId()];
        $data['email_notifications_collection']['memberEntries'][2] =
            ['emailNotifications' => $changeTypeIds, 'id' => $members[1]->getId()];
        $data['email_notifications_collection']['empty_field'] = 'abcdef';
        $crawler = $client->request('POST', '/administration/manage_email_notifications_edit', $data);

        $this->getEntityManager()->clear();
        $members = $this->getEntityManager()->getRepository(MemberEntry::class)->findby(['firstName' => ['Firstname1', 'Firstname3', 'Firstname4']]);
        $this->checkMembersPresence($crawler, $members, "testEditCommitWithDeletedRows", false);
    }


    /**
     * Check for each table line is the needed elements are present and correct. Also check that the expected number of
     * members is found in the table.
     *
     * @param Crawler       $crawler
     * @param MemberEntry[] $members
     * @param string        $testCase
     * @param bool          $editMode When true, search for checkboxed instead of check signs
     */
    private function checkMembersPresence(Crawler $crawler, array $members, string $testCase, bool $editMode): void
    {
        // Check the number of members in the table.
        $this->assertEquals(count($members), $crawler->filter('tbody tr.js-email-notification-item')->count(), 'Expected number of members not found');
        foreach ($members as $key => $member) {
            $message = "$testCase - $member";

            // nth-child is a 1-based counter (so key+1)
            $tableRow = $crawler->filter('tbody tr:nth-child('.($key+1).')');
            $this->assertEquals(1, $tableRow->filter('td:contains("'.$member->getFirstName().'")')->count(), $message);
            $this->assertEquals(1, $tableRow->filter('td:contains("'.$member->getName().'")')->count(), $message);
            $membersEmailNotifications = $member->getEmailNotifications();
            $searchKey = 'span.fa-check';
            if ($editMode) {
                $searchKey = 'input[checked=checked]';
                $this->assertEquals(12, $tableRow->filter('input[type=checkbox]')->count(), $message);
            }

            // Check the number of checked email notifications. For the normal html table, these are fa-checks, for the
            // edit table, these are checked inputs.
            $this->assertEquals(count($member->getEmailNotifications()), $tableRow->filter($searchKey)->count(), $message);
            // Now check if the correct fields are checked, i.e. if the check is in the right column
            foreach ($membersEmailNotifications as $membersEmailNotification) {
                $column = $this->changeTypeIndexes["$membersEmailNotification"];
                $this->assertEquals(1, $tableRow->filter("td:nth-child($column) $searchKey")->count(), $message.": $membersEmailNotification not found in column $column");
            }
        }
    }


    /**
     * Check if the table with the selected email notifications contains the correct headings.
     *
     * @param Crawler $crawler
     * @param bool    $tableDataAvailable When true, there is a table with headings.
     */
    private function checkTableHeadings(Crawler $crawler, bool $tableDataAvailable): void
    {
        $this->assertEquals(1, $crawler->filter('h6.card-header:contains("Email notifications")')->count());
        $this->assertEquals($tableDataAvailable, $crawler->filter('th:contains("First name")')->count());
        $this->assertEquals($tableDataAvailable, $crawler->filter('th:contains("Last name")')->count());
        $this->assertEquals($tableDataAvailable, $crawler->filter('th:contains("Error")')->count());
        $this->assertEquals($tableDataAvailable, $crawler->filter('th:contains("Member data change")')->count());
        $this->assertEquals($tableDataAvailable, $crawler->filter('th:contains("System settings change")')->count());
    }


    /**
     * This method checks and unchecks the checkboxes on the page. For this, a list of checkboxes to tick and a list for
     * those to untick is passed. The arrays have the format [ [row, column], [row, column], [row, column] ], where both
     * are 0-based.
     *
     * @param array $unticks
     * @param array $ticks
     *
     * @return Crawler
     *
     * @throws Exception
     */
    private function changeCheckboxes(array $unticks, array $ticks): Crawler
    {
        $client = $this->getMyClient();
        $crawler = $client->request('GET', '/administration/manage_email_notifications_edit');
        $buttonCrawlerNode = $crawler->selectButton('Save');
        $form = $buttonCrawlerNode->form();

        foreach ($unticks as $untick) {
            $form['email_notifications_collection[memberEntries]['.$untick[0].'][emailNotifications]['.$untick[1].']']->untick();
        }
        foreach ($ticks as $tick) {
            $form['email_notifications_collection[memberEntries]['.$tick[0].'][emailNotifications]['.$tick[1].']']->tick(
            );
        }
        $crawler = $client->submit($form);

        return $crawler;
    }


    /**
     * Add two notifications to member 1 and one to member 2.
     *
     * @return MemberEntry[]
     */
    private function generateDefaultEmailNotifications(): array
    {
        // Make sure that there are no parts of data in the cache!
        $this->getEntityManager()->clear();

        // Add some email notifications since these are not defined in the fixtures. Now the table must be shown.
        /** @var MemberEntry[] $members */
        $members = $this->getEntityManager()->getRepository(MemberEntry::class)
                                       ->findby(['firstName' => ['Firstname1', 'Firstname2', 'Firstname3']]);

        // First add the error change type to members 1 and 2.
        $changeType = $this->getEntityManager()->getRepository(ChangeType::class)
                                               ->findOneBy(['changeType' => 'error']);
        foreach ($members as $member) {
            $member->addEmailNotification($changeType);
        }

        // Now also add the member add notification to member 1.
        $changeType = $this->getEntityManager()->getRepository(ChangeType::class)
                                          ->findOneBy(['changeType' => 'member added']);
        $members[0]->addEmailNotification($changeType);
        $this->getEntityManager()->flush();

        return $members;
    }
}
