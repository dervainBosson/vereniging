<?php

/*
 * Copyright (C) 2022 by Luc Hamers <vereniging@hamers.de>
 *
 * This file is part of Vereniging.
 *
 * Vereniging is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Vereniging is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Vereniging.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Tests\Controller\Admin;

use App\Tests\WebTestCase;
use Exception;
use Symfony\Component\DomCrawler\Crawler;

/**
 * Test class for the membership type controller.
 *
 */
class MembershipTypeControllerTest extends WebTestCase
{
    /**
     * @inheritDoc
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->loadAllFixtures();
    }

    /**
     * Test if there is a link on the dashboard page to the new member page.
     *
     * @throws Exception
     */
    public function testLinkOnDashboardPage(): void
    {
        $this->checkLinkOnDashboardPage($this, 'Manage membership types', 'Manage membership types', '/administration');
    }


    /**
     * Test the index controller.
     *
     * @throws Exception
     */
    public function testIndex(): void
    {
        $client = $this->getMyClient();

        $crawler = $client->request('GET', '/administration/manage_membership_types');

        // Check the page title
        $this->assertEquals(1, $crawler->filter('title:contains("Administration")')->count());
        $this->assertEquals(1, $crawler->filter('title:contains("Manage membership types")')->count());

        // Check the navigation links
        $this->assertEquals(1, $crawler->filter('a.navbar-brand:contains("the Vereniging member system dashboard")')->count());
        $this->assertEquals(2, $crawler->filter('a.nav-link:contains("Administration")')->count());
        $this->assertEquals(1, $crawler->filter('a.nav-link:contains("Manage membership types")')->count());

        $this->checkIndexContent($crawler);

        // Check some javascripts
        $this->assertEquals(1, $crawler->filter('script:contains("function doAjax")')->count());
        $this->assertEquals(1, $crawler->filter('script:contains("#edit_membership_types\').on(\'click")')->count());
        $this->assertEquals(1, $crawler->filter('script:contains("#reset_membership_type_data\').on(\'click")')->count());

        // Check ajax anker
        $this->assertEquals(1, $crawler->filter('div#membership_type_content')->count());
    }


    /**
     * Test the index controller when the showOnlyContent flag is set.
     *
     * @throws Exception
     */
    public function testIndexWithShowOnlyContentTable(): void
    {
        $client = $this->getMyClient();

        $crawler = $client->request('GET', '/administration/manage_membership_types/1');
        // Check the page title
        $this->assertEquals(0, $crawler->filter('title:contains("Administration")')->count());
        $this->assertEquals(0, $crawler->filter('title:contains("Manage membership types")')->count());

        // Check the navigation links
        $this->assertEquals(0, $crawler->filter('a.navbar-brand:contains("the Vereniging member system dashboard")')->count());
        $this->assertEquals(0, $crawler->filter('a.nav-link:contains("Administration")')->count());
        $this->assertEquals(0, $crawler->filter('a.nav-link:contains("Manage membership types")')->count());

        $this->checkIndexContent($crawler);
    }


    /**
     * Test the edit action without submitting data.
     *
     * @throws Exception
     */
    public function testEditShow(): void
    {
        $client = $this->getMyClient();

        $crawler = $client->request('GET', '/administration/manage_membership_types_edit');
        $this->assertEquals(1, $crawler->filter('h6.card-header:contains("Membership types")')->count());

        // Check if the javascript classes used for jquery are present
        $this->assertEquals(1, $crawler->filter('tbody.js-membership-type-table')->count());
        $this->assertEquals(1, $crawler->filter('tbody[data-prototype]')->count());
        $this->assertEquals(1, $crawler->filter('tbody[data-index=4]')->count());
        $this->assertEquals(1, $crawler->filter('tbody.js-membership-type-table')->count());
        $this->assertEquals(4, $crawler->filter('tr.js-membership-type-item')->count());

        // Group member [1] and student member [3] can be removed, family member [0] and regular member [2] cannot be removed
        $this->assertEquals(0, $crawler->filter('tr#membershipType_0>td a.js-remove-membership-type')->count());
        $this->assertEquals(0, $crawler->filter('tr#membershipType_0>td span.far.fa-trash-alt')->count());
        $this->assertEquals(1, $crawler->filter('tr#membershipType_1>td a.js-remove-membership-type')->count());
        $this->assertEquals(0, $crawler->filter('tr#membershipType_1>td span.fa-lock')->count());
        $this->assertEquals(0, $crawler->filter('tr#membershipType_2>td a.js-remove-membership-type')->count());
        $this->assertEquals(1, $crawler->filter('tr#membershipType_2>td span.fa-lock')->count());
        $this->assertEquals(1, $crawler->filter('tr#membershipType_3>td a.js-remove-membership-type')->count());
        $this->assertEquals(1, $crawler->filter('tr#membershipType_3>td span.far.fa-trash-alt')->count());
        $this->assertEquals(1, $crawler->filter('td a.js-membership-type-add')->count());

        // Check if both buttons are available
        $this->assertEquals(1, $crawler->filter('button#save_membership_type_data[type="submit"]:contains("Save")')->count());
        $this->assertEquals(1, $crawler->filter('button#reset_membership_type_data[type="reset"]:contains("Cancel")')->count());

        // Check if the input fields are present
        $this->assertEquals(1, $crawler->filter('tr#membershipType_0>td input[type="text"][value*="Family member"]')->count());
        $this->assertEquals(1, $crawler->filter('tr#membershipType_1>td input[type="text"][value*="Group member"]')->count());
        $this->assertEquals(1, $crawler->filter('tr#membershipType_2>td input[type="text"][value*="Regular member"]')->count());
        $this->assertEquals(1, $crawler->filter('tr#membershipType_3>td input[type="text"][value*="Student member"]')->count());
        $this->assertEquals(1, $crawler->filter('tr#membershipType_0>td input[type="text"][value*="36"]')->count());
        $this->assertEquals(1, $crawler->filter('tr#membershipType_1>td input[type="text"][value*="36"]')->count());
        $this->assertEquals(1, $crawler->filter('tr#membershipType_2>td input[type="text"][value*="24"]')->count());
        $this->assertEquals(1, $crawler->filter('tr#membershipType_3>td input[type="text"][value*="12"]')->count());
        $this->assertEquals(4, $crawler->filter('input[type="checkbox"]')->count());
        $this->assertEquals(8, $crawler->filter('input[type="radio"]')->count());
        $this->assertEquals(4, $crawler->filter('input[checked="checked"]')->count());
        $this->assertEquals(1, $crawler->filter('tr#membershipType_0>td:nth-child(7) input[checked="checked"]')->count());
        $this->assertEquals(1, $crawler->filter('tr#membershipType_0>td:nth-child(8) input[checked="checked"]')->count());
        $this->assertEquals(1, $crawler->filter('tr#membershipType_2>td:nth-child(9) input[checked="checked"]')->count());
    }


    /**
     * Test the update action.
     *
     * @throws Exception
     */
    public function testEditCommit()
    {
        $client = $this->getMyClient();

        // Change members content
        $crawler = $client->request('GET', '/administration/manage_membership_types_edit');
        $buttonCrawlerNode = $crawler->selectButton('Save');
        $form = $buttonCrawlerNode->form();
        $form['form_collection[members][3][type]'] = 'special member';
        $form['form_collection[members][3][type_de]'] = 'besonderes Mitglied';
        $form['form_collection[members][3][type_es]'] = '';
        $form['form_collection[members][3][type_nl]'] = 'bijzonder lid';
        $form['form_collection[members][3][membershipFee]'] = 23;
        $form['form_collection[members][3][groupMembership]']->tick();
        $form['form_collection[members][3][groupMembership]']->tick();
        $form['form_collection[members][3][defaultGroupMembershipType]']->select(1);
        $form['form_collection[members][3][defaultNewMembershipType]']->select(1);

        $crawler = $client->submit($form);

        // Check if all the expected membership types are present
        $expected = [
            [
                'type' => 'Family member',
                'type_de' => 'Familienmitglied',
                'type_es' => '',
                'type_nl' => 'Familielid',
                'membershipFee' => "36.00",
                'groupMembership' => true,
                'defaultGroupMembershipType' => false,
                'defaultNewMembershipType' => false,
            ],
            [
                'type' => 'Group member',
                'type_de' => 'Gruppenmitglied',
                'type_es' => '',
                'type_nl' => 'Groepslid',
                'membershipFee' => "36.00",
                'groupMembership' => true,
                'defaultGroupMembershipType' => false,
                'defaultNewMembershipType' => false,
            ],
            [
                'type' => 'Regular member',
                'type_de' => 'Reguläres Mitglied',
                'type_es' => '',
                'type_nl' => 'Regulier lid',
                'membershipFee' => "24.00",
                'groupMembership' => false,
                'defaultGroupMembershipType' => false,
                'defaultNewMembershipType' => false,
            ],
            [
                'type' => 'special member',
                'type_de' => 'besonderes Mitglied',
                'type_es' => '',
                'type_nl' => 'bijzonder lid',
                'membershipFee' => "23.00",
                'groupMembership' => true,
                'defaultGroupMembershipType' => true,
                'defaultNewMembershipType' => true,
            ],
        ];
        $this->checkMembersPresence($crawler, $expected, "testEditCommit");

        // Check if clearing the type name or the membership fee is handled correctly (type name has to be reset to the
        // original value, the empty membership fee is set to 0)
        $crawler = $client->request('GET', '/administration/manage_membership_types_edit');
        $buttonCrawlerNode = $crawler->selectButton('Save');
        $form = $buttonCrawlerNode->form();
        $form['form_collection[members][2][type]'] = '';
        $form['form_collection[members][3][membershipFee]'] = '';

        $crawler = $client->submit($form);
        // Check if all the expected membership types are present
        $expected = [
            [
                'type' => 'Family member',
                'type_de' => 'Familienmitglied',
                'type_es' => '',
                'type_nl' => 'Familielid',
                'membershipFee' => "36.00",
                'groupMembership' => true,
                'defaultGroupMembershipType' => false,
                'defaultNewMembershipType' => false,
            ],
            [
                'type' => 'Group member',
                'type_de' => 'Gruppenmitglied',
                'type_es' => '',
                'type_nl' => 'Groepslid',
                'membershipFee' => "36.00",
                'groupMembership' => true,
                'defaultGroupMembershipType' => false,
                'defaultNewMembershipType' => false,
            ],
            [
                'type' => 'Regular member',
                'type_de' => 'Reguläres Mitglied',
                'type_es' => '',
                'type_nl' => 'Regulier lid',
                'membershipFee' => "24.00",
                'groupMembership' => false,
                'defaultGroupMembershipType' => false,
                'defaultNewMembershipType' => false,
            ],
            [
                'type' => 'special member',
                'type_de' => 'besonderes Mitglied',
                'type_es' => '',
                'type_nl' => 'bijzonder lid',
                'membershipFee' => "0.00",
                'groupMembership' => true,
                'defaultGroupMembershipType' => true,
                'defaultNewMembershipType' => true,
            ],
        ];
        $this->checkMembersPresence($crawler, $expected, "testEditCommit");

        // Remove member
        $crawler = $client->request('GET', '/administration/manage_membership_types_edit');
        $buttonCrawlerNode = $crawler->selectButton('Save');
        $form = $buttonCrawlerNode->form();
        unset($form['form_collection[members][3]']);

        $crawler = $client->submit($form);

        // Check if all the expected membership types are present
        $expected = [
            [
                'type' => 'Family member',
                'type_de' => 'Familienmitglied',
                'type_es' => '',
                'type_nl' => 'Familielid',
                'membershipFee' => "36.00",
                'groupMembership' => true,
                'defaultGroupMembershipType' => true,
                'defaultNewMembershipType' => true,
            ],
            [
                'type' => 'Group member',
                'type_de' => 'Gruppenmitglied',
                'type_es' => '',
                'type_nl' => 'Groepslid',
                'membershipFee' => "36.00",
                'groupMembership' => true,
                'defaultGroupMembershipType' => false,
                'defaultNewMembershipType' => false,
            ],
            [
                'type' => 'Regular member',
                'type_de' => 'Reguläres Mitglied',
                'type_es' => '',
                'type_nl' => 'Regulier lid',
                'membershipFee' => "24.00",
                'groupMembership' => false,
                'defaultGroupMembershipType' => false,
                'defaultNewMembershipType' => false,
            ],
        ];
        $this->checkMembersPresence($crawler, $expected, "testIndexWithShowOnlyContentTable");


        // Add member
        $crawler = $client->request('GET', '/administration/manage_membership_types_edit');
        $buttonCrawlerNode = $crawler->selectButton('Save');
        $form = $buttonCrawlerNode->form();
        $data = $form->getPhpValues();
        $data['form_collection']['members'][] = [
            'type' => 'New member',
            'membershipFee' => "15.00",
        ];
        $expected = [
            [
                'type' => 'Family member',
                'type_de' => 'Familienmitglied',
                'type_es' => '',
                'type_nl' => 'Familielid',
                'membershipFee' => "36.00",
                'groupMembership' => true,
                'defaultGroupMembershipType' => true,
                'defaultNewMembershipType' => true,
            ],
            [
                'type' => 'Group member',
                'type_de' => 'Gruppenmitglied',
                'type_es' => '',
                'type_nl' => 'Groepslid',
                'membershipFee' => "36.00",
                'groupMembership' => true,
                'defaultGroupMembershipType' => false,
                'defaultNewMembershipType' => false,
            ],
            [
                'type' => 'New member',
                'type_de' => '',
                'type_es' => '',
                'type_nl' => '',
                'membershipFee' => "15.00",
                'groupMembership' => false,
                'defaultGroupMembershipType' => false,
                'defaultNewMembershipType' => false,
            ],
            [
                'type' => 'Regular member',
                'type_de' => 'Reguläres Mitglied',
                'type_es' => '',
                'type_nl' => 'Regulier lid',
                'membershipFee' => "24.00",
                'groupMembership' => false,
                'defaultGroupMembershipType' => false,
                'defaultNewMembershipType' => false,
            ],
        ];
        $crawler = $client->request('POST', '/administration/manage_membership_types_edit', $data);
        $this->checkMembersPresence($crawler, $expected, "testIndexWithShowOnlyContentTable");

        // Add member
        $crawler = $client->request('GET', '/administration/manage_membership_types_edit');
        $buttonCrawlerNode = $crawler->selectButton('Save');
        $form = $buttonCrawlerNode->form();
        $data = $form->getPhpValues();
        $data['form_collection']['members'][] = [
            'type' => '',
            'membershipFee' => "15.00",
            'groupMembership' => false,
            'defaultGroupMembershipType' => false,
            'defaultNewMembershipType' => false,
        ];
        $crawler = $client->request('POST', '/administration/manage_membership_types_edit', $data);
        $this->checkMembersPresence($crawler, $expected, "testIndexWithShowOnlyContentTable");
    }


    /**
     * @param Crawler $crawler
     * @param array[] $typesExpected
     * @param string  $testCase
     */
    private function checkMembersPresence(Crawler $crawler, array $typesExpected, string $testCase): void
    {
        $this->assertEquals(count($typesExpected), $crawler->filter('tr.js-membership-type-item')->count());
        foreach ($typesExpected as $key => $type) {
            $message = "$testCase - ".$type['type'];
            $cellSelector = 'tr#membershipType_'.$key.'>';
            $this->assertEquals(1, $crawler->filter($cellSelector.'td:nth-child(2):contains("'.$type['type'].'")')->count(), $message.' - type');
            $this->assertEquals(1, $crawler->filter($cellSelector.'td:nth-child(3):contains("'.$type['type_de'].'")')->count(), $message.' - type');
            $this->assertEquals(1, $crawler->filter($cellSelector.'td:nth-child(4):contains("'.$type['type_es'].'")')->count(), $message.' - type');
            $this->assertEquals(1, $crawler->filter($cellSelector.'td:nth-child(5):contains("'.$type['type_nl'].'")')->count(), $message.' - type');
            $this->assertEquals(1, $crawler->filter($cellSelector.'td:nth-child(6):contains("'.$type['membershipFee'].'")')->count(), $message.' - membershipFee');
            $this->assertEquals((int) $type['groupMembership'], $crawler->filter($cellSelector.'td:nth-child(7)>span[class*=fa-check]')->count(), $message.' - groupMembership');
            $this->assertEquals((int) $type['defaultGroupMembershipType'], $crawler->filter($cellSelector.'td:nth-child(8)>span[class*=fa-check]')->count(), $message.' - defaultGroupMembershipType');
            $this->assertEquals((int) $type['defaultNewMembershipType'], $crawler->filter($cellSelector.'td:nth-child(9)>span[class*=fa-check]')->count(), $message.' - defaultNewMembershipType');
        }
    }


    /**
     * @param Crawler $crawler
     */
    private function checkIndexContent(Crawler $crawler)
    {
        // Check the table headings
        $this->assertEquals(1, $crawler->filter('h6.card-header:contains("Membership types")')->count());
        $this->assertEquals(1, $crawler->filter('th:contains("Membership type (en)")')->count());
        $this->assertEquals(1, $crawler->filter('th:contains("Membership type (de)")')->count());
        $this->assertEquals(1, $crawler->filter('th:contains("Membership type (es)")')->count());
        $this->assertEquals(1, $crawler->filter('th:contains("Membership type (nl)")')->count());
        $this->assertEquals(1, $crawler->filter('th:contains("Contribution claim")')->count());
        $this->assertEquals(1, $crawler->filter('th:contains("Group membership")')->count());
        $this->assertEquals(1, $crawler->filter('th:contains("Default for member groups")')->count());
        $this->assertEquals(1, $crawler->filter('th:contains("Default for new members")')->count());

        // Check if all the expected membership types are present
        $expected = [
            [
                'type' => 'Family member',
                'type_de' => 'Familienmitglied',
                'type_es' => '',
                'type_nl' => 'Familielid',
                'membershipFee' => 36,
                'groupMembership' => true,
                'defaultGroupMembershipType' => true,
                'defaultNewMembershipType' => false,
            ],
            [
                'type' => 'Group member',
                'type_de' => 'Gruppenmitglied',
                'type_es' => '',
                'type_nl' => 'Groepslid',
                'membershipFee' => 36,
                'groupMembership' => true,
                'defaultGroupMembershipType' => false,
                'defaultNewMembershipType' => false,
            ],
            [
                'type' => 'Regular member',
                'type_de' => 'Reguläres Mitglied',
                'type_es' => '',
                'type_nl' => 'Regulier lid',
                'membershipFee' => 24,
                'groupMembership' => false,
                'defaultGroupMembershipType' => false,
                'defaultNewMembershipType' => true,
            ],
            [
                'type' => 'Student member',
                'type_de' => 'Studentisches Mitglied',
                'type_es' => '',
                'type_nl' => 'Student lid',
                'membershipFee' => 12,
                'groupMembership' => false,
                'defaultGroupMembershipType' => false,
                'defaultNewMembershipType' => false,
            ],
        ];
        $this->checkMembersPresence($crawler, $expected, "testIndexWithShowOnlyContentTable");
    }
}
