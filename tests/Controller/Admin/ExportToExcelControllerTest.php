<?php

/*
 * Copyright (C) 2022 by Luc Hamers <vereniging@hamers.de>
 *
 * This file is part of Vereniging.
 *
 * Vereniging is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Vereniging is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Vereniging.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Tests\Controller\Admin;

use App\Entity\LogfileEntry;
use App\Entity\Status;
use App\Service\GenerateExcelDataFile;
use App\Tests\WebTestCase;
use Exception;
use PhpOffice\PhpSpreadsheet\IOFactory;

/**
 * Test calls to the export to excel controller
 *
 * Class ExportToExcelControllerTest
 */
class ExportToExcelControllerTest extends WebTestCase
{
    /**
     * @inheritDoc
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->loadAllFixtures();
    }


    /**
     * Test if there is a link on the dashboard page to the new member page.
     *
     * @throws Exception
     */
    public function testLinkOnDashboardPage(): void
    {
        $this->checkLinkOnDashboardPage($this, 'Export to Excel', 'Export to Excel', '/administration');
    }


    /**
     * Test the index controller.
     *
     * @throws Exception
     */
    public function testIndex(): void
    {
        $client = $this->getMyClient();

        $crawler = $client->request('GET', '/administration/export_excel');

        // Check the page title
        $this->assertEquals(1, $crawler->filter('title:contains("Administration")')->count());
        $this->assertEquals(1, $crawler->filter('title:contains("Export to Excel")')->count());

        // Check the navigation links
        $this->assertEquals(1, $crawler->filter('a.navbar-brand:contains("the Vereniging member system dashboard")')->count());
        $this->assertEquals(2, $crawler->filter('a.nav-link:contains("Administration")')->count());
        $this->assertEquals(1, $crawler->filter('a.nav-link:contains("Export to Excel")')->count());

        // Check the selection form
        $this->assertEquals(17, $crawler->filter('form#selection_form input[type=checkbox]')->count());
        $this->assertEquals(3, $crawler->filter('form#selection_form input[checked=checked]')->count());

        // Check the result table
        $this->assertEquals(1, $crawler->filter('div#member_table')->filter('div.card-body:contains("Selected 11 member entries")')->count());
        // For the excel selection to work, the content of the form has to be replicated into a hidden form
        $this->assertEquals(17, $crawler->filter('form#hidden_selection_form input[type=checkbox]')->count());
        $this->assertEquals(3, $crawler->filter('form#hidden_selection_form input[checked=checked]')->count());

        $form = $crawler->filter("#selection_form")->form();
        $form['member_lists_selection[status]'][2]->untick();

        // Submit form with all checkboxes cleared --> the header should be "None", the content empty
        $crawler = $client->submit($form);
        $this->assertEquals(1, $crawler->filter('div.card-body:contains("No member entries found")')->count());
    }


    /**
     * Test updating the member list.
     *
     * @throws Exception
     */
    public function testShow(): void
    {
        $client = $this->getMyClient();

        $crawler = $client->request('GET', '/administration/export_excel');

        $form = $crawler->filter("#selection_form")->form();
        // Add future member status. This should increase the number of members from 11 to 12.
        $form['member_lists_selection[status]'][1]->tick();
        $crawler = $client->submit($form);

        // Check the result table
        $this->assertEquals(1, $crawler->filter('div.card-body:contains("Selected 12 member entries")')->count());
        // For the excel selection to work, the content of the form has to be replicated into a hidden form
        $this->assertEquals(17, $crawler->filter('form#hidden_selection_form input[type=checkbox]')->count());
        $this->assertEquals(4, $crawler->filter('form#hidden_selection_form input[checked=checked]')->count());
    }


    /**
     * Test updating the member list.
     *
     * @throws Exception
     */
    public function testDownloadExcelFile(): void
    {
        $client = $this->getMyClient();

        $crawler = $client->request('GET', '/administration/export_excel');

        $form = $crawler->filter("#hidden_selection_form")->form();
        $randomFileName = '/tmp/test'.rand(1, 1000).'.xlsx';
        ob_start();
        // Send the response to the output buffer
        $client->submit($form);
        // Get the contents of the output buffer
        $content = ob_get_contents();
        // Clean the output buffer and end it
        ob_end_clean();
        file_put_contents($randomFileName, $content);
        $excelFile = IOFactory::load($randomFileName);
        $this->assertEquals('Lastname1', $excelFile->getActiveSheet()->getCell('A3')->getValue());
        $this->assertEquals('Lastname9', $excelFile->getActiveSheet()->getCell('A13')->getValue());
        $this->assertNull($excelFile->getActiveSheet()->getCell('A14')->getValue());
        unlink($randomFileName);
    }


    /**
     * Test reaction to an exception generated during file creation
     *
     * @throws Exception
     */
    public function testDownloadExcelFileWithError(): void
    {
        $client = $this->getMyClient();
        $generatorMock = $this->createMock(GenerateExcelDataFile::class);
        $generatorMock->method('createExcelFile')
                      ->will($this->throwException((new \Exception('Testexception'))));

        $status = $this->getEntityManager()->getRepository(Status::class)
                                           ->findOneBy(['status' => 'member']);
        $data = [
            "member_lists_selection" => [
                "status" => [$status->getId()],
                "email" => "all",
                ],
        ];

        $client->getContainer()->set(GenerateExcelDataFile::class, $generatorMock);
        $crawler = $client->request('POST', '/administration/export_excel_download', $data);
        $this->assertEquals('Exporting excel file failed!', $crawler->text());
        $logMessage = $this->getEntityManager()->getRepository(LogfileEntry::class)
                                               ->findOneBy([], ['id' => 'DESC']);
        $this->assertEquals('Export to excel file failed', $logMessage->getLogentry());
    }


    /**
     * Test updating the member list.
     *
     * @throws Exception
     */
    public function testDownloadExcelFileWithCommitteeFunction(): void
    {
        $client = $this->getMyClient();

        $crawler = $client->request('GET', '/administration/export_excel');

        $form = $crawler->filter("#selection_form")->form();
        // Select activities.
        $form['member_lists_selection[committee]'][0]->tick();
        $crawler = $client->submit($form);

        $form = $crawler->filter("#hidden_selection_form")->form();
        $randomFileName = '/tmp/test'.rand(1, 1000).'.xlsx';
        ob_start();
        // Send the response to the output buffer
        $client->submit($form);
        // Get the contents of the output buffer
        $content = ob_get_contents();
        // Clean the output buffer and end it
        ob_end_clean();
        file_put_contents($randomFileName, $content);
        $excelFile = IOFactory::load($randomFileName);
        $this->assertEquals('Lastname1', $excelFile->getActiveSheet()->getCell('A3')->getValue());
        $this->assertEquals('Lastname2', $excelFile->getActiveSheet()->getCell('A4')->getValue());
        $this->assertNull($excelFile->getActiveSheet()->getCell('A5')->getValue());
        unlink($randomFileName);
    }
}
