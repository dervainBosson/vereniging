<?php

/*
 * Copyright (C) 2022 by Luc Hamers <vereniging@hamers.de>
 *
 * This file is part of Vereniging.
 *
 * Vereniging is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Vereniging is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Vereniging.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Tests\Controller\Member;

use App\Entity\Language;
use App\Entity\MemberEntry;
use App\Tests\WebTestCase;
use Exception;
use Symfony\Component\DomCrawler\Crawler;
use Symfony\Component\DomCrawler\Form;

/**
 * Class MemberControllerTest
 */
class NewMemberControllerTest extends WebTestCase
{
    /**
     * @inheritDoc
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->loadAllFixtures();
    }


    /**
     * Test if there is a link on the dashboard page to the new member page.
     *
     * @throws Exception
     */
    public function testLinkOnDashboardPage(): void
    {
        $this->checkLinkOnDashboardPage($this, 'New user', 'Create new member', '/dashboard');
    }


    /**
     * Test calling the log file view for a member.
     */
    public function testShowPage(): void
    {
        $crawler = $this->client->request('GET', '/create_new_member');

        // Check the page title
        $this->assertEquals(1, $crawler->filter('title:contains("Create new member")')->count());

        // Check the navigation links
        $this->assertEquals(1, $crawler->filter('a.navbar-brand:contains("the Vereniging member system dashboard")')->count());
        $this->assertEquals(1, $crawler->filter('a.nav-link:contains("Create new member")')->count());

        // Check if all labels are present. Since every string is encapsulated in 2 label tags, there is a multiple of 2.
        $this->assertEquals(1, $crawler->filter('label.control-label:contains("First name")')->count());
        $this->assertEquals(1, $crawler->filter('label.control-label:contains("Name")')->count());
        $this->assertEquals(1, $crawler->filter('label.control-label:contains("Gender")')->count());
        $this->assertEquals(1, $crawler->filter('label.control-label:contains("User status")')->count());
        $this->assertEquals(1, $crawler->filter('label.control-label:contains("Preferred language")')->count());
        $this->assertEquals(1, $crawler->filter('label:contains("Male")')->count());
        $this->assertEquals(1, $crawler->filter('label:contains("Female")')->count());
        $this->assertEquals(1, $crawler->filter('label:contains("Diverse")')->count());
        $this->assertEquals(1, $crawler->filter('label:contains("None")')->count());
    }


    /**
     * Test creating a new member
     */
    public function testCreateNewMember(): void
    {
        $this->client->followRedirects(true);
        $crawler = $this->client->request('GET', '/create_new_member');

        $buttonCrawlerNode = $crawler->selectButton('Create member');
        $form = $buttonCrawlerNode->form();
        // Change all options to see if the form can be saved with changes
        $statusId = $this->getFixtureReference('status_future_member')->getId();
        $crawler = $this->submitFormWithDefaultValues($statusId, $form);

        $this->assertEquals(1, $crawler->filter('title:contains("View and edit data")')->count());
        $this->assertEquals(1, $crawler->filter('a.nav-link:contains("View and edit data")')->count());
        // Since every string is encapsulated in 2 div tags (in class="row"), there is a multiple of 2.
        $this->assertEquals(2, $crawler->filter('div.row:contains("first name'.$statusId.'")')->count());
        $this->assertEquals(2, $crawler->filter('div.row:contains("last name")')->count());
        $this->assertEquals(2, $crawler->filter('div.row:contains("Female")')->count());
        $this->assertEquals(2, $crawler->filter('div.row:contains("English")')->count());
        $this->assertEquals(1, $crawler->filter('div.col-sm-10:contains("User")')->count());
        // Check if the membership number is empty
        $this->assertStringContainsString('>-</div>', $crawler->filter('div.row:contains("Membership number")')->html());

        // Check if there is a log message for this new member, which is linked to the member
        $logMessage = $this->getAllLogs()[0];
        $member = $this->getEntityManager()->getRepository(MemberEntry::class)->findOneBy(['firstName' => "first name$statusId"]);
        $this->assertNotNull($logMessage);
        $this->assertStringContainsString("first name$statusId", $logMessage->getLogentry());
        $this->assertEquals($member, $logMessage->getChangesOnMember());
        $this->assertEquals('Member added', $logMessage->getChangeType()->getChangeType());
    }


    /**
     * Check if the form validation works correctly
     */
    public function testCreateNewMemberWithErrors(): void
    {
        $this->client->followRedirects(true);
        $crawler = $this->client->request('GET', '/create_new_member');

        $buttonCrawlerNode = $crawler->selectButton('Create member');
        $form = $buttonCrawlerNode->form();
        $crawler = $this->client->submit($form);

        $this->assertEquals(1, $crawler->filter('title:contains("Create new member")')->count());
        $this->assertEquals(3, $crawler->filter('span.form-error-message:contains("This value should not be blank")')->count());
    }


    /**
     * Test if the membership number is set when creating a member with a membership type that has a membership number.
     */
    public function testCreateNewMemberWithMembershipNumber(): void
    {
        $this->client->followRedirects(true);
        $crawler = $this->client->request('GET', '/create_new_member');

        $buttonCrawlerNode = $crawler->selectButton('Create member');
        $form = $buttonCrawlerNode->form();
        $statusId = $this->getFixtureReference('status_member')->getId();
        $crawler = $this->submitFormWithDefaultValues($statusId, $form);

        // Check that the membership number is not empty
        $this->assertStringNotContainsString('>-</div>', $crawler->filter('div.row:contains("Membership number")')->html());

        $member = $this->getEntityManager()->getRepository(MemberEntry::class)->findOneBy(['firstName' => "first name$statusId"]);
        $membershipNumber = $member->getMembershipNumber();
        $this->assertNotNull($membershipNumber);
        $this->assertStringContainsString(">$membershipNumber</div>", $crawler->filter('div.row:contains("Membership number")')->html());
    }


    /**
     * @param int  $statusId
     * @param Form $form
     *
     * @return Crawler
     */
    private function submitFormWithDefaultValues(int $statusId, Form $form): Crawler
    {
        $form['new_member_form[firstName]'] = "first name$statusId";
        $form['new_member_form[name]'] = "last name";
        $form['new_member_form[gender]']->select('f');
        $form['new_member_form[status]']->select($statusId);
        $languageEnglishId = $this->getEntityManager()->getRepository(Language::class)->findOneBy(['language' => 'English'])->getId();
        $form['new_member_form[preferredLanguage]']->select($languageEnglishId);

        return $this->client->submit($form);
    }
}
