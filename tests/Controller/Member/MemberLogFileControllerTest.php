<?php

/*
 * Copyright (C) 2022 by Luc Hamers <vereniging@hamers.de>
 *
 * This file is part of Vereniging.
 *
 * Vereniging is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Vereniging is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Vereniging.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Tests\Controller\Member;

use App\Entity\ChangeType;
use App\Entity\LogfileEntry;
use App\Entity\MemberEntry;
use App\Tests\WebTestCase;

/**
 * Class MemberControllerTest
 */
class MemberLogFileControllerTest extends WebTestCase
{
    private ?MemberEntry $member1;


    /**
     * @inheritDoc
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->loadAllFixtures();
        $this->member1 = $this->getFixtureReference('member_entry_last_name1');
        $this->setLoggedInUserForLogging();
    }


    /**
     * Test calling the log file view for a member.
     */
    public function testShowMemberLogFileAction(): void
    {
        /** @var ChangeType $changeType */
        $changeType = $this->getFixtureReference('change_type_member_data_change');
        for ($i = 0; $i < 5; ++$i) {
            $logEntry = new LogfileEntry();
            $logEntry->setChangedByMember($this->member1);
            $logEntry->setChangesOnMember($this->member1);
            $logEntry->setChangeType($changeType);
            $logEntry->setLogentry("Test$i");
            $this->getEntityManager()->persist($logEntry);
            $this->getEntityManager()->flush();
        }

        $crawler = $this->client->request('GET', '/member/show_member_logs/'.$this->member1->getId());

        $this->assertEquals(1, $crawler->filter('td:contains("Test0")')->count());
        $this->assertEquals(1, $crawler->filter('td:contains("Test1")')->count());
        $this->assertEquals(1, $crawler->filter('td:contains("Test2")')->count());
        $this->assertEquals(1, $crawler->filter('td:contains("Test3")')->count());
        $this->assertEquals(1, $crawler->filter('td:contains("Test4")')->count());
    }


    /**
     * Test if log messages are translated correctly. To check, the email address of the member is changed
     */
    public function testShowMemberLogFileActionWithTranslations(): void
    {
        $this->member1->setEmail('new@email.com');
        $this->member1->setGender('f');
        $this->getEntityManager()->flush();

        $crawler = $this->client->request('GET', '/member/show_member_logs/'.$this->member1->getId());
        // The following tests check for more than 1, because they are encapsulated in multiple td tags
        $this->assertCount(1, $crawler->filter('td:contains("Updated entry of type member")'));
        $this->assertCount(1, $crawler->filter('td:contains("Gender")'));
        $this->assertCount(1, $crawler->filter('td:contains("Male")'));
        $this->assertCount(1, $crawler->filter('td:contains("Female")'));
    }
}
