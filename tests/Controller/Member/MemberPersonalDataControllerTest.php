<?php

/*
 * Copyright (C) 2022 by Luc Hamers <vereniging@hamers.de>
 *
 * This file is part of Vereniging.
 *
 * Vereniging is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Vereniging is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Vereniging.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Tests\Controller\Member;

use App\Entity\MemberEntry;
use App\Tests\WebTestCase;
use Exception;
use Symfony\Component\PasswordHasher\Hasher\PasswordHasherFactory;

/**
 * Class MemberPersonalDataControllerTest
 */
class MemberPersonalDataControllerTest extends WebTestCase
{
    private ?MemberEntry $member1;


    /**
     * @inheritDoc
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->loadAllFixtures();
        $this->member1 = $this->getFixtureReference('member_entry_last_name1');
        $this->setLoggedInUserForLogging(true);
    }


    /**
     * Test if clicking the edit button changed the output to a form.
     *
     * @throws Exception
     */
    public function testShowEdit(): void
    {
        // This should show input fields for the personal data fields
        $crawler = $this->getMyClient()->request('GET', '/member/edit_personal_data/'.$this->member1->getId());

        // Only personal data title should appear
        $this->assertGreaterThan(0, $crawler->filter('h6.card-header:contains("Personal data")')->count());
        $this->assertEquals(0, $crawler->filter('h6.card-header:contains("Contact data")')->count());
        $this->assertEquals(0, $crawler->filter('h6.card-header:contains("Company data")')->count());
        $this->assertEquals(0, $crawler->filter('h6.card-header:contains("Club data")')->count());

        // There should be four radio fields for the gender (male, female, diverse, none), the male field should be selected.
        $this->assertEquals(4, $crawler->filter('input[name="member_entry_personal_data[gender]"]')->count());
        $this->assertEquals(1, $crawler->filter('input[name="member_entry_personal_data[gender]"][value="m"]')->count());
        $this->assertEquals(1, $crawler->filter('input[name="member_entry_personal_data[gender]"][value="f"]')->count());
        $this->assertEquals(1, $crawler->filter('input[name="member_entry_personal_data[gender]"][value="d"]')->count());
        $this->assertEquals(1, $crawler->filter('input[name="member_entry_personal_data[gender]"][value="n"]')->count());
        $this->assertEquals(1, $crawler->filter('input[value="m"][checked="checked"]')->count());

        // It should be possible to select an empty title (id ends with memberentry_title)
        $this->assertEquals(1, $crawler->filter('select[id$="member_entry_personal_data_title"]>option[value=""]')->count());
        // The second title should be selected
        $this->assertEquals(1, $crawler->filter('option[selected="selected"]:contains("Dipl.-Ing.")')->count());

        $this->assertEquals(1, $crawler->filter('label:contains("New password")')->count());
        $this->assertEquals(1, $crawler->filter('label:contains("Repeat new password")')->count());
    }


    /**
     * Test if changes in the form are written to the database and then shown correctly.
     *
     * @throws Exception
     */
    public function testSubmitEdit(): void
    {
        // This should show input fields for the personal data fields
        $crawler = $this->getMyClient()->request('GET', '/member/edit_personal_data/'.$this->member1->getId());
        $buttonCrawlerNode = $crawler->selectButton('Save');
        $form = $buttonCrawlerNode->form();

        // Change two options to see if the form can be saved with changes
        $form['member_entry_personal_data[title]']->select($this->getFixtureReference('title2')->getId());
        $form['member_entry_personal_data[gender]']->select('d');
        $crawler = $this->client->submit($form);

        // The title "M.A." and the gender "Female" should be selected
        $this->assertTrue($crawler->filter('div:contains("M.A.")')->count() > 0);
        // This now only checks the gender, but also the language (Dutch)
        $this->assertTrue($crawler->filter('div:contains("Diverse")')->count() > 0);
    }


    /**
     * When the language is changed, then the system also changes the gui language when the language is changed by the
     * currently logged-in user.
     *
     * @throws Exception
     */
    public function testChangeLanguage(): void
    {
        // Select member2 (language is set to German). Now change the language to dutch. This should NOT change the gui
        // language, because we are logged in as member1.
        $member2 = $this->getFixtureReference('member_entry_last_name2');
        $crawler = $this->getMyClient()->request('GET', '/member/edit_personal_data/'.$member2->getId());
        $buttonCrawlerNode = $crawler->selectButton('Save');
        $form = $buttonCrawlerNode->form();

        // Set language to dutch for member2
        $form['member_entry_personal_data[preferredLanguage]']->select($this->getFixtureReference('language_dutch')->getId());
        $crawler = $this->client->submit($form);
        // Since the gui language will not be changed changed, a javascript reload command should not be found
        $this->assertEquals(0, $crawler->filter('script:contains("setTimeout(pageReload")')->count());

        // This checks that the language has been changed to dutch, but also that the gui stays in English
        $crawler = $this->client->request('GET', '/member/firstname2.lastname2');
        $this->assertTrue($crawler->filter('div:contains("Dutch")')->count() > 0);


        // Then change the language of the currently logged in user. Changing the langauge should also change the gui
        // language.
        $crawler = $this->client->request('GET', '/member/edit_personal_data/'.$this->member1->getId());
        $buttonCrawlerNode = $crawler->selectButton('Save');
        $form = $buttonCrawlerNode->form();
        $form['member_entry_personal_data[preferredLanguage]']->select($this->getFixtureReference('language_dutch')->getId());
        $crawler = $this->client->submit($form);
        // Since the gui language has to be changed, a javascript reload command has to be found
        $this->assertEquals(1, $crawler->filter('script:contains("setTimeout(pageReload")')->count());

        // This checks that the language has been changed to Dutch as well as the gui
        $crawler = $this->client->request('GET', '/member/firstname1.lastname1');
        $this->assertTrue($crawler->filter('div:contains("Nederlands")')->count() > 0);
    }


    /**
     * Test that Javascript tags are not executed
     *
     * @throws Exception
     */
    public function testSubmitJavascriptTags(): void
    {
        // This should show input fields for the personal data fields
        $crawler = $this->getMyClient()->request('GET', '/member/edit_personal_data/'.$this->member1->getId());
        $buttonCrawlerNode = $crawler->selectButton('Save');
        $form = $buttonCrawlerNode->form();
        $form['member_entry_personal_data[firstName]'] = 'Firstname1<script>';
        $form['member_entry_personal_data[name]'] = 'Lastname1<script>';
        $crawler = $this->client->submit($form);

        // < and > have to be replaced by &lt; and &gt;
        $this->assertEquals(2, substr_count($crawler->html(), '&lt;script&gt;'));
    }


    /**
     * Clicking the cancel button would just render the personal data section with edit = false. This is tested by just
     * calling this route.
     *
     * @throws Exception
     */
    public function testCancelEdit(): void
    {
        // This should show input fields for the personal data fields
        $crawler = $this->getMyClient()->request('GET', '/member/cancel_personal_data/'.$this->member1->getId());
        $this->assertTrue($crawler->filter('div:contains("Dipl.-Ing.")')->count() > 1);
        $this->assertTrue($crawler->filter('div:contains("Male")')->count() > 1);
        $this->assertEquals(1, $crawler->filter('label:contains("Password")')->count());
        $this->assertEquals(1, $crawler->filter('div.col-sm-10:contains("-----")')->count());
    }


    /**
     * Test calling the edit page for a user which is not allowed to do this
     *
     * @throws Exception
     */
    public function testPersonalDataWithoutAccessRight(): void
    {
        $this->getMyClient('firstname2.lastname2');

        $crawler = $this->client->request('GET', '/member/edit_personal_data/'.$this->member1->getId());

        $this->assertEquals(1, $crawler->filter('title:contains("You are not allowed to edit other users data! (403 Forbidden)")')->count());
    }


    /**
     * Test if the user password can be changed
     *
     * @throws Exception
     */
    public function testChangePassword(): void
    {
        $this->clearAllLogFiles();

        // Submit form with both identical password entries. This should change the members password.
        $crawler = $this->getMyClient()->request('GET', '/member/edit_personal_data/'.$this->member1->getId());
        $buttonCrawlerNode = $crawler->selectButton('Save');
        $form = $buttonCrawlerNode->form();
        $newPassword = 'My secret password';
        $form['member_entry_personal_data[plainPassword][first]'] = $newPassword;
        $form['member_entry_personal_data[plainPassword][second]'] = $newPassword;
        $crawler = $this->client->submit($form);
        $this->assertEquals(1, $crawler->filter('div.col-sm-10:contains("-----")')->count());
        // Refresh the member (refresh() doesn't work because of different entity managers)
        /** @var MemberEntry $member */
        $member = $this->client->getContainer()->get('doctrine.orm.entity_manager')->getRepository(MemberEntry::class)->find($this->member1->getId());

        /** @var PasswordHasherFactory $hasherService */
        $hasherService = $this->getContainer()->get('security.password_hasher_factory');
        $hasher = $hasherService->getPasswordHasher($member);
        $this->assertTrue($hasher->verify($member->getPassword(), $newPassword, $member->getSalt()));

        $logMessages = $this->getAllLogs();
        $this->assertEquals(1, count($logMessages));
        $this->assertEquals('Updated password', $logMessages[0]->getLogEntry());
    }


    /**
     * Test if changing the password with incorrect values returns the expected error messages
     *
     * @throws Exception
     */
    public function testChangePasswordWithErrors(): void
    {
        $oldPassword = $this->member1->getPassword();

        // Submit form without password entries. This should not change the members password.
        $crawler = $this->getMyClient()->request('GET', '/member/edit_personal_data/'.$this->member1->getId());
        $buttonCrawlerNode = $crawler->selectButton('Save');
        $form = $buttonCrawlerNode->form();
        $form['member_entry_personal_data[plainPassword][first]'] = "";
        $form['member_entry_personal_data[plainPassword][second]'] = "";
        $this->client->submit($form);
        $this->assertEquals($oldPassword, $this->member1->getPassword());

        // Submit form with only one password entry (first). This should not change the members password and show an
        // error.
        $password1 = 'test';
        $password2 = '';
        $errorMessage = 'The password fields must match';
        $this->submitAndCheckPasswordChangeWithError($this->member1, $password1, $password2, $errorMessage);

        // Submit form with only one password entry (second). This should not change the members password and show an
        // error.
        $password1 = '';
        $password2 = 'test';
        $errorMessage = 'The password fields must match';
        $this->submitAndCheckPasswordChangeWithError($this->member1, $password1, $password2, $errorMessage);

        // Submit two identical passwords which are too short. This should not change the members password and show an
        // error.
        $password1 = 'test';
        $password2 = 'test';
        $errorMessage = 'This value is too short. It should have 10 characters or more.';
        $this->submitAndCheckPasswordChangeWithError($this->member1, $password1, $password2, $errorMessage);

        // Submit less than 10 unicode characters. This should not change the members password and show an error.
        $password1 = 'öäüßöäüß';
        $password2 = 'öäüßöäüß';
        $errorMessage = 'This value is too short. It should have 10 characters or more.';
        $this->submitAndCheckPasswordChangeWithError($this->member1, $password1, $password2, $errorMessage);

        // Submit less than 4 different characters. This should not change the members password and show an error.
        $password1 = 'aaaabbbbcc';
        $password2 = 'aaaabbbbcc';
        $errorMessage = 'This value is not complex enough. It should have 4 different characters or more.';
        $this->submitAndCheckPasswordChangeWithError($this->member1, $password1, $password2, $errorMessage);

        // Submit utf8 characters to check that the number of unique chars is calculated correctly
        $password1 = 'ööööääääüü';
        $password2 = 'ööööääääüü';
        $errorMessage = 'This value is not complex enough. It should have 4 different characters or more.';
        $this->submitAndCheckPasswordChangeWithError($this->member1, $password1, $password2, $errorMessage);


        // Submit a password which contains the user name. This should not change the members password and show an error.
        $password1 = 'abFirstname1.lastname1ab';
        $password2 = 'abFirstname1.lastname1ab';
        $errorMessage = 'This value should not contain the user name.';
        $this->submitAndCheckPasswordChangeWithError($this->member1, $password1, $password2, $errorMessage);

        // Submit a password which contains the users email address. This should not change the members password and
        // show an error.
        $password1 = 'abFirstname1@email.comab';
        $password2 = 'abFirstname1@email.comab';
        $errorMessage = 'This value should not contain the users email address.';
        $this->submitAndCheckPasswordChangeWithError($this->member1, $password1, $password2, $errorMessage);

        // Submit a password which contains the users first name. This should not change the members password and show
        // an error.
        $password1 = 'abFirstname1ab';
        $password2 = 'abFirstname1ab';
        $errorMessage = 'This value should not contain the users first name.';
        $this->submitAndCheckPasswordChangeWithError($this->member1, $password1, $password2, $errorMessage);

        // Submit a password which contains the users last name. This should not change the members password and show
        // an error.
        $password1 = 'abLastname1ab';
        $password2 = 'abLastname1ab';
        $errorMessage = 'This value should not contain the users last name.';
        $this->submitAndCheckPasswordChangeWithError($this->member1, $password1, $password2, $errorMessage);
    }


    /**
     * Test if removing the first name changes it to dash before saving
     *
     * @throws Exception
     */
    public function testRemoveFirstName(): void
    {
        /** @var MemberEntry $member2 */
        $member2 = $this->getFixtureReference('member_entry_last_name2');

        // Submit form without password entries. This should not change the members password.
        $crawler = $this->getMyClient()->request('GET', '/member/edit_personal_data/'.$member2->getId());
        $buttonCrawlerNode = $crawler->selectButton('Save');
        $form = $buttonCrawlerNode->form();
        $form['member_entry_personal_data[firstName]'] = "";
        $this->client->submit($form);
        // Refresh the member2 (refresh() doesn't work because of different entity managers)
        $member2 = $this->getEntityManager()->getRepository(MemberEntry::class)->find($member2->getId());
        $this->assertEquals('-', $member2->getFirstName());
    }


    /**
     * @param MemberEntry $member
     * @param string      $password1
     * @param string      $password2
     * @param string      $errorMessage
     *
     * @throws Exception
     */
    private function submitAndCheckPasswordChangeWithError(MemberEntry $member, string $password1, string $password2, string $errorMessage): void
    {
        $crawler = $this->getMyClient()->request('GET', '/member/edit_personal_data/'.$member->getId());
        $buttonCrawlerNode = $crawler->selectButton('Save');
        $form = $buttonCrawlerNode->form();

        // Set passwords to both input fields
        $form['member_entry_personal_data[plainPassword][first]'] = $password1;
        $form['member_entry_personal_data[plainPassword][second]'] = $password2;

        // Store the current password hash to compare the new password with this.
        $oldPassword = $member->getPassword();
        $crawler = $this->client->submit($form);

        // Check if the correct error message is shown
        $this->assertEquals(1, $crawler->filter('span.form-error-message:contains("'.$errorMessage.'")')->count(), "Error message not found: $errorMessage");
        // Check that the password hash has not changed
        $this->assertEquals($oldPassword, $member->getPassword());
    }
}
