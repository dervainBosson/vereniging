<?php

/*
 * Copyright (C) 2022 by Luc Hamers <vereniging@hamers.de>
 *
 * This file is part of Vereniging.
 *
 * Vereniging is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Vereniging is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Vereniging.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Tests\Controller\Member;

use App\Entity\AddressType;
use App\Entity\Country;
use App\Entity\LogfileEntry;
use App\Entity\MemberEntry;
use App\Tests\WebTestCase;
use Exception;
use Symfony\Component\Mime\RawMessage;

/**
 * Class MemberContactDataControllerTest
 */
class MemberContactDataControllerTest extends WebTestCase
{
    private ?MemberEntry $member1;


    /**
     * @inheritDoc
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->loadAllFixtures();
        $this->member1 = $this->getFixtureReference('member_entry_last_name1');

        LogfileEntry::prepareTranslation($this->getEntityManager(), $this->getTranslator(), 'en');
    }


    /**
     * Test if clicking the edit button changed the output to a form.
     */
    public function testShowEdit(): void
    {
        // This should show input fields for the personal data fields
        $crawler = $this->client->request('GET', '/member/edit_contact_data/'.$this->member1->getId());

        // Only personal data title should appear
        $this->assertTrue($crawler->filter('h6.card-header:contains("Personal data")')->count() === 0);
        $this->assertTrue($crawler->filter('h6.card-header:contains("Contact data")')->count() > 0);
        $this->assertTrue($crawler->filter('h6.card-header:contains("Company data")')->count() === 0);
        $this->assertTrue($crawler->filter('h6.card-header:contains("Club data")')->count() === 0);

        // There should be two radio fields for the gender, the male field should be selected.
        $this->assertEquals(1, $crawler->filter('input[name="member_entry_contact_data_form[email]"]')->count());
    }


    /**
     * There was a problem with translations of the page (issue #68) when the current user and the user to which email
     * notifications had to be send, had different preferred languages. This generated the following exception:
     * Error: Method App\Entity\AddressType::__toString() must return a string value
     * in LogEntryDataField.php:124 ("return (string) $value")
     * The second problem (issue #69) showed wrong translations after saving a new address also when the notifications
     * used a different language.
     *
     * To prevent regressions, this test case will check for this.
     */
    public function testAddAddressWithTranslation(): void
    {
        $this->clearAllLogFiles();
        $member3 = $this->getFixtureReference('member_entry_last_name3');
        $member3->addEmailNotification($this->getFixtureReference('change_type_member_data_change'));
        $this->getEntityManager()->flush();

        // This should show input fields for the personal data fields
        $member2 = $this->getFixtureReference('member_entry_last_name2');
        $crawler = $this->client->request('GET', '/member/edit_contact_data/'.$member2->getId(), ['_locale' => 'de']);
        $buttonCrawlerNode = $crawler->selectButton('Speichern');

        /** @var AddressType $addressTypeHome */
        $addressTypeHome = $this->getFixtureReference('address_type_private');
        /** @var Country $country */
        $country = $this->getFixtureReference('country_germany');
        $form = $buttonCrawlerNode->form();
        $data = $form->getPhpValues();
        $data["member_entry_contact_data_form"]["member_address_maps"][] = [
            'address' => [
                "addressType" => $addressTypeHome->getId(),
                "address" => "street",
                "zip" => "12345",
                "city" => "test city",
                "country" => $country->getId(),
            ],
        ];
        $this->clearAllLogFiles();

        // Before issue #68 was fixed, this created a crash.
        $crawler = $this->client->request('POST', '/member/edit_contact_data/'.$member2->getId(), $data);

        // Before issue #69 was fixed, tha country was shown in Dutch, while the complete page is in German
        $this->assertCount(1, $crawler->filter('td:contains("Deutschland")'));
        $logMessages = $this->getAllLogs();
        $this->assertEquals(1, count($logMessages));
        $this->checkLogMessage($logMessages[0], 'Verandering ledengegevens', '{"numberOfDataFields":1,"mainMessage":"Created new %classname% entry %stringIdentifier%|address,","dataArray":{"address":["street"],"address type":[{"className":"App\\\\Entity\\\\AddressType","dbIndex":1,"defaultString":"Privatadresse","translate":true}],"zip":["12345"],"city":["test city"],"country":[{"className":"App\\\\Entity\\\\Country","dbIndex":2,"defaultString":"Deutschland","translate":true}],"is main address":[{"className":null,"dbIndex":null,"defaultString":"yes","translate":true}]}}', null, $member2->getId());
    }


    /**
     * Test if address and phone number form is shown as expected when they are emppty
     */
    public function testShowEditWithoutPhoneOrAddress(): void
    {
        // Select a member without addresses or phone numbers
        $member3 = $this->getFixtureReference('member_entry_last_name3');

        // This should show input fields for the personal data fields
        $crawler = $this->client->request('GET', '/member/edit_contact_data/'.$member3->getId());

        // When there are no phone numbers or addresses, the form elements have to set to rendered manually
        $this->assertTrue($crawler->filter('label.required:contains("Member address maps")')->count() === 0);
        $this->assertTrue($crawler->filter('label.required:contains("[0,1] Phone number")')->count() === 0);

        // There should be two radio fields for the gender, the male field should be selected.
        $this->assertEquals(1, $crawler->filter('input[name="member_entry_contact_data_form[email]"]')->count());
    }


    /**
     * Test if the email address is validated correctly
     */
    public function testSubmitFaultyEmailAddress(): void
    {
        // This should show input fields for the personal data fields
        $crawler = $this->client->request('GET', '/member/edit_contact_data/'.$this->member1->getId());
        $buttonCrawlerNode = $crawler->selectButton('Save');
        $form = $buttonCrawlerNode->form();
        // Change the email to see if the form returns an error when the email address is not valid
        $form['member_entry_contact_data_form[email]'] = "test";

        $crawler = $this->client->submit($form);

        $this->assertEquals(1, $crawler->filter('span.form-error-message:contains("This value is not a valid email address")')->count());
    }


    /**
     * Test if a change of the email address not only changes the address, but also changes the mailing lists
     * subscriptions.
     */
    public function testSubmitEditEmail(): void
    {
        $this->clearAllLogFiles();

        // This should show input fields for the contact data fields
        $crawler = $this->client->request('GET', '/member/edit_contact_data/'.$this->member1->getId());
        $buttonCrawlerNode = $crawler->selectButton('Save');
        $form = $buttonCrawlerNode->form();
        // Change the email to see if the form can be saved with changes
        $form['member_entry_contact_data_form[email]'] = "test@mail.com";

        $crawler = $this->client->submit($form);
        $this->assertTrue($crawler->filter('div:contains("test@mail.com")')->count() > 0);
        $this->assertEquals(0, $crawler->filter('div:contains("firstname1@email.com")')->count());

        // There should be 2 unsubscribes and 2 subscribes
        $this->assertEmailCount(4);
        $emails = $this->getMailerMessages();
        $this->checkEmailMessage(1, 'unsubscribe', 'firstname1@email.com', $emails[0]);
        $this->checkEmailMessage(3, 'unsubscribe', 'firstname1@email.com', $emails[1]);
        $this->checkEmailMessage(1, 'subscribe', 'test@mail.com', $emails[2]);
        $this->checkEmailMessage(3, 'subscribe', 'test@mail.com', $emails[3]);

        $logMessages = $this->getAllLogs();
        $this->assertEquals(5, count($logMessages));
        $this->checkLogMessage($logMessages[0], 'Member data change', '{"numberOfDataFields":2,"mainMessage":"Updated %classname% entry %stringIdentifier%|memberentry,","dataArray":{"email":["firstname1@email.com","test@mail.com"]}}');
        $this->checkLogMessage($logMessages[1], 'Member settings change', 'Subscribed test@mail.com to mailing list MailingList_3');
        $this->checkLogMessage($logMessages[2], 'Member settings change', 'Subscribed test@mail.com to mailing list MailingList_1');
        $this->checkLogMessage($logMessages[3], 'Member settings change', 'Unsubscribed firstname1@email.com from mailing list MailingList_3');
        $this->checkLogMessage($logMessages[4], 'Member settings change', 'Unsubscribed firstname1@email.com from mailing list MailingList_1');
    }


    /**
     * Test that Javascript tags are not executed
     */
    public function testSubmitJavascriptTags(): void
    {
        $crawler = $this->client->request('GET', '/member/edit_contact_data/'.$this->member1->getId());
        $buttonCrawlerNode = $crawler->selectButton('Save');
        $form = $buttonCrawlerNode->form();
        $form['member_entry_contact_data_form[email]'] = "test@mail.com<script>";
        $crawler = $this->client->submit($form);

        // < and > have to be replaced by &lt; and &gt;
        $this->assertEquals(1, substr_count($crawler->html(), '&lt;script&gt;'));
    }


    /**
     * Test if an exception thrown when updating the email list subscriptions is handled correctly. To do this, it
     * doesn't matter, if the majordomo or mailman updater is used. For convenience, we create exceptions with the
     * majordomo implementation here, by giving it a broken email address.
     */
    public function testSubmitEditEmailWithExceptions(): void
    {
        $this->setLoggedInUserForLogging();
        $mailingList = $this->getFixtureReference('mailinglist1');
        // In the default fixtures, the domain address is an email, This is changed here for this test.
        $mailingList->setManagementEmail('bla');
        $this->getEntityManager()->flush();
        $this->setLoggedInUserForLogging(true);

        $this->clearAllLogFiles();

        // This should show input fields for the personal data fields
        $crawler = $this->client->request('GET', '/member/edit_contact_data/'.$this->member1->getId());
        $buttonCrawlerNode = $crawler->selectButton('Save');
        $form = $buttonCrawlerNode->form();
        // Change the email to see if the form can be saved with changes
        $form['member_entry_contact_data_form[email]'] = "test@mail.com";

        $crawler = $this->client->submit($form);
        $this->assertTrue($crawler->filter('div:contains("test@mail.com")')->count() > 0);
        $this->assertEquals(0, $crawler->filter('div:contains("firstname1@email.com")')->count());

        // The reaction to an exception should be to log it as an error type.
        $logMessages = $this->getAllLogs();
        $this->assertEquals(7, count($logMessages));
        $this->checkLogMessage($logMessages[0], 'Member data change', '{"numberOfDataFields":2,"mainMessage":"Updated %classname% entry %stringIdentifier%|memberentry,","dataArray":{"email":["firstname1@email.com","test@mail.com"]}}');
        $this->checkLogMessage($logMessages[1], 'Member settings change', 'Subscribed test@mail.com to mailing list MailingList_3');
        $this->checkLogMessage($logMessages[2], 'Member settings change', 'Subscribed test@mail.com to mailing list MailingList_1');
        $this->checkLogMessage($logMessages[3], 'Error', 'Email "bla" does not comply with addr-spec of RFC 2822.');
        $this->checkLogMessage($logMessages[4], 'Member settings change', 'Unsubscribed firstname1@email.com from mailing list MailingList_3');
        $this->checkLogMessage($logMessages[5], 'Member settings change', 'Unsubscribed firstname1@email.com from mailing list MailingList_1');
        $this->checkLogMessage($logMessages[6], 'Error', 'Email "bla" does not comply with addr-spec of RFC 2822.');
    }


    /**
     * Test if a change of the email list selections updates the mailing lists subscriptions.
     */
    public function testEditMailingListSubscriptions(): void
    {
        $this->clearAllLogFiles();

        // This should show input fields for the personal data fields
        $crawler = $this->client->request('GET', '/member/edit_contact_data/'.$this->member1->getId());
        $buttonCrawlerNode = $crawler->selectButton('Save');
        $form = $buttonCrawlerNode->form();
        // Change the email to see if the form can be saved with changes
        $form['member_entry_contact_data_form[mailingLists][0]']->untick();
        $form['member_entry_contact_data_form[mailingLists][1]']->tick();
        $this->client->submit($form);

        // There should be 1 unsubscribe and 1 subscribe
        $this->assertEmailCount(2);
        $emails = $this->getMailerMessages();
        $this->checkEmailMessage(1, 'unsubscribe', 'firstname1@email.com', $emails[0]);
        $this->checkEmailMessage(2, 'subscribe', 'firstname1@email.com', $emails[1]);

        $logMessages = $this->getAllLogs();
        $this->assertEquals(2, count($logMessages));
        $this->checkLogMessage($logMessages[0], 'Member settings change', 'Subscribed firstname1@email.com to mailing list MailingList_2');
        $this->checkLogMessage($logMessages[1], 'Member settings change', 'Unsubscribed firstname1@email.com from mailing list MailingList_1');
    }


    /**
     * Test if a change clearing the email address changes the mailing lists subscriptions.
     */
    public function testClearEmailAddressMailingListSubscriptions(): void
    {
        $this->clearAllLogFiles();

        // This should show input fields for the personal data fields
        $crawler = $this->client->request('GET', '/member/edit_contact_data/'.$this->member1->getId());
        $buttonCrawlerNode = $crawler->selectButton('Save');
        $form = $buttonCrawlerNode->form();
        // Change the email to see if the form can be saved with changes
        $form['member_entry_contact_data_form[email]'] = '';
        $this->client->submit($form);

        // There should be 2 unsubscribe
        $this->assertEmailCount(2);
        $emails = $this->getMailerMessages();
        $this->checkEmailMessage(1, 'unsubscribe', 'firstname1@email.com', $emails[0]);
        $this->checkEmailMessage(3, 'unsubscribe', 'firstname1@email.com', $emails[1]);

        $logMessages = $this->getAllLogs();
        $this->assertEquals(3, count($logMessages));
        $this->checkLogMessage($logMessages[0], 'Member data change', 'Updated entry of type member ', ["email" => ["firstname1@email.com", null]]);
        $this->checkLogMessage($logMessages[1], 'Member settings change', 'Unsubscribed firstname1@email.com from mailing list MailingList_3');
        $this->checkLogMessage($logMessages[2], 'Member settings change', 'Unsubscribed firstname1@email.com from mailing list MailingList_1');
    }


    /**
     * Test if adding or removing a subscription for a mailing list is ignored, when no email address is present
     */
    public function testAddMailingListSubscriptionWithoutEmail(): void
    {
        // First remove the email address.
        $crawler = $this->client->request('GET', '/member/edit_contact_data/'.$this->member1->getId());
        $buttonCrawlerNode = $crawler->selectButton('Save');
        $form = $buttonCrawlerNode->form();
        // Change the email to see if the form can be saved with changes
        $form['member_entry_contact_data_form[email]'] = '';
        $this->client->submit($form);

        $this->clearAllLogFiles();

        // This should show input fields for the personal data fields
        $crawler = $this->client->request('GET', '/member/edit_contact_data/'.$this->member1->getId());
        $buttonCrawlerNode = $crawler->selectButton('Save');
        $form = $buttonCrawlerNode->form();
        $form['member_entry_contact_data_form[mailingLists][0]']->untick();
        $form['member_entry_contact_data_form[mailingLists][1]']->tick();

        $this->client->submit($form);
        $this->assertEmailCount(0);
        $this->assertEquals(0, count($this->getAllLogs()));
    }


    /**
     * Test if adding an email address for the first time (i.e. the address was empty before) updates the mailing lists
     * subscriptions.
     */
    public function testSetEmailAddressMailingListSubscriptions(): void
    {
        // First remove the email address.
        $crawler = $this->client->request('GET', '/member/edit_contact_data/'.$this->member1->getId());
        $buttonCrawlerNode = $crawler->selectButton('Save');
        $form = $buttonCrawlerNode->form();
        // Change the email to see if the form can be saved with changes
        $form['member_entry_contact_data_form[email]'] = '';
        $this->client->submit($form);

        $this->clearAllLogFiles();

        // This should show input fields for the personal data fields
        $crawler = $this->client->request('GET', '/member/edit_contact_data/'.$this->member1->getId());
        $buttonCrawlerNode = $crawler->selectButton('Save');
        $form = $buttonCrawlerNode->form();
        // Change the email to see if the form can be saved with changes
        $form['member_entry_contact_data_form[email]'] = 'test@mail.com';
        $this->client->submit($form);

        // There should be 2 subscribe
        $this->assertEmailCount(2);
        $emails = $this->getMailerMessages();
        $this->checkEmailMessage(1, 'subscribe', 'test@mail.com', $emails[0]);
        $this->checkEmailMessage(3, 'subscribe', 'test@mail.com', $emails[1]);

        $logMessages = $this->getAllLogs();
        $this->assertEquals(3, count($logMessages));
        $this->checkLogMessage($logMessages[0], 'Member data change', 'Updated entry of type member ', ["email" => [null, "test@mail.com"]]);
        $this->checkLogMessage($logMessages[1], 'Member settings change', 'Subscribed test@mail.com to mailing list MailingList_3');
        $this->checkLogMessage($logMessages[2], 'Member settings change', 'Subscribed test@mail.com to mailing list MailingList_1');
    }


    /**
     * Test if changes in the form are written to the database and then shown correctly.
     */
    public function testSubmitEditPhoneNumber(): void
    {
        $this->clearAllLogFiles();

        // This should show input fields for the personal data fields
        $crawler = $this->client->request('GET', '/member/edit_contact_data/'.$this->member1->getId());
        $buttonCrawlerNode = $crawler->selectButton('Save');
        $form = $buttonCrawlerNode->form();
        // Change the first phone to see if the form can be saved with changes
        $form['member_entry_contact_data_form[phone_numbers][0][phoneNumber]'] = "123";
        $form['member_entry_contact_data_form[phone_numbers][1][phoneNumber]'] = "";

        $crawler = $this->client->submit($form);
        // Check the content of the phone numbers
        $this->assertEquals(0, $crawler->filter('td:contains("343434")')->count());
        $this->assertEquals(0, $crawler->filter('td:contains("9876543210")')->count());
        $this->assertEquals(1, $crawler->filter('td:contains("123")')->count());
        $this->assertEquals(1, $crawler->filter('td:contains("121212")')->count());
        $this->assertEquals(1, $crawler->filter('td:contains("2876543210")')->count());
        // Check the phone number and address types
        $this->assertEquals(2, $crawler->filter('td:contains("Company")')->count());
        $this->assertEquals(2, $crawler->filter('td:contains("Home")')->count());
        $this->assertEquals(1, $crawler->filter('td:contains("Mobile")')->count());
        $this->assertEquals(0, $crawler->filter('td:contains("Company mobile")')->count());

        $phoneNumbers = $this->getPhoneNumbersAsArray($this->member1);
        foreach ($phoneNumbers as $phoneNumber) {
            // Check that the home number has been changed
            if ($phoneNumber['type'] === 'Home') {
                $this->assertEquals("123", $phoneNumber['phone_number']);
            }
        }

        $logMessages = $this->getAllLogs();
        $this->assertEquals(2, count($logMessages));
        $this->checkLogMessage($logMessages[0], 'Member data change', 'Deleted entry of type phonenumber ', ["phone number" => ["9876543210"], "phone number type" => ["Company"]]);
        $this->checkLogMessage($logMessages[1], 'Member data change', 'Updated entry of type phonenumber ', ["phone number" => ["0123456789", "123"]]);
    }


    /**
     * Test editing addresses
     */
    public function testSubmitEditAddresses(): void
    {
        $this->clearAllLogFiles();

        // This should show input fields for the personal data fields
        $crawler = $this->client->request('GET', '/member/edit_contact_data/'.$this->member1->getId());
        $buttonCrawlerNode = $crawler->selectButton('Save');
        $form = $buttonCrawlerNode->form();
        // Change the first address and remove the second address to see if the form can be saved with changes
        $form['member_entry_contact_data_form[member_address_maps][0][address][address]'] = "new address";
        $crawler = $this->client->submit($form);

        // Check the content of address 1
        $this->assertEquals(1, $crawler->filter('td:contains("Main address")')->count());
        $this->assertEquals(1, $crawler->filter('td:contains("Home address")')->count());
        $this->assertEquals(0, $crawler->filter('td:contains("MyAddress1")')->count());
        $this->assertEquals(1, $crawler->filter('td:contains("new address")')->count());
        $this->assertEquals(1, $crawler->filter('td:contains("MyZip1")')->count());
        $this->assertEquals(1, $crawler->filter('td:contains("MyCity1")')->count());
        $this->assertEquals(1, $crawler->filter('td:contains("Germany")')->count());

        // Check the content of address 2
        $this->assertEquals(1, $crawler->filter('td:contains("Company address")')->count());
        $this->assertEquals(1, $crawler->filter('td:contains("MyAddress2")')->count());
        $this->assertEquals(1, $crawler->filter('td:contains("MyZip2")')->count());
        $this->assertEquals(1, $crawler->filter('td:contains("MyCity2")')->count());
        $this->assertEquals(1, $crawler->filter('td:contains("Netherlands")')->count());

        // Check log messages
        $logMessages = $this->getAllLogs();
        $this->assertEquals(1, count($logMessages));
        $this->checkLogMessage($logMessages[0], 'Member data change', 'Updated entry of type address ', ["address" => ["MyAddress1", "new address"]]);
    }


    /**
     * Test removing the additional address
     */
    public function testSubmitRemoveAdditionalAddress(): void
    {
        $this->clearAllLogFiles();

        // This should show input fields for the personal data fields
        $crawler = $this->client->request('GET', '/member/edit_contact_data/'.$this->member1->getId());
        $buttonCrawlerNode = $crawler->selectButton('Save');
        $form = $buttonCrawlerNode->form();
        // Remove the additional address
        unset($form['member_entry_contact_data_form[member_address_maps][1]']);
        $crawler = $this->client->submit($form);

        // Check the content of address 1
        $this->assertEquals(1, $crawler->filter('td:contains("Main address")')->count());
        $this->assertEquals(1, $crawler->filter('td:contains("Home address")')->count());
        $this->assertEquals(1, $crawler->filter('td:contains("MyAddress1")')->count());
        $this->assertEquals(1, $crawler->filter('td:contains("MyZip1")')->count());
        $this->assertEquals(1, $crawler->filter('td:contains("MyCity1")')->count());
        $this->assertEquals(1, $crawler->filter('td:contains("Germany")')->count());

        // Check the content of address 2
        $this->assertEquals(0, $crawler->filter('td:contains("Company address")')->count());
        $this->assertEquals(0, $crawler->filter('td:contains("MyAddress2")')->count());
        $this->assertEquals(0, $crawler->filter('td:contains("MyZip2")')->count());
        $this->assertEquals(0, $crawler->filter('td:contains("MyCity2")')->count());

        // Check log messages
        $logMessages = $this->getAllLogs();
        $this->assertEquals(1, count($logMessages));
        $this->checkLogMessage($logMessages[0], 'Member data change', 'Deleted entry of type address ', ["address" => ["MyAddress2"], "address type" => ["Company address"], "zip" => ["MyZip2"], "city" => ["MyCity2"], "country" => ["Netherlands"], "is main address" => ["no"]]);
    }


    /**
     * Test removing the main address
     */
    public function testSubmitRemoveMainAddress(): void
    {
        $this->clearAllLogFiles();

        // This should show input fields for the personal data fields
        $crawler = $this->client->request('GET', '/member/edit_contact_data/'.$this->member1->getId());
        $buttonCrawlerNode = $crawler->selectButton('Save');
        $form = $buttonCrawlerNode->form();
        // Remove the additional address
        unset($form['member_entry_contact_data_form[member_address_maps][0]']);
        $crawler = $this->client->submit($form);

        // Check the content of address 1
        $this->assertEquals(0, $crawler->filter('td:contains("Home address")')->count());
        $this->assertEquals(0, $crawler->filter('td:contains("new address")')->count());
        $this->assertEquals(0, $crawler->filter('td:contains("MyZip1")')->count());
        $this->assertEquals(0, $crawler->filter('td:contains("MyCity1")')->count());

        // Check the content of address 2
        $this->assertEquals(1, $crawler->filter('td:contains("Main address")')->count());
        $this->assertEquals(1, $crawler->filter('td:contains("MyAddress2")')->count());
        $this->assertEquals(1, $crawler->filter('td:contains("MyZip2")')->count());
        $this->assertEquals(1, $crawler->filter('td:contains("MyCity2")')->count());
        $this->assertEquals(1, $crawler->filter('td:contains("Netherlands")')->count());
        $this->assertEquals(1, $crawler->filter('td:contains("Company address")')->count());

        // Check log messages
        $logMessages = $this->getAllLogs();
        $this->assertEquals(1, count($logMessages));
        $this->checkLogMessage($logMessages[0], 'Member data change', 'Deleted entry of type address ', ["address" => ["MyAddress1"], "address type" => ["Home address"], "zip" => ["MyZip1"], "city" => ["MyCity1"], "country" => ["Germany"], "is main address" => ["yes"]]);
    }


    /**
     * Test removing both addresses: there should be a way to add new addresses.
     */
    public function testSubmitRemoveBothAddresses(): void
    {
        // This should show input fields for the personal data fields
        $crawler = $this->client->request('GET', '/member/edit_contact_data/'.$this->member1->getId());
        $buttonCrawlerNode = $crawler->selectButton('Save');
        $form = $buttonCrawlerNode->form();
        // Remove the additional address
        unset($form['member_entry_contact_data_form[member_address_maps][0]']);
        unset($form['member_entry_contact_data_form[member_address_maps][1]']);
        $crawler = $this->client->submit($form);

        // Check that both addresses have been removed
        $this->assertEquals(0, $crawler->filter('td:contains("MyAddress1")')->count());
        $this->assertEquals(0, $crawler->filter('td:contains("MyAddress2")')->count());

        $crawler = $this->client->request('GET', '/member/edit_contact_data/'.$this->member1->getId());
        $this->assertEquals(1, $crawler->filter('a:contains("Add another address")')->count());
    }


    /**
     * Test if editing the contact data without the right to edit mailing list settings is shown correctly. The input
     * fields should be hidden and only the subscribed  mailing lists should be shown.
     *
     * @throws Exception
     */
    public function testMailingListSettingsWithoutEditRights(): void
    {
        // First subscribe member2 to mailinglist 2, so there is one subscription to test
        $member2 = $this->getFixtureReference('member_entry_last_name2');
        $mailingList = $this->getFixtureReference('mailinglist2');
        $mailingList->addMemberEntry($member2);
        $this->getEntityManager()->flush();

        $this->getMyClient('firstname2.lastname2');

        $crawler = $this->client->request('GET', '/member/edit_contact_data/'.$member2->getId());
        $this->assertEquals(0, $crawler->filter('label:contains("MailingList_1")')->count());
        $this->assertEquals(1, $crawler->filter('label:contains("MailingList_2")')->count());
        $this->assertEquals(0, $crawler->filter('label:contains("MailingList_3")')->count());
        // The checkbox is hidden by style sheet.
        $this->assertEquals(1, $crawler->filter('input.hidden')->count());
    }


    /**
     * Clicking the cancel button would just render the contact data section with edit = false. This is tested by just
     * calling this route.
     */
    public function testCancelEdit(): void
    {
        // This should show input fields for the personal data fields
        $crawler = $this->client->request('GET', '/member/edit_contact_data/'.$this->member1->getId());
        $buttonCrawlerNode = $crawler->selectButton('Save');
        $form = $buttonCrawlerNode->form();
        // Change the email to see if cancelling the form really doesn't change anything
        $form['member_entry_contact_data_form[email]'] = "test@mail.com";

        $crawler = $this->client->request('GET', '/member/cancel_contact_data/'.$this->member1->getId());
        $this->assertTrue($crawler->filter('div:contains("firstname1@email.com")')->count() > 0);
    }


    /**
     * Test calling the edit page for a user which is not allowed to do this
     *
     * @throws Exception
     */
    public function testContactDataWithoutAccessRight(): void
    {
        $this->getMyClient('firstname2.lastname2');

        $crawler = $this->client->request('GET', '/member/edit_contact_data/'.$this->member1->getId());

        $this->assertEquals(1, $crawler->filter('title:contains("You are not allowed to edit other users data! (403 Forbidden)")')->count());
    }


    /**
     * Refresh the member entry (reload all data from the database) and return the phone numbers as array.
     *
     * @param MemberEntry $memberEntry Member entry from which the phone data has to be read.
     *
     * @return array
     */
    private function getPhoneNumbersAsArray(MemberEntry $memberEntry): array
    {
        // Reread the member entry from the database
        $memberEntry = $this->getEntityManager()->getRepository(MemberEntry::class)->find($memberEntry->getId());

        return ($memberEntry->getPhoneNumbersAsArray());
    }


    /**
     * @param string     $list
     * @param string     $action
     * @param string     $address
     * @param RawMessage $message
     */
    private function checkEmailMessage(string $list, string $action, string $address, RawMessage $message): void
    {
        $this->assertEquals("Update mailinglist \"MailingList_$list\"", $message->getSubject());
        $this->assertEquals('vereninging@example.com', $message->getFrom()[0]->getAddress());
        $this->assertEquals("mailinglist$list@mail.org", $message->getTo()[0]->getAddress());
        $this->assertEquals("approve password$list $action MailingList_$list $address", $message->getTextBody());
    }


    /**
     * Check the content of a log message
     *
     * @param LogfileEntry $logMessage   Log message to check
     * @param string       $changeType   Expected change type
     * @param string       $message      Expected string content
     * @param array|null   $expectedData Complex data array to compare to
     * @param int|null     $userId
     */
    private function checkLogMessage(LogfileEntry $logMessage, string $changeType, string $message, array $expectedData = null, ?int $userId = null): void
    {
        $this->assertEquals($changeType, $logMessage->getChangeType()->getChangeType());
        if ('Error' !== $changeType) {
            $userId = $userId ?? $this->member1->getId();
            $this->assertEquals($userId, $logMessage->getChangesOnMember()->getId());
        }

        if (is_null($expectedData)) {
            $this->assertEquals($message, $logMessage->getLogEntry());
        } else {
            $complexLogEntry = $logMessage->getComplexLogEntry();
            $this->assertEquals($message, $complexLogEntry->getMainMessage());
            $this->assertEquals($expectedData, $complexLogEntry->getDataAsArray());
        }
    }
}
