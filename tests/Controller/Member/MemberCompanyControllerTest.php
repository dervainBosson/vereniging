<?php

/*
 * Copyright (C) 2022 by Luc Hamers <vereniging@hamers.de>
 *
 * This file is part of Vereniging.
 *
 * Vereniging is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Vereniging is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Vereniging.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Tests\Controller\Member;

use App\Entity\CompanyInformation;
use App\Entity\MemberEntry;
use App\Tests\WebTestCase;
use Exception;

/**
 * Class MemberCompanyControllerTest
 */
class MemberCompanyControllerTest extends WebTestCase
{
    private ?MemberEntry $member1;


    /**
     * @inheritDoc
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->loadAllFixtures();
        $this->member1 = $this->getFixtureReference('member_entry_last_name1');
    }


    /**
     * Test if clicking the edit button changed the output to a form.
     */
    public function testShowEdit(): void
    {
        $crawler = $this->client->request('GET', '/member/edit_company_data/'.$this->member1->getId());

        // Only company data title should appear
        $this->assertEquals(0, $crawler->filter('h6.card-header:contains("Personal data")')->count());
        $this->assertEquals(0, $crawler->filter('h6.card-header:contains("Contact data")')->count());
        $this->assertEquals(1, $crawler->filter('h6.card-header:contains("Company data")')->count());
        $this->assertEquals(0, $crawler->filter('h6.card-header:contains("Club data")')->count());

        // There should be input text fields for name, city and url
        $this->assertEquals(1, $crawler->filter('input#member_entry_company_name[value="My Company"]')->count());
        $this->assertEquals(1, $crawler->filter('input#member_entry_company_city[value="My city"]')->count());
        $this->assertEquals(1, $crawler->filter('input#member_entry_company_url[value="http://company.com"]')->count());

        // There should be input text fields for name, city and url
        $this->assertEquals(1, $crawler->filter('textarea#member_entry_company_description:contains("My description")')->count());
        $this->assertEquals(1, $crawler->filter('textarea#member_entry_company_functionDescription:contains("My function")')->count());

        // Check if both buttons are available
        $this->assertEquals(1, $crawler->filter('button#save_company_data[type="submit"]:contains("Save")')->count());
        $this->assertEquals(1, $crawler->filter('button#reset_company_data[type="reset"]:contains("Cancel")')->count());
    }


    /**
     * Test if changes in the form are written to the database and then shown correctly.
     */
    public function testSubmitEdit(): void
    {
        // This should show input fields for the personal data fields
        $crawler = $this->client->request('GET', '/member/edit_company_data/'.$this->member1->getId());
        $buttonCrawlerNode = $crawler->selectButton('Save');
        $form = $buttonCrawlerNode->form();
        // Change all options to see if the form can be saved with changes
        $form['member_entry_company[name]'] = "new name";
        $form['member_entry_company[city]'] = "new city";
        $form['member_entry_company[description]'] = "new description";
        $form['member_entry_company[functionDescription]'] = "new function description";
        $form['member_entry_company[url]'] = "new url";
        $crawler = $this->client->submit($form);

        // Check if all the changed fields have been saved correctly
        $this->assertEquals(1, $crawler->filter('div.col-sm-10:contains("new name")')->count());
        $this->assertEquals(1, $crawler->filter('div.col-sm-10:contains("new city")')->count());
        $this->assertEquals(1, $crawler->filter('div.col-sm-10:contains("new description")')->count());
        $this->assertEquals(1, $crawler->filter('div.col-sm-10:contains("new function description")')->count());
        $this->assertEquals(1, $crawler->filter('div.col-sm-10:contains("new url")')->count());

        // Submit without company name
        $this->assertEquals(2, count($this->getEntityManager()->getRepository(CompanyInformation::class)->findAll()));
        $crawler = $this->client->request('GET', '/member/edit_company_data/'.$this->member1->getId());
        $buttonCrawlerNode = $crawler->selectButton('Save');
        $form = $buttonCrawlerNode->form();
        // Change all options to see if the form can be saved with changes
        $form['member_entry_company[name]'] = "";
        $form['member_entry_company[city]'] = "new city";
        $form['member_entry_company[description]'] = "new description";
        $form['member_entry_company[functionDescription]'] = "new function description";
        $form['member_entry_company[url]'] = "new url";
        $crawler = $this->client->submit($form);
        $this->assertEquals(0, $crawler->filter('div.col-sm-10:contains("new name")')->count());
        $this->assertEquals(0, $crawler->filter('div.col-sm-10:contains("new city")')->count());
        $this->assertEquals(0, $crawler->filter('div.col-sm-10:contains("new description")')->count());
        $this->assertEquals(0, $crawler->filter('div.col-sm-10:contains("new function description")')->count());
        $this->assertEquals(0, $crawler->filter('div.col-sm-10:contains("new url")')->count());
        // This should have deleted the company information entity in the database
        $this->assertEquals(1, count($this->getEntityManager()->getRepository(CompanyInformation::class)->findAll()));

        // Submit to a member without company information
        $crawler = $this->client->request('GET', '/member/edit_company_data/'.$this->member1->getId());
        $buttonCrawlerNode = $crawler->selectButton('Save');
        $form = $buttonCrawlerNode->form();
        // Change all options to see if the form can be saved with changes
        $form['member_entry_company[name]'] = "new name";
        $form['member_entry_company[city]'] = "new city";
        $crawler = $this->client->submit($form);
        $this->assertEquals(1, $crawler->filter('div.col-sm-10:contains("new name")')->count());
        $this->assertEquals(1, $crawler->filter('div.col-sm-10:contains("new city")')->count());
        // This should have inserted the company information entity in the database
        $this->assertEquals(2, count($this->getEntityManager()->getRepository(CompanyInformation::class)->findAll()));
    }


    /**
     * Test that Javascript tags are not executed
     */
    public function testSubmitJavascriptTags(): void
    {
        // This should show input fields for the personal data fields
        $crawler = $this->client->request('GET', '/member/edit_company_data/'.$this->member1->getId());
        $buttonCrawlerNode = $crawler->selectButton('Save');
        $form = $buttonCrawlerNode->form();
        // Change all options to see if the form can be saved with changes. Check if special characters like < and > are
        // replaced, without replacing characters with accents.
        $form['member_entry_company[name]'] = "My company<scriptäöé>";
        $form['member_entry_company[city]'] = "new city<scriptäöé>";
        $form['member_entry_company[description]'] = "new description<scriptäöé>";
        $form['member_entry_company[functionDescription]'] = "new function description<scriptäöé>";
        $form['member_entry_company[url]'] = "new url<scriptäöé>";
        $crawler = $this->client->submit($form);

        // < and > have to be replaced by &lt; and &gt;
        $this->assertEquals(5, substr_count($crawler->html(), '&lt;scriptäöé&gt;'));
    }


    /**
     * Test calling the edit page for a user which is not allowed to do this
     *
     * @throws Exception
     */
    public function testContactDataWithoutAccessRight(): void
    {
        $this->getMyClient('firstname2.lastname2');

        $crawler = $this->client->request('GET', '/member/edit_company_data/'.$this->member1->getId());

        $this->assertEquals(1, $crawler->filter('title:contains("You are not allowed to edit other users data! (403 Forbidden)")')->count());
    }


    /**
     * Clicking the cancel button would just render the personal data section with edit = false. This is tested by just
     * calling this route.
     */
    public function testCancelEdit(): void
    {
        // This should show input fields for the personal data fields
        $crawler = $this->client->request('GET', '/member/cancel_company_data/'.$this->member1->getId());
        $this->assertEquals(1, $crawler->filter('div.col-sm-10:contains("My Company")')->count());
        $this->assertEquals(1, $crawler->filter('div.col-sm-10:contains("My city")')->count());
        $this->assertEquals(1, $crawler->filter('div.col-sm-10:contains("My description")')->count());
        $this->assertEquals(1, $crawler->filter('div.col-sm-10:contains("My function")')->count());
        $this->assertEquals(1, $crawler->filter('div.col-sm-10:contains("http://company.com")')->count());
    }
}
