<?php

/*
 * Copyright (C) 2022 by Luc Hamers <vereniging@hamers.de>
 *
 * This file is part of Vereniging.
 *
 * Vereniging is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Vereniging is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Vereniging.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Tests\Controller\Member;

use App\Entity\LogfileEntry;
use App\Entity\MemberEntry;
use App\Tests\WebTestCase;
use Exception;
use Symfony\Component\DomCrawler\Crawler;

/**
 * Class MemberClubDataControllerTest
 */
class MemberClubDataControllerTest extends WebTestCase
{
    private ?MemberEntry $member1;


    /**
     * @inheritDoc
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->loadAllFixtures();
        $this->member1 = $this->getFixtureReference('member_entry_last_name1');
    }


    /**
     * Test if clicking the edit button changed the output to a form.
     *
     * @throws Exception
     */
    public function testShowEdit(): void
    {
        // This should show input fields for the personal data fields
        $crawler = $this->getMyClient()->request('GET', "/member/edit_club_data/".$this->member1->getId());
        $this->checkClubDataEditFields($crawler, 'firstname1.lastname1', 'Family member');

        // Check if the member status is selected
        $this->assertEquals(1, $crawler->filter('select#member_entry_club_data_userRole option[selected="selected"]:contains("System administrator")')->count());

        // Check function checkboxes
        $this->functionCheckboxCheck($crawler, 'Activities', 'Chairman', false);
        $this->functionCheckboxCheck($crawler, 'Activities', 'Member', true);
        $this->functionCheckboxCheck($crawler, 'Board', 'Chairman', true);
        $this->functionCheckboxCheck($crawler, 'Board', 'Vice chairman', true);
        $this->functionCheckboxCheck($crawler, 'Board', 'Member', true);
        $this->functionCheckboxCheck($crawler, 'Public relations', 'Chairman', false);
        $this->functionCheckboxCheck($crawler, 'Public relations', 'Member', true);

        // Call the same page for a member where most data fields are not set. This test was needed because of a bug in
        // the form query builder when the member has no address fields.
        /** @var MemberEntry $member4 */
        $member4 = $this->getFixtureReference('member_entry_last_name4');

        $crawler = $this->client->request('GET', "/member/edit_club_data/".$member4->getId());
        $this->checkClubDataEditFields($crawler, 'firstname4.lastname4', 'Regular member');

        // Check the order of the committee memberships
        // The language must be switched to German, because in the English fields are ordered correctly by their
        // definition
        $crawler = $this->getMyClient()->request('GET', "/member/edit_club_data/".$this->member1->getId(), ['_locale' => 'de']);
        $expected = [
            'Public relations' => ['Mitglied', 'Vorsitzender'],
            'Veranstaltungen'  => ['Mitglied', 'Vorsitzender'],
            'Vorstand'         => ['Mitglied', 'Stellvertretender Vorsitzender', 'Vorsitzender'],
        ];
        $this->checkFunctionTable($expected, $crawler);

        $crawler = $this->getMyClient()->request('GET', "/member/cancel_club_data/".$this->member1->getId(), ['_locale' => 'de']);
        $expected = [
            'Public relations' => ['Mitglied'],
            'Veranstaltungen'  => ['Mitglied'],
            'Vorstand'         => ['Mitglied', 'Stellvertretender Vorsitzender', 'Vorsitzender'],
        ];
        $this->checkFunctionTable($expected, $crawler);

        $crawler = $this->getMyClient()->request('GET', "/member/firstname1.lastname1", ['_locale' => 'de']);
        $this->checkFunctionTable($expected, $crawler);
    }


    /**
     * Test if changes in the form are written to the database and then shown correctly.
     *
     * @throws Exception
     */
    public function testSubmitEdit(): void
    {
        $this->clearAllLogFiles();

        // This should show input fields for the club data fields
        $crawler = $this->getMyClient()->request('GET', "/member/edit_club_data/".$this->member1->getId());
        $buttonCrawlerNode = $crawler->selectButton('Save');
        $form = $buttonCrawlerNode->form();
        // Change all options to see if the form can be saved with changes
        $form['member_entry_club_data[memberSince]']->select(2015);
        $form['member_entry_club_data[membershipEndRequested]']->tick();
        $form['member_entry_club_data[membershipNumber]']->untick();
        $form['member_entry_club_data[membershipType]']->select($this->getFixtureReference('membership_type_group')->getId());
        $form['member_entry_club_data[userRole]']->select($this->getFixtureReference('user_role_role_member_administrator')->getId());
        // Check activities chairman
        $form['member_entry_club_data[committeeFunctionMaps][0]']->tick();
        // Uncheck board vice chariman
        $form['member_entry_club_data[committeeFunctionMaps][4]']->untick();
        $crawler = $this->client->submit($form);

        // Check if all the changed fields have been saved correctly
        $this->assertEquals(1, $crawler->filter('div.col-sm-10:contains("2015")')->count());
        $this->assertEquals(1, $crawler->filter('label:contains("Membership end requested")')->siblings()->filter('div.col-sm-10:contains("Yes")')->count());
        $this->assertEquals(1, $crawler->filter('label:contains("Use direct debit")')->siblings()->filter('div.col-sm-10:contains("No")')->count());
        // 2 because of the table header
        $this->assertEquals(2, $crawler->filter('div.col-sm-10:contains("Group member")')->count());
        $this->assertEquals(1, $crawler->filter('div.col-sm-10:contains("Member administrator")')->count());
        $this->assertEquals(1, $crawler->filter('td:contains("Activities")')->siblings()->filter('td:contains("Chairman")')->count());
        $this->assertEquals(1, $crawler->filter('td:contains("Activities")')->siblings()->filter('td:contains("Member")')->count());
        $this->assertEquals(1, $crawler->filter('td:contains("Board")')->siblings()->filter('td:contains("Member")')->count());
        $this->assertEquals(1, $crawler->filter('td:contains("Board")')->siblings()->filter('td:contains("Chairman")')->count());
        $this->assertEquals(0, $crawler->filter('td:contains("Board")')->siblings()->filter('td:contains("Vice chairman")')->count());
        $this->assertEquals(0, $crawler->filter('td:contains("Public relations")')->siblings()->filter('td:contains("Chairman")')->count());
        $this->assertEquals(1, $crawler->filter('td:contains("Public relations")')->siblings()->filter('td:contains("Member")')->count());

        $logMessages = $this->getAllLogs();
        $this->assertEquals(10, count($logMessages));

        // Check messages 9 and 10 on adding and removing committee memberships
        // First check the German translation
        LogfileEntry::prepareTranslation($this->getEntityManager(), $this->getTranslator(), 'de');
        $this->assertEquals(['Funktion' => ['Stellvertretender Vorsitzender'], 'Arbeitsgruppe' => ['Vorstand']], $logMessages[9]->getComplexLogEntry()->getDataAsArray());
        $this->assertEquals('Arbeitsgruppenmitgliedschaft entfernt', $logMessages[9]->getComplexLogEntry()->getMainMessage());
        $this->assertEquals(['Funktion' => ['Vorsitzender'], 'Arbeitsgruppe' => ['Veranstaltungen']], $logMessages[8]->getComplexLogEntry()->getDataAsArray());
        $this->assertEquals('Arbeitsgruppenmitgliedschaft hinzugefügt', $logMessages[8]->getComplexLogEntry()->getMainMessage());
        // Second check the English translation
        LogfileEntry::prepareTranslation($this->getEntityManager(), $this->getTranslator(), 'en');
        $this->assertEquals(['Function' => ['Vice chairman'], 'Committee' => ['Board']], $logMessages[9]->getComplexLogEntry()->getDataAsArray());
        $this->assertEquals('Removed committee membership', $logMessages[9]->getComplexLogEntry()->getMainMessage());
        $this->assertEquals(['Function' => ['Chairman'], 'Committee' => ['Activities']], $logMessages[8]->getComplexLogEntry()->getDataAsArray());
        $this->assertEquals('Added committee membership', $logMessages[8]->getComplexLogEntry()->getMainMessage());

        // Check if the direct debit changes are logged for all members of the current group
        $this->assertEquals('Firstname1', $logMessages[7]->getChangesOnMember()->getFirstName());
        $this->assertEquals('Firstname10', $logMessages[6]->getChangesOnMember()->getFirstName());
        $this->assertEquals('Firstname11', $logMessages[5]->getChangesOnMember()->getFirstName());
        $this->assertEquals('Firstname12', $logMessages[4]->getChangesOnMember()->getFirstName());
        $this->assertEquals("Don't use direct debit for membership number 1", $logMessages[7]->getLogentry());
        $this->assertEquals("Don't use direct debit for membership number 1", $logMessages[6]->getLogentry());
        $this->assertEquals("Don't use direct debit for membership number 1", $logMessages[5]->getLogentry());
        $this->assertEquals("Don't use direct debit for membership number 1", $logMessages[4]->getLogentry());


        $expected = [
            'membership end requested' => ['no', 'yes'],
            'membership type' => ['Family member', 'Group member'],
            'user role' => ['System administrator', 'Member administrator'],
            'member since' => ['2016', '2015'],
        ];
        $this->checkLogMessage($logMessages[3], $expected, 'Firstname1');

        // Check that the other 3 group members also got a new membership type
        $expected = [
            'membership type' => ['Family member', 'Group member'],
        ];

        $this->checkLogMessage($logMessages[0], $expected, 'Firstname12');
        $this->checkLogMessage($logMessages[1], $expected, 'Firstname11');
        $this->checkLogMessage($logMessages[2], $expected, 'Firstname10');
    }


    /**
     * Test if changes in the form are written to the database and then shown correctly.
     *
     * @throws Exception
     */
    public function testSubmitEdit1(): void
    {
        $this->clearAllLogFiles();

        // Set the status to future member to check if this can be changed and if the membership number and direct debit
        // are cleared.
        $crawler = $this->getMyClient()->request('GET', "/member/edit_club_data/".$this->member1->getId());
        $buttonCrawlerNode = $crawler->selectButton('Save');
        $form = $buttonCrawlerNode->form();
        $form['member_entry_club_data[status]']->select($this->getFixtureReference('status_future_member')->getId());
        $crawler = $this->client->submit($form);

        $this->assertEquals(1, $crawler->filter('label:contains("Membership number")')->siblings()->filter('div.col-sm-10:contains("-")')->count());
        $this->assertEquals(1, $crawler->filter('label:contains("Use direct debit")')->siblings()->filter('div.col-sm-10:contains("-")')->count());
        $this->assertEquals(1, $crawler->filter('div.col-sm-10:contains("Future member")')->count());

        $logMessages = $this->getAllLogs();
        $this->assertEquals(1, count($logMessages));

        // Check that the other 3 group members also got a new membership type
        $expected = [
            'status' => ['Member', 'Future member'],
            'membership number' => ['1', '-'],
        ];

        $this->checkLogMessage($logMessages[0], $expected, 'Firstname1');
    }


    /**
     * When the status is changed, then also the membership number and the member since fields can change. Test if this
     * is done correctly.
     *
     * @throws Exception
     */
    public function testStatusChanges(): void
    {
        /** @var MemberEntry $member4 */
        $member4 = $this->getFixtureReference('member_entry_last_name4');

        // Call the edit page for the future member (member 4)
        $crawler = $this->getMyClient()->request('GET', "/member/edit_club_data/".$member4->getId());
        $buttonCrawlerNode = $crawler->selectButton('Save');
        $form = $buttonCrawlerNode->form();
        // Change the status to member. This should set the membership number and the member since fields.
        $form['member_entry_club_data[status]']->select($this->getFixtureReference('status_member')->getId());
        $crawler = $this->client->submit($form);

        // Check if all the changed fields have been saved correctly
        $this->assertEquals(1, $crawler->filter('label:contains("User status")')->siblings()->filter('div:contains("Member")')->count());
        $this->assertEquals(1, $crawler->filter('label:contains("Membership number")')->siblings()->filter('div:contains("21")')->count());
        $this->assertEquals(1, $crawler->filter('label:contains("Member since")')->siblings()->filter('div:contains("'.date('Y').'")')->count());

        // Call the edit page for member 4 and change the status back to future_member. This should remove the
        // membership number, but leave the member since in tact.
        $crawler = $this->client->request('GET', "/member/edit_club_data/".$member4->getId());
        $buttonCrawlerNode = $crawler->selectButton('Save');
        $form = $buttonCrawlerNode->form();
        // Change the status to future_member. This should remove the membership number
        $form['member_entry_club_data[status]']->select($this->getFixtureReference('status_future_member')->getId());
        $crawler = $this->client->submit($form);

        // Check if all the changed fields have been saved correctly
        $this->assertEquals(1, $crawler->filter('label:contains("User status")')->siblings()->filter('div:contains("Future member")')->count());
        $this->assertEquals(1, $crawler->filter('label:contains("Membership number")')->siblings()->filter('div:contains("-")')->count());
        $this->assertEquals(1, $crawler->filter('label:contains("Member since")')->siblings()->filter('div:contains("'.date('Y').'")')->count());
    }


    /**
     * Clicking the cancel button would just render the personal data section with edit = false. This is tested by just
     * calling this route.
     *
     * @throws Exception
     */
    public function testCancelEdit(): void
    {
        // This should show input fields for the personal data fields
        $crawler = $this->getMyClient()->request('GET', "/member/cancel_club_data/".$this->member1->getId());
        $this->assertEquals(1, $crawler->filter('label:contains("User name")')->count());
        $this->assertEquals(1, $crawler->filter('div.col-sm-10:contains("firstname1.lastname1")')->count());
        $this->assertEquals(1, $crawler->filter('label:contains("Membership number")')->count());
        $this->assertEquals(1, $crawler->filter('label:contains("Membership number")')->siblings()->filter('div.col-sm-10:contains("1")')->count());
        $this->assertEquals(1, $crawler->filter('label:contains("Use direct debit")')->count());
        $this->assertEquals(1, $crawler->filter('label:contains("Use direct debit")')->siblings()->filter('div.col-sm-10:contains("Yes")')->count());
        $this->assertEquals(1, $crawler->filter('label:contains("Member since")')->count());
        $this->assertEquals(1, $crawler->filter('div.col-sm-10:contains("2016")')->count());
        $this->assertEquals(1, $crawler->filter('label:contains("Membership end requested")')->count());
        $this->assertEquals(1, $crawler->filter('div.col-sm-10:contains("No")')->count());
        $this->assertEquals(1, $crawler->filter('label:contains("User status")')->count());
        $this->assertEquals(3, $crawler->filter('div.col-sm-10:contains("Member")')->count());
        $this->assertEquals(2, $crawler->filter('div.col-sm-10:contains("member")')->count());
        $this->assertEquals(1, $crawler->filter('label:contains("Membership type")')->count());
        $this->assertEquals(2, $crawler->filter('div.col-sm-10:contains("Family member")')->count());
        $this->assertEquals(1, $crawler->filter('label:contains("User role")')->count());
        $this->assertEquals(1, $crawler->filter('div.col-sm-10:contains("System administrator")')->count());
        $this->assertEquals(1, $crawler->filter('label:contains("Committee memberships")')->count());
        $this->assertEquals(0, $crawler->filter('td:contains("Activities")')->siblings()->filter('td:contains("Chairman")')->count());
        $this->assertEquals(1, $crawler->filter('td:contains("Activities")')->siblings()->filter('td:contains("Member")')->count());
        $this->assertEquals(1, $crawler->filter('td:contains("Board")')->siblings()->filter('td:contains("Member")')->count());
        $this->assertEquals(1, $crawler->filter('td:contains("Board")')->siblings()->filter('td:contains("Chairman")')->count());
        $this->assertEquals(1, $crawler->filter('td:contains("Board")')->siblings()->filter('td:contains("Vice chairman")')->count());
        $this->assertEquals(0, $crawler->filter('td:contains("Public relations")')->siblings()->filter('td:contains("Chairman")')->count());
        $this->assertEquals(1, $crawler->filter('td:contains("Public relations")')->siblings()->filter('td:contains("Member")')->count());
        $this->assertEquals(1, $crawler->filter('label:contains("Membership number")')->siblings()->filter('div:contains("1")')->count());
    }


    /**
     * Test adding and removing members to and from the group.
     *
     * @throws Exception
     */
    public function testGroupFunctions(): void
    {
        $this->clearAllLogFiles();

        // This should show input fields for the club data fields
        $crawler = $this->getMyClient()->request('GET', "/member/edit_club_data/".$this->member1->getId());
        $buttonCrawlerNode = $crawler->selectButton('Save');
        $form = $buttonCrawlerNode->form();
        // Add member9 to group of member1, remove member11
        $form['member_entry_club_data[groupMembers][0]']->untick();
        $form['member_entry_club_data[groupMembers][3]']->tick();
        $crawler = $this->client->submit($form);

        $this->assertEquals(0, $crawler->filter('td:contains("Firstname11")')->count());
        $this->assertEquals(1, $crawler->filter('td:contains("Firstname9")')->count());
        $this->assertEquals(1, $crawler->filter('td:contains("Firstname10")')->count());
        $this->assertEquals(1, $crawler->filter('td:contains("Firstname12")')->count());

        $logMessages = $this->getAllLogs();
        $this->assertEquals(5, count($logMessages));

        $this->checkLogMessage($logMessages[0], ['Membership group members' => ["",                                                                      "Firstname10 Lastname10, Firstname12 Lastname12, Firstname1 Lastname1"]], 'Firstname9');
        $this->checkLogMessage($logMessages[1], ['Membership group members' => ["Firstname1 Lastname1, Firstname10 Lastname10, Firstname11 Lastname1",   "Firstname1 Lastname1, Firstname10 Lastname10, Firstname9 Lastname9"  ]], 'Firstname12');
        $this->checkLogMessage($logMessages[2], ['Membership group members' => ["Firstname1 Lastname1, Firstname10 Lastname10, Firstname12 Lastname12",  ""                                                                    ]], 'Firstname11');
        $this->checkLogMessage($logMessages[3], ['Membership group members' => ["Firstname1 Lastname1, Firstname11 Lastname1, Firstname12 Lastname12",   "Firstname1 Lastname1, Firstname12 Lastname12, Firstname9 Lastname9"  ]], 'Firstname10');
        $this->checkLogMessage($logMessages[4], ['Membership group members' => ["Firstname10 Lastname10, Firstname11 Lastname1, Firstname12 Lastname12", "Firstname10 Lastname10, Firstname12 Lastname12, Firstname9 Lastname9"]], 'Firstname1');
    }


    /**
     * Test if the user rights for working with the club data section are correct.
     *
     * @throws Exception
     */
    public function testCheckUserRights(): void
    {
        // User 1 has the right to change club data settings, so there should be a link in the header for that
        $crawler = $this->getMyClient()->request('GET', "/member/firstname1.lastname1");
        $this->assertEquals(1, $crawler->filter('a[id="open_club_data"]')->count());

        // User 2 should not see a link for editing club data
        $client = $this->getMyClient('firstname2.lastname2');
        $crawler = $client->request('GET', "/member/firstname2.lastname2");
        $this->assertEquals(0, $crawler->filter('a[id="open_club_data"]')->count());

        // Accessing the edit club data page should throw an exception for user 2
        /** @var MemberEntry $member2 */
        $member2 = $this->getFixtureReference('member_entry_last_name2');
        $crawler = $client->request('GET', "/member/edit_club_data/".$member2->getId());
        $this->assertEquals(1, $crawler->filter('title:contains("Access denied")')->count());
    }


    /**
     * @param Crawler $crawler   Crawler component pointing to current page
     * @param string  $committee Committee name
     * @param string  $function  Function name
     * @param bool    $checked   True, when the checkbox is expected to be set
     */
    private function functionCheckboxCheck(Crawler $crawler, string $committee, string $function, bool $checked): void
    {
        $this->assertEquals(1, $crawler->filter("td:contains(".$committee.")")->count());

        $tdCrawler = $crawler->filter("td#".$this->replaceSpaces($committee.'_'.$function));
        $this->assertEquals(1, $tdCrawler->filter("label:contains(".$function.")")->count());
        $this->assertEquals(1, $tdCrawler->filter("input[type=checkbox]")->count());
        $this->assertEquals($checked ? 1 : 0, $tdCrawler->filter("input[checked=checked]")->count());
    }


    /**
     * Replace spaces in the string by dashes.
     *
     * @param string $value
     *
     * @return string
     */
    private function replaceSpaces(string $value): string
    {
        return (str_replace(' ', '-', $value));
    }


    /**
     * @param Crawler $crawler        Current crawler to the member club data edit page
     * @param string  $userName       Username to check
     * @param string  $membershipType Membership type for the current user
     */
    private function checkClubDataEditFields(Crawler $crawler, string $userName, string $membershipType): void
    {
        // Only the club data title should appear
        $this->assertEquals(0, $crawler->filter('h6.card-header:contains("Personal data")')->count());
        $this->assertEquals(0, $crawler->filter('h6.card-header:contains("Contact data")')->count());
        $this->assertEquals(0, $crawler->filter('h6.card-header:contains("Company data")')->count());
        $this->assertEquals(1, $crawler->filter('h6.card-header:contains("Club data")')->count());

        $this->assertEquals(1, $crawler->filter('label:contains("User name")')->count());
        $this->assertEquals(1, $crawler->filter('div.col-sm-10:contains("'.$userName.'")')->count());
        $this->assertEquals(1, $crawler->filter('label:contains("Membership number")')->count());
        $this->assertEquals(1, $crawler->filter('label:contains("Member since")')->count());
        $this->assertEquals(1, $crawler->filter('label:contains("Membership end requested")')->count());
        $this->assertEquals(1, $crawler->filter('label:contains("User status")')->count());
        $this->assertEquals(1, $crawler->filter('label:contains("Membership type")')->count());
        $this->assertEquals(1, $crawler->filter('label:contains("User role")')->count());
        $this->assertEquals(1, $crawler->filter('label:contains("Committee memberships")')->count());

        // It should be possible to select an empty member since year, and all years starting 1950 until today
        $this->assertEquals(1, $crawler->filter('select#member_entry_club_data_memberSince option[value=""]')->count());
        $this->assertEquals(1, $crawler->filter('select#member_entry_club_data_memberSince option[value="1950"]')->count());
        $this->assertEquals(1, $crawler->filter('select#member_entry_club_data_memberSince option[value="1985"]')->count());
        $this->assertEquals(1, $crawler->filter('select#member_entry_club_data_memberSince option[value="'.date('Y').'"]')->count());

        // There is a checkbox for the membership end request
        $this->assertEquals(1, $crawler->filter('input#member_entry_club_data_membershipEndRequested[type="checkbox"]')->count());

        // Check if the member status is selected
        if ('firstname4.lastname4' === $userName) {
            // There is no checkbox for the membership end request
            $this->assertEquals(1, $crawler->filter('label:contains("Use direct debit")')->siblings()->filter('div.col-sm-10:contains("-")')->count());
            $this->assertEquals('Future member', $crawler->filter('select#member_entry_club_data_status option[selected="selected"]')->html());
        } else {
            // There is a checkbox for the membership end request
            $this->assertEquals(1, $crawler->filter('input#member_entry_club_data_membershipNumber[type="checkbox"]')->count());
            $this->assertEquals('Member', $crawler->filter('select#member_entry_club_data_status option[selected="selected"]')->html());
        }


        // Check if the membership type regular member is selected
        $this->assertEquals(
            1,
            $crawler->filter(
                'select#member_entry_club_data_membershipType option[selected="selected"]:contains("'.$membershipType.'")'
            )->count()
        );

        // Check if both buttons are available
        $this->assertEquals(1, $crawler->filter('button#save_club_data[type="submit"]:contains("Save")')->count());
        $this->assertEquals(1, $crawler->filter('button#reset_club_data[type="reset"]:contains("Cancel")')->count());
    }


    /**
     * @param LogfileEntry $logEntry
     * @param array        $expected
     * @param string       $firstName
     */
    private function checkLogMessage(LogfileEntry $logEntry, array $expected, string $firstName): void
    {
        $complexLogEntry = $logEntry->getComplexLogEntry();
        // Ensure that no previous test has set the translator, which leads to different results when running only this
        // test or running the test in the test suite.
        $complexLogEntry->prepareTranslation(null, null, null);
        $this->assertEquals('Updated %classname% entry %stringIdentifier%|memberentry,', $complexLogEntry->getMainMessage());
        $this->assertEquals($expected, $complexLogEntry->getDataAsArray());
        $this->assertEquals($firstName, $logEntry->getChangesOnMember()->getFirstName());
    }


    /**
     * Check if the texts in the committee function table are correct. This works with the plain text table as well as
     * with the table containing check boxes.
     *
     * @param array        $expected
     * @param Crawler|null $crawler
     */
    private function checkFunctionTable(array $expected, ?Crawler $crawler): void
    {
        $committeeIndex = 1;
        foreach ($expected as $committee => $functions) {
            // Check the committee name in the first column
            $selector = "#committee_table tr:nth-child($committeeIndex)>td";
            $this->assertEquals($committee, $crawler->filter($selector)->html(), "Expected committee name '$committee' in line $committeeIndex of the function table.");
            foreach ($functions as $functionIndex => $function) {
                $position = $functionIndex + 1;
                $selector = "#committee_table tr:nth-child($committeeIndex)>td:nth-child(".($functionIndex + 2).")";
                $this->assertEquals($function, $crawler->filter($selector)->text(), "Expected function '$function' in position $position of the line $committee.");
            }
            $committeeIndex++;
        }
    }
}
