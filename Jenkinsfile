#!/usr/bin/env groovy

// The following plugins have to be installed when using this jenkins file:
// * Warnings Next Generation Plugin (usually installed automatically)
// * Clover Plugin
// * HTML publisher
//
// When configuring the job, add the following to the jobs description, to see the resulting images generate by pdepend:
// <img type="image/svg+xml" height="300" src="job/master/lastSuccessfulBuild/artifact/build/pdepend/overview-pyramid.svg" width="500"></img>
// <img type="image/svg+xml" height="300" src="job/master/lastSuccessfulBuild/artifact/build/pdepend/dependencies.svg" width="500"></img>


pipeline {
    agent any

    triggers {
      pollSCM 'H/2 * * * *'
    }

    stages {
        stage('Install tools') {
            steps {
                // The phive cache is stored in the home directory of the jenkins user, which usually is in the
                // directory /var/lib/jenkins. There we use the caches subdirectory.
                sh 'phive --home ${HOME}/caches/phive --no-progress install --trust-gpg-keys 4AA394086372C20A,E82B2FB314E9906E,0F9684B8B16B7AB0,2A8299CE842DD38C,31C7E470E2138192,CBB3D576F2A0946F --force-accept-unsigned'
            }
        }

        stage('PHP lint') {
            steps {
                sh 'tools/phing lint'
                recordIssues enabledForFailure: true, tool: php()
            }
        }

//         stage('Check copyrights') {
//             steps {
//                 sh 'grep -c -r -l -L --include "*.twig" --exclude-dir={bin,vendor,var} "Copyright" . | wc -l'
//                 sh 'grep -c -r -l -L --include "*.php" --exclude-dir={bin,vendor,var} --exclude Kernel.php --exclude bootstrap.php --exclude xdebug-filter.php --exclude index.php --exclude bundles.php --exclude preload.php "Copyright" . | wc -l'
//                 recordIssues enabledForFailure: true, tool: php()
//             }
//         }

        stage('Install components') {
            steps {
                // The composer cache is stored in the home directory of the jenkins user, which usually is in the
                // directory /var/lib/jenkins. There we use the caches subdirectory.
                sh 'COMPOSER_HOME=${HOME}/caches/composer tools/composer install'
            }
        }

        stage('Clean and prepare') {
            steps {
                sh 'tools/phing prepare-coverage'
            }
        }

        stage('Code sniffer') {
            steps {
                sh 'tools/phing phpcs'
                recordIssues(
                    enabledForFailure: true, aggregatingResults: true,
                    tools: [php(), phpCodeSniffer(pattern: 'build/logs/checkstyle-result.xml', reportEncoding: 'UTF-8')]
                )
            }
        }

        stage('Mess detector') {
            steps {
                sh 'tools/phing phpmd'
                recordIssues enabledForFailure: true, tool: pmdParser(pattern: 'build/logs/pmd.xml')
            }
        }

        stage('Copy detector') {
            steps {
                sh 'tools/phing phpcpd'
                recordIssues enabledForFailure: true, tool: cpd(pattern: 'build/logs/pmd-cpd.xml')
            }
        }

        stage('PHPloc') {
            steps {
                sh 'tools/phing phploc'
            }
        }

        stage('pdepend') {
            steps {
                sh 'tools/phing pdepend'
                archiveArtifacts artifacts: 'build/pdepend/overview-pyramid.svg', followSymlinks: false
                archiveArtifacts artifacts: 'build/pdepend/dependencies.svg', followSymlinks: false
            }
        }

// This step is disabled because phpdox at the moment has some compatibility issues between php 7 and 8 and xdebug...
//         stage('Generate documentation') {
//             steps {
//                 sh 'tools/phing phpdox'
//                 publishHTML (target: [
//                                 allowMissing: false,
//                                 alwaysLinkToLastBuild: false,
//                                 keepAll: true,
//                                 reportDir: 'build/api/html',
//                                 reportFiles: 'index.xhtml',
//                                 reportName: "PHPDox Documentation"
//
//                         ])
//             }
//         }

        stage('phpunit') {
            steps {
                sh 'tools/phing db-init'
                sh 'tools/phing phpunit-coverage'
                step([$class: 'CloverPublisher', cloverReportDir: 'build/logs', cloverReportFileName: 'clover.xml'])
                recordIssues enabledForFailure: true, tool: junitParser(pattern: 'build/logs/junit.xml')
                junit 'build/logs/junit.xml'
                publishHTML (target: [
                                allowMissing: false,
                                alwaysLinkToLastBuild: false,
                                keepAll: true,
                                reportDir: 'build/coverage',
                                reportFiles: 'index.html',
                                reportName: "Coverage Report"

                ])
            }
        }
    }
//     post {
//         always {
//             sh 'chmod -R 777 .' // set rights so that the cleanup job can do its work
// //            cleanWs(disableDeferredWipeout: true)
//         }
//     }
}