<?php

/*
 * Copyright (C) 2022 by Luc Hamers <vereniging@hamers.de>
 *
 * This file is part of Vereniging.
 *
 * Vereniging is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Vereniging is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Vereniging.  If not, see <https://www.gnu.org/licenses/>.
 */


/**
 * This script is used by a local build server to deploy a vereniging installation to a remote server. It does the
 * following steps:
 * 1.  Install the correct third party packages
 * 2.  Clear cache directories
 * 3.  Remove installer files
 * 4.  Zip all content
 * 5.  Create security token
 * 6.  Create temporary directory on FTP server and copy zip file to there
 * 7.  Unpack the zip file on remote server
 * 8.  Remove old directory
 * 9.  Update database structures via doctrine migrations
 * 10. Rename temporary directory to productive directory
 * 11. Clean up local files
 *
 * To prevent problems with database settings, it is important to first install a vereniging installation according to
 * the README.md file. The deploy script needs a working installation before it is called for the first time.
 */
class Deploy
{
    private static $userName = '';
    private static $password = '';
    private static $ftpServer = '';
    private static $webDir = '';
    private static $installDir = '';
    private static $webUrl = '';
    private static $phpExecutable = '';


    /**
     * @param string $userName
     * @param string $password
     * @param string $ftpServer
     * @param string $webDir
     * @param string $installDir
     * @param string $webUrl
     * @param string $phpExecutable
     */
    public static function init(string $userName, string $password, string $ftpServer, string $webDir, string $installDir, string $webUrl, string $phpExecutable)
    {
        self::$userName = $userName;
        self::$password = $password;
        self::$ftpServer = $ftpServer;
        self::$webDir = $webDir;
        self::$installDir = $installDir;
        self::$webUrl = $webUrl;
        self::$phpExecutable = $phpExecutable;

        echo "Initializing variables:\n";
        echo "   FTP user name:                $userName\n";
        echo "   FTP password:                 **********\n";
        echo "   FTP-server URI:               $ftpServer\n";
        echo "   FTP server web directory:     $webDir\n";
        echo "   Vereniging install directory: $installDir\n";
        echo "   web URI:                      $webUrl\n";
        echo "   path to php executable:       $phpExecutable\n";
        echo "   FTP location:                 ftp://$ftpServer/$webDir/$installDir\n";
        echo "   Web location:                 $webUrl/$installDir/public/\n";
    }


    /**
     * This function is called either by CLI or by web server.
     */
    public static function execute()
    {
        if (isset($_GET['unzip']) && '' === $_GET['unzip']) {
            if (self::checkToken()) {
                self::remoteUnzip();
            }

            return;
        }

        if (isset($_GET['migrate']) && '' !== $_GET['migrate']) {
            if (self::checkToken()) {
                self::remoteMigrate($_GET['migrate']);
            }

            return;
        }

        self::localActions();
    }


    /**
     * This method is called when this class is used on the remote server. It unzips the previously copied zip file and
     * then removes the zip file.
     */
    private static function remoteUnzip()
    {
        $archive = 'vereniging.zip';
        echo "        Extracting zip archive $archive\n";
        $zip = new ZipArchive();
        if (true === $zip->open($archive)) {
            $zip->extractTo(getcwd());
            $zip->close();
            echo "        Deleting zip archive $archive\n";
            unlink($archive);
        }
    }


    /**
     * This method is called when this class is used on the remote server. It unzips the previously copied zip file and
     * then removes the zip file.
     *
     * @param string $phpExecutable Path to the php executable, where slashes are to be encoded by '__', e.g. __bin__php
     */
    private static function remoteMigrate(string $phpExecutable)
    {
        $phpExecutable = str_replace('__', '/', $phpExecutable);
        if (!self::checkPhpExecutable($phpExecutable)) {
            return;
        }
        echo "        Migrating database with php executable $phpExecutable\n";
        $output = [];
        exec("$phpExecutable bin/console doctrine:migrations:migrate --no-interaction -e prod  2>&1", $output);
        foreach ($output as $line) {
            echo "          $line\n";
        }
    }


    /**
     * This clears the remote installation directory by copying a php file with an rm command, and then the directory is
     * created again. The file is created here, send by ftp to the server, executed and then removed. This is needed to
     * be able to do a recursive delete.
     */
    private static function clearRemoteDirectory()
    {
        $installDir = self::$installDir;
        echo "        Clearing remote directory \"$installDir\":\n";

        // Create php file containing the rm command for the selected directory
        $fileContent = "<?php\nexec('rm -rf $installDir');\n";
        $fileName = "delete_".time().".php";
        file_put_contents($fileName, $fileContent);

        echo "          1. copy delete script \"$fileName\" to server\n";
        self::executeFtpCommand("put $fileName");

        echo "          2. executing script on server\n";
        file_get_contents(self::$webUrl."/$fileName");

        echo "          3. cleaning up\n";
        self::executeFtpCommand("rm $fileName");
        unlink($fileName);
    }


    /**
     * This is the main routine for teh deployment process. It runs on the local server, sends all the needed data to
     * the remote server and runs several st
     *
     * @throws Exception
     */
    private static function localActions()
    {
        // 1. Install changed third party packages. This would also ask for database settings, when composer install is
        // called for first time, so call it before deploy is used.
        echo "1.  Updating third party packages\n";
        $output = [];
        exec('composer install 2>&1', $output);
        foreach ($output as $line) {
            echo "        $line\n";
        }

        // 2. empty cache directories
        echo "2.  Clearing cache directories\n";
        self::rrmdir(getcwd()."/var/cache/dev");
        self::rrmdir(getcwd()."/var/cache/prod");
        self::rrmdir(getcwd()."/var/cache/test");

        // 3. remove installer files
        echo "3.  Removing installer files\n";
        unlink("installer.php");
        unlink("installer_step2.php");


        // 4. Zip the current directory with all subdirectories, omitting .git, var and build
        echo "4.  Compressing the directory\n";
        exec("zip -r vereniging.zip . -x .git/\* -x build/\* -x var/\*");

        echo "5.  Creating security token\n";
        $token = bin2hex(random_bytes(64));
        file_put_contents('token.txt', $token);
        // 5. create temporary directory on FTP server
        // 6. copy zip file to remote server
        $deployTempDir = "deploy_".time();
        echo "6.  Copying files to ".self::$ftpServer."/".self::$webDir."/$deployTempDir, including token\n";
        self::executeFtpCommand("mkdir $deployTempDir\ncd $deployTempDir\nput vereniging.zip\nput deploy.php\nput token.txt");

        // 7. unpack the zip file
        echo "7.  Calling remote script to unpack files\n";
        $webUrl = self::$webUrl;
        $installDir = self::$installDir;
        echo file_get_contents("$webUrl/$deployTempDir/deploy.php?unzip&token=$token");

        // 8. remove old directory
        echo "8.  Delete current install directory \"$installDir\"\n";
        self::clearRemoteDirectory();

        // 9. Migrate database
        echo "9.  Migrating database\n";
        // Encode the slashes by double underscore
        $phpExecutable = str_replace('/', '__', self::$phpExecutable);
        echo file_get_contents("$webUrl/$deployTempDir/deploy.php?migrate=$phpExecutable&token=$token");

        // 10. rename temporary directory
        echo "10. Renaming temporary directory to \"$installDir\" and removing temporary files\n";
        self::executeFtpCommand("mv $deployTempDir $installDir\ncd $installDir\nrm deploy.php\nrm token.txt");
        unlink("token.txt");

        echo "11. Deleting local zip file\n";
        unlink('vereniging.zip');
    }


    /**
     * Connect to FTP server, change to webdir and execute commands in the command string
     *
     * @param string $command Command(s) to be executed. Multiple commands must be separated by "\n"
     */
    private static function executeFtpCommand(string $command)
    {
        $user = self::$userName;
        $password = self::$password;
        $server = self::$ftpServer;
        $webDir = self::$webDir;
        exec("lftp -u $user,$password -e \"set ftp:ssl-allow no;\" ftp://$server << EOF
cd $webDir        
$command
EOF");
    }


    /**
     * Recursively removes a folder along with all its files and directories. This is based on a snipped written by Ben
     * Lobaugh on https://ben.lobaugh.net/blog/910/php-recursively-remove-a-directory-and-all-files-and-folder-contained-within
     *
     * @param String $path
     */
    private static function rrmdir($path)
    {
        // Open the source directory to read in files
        try {
            $directoryIterator = new DirectoryIterator($path);
            foreach ($directoryIterator as $item) {
                if ($item->isFile()) {
                    unlink($item->getRealPath());
                } elseif (!$item->isDot() && $item->isDir()) {
                    self::rrmdir($item->getRealPath());
                }
            }
            rmdir($path);
        } catch (Exception $e) {
            // left intentionally blank
        }
    }

    /**
     * Check if the security token passed by the get parameter matches the tokes previously send via ftp.
     *
     * @return bool
     */
    private static function checkToken(): bool
    {
        // Suppress warning
        $token = @file_get_contents('token.txt');
        echo "        Checking token: ";

        if (false === $token) {
            echo "not set\n";

            return false;
        }

        if (!isset($_GET['token'])) {
            echo "not passed\n";

            return false;
        }

        if ($token !== $_GET['token']) {
            echo "token doesn't match\n";

            return false;
        }

        echo "token is OK\n";

        return true;
    }


    /**
     * This method tests if the php executable is available and works correctly.
     *
     * @param string $phpExecutable php executable including path.
     *
     * @return bool True when the executable is OK
     */
    private static function checkPhpExecutable(string $phpExecutable): bool
    {
        // The executable must end with /php
        if ('/php' !== substr($phpExecutable, -4)) {
            echo "        Php executable '$phpExecutable' has wrong file name\n";

            return false;
        }

        // Check if the php executable works as expected using calling a small piece of code.
        if ('test' === exec("$phpExecutable -r \"echo 'test';\"")) {
            return true;
        }

        echo "        Php executable '$phpExecutable' is no php executable\n";

        return false;
    }
}


if (defined('STDIN')) {
    if (8 !== $argc) {
        $arguments = [
            'FTP user name' => 'User name to be used for the FTP server',
            'FTP password' => 'User password for the ftp server',
            'FTP server URI' => 'Address for the FTP server, e.g. ftp.myserver.com',
            'FTP server web directory' => 'Directory on the FTP server in which the directory for the vereniging software has to be copied, e.g httpdoc',
            'vereniging install directory' => 'Sub directory in FTP server web directory, in which the files for the vereniging software are to be copied, e.g. member_system',
            'web URI' => 'Address of the web server under which the vereniging install directory is found on the web server, e.g. https://www.myserver.com',
            'path to php executable' => 'Path the the php executable in the correct version on the web server, e.g. /opt/php/7.3/bin/php',
        ];
        echo "The script was not called with correct number of parameters:\n";
        echo "php ".$argv[0];
        $maxKeyLength = 0;
        foreach ($arguments as $key => $value) {
            echo " <$key>";
            if ($maxKeyLength < strlen($key)) {
                $maxKeyLength = strlen($key);
            }
        }
        echo "\n";

        $maxKeyLength += 6;
        foreach ($arguments as $key => $value) {
            echo str_pad("    $key: ", $maxKeyLength)."$value\n";
        }

        if (!is_executable('lftp')) {
            echo "The script needs the program \"lftp\" to be installed and in the path, which is not the case!\n";
        }

        exit(-1);
    }

    Deploy::init($argv[1], $argv[2], $argv[3], $argv[4], $argv[5], $argv[6], $argv[7]);
}


Deploy::execute();
