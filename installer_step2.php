<?php

/*
 * Copyright (C) 2022 by Luc Hamers <vereniging@hamers.de>
 *
 * This file is part of Vereniging.
 *
 * Vereniging is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Vereniging is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Vereniging.  If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * Copied code from davidkonrad on StackOverflow
 */

class FormHandler
{
    private $isFormProcessed;
    private $parameterFile;

    /**
     * FormHandler constructor.
     */
    public function __construct()
    {
        $this->isFormProcessed = false;
        $this->parameterFile = '';

        if (count($_POST)) {
            $this->updateConfigFile();
            $this->isFormProcessed = true;
        }
    }


    /**
     * @return bool
     */
    public function isFormProcessed(): bool
    {
        return $this->isFormProcessed;
    }


    /**
     * @return string
     */
    public function getParameterFile(): string
    {
        return $this->parameterFile;
    }


    /**
     * Read the configuration file and write all post variables to the file
     */
    public function updateConfigFile()
    {
        $environmentFile = file('.env.local');
        $this->parameterFile = '';
        $outputFile = '';
        // These fields were not in the form, set them here
        $_POST['APP_ENV'] = 'dev';
        $_POST['APP_SECRET'] = $this->generateRandomString();
        foreach ($environmentFile as $line) {
            // The variable lines look like this: "MY_PARAMETER=some value or string"
            preg_match("/^([A-Z_]+)=(.+)/", $line, $entries);
            // Omit all lines which do not contain variables
            if ((count($entries) > 2) && (array_key_exists($entries[1], $_POST))) {
                // When the content is a json string, add quotes
                if (false !== strpos($_POST[$entries[1]], '{')) {
                    $_POST[$entries[1]] = '\''.$_POST[$entries[1]].'\'';
                }
                $line = "$entries[1]=".$_POST[$entries[1]]."\n";
                $outputFile .= $line;
                $this->parameterFile .= $line;
            } else {
                $outputFile .= $line;
            }
        }

        file_put_contents('.env.local', $outputFile);
    }


    /**
     * Write the form to edit all values in the parameters.yml file or, when this form has been submitted, the resulting
     * file and next steps.
     */
    public function writeFormOrResult()
    {
        $configFile = getcwd().'/.env.local';
        if ($this->isFormProcessed()) {
            $parameterFile = $this->getParameterFile();
            echo <<<EOT
<!DOCTYPE html>
<html><body>
<p>Written the following data to the file $configFile</p>
<pre>$parameterFile</pre>
EOT;
            $this->initialiseDatabase();
            echo "</body></html>";
        } else {
            $htmlForm = $this->generateParameterForm();
            echo <<<EOT
<!DOCTYPE html>
<html><body>
<p>Documentation on the meaning of the parameters can be found in $configFile</p>
<form method="post">
<table>
    <tr><th>Variable</th><th>Value</th></tr>
$htmlForm
</table>
<input type="submit" name="submit" value="Submit">
</form>
</body></html>
EOT;
        }
    }

    /**
     * Generate random string
     *
     * @param int $length
     *
     * @return bool|string
     */
    private function generateRandomString($length = 40)
    {
        return substr(str_shuffle(str_repeat($x = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil($length/strlen($x)))), 1, $length);
    }

    /**
     * Read the parameter file and generate a form for its variables.
     *
     * @return string
     */
    private function generateParameterForm()
    {
        $htmlForm = '';
        $parameterFile = file('.env.local');
        foreach ($parameterFile as $line) {
            // The variable lines look like this: "MY_PARAMETER=some value or string"
            preg_match("/^([A-Z_]+)=(.+)/", $line, $entries);
            // Omit all lines which do not contain variables
            if (count($entries) > 2) {
                if (('APP_ENV' !== $entries[1]) && ('APP_SECRET' !== $entries[1])) {
                    if (0 === strpos($entries[2], "'")) {
                        $entries[2] = substr($entries[2], 1, -1);
                    }
                    $htmlForm .= "    <tr><td>$entries[1]: </td><td><input type='text' name='$entries[1]' size='100' value='$entries[2]'></td>\n";
                }
            }
        }

        return $htmlForm;
    }

    /**
     * Create database, create tables and initialise it with some default data
     */
    private function initialiseDatabase()
    {
        echo "<p>Initialising database:</p>\n<pre>\n";

        exec('./bin/console doctrine:database:create', $output);
        exec('./bin/console doctrine:schema:update --force', $output);
        exec('./bin/console doctrine:fixtures:load --group=installerFixtures -n', $output);
        exec('rm -v installer.php', $output);
        exec('rm -v installer_step2.php', $output);
        exec('rm -v adminFixtureData.php', $output);
        exec('rm -v composer', $output);

        foreach ($output as $outputLine) {
            echo "$outputLine\n";
        }
        echo <<<EOT
        </pre>

        <p>Basic setup for the Vereniging system finished. You can now log into the system: <a href='public/'>Login</a>, with the following credentials:</p>
        <ul>
          <li>Username: admin.admin</li>
          <li>Password: top-secret</li>
         </ul>

        <p>Before you continue, you should do the following:</p>
        <ul>
          <li>Change your password (My data -> edit symbol next to personal data)</li>
          <li>Setup the site parameters. To do this, open an editor (could be on your providers site dashboard) and edit the file .env.local</li>
         </ul>
EOT;
    }
}

$formHandler = new FormHandler();
$formHandler->writeFormOrResult();
