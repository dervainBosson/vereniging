<?php

/*
 * Copyright (C) 2022 by Luc Hamers <vereniging@hamers.de>
 *
 * This file is part of Vereniging.
 *
 * Vereniging is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Vereniging is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Vereniging.  If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * Execute the given command by displaying console output live to the user. (Amith, Stackoverflow)
 *
 * @param  string $cmd Command to be executed
 *
 * @return array   exit_status  :  exit status of the executed command
 *                 output       :  console output of the executed command
 */
function liveExecuteCommand(string $cmd)
{

    while (@ ob_end_flush()) {
        // left intentionally blank
    }; // end all output buffers if any

    $proc = popen("$cmd 2>&1 ; echo Exit status : $?", 'r');

    $completeOutput = "";

    while (!feof($proc)) {
        $liveOutput     = fread($proc, 4096);
        $completeOutput = $completeOutput.$liveOutput;
        echo "$liveOutput";
        @ flush();
    }

    pclose($proc);

    // get exit status
    preg_match('/[0-9]+$/', $completeOutput, $matches);

    // return exit status and intended output
    return [
        'exit_status'  => intval($matches[0]),
        'output'       => str_replace("Exit status : ".$matches[0], '', $completeOutput),
    ];
}

echo "<html><body>";

/**
 * Download composer into current directory and make it executable.
 */
function downloadComposer(): void
{
    echo "<pre>Downloading composer into ".getcwd()."</pre>";

    // Download composer
    if (false === file_put_contents("composer", fopen("https://getcomposer.org/download/1.9.0/composer.phar", 'r'))) {
        throw new Exception('Could not download composer');
    }

    // Make it excecutable
    chmod("composer", 0744);
}

try {
    // Check if the current directory is writable
    if (!is_writable('./')) {
        throw new Exception(sprintf('%s is not writable', getcwd()));
    }
    // Start by downloading the composer
    downloadComposer();

    echo "<pre>Setup configuration files</pre>";
    copy('.env', '.env.local');

    echo "<pre>Download and install external components</pre>";
    putenv("COMPOSER_HOME=".getcwd());
    echo "<pre>";
    liveExecuteCommand('php -d memory_limit=-1 composer install');
    echo "</pre>";
    echo "<p>The system is now installed correctly. Proceed with the first time configuration:</p>";
    echo '<a href="installer_step2.php"><button>Proceed to configuration</button></a>';
    exec('rm .htaccess');
} catch (\Exception $e) {
    echo $e->getMessage();
}

echo "</body></html>";
