<?php

/*
 * Copyright (C) 2022 by Luc Hamers <vereniging@hamers.de>
 *
 * This file is part of Vereniging.
 *
 * Vereniging is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Vereniging is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Vereniging.  If not, see <https://www.gnu.org/licenses/>.
 */

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201128213307 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql("Update user_role set  system_roles=JSON_ARRAY_APPEND(system_roles, '$', 'ROLE_VIEW_STATISTICS') where user_role in ('Serial letter signer', 'Serial letter author', 'Member administrator', 'Treasurer', 'System administrator')");
    }

    public function down(Schema $schema) : void
    {
        $this->addSql("UPDATE `user_role` SET `system_roles`= JSON_REMOVE(system_roles, JSON_UNQUOTE(JSON_SEARCH(system_roles, 'one', 'ROLE_VIEW_STATISTICS'))) WHERE user_role in ('Serial letter signer', 'Serial letter author', 'Member administrator', 'Treasurer', 'System administrator')");
    }
}
