<?php

/*
 * Copyright (C) 2022 by Luc Hamers <vereniging@hamers.de>
 *
 * This file is part of Vereniging.
 *
 * Vereniging is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Vereniging is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Vereniging.  If not, see <https://www.gnu.org/licenses/>.
 */

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;
use Psr\Log\LoggerInterface;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220109160135 extends AbstractMigration
{
    /**
     * @return string
     */
    public function getDescription() : string
    {
        return 'This adds translatable sender addresses to the email templates';
    }

    /**
     * @param Schema $schema
     *
     * @return void
     */
    public function up(Schema $schema) : void
    {
        $adminAddress = $_ENV["VERENIGING_ADMIN_EMAIL_ADDRESS"];
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE email_template ADD sender_address LONGTEXT NOT NULL');
        $this->addSql('ALTER TABLE email_template_translation DROP FOREIGN KEY FK_295ED2D12C2AC5D3');
        $this->addSql('ALTER TABLE email_template_translation ADD sender_address_translated VARCHAR(255) NOT NULL');
        $this->addSql('DROP INDEX idx_295ed2d12c2ac5d3 ON email_template_translation');
        $this->addSql('CREATE INDEX IDX_71F8068D2C2AC5D3 ON email_template_translation (translatable_id)');
        $this->addSql('ALTER TABLE email_template_translation ADD CONSTRAINT FK_295ED2D12C2AC5D3 FOREIGN KEY (translatable_id) REFERENCES email_template (id) ON DELETE CASCADE');
        $this->addSql("UPDATE email_template SET sender_address = '$adminAddress' WHERE 1");
        $this->addSql("UPDATE email_template_translation SET sender_address_translated = '$adminAddress' WHERE 1");
    }

    /**
     * @param Schema $schema
     *
     * @return void
     */
    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE email_template DROP sender_address');
        $this->addSql('ALTER TABLE email_template_translation DROP FOREIGN KEY FK_71F8068D2C2AC5D3');
        $this->addSql('ALTER TABLE email_template_translation DROP sender_address_translated');
        $this->addSql('DROP INDEX idx_71f8068d2c2ac5d3 ON email_template_translation');
        $this->addSql('CREATE INDEX IDX_295ED2D12C2AC5D3 ON email_template_translation (translatable_id)');
        $this->addSql('ALTER TABLE email_template_translation ADD CONSTRAINT FK_71F8068D2C2AC5D3 FOREIGN KEY (translatable_id) REFERENCES email_template (id) ON DELETE CASCADE');
    }
}
