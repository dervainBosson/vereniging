<?php

/*
 * Copyright (C) 2022 by Luc Hamers <vereniging@hamers.de>
 *
 * This file is part of Vereniging.
 *
 * Vereniging is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Vereniging is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Vereniging.  If not, see <https://www.gnu.org/licenses/>.
 */

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210717152103 extends AbstractMigration
{
    const OPENING = '<p><span class=\"mceNonEditable\" style=\"border: thin dotted grey;\" data-variable=\"opening\">opening</span></p>'."\n";
    const SIGNERS = '<p><span class=\"mceNonEditable\" style=\"border: thin dotted grey;\" data-variable=\"signers\">signers</span></p>';
    const CLOSING_DE = "\n<p>Mit Freundlichen Grüßen,</p>".self::SIGNERS."\n";
    const CLOSING_EN = "\n<p>Kind regards,</p>".self::SIGNERS."\n";
    const CLOSING_NL = "\n<p>Met vriendelijke groeten,</p>".self::SIGNERS."\n";
    const TEMPLATE_VARIABLE = '<span class=\"mceNonEditable\" style=\"border: thin dotted grey;\" data-variable=\"email content\">email content</span>';

    public function getDescription() : string
    {
        return 'Update for the serial email feature';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql("Update user_role set  system_roles=JSON_ARRAY_APPEND(system_roles, '$', 'ROLE_MANAGE_EMAIL_TEMPLATES') where user_role in ('Member administrator', 'Treasurer', 'System administrator')");
        $this->addSql("Update user_role set  system_roles=JSON_ARRAY_APPEND(system_roles, '$', 'ROLE_SEND_SERIAL_EMAILS') where user_role in ('Serial letter author', 'Member administrator', 'Treasurer', 'System administrator')");
        $this->addSql("Update user_role set  system_roles=JSON_ARRAY_APPEND(system_roles, '$', 'ROLE_MANAGE_SYSTEM_EMAIL_TEMPLATES') where user_role in ('System administrator')");
        $this->addSql('CREATE TABLE email_template (id SMALLINT AUTO_INCREMENT NOT NULL, template_name LONGTEXT NOT NULL, content LONGTEXT NOT NULL, template_type LONGTEXT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE email_template_translation (id INT AUTO_INCREMENT NOT NULL, translatable_id SMALLINT DEFAULT NULL, template_name_translated VARCHAR(255) NOT NULL, locale VARCHAR(255) NOT NULL, INDEX IDX_295ED2D12C2AC5D3 (translatable_id), UNIQUE INDEX email_template_translation_unique_translation (translatable_id, locale), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE email_template_translation ADD CONSTRAINT FK_295ED2D12C2AC5D3 FOREIGN KEY (translatable_id) REFERENCES email_template (id) ON DELETE CASCADE');
        $this->addSql("UPDATE serial_letter_content SET content=concat('".self::OPENING."', content) WHERE 1");
        $this->addSql("UPDATE serial_letter_content s JOIN language l ON (s.language_id = l.id) SET content=CONCAT(content, '".self::CLOSING_NL."') WHERE l.locale = 'nl'");
        $this->addSql("UPDATE serial_letter_content s JOIN language l ON (s.language_id = l.id) SET content=CONCAT(content, '".self::CLOSING_DE."') WHERE l.locale = 'de'");
        $this->addSql("UPDATE serial_letter_content s JOIN language l ON (s.language_id = l.id) SET content=CONCAT(content, '".self::CLOSING_EN."') WHERE l.locale = 'en'");
        $this->addSql("INSERT INTO email_template (id, template_name, content, template_type) VALUES (1, 'change message', '".self::TEMPLATE_VARIABLE."', 'change message')");
        $this->addSql("INSERT INTO email_template (id, template_name, content, template_type) VALUES (2, 'login data', '".self::TEMPLATE_VARIABLE."', 'login data')");
        $this->addSql("INSERT INTO email_template (id, template_name, content, template_type) VALUES (3, 'serial email', '".self::TEMPLATE_VARIABLE."', 'serial email')");
        $this->addSql("INSERT INTO email_template_translation (id, translatable_id, template_name_translated, locale) VALUES ('1', '1', 'Change message', 'en');");
        $this->addSql("INSERT INTO email_template_translation (id, translatable_id, template_name_translated, locale) VALUES ('2', '1', 'Veranderingsbericht', 'nl');");
        $this->addSql("INSERT INTO email_template_translation (id, translatable_id, template_name_translated, locale) VALUES ('3', '1', 'Änderungsnachricht', 'de');");
        $this->addSql("INSERT INTO email_template_translation (id, translatable_id, template_name_translated, locale) VALUES ('4', '2', 'Login data', 'en');");
        $this->addSql("INSERT INTO email_template_translation (id, translatable_id, template_name_translated, locale) VALUES ('5', '2', 'Aanmeldgegevens', 'nl');");
        $this->addSql("INSERT INTO email_template_translation (id, translatable_id, template_name_translated, locale) VALUES ('6', '2', 'Anmeldedaten', 'de');");
        $this->addSql("INSERT INTO email_template_translation (id, translatable_id, template_name_translated, locale) VALUES ('7', '3', 'Serial email', 'en');");
        $this->addSql("INSERT INTO email_template_translation (id, translatable_id, template_name_translated, locale) VALUES ('8', '3', 'Serie-email', 'nl');");
        $this->addSql("INSERT INTO email_template_translation (id, translatable_id, template_name_translated, locale) VALUES ('9', '3', 'Serien-E-Mail', 'de');");
    }

    public function down(Schema $schema) : void
    {
        $this->addSql("UPDATE `user_role` SET `system_roles`= JSON_REMOVE(system_roles, JSON_UNQUOTE(JSON_SEARCH(system_roles, 'one', 'ROLE_MANAGE_EMAIL_TEMPLATES'))) WHERE user_role in ('Member administrator', 'Treasurer', 'System administrator')");
        $this->addSql("UPDATE `user_role` SET `system_roles`= JSON_REMOVE(system_roles, JSON_UNQUOTE(JSON_SEARCH(system_roles, 'one', 'ROLE_SEND_SERIAL_EMAILS'))) WHERE user_role in ('Serial letter author', 'Member administrator', 'Treasurer', 'System administrator')");
        $this->addSql("UPDATE `user_role` SET `system_roles`= JSON_REMOVE(system_roles, JSON_UNQUOTE(JSON_SEARCH(system_roles, 'one', 'ROLE_MANAGE_SYSTEM_EMAIL_TEMPLATES'))) WHERE user_role in ('System administrator')");
        $this->addSql('ALTER TABLE email_template_translation DROP FOREIGN KEY FK_295ED2D12C2AC5D3');
        $this->addSql('DROP TABLE email_template_translation');
        $this->addSql('DROP TABLE email_template');
        $this->addSql("UPDATE serial_letter_content SET content=REPLACE(content, '".self::OPENING."', '') WHERE 1");
        $this->addSql("UPDATE serial_letter_content SET content=REPLACE(content, '".self::CLOSING_NL."', '') WHERE 1");
        $this->addSql("UPDATE serial_letter_content SET content=REPLACE(content, '".self::CLOSING_DE."', '') WHERE 1");
        $this->addSql("UPDATE serial_letter_content SET content=REPLACE(content, '".self::CLOSING_EN."', '') WHERE 1");
    }
}
