<?php

/*
 * Copyright (C) 2022 by Luc Hamers <vereniging@hamers.de>
 *
 * This file is part of Vereniging.
 *
 * Vereniging is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Vereniging is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Vereniging.  If not, see <https://www.gnu.org/licenses/>.
 */

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201121184102 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'Create new user role treasurer and move ROLE_MANAGE_FINANCES there from the member administrator';
    }

    public function up(Schema $schema) : void
    {
        // Copy the roles from the member administrator into the new role treasurer and create translations
        $this->addSql('insert into user_role( `user_role`,`system_roles`,`default_role` ) VALUES ("Treasurer", (select role.system_roles from user_role role where `user_role`="Member administrator"), 0);');
        $this->addSql('insert into user_role_translation( `translatable_id`,`user_role_translated`,`locale` ) VALUES ((select role.id from user_role role where `user_role`="Treasurer"), "Treasurer", "en")');
        $this->addSql('insert into user_role_translation( `translatable_id`,`user_role_translated`,`locale` ) VALUES ((select role.id from user_role role where `user_role`="Treasurer"), "Schatzmeister", "de")');
        $this->addSql('insert into user_role_translation( `translatable_id`,`user_role_translated`,`locale` ) VALUES ((select role.id from user_role role where `user_role`="Treasurer"), "Penningmeester", "nl")');
        $this->addSql('UPDATE `user_role` SET `system_roles`= JSON_REMOVE(system_roles, JSON_UNQUOTE(JSON_SEARCH(system_roles, "one", "ROLE_MANAGE_FINANCES"))) WHERE `user_role` = "Member administrator"');
    }

    public function down(Schema $schema) : void
    {
        $this->addSql("Update user_role set  system_roles=JSON_ARRAY_APPEND(system_roles, '$', 'ROLE_MANAGE_FINANCES') where user_role in ('Member administrator', 'System administrator')");
        $this->addSql('DELETE FROM `user_role_translation` WHERE `translatable_id` = (select role.id from user_role role where `user_role`="Treasurer");');
        $this->addSql('DELETE FROM `user_role` WHERE `user_role` = "Treasurer";');
    }
}
