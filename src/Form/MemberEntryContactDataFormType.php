<?php

/*
 * Copyright (C) 2022 by Luc Hamers <vereniging@hamers.de>
 *
 * This file is part of Vereniging.
 *
 * Vereniging is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Vereniging is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Vereniging.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Form;

use App\Entity\MailingList;
use App\Entity\MemberEntry;
use App\Repository\MailingListRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class MemberEntryContactDataType
 */
class MemberEntryContactDataFormType extends AbstractType
{
    /**
     * {@inheritdoc}
    */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('email')
            ->add('phone_numbers', CollectionType::class, [
                'entry_type' => PhoneNumberFormType::class,
                'entry_options' => ['current_locale' => $options['current_locale']],
                'by_reference' => false,
                'allow_add'    => true,
                'allow_delete' => true,
            ])
            ->add('member_address_maps', CollectionType::class, [
                'entry_type' => MemberAddressMapFormType::class,
                'entry_options' => ['current_locale' => $options['current_locale']],
                'by_reference' => false,
                'allow_add'    => true,
                'allow_delete' => true,
            ])
            ->add('mailingLists', EntityType::class, [
                'class' => MailingList::class,
                'expanded' => true,
                'multiple' => true,
                'by_reference' => false,
                'query_builder' => function (MailingListRepository $repository) {
                    return ($repository->createFindAllQueryBuilder());
                },
            ])
        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => MemberEntry::class,
            'current_locale' => null,
        ]);
    }
}
