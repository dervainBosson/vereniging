<?php

/*
 * Copyright (C) 2022 by Luc Hamers <vereniging@hamers.de>
 *
 * This file is part of Vereniging.
 *
 * Vereniging is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Vereniging is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Vereniging.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Form;

use App\Entity\MemberEntry;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * This is a helper class which is needed to provide data to the MailingListCollectionType form. The email notification
 * settings are part of the member entry, so here we can add, remove and get member entries.
 *
 * Class EmailNotificationsCollection
 */
class EmailNotificationsCollection
{
    private ArrayCollection $memberEntries;


    /**
     * EmailNotificationsCollection constructor. (the default parameters are needed for the form test)
     *
     * @param MemberEntry[] $memberEntries
     */
    public function __construct(array $memberEntries = [])
    {
        $this->memberEntries = new ArrayCollection($memberEntries);
    }


    /**
     * @return ArrayCollection|MemberEntry[]
     */
    public function getMemberEntries(): ArrayCollection
    {
        return ($this->memberEntries);
    }


    /**
     * @param MemberEntry $memberEntry
     */
    public function addMemberEntry(MemberEntry $memberEntry): void
    {
        if ($this->memberEntries->contains($memberEntry)) {
            return;
        }

        // Don't add empty entries, which are entries without
        if (0 === $memberEntry->getEmailNotifications()->count()) {
            return;
        }

        $this->memberEntries[] = $memberEntry;
    }


    /**
     * @param MemberEntry $memberEntry
     */
    public function removeMemberEntry(MemberEntry $memberEntry): void
    {
        if (!$this->memberEntries->contains($memberEntry)) {
            return;
        }

        // When the member is removed from the collection (i.e. it is no longer present in the members list with email
        // notifications), then all notifications have to be removed in this member entry.
        $notifications = $memberEntry->getEmailNotifications();
        foreach ($notifications as $notification) {
            $memberEntry->removeEmailNotification($notification);
        }
        $this->memberEntries->removeElement($memberEntry);
    }
}
