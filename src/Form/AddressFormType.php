<?php

/*
 * Copyright (C) 2022 by Luc Hamers <vereniging@hamers.de>
 *
 * This file is part of Vereniging.
 *
 * Vereniging is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Vereniging is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Vereniging.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Form;

use App\Entity\Address;
use App\Entity\AddressType;
use App\Entity\Country;
use App\Repository\AddressTypeRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Creates a form to enter a postal address with all its fields
 */
class AddressFormType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('address', null, [
                'label' => false,
            ])
            ->add('zip', null, [
                'label' => false,
            ])
            ->add('city', null, [
                'label' => false,
            ])
            ->add('addressType', EntityType::class, [
                'class' => AddressType::class,
                'required' => true,
                'label' => false,
                'query_builder' => function (AddressTypeRepository $repo) use ($options) {
                    return $repo->createOrderByTypeQueryBuilder($options['current_locale']);
                },
            ])
            ->add('country', EntityType::class, [
                'class' => Country::class,
                'required' => true,
                'label' => false,
            ])
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Address::class,
            'validation_groups' => false,
            'current_locale' => null,
        ]);
    }
}
