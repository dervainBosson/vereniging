<?php

/*
 * Copyright (C) 2022 by Luc Hamers <vereniging@hamers.de>
 *
 * This file is part of Vereniging.
 *
 * Vereniging is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Vereniging is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Vereniging.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Form;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;

/**
 * This is a helper class which is needed to provide data to forms which need to add or remove members.
 *
 * Class FormCollection
 */
class FormCollection
{
    private ArrayCollection $members;
    private ?EntityManagerInterface $entityManager;


    /**
     * FormCollection constructor. (the default parameters are needed for the form test)
     *
     * @param array                       $members
     * @param EntityManagerInterface|null $entityManager
     */
    public function __construct(array $members, ?EntityManagerInterface $entityManager)
    {
        $this->members = new ArrayCollection($members);
        $this->entityManager = $entityManager;
    }


    /**
     * @return ArrayCollection
     */
    public function getMembers(): ArrayCollection
    {
        return ($this->members);
    }


    /**
     * @param mixed $member
     */
    public function addMember($member): void
    {
        if ($this->members->contains($member)) {
            return;
        }

        if (0 === strlen($member)) {
            return;
        }

        $this->members[] = $member;
        if ($this->entityManager) {
            $this->entityManager->persist($member);
        }
    }


    /**
     * @param mixed $member
     */
    public function removeMember($member): void
    {
        if (!$this->members->contains($member)) {
            return;
        }

        $this->members->removeElement($member);
        if ($this->entityManager) {
            $this->entityManager->remove($member);
        }
    }
}
