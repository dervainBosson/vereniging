<?php

/*
 * Copyright (C) 2022 by Luc Hamers <vereniging@hamers.de>
 *
 * This file is part of Vereniging.
 *
 * Vereniging is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Vereniging is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Vereniging.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Form;

use App\Repository\CommitteeRepository;
use App\Repository\MembershipTypeRepository;
use App\Repository\StatusRepository;
use App\Repository\UserRoleRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class MemberListsSelectionType
 * This form offers selection fields by status, membership type, ... for the selection of member entry lists.
 *
 */
class MemberListsSelectionType extends AbstractType
{
    /**
     * {@inheritdoc}
    */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $selectedStatus                 = $options['selectedStatus'];
        $currentLocale                  = $options['currentLocale'];
        $statusWithMembershipNumberOnly = $options['statusWithMembershipNumberOnly'] && true;
        $builder
            ->add('status', EntityType::class, [
                'class' => 'App\Entity\Status',
                'expanded' => true,
                'multiple' => true,
                'data' => [$selectedStatus],
                'query_builder' => function (StatusRepository $repository) use ($currentLocale, $statusWithMembershipNumberOnly) {
                    return ($repository->createFindAllWithMembershipNumberQueryBuilder($currentLocale, $statusWithMembershipNumberOnly));
                },
            ])
            ->add('membershipType', EntityType::class, [
                'class' => 'App\Entity\MembershipType',
                'expanded' => true,
                'multiple' => true,
                'query_builder' => function (MembershipTypeRepository $repository) use ($currentLocale) {
                    return $repository->createFindByGroupMembershipQueryBuilder($currentLocale, false);
                },
            ])
            ->add('committee', EntityType::class, [
                'class' => 'App\Entity\Committee',
                'expanded' => true,
                'multiple' => true,
                'query_builder' => function (CommitteeRepository $repository) use ($currentLocale) {
                    return $repository->createFindAllOrderedQueryBuilder($currentLocale);
                },
            ])
            ->add('userRole', EntityType::class, [
                'class' => 'App\Entity\UserRole',
                'expanded' => true,
                'multiple' => true,
                'query_builder' => function (UserRoleRepository $repository) use ($currentLocale) {
                    return $repository->createFindAllOrderedQueryBuilder($currentLocale);
                },
            ])
            ->add('email', ChoiceType::class, [
                'choices' => [
                    'All' => 'all',
                    'Only with email' => 'with',
                    'Only without email' => 'without',
                    'Only without email in group' => 'without group',
                    ],
                'empty_data'  => null,
                'expanded' => true,
                'multiple' => false,
                'data' => 'all',
            ])
            ->add('debt', ChoiceType::class, [
                'choices' => [
                    'All' => 'all',
                    'Only with outstanding debt' => 'with_debt',
                    'Only without outstanding debt' => 'without_dept',
                ],
                'empty_data'  => null,
                'expanded' => true,
                'multiple' => false,
                'data' => 'all',
            ])
            ->add('others', MemberListsSelectionOthersType::class)
        ;
    }


    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'csrf_protection'   => false,
            'selectedStatus' => null,
            'currentLocale' => null,
            'statusWithMembershipNumberOnly' => null,
        ]);
    }
}
