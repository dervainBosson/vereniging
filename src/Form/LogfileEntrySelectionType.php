<?php

/*
 * Copyright (C) 2022 by Luc Hamers <vereniging@hamers.de>
 *
 * This file is part of Vereniging.
 *
 * Vereniging is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Vereniging is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Vereniging.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Form;

use App\Entity\ChangeType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class LogfileEntrySelectionType
 * This form offers selection fields by time range, change type, ... for the selection of logfile entries.
 *
 */
class LogfileEntrySelectionType extends AbstractType
{
    /**
     * {@inheritdoc}
    */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $selectedNumber = $options['selectedNumber'];
        $builder
            ->add('dateRange', ChoiceType::class, [
                'choices'  => array(
                    'Last day' => '1',
                    'Last week' => '7',
                    'Last month' => '31',
                    'Last year' => '365',
                ),
                'required' => false,
                'label' => false,
            ])
            ->add('numberLimit', ChoiceType::class, [
                'choices'  => array(
                    '10' => 10,
                    '50' => 50,
                    '100' => 100,
                ),
                'data' => $selectedNumber,
            ])
            ->add('searchByWhom', TextType::class, [
                'label' => false,
            ])
            ->add('searchOnWho', TextType::class, [
                'label' => false,
            ])
            ->add('changeType', EntityType::class, [
                'class' => ChangeType::class,
                'required' => false,
                'label' => false,
            ])
            ->add('searchWhat', TextType::class, [
                'label' => false,
            ]);
    }


    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'csrf_protection'   => false,
            'selectedNumber' => '10',
        ]);
    }
}
