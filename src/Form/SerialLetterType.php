<?php

/*
 * Copyright (C) 2022 by Luc Hamers <vereniging@hamers.de>
 *
 * This file is part of Vereniging.
 *
 * Vereniging is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Vereniging is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Vereniging.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Form;

use App\Entity\CommitteeFunctionMap;
use App\Entity\MemberEntry;
use App\Entity\SerialLetter;
use App\Entity\SerialLetterContent;
use App\Repository\MemberEntryRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class SerialLetterType
 */
class SerialLetterType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        // It can happen, that a member who signed a letter in the past doesn't have the rights to do so currently. By
        // adding the ids of both signing members to the dropdown list, the members who previously signed the letter are
        // now in the dropdown lists.
        /** @var SerialLetter $letter */
        $letter = $builder->getData();
        $signatureMembers = [];
        if (!is_null($letter->getFirstSignatureMember())) {
            $signatureMembers[] = $letter->getFirstSignatureMember()->getId();
        }
        if (!is_null($letter->getSecondSignatureMember())) {
            $signatureMembers[] = $letter->getSecondSignatureMember()->getId();
        }

        $builder
            ->add('serialLetterContents', CollectionType::class, [
                'entry_type' => SerialLetterContentType::class,
                // Remove numbers for each letter
                'entry_options' => array('label' => false),
                // Remove label for all contents
                'label' => false,
            ])
            ->add('firstSignatureMember', EntityType::class, [
                'class' => MemberEntry::class,
                'label' => 'Letter signed by',
                'required' => false,
                'query_builder' => function (MemberEntryRepository $repository) use ($signatureMembers) {
                    return ($repository->createFindAllWithRoleQueryBuilder('ROLE_SIGN_SERIAL_LETTERS', $signatureMembers));
                },
            ])
            ->add('secondSignatureMember', EntityType::class, [
                'class' => MemberEntry::class,
                'label' => 'Letter signed by',
                'required' => false,
                'query_builder' => function (MemberEntryRepository $repository) use ($signatureMembers) {
                    return ($repository->createFindAllWithRoleQueryBuilder('ROLE_SIGN_SERIAL_LETTERS', $signatureMembers));
                },
            ])
            ->add('signatureUseTitle', CheckboxType::class, [
                'label' => 'Use academic titles',
                'required' => false,
            ])
        ;

        // This function adds a first-/secondSignatureMembersFunction field with the committee functions for the
        // selected member. The idea for this comes from the symfony documentation
        // (https://symfony.com/doc/2.8/form/dynamic_form_modification.html#form-events-submitted-data). This has been
        // modified to this applications needs.
        $formModifier = function (FormInterface $form, $firstSecond, MemberEntry $member = null) {
            $positions = null === $member ? [] : $member->getCommitteeFunctionMaps();

            $form->add($firstSecond.'SignatureMembersFunction', EntityType::class, array(
                'class' => CommitteeFunctionMap::class,
                'label' => 'with function',
                'required' => false,
                'choices' => $positions,
            ));
        };

        // This will call the formModifier function when the form is build for the first time, i.e. when there is no
        // post data.
        $builder->addEventListener(
            FormEvents::PRE_SET_DATA,
            function (FormEvent $event) use ($formModifier) {
                $data = $event->getData();
                $formModifier($event->getForm(), 'first', $data->getFirstSignatureMember());
                $formModifier($event->getForm(), 'second', $data->getsecondSignatureMember());
            }
        );

        $builder->get('firstSignatureMember')->addEventListener(
            FormEvents::POST_SUBMIT,
            function (FormEvent $event) use ($formModifier) {
                // It's important here to fetch $event->getForm()->getData(), as
                // $event->getData() will get you the client data (that is, the ID)
                $firstSignatureMember = $event->getForm()->getData();

                // since we've added the listener to the child, we'll have to pass on
                // the parent to the callback functions!
                $formModifier($event->getForm()->getParent(), 'first', $firstSignatureMember);
            }
        );
        $builder->get('secondSignatureMember')->addEventListener(
            FormEvents::POST_SUBMIT,
            function (FormEvent $event) use ($formModifier) {
                // It's important here to fetch $event->getForm()->getData(), as
                // $event->getData() will get you the client data (that is, the ID)
                $secondSignatureMember = $event->getForm()->getData();
                // since we've added the listener to the child, we'll have to pass on
                // the parent to the callback functions!
                $formModifier($event->getForm()->getParent(), 'second', $secondSignatureMember);
            }
        );
    }


    /**
     * Order the serial letter content children by language
     *
     * @param FormView           $view
     * @param FormInterface|null $form
     * @param array|null         $options
     *
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function finishView(FormView $view, ?FormInterface $form, ?array $options): void
    {
        usort($view['serialLetterContents']->children, function (FormView $a, FormView $b) {
            /** @var SerialLetterContent $contentA */
            $contentA = $a->vars['data'];
            /** @var SerialLetterContent $contentB */
            $contentB = $b->vars['data'];

            $langA = $contentA->getLanguage()->getLanguage();
            $langB = $contentB->getLanguage()->getLanguage();

            return ($langA < $langB) ? -1 : 1;
        });
    }


    /**
     * Configures the options for this type.
     *
     * @param OptionsResolver $resolver The resolver for the options
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => SerialLetter::class,
            'csrf_protection'   => false,
        ]);
    }
}
