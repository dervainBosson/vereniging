<?php

/*
 * Copyright (C) 2022 by Luc Hamers <vereniging@hamers.de>
 *
 * This file is part of Vereniging.
 *
 * Vereniging is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Vereniging is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Vereniging.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Form;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\Mapping\MappingException;
use Exception;
use ReflectionException;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Form used for display and editing of simple entities, which can be added and deleted.
 *
 */
class ValueFormType extends AbstractType
{
    private string $entityType;
    private EntityManagerInterface$entityManager;
    private ?array $locales;
    private string $mainFieldName;


     /**
      * Add form fields to the builder depending on the data type / properties of the entity. All fields are added in an
      * event listener instead of using $builder->add because of two reasons:
      * 1. when the entity is translatable, fields must be added. To initialise these fields, the object is needed, which
      *    is only available in the event handler.
      * 2. the translation fields must be added directly after the main field, to be able to show them with a simple
      *    form output in twig. This can only be done when also all the others fields are added in the event handler.
      *
      * @param FormBuilderInterface $builder
      * @param array                $options
      *
      * @SuppressWarnings(PHPMD.UnusedFormalParameter) --> $options is needed by the inheritance, suppress is needed
      * because of a bug in PHPMD.
      *
      * @throws Exception
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $this->entityType    = $options['data_class'];
        $this->entityManager = $options['entityManager'];
        $this->locales = $options['locales'];

        // Create a form field for each field in the entity. Omit id and slug fields. Since the fields in the current
        // entities are only text or bool, only use these two types.
        $builder->addEventListener(
            FormEvents::PRE_SET_DATA,
            function (FormEvent $event) {
                $form = $event->getForm();
                $entry = $event->getData();

                $fieldNames = $this->initFieldNames();

                // Add field for main value
                $this->addMainFieldWithTranslations($form, $entry);

                // Now add all the other types
                foreach ($fieldNames as $fieldName) {
                    $this->addFormField($fieldName, $form);
                }
            }
        );
    }


    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            // The following fields are passed in via the options array on the buildForm method
            'entityType' => null,
            'entityManager' => null,
            'locales' => null,
            // This is needed to add the extra fields with the translated values.
            'allow_extra_fields' => true,
        ]);

        // The data class has to be passed in dynamically, which is only possible by using the setDefined method. By
        // this, the data_class must be set in the options which are passed to the buildForm method.
        $resolver->setDefined('data_class');
    }



    /**
     * Returns true if the entity type uses the Translatable behaviour.
     *
     * @param string $type
     *
     * @return bool
     */
    private function isTranslatable(string $type): bool
    {
        // To check if entity value must be translated, check if it used the Translatable behaviour by searching for one
        // of the properties defined there.
        return property_exists($type, 'translations');
    }


    /**
     * Return the list of field names for the current entity type. Id and slug are removed from this list. It also
     * removes the first (main) field, which is stored separately in the mainFieldName property.
     *
     * @return array
     *
     * @throws MappingException
     * @throws ReflectionException
     */
    private function initFieldNames(): array
    {
        $fieldNames = $this->entityManager->getMetadataFactory()->getMetadataFor($this->entityType)->getFieldNames();
        unset($fieldNames[array_search('id', $fieldNames)]);
        unset($fieldNames[array_search('slug', $fieldNames)]);
        // Unset left the array with non-continuous indexes. To fix this, create a new array.
        $fieldNames = array_values($fieldNames);

        // The main field is now stored in the first position of the array. Copy it to the mainFieldName property and
        // removed it from the remaining field names.
        $this->mainFieldName = $fieldNames[0];
        unset($fieldNames[0]);

        return $fieldNames;
    }


    /**
     * Add a form field, which can be either a checkbox or a text field.
     *
     * @param string        $fieldName Name of the field to add.
     * @param FormInterface $form      Form to add the form to.
     * @param string|null   $data      If the form field has to be initialised with defined data (i.e. when this is the
     *                                 main field AND we have to omit the user's locale), then a data value can be
     *                                 passed.
     *
     * @throws MappingException
     * @throws ReflectionException
     */
    private function addFormField(string $fieldName, FormInterface $form, ?string $data = null): void
    {
        if ('boolean' === $this->entityManager->getMetadataFactory()->getMetadataFor($this->entityType)->getTypeOfField($fieldName)) {
            $form->add($fieldName, CheckboxType::class, [
                'label' => false,
                'required' => false,
            ]);
        } else {
            if ($data) {
                $form->add($fieldName, TextType::class, [
                    'label' => false,
                    'required' => true,
                    'data' => $data,
                ]);
            } else {
                $form->add($fieldName, TextType::class, [
                    'label' => false,
                    'required' => true,
                ]);
            }
        }
    }


    /**
     * Add the main field and (if this entity is translatable) the translation fields for this main field.
     *
     * @param FormInterface $form   Form to which the fields are to be added.
     * @param mixed         $entity Entity which is used to initialise the translation fields.
     *
     * @throws MappingException
     * @throws ReflectionException
     */
    private function addMainFieldWithTranslations(FormInterface $form, $entity): void
    {
        if (!$this->isTranslatable($this->entityType)) {
            $this->addFormField($this->mainFieldName, $form);

            return;
        }

        $fieldGetter = 'get'.ucfirst($this->mainFieldName).'Translated';
        $data = null;
        if ($entity) {
            $data = $entity->translate($this->locales[0], false)->$fieldGetter();
            $this->addFormField($this->mainFieldName, $form, $data);
            // Add translation fields for main value (if the entity type is translatable)
            $this->addTranslationFields($form, $entity);
        } else {
            $this->addFormField($this->mainFieldName, $form);
            $this->addTranslationFields($form, $entity);
        }
    }


    /**
     * Add translation fields to the field, when the entity type is translatable.
     *
     * @param FormInterface $form   Form to which the fields are to be added.
     * @param mixed         $entity Entity which is used to initialise the translation fields.
     */
    private function addTranslationFields(FormInterface $form, $entity): void
    {
        $fieldGetter = 'get'.ucfirst($this->mainFieldName).'Translated';

        // Use a copy of the locales property, to not change that. Remove default locale, since this is
        // already covered by adding the main field.
        $localesExceptDefault = array_slice($this->locales, 1);
        foreach ($localesExceptDefault as $locale) {
            // These additional translation fields are not mapped to the entity. They will be handled as
            // extra fields in the controller and added to the translation of the entity by hand, not by
            // the form system.
            if ($entity) {
                $data = $entity->translate($locale, false)->$fieldGetter();
            } else {
                $data = null;
            }
            $form->add($this->mainFieldName.'_'.$locale, TextType::class, [
                'mapped' => false,
                'required' => false,
                'data' => $data,
                'label' => false,
            ]);
        }
    }
}
