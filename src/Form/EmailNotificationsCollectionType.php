<?php

/*
 * Copyright (C) 2022 by Luc Hamers <vereniging@hamers.de>
 *
 * This file is part of Vereniging.
 *
 * Vereniging is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Vereniging is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Vereniging.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * This form combines the email notification form for multiple member entries.
 *
 * Class EmailNotificationsCollectionType
 */
class EmailNotificationsCollectionType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('memberEntries', CollectionType::class, [
                'entry_type' => EmailNotificationsType::class,
                'entry_options' => ['label' => false],
                'by_reference' => false,
                'allow_delete' => true,
                'allow_add' => true,
                'label' => false,
            ])
            // Add a hidden field to prevent form errors when all checkboxes are cleared and therefor the form data
            // would be empty.
            ->add('empty_field', HiddenType::class, [
                'data' => 'abcdef',
                'mapped' => false,
            ])
            ;
    }


    /**
     * Configures the options for this type.
     *
     * @param OptionsResolver $resolver The resolver for the options
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => EmailNotificationsCollection::class,
            'csrf_protection'   => false,
        ]);
    }
}
