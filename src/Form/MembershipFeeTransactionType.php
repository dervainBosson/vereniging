<?php

/*
 * Copyright (C) 2022 by Luc Hamers <vereniging@hamers.de>
 *
 * This file is part of Vereniging.
 *
 * Vereniging is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Vereniging is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Vereniging.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Form;

use App\Entity\MembershipFeeTransaction;
use DateTime;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class MembershipFeeTransactionType
 */
class MembershipFeeTransactionType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('amount', MoneyType::class, [
                'label' => false,
            ])
            ->add('createDate', DateType::class, [
                'label' => false,
            ])
            ->add('closeDate', DateType::class, [
                'label' => false,
                'required' => false,
            ])
            ;

        $fee = $options['membershipFee'];
        $builder->get('amount')->addModelTransformer(new CallbackTransformer(
            function ($value) use ($fee) {
                if (!$value) {
                    return $fee;
                }

                return $value;
            },
            function ($value) {
                return $value;
            }
        ));

        $builder->get('createDate')->addModelTransformer(new CallbackTransformer(
            function ($value) {
                if (!$value) {
                    return new DateTime();
                }

                return $value;
            },
            function ($value) {
                return $value;
            }
        ));
    }


    /**
     * Configures the options for this type.
     *
     * @param OptionsResolver $resolver The resolver for the options
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => MembershipFeeTransaction::class,
            'membershipFee' => null,
        ]);
    }
}
