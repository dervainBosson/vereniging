<?php

/*
 * Copyright (C) 2022 by Luc Hamers <vereniging@hamers.de>
 *
 * This file is part of Vereniging.
 *
 * Vereniging is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Vereniging is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Vereniging.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

/**
 * This class provides a filter to capitalize the first character of a string. Other then the capitalize filter, this
 * doesn't change all the other characters.
 *
 * Class FirstLetterCapital
 */
class FirstLetterCapital extends AbstractExtension
{
    /**
     * Register the first_letter_capital filter
     *
     * @return array|TwigFilter[]
     */
    public function getFilters(): array
    {
        return [new TwigFilter('first_letter_capital', [$this, 'firstLetterCapital'])];
    }


    /**
     * Capitalize the first character of the string.
     *
     * @param string $inputString
     *
     * @return string
     */
    public function firstLetterCapital(string $inputString): string
    {
        return ucfirst($inputString);
    }
}
