<?php

/*
 * Copyright (C) 2022 by Luc Hamers <vereniging@hamers.de>
 *
 * This file is part of Vereniging.
 *
 * Vereniging is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Vereniging is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Vereniging.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Doctrine;

use App\Entity\LogEntityChangeContainer;
use App\Entity\LogfileEntry;
use App\Entity\MemberEntry;
use App\Entity\MembershipType;
use App\Service\LogEventsDisabler;
use App\Service\LogMessageCreator;
use Doctrine\Bundle\DoctrineBundle\EventSubscriber\EventSubscriberInterface;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Event\OnFlushEventArgs;
use Doctrine\ORM\Events;
use Exception;

/**
 * When changing the membership type of a member entry to a group membership, then all group members should inherit this
 * membership type.
 *
 * Class ChangeGroupMembershipTypeOnMemberListener
 */
class ChangeGroupMembershipTypeOnMemberSubscriber implements EventSubscriberInterface
{
    private LogMessageCreator $logMessageCreator;
    private LogEventsDisabler $logEventsDisabler;


    /**
     * LogEntityChangesListener constructor.
     *
     * @param LogMessageCreator $logMessageCreator
     * @param LogEventsDisabler $logEventsDisabler
     */
    public function __construct(LogMessageCreator $logMessageCreator, LogEventsDisabler $logEventsDisabler)
    {
        $this->logMessageCreator = $logMessageCreator;
        $this->logEventsDisabler = $logEventsDisabler;
    }


    /**
     *
     * @param OnFlushEventArgs $eventArgs
     *
     * @throws Exception Exceptions when logged in user is not set or the changed type is not found.
     */
    public function onFlush(OnFlushEventArgs $eventArgs): void
    {
        $entityManager = $eventArgs->getEntityManager();
        $uow = $entityManager->getUnitOfWork();

        foreach ($uow->getScheduledEntityUpdates() as $entity) {
            if ($entity instanceof MemberEntry) {
                foreach ($uow->getEntityChangeSet($entity) as $keyField => $field) {
                    // If the membership type is change to a group membership type, then update all members in the group
                    /** @var  array|MembershipType[] $field */
                    if (('membershipType' === $keyField) && ($field[1]->getGroupMembership())) {
                        $metaMember = $entityManager->getClassMetadata(MemberEntry::class);

                        foreach ($entity->getGroupMembers() as $groupMember) {
                            // Store the old content of the group member, which is needed for the log message generation
                            $oldContent = $groupMember->getEntityContent();

                            // Change and store the membership type
                            $groupMember->setMembershipType($field[1]);
                            $uow->computeChangeSet($metaMember, $groupMember);
                        }
                    }
                }
            }
        }
    }


    /**
     * @inheritdoc
     *
     * @return array
     */
    public function getSubscribedEvents(): array
    {
        return [Events::onFlush];
    }


    /**
     * Generate a log message for the changed membership type of the group member.
     *
     * @param MemberEntry              $groupMember
     * @param LogEntityChangeContainer $oldContent
     * @param EntityManagerInterface   $entityManager
     *
     * @throws Exception
     */
    private function generateLogMessage(MemberEntry $groupMember, LogEntityChangeContainer $oldContent, EntityManagerInterface $entityManager): void
    {
        $newContent = $groupMember->getEntityContent();
        $logEntryContent = $oldContent->diffObjects($newContent);
        $logEntryContent->setMainMessage("Updated %classname% entry %stringIdentifier%|memberentry,");

        if (!$this->logEventsDisabler->lifeCycleEventsAreDisabled()) {
            $logMessage = $this->logMessageCreator->createLogEntry('member data change', $logEntryContent, $groupMember, $entityManager);
            $metaLog = $entityManager->getClassMetadata(LogfileEntry::class);
            $entityManager->getUnitOfWork()->computeChangeSet($metaLog, $logMessage);
        }
    }
}
