<?php

/*
 * Copyright (C) 2022 by Luc Hamers <vereniging@hamers.de>
 *
 * This file is part of Vereniging.
 *
 * Vereniging is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Vereniging is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Vereniging.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Doctrine;

use App\Entity\AbstractComplexLogEntryBase;
use App\Entity\EntityLoggerInterface;
use App\Entity\LogEntityChangeContainer;
use App\Entity\LogEntryDataField;
use App\Entity\MemberEntry;
use App\Service\HtmlDiffer;
use App\Service\LogEventsDisabler;
use App\Service\LogMessageCreator;
use Doctrine\Bundle\DoctrineBundle\EventSubscriber\EventSubscriberInterface;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\OnFlushEventArgs;
use Doctrine\ORM\Events;
use Exception;

/**
 * Class LogEntityChangesListener
 *
 * This event subscriber listens to all entity changes for registered types (i.e. those which implement the entity
 * logger interface) and created log file entries on changes.
 *
 * On loading or creating an object, a copy of this object with its relevant fields (these are determined for each
 * entity type in the getEntityContent method) is stored in the local array $loadedEntities. When an entity is saved
 * (i.e. after a change or after creation) of after deletion, the current version is compared to the version in the
 * local array and a log message is generated.
 *
 */
class LogEntityChangesListener implements EventSubscriberInterface
{
    private LogMessageCreator $logMessageCreator;
    private array $loadedEntities;
    private LogEventsDisabler $logEventsDisabler;
    private HtmlDiffer $differ;

    /**
     * LogEntityChangesListener constructor.
     *
     * @param LogMessageCreator $logMessageCreator
     * @param LogEventsDisabler $logEventsDisabler
     * @param HtmlDiffer        $differ
     */
    public function __construct(LogMessageCreator $logMessageCreator, LogEventsDisabler $logEventsDisabler, HtmlDiffer $differ)
    {
        $this->logMessageCreator = $logMessageCreator;
        $this->logEventsDisabler = $logEventsDisabler;
        $this->differ = $differ;
        $this->loadedEntities = [];
    }


    /**
     * Store all entities which implement the entity logger interface in the entity content array, ordered by class name
     * and index.
     *
     * @param LifecycleEventArgs $args
     */
    public function postLoad(LifecycleEventArgs $args): void
    {
        $this->storeEntityCopy($args);
    }


    /**
     * Store all entities which implement the entity logger interface in the entity content array, ordered by class name
     * and index. The postPersist is needed to add new entities to the entity storage for when they are to be changed
     * directly. Adding these to the entity storage cannot be done in onFlush, because the index fields are not yet
     * calculated there.
     *
     * @param LifecycleEventArgs $args
     */
    public function postPersist(LifecycleEventArgs $args): void
    {
        $this->storeEntityCopy($args);
    }


    /**
     * @param OnFlushEventArgs $eventArgs
     *
     * @throws Exception
     */
    public function onFlush(OnFlushEventArgs $eventArgs): void
    {
        if ($this->logEventsDisabler->lifeCycleEventsAreDisabled()) {
            return;
        }

        $entityManager = $eventArgs->getEntityManager();
        $uow = $entityManager->getUnitOfWork();

        // Check for entity changes
        foreach ($uow->getScheduledEntityUpdates() as $entity) {
            if ($entity instanceof EntityLoggerInterface) {
                $currentContent = $entity->getEntityContent();
                $previousContent = $this->findOriginalContent($entity);
                $this->generateLogMessage($previousContent, $currentContent, $entityManager);
                // When this entity is updated a second time within the same call, then the entity copy should also
                // contain the updated values.
                $this->updateEntityCopy($entity);
            }
        }

        // Check for added entities
        foreach ($uow->getScheduledEntityInsertions() as $entity) {
            if ($entity instanceof EntityLoggerInterface) {
                $currentContent = $entity->getEntityContent();
                $previousContent = new LogEntityChangeContainer();
                $this->generateLogMessage($previousContent, $currentContent, $entityManager);
            }
        }

        // Check for deleted entities
        foreach ($uow->getScheduledEntityDeletions() as $entity) {
            if ($entity instanceof EntityLoggerInterface) {
                $currentContent = new LogEntityChangeContainer();
                $previousContent = $this->findOriginalContent($entity);
                $this->generateLogMessage($previousContent, $currentContent, $entityManager);
            }
        }
    }


    /**
     * @return array All subscribed events
     */
    public function getSubscribedEvents(): array
    {
        return [Events::postLoad, Events::onFlush, Events::postPersist];
    }

    /**
     * @param EntityLoggerInterface $entity
     *
     * @return null|LogEntityChangeContainer
     */
    private function findOriginalContent(EntityLoggerInterface $entity): ?LogEntityChangeContainer
    {
        $currentContent = $entity->getEntityContent();

        $className = $currentContent->getClassName();
        $index = $currentContent->getIndex();

        if (isset($this->loadedEntities[$className][$index])) {
            return $this->loadedEntities[$className][$index];
        }

        return null;
    }


    /**
     * This method generates log messages, which
     *
     * @param LogEntityChangeContainer $originalContent Array with the original content (can be empty for new entities)
     * @param LogEntityChangeContainer $currentContent  Array with the current content (can be empty for deleted entities)
     * @param EntityManagerInterface   $entityManager
     *
     * @throws Exception
     */
    private function generateLogMessage(LogEntityChangeContainer $originalContent, LogEntityChangeContainer $currentContent, EntityManagerInterface $entityManager): void
    {
        /** @var AbstractComplexLogEntryBase $logEntryContent */
        $logEntryContent = $originalContent->diffObjects($currentContent);

        // When fields are updated which are not part of the monitored class fields (i.e. not part of content in the
        // entity content array, then don't generate a log message.
        $diffData = &$logEntryContent->getDataAsArray(false);
        if (count($diffData) === 0) {
            return;
        }

        $diffData = $this->improveHtmlData($diffData);

        // Generate a matching log message depending on whether the object has been added, deleted or updated.
        $className = $currentContent->getClassName() ?? $originalContent->getClassName();
        $changeType = $currentContent->getChangeType() ?? $originalContent->getChangeType();
        $stringIdentifier = $currentContent->getStringIdentifier() ?? $originalContent->getStringIdentifier();
        $changesOnMember = null;
        $changesOnMemberId = $currentContent->getChangesOnMemberId() ?? $originalContent->getChangesOnMemberId();
        if (!is_null($changesOnMemberId)) {
            $changesOnMember = $entityManager->getRepository(MemberEntry::class)
                                                   ->find($changesOnMemberId);
        }

        $logEntryContent->setMainMessage("Updated %classname% entry %stringIdentifier%|$className,$stringIdentifier");
        if (is_null($originalContent->getClassName())) {
            $logEntryContent->setMainMessage("Created new %classname% entry %stringIdentifier%|$className,$stringIdentifier");
        } elseif (is_null($currentContent->getClassName())) {
            $logEntryContent->setMainMessage("Deleted %classname% entry %stringIdentifier%|$className,$stringIdentifier");
        }

        // Generate a log message object and add it to the current unit of work, so it is stored to the database
        $logMessage = $this->logMessageCreator->createLogEntry($changeType, $logEntryContent, $changesOnMember, $entityManager);
        $meta = $entityManager->getClassMetadata(get_class($logMessage));
        $entityManager->getUnitOfWork()->computeChangeSet($meta, $logMessage);
    }


    /**
     * @param LifecycleEventArgs $args
     */
    private function storeEntityCopy(LifecycleEventArgs $args): void
    {
        if ($this->logEventsDisabler->lifeCycleEventsAreDisabled()) {
            return;
        }

        $entity = $args->getEntity();

        if (!$entity instanceof EntityLoggerInterface) {
            return;
        }

        $this->UpdateEntityCopy($entity);
    }


    /**
     * @param EntityLoggerInterface $entity
     */
    private function updateEntityCopy(EntityLoggerInterface $entity): void
    {
        $entityContent = $entity->getEntityContent();
        $className = $entityContent->getClassName();
        $index = $entityContent->getIndex();
        $this->loadedEntities[$className][$index] = $entityContent;
    }


    /**
     * When the data contains html, then improve the content by creating a diff so only changed parts are shown.
     *
     * @param array $diffData
     *
     * @return array
     */
    private function improveHtmlData(array $diffData): array
    {
        foreach ($diffData as $key => $content) {
            // Diff is only needed when there are 2 data fields, i.e. when there is old and new data AND when the data
            // is not a LogEntryDataField, i.e. a complex field used to translate entities when they are viewed as log
            // message
            if (2 === count($content)) {
                if ($content[0] && $content[1] && !($content[0] instanceof LogEntryDataField)) {
                    $diffData[$key] = $this->differ->diff($content[0], $content[1]);
                }
            }
        }

        return $diffData;
    }
}
