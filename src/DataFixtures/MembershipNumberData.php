<?php

/*
 * Copyright (C) 2022 by Luc Hamers <vereniging@hamers.de>
 *
 * This file is part of Vereniging.
 *
 * Vereniging is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Vereniging is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Vereniging.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\DataFixtures;

use App\Entity\MembershipNumber;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

/**
 * Fixture data for the Status class
 */
class MembershipNumberData extends Fixture implements DependentFixtureInterface, FixtureGroupInterface
{
    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager): void
    {
        $number1 = $this->createMembershipNumber(1, $manager);
        $number1->setUseDirectDebit(true);
        $this->createMembershipNumber(2, $manager);
        $this->createMembershipNumber(3, $manager);
        $this->createMembershipNumber(4, $manager);
        $this->createMembershipNumber(5, $manager);
        $this->createMembershipNumber(6, $manager);
        // This is needed to check ordering in some views
        $this->createMembershipNumber(20, $manager);
        $this->createMembershipNumber(8, $manager);
        $removedNumber1 = $this->createMembershipNumber(9, $manager);
        $removedNumber1->setDateRemoved(new \DateTime());
        $removedNumber2 = $this->createMembershipNumber(10, $manager);
        $removedNumber2->setDateRemoved((new \DateTime())->modify('- 5 day'));

        $manager->flush();
    }


    /**
     * @inheritDoc
     *
     * @return array
     */
    public function getDependencies(): array
    {
        return [DisableLogListener::class];
    }


    /**
     * @inheritDoc
     *
     * @return string[]
     */
    public static function getGroups(): array
    {
        return ['default'];
    }


    /**
     * @param int           $number  Membership number to create
     * @param ObjectManager $manager Object manager
     *
     * @return MembershipNumber
     */
    private function createMembershipNumber(int $number, ObjectManager $manager): MembershipNumber
    {
        $numberObject = new MembershipNumber();
        $numberObject->setMembershipNumber($number);
        $numberObject->setUseDirectDebit(false);

        $manager->persist($numberObject);
        $this->addReference("membership_number_$number", $numberObject);

        return ($numberObject);
    }
}
