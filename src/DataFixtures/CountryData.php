<?php

/*
 * Copyright (C) 2022 by Luc Hamers <vereniging@hamers.de>
 *
 * This file is part of Vereniging.
 *
 * Vereniging is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Vereniging is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Vereniging.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\DataFixtures;

use App\Entity\Country;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

/**
 * Fixture data for the Country class
 */
class CountryData extends Fixture implements DependentFixtureInterface, FixtureGroupInterface
{
    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager): void
    {
        $country1 = new Country();
        $country1->setCountry("Germany");

        $country2 = new Country();
        $country2->setCountry("Netherlands");

        $country1->addNewTranslation('en', 'Germany');
        $country1->addNewTranslation('nl', 'Duitsland');
        $country1->addNewTranslation('de', 'Deutschland');
        $country2->addNewTranslation('en', 'Netherlands');
        $country2->addNewTranslation('nl', 'Nederland');
        $country2->addNewTranslation('de', 'Niederlande');

        // Order is reversed to test the findAll method in the repository class.
        $manager->persist($country2);
        $manager->persist($country1);
        $manager->flush();

        $this->addReference('country_germany', $country1);
        $this->addReference('country_netherlands', $country2);
    }


    /**
     * @inheritDoc
     *
     * @return array
     */
    public function getDependencies(): array
    {
        return [DisableLogListener::class];
    }


    /**
     * @inheritDoc
     *
     * @return string[]
     */
    public static function getGroups(): array
    {
        return ['default'];
    }
}
