<?php

/*
 * Copyright (C) 2022 by Luc Hamers <vereniging@hamers.de>
 *
 * This file is part of Vereniging.
 *
 * Vereniging is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Vereniging is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Vereniging.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\DataFixtures;

use App\Entity\EmailTemplate;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

/**
 * Fixture data for the Title class
 */
class MigrationData extends Fixture implements DependentFixtureInterface, FixtureGroupInterface
{
    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager): void
    {
        $migrationData = <<<'EOT'
            CREATE TABLE IF NOT EXISTS `doctrine_migration_versions` (
              `version` varchar(191) PRIMARY KEY,
              `executed_at` datetime DEFAULT NULL,
              `execution_time` int(11) DEFAULT NULL
            );
                        --
            -- Dumping data for table `doctrine_migration_versions`
                --
            
            INSERT INTO `doctrine_migration_versions` (`version`, `executed_at`, `execution_time`) VALUES
            ('DoctrineMigrations\\Version20200726104150', '2020-08-09 20:49:43', 114),
            ('DoctrineMigrations\\Version20201003184613', '2020-10-10 16:39:33', 63),
            ('DoctrineMigrations\\Version20201011165452', '2020-11-15 15:40:44', 41),
            ('DoctrineMigrations\\Version20201121184102', '2020-11-22 16:03:35', 24),
            ('DoctrineMigrations\\Version20201128213307', '2020-12-06 18:06:34', 20),
            ('DoctrineMigrations\\Version20201210203128', '2021-01-17 13:56:46', 23),
            ('DoctrineMigrations\\Version20210717152103', '2021-07-20 14:19:46', 119),
            ('DoctrineMigrations\\Version20220109160135', '2022-01-09 17:10:35', 22);
        EOT;

        $manager->getConnection()->exec($migrationData);
    }


    /**
     * @inheritDoc
     *
     * @return array
     */
    public function getDependencies(): array
    {
        return [DisableLogListener::class];
    }


    /**
     * @inheritDoc
     *
     * @return string[]
     */
    public static function getGroups(): array
    {
        return ['default'];
    }
}
