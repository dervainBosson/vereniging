<?php

/*
 * Copyright (C) 2022 by Luc Hamers <vereniging@hamers.de>
 *
 * This file is part of Vereniging.
 *
 * Vereniging is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Vereniging is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Vereniging.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\DataFixtures;

use App\Entity\ChangeType;
use App\Entity\Language;
use App\Entity\MemberEntry;
use App\Entity\MembershipType;
use App\Entity\Status;
use App\Entity\UserRole;
use App\Service\LogEventsDisabler;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

/**
 * Fixture data for the installer
 */
class InstallerFixtureData extends Fixture implements FixtureGroupInterface, DependentFixtureInterface
{
    private LogEventsDisabler $logEventsDisabler;


    /**
     * InstallerFixtureData constructor.
     *
     * @param LogEventsDisabler $logEventsDisabler
     */
    public function __construct(LogEventsDisabler $logEventsDisabler)
    {
        $this->logEventsDisabler = $logEventsDisabler;
    }


    /**
     * Load admin user into database
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager): void
    {
        $this->logEventsDisabler->disableLifecycleEvents();
        $this->addLanguages($manager);
        $this->addStatus($manager);
        $this->addMembershipType($manager);
        $this->addAdminMember($manager);
    }


    /**
     * This method must return an array of groups
     * on which the implementing class belongs to
     *
     * @return string[]
     */
    public static function getGroups(): array
    {
        return ['installerFixtures'];
    }


    /**
     * This method must return an array of fixtures classes
     * on which the implementing class depends on
     *
     * @return array
     */
    public function getDependencies(): array
    {
        return [
            DisableLogListener::class,
            UserRoleData::class,
            ChangeTypeData::class,
        ];
    }


    /**
     * @param ObjectManager $manager
     */
    private function addLanguages(ObjectManager $manager): void
    {
        $language1 = new Language();
        $language1->setLanguage("German");
        $language1->setLocale("de");
        $language1->setFlagImage("de.png");
        $language1->setUseForSerialLetters(false);
        $language1->setBabelName('ngerman');

        $language2 = new Language();
        $language2->setLanguage("Dutch");
        $language2->setLocale("nl");
        $language2->setFlagImage("nl.png");
        $language2->setUseForSerialLetters(true);

        $language3 = new Language();
        $language3->setLanguage("English");
        $language3->setLocale("en");
        $language3->setFlagImage("en.png");
        $language3->setUseForSerialLetters(true);

        $language1->addNewTranslation('en', 'German');
        $language1->addNewTranslation('nl', 'Duits');
        $language1->addNewTranslation('de', 'Deutsch');
        $language2->addNewTranslation('en', 'Dutch');
        $language2->addNewTranslation('nl', 'Nederlands');
        $language2->addNewTranslation('de', 'Niederländisch');
        $language3->addNewTranslation('en', 'English');
        $language3->addNewTranslation('nl', 'Engels');
        $language3->addNewTranslation('de', 'Englisch');

        $manager->persist($language1);
        $manager->persist($language2);
        $manager->persist($language3);
        $manager->flush();

        $this->addReference('language_german', $language1);
        $this->addReference('language_dutch', $language2);
        $this->addReference('language_english', $language3);
    }


    /**
     * @param ObjectManager $manager
     */
    private function addStatus(ObjectManager $manager): void
    {
        $status1 = new Status("member", true);

        $status1->addNewTranslation('en', 'Member');
        $status1->addNewTranslation('nl', 'Lid');
        $status1->addNewTranslation('de', 'Mitglied');

        $manager->persist($status1);
        $manager->flush();

        $this->addReference('status_member', $status1);
    }


    /**
     * @param ObjectManager $manager
     */
    private function addMembershipType(ObjectManager $manager): void
    {
        $membershipType1 = new MembershipType();
        $membershipType1->setType("regular member");
        $membershipType1->setMembershipFee(24.00);
        $membershipType1->setGroupMembership(false);
        $membershipType1->setDefaultGroupMembershipType(false);
        $membershipType1->setDefaultNewMembershipType(true);

        $membershipType1->translate('en')->setTypeTranslated('Regular member');
        $membershipType1->translate('nl')->setTypeTranslated('Regulier lid');
        $membershipType1->translate('de')->setTypeTranslated('Reguläres Mitglied');

        $manager->persist($membershipType1);
        $manager->flush();

        $this->addReference('membership_type_regular', $membershipType1);
    }


    /**
     * @param ObjectManager $manager
     */
    private function addAdminMember(ObjectManager $manager): void
    {
        $memberEntry = new MemberEntry();
        $memberEntry->setGender("n");
        $memberEntry->setName("Admin");
        $memberEntry->setFirstName("Admin");
        $memberEntry->setMembershipEndRequested(false);
        $memberEntry->setPreferredLanguage($this->getReference("language_english"));
        $memberEntry->setStatus($this->getReference("status_member"));
        $memberEntry->setMembershipType($this->getReference("membership_type_regular"));
        $memberEntry->setUserRole($this->getReference("user_role_role_system_administrator"));
        $memberEntry->setPlainPassword('top-secret');

        $manager->persist($memberEntry);
        $manager->flush();
    }
}
