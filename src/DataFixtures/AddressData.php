<?php

/*
 * Copyright (C) 2022 by Luc Hamers <vereniging@hamers.de>
 *
 * This file is part of Vereniging.
 *
 * Vereniging is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Vereniging is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Vereniging.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\DataFixtures;

use App\Entity\Address;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

/**
 * Fixture data for the AddressType class
 */
class AddressData extends Fixture implements DependentFixtureInterface, FixtureGroupInterface
{
    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager): void
    {
        $address1 = new Address();
        $address1->setAddress("MyAddress1");
        $address1->setZip("MyZip1");
        $address1->setCity("MyCity1");
        $address1->setAddressType($this->getReference("address_type_private"));
        $address1->setCountry($this->getReference("country_germany"));

        $address2 = new Address();
        $address2->setAddress("MyAddress2");
        $address2->setZip("MyZip2");
        $address2->setCity("MyCity2");
        $address2->setAddressType($this->getReference("address_type_company"));
        $address2->setCountry($this->getReference("country_netherlands"));

        // This is used to have the same address as for member1, for the group tests.
        $address3 = new Address();
        $address3->setAddress($address1->getAddress());
        $address3->setZip($address1->getZip());
        $address3->setCity($address1->getCity());
        $address3->setAddressType($address1->getAddressType());
        $address3->setCountry($address1->getCountry());

        // This is used to have the same address as for member1, for the group tests.
        $address4 = new Address();
        $address4->setAddress($address1->getAddress());
        $address4->setZip($address1->getZip());
        $address4->setCity($address1->getCity());
        $address4->setAddressType($address1->getAddressType());
        $address4->setCountry($address1->getCountry());

        $address5 = new Address();
        $address5->setAddress("MyAddress5");
        $address5->setZip("MyZip5");
        $address5->setCity("MyCity5");
        $address5->setAddressType($this->getReference("address_type_private"));
        $address5->setCountry($this->getReference("country_netherlands"));

        $address6 = new Address();
        $address6->setAddress("MyAddress6");
        $address6->setZip("MyZip6");
        $address6->setCity("MyCity6");
        $address6->setAddressType($this->getReference("address_type_private"));
        $address6->setCountry($this->getReference("country_netherlands"));

        $manager->persist($address1);
        $manager->persist($address2);
        $manager->persist($address3);
        $manager->persist($address4);
        $manager->persist($address5);
        $manager->persist($address6);
        $manager->flush();

        $this->addReference('address1', $address1);
        $this->addReference('address2', $address2);
        $this->addReference('address3', $address3);
        $this->addReference('address4', $address4);
        $this->addReference('address5', $address5);
        $this->addReference('address6', $address6);
    }


    /**
     * @inheritDoc
     *
     * @return array
     */
    public function getDependencies(): array
    {
        return [DisableLogListener::class, AddressTypeData::class, CountryData::class];
    }


    /**
     * @inheritDoc
     *
     * @return string[]
     */
    public static function getGroups(): array
    {
        return ['default'];
    }
}
