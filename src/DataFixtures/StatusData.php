<?php

/*
 * Copyright (C) 2022 by Luc Hamers <vereniging@hamers.de>
 *
 * This file is part of Vereniging.
 *
 * Vereniging is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Vereniging is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Vereniging.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use App\Entity\Status;

/**
 * Fixture data for the Status class
 */
class StatusData extends Fixture implements DependentFixtureInterface, FixtureGroupInterface
{
    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager): void
    {
        $status1 = new Status("member", true);
        $status2 = new Status("future_member", false);
        $status3 = new Status("former_member", false);

        $status1->addNewTranslation('en', 'Member');
        $status1->addNewTranslation('nl', 'Lid');
        $status1->addNewTranslation('de', 'Mitglied');
        $status2->addNewTranslation('en', 'Future member');
        $status2->addNewTranslation('nl', 'Toekomstig lid');
        $status2->addNewTranslation('de', 'Zukünftiges Mitglied');
        $status3->addNewTranslation('en', 'Former member');
        $status3->addNewTranslation('nl', 'Oudlid');
        $status3->addNewTranslation('de', 'Ehemaliges Mitglied');

        $manager->persist($status1);
        $manager->persist($status2);
        $manager->persist($status3);
        $manager->flush();

        $this->addReference('status_member', $status1);
        $this->addReference('status_future_member', $status2);
        $this->addReference('status_former_member', $status3);
    }


    /**
     * @inheritDoc
     *
     * @return array
     */
    public function getDependencies(): array
    {
        return [DisableLogListener::class];
    }


    /**
     * @inheritDoc
     *
     * @return string[]
     */
    public static function getGroups(): array
    {
        return ['default'];
    }
}
