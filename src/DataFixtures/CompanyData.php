<?php

/*
 * Copyright (C) 2022 by Luc Hamers <vereniging@hamers.de>
 *
 * This file is part of Vereniging.
 *
 * Vereniging is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Vereniging is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Vereniging.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\DataFixtures;

use App\Entity\CompanyInformation;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

/**
 * Fixture data for the CompanyInformation class
 */
class CompanyData extends Fixture implements DependentFixtureInterface, FixtureGroupInterface
{
    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager): void
    {
        $company1 = new CompanyInformation();
        $company1->setName('My Company');
        $company1->setCity('My city');
        $company1->setDescription('My description');
        $company1->setFunctionDescription('My function');
        $company1->setUrl('http://company.com');
        $manager->persist($company1);

        $company2 = new CompanyInformation();
        $company2->setName('My Company2');
        $company2->setCity('My city2');
        $company2->setDescription('My description2');
        $company2->setFunctionDescription('My function2');
        $company2->setUrl('http://company2.com');
        $manager->persist($company2);
        $manager->flush();

        $this->addReference('company1', $company1);
        $this->addReference('company2', $company2);
    }


    /**
     * @inheritDoc
    *
     * @return array
     */
    public function getDependencies(): array
    {
        return [DisableLogListener::class];
    }


    /**
     * @inheritDoc
     *
     * @return string[]
     */
    public static function getGroups(): array
    {
        return ['default'];
    }
}
