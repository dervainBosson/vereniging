<?php

/*
 * Copyright (C) 2022 by Luc Hamers <vereniging@hamers.de>
 *
 * This file is part of Vereniging.
 *
 * Vereniging is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Vereniging is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Vereniging.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\DataFixtures;

use App\Entity\MembershipFeeTransaction;
use DateTime;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

/**
 * Fixture data for the MembershipType class
 */
class MembershipFeeTransactionData extends Fixture implements DependentFixtureInterface, FixtureGroupInterface
{
    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager): void
    {
        $transaction1 = new MembershipFeeTransaction();
        $transaction1->setAmount(36);
        $transaction1->setMembershipNumber($this->getReference('membership_number_1'));
        $transaction1->setCreateDate(new DateTime('2020-11-1'));
        $transaction1->setCloseDate(new DateTime('2020-11-10'));

        $transaction2 = new MembershipFeeTransaction();
        $transaction2->setAmount(36);
        $transaction2->setMembershipNumber($this->getReference('membership_number_1'));
        $transaction2->setCreateDate(new DateTime('2020-11-28'));

        $transaction3 = new MembershipFeeTransaction();
        $transaction3->setAmount(52);
        $transaction3->setMembershipNumber($this->getReference('membership_number_1'));
        $transaction3->setCreateDate(new DateTime('2020-11-29'));

        $transaction4 = new MembershipFeeTransaction();
        $transaction4->setAmount(24);
        $transaction4->setMembershipNumber($this->getReference('membership_number_2'));
        $transaction4->setCreateDate(new DateTime('2020-11-20'));
        $transaction4->setCloseDate(new DateTime('2020-11-25'));

        $transaction5 = new MembershipFeeTransaction();
        $transaction5->setAmount(20);
        $transaction5->setMembershipNumber($this->getReference('membership_number_2'));
        $transaction5->setCreateDate(new DateTime('2020-11-28'));

        $transaction6 = new MembershipFeeTransaction();
        $transaction6->setAmount(24);
        $transaction6->setMembershipNumber($this->getReference('membership_number_3'));
        $transaction6->setCreateDate(new DateTime('2020-11-29'));

        $manager->persist($transaction1);
        $manager->persist($transaction2);
        $manager->persist($transaction3);
        $manager->persist($transaction4);
        $manager->persist($transaction5);
        $manager->persist($transaction6);
        $manager->flush();
    }


    /**
     * @inheritDoc
     *
     * @return array
     */
    public function getDependencies(): array
    {
        return [
            DisableLogListener::class,
            MembershipNumberData::class,
            MembershipTypeData::class,
        ];
    }


    /**
     * @inheritDoc
     *
     * @return string[]
     */
    public static function getGroups(): array
    {
        return ['default'];
    }
}
