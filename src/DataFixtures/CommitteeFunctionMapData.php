<?php

/*
 * Copyright (C) 2022 by Luc Hamers <vereniging@hamers.de>
 *
 * This file is part of Vereniging.
 *
 * Vereniging is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Vereniging is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Vereniging.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use App\Entity\CommitteeFunctionMap;

/**
 * Fixture data for the CommitteeFunctionMap class
 */
class CommitteeFunctionMapData extends Fixture implements DependentFixtureInterface, FixtureGroupInterface
{
    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager): void
    {
        $functionMap1 = new CommitteeFunctionMap();
        $functionMap1->setCommittee($this->getReference("committee_board"));
        $functionMap1->setCommitteeFunction($this->getReference("committee_function_chairman"));
        $functionMap1->addMemberEntry($this->getReference('member_entry_last_name1'));

        $functionMap2 = new CommitteeFunctionMap();
        $functionMap2->setCommittee($this->getReference("committee_board"));
        $functionMap2->setCommitteeFunction($this->getReference("committee_function_vice_chairman"));
        $functionMap2->addMemberEntry($this->getReference('member_entry_last_name1'));

        $functionMap3 = new CommitteeFunctionMap();
        $functionMap3->setCommittee($this->getReference("committee_board"));
        $functionMap3->setCommitteeFunction($this->getReference("committee_function_member"));
        $functionMap3->addMemberEntry($this->getReference('member_entry_last_name1'));

        $functionMap4 = new CommitteeFunctionMap();
        $functionMap4->setCommittee($this->getReference("committee_activities"));
        $functionMap4->setCommitteeFunction($this->getReference("committee_function_chairman"));
        $functionMap4->addMemberEntry($this->getReference('member_entry_last_name2'));

        $functionMap5 = new CommitteeFunctionMap();
        $functionMap5->setCommittee($this->getReference("committee_activities"));
        $functionMap5->setCommitteeFunction($this->getReference("committee_function_member"));
        $functionMap5->addMemberEntry($this->getReference('member_entry_last_name1'));

        $functionMap6 = new CommitteeFunctionMap();
        $functionMap6->setCommittee($this->getReference("committee_public_relations"));
        $functionMap6->setCommitteeFunction($this->getReference("committee_function_chairman"));

        $functionMap7 = new CommitteeFunctionMap();
        $functionMap7->setCommittee($this->getReference("committee_public_relations"));
        $functionMap7->setCommitteeFunction($this->getReference("committee_function_member"));
        $functionMap7->addMemberEntry($this->getReference('member_entry_last_name1'));
        $functionMap7->addMemberEntry($this->getReference('member_entry_last_name2'));

        $manager->persist($functionMap1);
        $manager->persist($functionMap2);
        $manager->persist($functionMap3);
        $manager->persist($functionMap4);
        $manager->persist($functionMap5);
        $manager->persist($functionMap6);
        $manager->persist($functionMap7);
        $manager->flush();

        $this->addReference('committee_function_map_board_chairman', $functionMap1);
        $this->addReference('committee_function_map_board_vice_chairman', $functionMap2);
        $this->addReference('committee_function_map_board_member', $functionMap3);
        $this->addReference('committee_function_map_activities_chairman', $functionMap4);
        $this->addReference('committee_function_map_activities_member', $functionMap5);
        $this->addReference('committee_function_map_public_relations_chairman', $functionMap6);
        $this->addReference('committee_function_map_public_relations_member', $functionMap7);
    }


    /**
     * @inheritDoc
     *
     * @return array
     */
    public function getDependencies(): array
    {
        return [DisableLogListener::class, CommitteeData::class, CommitteeFunctionData::class, MemberEntryData::class];
    }


    /**
     * @inheritDoc
     *
     * @return string[]
     */
    public static function getGroups(): array
    {
        return ['default'];
    }
}
