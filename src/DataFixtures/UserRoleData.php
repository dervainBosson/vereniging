<?php

/*
 * Copyright (C) 2022 by Luc Hamers <vereniging@hamers.de>
 *
 * This file is part of Vereniging.
 *
 * Vereniging is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Vereniging is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Vereniging.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use App\Entity\UserRole;

/**
 * Fixture data for the Role class
 */
class UserRoleData extends Fixture implements DependentFixtureInterface, FixtureGroupInterface
{
    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager): void
    {
        $role1 = new UserRole();
        $role1->setUserRole('User');
        $role1->setDefaultRole(true);
        $role1->setSystemRoles([
            'ROLE_USER',
            'ROLE_EDIT_MY_DATA',
            'ROLE_VIEW_CLUB_STRUCTURE',
            'ROLE_VIEW_COMPANY_INFORMATION',
            'ROLE_VIEW_MEMBER_LISTS',
            'ROLE_VIEW_DASHBOARD',
        ]);

        $role2 = new UserRole();
        $role2->setUserRole('Serial letter signer');
        $role2->setDefaultRole(false);
        $role2->setSystemRoles(array_merge($role1->getSystemRoles(), [
            'ROLE_SERIAL_LETTER_SIGNER',
            'ROLE_SIGN_SERIAL_LETTERS',
        ]));

        $role3 = new UserRole();
        $role3->setUserRole('Serial letter author');
        $role3->setDefaultRole(false);
        $role3->setSystemRoles(array_merge($role2->getSystemRoles(), [
            'ROLE_SERIAL_LETTER_AUTHOR',
            'ROLE_CREATE_SERIAL_LETTERS',
            'ROLE_MANAGE_SERIAL_LETTER_TEMPLATES',
            'ROLE_SEND_SERIAL_EMAILS',
        ]));

        $role4 = new UserRole();
        $role4->setUserRole('Member administrator');
        $role4->setDefaultRole(false);
        $role4->setSystemRoles(array_merge($role3->getSystemRoles(), [
            'ROLE_MEMBER_ADMINISTRATOR',
            'ROLE_CREATE_NEW_MEMBERS',
            'ROLE_EDIT_OTHER_USERS',
            'ROLE_SELECT_OTHER_USER_GROUPS',
            'ROLE_EXPORT_EXCEL',
            'ROLE_EXPORT_PDF',
            'ROLE_MANAGE_COMMITTEES',
            'ROLE_MANAGE_LIST_VALUES',
            'ROLE_MANAGE_LIST_VALUE_TRANSLATIONS',
            'ROLE_MANAGE_MEMBER_CLUB_SETTINGS',
            'ROLE_MANAGE_MAILINGLISTS',
            'ROLE_MANAGE_MEMBERSHIP_TYPES',
            'ROLE_MANAGE_SNAPSHOT_DIFFS',
            'ROLE_VIEW_ADMIN_DASHBOARD',
            'ROLE_VIEW_LOG_FILE',
            'ROLE_VIEW_STATISTICS',
            'ROLE_MANAGE_EMAIL_TEMPLATES',
        ]));

        $role5 = new UserRole();
        $role5->setUserRole('Treasurer');
        $role5->setDefaultRole(false);
        $role5->setSystemRoles(array_merge($role4->getSystemRoles(), [
            'ROLE_MANAGE_FINANCES',
        ]));
        $role6 = new UserRole();
        $role6->setUserRole('System administrator');
        $role6->setDefaultRole(false);
        $role6->setSystemRoles(array_merge($role5->getSystemRoles(), [
            'ROLE_SYSTEM_ADMINISTRATOR',
            'ROLE_IMPORT_EXCEL',
            'ROLE_MANAGE_EMAIL_NOTIFICATIONS',
            'ROLE_MANAGE_SYSTEM_BACKUP',
            'ROLE_MANAGE_USER_ROLES',
            'ROLE_MANAGE_USER_RIGHTS',
            'ROLE_MANAGE_PASSWORDS',
            'ROLE_DELETE_MEMBERS',
            'ROLE_MANAGE_SYSTEM_EMAIL_TEMPLATES',
        ]));

        $role1->translate('en')->setUserRoleTranslated('User');
        $role1->translate('nl')->setUserRoleTranslated('Gebruiker');
        $role1->translate('de')->setUserRoleTranslated('Benutzer');
        $role2->translate('en')->setUserRoleTranslated('Serial letter signer');
        $role2->translate('nl')->setUserRoleTranslated('Seriebriefondertekenaar');
        $role2->translate('de')->setUserRoleTranslated('Serienbriefunterzeichner');
        $role3->translate('en')->setUserRoleTranslated('Serial letter author');
        $role3->translate('nl')->setUserRoleTranslated('Seriebriefauteur');
        $role3->translate('de')->setUserRoleTranslated('Serienbriefautor');
        $role4->translate('en')->setUserRoleTranslated('Member administrator');
        $role4->translate('nl')->setUserRoleTranslated('Ledenadministrator');
        $role4->translate('de')->setUserRoleTranslated('Mitgliederadministrator');
        $role5->translate('en')->setUserRoleTranslated('Treasurer');
        $role5->translate('nl')->setUserRoleTranslated('Penningmeester');
        $role5->translate('de')->setUserRoleTranslated('Schatzmeister');
        $role6->translate('en')->setUserRoleTranslated('System administrator');
        $role6->translate('nl')->setUserRoleTranslated('Systeemadministrator');
        $role6->translate('de')->setUserRoleTranslated('Systemadministrator');

        $manager->persist($role1);
        $manager->persist($role2);
        $manager->persist($role3);
        $manager->persist($role4);
        $manager->persist($role5);
        $manager->persist($role6);
        $manager->flush();

        $this->addReference('user_role_role_user', $role1);
        $this->addReference('user_role_role_serial_letter_signer', $role2);
        $this->addReference('user_role_role_serial_letter_author', $role3);
        $this->addReference('user_role_role_member_administrator', $role4);
        $this->addReference('user_role_role_treasurer', $role5);
        $this->addReference('user_role_role_system_administrator', $role6);
    }


    /**
     * @inheritDoc
     *
     * @return array
     */
    public function getDependencies(): array
    {
        return [DisableLogListener::class];
    }


    /**
     * @inheritDoc
     *
     * @return string[]
     */
    public static function getGroups(): array
    {
        return ['default', 'installerFixtures'];
    }
}
