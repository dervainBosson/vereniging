<?php

/*
 * Copyright (C) 2022 by Luc Hamers <vereniging@hamers.de>
 *
 * This file is part of Vereniging.
 *
 * Vereniging is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Vereniging is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Vereniging.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use App\Entity\Committee;

/**
 * Fixture data for the Committee class
 */
class CommitteeData extends Fixture implements DependentFixtureInterface, FixtureGroupInterface
{
    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager): void
    {
        $committee1 = new Committee("board");
        $committee2 = new Committee("activities");
        $committee3 = new Committee("public relations");

        $committee1->translate('en')->setCommitteeNameTranslated('Board');
        $committee1->translate('nl')->setCommitteeNameTranslated('Bestuur');
        $committee1->translate('de')->setCommitteeNameTranslated('Vorstand');
        $committee2->translate('en')->setCommitteeNameTranslated('Activities');
        $committee2->translate('nl')->setCommitteeNameTranslated('Activiteiten');
        $committee2->translate('de')->setCommitteeNameTranslated('Veranstaltungen');
        $committee3->translate('en')->setCommitteeNameTranslated('Public relations');
        $committee3->translate('nl')->setCommitteeNameTranslated('Public relations');
        $committee3->translate('de')->setCommitteeNameTranslated('Public relations');

        $manager->persist($committee1);
        $manager->persist($committee2);
        $manager->persist($committee3);
        $manager->flush();

        $this->addReference('committee_board', $committee1);
        $this->addReference('committee_activities', $committee2);
        $this->addReference('committee_public_relations', $committee3);
    }


    /**
     * @inheritDoc
     *
     * @return array
     */
    public function getDependencies(): array
    {
        return [DisableLogListener::class];
    }


    /**
     * @inheritDoc
     *
     * @return string[]
     */
    public static function getGroups(): array
    {
        return ['default'];
    }
}
