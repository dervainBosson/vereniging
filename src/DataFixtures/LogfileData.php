<?php

/*
 * Copyright (C) 2022 by Luc Hamers <vereniging@hamers.de>
 *
 * This file is part of Vereniging.
 *
 * Vereniging is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Vereniging is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Vereniging.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\DataFixtures;

use App\Entity\ChangeType;
use App\Entity\LogfileEntry;
use App\Entity\MemberEntry;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Exception;

/**
 * Fixture data for the Logfile class
 */
class LogfileData extends Fixture implements DependentFixtureInterface, FixtureGroupInterface
{
    /**
     * {@inheritDoc}
     *
     * @throws Exception
     */
    public function load(ObjectManager $manager): void
    {
        $member1 = $this->getReference('member_entry_last_name1');
        $member2 = $this->getReference('member_entry_last_name2');
        $changeType1 = $this->getReference('change_type_member_data_change');
        $changeType2 = $this->getReference('change_type_system_settings_change');

        $this->generateLogfileEntry($manager, 'Logentry 1', $member1, $member2, $changeType1, 1);
        $this->generateLogfileEntry($manager, 'Logentry 2', null, $member1, $changeType1, 2);
        $this->generateLogfileEntry($manager, 'Logentry 3', $member1, $member2, $changeType1, 3);
        $this->generateLogfileEntry($manager, 'Logentry 4', $member1, $member2, $changeType2, 4);
        $this->generateLogfileEntry($manager, 'Logentry 5', $member1, $member2, $changeType1, 5);
        $this->generateLogfileEntry($manager, 'Logentry 6', null, $member1, $changeType1, 6);
        $this->generateLogfileEntry($manager, 'Logentry 7', $member1, $member2, $changeType1, 7);
        $this->generateLogfileEntry($manager, 'Logentry 8', $member1, $member2, $changeType2, 8);
        $this->generateLogfileEntry($manager, 'Logentry 9', $member1, $member2, $changeType1, 9);
        $this->generateLogfileEntry($manager, 'Logentry 10', null, $member1, $changeType1, 10);
        $this->generateLogfileEntry($manager, 'Logentry 11', $member1, $member2, $changeType1, 10);
        $this->generateLogfileEntry($manager, 'Logentry 12', $member1, $member2, $changeType2, 10);
        $this->generateLogfileEntry($manager, 'Logentry 13', $member1, $member2, $changeType1, 10);
        $this->generateLogfileEntry($manager, 'Logentry 14', null, $member1, $changeType1, 10);
        $this->generateLogfileEntry($manager, 'Logentry 15', $member1, $member2, $changeType1, 11);
        $this->generateLogfileEntry($manager, 'Logentry 16', $member1, $member2, $changeType2, 11);
        $this->generateLogfileEntry($manager, 'Logentry 17', $member1, $member2, $changeType1, 11);
        $this->generateLogfileEntry($manager, 'Logentry 18', null, $member1, $changeType1, 12);
        $this->generateLogfileEntry($manager, 'Logentry 19', $member1, $member2, $changeType1, 12);
        $this->generateLogfileEntry($manager, 'Logentry 20', $member1, $member2, $changeType2, 12);
        $this->generateLogfileEntry($manager, 'Logentry 21', $member1, $member2, $changeType1, 13);
        $this->generateLogfileEntry($manager, 'Logentry 22', null, $member1, $changeType1, 13);
        $this->generateLogfileEntry($manager, 'Logentry 23', $member1, $member2, $changeType1, 13);
        $this->generateLogfileEntry($manager, 'Logentry 24', $member1, $member2, $changeType2, 14);

        $this->generateLogfileEntry($manager, 'Logentry 25', $member1, $member2, $changeType2, 0, 1);
        $this->generateLogfileEntry($manager, 'Logentry 26', $member1, $member2, $changeType2, 0, 2);
        $this->generateLogfileEntry($manager, 'Logentry 27', $member1, $member2, $changeType2, 0, 7);
        $this->generateLogfileEntry($manager, 'Logentry 28', $member1, $member2, $changeType2, 0, 8);
        $this->generateLogfileEntry($manager, 'Logentry 29', $member1, $member2, $changeType2, 0, 30);
        $this->generateLogfileEntry($manager, 'Logentry 30', $member1, $member2, $changeType2, 0, 31);
        $this->generateLogfileEntry($manager, 'Logentry 31', $member1, $member2, $changeType2, 0, 32);
        $this->generateLogfileEntry($manager, 'Logentry 32', $member1, $member2, $changeType2, 0, 364);
        $this->generateLogfileEntry($manager, 'Logentry 33', $member1, $member2, $changeType2, 0, 365);
        $this->generateLogfileEntry($manager, 'Logentry 34', $member1, $member2, $changeType2, 0, 366);
    }


    /**
     * @inheritDoc
     *
     * @return array
     */
    public function getDependencies(): array
    {
        return [DisableLogListener::class, MemberEntryData::class, ChangeTypeData::class];
    }


    /**
     * @inheritDoc
     *
     * @return string[]
     */
    public static function getGroups(): array
    {
        return ['default'];
    }


    /**
     * @param ObjectManager    $manager
     * @param string           $content
     * @param MemberEntry|null $changesOn
     * @param MemberEntry|null $changedBy
     * @param ChangeType       $changeType
     * @param int              $pastMinutes
     * @param int              $pastDays
     *
     * @throws Exception
     */
    private function generateLogfileEntry(ObjectManager $manager, string $content, $changesOn, $changedBy, ChangeType $changeType, int $pastMinutes, int $pastDays = 0): void
    {
        $logfileEntry = new LogfileEntry();
        $logfileEntry->setDate((new \DateTime("now"))->modify("- $pastMinutes minute")->modify("- $pastDays day"));
        $logfileEntry->setLogentry($content);
        $logfileEntry->setChangedByMember($changedBy);
        $logfileEntry->setChangesOnMember($changesOn);
        $logfileEntry->setChangeType($changeType);

        $manager->persist($logfileEntry);
        $manager->flush();
    }
}
