<?php

/*
 * Copyright (C) 2022 by Luc Hamers <vereniging@hamers.de>
 *
 * This file is part of Vereniging.
 *
 * Vereniging is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Vereniging is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Vereniging.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\DataFixtures;

use App\Entity\MailingList;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

/**
 * Fixture data for the MailingList class
 */
class MailingListData extends Fixture implements DependentFixtureInterface, FixtureGroupInterface
{
    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager): void
    {
        $mailingLists = [];
        for ($i = 3; $i > 0; --$i) {
            $mailingList = new MailingList();
            $mailingList->setListName("MailingList_$i");
            $mailingList->setManagementEmail("mailinglist$i@mail.org");
            $mailingList->setManagementPassword("password$i");
            $manager->persist($mailingList);
            $manager->flush();

            $mailingLists[] = $mailingList;
            $this->addReference("mailinglist$i", $mailingList);
        }

        $mailingLists[0]->addMemberEntry($this->getReference('member_entry_last_name1'));
        $mailingLists[2]->addMemberEntry($this->getReference('member_entry_last_name1'));
        $manager->persist($mailingLists[0]);
        $manager->persist($mailingLists[2]);
        $manager->flush();
    }


    /**
     * @inheritDoc
     *
     * @return array
     */
    public function getDependencies(): array
    {
        return [DisableLogListener::class, MemberEntryData::class];
    }


    /**
     * @inheritDoc
     *
     * @return string[]
     */
    public static function getGroups(): array
    {
        return ['default'];
    }
}
