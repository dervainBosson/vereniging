<?php

/*
 * Copyright (C) 2022 by Luc Hamers <vereniging@hamers.de>
 *
 * This file is part of Vereniging.
 *
 * Vereniging is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Vereniging is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Vereniging.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\DataFixtures;

use App\Entity\SerialLetterContent;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

/**
 * Fixture data for the SerialLetterContent class
 */
class SerialLetterContentData extends Fixture implements DependentFixtureInterface, FixtureGroupInterface
{
    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager): void
    {
        $opening = '<span class="mceNonEditable" style="" data-variable="opening"></span>'."\n";
        $signers = '<span class="mceNonEditable" style="" data-variable="signers"></span>';
        $serialLetterContent11 = new SerialLetterContent();
        $serialLetterContent11->setSerialLetter($this->getReference("serial_letter1"));
        $serialLetterContent11->setLanguage($this->getReference("language_english"));
        $serialLetterContent11->setLetterTitle('Serial letter1');
        $serialLetterContent11->setContent("{$opening}This is a serial letter1.\nKind regards,\n$signers");

        $serialLetterContent12 = new SerialLetterContent();
        $serialLetterContent12->setSerialLetter($this->getReference("serial_letter1"));
        $serialLetterContent12->setLanguage($this->getReference("language_dutch"));
        $serialLetterContent12->setLetterTitle('Seriebrief1');
        $serialLetterContent12->setContent("{$opening}Dit is een seriebrief1.\nMet vriendelijke groeten,\n$signers");

        $serialLetterContent21 = new SerialLetterContent();
        $serialLetterContent21->setSerialLetter($this->getReference("serial_letter2"));
        $serialLetterContent21->setLanguage($this->getReference("language_english"));
        $serialLetterContent21->setLetterTitle('Serial letter2');
        $serialLetterContent21->setContent("{$opening}This is a serial letter2.\nKind regards,\n$signers");

        $serialLetterContent22 = new SerialLetterContent();
        $serialLetterContent22->setSerialLetter($this->getReference("serial_letter2"));
        $serialLetterContent22->setLanguage($this->getReference("language_dutch"));
        $serialLetterContent22->setLetterTitle('Seriebrief2');
        $serialLetterContent22->setContent("{$opening}Dit is een seriebrief2.\nMet vriendelijke groeten,\n$signers");

        $serialLetterContent31 = new SerialLetterContent();
        $serialLetterContent31->setSerialLetter($this->getReference("serial_letter3"));
        $serialLetterContent31->setLanguage($this->getReference("language_english"));
        $serialLetterContent31->setLetterTitle('Serial letter3');
        $serialLetterContent31->setContent("{$opening}This is a serial letter3.\nKind regards,\n$signers");

        $serialLetterContent32 = new SerialLetterContent();
        $serialLetterContent32->setSerialLetter($this->getReference("serial_letter3"));
        $serialLetterContent32->setLanguage($this->getReference("language_dutch"));
        $serialLetterContent32->setLetterTitle('Seriebrief3');
        $serialLetterContent32->setContent("{$opening}Dit is een seriebrief3.\nMet vriendelijke groeten,\n$signers");

        $manager->persist($serialLetterContent11);
        $manager->persist($serialLetterContent12);
        $manager->persist($serialLetterContent21);
        $manager->persist($serialLetterContent22);
        $manager->persist($serialLetterContent31);
        $manager->persist($serialLetterContent32);
        $manager->flush();

        $this->addReference('serial_letter_content11', $serialLetterContent11);
        $this->addReference('serial_letter_content12', $serialLetterContent12);
        $this->addReference('serial_letter_content21', $serialLetterContent21);
        $this->addReference('serial_letter_content22', $serialLetterContent22);
        $this->addReference('serial_letter_content31', $serialLetterContent31);
        $this->addReference('serial_letter_content32', $serialLetterContent32);
    }


    /**
     * @inheritDoc
    *
     * @return array
     */
    public function getDependencies(): array
    {
        return [DisableLogListener::class, SerialLetterData::class, LanguageData::class];
    }


    /**
     * @inheritDoc
     *
     * @return string[]
     */
    public static function getGroups(): array
    {
        return ['default'];
    }
}
