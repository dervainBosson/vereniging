<?php

/*
 * Copyright (C) 2022 by Luc Hamers <vereniging@hamers.de>
 *
 * This file is part of Vereniging.
 *
 * Vereniging is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Vereniging is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Vereniging.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use App\Entity\ChangeType;

/**
 * Fixture data for the AddressType class
 */
class ChangeTypeData extends Fixture implements DependentFixtureInterface, FixtureGroupInterface
{
    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager): void
    {
        $changeType1 = new ChangeType();
        $changeType1->setChangeType("member data change");

        $changeType2 = new ChangeType();
        $changeType2->setChangeType("member settings change");

        $changeType3 = new ChangeType();
        $changeType3->setChangeType("member free text");

        $changeType4 = new ChangeType();
        $changeType4->setChangeType("member added");

        $changeType5 = new ChangeType();
        $changeType5->setChangeType("member moved");

        $changeType6 = new ChangeType();
        $changeType6->setChangeType("system settings change");

        $changeType7 = new ChangeType();
        $changeType7->setChangeType("export action");

        $changeType8 = new ChangeType();
        $changeType8->setChangeType("import action");

        $changeType9 = new ChangeType();
        $changeType9->setChangeType("error");

        $changeType10 = new ChangeType();
        $changeType10->setChangeType("login");

        $changeType11 = new ChangeType();
        $changeType11->setChangeType("login error");

        $changeType12 = new ChangeType();
        $changeType12->setChangeType("serial letter change");

        $changeType1->translate('en')->setChangeTypeTranslated('Member data change');
        $changeType1->translate('nl')->setChangeTypeTranslated('Verandering ledengegevens');
        $changeType1->translate('de')->setChangeTypeTranslated('Änderung Mitgliederdaten');
        $changeType2->translate('en')->setChangeTypeTranslated('Member settings change');
        $changeType2->translate('nl')->setChangeTypeTranslated('Verandering ledeninstellingen');
        $changeType2->translate('de')->setChangeTypeTranslated('Änderung Mitgliedereinstellungen');
        $changeType3->translate('en')->setChangeTypeTranslated('Member free text');
        $changeType3->translate('nl')->setChangeTypeTranslated('Lid vrije tekst');
        $changeType3->translate('de')->setChangeTypeTranslated('Mitglied Freitext');
        $changeType4->translate('en')->setChangeTypeTranslated('Member added');
        $changeType4->translate('nl')->setChangeTypeTranslated('Lid toegevoegd');
        $changeType4->translate('de')->setChangeTypeTranslated('Mitglied hinzugefügt');
        $changeType5->translate('en')->setChangeTypeTranslated('Member moved');
        $changeType5->translate('nl')->setChangeTypeTranslated('Lid verschoven');
        $changeType5->translate('de')->setChangeTypeTranslated('Mitglied verschoben');
        $changeType6->translate('en')->setChangeTypeTranslated('System settings change');
        $changeType6->translate('nl')->setChangeTypeTranslated('Verandering systeeminstellingen');
        $changeType6->translate('de')->setChangeTypeTranslated('Änderung Systemeinstellungen');
        $changeType7->translate('en')->setChangeTypeTranslated('Export action');
        $changeType7->translate('nl')->setChangeTypeTranslated('Exportactie');
        $changeType7->translate('de')->setChangeTypeTranslated('Exportaktion');
        $changeType8->translate('en')->setChangeTypeTranslated('Import action');
        $changeType8->translate('nl')->setChangeTypeTranslated('Importactie');
        $changeType8->translate('de')->setChangeTypeTranslated('Importaktion');
        $changeType9->translate('en')->setChangeTypeTranslated('Error');
        $changeType9->translate('nl')->setChangeTypeTranslated('Fout');
        $changeType9->translate('de')->setChangeTypeTranslated('Fehler');
        $changeType10->translate('en')->setChangeTypeTranslated('Login');
        $changeType10->translate('nl')->setChangeTypeTranslated('Login');
        $changeType10->translate('de')->setChangeTypeTranslated('Login');
        $changeType11->translate('en')->setChangeTypeTranslated('Login error');
        $changeType11->translate('nl')->setChangeTypeTranslated('Loginfout');
        $changeType11->translate('de')->setChangeTypeTranslated('Loginfehler');
        $changeType12->translate('en')->setChangeTypeTranslated('Serial letter change');
        $changeType12->translate('nl')->setChangeTypeTranslated('Verandering aan seriebrieven');
        $changeType12->translate('de')->setChangeTypeTranslated('Änderung an Serienbriefe');

        $manager->persist($changeType1);
        $manager->persist($changeType2);
        $manager->persist($changeType3);
        $manager->persist($changeType4);
        $manager->persist($changeType5);
        $manager->persist($changeType6);
        $manager->persist($changeType7);
        $manager->persist($changeType8);
        $manager->persist($changeType9);
        $manager->persist($changeType10);
        $manager->persist($changeType11);
        $manager->persist($changeType12);
        $manager->flush();

        $this->addReference('change_type_member_data_change', $changeType1);
        $this->addReference('change_type_member_settings_change', $changeType2);
        $this->addReference('change_type_member_free_text', $changeType3);
        $this->addReference('change_type_member_added', $changeType4);
        $this->addReference('change_type_member_member_moved', $changeType5);
        $this->addReference('change_type_system_settings_change', $changeType6);
        $this->addReference('change_type_export_action', $changeType7);
        $this->addReference('change_type_import_action', $changeType8);
        $this->addReference('change_type_error', $changeType9);
    }


    /**
     * @inheritDoc
    *
     * @return array
     */
    public function getDependencies(): array
    {
        return [DisableLogListener::class];
    }


    /**
     * @inheritDoc
     *
     * @return string[]
     */
    public static function getGroups(): array
    {
        return ['default', 'installerFixtures'];
    }
}
