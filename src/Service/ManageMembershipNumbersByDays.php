<?php

/*
 * Copyright (C) 2022 by Luc Hamers <vereniging@hamers.de>
 *
 * This file is part of Vereniging.
 *
 * Vereniging is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Vereniging is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Vereniging.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Service;

use App\Entity\MemberEntry;
use App\Entity\MembershipNumber;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\QueryBuilder;
use Exception;

/**
 * Class MembershipNumberEnumerator
 */
class ManageMembershipNumbersByDays implements ManageMembershipNumberInterface
{
    private int $recycleTimeDays;
    private EntityManagerInterface $entityManager;


    /**
     * MembershipNumberEnumerator constructor.
     *
     * @param int                    $recycleTimeDays Defines the number of days after which an unused member number can
     *                                                be reused.
     * @param EntityManagerInterface $entityManager   Entity manager
     */
    public function __construct(int $recycleTimeDays, EntityManagerInterface $entityManager)
    {
        $this->recycleTimeDays = $recycleTimeDays;
        $this->entityManager = $entityManager;
    }


    /**
     * Add a membership number to a given member entry. There are 3 different situations:
     * 1. If the member is part of a group, then set the membership number to that of the other group members
     * 2. If there is an unused number in the database of which the blocking period has expired, set this
     * 3. Set the next higher free number above the numbers already in use
     *
     * @param MemberEntry $memberEntry MemberEntry to which a new membership number is added
     *
     * @throws Exception
     */
    public function addMembershipNumber(MemberEntry $memberEntry): void
    {
        // If there are group members, return the membership number of the first group member
        $groupMembers = $memberEntry->getGroupMembers();
        if (count($groupMembers)) {
            $memberEntry->setMembershipNumber($groupMembers[0]->getMembershipNumber());

            return;
        }

        // When there is an unused membership number of which the blocking period has expired, recycle this number.
        $qb = $this->entityManager->getRepository(MembershipNumber::class)
                                  ->createQueryBuilder('m');
        $result = $qb->where('m.dateRemoved < :date')
                     ->setParameter('date', (new DateTime())->modify("-$this->recycleTimeDays day"))
                     ->setMaxResults(1)
                     ->getQuery()
                     ->getOneOrNullResult();

        if ($result) {
            /** @var MembershipNumber $result */
            $result->setDateRemoved(null);
            $memberEntry->setMembershipNumber($result);

            return;
        }

        // No matching membership numbers were found, so a new number has to be created
        $lastNumberEntry = $this->entityManager->getRepository(MembershipNumber::class)
                                               ->findOneBy([], ['membershipNumber' => 'DESC']);
        // When there already are entries in the database, create an entry with the highest number + 1, or 1 if the
        // table is empty.
        $lastNumber = 1;
        if ($lastNumberEntry) {
            $lastNumber = $lastNumberEntry->getMembershipNumber() + 1;
        }

        $newNumber = new MembershipNumber();
        $newNumber->setMembershipNumber($lastNumber);
        $newNumber->setUseDirectDebit(false);
        $memberEntry->setMembershipNumber($newNumber);
    }


    /**
     * Remove a membership number from a given member entry. There are two different situations:
     * 1. This member is part of a member group and the membership number is still in use by other group members. Then
     *    the membership number is removed from the member entry, but it is not changed in the database.
     * 2. The membership number is only used by the given member entry. Then the number is removed from the member entry
     *    and the remove date for the membership number is set to today, indicating that the number can be reused in the
     *    future.
     * 3. Removing the membership number from a member entry which has no number should not generate an exception.
     *
     * @param MemberEntry $memberEntry
     *
     * @throws Exception
     */
    public function removeMembershipNumber(MemberEntry $memberEntry): void
    {
        $membershipNumber = $memberEntry->getMembershipNumber();

        if (is_null($membershipNumber)) {
            return;
        }

        if ($membershipNumber->getMemberEntries()->count() === 1) {
            $memberEntry->getMembershipNumber()->setDateRemoved(new DateTime());
            $memberEntry->setMembershipNumber();
        }

        $memberEntry->setMembershipNumber();
    }
}
