<?php

/*
 * Copyright (C) 2022 by Luc Hamers <vereniging@hamers.de>
 *
 * This file is part of Vereniging.
 *
 * Vereniging is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Vereniging is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Vereniging.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Service;

/**
 * This service class converts html files into latex files
 *
 * Class Html2Latex
 */
class Html2Latex
{
    /**
     * The method converts an html string into a latex string.
     *
     * @param string $htmlInputString HTML string to convert.
     *
     * @return string
     */
    public function convert(string $htmlInputString): string
    {
        $returnString = $htmlInputString;

        $returnString = $this->replaceUrl($returnString);
        $returnString = $this->replaceImage($returnString);

        // Replace " by latex quotes. Since this will also replace marks in the html like style, we have to replace only
        // those quotes which are between opening and closing quotes, e.g. between <p> and </p>.
        preg_match_all('#<(p|td|li)[^>]*>(.*)</\1>#U', $returnString, $hits);
        // $hits[0] contains all text including the surrounding html tags, $hits[2] without.
        foreach ($hits[2] as $orig) {
            // Replace the quotes withing the html tags
            $new = str_replace('"', "''", $orig);
            // Then replace this part of the text in the original string
            $returnString = str_replace($orig, $new, $returnString);
        }
        // Now replace ''some text'' by „some text“.
        $returnString = $this->replaceStartEndTag(
            $returnString,
            "''",
            "''",
            "„",
            "“"
        );

        // Replace <p style="text-align: center;">bla bla</p> by \begin{center}bla bla\end{center}
        $returnString = $this->replaceStartEndTag(
            $returnString,
            "<p style=\"text-align: center;\">",
            "</p>",
            '\begin{center}',
            '\end{center}'
        );

        // Replace <p style="text-align: left;">bla bla</p> by \begin{flushleft}bla bla\end{flushleft}
        $returnString = $this->replaceStartEndTag(
            $returnString,
            "<p style=\"text-align: left;\">",
            "</p>",
            '\begin{flushleft}',
            '\end{flushleft}'
        );

        // Replace <p style="text-align: right;">bla bla</p> by \begin{flushright}bla bla\end{flushright}
        $returnString = $this->replaceStartEndTag(
            $returnString,
            "<p style=\"text-align: right;\">",
            "</p>",
            '\begin{flushright}',
            '\end{flushright}'
        );

        // Remove remaining paragraph styles
        preg_match_all("#<p (.*)>#U", $returnString, $hits);
        // $hits[0] contains all text including the surrounding replace tags, $hits[1] without.
        foreach ($hits[1] as $orig) {
            $returnString = str_replace("<p ".$orig.">", "<p>", $returnString);
        }

        // Replace the space(s) between a euro symbol and a digit (or vice versa) by a fixed space
        preg_match_all('/((&euro;)([\s]*)([\d]))|(([\d])([\s]*)(&euro;))/U', $returnString, $hits);
        for ($i = 0; $i < count($hits[0]); ++$i) {
            // Replace spaces when they are after the euro symbol (first half of regex). \, is a special small space.
            $returnString = str_replace(
                $hits[2][$i].$hits[3][$i].$hits[4][$i],
                $hits[2][$i].'\,'.$hits[4][$i],
                $returnString
            );
            // Replace spaces when they are before the euro symbol (second half of regex). \, is a special small space.
            $returnString = str_replace(
                $hits[6][$i].$hits[7][$i].$hits[8][$i],
                $hits[6][$i].'\,'.$hits[8][$i],
                $returnString
            );
        }

        $returnString = str_replace("<strong>", "\\textbf{", $returnString);
        $returnString = str_replace("</strong>", "}", $returnString);
        $returnString = str_replace("<em>", "\\textit{", $returnString);
        $returnString = str_replace("</em>", "}", $returnString);
        $returnString = str_replace("<sub>", '$_{', $returnString);
        $returnString = str_replace("</sub>", '}$', $returnString);
        $returnString = str_replace("<sup>", '$^{', $returnString);
        $returnString = str_replace("</sup>", '}$', $returnString);

        $returnString = str_replace("<ol>", '\begin{enumerate}[noitemsep]', $returnString);
        $returnString = str_replace("</ol>", '\end{enumerate}', $returnString);
        $returnString = str_replace("<ul>", '\begin{itemize}[noitemsep]', $returnString);
        $returnString = str_replace("</ul>", '\end{itemize}', $returnString);
        $returnString = str_replace("<li>", "\item ", $returnString);
        $returnString = str_replace("</li>\n", "\n", $returnString);
        $returnString = str_replace("</li>", "", $returnString);

        $returnString = str_replace("<p>", "", $returnString);
        $returnString = str_replace("</p>", "\n\n", $returnString);
        $returnString = str_replace("<br />\n", "\\newline \n", $returnString);
        $returnString = str_replace("<br />", "\\newline ", $returnString);
        $returnString = str_replace("\n\\newline", "\n ", $returnString);
        $returnString = str_replace("<!--newpage-->", "\\newpage ", $returnString);
        $returnString = str_replace("&nbsp;", "~", $returnString);

        $returnString = str_replace("&euro;", '\euro{}', $returnString);
        $returnString = str_replace("&acute;", "'", $returnString);
        $returnString = str_replace("&lowbar;", '\_', $returnString);

        // Replace several newlines by just a single one
        $returnString = preg_replace('#\\\\newline(\s|~)*\\\\newline#', '\newline ', $returnString);
        // In case the first call removes a \newline ~ \newline, we have to call again
        $returnString = preg_replace('#\\\\newline(\s|~)*\\\\newline#', '\newline ', $returnString);
        // Remove a newline at the start of a line
        $returnString = preg_replace('#^\s*\\\\newline#', '', $returnString);
        // Remove a newline at the end of a line
        $returnString = preg_replace('#\\\\newline\s*$#', '', $returnString);

        // The following replacement must be done before the table conversion!
        $returnString = str_replace("&amp;", "\&", $returnString);
        $returnString = $this->searchAndConvertTables($returnString);

        $returnString = html_entity_decode($returnString, ENT_NOQUOTES, "UTF-8");
        $returnString = htmlspecialchars_decode($returnString);

        // The following characters have to be converted from their utf8-value to the latex-value, to ensure that latex
        // processes them without errors.
        $returnString = str_replace("°", "$^{\circ}$", $returnString);
        $returnString = str_replace("²", "$^2$", $returnString);
        // Remove utf8 newline character (0xE2 0x80 0xA8, \u2028)
        $unicodeChar = '\u2028';
        $returnString = str_replace(json_decode('"'.$unicodeChar.'"'), "xxx", $returnString);

        return ($returnString);
    }


    /**
     * This method dissects a html table into a php array which contains the content and the format information.
     *
     * @param string $tableString String which starts with '<table>' and ends with '</table>.
     *
     * @return array Array with the table information.
     */
    private function dissectTable(string $tableString): array
    {
        // Split up the table into its rows. $rows[0] contains the complete row strings <tr>...</tr>.
        preg_match_all("#<tr(.*)</tr>#sU", $tableString, $rows);

        $resultTable = [];
        foreach ($rows[0] as $row) {
            // Split up the row into its columns. $columns[1] contains all date between <td and > (i.e. style info),
            // $columns[2] contains all data between <td> and </td>.
            preg_match_all('#<t[dh](.*)>(.*)</t[dh]>#sU', $row, $columns);
            $resultRow = [];
            for ($column = 0; $column < count($columns[0]); ++$column) {
                $cell = [];
                $cell['content'] = $columns[2][$column];
                // $columns[1][$column] contains the style sheet (if any), e.g. ' style="width: 15cm;"'. The following
                // filters out everything outside of the quotes.
                $tdAttribute = $columns[1][$column];
                if ('' === $tdAttribute) {
                    $cell['style'] = [];
                } else {
                    preg_match_all('# style=\"(.*)\"#', $columns[1][$column], $style);
                    // $style[1][0] contains every style information between " and ". Now we split these up.
                    preg_match_all('#((.*?): (.*?);)+?#', $style[1][0], $style);
                    for ($i = 0; $i < count($style[2]); ++$i) {
                        $cell['style'][$style[2][$i]] = $style[3][$i];
                    }
                }
                $resultRow[] = $cell;
            }
            $resultTable[] = $resultRow;
        }

        return ($resultTable);
    }


    /**
     * Convert an html table into a latex table
     *
     * @param string $tableString String containing the complete html table.
     *
     * @return string
     */
    private function htmlTable2LatexTable(string $tableString): string
    {
        // Transform the html table into a php table with the content and the style information.
        $table = $this->dissectTable($tableString);

        $latex = "\begin{longtable}{";

        // Use the tables first line to extract the style information
        foreach ($table[0] as $column) {
            $format = $column['style'];
            // When there is a column width information, extract the size information from e.g. 'width: 15cm'
            if (array_key_exists('width', $format)) {
                $latex .= "p{".$format['width']."} ";
            } elseif (array_key_exists('text-align', $format)) {
                switch ($format['text-align']) {
                    case "left":
                        $latex .= "l ";
                        break;
                    case "center":
                        $latex .= "c ";
                        break;
                    case "right":
                        $latex .= "r ";
                        break;
                    default:
                        $latex .= "l ";
                }
            } else {
                $latex .= "l ";
            }
        }
        $latex .= "}\n";

        for ($row = 0; $row < count($table); ++$row) {
            // Last element is written separately because of the line end.
            for ($column = 0; $column < count($table[0]) - 1; ++$column) {
                $latex .= $table[$row][$column]['content']." & ";
            }
            $latex .= $table[$row][count($table[0]) - 1]['content']."\\\\ \n";
        }

        $latex .= "\\end{longtable}\n\n";

        return ($latex);
    }


    /**
     * Search input string for tables and replace them by a latex table
     *
     * @param string $completeText HTML string to convert
     *
     * @return string
     */
    private function searchAndConvertTables(string $completeText): string
    {
        // Search for all tables. These are written to the $tables[0] array. This contains again an array with the found
        // tables and the offset in the string. Add s to include newlines in the search
        preg_match_all("#<table(.*)</table>#sU", $completeText, $tables, PREG_OFFSET_CAPTURE);

        // $table[0] contains the complete table string, $table[1] the offset in the string. Since the offset is valid
        // for the string before it is replaced by latex commands, we have to replace the arrays back to front.
        foreach (array_reverse($tables[0]) as $table) {
            // Convert html table to a latex table
            $latexTable = $this->htmlTable2LatexTable($table[0]);

            // Replace the html table by the latex table in the return string
            $completeText = substr_replace($completeText, $latexTable, $table[1], strlen($table[0]));
        }

        return ($completeText);
    }


    /**
     * Search for a given string part $StartReplaceTag..bla bla...$EndReplaceTag and replace it by its LaTeX counterpart
     * $StartReplacement..bla bla...$EndReplacement.
     *
     * @param string $inputString      HTML string in which the replacements are done.
     * @param string $startReplaceTag  Start html part
     * @param string $endReplaceTag    End html part
     * @param string $startReplacement Start latex part
     * @param string $endReplacement   End latex part
     *
     * @return string String, in which the found patterns have been replaced.
     */
    private function replaceStartEndTag(string $inputString, string $startReplaceTag, string $endReplaceTag, string $startReplacement, string $endReplacement): string
    {
        // Search for the text between start and end replace tag
        preg_match_all("#$startReplaceTag(.*)$endReplaceTag#U", $inputString, $hits);
        // $hits[0] contains all text including the surrounding replace tags, $hits[1] without.
        foreach ($hits[1] as $orig) {
            $inputString = str_replace(
                $startReplaceTag.$orig.$endReplaceTag,
                $startReplacement.$orig.$endReplacement,
                $inputString
            );
        }

        return ($inputString);
    }


    /**
     * Search from the style sheet information os an image how it should be aligned.
     *
     * @param string $styleValue String containing the style steet for the image.
     *
     * @return string '', 'center', 'left-float', 'right-float'
     */
    private function getImageAlignment(string $styleValue): string
    {
        // If there is an auto value for left and right margin, center the image
        if ((preg_match('#margin-left:\s*auto;#', $styleValue)) &&
            (preg_match('#margin-right:\s*auto;#', $styleValue))) {
            return 'center';
        }

        if (preg_match('#float:\s*left;#', $styleValue)) {
            return 'float-left';
        }

        if (preg_match('#float:\s*right;#', $styleValue)) {
            return 'float-right';
        }

        return '';
    }


    /**
     * Replace an html link (i.e. <a href...>) by its LaTeX hyperref pendant
     *
     * @param string $inputString Text string in which the html links have to be replaced by their hyperref pendants.
     *
     * @return string Corrected text.
     */
    private function replaceImage(string $inputString): string
    {
        // Search for the elements in strings like <img scr="xxx" alt="yyy" width="20%"/>. The information is stored in hits:
        // $hits[1]: style part with optional trailing spaces (if available)
        // $hits[2]: style content
        // $hits[3]: picture address
        // $hits[4]: alt part with optional trailing spaces (if available)
        // $hits[5]: alt content
        // $hits[6]: width part with optional trailing spaces (if available)
        // $hits[7]: width content without %
        // $hits[8]: trailing spaces
        $pattern = "#<img(\s+style=\"([^\"]*)\")? src=\"([^\"]*)\"(\s+alt=\"([^\"]*)\")?(\s+width=\"([^\"]*)\")?(\s*)/>#U";
        preg_match_all($pattern, $inputString, $hits);

        for ($i = 0; $i < count($hits[1]); ++$i) {
            // Create image string without any unnecessary spaces.
            $replaceStr = "<img".$hits[1][$i]." src=\"".$hits[3][$i]."\"".$hits[4][$i].$hits[6][$i].$hits[8][$i]."/>";
            $size = 1;

            $widthValue = $hits[7][$i];
            // When there is a size value in %, then
            if (strpos($widthValue, '%') !== false) {
                // Remove %
                $sizeValue = substr($widthValue, 0, -1) / 100;

                if (($sizeValue > 0) && ($sizeValue < 1)) {
                    $size = $sizeValue;
                }
            }

            $graphicsStr = "\includegraphics[width=$size\\textwidth]{".$hits[3][$i]."}";
            $styleValue = $this->getImageAlignment($hits[2][$i]);

            switch ($styleValue) {
                case 'center':
                    $graphicsStr = "\\begin{center}$graphicsStr\\end{center}";
                    break;
                case 'float-left':
                    $graphicsStr = '\parpic[l]{'.$graphicsStr.'}';
                    break;
                case 'float-right':
                    $graphicsStr = '\parpic[r]{'.$graphicsStr.'}';
                    break;
            }

            $inputString = str_replace($replaceStr, $graphicsStr, $inputString);
        }

        return ($inputString);
    }


    /**
     * Replace an html link (i.e. <a href...>) by its LaTeX hyperref pendant
     *
     * @param string $inputString Text string in which the html links have to be replaced by their hyperref pendants.
     *
     * @return string Corrected text.
     */
    private function replaceUrl(string $inputString): string
    {
        // Search for the elements xxx yyy in strings like <a title="title" href="xxx" target=\"_blank\">yyy</a>. $hits[1]
        // contains a title tag (if found), $hits[2] contains the url, $hits[3] the target tag (if found) and $hits[4] the
        // description.
        preg_match_all("#<a(.*) href=\"(.*)\"(.*)?>(.*)</a>#U", $inputString, $hits);

        for ($i = 0; $i < count($hits[2]); ++$i) {
            $addHttps = true;
            // Check if the URL has been given with http://.
            if (strpos($hits[2][$i], "http://") !== false) {
                $addHttps = false;
            }
            // Check if the URL has been given with http://.
            if (strpos($hits[2][$i], "https://") !== false) {
                $addHttps = false;
            }
            // Check if the URL has been given with mailto:.
            if (strpos($hits[2][$i], "mailto:") !== false) {
                $addHttps = false;
            }

            $replaceStr = "\href{";
            if ($addHttps) {
                $replaceStr .= "https://";
            }
            $replaceStr .= $hits[2][$i]."}{".$hits[4][$i]."}";
            $inputString = str_replace(
                "<a".$hits[1][$i]." href=\"".$hits[2][$i]."\"".$hits[3][$i].">".$hits[4][$i]."</a>",
                $replaceStr,
                $inputString
            );
        }

        return ($inputString);
    }
}
