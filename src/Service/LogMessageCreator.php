<?php

/*
 * Copyright (C) 2022 by Luc Hamers <vereniging@hamers.de>
 *
 * This file is part of Vereniging.
 *
 * Vereniging is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Vereniging is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Vereniging.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Service;

use App\Entity\AbstractComplexLogEntryBase;
use App\Entity\ChangeType;
use App\Entity\LogfileEntry;
use App\Entity\MemberEntry;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Component\Security\Core\Security;

/**
 * Create a log file entry.
 *
 * Class LogMessageCreator
 */
class LogMessageCreator
{
    private EntityManagerInterface $entityManager;
    private EmailNotificationSender $emailNotificationSender;
    private Security $security;
    private static ?MemberEntry $loggedInUserForTests = null;
    private static ?MemberEntry $currentUser = null;
    private bool $disableEmailNotification;


    /**
     * LogMessageCreator constructor.
     *
     * @param EntityManagerInterface  $entityManager
     * @param EmailNotificationSender $notificationSender
     * @param Security                $security
     */
    public function __construct(EntityManagerInterface $entityManager, EmailNotificationSender $notificationSender, Security $security)
    {
        $this->entityManager = $entityManager;
        $this->emailNotificationSender = $notificationSender;
        $this->disableEmailNotification = false;
        $this->security = $security;
    }


    /**
     * Set the user which is currently logged in and who is the ChangedByMember member in the log messages.
     *
     * @param MemberEntry|null $loggedInUser
     */
    public function setUser(?MemberEntry $loggedInUser): void
    {
        self::$currentUser = $loggedInUser;
    }


    /**
     * Remove the current user
     */
    public static function resetUser(): void
    {
        self::$currentUser = null;
    }


    /**
     * Call this to disable sending emails for all future log messages.
     */
    public function disableEmailNotifications(): void
    {
        $this->disableEmailNotification = true;
    }


    /**
     * Call this to enable sending emails for all future log messages.
     */
    public function enableEmailNotifications(): void
    {
        $this->disableEmailNotification = false;
    }


    /**
     * Create a new log message which contains all the changes in entities and write the message to the database. Before
     * any changes are made to the member entity and its referenced fields, the method storeEntityCopy on the
     * $changesOnMember has to be called.
     *
     * @param string                             $changeTypeString Change type of the expected entities change
     * @param AbstractComplexLogEntryBase|string $content          Complex log content or string
     * @param MemberEntry|null                   $changesOnMember  Member on which the changes are done.
     * @param EntityManagerInterface|null        $entityManager    When using a special entity manager (e.g. in doctrine
     *                                                             event listeners, this manager can be passed here to
     *                                                             use instead of the one passed to the constructor.
     *
     * @return LogfileEntry New log file entry, which is already persisted, but not flushed.
     *
     * @throws Exception Exceptions when logged-in user is not set or the changed type is not found.
     */
    public function createLogEntry(string $changeTypeString, $content, MemberEntry $changesOnMember = null, EntityManagerInterface $entityManager = null): LogfileEntry
    {
        if (!$this->security->getUser() && !self::$loggedInUserForTests && !self::$currentUser) {
            throw new Exception("Logged in user is not set!");
        }

        return $this->createLogEntryWithoutLoggedInUser($changeTypeString, $content, $changesOnMember, $entityManager);
    }


    /**
     * Create a new log message which contains all the changes in entities and write the message to the database. Before
     * any changes are made to the member entity and its referenced fields, the method storeEntityCopy on the
     * $changesOnMember has to be called.
     *
     * @param string                             $changeTypeString Change type of the expected entities change
     * @param AbstractComplexLogEntryBase|string $content          Complex log content or string
     * @param MemberEntry|null                   $changesOnMember  Member on which the changes are done.
     * @param EntityManagerInterface|null        $entityManager    When using a special entity manager (e.g. in doctrine
     *                                                             event listeners, this manager can be passed here to
     *                                                             use instead of the one passed to the constructor.
     *
     * @return LogfileEntry New log file entry, which is already persisted, but not flushed.
     *
     * @throws Exception Exceptions when logged-in user is not set or the changed type is not found.
     */
    public function createLogEntryWithoutLoggedInUser(string $changeTypeString, $content, MemberEntry $changesOnMember = null, EntityManagerInterface $entityManager = null): LogfileEntry
    {
        if (!$entityManager) {
            $entityManager = $this->entityManager;
        }

        // Translate change type string to a change type object
        /** @var ChangeType $changeType */
        $changeType = $entityManager->getRepository(ChangeType::class)
                                    ->findOneBy(["changeType" => $changeTypeString]);
        if (!$changeType) {
            throw new Exception(sprintf("Change type %s not found!", $changeTypeString));
        }

        $logfileEntry = new LogfileEntry();
        $logfileEntry->setChangedByMember(self::$currentUser ?? self::$loggedInUserForTests ?? $this->security->getUser());
        $logfileEntry->setChangesOnMember($changesOnMember);
        $logfileEntry->setChangeType($changeType);
        $logfileEntry->setLogentry($content);
        // The log could be created with a different entity manager then the one which set the changed by user. This
        // entity would then not be managed by the current manager and would create an exception like the following:
        // A new entity was found through the relationship 'App\Entity\LogfileEntry#changedByMember' that was not
        // configured to cascade persist operations for entity: firstname1.lastname1. To solve this issue: Either
        // explicitly call EntityManager#persist() on this unknown entity or configure cascade persist this association
        // in the mapping for example @ManyToOne(..,cascade={"persist"}).
        // To omit this, we have to reload the member entity with the current entity manager.
        /** @var MemberEntry $changedByMember */
        if ($logfileEntry->getChangedByMember()) {
            $changedByMember = $entityManager->getRepository(MemberEntry::class)->find(
                $logfileEntry->getChangedByMember()->getId()
            );
            $logfileEntry->setChangedByMember($changedByMember);
        }
        $entityManager->persist($logfileEntry);

        if (!$this->disableEmailNotification) {
            $this->emailNotificationSender->sendEmailNotification($logfileEntry);
        }

        return $logfileEntry;
    }
}
