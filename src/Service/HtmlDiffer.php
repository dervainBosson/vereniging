<?php

/*
 * Copyright (C) 2022 by Luc Hamers <vereniging@hamers.de>
 *
 * This file is part of Vereniging.
 *
 * Vereniging is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Vereniging is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Vereniging.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Service;

/**
 * Class HtmlDiffer
 */
class HtmlDiffer
{
    /**
     * @param string $original
     * @param string $new
     *
     * @return array|string[]
     */
    public function diff(string $original, string $new): array
    {
        if ((!$this->isHTML($original)) && (!$this->isHTML($new))) {
            if ($original !== $new) {
                return [$original, $new];
            }

            return [];
        }

        return $this->htmlDiff($original, $new);
    }


    /**
     * Return true when the text contains html tags
     *
     * @param string $text
     *
     * @return bool
     */
    private function isHTML(string $text): bool
    {
        return $text !== strip_tags($text);
    }


    /**
     * Do the html diff using the phpdiff library
     *
     * @param string $old
     * @param string $new
     *
     * @return array
     */
    private function htmlDiff(string $old, string $new): array
    {
        if ($old === $new) {
            return [];
        }

        // Split up the strings into lines by using the closing entities for paragraph, table, table line and table cell
        preg_match_all('#.*(</p>|</table>|</tr>|</td>)#U', $old, $oldArrayLines);
        preg_match_all('#.*(</p>|</table>|</tr>|</td>)#U', $new, $newArrayLines);

        // The resulting complete lines (including the html end tags) is found in the first array entry. Rewrite these
        // to improve readability.
        $oldArrayLines = $oldArrayLines[0];
        $newArrayLines = $newArrayLines[0];
        $this->levelLines($oldArrayLines, $newArrayLines);

        // Now find all lines which are present in both strings
        $mutualLinesOld = array_intersect($oldArrayLines, $newArrayLines);
        $mutualLinesNew = array_intersect($newArrayLines, $oldArrayLines);

        // This is needed because of a strange behaviour of array_intersect when &nbsp; elements are in lines.
        $mutualLines = array_intersect($mutualLinesOld, $mutualLinesNew);
        foreach ($mutualLines as $key => $content) {
            // Do not remove newly added empty lines
            if ('' !== $content) {
                unset($oldArrayLines[$key]);
            }
        }
        foreach ($mutualLines as $key => $content) {
            // Do not remove newly added empty lines
            if ('' !== $content) {
                unset($newArrayLines[$key]);
            }
        }

        // To be able to loop over both arrays with consecutive keys, the keys need to be recalculated.
        $oldArrayLines = array_values($oldArrayLines);
        $newArrayLines = array_values($newArrayLines);

        $oldAnnotated = '';
        $newAnnotated = '';
        for ($i = 0; $i < count($oldArrayLines); $i++) {
            // Special handler for when comparing with an empty strings: the other string is completely different.
            if ('' === $oldArrayLines[$i]) {
                $oldAnnotated .= "-\n";
                $newAnnotated .= "<ins>$newArrayLines[$i]</ins>\n";
            } elseif ('' === $newArrayLines[$i]) {
                $oldAnnotated .= "<del>$oldArrayLines[$i]</del>\n";
                $newAnnotated .= "-\n";
            } else {
                // Split up by html codes or spaces
                $oldArray = preg_split('#(<[/]?[[:alpha:]]+>)|(\s)|(,)#U', $oldArrayLines[$i], 0, PREG_SPLIT_DELIM_CAPTURE);
                $newArray = preg_split('#(<[/]?[[:alpha:]]+>)|(\s)|(,)#U', $newArrayLines[$i], 0, PREG_SPLIT_DELIM_CAPTURE);

                // Insert annotated string containing all the diffs in this line
                $oldAnnotated .= $this->createCompareString($oldArray, $newArray, 'del')."\n";
                $newAnnotated .= $this->createCompareString($newArray, $oldArray, 'ins')."\n";
            }
        }

        $oldAnnotated = substr($oldAnnotated, 0, -1);
        $newAnnotated = substr($newAnnotated, 0, -1);

        $oldAnnotated = str_replace('</ins><ins>', '', $oldAnnotated);
        $newAnnotated = str_replace('</ins><ins>', '', $newAnnotated);

        return [$oldAnnotated, $newAnnotated];
    }


    /**
     * Create a string with the differences between two arrays, in which the differences are marked by
     * <marker>...</marker>
     *
     * @param string[] $oldArray
     * @param string[] $newArray
     * @param string   $marker
     *
     * @return string
     */
    private function createCompareString(array $oldArray, array $newArray, string $marker): string
    {
        $diffArray = array_diff($oldArray, $newArray);
        foreach ($diffArray as $index => $word) {
            $oldArray[$index] = "<$marker>$word</$marker>";
        }
        // Change array back to string
        $annotated = implode('', $oldArray);
        // Replace double deletes (</del> <del>) or inserts (</ins> <ins>) by a space
        $annotated = preg_replace("#</$marker>\s<$marker>#U", ' ', $annotated);

        return $annotated;
    }


    /**
     * This method tries to level out arrays with similar content. Example:
     * old:         new:
     *   test1        test1
     *   test3        test2
     *   test4        test3
     * will result in
     * old:         new:
     *   test1        test1
     *   ''           test2
     *   test3        test3
     *   test4        ''
     * This improves the results of the diffs because now new lines are compared to empty lines. This however only works
     * perfectly when the lines surrounding the new lines have not changed.
     *
     * @param array $old
     * @param array $new
     * @param int   $matchesIndex
     */
    private function levelLines(array &$old, array &$new, int $matchesIndex = 0): void
    {
        // Find the identical lines with the corresponding index, both in matchesOld and matchesNew
        $matchesOld = array_intersect($old, $new);
        $matchesNew = array_intersect($new, $old);

        // Remove the "" entries (the newly added lines to level out) from the list
        $emptyValueKeys = array_keys($matchesOld, '');
        foreach ($emptyValueKeys as $key) {
            unset($matchesOld[$key]);
        }
        $emptyValueKeys = array_keys($matchesNew, '');
        foreach ($emptyValueKeys as $key) {
            unset($matchesNew[$key]);
        }

        // Leave the recursive calls when all lines are processed
        if ((count($matchesOld) <= $matchesIndex) && (count($old) === count($new))) {
            return;
        }

        $matchesOldKeys = array_keys($matchesOld);
        $matchesNewKeys = array_keys($matchesNew);

        // When the index is past the number of elements of the matches arrays, then one of the arrays has more entries
        // than the other one (i.e. one has elements past the last matching line). Then we have to fill the smaller
        // array and we use the number of elements of both arrays.
        $oldIndex = $matchesOldKeys[$matchesIndex] ?? count($old);
        $newIndex = $matchesNewKeys[$matchesIndex] ?? count($new);

        if ($oldIndex < $newIndex) {
            $indexDiff = $newIndex - $oldIndex;
            for ($i = 0; $i < $indexDiff; $i++) {
                array_splice($old, $oldIndex, 0, '');
                $matchesIndex++;
            }
        } elseif ($oldIndex > $newIndex) {
            $indexDiff = $oldIndex - $newIndex;
            for ($i = 0; $i < $indexDiff; $i++) {
                array_splice($new, $newIndex, 0, '');
                $matchesIndex++;
            }
        } else {
            $matchesIndex++;
        }

        $this->levelLines($old, $new, $matchesIndex);
    }
}
