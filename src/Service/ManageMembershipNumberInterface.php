<?php

/*
 * Copyright (C) 2022 by Luc Hamers <vereniging@hamers.de>
 *
 * This file is part of Vereniging.
 *
 * Vereniging is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Vereniging is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Vereniging.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Service;

use App\Entity\MemberEntry;

/**
 * Interface CreateMembershipNumberInterface
 */
interface ManageMembershipNumberInterface
{
    /**
     * Add a membership number to a given member entry.
     *
     * @param MemberEntry $memberEntry MemberEntry to which a new membership number is added
     */
    public function addMembershipNumber(MemberEntry $memberEntry): void;


    /**
     * Remove a membership number from a given member entry.
     *
     * @param MemberEntry $memberEntry
     */
    public function removeMembershipNumber(MemberEntry $memberEntry): void;
}
