<?php

/*
 * Copyright (C) 2022 by Luc Hamers <vereniging@hamers.de>
 *
 * This file is part of Vereniging.
 *
 * Vereniging is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Vereniging is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Vereniging.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Service;

use App\Entity\CommitteeFunctionMap;
use App\Entity\MailingList;
use App\Entity\MemberEntry;
use App\Entity\SerialLetter;

/**
 * This bundles all methods used both by serial email and letter generator classes
 */
trait SerialLetterEmailTrait
{
    /**
     * Create a letter opening, e.g. "Dear Mr."
     *
     * @param MemberEntry $member
     * @param string      $locale
     * @param bool        $capital True if "dear" has to start with a capital letter
     *
     * @return string
     */
    private function createOpening(MemberEntry $member, string $locale, bool $capital): string
    {
        if ($capital) {
            $capital = ' capital';
        } else {
            $capital = '';
        }

        $opening = " ".$member->getName().", ";

        // The first translated text starts with a capital
        switch ($member->getGender()) {
            case 'f':
                $opening = $this->translator->trans('Dear Ms.'.$capital, [], null, $locale).$opening;
                break;
            case 'm':
                $opening = $this->translator->trans('Dear Mr.'.$capital, [], null, $locale).$opening;
                break;
            case 'n':
                $opening = $this->translator->trans('Dear ladies and gentlemen,', [], null, $locale);
                break;
            case 'd':
                if ('-' !== $member->getFirstName()) {
                    $opening = " ".$member->getFirstName().$opening;
                }
                $opening = $this->translator->trans('Dear Ms.Mr.'.$capital, [], null, $locale).$opening;
        }

        return $opening;
    }


    /**
     * Replace the variables in the letters text by the corresponding values from the member object.
     *
     * @param MemberEntry  $memberEntry       Member entry from which the data is taken
     * @param string       $content           Letter content in which the variables like <span class="mceNonEditable"
     *                                        style="border: thin dotted grey;" data-variable="name">name</span> are
     *                                        replaced.
     * @param string       $locale            Locale to be used when translating replacement values.
     * @param SerialLetter $letter            Serial letter from which the signers are read
     * @param array|int[]  $selectedMemberIds List of the member ids which are also in this serial letter or email.
     *
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    private function replaceVariables(MemberEntry $memberEntry, string &$content, string $locale, SerialLetter $letter, array $selectedMemberIds): void
    {
        // Direct member entry fields
        $this->replaceMemberVariables($memberEntry, $content, 'name');
        $this->replaceMemberVariables($memberEntry, $content, 'firstName');
        $this->replaceMemberVariables($memberEntry, $content, 'email');
        $this->replaceMemberVariables($memberEntry, $content, 'userName');
        $this->replaceMemberVariables($memberEntry, $content, 'memberSince');

        // Translated member entry fields
        $this->replaceMemberVariables($memberEntry, $content, 'preferredLanguage', 'getLanguageTranslated', $locale);
        $this->replaceMemberVariables($memberEntry, $content, 'userRole', 'getUserRoleTranslated', $locale);
        $this->replaceMemberVariables($memberEntry, $content, 'membershipType', 'getTypeTranslated', $locale);

        // Specially formatted fields
        $replacement = $memberEntry->getTitle() ? $memberEntry->getTitle() : '-';
        $this->replaceVariable($replacement, $content, 'title');

        $replacement = $memberEntry->getBirthday() ? $memberEntry->getBirthday()->format('d.m.Y'): '-';
        $this->replaceVariable($replacement, $content, 'birthday');
        $replacement = $memberEntry->getMembershipNumber() ? $memberEntry->getMembershipNumber()->getMembershipNumber() : '-';
        $this->replaceVariable($replacement, $content, 'membershipNumber');
        $replacement = number_format($memberEntry->getMembershipType()->getMembershipFee(), 2, ',', '.');
        $this->replaceVariable($replacement, $content, 'membershipFee');
        $replacement = $this->translator->trans($memberEntry->getMembershipEndRequested() ? 'Yes' : 'No', [], null, $locale);
        $this->replaceVariable($replacement, $content, 'membershipEndRequested');
        $replacement = $this->translator->trans($memberEntry->getGender(true), [], null, $locale);
        $this->replaceVariable($replacement, $content, 'gender');
        $replacement = $memberEntry->getStatus()->translate($locale)->getStatusTranslated();
        $this->replaceVariable($replacement, $content, 'userStatus');

        // Company fields
        $this->replaceCompanyVariables($memberEntry, $content);

        // List fields
        $this->replacePhoneNumbersVariable($memberEntry, $content, $locale);
        $this->replaceAddressesVariable($memberEntry, $content, $locale);
        $this->replaceCommitteesVariable($memberEntry, $content, $locale);
        $this->replaceMailingListsVariable($memberEntry, $content);
        $this->replaceGroupMembersVariable($memberEntry, $content);

        // Special fields
        $this->replaceSignatureVariables($content, $letter, $locale);
        $this->replaceOpeningVariables($content, $memberEntry, $locale, $selectedMemberIds);
    }


    /**
     * Replace variables which can be read directly from the member entry object
     *
     * @param MemberEntry $memberEntry
     * @param string      $content
     * @param string      $variable        Text variable to replace
     * @param string|null $translateMethod Name of the translate method
     * @param string|null $locale          Locale to translate into
     */
    private function replaceMemberVariables(MemberEntry $memberEntry, string &$content, string $variable, ?string $translateMethod = null, ?string $locale = null): void
    {
        $function = 'get'.ucfirst($variable);
        if ($translateMethod) {
            $replacement = $memberEntry->$function()->translate($locale)->$translateMethod();
        } else {
            $replacement = $memberEntry->$function();
        }
        $this->replaceVariable($replacement, $content, $variable);
    }


    /**
     * @param MemberEntry $memberEntry
     * @param string      $content
     */
    private function replaceCompanyVariables(MemberEntry $memberEntry, string &$content): void
    {
        $replacement = $memberEntry->getCompanyInformation() ? $memberEntry->getCompanyInformation()->getName() : '-';
        $this->replaceVariable($replacement, $content, 'companyName');
        $replacement = $memberEntry->getCompanyInformation() ? $memberEntry->getCompanyInformation()->getCity() : '-';
        $this->replaceVariable($replacement, $content, 'companyLocation');
        $replacement = $memberEntry->getCompanyInformation() ? $memberEntry->getCompanyInformation()->getDescription() : '-';
        $this->replaceVariable($replacement, $content, 'companyDescription');
        $replacement = $memberEntry->getCompanyInformation() ? $memberEntry->getCompanyInformation()->getFunctionDescription() : '-';
        $this->replaceVariable($replacement, $content, 'companyFunction');
        $replacement = $memberEntry->getCompanyInformation() ? $memberEntry->getCompanyInformation()->getUrl() : '-';
        $this->replaceVariable($replacement, $content, 'companyHomepage');
    }


    /**
     * @param MemberEntry $memberEntry
     * @param string      $content
     * @param string      $locale
     */
    private function replacePhoneNumbersVariable(MemberEntry $memberEntry, string &$content, string $locale): void
    {
        if (count($memberEntry->getPhoneNumbers())) {
            $dataTable = [];
            foreach ($memberEntry->getPhoneNumbers() as $phoneNumber) {
                $dataTable[] = [
                    $phoneNumber->getPhoneNumberType()->translate($locale)->getPhoneNumberTypeTranslated().':',
                    $phoneNumber->getPhoneNumber(),
                ];
            }
            $replacement = $this->createTable($dataTable);
        } else {
            $replacement = '-';
        }
        $this->replaceVariable($replacement, $content, 'phoneNumbers');
    }


    /**
     * @param MemberEntry $memberEntry
     * @param string      $content
     * @param string      $locale
     */
    private function replaceAddressesVariable(MemberEntry $memberEntry, string &$content, string $locale): void
    {
        if (count($memberEntry->getMemberAddressMaps())) {
            $dataTable = [];
            foreach ($memberEntry->getMemberAddressMaps() as $address) {
                $dataTable[] = [
                    $address->getAddress()->getAddressType()->translate($locale)->getAddressTypeTranslated().':',
                    $address->getAddress()->getAddress(),
                    $address->getAddress()->getZip(),
                    $address->getAddress()->getCity(),
                    $address->getAddress()->getCountry()->translate($locale)->getCountryTranslated(),
                ];
            }
            $replacement = $this->createTable($dataTable, $locale);
        } else {
            $replacement = '-';
        }
        $this->replaceVariable($replacement, $content, 'postalAddresses');
    }


    /**
     * @param MemberEntry $memberEntry
     * @param string      $content
     * @param string      $locale
     */
    private function replaceCommitteesVariable(MemberEntry $memberEntry, string &$content, string $locale): void
    {
        if (count($memberEntry->getCommitteeFunctionMaps())) {
            $dataTable = [];
            /** @var CommitteeFunctionMap $committeeFunctionMap */
            foreach ($memberEntry->getCommitteeFunctionMaps() as $committeeFunctionMap) {
                $dataTable[] = [
                    $committeeFunctionMap->getCommittee()->translate($locale)->getCommitteeNameTranslated().':',
                    $committeeFunctionMap->getCommitteeFunction()->translate(
                        $locale
                    )->getCommitteeFunctionNameTranslated(),
                ];
            }
            $replacement = $this->createTable($dataTable, $locale);
        } else {
            $replacement = '-';
        }
        $this->replaceVariable($replacement, $content, 'committeeMemberships');
    }


    /**
     * @param MemberEntry $memberEntry
     * @param string      $content
     */
    private function replaceMailingListsVariable(MemberEntry $memberEntry, string &$content): void
    {
        if (count($memberEntry->getMailingLists())) {
            $replacement = '';
            /** @var MailingList $mailingList */
            foreach ($memberEntry->getMailingLists() as $mailingList) {
                $replacement .= $mailingList->getListName().', ';
            }
            $replacement = substr($replacement, 0, -2);
        } else {
            $replacement = '-';
        }
        $this->replaceVariable($replacement, $content, 'mailingListSubscriptions');
    }


    /**
     * @param MemberEntry $memberEntry
     * @param string      $content
     */
    private function replaceGroupMembersVariable(MemberEntry $memberEntry, string &$content): void
    {
        if (count($memberEntry->getPhoneNumbers())) {
            $replacement = '';
            foreach ($memberEntry->getGroupMembers() as $groupMember) {
                $replacement .= $groupMember->getCompleteName().', ';
            }
            if (count($memberEntry->getGroupMembers())) {
                $replacement = $memberEntry->getCompleteName().", $replacement";
                $replacement = substr($replacement, 0, -2);
            }
        } else {
            $replacement = '-';
        }
        $this->replaceVariable($replacement, $content, 'groupMembers');
    }


    /**
     * @param string       $content
     * @param SerialLetter $letter
     * @param string       $locale
     *
     * @return void
     */
    private function replaceSignatureVariables(string &$content, SerialLetter $letter, string $locale): void
    {
        $signature = '$signature$';
        $this->addSignature($letter, $signature, $locale);
        $signature = str_replace("\\\\", "\\\\\\\\", $signature);
        $this->replaceVariable($signature, $content, 'signers');
    }


    /**
     * @param string      $content            Text in which the opening is to be written.
     * @param MemberEntry $member             Member to which the letter is to be send.
     * @param string      $locale
     * @param array       $selectedMembersIds List of members ids which are in the list for the serial letters
     */
    private function replaceOpeningVariables(string &$content, MemberEntry $member, string $locale, array $selectedMembersIds): void
    {
        $signature = '$opening$';
        $this->AddOpening($signature, $member, $locale, $selectedMembersIds);
        $this->replaceVariable($signature, $content, 'opening');
    }


    /**
     * Replace a variable in the serial letters text
     *
     * @param string|null $replacement
     * @param string      $content
     * @param string      $variable
     */
    private function replaceVariable(?string $replacement, string &$content, string $variable): void
    {
        // This is needed to prevent underscores to mess up the latex compile job
        $replacement = str_replace('_', '&lowbar;', $replacement);
        // A variable in the text looks like this:
        // <span class="mceNonEditable" style="border: thin dotted grey;" data-variable="firstName">first name</span>
        $content = preg_replace("#<span class=\"mceNonEditable\" style=\"[\w;: ]*\" data-variable=\"$variable\">.*</span>#U", $replacement, $content);
    }


    /**
     * Insert the opening line, e.g. Dear Mr. A, dear Ms. B,
     *
     * @param string      $content            Text in which the opening is to be written.
     * @param MemberEntry $member             Member to which the letter is to be send.
     * @param string      $locale
     * @param array       $selectedMembersIds List of members ids which are in the list for the serial letters
     */
    private function addOpening(string &$content, MemberEntry $member, string $locale, array $selectedMembersIds): void
    {
        $opening = $this->createOpening($member, $locale, true);
        $address = $member->getMainAddress();
        foreach ($member->getGroupMembers() as $groupMember) {
            if ($address === $groupMember->getMainAddress()) {
                if (in_array($groupMember->getId(), $selectedMembersIds)) {
                    // For each added group member, the translated text starts with a small letter.
                    $opening .= $this->createOpening($groupMember, $locale, false);
                }
            }
        }

        $content = str_replace('$opening$', $opening, $content);
    }


    /**
     * Read the  contains the names of the people below the "kind regards".
     *
     * @param SerialLetter $letter Serial letter object from which the settings are read.
     * @param string       $locale Locale to translate the committee and function into.
     *
     * @return array
     */
    private function getSignatureData(SerialLetter $letter, string $locale): array
    {
        if (is_null($letter->getFirstSignatureMember())) {
            return [];
        }

        $values = [
            ['', ''],
            ['', ''],
        ];

        $values[0][0] = (string) $letter->getFirstSignatureMember()->getCompleteName($letter->getSignatureUseTitle());
        if ($letter->getFirstSignatureMembersFunction()) {
            $letter->getFirstSignatureMembersFunction()->getCommittee()->setCurrentLocale($locale);
            $letter->getFirstSignatureMembersFunction()->getCommitteeFunction()->setCurrentLocale($locale);
            $values[1][0] = (string) $letter->getFirstSignatureMembersFunction();
        }
        if (!is_null($letter->getSecondSignatureMember())) {
            $values[0][1] = (string) $letter->getSecondSignatureMember()->getCompleteName($letter->getSignatureUseTitle());
            if ($letter->getSecondSignatureMembersFunction()) {
                $letter->getSecondSignatureMembersFunction()->getCommittee()->setCurrentLocale($locale);
                $letter->getSecondSignatureMembersFunction()->getCommitteeFunction()->setCurrentLocale($locale);
                $values[1][1] = (string) $letter->getSecondSignatureMembersFunction();
            }
        }

        return $values;
    }
}
