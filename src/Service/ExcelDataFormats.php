<?php

/*
 * Copyright (C) 2022 by Luc Hamers <vereniging@hamers.de>
 *
 * This file is part of Vereniging.
 *
 * Vereniging is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Vereniging is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Vereniging.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Service;

/**
 * Define ex
 *
 * Class ExportDataFormats
 */
class ExcelDataFormats
{
    const STATIC_HEADERS = ['Name', 'First name', 'Gender', 'Title', 'Birthday', 'Preferred language', 'Email address',
                           'Membership number', 'Direct debit', 'Membership end requested', 'Member since',
                           'User status', 'Membership type', 'User role', ];

    const DYNAMIC_HEADERS = [
        'companyInformation' => ['header' => 'Company information', 'fields' => ['Company name', 'Company location', 'Company description', 'Company function', 'Company URL']],
        'phoneNumbers'       => ['header' => 'Phone number', 'fields' => ['Phone number type', 'Phone number']],
        'addresses'          => ['header' => 'Address', 'fields' => ['Main address', 'Address type', 'Address', 'Zip', 'City', 'Country']],
        'committees'         => ['header' => 'Committee function', 'fields' => ['Committee', 'Function']],
        'groupMembers'       => ['header' => 'Group member', 'fields' => ['Name', 'First name', 'Email address']],
        'mailingLists'       => ['header' => 'Mailing list', 'fields' => ['Mailing list']],
        'logFileEntries'     => ['header' => 'Log file entry', 'fields' => ['Entry', 'Editable']],
        ];

    const REQUIRED_FIELDS = [
        'Name', 'First name', 'Gender', 'Preferred language', 'Membership end requested', 'User status',
        'Company name', 'Company location',
        'Phone number type', 'Phone number',
        'Main address', 'Address type', 'Address', 'Zip', 'City', 'Country',
        'Committee', 'Function', 'Editable',
    ];

    /**
     * Returns true, which the selected field is required, i.e. must contain data.
     *
     * @param string $fieldName Name of the excel field which is to be checked.
     *
     * @return bool True when the field is required.
     */
    public static function isRequiredField(string $fieldName): bool
    {
        return (in_array($fieldName, self::REQUIRED_FIELDS));
    }
}
