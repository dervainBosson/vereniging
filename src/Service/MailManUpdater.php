<?php

/*
 * Copyright (C) 2022 by Luc Hamers <vereniging@hamers.de>
 *
 * This file is part of Vereniging.
 *
 * Vereniging is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Vereniging is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Vereniging.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Service;

use App\Entity\MailingList;
use Doctrine\ORM\EntityManagerInterface;
use Exception;

/**
 * This class implements updating email lists using the GNU mailman software.
 *
 * Class MailManUpdater
 */
class MailManUpdater implements MailingListUpdateInterface
{
    private EntityManagerInterface $entityManager;
    /** @var callable */
    private  $readFileFunction;


    /**
     * MailManUpdater constructor.
     *
     * @param EntityManagerInterface $entityManager
     * @param callable|null          $readFileFunction To be able to test this class, a different read file function can
     *                                                 be passed. Trying to replace the get_file_contents function in
     *                                                 unit test worked, but broke all other unit tests using this
     *                                                 function.
     */
    public function __construct(EntityManagerInterface $entityManager, ?callable $readFileFunction = null)
    {
        $this->entityManager = $entityManager;
        $this->readFileFunction = $readFileFunction;
    }


    /**
     * @inheritdoc
     *
     * @param MailingList $mailingList  Mailing list object to which the email address has to be added.
     * @param string      $emailAddress Email address which has to be added to the list.
     *
     * @throws Exception
     */
    public function addEmailAddress(MailingList $mailingList, string $emailAddress): void
    {
        $url  = $this->getManagementAddress($mailingList);
        $url .= "add?subscribe_or_invite=0&subscribees=$emailAddress&adminpw=".$mailingList->getManagementPassword();

        // For unit tests, a different read function can be passed.
        $returnContent = $this->readFile($url);
        if (false === $returnContent) {
            throw new \Exception("Calling the url \"$url\" didn't work!");
        }

        if ((false === strpos($returnContent, 'Successfully subscribed')) ||
            (false === strpos($returnContent, $emailAddress))) {
            throw new \Exception("Adding email address $emailAddress with command \"$url\" failed!");
        }
    }


    /**
     * @inheritdoc
     *
     * @param MailingList $mailingList  Mailing list object to which the email address has to be added.
     * @param string      $emailAddress Email address which has to be added to the list.
     *
     * @throws Exception
     */
    public function removeEmailAddress(MailingList $mailingList, string $emailAddress): void
    {
        $url  = $this->getManagementAddress($mailingList);
        $url .= "remove?send_unsub_ack_to_this_batch=0&send_unsub_notifications_to_list_owner=1&unsubscribees=$emailAddress&adminpw=".$mailingList->getManagementPassword();
        $returnContent = $this->readFile($url);

        if (false === $returnContent) {
            throw new \Exception("Calling the url \"$url\" didn't work!");
        }

        if ((false === strpos($returnContent, 'Successfully Unsubscribed')) ||
            (false === strpos($returnContent, $emailAddress))) {
            throw new \Exception("Removing email address $emailAddress with command \"$url\" failed!");
        }
    }


    /**
     * @inheritdoc
     *
     * @param bool $edit When true, return the field names above the form. If false, return the field names above the
     *                   text columns.
     *
     * @return string[]|array
     */
    public function getFieldNames(bool $edit): array
    {
        if ($edit) {
            return ['List name', 'Server domain name', 'Management password'];
        }

        return ['List name', 'Server domain name', 'Management URL', 'Management password'];
    }


    /**
     * This returns the field texts matching the field names when not editing, i.e. when only showing the text
     *
     * @param MailingList $mailingList Mailing list object from which the email address has to be removed.
     *
     * @return string[]|array
     */
    public function getTextFields(MailingList $mailingList): array
    {
        return [$mailingList->getListName(), $mailingList->getManagementEmail(), $this->getManagementAddress($mailingList), '******'];
    }


    /**
     * @inheritdoc
     *
     * @return string
     */
    public function getMailingListType(): string
    {
        return 'MailMan';
    }


    /**
     * @inheritdoc
     *
     * @param MailingList $mailingList Mailing list object from which the email address has to be removed.
     *
     * @return string
     */
    private function getManagementAddress(MailingList $mailingList): string
    {
        return $mailingList->getManagementEmail().'/mailman/admin/'.$mailingList->getListName().'/members/';
    }

    /**
     * This reads a file or url. For unit tests, a different read function can be passed to be used instead of
     * get_file_contents, to be able to test the results.
     *
     * @param string $url
     *
     * @return false|mixed|string
     */
    private function readFile(string $url)
    {
        // For unit tests, a different read function can be passed.
        if ($this->readFileFunction) {
            $returnContent = call_user_func($this->readFileFunction, $url);
        } else {
            // When the url is not found, file_get-content will generate a warning. To suppress this (we will check the
            // result in the next line), the @ is added.
            $returnContent = @file_get_contents($url);
        }

        return $returnContent;
    }
}
