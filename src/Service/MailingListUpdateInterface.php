<?php

/*
 * Copyright (C) 2022 by Luc Hamers <vereniging@hamers.de>
 *
 * This file is part of Vereniging.
 *
 * Vereniging is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Vereniging is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Vereniging.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Service;

use App\Entity\MailingList;

/**
 * Interface MailingListUpdateInterface
 */
interface MailingListUpdateInterface
{
    /**
     * Add an email address to the mailing list.
     *
     * @param MailingList $mailingList  Mailing list object to which the email address has to be added.
     * @param string      $emailAddress Email address which has to be added to the list.
     */
    public function addEmailAddress(MailingList $mailingList, string $emailAddress): void;

    /**
     * Remove an email address from the mailing list.
     *
     * @param MailingList $mailingList  Mailing list object from which the email address has to be removed.
     * @param string      $emailAddress Email address which has to be removed from the list.
     */
    public function removeEmailAddress(MailingList $mailingList, string $emailAddress): void;


    /**
     * This returns a list of field names for the database fields. Because of different usages of the database fields,
     * this is needed to correctly write and translated the field names in the controller. Since the form field names
     * can be different from the field names above the non-form-table, this can be distinguished by the edit parameter.
     *
     * @param bool $edit When true, return the field names above the form. If false, return the field names above the
     *                   text columns.
     *
     * @return string[]|array
     */
    public function getFieldNames(bool $edit): array;


    /**
     * This returns the field texts matching the field names when not editing, i.e. when only showing the text
     *
     * @param MailingList $mailingList Mailing list object from which the email address has to be removed.
     *
     * @return string[]|array
     */
    public function getTextFields(MailingList $mailingList): array;


    /**
     * This returns the type of the mailing list implementation
     *
     * @return string
     */
    public function getMailingListType(): string;
}
