<?php

/*
 * Copyright (C) 2022 by Luc Hamers <vereniging@hamers.de>
 *
 * This file is part of Vereniging.
 *
 * Vereniging is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Vereniging is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Vereniging.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Service;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Mailer\MailerInterface;

/**
 * Class MailingListUpdaterFactory
 */
class MailingListUpdaterFactory
{
    /**
     * This class is used by the service container / services.yaml to select a mailing list updater depending on the
     * environment variable for this.
     *
     * @param EntityManagerInterface $entityManager
     * @param MailerInterface        $mailer
     * @param string                 $senderAddress Senders email address used to send emails to majordomo
     * @param string                 $type          Type of the updater. Currently, only "MailMan" and "MajorDomo" are
     *                                              supported.
     *
     * @return MailingListUpdateInterface
     *
     * @throws \Exception
     */
    public static function createMailingListUpdater(EntityManagerInterface $entityManager, MailerInterface $mailer, string $senderAddress, string $type)
    {
        switch ($type) {
            case 'MailMan':
                return new MailManUpdater($entityManager);
            case 'MajorDomo':
                return new MajorDomoUpdater($mailer, $senderAddress);
            default:
                throw new \Exception(sprintf('Type "%s" not supported!', $type));
        }
    }
}
