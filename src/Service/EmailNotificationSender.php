<?php

/*
 * Copyright (C) 2022 by Luc Hamers <vereniging@hamers.de>
 *
 * This file is part of Vereniging.
 *
 * Vereniging is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Vereniging is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Vereniging.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Service;

use App\Entity\EmailTemplate;
use App\Entity\LogfileEntry;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;
use Symfony\Component\Security\Core\Security;
use Symfony\Contracts\Translation\TranslatorInterface;
use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

/**
 * This class is called when a new log message is written. Its job is then to send the log message by email to all users
 * which are registered to receive email notifications.
 *
 * Class EmailNotificationSender
 */
class EmailNotificationSender
{
    private EntityManagerInterface $entityManager;
    private MailerInterface $mailer;
    private Environment $twig;
    private ?array $siteName;
    private TranslatorInterface $translator;
    private Security $security;


    /**
     * EmailNotificationSender constructor.
     *
     * @param EntityManagerInterface $entityManager
     * @param MailerInterface        $mailer
     * @param Environment            $twig
     * @param TranslatorInterface    $translator
     * @param Security               $security
     * @param string|null            $siteName      Array as json string where the keys are the locales and the content
     *                                              is the site name in each language.
     *
     * @throws Exception
     */
    public function __construct(EntityManagerInterface $entityManager, MailerInterface $mailer, Environment $twig, TranslatorInterface $translator, Security $security, ?string $siteName)
    {
        $this->entityManager = $entityManager;
        $this->mailer = $mailer;
        $this->twig = $twig;
        $this->siteName = null;
        if ($siteName) {
            $this->siteName = json_decode($siteName, true);
            if (is_null($this->siteName)) {
                throw new Exception(sprintf('Exception in class %s in line %d: Malformed site name json string: %s', __CLASS__, __LINE__ - 2, $siteName));
            }
        }
        $this->translator = $translator;
        $this->security = $security;
    }


    /**
     * Send a change to all members which are subscribed to this change type.
     *
     * @param LogfileEntry $logfileEntry
     *
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     * @throws Exception
     */
    public function sendEmailNotification(LogfileEntry $logfileEntry): void
    {
        $members = $logfileEntry->getChangeType()->getEmailNotificationMembers();
        // When there are no subscribers for this change type, leave immediately
        if (0 === count($members)) {
            return;
        }

        $emailAddressesByLocale = [];
        foreach ($members as $member) {
            $emailAddressesByLocale[$member->getPreferredLanguage()->getLocale()][] = $member->getEmail();
        }

        /** @var EmailTemplate $template */
        $template = $this->entityManager->getRepository(EmailTemplate::class)->readChangeTemplate();

        foreach ($emailAddressesByLocale as $locale => $emailAddresses) {
            LogfileEntry::prepareTranslation($this->entityManager, $this->translator, $locale);

            $body = $this->twig->render('Email/EmailNotification.html.twig', [
                'logFileEntry' => $logfileEntry,
                'locale' => $locale,
                'hasChangesOnMember' => $logfileEntry->getChangesOnMember() ?'yes:' : 'no',
                'changedByMember' => $logfileEntry->getChangedByMember() ? $logfileEntry->getChangedByMember()->getCompleteName() : '-',
                'changesOnMember' => $logfileEntry->getChangesOnMember() ? $logfileEntry->getChangesOnMember()->getCompleteName() : '-',
            ]);

            // Use the email template for change messages and write the rendered body into this template
            $body = str_replace(EmailTemplate::HTML_CONTENT_VARIABLE, $body, $template->getContent());
            $message = $this->createMessage($logfileEntry, $locale);
            $message
                ->from($template->getSenderAddress($locale))
                ->to(...$emailAddresses)
                ->text(strip_tags($body))
                ->html($body);

            try {
                $this->mailer->send($message);
            } catch (TransportExceptionInterface $e) {
                $messageCreator = new LogMessageCreator($this->entityManager, $this, $this->security);
                // Since we have a problem sending emails, disable them for the coming log message
                $messageCreator->disableEmailNotifications();
                $message = 'Exception in class '.__CLASS__.' in line '.(__LINE__ - 4).': '.$e->getMessage();
                $messageCreator->createLogEntryWithoutLoggedInUser('error', $message);
                $messageCreator->enableEmailNotifications();
                $this->entityManager->flush();
            }


        }
    }


    /**
     * Create an empty email message including the translated subject
     *
     * @param LogfileEntry $logfileEntry
     * @param string       $locale
     *
     * @return Email
     */
    private function createMessage(LogfileEntry $logfileEntry, string $locale): Email
    {
        $logfileEntry->getChangeType()->setCurrentLocale($locale);
        $hasSiteName = is_null($this->siteName) ? 'no' : 'yes';
        $changeType = $logfileEntry->getChangeType()->getChangeType();
        $siteName = '';
        if ((!is_null($this->siteName) && array_key_exists($locale, $this->siteName))) {
            $siteName = $this->siteName[$locale];
        }
        $subject = $this->translator->trans(
            'change_email_subject',
            ['hasSiteName' => $hasSiteName, 'changeType' => $changeType, 'siteName' => $siteName],
            'messages',
            $locale
        );

        return (new Email())->subject($subject);
    }
}
