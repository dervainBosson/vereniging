<?php

/*
 * Copyright (C) 2022 by Luc Hamers <vereniging@hamers.de>
 *
 * This file is part of Vereniging.
 *
 * Vereniging is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Vereniging is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Vereniging.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Service;

use App\Entity\Language;
use App\Entity\MemberEntry;
use App\Entity\SerialLetter;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * Generate a PDF serial letter
 *
 * Class SerialLetterGenerator
 */
class SerialLetterGenerator
{
    use SerialLetterEmailTrait;

    private Html2Latex $html2Latex;
    private EntityManagerInterface $entityManager;
    private TranslatorInterface $translator;
    private LogMessageCreator $logMessageCreator;
    // File path to the used LaTeX template file, e.g. /srv/www/templates/SerialLetter/template.tex
    private string $templatePath;
    // File path to the used background file, e.g. /srv/www/templates/SerialLetter/background.pdf
    private string $backgroundPath;
    // String containing the complete LaTeX file. Starts as the original template and ends as the ready to process file
    // just before it is written to disk and pdflatex is called.
    private string $templateData;
    // Name of the LaTeX-, pdf- and intermediate files in the temp directory
    private string $baseFileName;
    // Temporary directory in which all files are stored
    private string $tempDirectory;
    // Binary to translate latex to pdf
    private string $pdfLatexBin;
    // Projects root directory
    private string $projectDir;
    private bool $debug;

    // These variables must be present in the latex template and will be replaced when the letter is created.
    public const NEEDED_VARIABLES = ['return_address', 'background', 'template_path', 'address', 'language',
        'title', 'content', 'footer', 'supported_languages',
        ];

    // These variables can be used by the user in the serial letter and will be replaced by the fields of the member for
    // which the serial letter is generated.
    public const REPLACEMENT_VARIABLES = [
        'opening', 'signers', '-',
        'name', 'firstName', 'title', 'gender', 'birthday', 'preferredLanguage', '-',
        'email', 'phoneNumbers', 'postalAddresses', 'mailingListSubscriptions', '-',
        'companyName', 'companyLocation', 'companyDescription', 'companyFunction', 'companyHomepage', '-',
        'userName', 'userRole', 'membershipNumber', 'memberSince', 'membershipEndRequested', 'userStatus',
        'membershipType', 'membershipFee', 'groupMembers', 'committeeMemberships',
    ];


    /**
     * GenerateSerialLetter constructor.
     * @param Html2Latex             $html2Latex
     * @param EntityManagerInterface $entityManager
     * @param TranslatorInterface    $translator
     * @param LogMessageCreator      $logMessageCreator
     * @param array                  $settings          Array containing the following settings:
     *                                                  template: path to the latex template file. When $template starts
     *                                                  with a '/', the file is searched in an absolute path. If not, it
     *                                                  starts in the projects base directory.
     *                                                  returnAddress: return address above the recipients address. A
     *                                                  newline has to be written as "\n".
     *                                                  footer: footer below the first page. A newline has to be written as
     *                                                  an "\n".
     *                                                  tempDirectory: directory where the files for PDF generation can
     *                                                  temporarily be stored.
     *                                                  pdfLatexBin: binary used to generate the pdf, e.g. /usr/bin/pdflatex
     * @param string                 $projectDir        Projects root directory
     * @param boolean                $debug             When true, the created files are not deleted after generating the
     *                                                  serial letter. This makes debugging easier.
     *
     * @throws Exception Exception is thrown when the template file is not found.
     */
    public function __construct(Html2Latex $html2Latex, EntityManagerInterface $entityManager, TranslatorInterface $translator, LogMessageCreator $logMessageCreator, array $settings, string $projectDir, bool $debug = false)
    {
        try {
            $this->html2Latex = $html2Latex;
            $this->entityManager = $entityManager;
            $this->translator = $translator;
            $this->pdfLatexBin = $settings['pdfLatexBin'] ?? '';
            $this->logMessageCreator = $logMessageCreator;
            $this->projectDir = $projectDir;
            $this->debug = $debug;
            $this->templatePath = $this->checkTemplateFilesLocation($settings['template'] ?? '', 'Latex template');
            $this->backgroundPath = $this->checkTemplateFilesLocation($settings['background'] ?? '', 'Background image');
            $this->checkTempDirectory($settings['tempDirectory'] ?? '');
            // Add a random string at the end, to prevent files being deleted in the destructor when several requests
            // are processed in the same second.
            $this->baseFileName = $this->tempDirectory.'serial_letter_'.date('Ymd_His').'_'.md5(rand());
            $this->checkPdfLatex();
            // Load template file into string for all further operations
            $this->templateData = file_get_contents($this->templatePath);
            $this->checkTemplateFileVariables();
            $this->initialiseTemplateFile($settings['returnAddress'] ?? '', $settings['footer'] ?? '');
        } catch (Exception $e) {
            $this->logMessageCreator->createLogEntry('error', $e->getMessage());
            $this->entityManager->flush();
            throw $e;
        }
    }


    /**
     * Cleanup all temporary files
     */
    public function __destruct()
    {
        if (!$this->debug) {
            array_map('unlink', glob($this->baseFileName.'*'));
        }
    }


    /**
     * return all variables which have to be defined in the template. All variable name in the LaTeX file must be
     * enclosed by $, e.g. $return_address$.
     *
     * @return array
     */
    public static function getNeededVariables(): array
    {
        return self::NEEDED_VARIABLES;
    }


    /**
     * Get the list of possible variables which can be used in the serial letter by the user.
     *
     * @return string[]
     */
    public static function getReplacementVariables(): array
    {
        return self::REPLACEMENT_VARIABLES;
    }


    /**
     * Return the current data objects. This is probably only needed for unit testing.
     *
     * @return string
     */
    public function getCurrentData(): string
    {
        return $this->templateData;
    }


    /**
     * @param SerialLetter $letter        Serial letter object which contains the data for the serial letter.
     * @param Language     $language      Language in which the serial letter has to be generated.
     * @param MemberEntry  $currentMember Member entry for which the serial letter will be generated. The data from this
     *                                    member is used to fill the replacement variables.
     * @param bool         $simulate      When true, no files are written. This can be used for tests.
     *
     * @return string Path to PDF file.
     *
     * @throws Exception
     */
    public function generatePdfPreview(SerialLetter $letter, Language $language, MemberEntry $currentMember, bool $simulate = false): string
    {
        try {
            // The address data cannot be completely empty, so insert a non breaking space.
            $this->templateData = str_replace('$address$', '~', $this->templateData);
            $this->templateData = str_replace('$language$', $language->getBabelName(), $this->templateData);

            // Underscore symbol generates an error in latex. But it cannot be replaced there, because the underscore
            // could also be used in files paths. To be able to replace it later on, we replace it by the html symbol.
            $content = $letter->getContent($language)->getContent();
            $content = str_replace('_', '&lowbar;', $content);
            $title = htmlentities($letter->getTitle($language));
            $title = str_replace('_', '&lowbar;', $title);

            // Encode special characters by their html representation, so all special handlers for the LaTeX entities work
            // correctly.
            $this->templateData = str_replace('$title$', $title, $this->templateData);
            $this->templateData = str_replace('$content$', $content, $this->templateData);
            $locale = strtolower($language->getLocale());
            // Since we are generating a preview, we will write "Dear ladies and gentlemen" instead of "Dear Mr. ...".
            // We replace the html variable, so it is not replaced later on.
            $this->replaceVariable($this->translator->trans('Dear ladies and gentlemen,', [], null, $locale), $this->templateData, 'opening');
            $this->replaceVariables($currentMember, $this->templateData, $locale, $letter, []);
            $this->templateData = $this->html2Latex->convert($this->templateData);
            $fileName = '';
            if (!$simulate) {
                $fileName = $this->generatePdfFile();
            }
        } catch (Exception $e) {
            $this->logMessageCreator->createLogEntry('error', $e->getMessage());
            $this->entityManager->flush();
            throw $e;
        }

        return $fileName;
    }


    /**
     * Generate a pdf file containing serial letters for all members passed to this method.
     *
     * @param SerialLetter  $selectedLetter  Serial letter object which contains the data for the serial letter.
     * @param MemberEntry[] $selectedMembers List of members for which the serial letters are to be generated.
     * @param bool          $simulate        When true, no files are written. This can be used for tests.
     *
     * @return string Path to PDF file.
     *
     * @throws Exception
     */
    public function generatePdfLetters(SerialLetter $selectedLetter, array $selectedMembers, bool $simulate = false): string
    {
        list($header, $body, $footer) = $this->splitupTemplateDataIntoHeaderBodyFooter();
        // letterData contains the latex content, which is passed to pdflatex later on.
        $letterData = $header;

        // This is to filter out group members which have not been selected
        $selectedMembersIds = [];
        foreach ($selectedMembers as $selectedMember) {
            $selectedMembersIds[] = $selectedMember->getId();
        }

        $selectedMembers = $this->removeGroupMembersWithSameAddress($selectedMembers);

        // Insert a letter for each member in the list.
        foreach ($selectedMembers as $member) {
            $letter = $body;
            // Try to load the content for the preferred member language. If there is no letter in that language, use
            // the language of the first letter.
            $language = $selectedLetter->getContent($member->getPreferredLanguage())->getLanguage();
            $locale = strtolower($language->getLocale());

            // Underscore symbol generates an error in latex. But it cannot be replaced there, because the underscore
            // could also be used in files paths. To be able to replace it later on, we replace it by the html symbol.
            $content = $selectedLetter->getContent($language)->getContent();
            $content = str_replace('_', '&lowbar;', $content);
            $title = htmlentities($selectedLetter->getTitle($language));
            $title = str_replace('_', '&lowbar;', $title);

            $letter = str_replace('$language$', $language->getBabelName(), $letter);
            $letter = str_replace('$title$', $title, $letter);
            $letter = str_replace('$content$', $content, $letter);
            $this->addOpening($letter, $member, $locale, $selectedMembersIds);
            $this->addSignature($selectedLetter, $letter, $locale);
            $this->addNameAndAddress($letter, $member, $selectedMembersIds, $locale);
            $this->replaceVariables($member, $letter, $locale, $selectedLetter, $selectedMembersIds);


            $letterData .= $letter."\n\n";
        }
        $letterData .= $footer;
        $this->templateData = $letterData;
        $this->templateData = $this->html2Latex->convert($this->templateData);
        $fileName = '';
        if (!$simulate) {
            $fileName = $this->generatePdfFile();
        }

        return $fileName;
    }


    /**
     * Return the log file which was generated by the pdflatex run.
     *
     * @return string
     *
     * @throws Exception
     */
    public function getLogFile(): string
    {
        $logFile = $this->baseFileName.'.err';

        if (file_exists($logFile)) {
            return $logFile;
        }

        throw new Exception('Log file doesn\'t exist, pdflatex has not yet been called.');
    }


    /**
     * @param string $returnAddress
     * @param string $footer
     */
    private function initialiseTemplateFile(string $returnAddress, string $footer): void
    {
        // Replace \n in the return address and the footer by its LaTeX pendant \\
        $returnAddress = str_replace("\n", '\\\\', $returnAddress);
        $footer = str_replace("\n", '\\\\', $footer);
        $this->templateData = str_replace('$return_address$', $returnAddress, $this->templateData);
        $this->templateData = str_replace('$footer$', $footer, $this->templateData);
        $this->templateData = str_replace('$template_path$', dirname($this->templatePath), $this->templateData);
        $this->templateData = str_replace('$background$', $this->backgroundPath, $this->templateData);

        $languages = $this->entityManager->getRepository(Language::class)->findAllForSerialLetters();
        $languageStr = '';
        /** @var Language $language */
        foreach ($languages as $language) {
            $languageStr .= $language->getBabelName().',';
        }
        $languageStr = substr($languageStr, 0, -1);
        $this->templateData = str_replace('$supported_languages$', $languageStr, $this->templateData);
    }


    /**
     * Check if the pdflatex executable is found and can be called.
     *
     * @throws Exception
     */
    private function checkPdfLatex(): void
    {
        $log = array();
        $returnValue = 0;
        $command = $this->pdfLatexBin.' --version 2>&1';
        exec($command, $log, $returnValue);

        if (0  !== $returnValue) {
            throw new Exception(sprintf('pdflatex cannot be called; reason: %s.', $this->translateReturnValue($returnValue)));
        }
    }


    /**
     * Translate the pdflatex return value into human-readable text.
     *
     * @param int|null $returnValue
     *
     * @return string
     */
    private function translateReturnValue(?int $returnValue): string
    {
        switch ($returnValue) {
            case 1:
                return "general error";
            case 126:
                return "cannot execute: permissions?";
            case 127:
                return "command not found";
            default:
                return "return value $returnValue";
        }
    }


    /**
     * This method checks if the template and background file are available. This class also sets the absolute paths to
     * both files.
     *
     * @param string $template Path to the template file
     * @param string $file     Name of the file to check, e.g. latex template or background
     *
     * @return string Returns the template path
     *
     * @throws Exception Exception is thrown when the template file is not found.
     */
    private function checkTemplateFilesLocation(string $template, string $file): string
    {
        if ('' === $template) {
            throw new Exception("$file file must be set.");
        }

        // Special handler for unit tests and config files with absolute paths: when the path starts with a /, look in
        // the absolute path. If not, look in the bundles directory.
        if (0 !== strpos($template, '/')) {
            $template = "$this->projectDir/$template";
        }

        // Check paths. The following lines throw an InvalidArgumentException when the files are not found.
        if (!file_exists($template)) {
            throw new Exception(sprintf("%s file \"%s\" does not exist.", $file, $template));
        }

        return $template;
    }


    /**
     * Check if all the needed variables are present in the latex template file.
     *
     * @throws Exception When a variable is missing.
     */
    private function checkTemplateFileVariables(): void
    {
        foreach (self::NEEDED_VARIABLES as $variable) {
            if (false === strpos($this->templateData, '$'.$variable.'$')) {
                throw new Exception(sprintf('Variable $%s$ is missing in LaTeX template file %s.', $variable, $this->templatePath));
            }
        }
    }


    /**
     * Create and add the signature which contains the names of the people below the "kind regards".
     *
     * @param SerialLetter $letter  Serial letter object from which the settings are read.
     * @param string       $content Text in which the signature is to be written.
     * @param string       $locale  Locale to translate the committee and function into.
     */
    private function addSignature(SerialLetter $letter, string &$content, string $locale): void
    {
        $signatureDate = $this->getSignatureData($letter, $locale);

        $signatureStr = "{\begin{tabularx}{\\textwidth}{@{}XX@{}}";
        foreach ($signatureDate as $line) {
            $signatureStr .= "$line[0] & $line[1] \\\\ ";
        }
        $signatureStr .= "\\end{tabularx}}";

        $content = str_replace('$signature$', $signatureStr, $content);
    }


    /**
     * Create and add the address block to the serial letter content.
     *
     * @param string      $content            Text in which the signature is to be written.
     * @param MemberEntry $member             Member of which the complete namd and the main address is added. When
     *                                        there are group members with the same address, they are also added.
     * @param array       $selectedMembersIds List of members ids which are in the list for the serial letters
     * @param string      $locale             Locale to use when there is no first name
     */
    private function addNameAndAddress(string &$content, MemberEntry $member, array $selectedMembersIds, string $locale): void
    {
        $addressStr = '';
        $address = $member->getMainAddress();
        if ($address) {
            if (array_key_exists('company', $address)) {
                $addressStr .= $address['company'].'\\\\';
            }
            $addressStr .= $this->getMembersName($member, $locale).'\\\\';
            foreach ($member->getGroupMembers() as $groupMember) {
                if ($address === $groupMember->getMainAddress()) {
                    if (in_array($groupMember->getId(), $selectedMembersIds)) {
                        $addressStr .= $this->getMembersName($groupMember, $locale).'\\\\';
                    }
                }
            }
            $addressStr .= $address['address'].'\\\\';
            $addressStr .= $address['zip'].' ';
            $addressStr .= $address['city'].'\\\\';
            $addressStr .= $address['country'].'\\\\';
        } else {
            // When no address is found, enter a non breaking space
            $addressStr = '~';
        }

        $content = str_replace('$address$', $addressStr, $content);
    }



    /**
     * Write the prepared data to the latex file in the temp directory.
     *
     * @return string Path to PDF file.
     *
     * @throws Exception
     */
    private function generatePdfFile(): string
    {
        // Write processed template data to file
        $returnValue = file_put_contents($this->baseFileName.'.tex', $this->templateData);
        if (!$returnValue) {
            throw new Exception(sprintf('file_put_contents(%s.tex): failed to open stream: Permission denied', $this->baseFileName));
        }

        $returnValue = 0;
        // $consolebuffer is needed as reference for the call to exec. It is not evaluated after the call, since it
        // contains no usefull information in batchmode.
        $consoleBuffer = [];
        exec("$this->pdfLatexBin -interaction=batchmode -output-directory $this->tempDirectory $this->baseFileName.tex", $consoleBuffer, $returnValue);

        // When creating the PDF worked correctly, then just return the path to the generated PDF file.
        if (0 === $returnValue) {
            return ($this->baseFileName.'.pdf');
        }

        // There was an error during PDF creation. To help in finding the problem, reduce the log file to start with the
        // part where the error is shown. In the PDF log, the error starts with a !.
        $errorLog = '';
        $logData = file($this->baseFileName.'.log');
        $errorFound = false;
        foreach ($logData as $row) {
            // When the line starts with a !, then start writing the file content
            if (preg_match("/^!/", $row)) {
                $errorFound = true;
            }
            if ($errorFound) {
                $errorLog .= $row;
            }
        }
        file_put_contents($this->baseFileName.'.err', $errorLog);

        throw new Exception('pdflatex run failed. Call getLogFile() to get information why.');
    }


    /**
     * @param string $tempDirectory
     *
     * @throws Exception When temp directory is not writable.
     */
    private function checkTempDirectory(string $tempDirectory): void
    {
        if ('/' !== substr($tempDirectory, -1)) {
            $tempDirectory .= '/';
        }
        $this->tempDirectory = $tempDirectory;

        if (!is_writable($tempDirectory)) {
            throw new Exception(sprintf('Cannot write to temp directory %s.', $tempDirectory));
        }
    }


    /**
     * @return array
     */
    private function splitupTemplateDataIntoHeaderBodyFooter(): array
    {
        $matches = [];
        preg_match('#(.*)(\\\\begin{letter})(.*)(\\\\end{letter})(.*)#ms', $this->templateData, $matches);
        $header = $matches[1];
        $body = $matches[2].$matches[3].$matches[4];
        $footer = $matches[5];

        return array($header, $body, $footer);
    }


    /**
     * Group members with the same address only receive one letter. This method removes the group members from the
     * member list which is used to generate the serial letter.
     *
     * @param array $members
     *
     * @return array
     */
    private function removeGroupMembersWithSameAddress(array $members): array
    {
        // Since it is not possible to remove array members during a foreach loop, we first loop to find the ids of the
        // group members to remove.
        $groupMembersWithSameAddress = [];
        foreach ($members as $member) {
            // To prevent a recusion, first check if the current member is already on the list.
            if (!in_array($member->getId(), $groupMembersWithSameAddress)) {
                $address = $member->getMainAddress();
                /** @var MemberEntry $groupMember */
                foreach ($member->getGroupMembers() as $groupMember) {
                    // Group members which share the same main address are put on the list to be removed in step 2.
                    if ($address === $groupMember->getMainAddress()) {
                        $groupMembersWithSameAddress[] = $groupMember->getId();
                    }
                }
            }
        }

        // Second we generate a new array, in which only the members remain which are not on the previously generated
        // id list.
        $newMemberList = [];
        foreach ($members as $member) {
            if (!in_array($member->getId(), $groupMembersWithSameAddress)) {
                $newMemberList[] = $member;
            }
        }

        return ($newMemberList);
    }


    /**
     * Get the complete members name including the academic title (if required) and do some special handling in case the
     * first name is missing.
     *
     * @param MemberEntry $member
     * @param string      $locale
     *
     * @return string
     */
    private function getMembersName(MemberEntry $member, string $locale): string
    {
        $name = $member->getCompleteName(true);
        // When the first name is a dash, then add Mr or Ms if the gender requires this (gender n or d does not)
        if ('-' === $member->getFirstName()) {
            // Cut off the first 2 chars
            if (('f' === $member->getGender()) || ('m' === $member->getGender())) {
                $call = $this->translator->trans('f' === $member->getGender() ? 'Ms.' : 'Mr.', [], null, $locale);
                $name = "$call $name";
            }
        }

        return $name;
    }


    /**
     * Convert a php array into a latex table. The first line of the array contains the headers of the table, which will
     * translated. All other rows in the array are added to the table following the header.
     *
     * @param array $dataTable
     *
     * @return string
     */
    private function createTable(array $dataTable): string
    {
        if (count($dataTable) === 0) {
            return ('-');
        }

        // Create a table with as many columns as the data table
        $table = '\\hspace{-0.5em}\\begin{tabular}[t]{';
        $columnCount = count($dataTable[0]);
        for ($column = 0; $column < $columnCount; $column++) {
            $table .= ' l ';
        }
        // Add the header fields
        $table .= '} ';

        // Add the data fields
        for ($row = 0; $row < count($dataTable); $row++) {
            for ($column = 0; $column < $columnCount - 1; $column++) {
                $table .= $dataTable[$row][$column].' & ';
            }
            $table .= $dataTable[$row][$columnCount - 1].' \\\\\\\\ ';
        }
        $table .= '\\end{tabular}';

        return $table;
    }
}
