<?php

/*
 * Copyright (C) 2022 by Luc Hamers <vereniging@hamers.de>
 *
 * This file is part of Vereniging.
 *
 * Vereniging is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Vereniging is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Vereniging.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Service;

use App\Entity\Address;
use App\Entity\AddressType;
use App\Entity\Committee;
use App\Entity\CommitteeFunction;
use App\Entity\CommitteeFunctionMap;
use App\Entity\CompanyInformation;
use App\Entity\ComplexLogEntrySingleDataField;
use App\Entity\Country;
use App\Entity\Language;
use App\Entity\MailingList;
use App\Entity\MemberAddressMap;
use App\Entity\MemberEntry;
use App\Entity\MembershipNumber;
use App\Entity\MembershipType;
use App\Entity\PhoneNumber;
use App\Entity\PhoneNumberType;
use App\Entity\Status;
use App\Entity\Title;
use App\Entity\UserRole;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;

/**
 * This is a service class which imports an excel file into the database
 *
 * Class ImportExcelDataFile
 */
class ImportExcelDataFile
{
    private EntityManagerInterface $entityManager;
    private LogMessageCreator $logMessageCreator;
    private LogEventsDisabler $logEventsDisabler;
    private ?Spreadsheet $spreadsheet;
    private string $excelFileWithPath;
    private string $originalFileName;
    private string $errorMessage;
    private array $spreadSheetAsArray;
    private array $numbers;
    private array $selectValues;
    private int $logFileEntriesColumn;


    /**
     * GenerateExcelDataFile constructor.
     *
     * @param EntityManagerInterface $entityManager
     * @param LogMessageCreator      $logMessageCreator
     * @param LogEventsDisabler      $logEventsDisabler
     */
    public function __construct(EntityManagerInterface $entityManager, LogMessageCreator $logMessageCreator, LogEventsDisabler $logEventsDisabler)
    {
        $this->entityManager = $entityManager;
        $this->logMessageCreator = $logMessageCreator;
        $this->logEventsDisabler = $logEventsDisabler;
        $this->excelFileWithPath = '';
        $this->originalFileName = '';
        $this->errorMessage = '';
        $this->spreadsheet = null;
        $this->spreadSheetAsArray = [];
        $this->numbers = [];
        $this->selectValues = [];
    }


    /**
     * Read the number array which contains the statistics of the import
     *
     * @return array Numbers array.
     */
    public function getNumbers(): array
    {
        ksort($this->numbers);

        return $this->numbers;
    }


    /**
     * Read the array with the select values. Each entry contains those which are found and those which are new.
     *
     * @return array
     */
    public function getSelectValues(): array
    {
        return ($this->selectValues);
    }


    /**
     * Store the excel file and check if it has the correct format for importing.
     *
     * @param string      $excelFileWithPath Complete path and file name of the excel file to import.
     * @param string|null $originalFileName  Original file name of the uploaded file (in case the name has been changed
     *                                       during upload).
     *
     * @throws Exception
     */
    public function checkExcelFileForImport(string $excelFileWithPath, ?string $originalFileName = null): void
    {
        $this->checkExcelFile($excelFileWithPath, $originalFileName);
        $message = "File check for importing file $this->originalFileName was successful, ";
        $message .= $this->numbers['memberEntries'].' member entries found.';
        $this->logMessageCreator->createLogEntry('import action', $message);
        $this->entityManager->flush();
    }


    /**
     * Import the data from the excel file into the database. The file is checked before the import is started.
     *
     * @param string      $excelFileWithPath Complete path and file name of the excel file to import.
     * @param string|null $originalFileName  Original file name of the uploaded file (in case the name has been changed
     *                                       during upload).
     *
     * @throws Exception
     */
    public function importExcelFile(string $excelFileWithPath, ?string $originalFileName = null): void
    {
        $this->checkExcelFile($excelFileWithPath, $originalFileName);

        $this->logEventsDisabler->disableLifecycleEvents();
        $this->importNewSelectValues();
        $this->importMemberData();
        $this->logEventsDisabler->enableLifecycleEvents();
        $message = "Importing file $this->originalFileName was successful, ";
        $message .= $this->numbers['memberEntries'].' member entries were imported.';
        $this->logMessageCreator->createLogEntry('import action', $message);
        $this->entityManager->flush();
    }


    /**
     * Return the last error message.
     *
     * @return string
     */
    public function getErrorMessage(): string
    {
        return $this->errorMessage;
    }


    /**
     * Check if the file exists and is an excel file
     *
     * @throws Exception
     */
    private function checkFile(): void
    {
        // Check if the file exists
        if (!file_exists($this->excelFileWithPath)) {
            $this->errorMessage = 'File '.$this->originalFileName.' not found.';
            throw new Exception($this->errorMessage);
        }

        // Check if the file has the correct extension
        $fileExtension = strtolower(pathinfo($this->excelFileWithPath, PATHINFO_EXTENSION));
        if (('xls' !== $fileExtension) and ('xlsx' !== $fileExtension)) {
            $this->errorMessage = 'File '.$this->originalFileName.' is no excel file.';
            throw new Exception($this->errorMessage);
        }
    }


    /**
     * Check if the file has the correct fix headers (those which are always present)
     *
     * @throws Exception
     */
    private function checkFixHeaders(): void
    {
        // Loop over the static headers and compare the file content with it. Merged cells A1 and A2 put a value in
        // array row 0, row 1 values are null.
        foreach (ExcelDataFormats::STATIC_HEADERS as $column => $header) {
            $this->checkHeaderField(0, $column, $header);
        }
    }


    /**
     * Check if the file has the correct fix headers (those which are always present)
     *
     * @throws Exception.
     */
    private function checkDynamicHeaders(): void
    {
        $column = count(ExcelDataFormats::STATIC_HEADERS);
        $headerInfo = ExcelDataFormats::DYNAMIC_HEADERS['companyInformation'];
        $column = $this->checkHeaderFields($this->numbers['companyInformation'], $column, $headerInfo, true);
        $headerInfo = ExcelDataFormats::DYNAMIC_HEADERS['phoneNumbers'];
        $column = $this->checkHeaderFields($this->numbers['phoneNumbers'], $column, $headerInfo);
        $headerInfo = ExcelDataFormats::DYNAMIC_HEADERS['addresses'];
        $column = $this->checkHeaderFields($this->numbers['addresses'], $column, $headerInfo);
        $headerInfo = ExcelDataFormats::DYNAMIC_HEADERS['committees'];
        $column = $this->checkHeaderFields($this->numbers['committeeFunctions'], $column, $headerInfo);
        $headerInfo = ExcelDataFormats::DYNAMIC_HEADERS['groupMembers'];
        $column = $this->checkHeaderFields($this->numbers['groupMembers'], $column, $headerInfo);
        // Mailing lists have a slightly different format, so only increase the column count, don't check the content.
        $column += $this->numbers['mailingLists'] * count(ExcelDataFormats::DYNAMIC_HEADERS['mailingLists']['fields']);
        $headerInfo = ExcelDataFormats::DYNAMIC_HEADERS['logFileEntries'];
        $this->checkHeaderFields($this->numbers['logFileEntries'], $column, $headerInfo);
    }


    /**
     * Check for each column if it is a required field and if so, if all rows contain data.
     *
     * @throws Exception.
     */
    private function checkRequiredDataFields(): void
    {
        // Number of members equals the number of lines in the array minus two header lines.
        $this->numbers['memberEntries'] = count($this->spreadSheetAsArray) - 2;

        // First check all the required data cells for static fields
        for ($column = 0; $column < count(ExcelDataFormats::STATIC_HEADERS); ++$column) {
            $this->checkDataInColumn($column);
        }

        $header = ExcelDataFormats::DYNAMIC_HEADERS['companyInformation'];
        $column = $this->checkRequiredDynamicDataFields($this->numbers['companyInformation'], $column, $header);
        $header = ExcelDataFormats::DYNAMIC_HEADERS['phoneNumbers'];
        $column = $this->checkRequiredDynamicDataFields($this->numbers['phoneNumbers'], $column, $header);
        $header = ExcelDataFormats::DYNAMIC_HEADERS['addresses'];
        $column = $this->checkRequiredDynamicDataFields($this->numbers['addresses'], $column, $header);
        $header = ExcelDataFormats::DYNAMIC_HEADERS['committees'];
        $column = $this->checkRequiredDynamicDataFields($this->numbers['committeeFunctions'], $column, $header);
        $header = ExcelDataFormats::DYNAMIC_HEADERS['groupMembers'];
        $column = $this->checkRequiredDynamicDataFields($this->numbers['groupMembers'], $column, $header);
        $header = ExcelDataFormats::DYNAMIC_HEADERS['mailingLists'];
        $column = $this->checkRequiredDynamicDataFields($this->numbers['mailingLists'], $column, $header);
        $header = ExcelDataFormats::DYNAMIC_HEADERS['logFileEntries'];
        $this->logFileEntriesColumn = $column;
        $this->checkRequiredDynamicDataFields($this->numbers['logFileEntries'], $column, $header);
    }


    /**
     * Check if all gender data is either m or f.
     *
     * @throws Exception.
     */
    private function checkGenderData(): void
    {
        $genderColumn = array_search('Gender', ExcelDataFormats::STATIC_HEADERS);
        for ($row = 2; $row < $this->numbers['memberEntries'] + 2; ++$row) {
            $value = $this->spreadSheetAsArray[$row][$genderColumn];
            if (('f' !== $value) && ('m' !== $value)) {
                $this->errorMessage = "Field Gender must be m or f in cell ".$this->translateCell($genderColumn, $row).'.';
                throw new Exception($this->errorMessage);
            }
        }
    }


    /**
     * Check if the editable field in the log file entry data is a bool
     *
     * @throws Exception.
     */
    private function checkEditableData(): void
    {
        $logFileEntriesColumn = $this->logFileEntriesColumn;
        for ($row = 2; $row < $this->numbers['memberEntries'] + 2; ++$row) {
            for ($column = 0; $column < 2 * $this->numbers['logFileEntries']; $column += 2) {
                if (is_null($this->spreadSheetAsArray[$row][$logFileEntriesColumn + $column])) {
                    continue;
                }
                $value = $this->spreadSheetAsArray[$row][$logFileEntriesColumn + $column + 1];
                if (is_string($value)) {
                    $value = strtolower($value);
                    if (('true' !== $value) && ('false' !== $value)) {
                        $this->errorMessage = "Field Editable must be true or false in cell ".$this->translateCell($logFileEntriesColumn + $column + 1, $row).'.';
                        throw new Exception($this->errorMessage);
                    }
                } elseif (!is_bool($value)) {
                    $this->errorMessage = "Field Editable must be true or false in cell ".$this->translateCell($logFileEntriesColumn + $column + 1, $row).'.';
                    throw new Exception($this->errorMessage);
                }
            }
        }
    }


    /**
     * @param int   $numberOfEntries Number of entries (e.g. phone numbers)
     * @param int   $column          Column of the array where to start checking
     * @param array $headerInfo      Array with an entry from ExcelDataFormats::DYNAMIC_HEADERS
     *
     * @return int Next column where to search for other entries.
     *
     * @throws Exception
     */
    private function checkRequiredDynamicDataFields(int $numberOfEntries, int $column, array $headerInfo): int
    {
        for ($i = 0; $i < $numberOfEntries; ++$i) {
            for ($row = 2; $row < $this->numbers['memberEntries'] + 2; ++$row) {
                $this->checkDynamicFieldData($row, $column, $headerInfo['fields']);
            }
            $column += count($headerInfo['fields']);
        }

        return $column;
    }


    /**
     * This checks of all the required fields of a dynamic data block (e.g. Address 1) are present. When none of the
     * fields of the block contains any data, the check is omitted.
     *
     * @param int   $row
     * @param int   $column
     * @param array $fields
     *
     * @throws Exception
     */
    private function checkDynamicFieldData(int $row, int $column, array $fields): void
    {
        // First find out if any of the fields contains data.
        $combinedData = '';
        for ($i = $column; $i < $column + count($fields); ++$i) {
            $combinedData .= $this->spreadSheetAsArray[$row][$i];
        }
        // When none of the fields contains any data, don't check the content
        if (strlen($combinedData) === 0) {
            return;
        }

        foreach ($fields as $i => $field) {
            if (ExcelDataFormats::isRequiredField($field)) {
                if (is_null($this->spreadSheetAsArray[$row][$column + $i])) {
                    $this->errorMessage = "Field $field cannot be empty in cell ".$this->translateCell($column + $i, $row).'.';
                    throw new Exception($this->errorMessage);
                }
            }
        }
    }


    /**
     * This translates an integer column-row-value (e.g. 0, 1) into a user readable value (e.g. A2).
     *
     * @param int $column Zero-based column
     * @param int $row    Zero-based row
     *
     * @return string Cell name
     */
    private function translateCell(int $column, int $row): string
    {
        // Array row 0 is row 1 in the excel file.
        ++$row;
        if ($column < 26) {
            return (chr(ord('A') + $column).$row);
        }

        $first = floor($column / 26) - 1;
        $second = $column % 26;

        return chr(ord('A') + $first).chr(ord('A') + $second).$row;
    }


    /**
     * This method tries to find out how often the dynamic entries are present in the spreadsheet array.
     */
    private function setNumberOfDynamicEntries(): void
    {
        $this->numbers['companyInformation'] = $this->guessNumberOfEntries(ExcelDataFormats::DYNAMIC_HEADERS['companyInformation']);
        $this->numbers['phoneNumbers'] = $this->guessNumberOfEntries(ExcelDataFormats::DYNAMIC_HEADERS['phoneNumbers']);
        $this->numbers['addresses'] = $this->guessNumberOfEntries(ExcelDataFormats::DYNAMIC_HEADERS['addresses']);
        $this->numbers['committeeFunctions'] = $this->guessNumberOfEntries(ExcelDataFormats::DYNAMIC_HEADERS['committees']);
        $this->numbers['groupMembers'] = $this->guessNumberOfEntries(ExcelDataFormats::DYNAMIC_HEADERS['groupMembers']);
        $this->numbers['mailingLists'] = $this->guessNumberOfEntries(ExcelDataFormats::DYNAMIC_HEADERS['mailingLists']);
        $this->numbers['logFileEntries'] = $this->guessNumberOfEntries(ExcelDataFormats::DYNAMIC_HEADERS['logFileEntries']);
    }


    /**
     * This method tries to guess how many entries of a certain type are found in the array. Therefore it searches for
     * then number of occurrences of the header and all of the fields. Then it takes the maximum number of these. So
     * when one of them is missing, it still guesses the right value.
     *
     * @param array $headerInfo Array with an entry from ExcelDataFormats::DYNAMIC_HEADERS.
     *
     * @return int
     */
    private function guessNumberOfEntries(array $headerInfo): int
    {
        $noFound = 0;
        // Find how often the header string is found in the first line of the spread sheet. To do this, encode the first
        // line as json and do a string count
        $firstLineJson = json_encode($this->spreadSheetAsArray[0]);
        $noFound = max($noFound, substr_count($firstLineJson, $headerInfo['header']));

        // To not find a field twice (e.g. "Phone number" when there is also "Phone number type"), all found occurrences
        // are deleted from the second line json. This only works correctly when the largest field name (e.g.
        // "Phone number type" is removed before "Phone number". Therefor the list of fields has to be ordered with the
        // longest field first.
        $searchArray = $headerInfo['fields'];
        usort($searchArray, function ($a, $b) {
            return strlen($b) <=> strlen($a);
        });

        // Now search how often a field name is found in the second line of the spreadsheet array.
        $secondLineJson = json_encode($this->spreadSheetAsArray[1]);
        foreach ($searchArray as $fieldName) {
            $noFound = max($noFound, substr_count($secondLineJson, $fieldName));
            // Remove the found fields from the json array to now find it twice.
            $secondLineJson = str_replace($fieldName, "", $secondLineJson);
        }

        return $noFound;
    }


    /**
     * Check if a certain header string can be found in a certain cell.
     *
     * @param int    $row    Row in which the value is searched.
     * @param int    $column Column in which the value is searched.
     * @param string $header Header string to search for
     *
     * @throws Exception
     */
    private function checkHeaderField(int $row, int $column, string $header): void
    {
        if ($header !== $this->spreadSheetAsArray[$row][$column]) {
            $this->errorMessage = "Header error: $header not found in cell ".$this->translateCell($column, $row).'.';
            throw new Exception($this->errorMessage);
        }
    }


    /**
     * Check if all the headers for dynamic fields of a certain type (e.g. phone numbers) are found in the expected
     * cells of the spreadsheet array.
     *
     * @param int   $numberOfEntries Number of entries (e.g. phone numbers)
     * @param int   $column          Column of the array where to start checking
     * @param array $headerInfo      Array with an entry from ExcelDataFormats::DYNAMIC_HEADERS
     * @param bool  $suppressCounter When true, the main header in row 1 is written without number.
     *
     * @return int Next column where to search for other entries.
     *
     * @throws Exception
     */
    private function checkHeaderFields(int $numberOfEntries, int $column, array $headerInfo, bool $suppressCounter = false): int
    {
        for ($i = 1; $i <= $numberOfEntries; ++$i) {
            $header = $headerInfo['header'];
            if (!$suppressCounter) {
                $header .= " $i";
            }
            $this->checkHeaderField(0, $column, $header);
            foreach ($headerInfo['fields'] as $field) {
                $this->checkHeaderField(1, $column, $field);
                ++$column;
            }
        }

        return $column;
    }


    /**
     * Check if data in a given column is required. If so, check each line of this column and throw an exception when no
     * data is found.
     *
     * @param int $column Column of the array to check (0-based)
     *
     * @throws Exception
     */
    private function checkDataInColumn(int $column): void
    {
        $fieldName = $this->spreadSheetAsArray[0][$column];
        // When the field is required (i.e. when each cell of this column must contain data), then iterate over all
        // rows.
        if (ExcelDataFormats::isRequiredField($fieldName)) {
            // Row 2 is the first data row after the headers
            for ($row = 2; $row < $this->numbers['memberEntries'] + 2; ++$row) {
                if (is_null($this->spreadSheetAsArray[$row][$column])) {
                    $this->errorMessage = "Field $fieldName cannot be empty in cell ".$this->translateCell($column, $row).'.';
                    throw new Exception($this->errorMessage);
                }
            }
        }
    }


    /**
     * Search the array for values which are used in select fields (e.g. titles, status, phone number types, etc.)
     */
    private function findSelectValues(): void
    {
        $this->searchForSelectValues('Title', 'Title', 'findOneByTitle');
        $this->searchForSelectValues('User status', 'Status', 'findOneByStatus');
        $this->searchForSelectValues('Membership type', 'MembershipType', 'findOneByType');
        $this->searchForSelectValues('Phone number type', 'PhoneNumberType', 'findOneByPhoneNumberType');
        $this->searchForSelectValues('Address type', 'AddressType', 'findOneByAddressType');
        $this->searchForSelectValues('Committee', 'Committee', 'findOneByCommitteeName');
        $this->searchForSelectValues('Function', 'CommitteeFunction', 'findOneByCommitteeFunctionName');
        $this->searchForSelectValues('Committee function', 'CommitteeFunctionMap', 'findOneByCommitteeFunction', true);
        $this->searchForSelectValues('Country', 'Country', 'findOneByCountry');
        $this->searchForSelectValues('Mailing list', 'MailingList', 'findOneByListName');
    }


    /**
     * Search the excel file for a certain select value (e.g. Title) and store all found entries in the
     * $this->selectValues array under ['<header title>']['usedValues']. When this element is not yet present in the
     * database, store it in the $this->selectValues array under ['<header title>']['newValues']. These can be used
     * later on the create new database entries.
     *
     * @param string $headerName      Name of the header in the excel file to search for.
     * @param string $repositoryClass Repository class in which to search for entries.
     * @param string $searchMethod    Method in the repository class to search for entries.
     * @param bool   $useTwoColumns   When true, then the values from 2 columns is read. This is needed when reading
     *                                committee and function name (as combination)
     */
    private function searchForSelectValues(string $headerName, string $repositoryClass, string $searchMethod, bool $useTwoColumns = false): void
    {
        // When two columns are used, only search for the first column name
        $searchName = $headerName;
        if ($useTwoColumns) {
            $searchName = explode(' ', $headerName)[0];
        }

        // Search both header lines for the passed header name to find all the columns in which this data can be found.
        $searchColumns = array_keys($this->spreadSheetAsArray[0], $searchName);
        $searchColumns = array_merge($searchColumns, array_keys($this->spreadSheetAsArray[1], $searchName));

        // Search for values in all search columns.
        $foundValues = [];
        foreach ($searchColumns as $searchColumn) {
            for ($row = 2; $row < $this->numbers['memberEntries'] + 2; ++$row) {
                if (!is_null($this->spreadSheetAsArray[$row][$searchColumn])) {
                    if ($useTwoColumns) {
                        $foundValues[] = $this->spreadSheetAsArray[$row][$searchColumn].' - '.$this->spreadSheetAsArray[$row][$searchColumn + 1];
                    } else {
                        $foundValues[] = $this->spreadSheetAsArray[$row][$searchColumn];
                    }
                }
            }
        }

        // Remove double values and store in usedValues.
        $foundValues = array_unique($foundValues);
        asort($foundValues);
        $this->selectValues[$headerName]['usedValues'] = $foundValues;

        // Check for each entry if it already exists in the database. If not, store it under newValues.
        $newValues = [];
        foreach ($foundValues as $foundValue) {
            $dbValue = $this->entityManager->getRepository('App\\Entity\\'.$repositoryClass)
                                           ->$searchMethod($foundValue);
            if (is_null($dbValue)) {
                $newValues[] = $foundValue;
            }
        }
        $this->selectValues[$headerName]['newValues'] = $newValues;
    }


    /**
     * Create all select values which are missing.
     *
     * @throws Exception
     */
    private function importNewSelectValues(): void
    {
        $logMessageContent = new ComplexLogEntrySingleDataField();
        // Create new title values
        $this->storeNewSelectValuesInDatabase('Title', 'Title', [false, null, null, null], $logMessageContent);
        // Create new status values
        $this->storeNewSelectValuesInDatabase('User status', 'Status', [false, null, null, null], $logMessageContent);
        // Create new membership types
        $this->storeNewSelectValuesInDatabase('Membership type', 'MembershipType', [0, false, false, false], $logMessageContent);
        // Create new phone number types
        $this->storeNewSelectValuesInDatabase('Phone number type', 'PhoneNumberType', [null, null, null, null], $logMessageContent);
        // Create new address types
        $this->storeNewSelectValuesInDatabase('Address type', 'AddressType', [false, null, null, null], $logMessageContent);
        // Create new countries
        $this->storeNewSelectValuesInDatabase('Country', 'Country', [null, null, null, null], $logMessageContent);
        // Create new committees
        $this->storeNewSelectValuesInDatabase('Committee', 'Committee', [null, null, null, null], $logMessageContent);
        // Create new functions
        $this->storeNewSelectValuesInDatabase('Function', 'CommitteeFunction', [null, null, null, null], $logMessageContent);
        // Create new functions
        $this->storeNewSelectValuesInDatabase('Mailing list', 'MailingList', [null, null, null, null], $logMessageContent);

        // Committee and function values must be stored so the can be found be the following searches
        $this->entityManager->flush();

        // Create new committee functions
        $this->createCommitteeFunctionValues($logMessageContent);

        if (count($logMessageContent->getDataAsArray())) {
            $logMessageContent->setMainMessage('Imported the following select values:');
            $this->logMessageCreator->createLogEntry('import action', $logMessageContent);
        }
        $this->entityManager->flush();
    }


    /**
     * Store the select values of a certain type (i.e. entity class) in the database.
     *
     * @param string                         $select            Name in the select values array.
     * @param string                         $entityClass       Name of the entity class of which an object is to be
     *                                                          created (without namespace)
     * @param array                          $params            Four additional parameters which are passed to the
     *                                                          constructor. If not needed, set the unused parameters to
     *                                                          null.
     * @param ComplexLogEntrySingleDataField $logMessageContent Log data object to fill
     */
    private function storeNewSelectValuesInDatabase(string $select, string $entityClass, array $params, ComplexLogEntrySingleDataField $logMessageContent): void
    {
        $newValues = $this->selectValues[$select]['newValues'];
        $entityClass = "App\\Entity\\$entityClass";
        $newValuesString = '';
        foreach ($newValues as $newValue) {
            $newValuesString .= "$newValue, ";
            $entity = new $entityClass($newValue, $params[0], $params[1], $params[2], $params[3]);
            $this->entityManager->persist($entity);
        }
        if ('' !== $newValuesString) {
            // When new entries have been added, write this to the log message (without the trailing ", " of the last
            // entry)
            $logMessageContent->addDataField($select, [substr($newValuesString, 0, -2)]);
        }
    }


    /**
     * Create the database entries for the committee functions. This method searches fort the committee and function
     * objects. Therefor these have to be created and stored prior to calling this method. Since the strings passed to
     * the for loop are created within this class using the tostring method of the CommitteeFunctionMap, no error
     * checking is done here.
     *
     * @param ComplexLogEntrySingleDataField $logMessageContent Log data object to fill
     */
    private function createCommitteeFunctionValues(ComplexLogEntrySingleDataField $logMessageContent): void
    {
        $newCommitteeFunctionValues = $this->selectValues['Committee function']['newValues'];
        $newValuesString = '';
        foreach ($newCommitteeFunctionValues as $committeeFunctionValue) {
            $newValuesString .= "$committeeFunctionValue, ";
            $parts = explode(' - ', $committeeFunctionValue);
            $committee = $this->entityManager->getRepository(Committee::class)
                                             ->findOneBy(['committeeName' => $parts[0]]);
            /** @var CommitteeFunction $function */
            $function = $this->entityManager->getRepository(CommitteeFunction::class)
                                            ->findOneBy(['committeeFunctionName' => $parts[1]]);

            $committeeFunction = new CommitteeFunctionMap($committee, $function);
            $this->entityManager->persist($committeeFunction);
        }
        if ('' !== $newValuesString) {
            // When new entries have been added, write this to the log message (without the trailing ", " of the last
            // entry)
            $logMessageContent->addDataField('Committee function', [substr($newValuesString, 0, -2)]);
        }
    }


    /**
     * Import the member objects into the database, including all the related objects
     *
     * @throws Exception
     */
    private function importMemberData(): void
    {
        $groupMemberStartRow = 0;
        // First create an array in which the name of the static header is the key and the column is the value
        for ($row = 2; $row < $this->numbers['memberEntries'] + 2; ++$row) {
            $data = $this->spreadSheetAsArray[$row];
            // For static headers, replace the integer index by a more practical header name string.
            foreach (ExcelDataFormats::STATIC_HEADERS as $column => $value) {
                $data[$value] = $data[$column];
            }

            /** @var MemberEntry $member */
            $member = $this->entityManager->getRepository(MemberEntry::class)->findOneBy([
                'name' => $data['Name'],
                'firstName' => $data['First name'],
                'email' => $data['Email address'],
            ]);
            if (is_null($member)) {
                $member = new MemberEntry();
            } else {
                $this->clearMemberData($member);
            }

            // First set all fields which can be written directly
            $member->setGender($data['Gender']);
            $member->setName($data['Name']);
            $member->setFirstName($data['First name']);
            $member->setBirthday(new DateTime($data['Birthday']));
            $member->setEmail($data['Email address']);
            $member->setMemberSince($data['Member since']);
            $member->setMembershipEndRequested($data['Membership end requested']);

            // Now set all fields in the static header which are related to other entities
            $member->setTitle($this->entityManager->getRepository(Title::class)->findOneBy(['title' => $data['Title']]));
            $member->setPreferredLanguage($this->entityManager->getRepository(Language::class)->findOneBy(['language' => $data['Preferred language']]));
            $member->setStatus($this->entityManager->getRepository(Status::class)->findOneBy(['status' => $data['User status']]));
            $member->setMembershipType($this->entityManager->getRepository(MembershipType::class)->findOneBy(['type' => $data['Membership type']]));
            $member->setUserRole($this->entityManager->getRepository(UserRole::class)->findOneBy(['userRole' => $data['User role']]));
            $member->setMembershipNumber($this->readOrCreateMembershipNumber($data['Membership number'], $data['Direct debit']));

            $column = count(ExcelDataFormats::STATIC_HEADERS);
            $column = $this->addCompanyData($data, $column, $member);
            $column = $this->addPhoneNumberData($data, $column, $member);
            $column = $this->addAddressData($data, $column, $member);
            $groupMemberStartRow = $this->addCommitteeFunctionData($data, $column, $member);
            // Calculate how many table columns are used for the group members (mailing lists are behind that)
            $groupMemberColumns = $this->numbers['groupMembers'] * count(ExcelDataFormats::DYNAMIC_HEADERS['groupMembers']['fields']);
            $column = $this->addMailingListData($data, $groupMemberStartRow +  $groupMemberColumns, $member);
            $this->entityManager->persist($member);
            $this->addLogEntryData($data, $column, $member);
            $this->logMessageCreator->createLogEntry('import action', 'Imported member from file '.$this->originalFileName, $member);
        }

        // Store all created objects. This has to be done before group members are added, because this only works when
        // they are already persisted.
        $this->entityManager->flush();
        for ($row = 2; $row < $this->numbers['memberEntries'] + 2; ++$row) {
            $data = $this->spreadSheetAsArray[$row];
            $column = $groupMemberStartRow;
            $member = $this->entityManager->getRepository(MemberEntry::class)->findOneBy([
                'name' => $data[0],
                'firstName' => $data[1],
                'email' => $data[6],
            ]);
            for ($i = 0; $i < $this->numbers['groupMembers']; ++$i) {
                /** @var MemberEntry $groupMember */
                $groupMember = $this->entityManager->getRepository(MemberEntry::class)->findOneBy([
                    'name' => $data[$column],
                    'firstName' => $data[$column + 1],
                    'email' => $data[$column + 2],
                ]);
                if (!is_null($groupMember)) {
                    $member->addGroupMember($groupMember);
                }
                $column += 3;
            }
        }
        $this->entityManager->flush();
    }


    /**
     * Search for a membership number in the database. If found, return it. If not, create it and return the new object.
     *
     * @param int|null $number
     * @param int|null $directDebit
     *
     * @return MembershipNumber|null Found or newly created membership number
     */
    private function readOrCreateMembershipNumber(?int $number = null, ?int $directDebit = null): ?MembershipNumber
    {
        // When there is no membership number in the selected row, then set membership number to null.
        if (is_null($number)) {
            return null;
        }

        $membershipNumber = $this->entityManager->getRepository(MembershipNumber::class)->find($number);

        if (is_null($membershipNumber)) {
            $membershipNumber = new MembershipNumber();
            $membershipNumber->setMembershipNumber($number);
            $membershipNumber->setUseDirectDebit(false);
            if ($directDebit) {
                $membershipNumber->setUseDirectDebit(true);
            }
        }

        return $membershipNumber;
    }


    /**
     * @param string      $excelFileWithPath Complete path and file name of the excel file to import.
     * @param string|null $originalFileName  Original file name of the uploaded file (in case the name has been changed
     *                                       during upload).
     *
     * @throws Exception
     */
    private function checkExcelFile(string $excelFileWithPath, ?string $originalFileName = null): void
    {
        $this->excelFileWithPath = $excelFileWithPath;
        if (is_null($originalFileName)) {
            $this->originalFileName = pathinfo($this->excelFileWithPath, PATHINFO_BASENAME);
        } else {
            $this->originalFileName = $originalFileName;
        }
        try {
            $this->checkFile();
            $this->spreadsheet = IOFactory::load($this->excelFileWithPath);
            $this->spreadSheetAsArray = $this->spreadsheet->getActiveSheet()->toArray();
            $this->checkFixHeaders();
            $this->setNumberOfDynamicEntries();
            $this->checkDynamicHeaders();
            $this->checkRequiredDataFields();
            $this->checkGenderData();
            $this->checkEditableData();
            $this->findSelectValues();
        } catch (Exception $e) {
            $message = "File check for importing file $this->originalFileName failed with the following error: ".$this->errorMessage;
            $logmessage = $this->logMessageCreator->createLogEntry('import action', $message);
            $this->entityManager->persist($logmessage);

            $this->entityManager->flush();
            throw $e;
        }
    }


    /**
     * For the currently selected line of the spreadsheet array, check if there is company data and if so, generate the
     * company information objects and add it to the member we are currently creating.
     *
     * @param array       $data   Selected line from $this->spreadSheetAsArray
     * @param int         $column Column where to start reading from the array
     * @param MemberEntry $member Member object which we are creating for this line
     *
     * @return int Current column where the next data field has to be imported
     */
    private function addCompanyData(array $data, int $column, MemberEntry $member): int
    {
        if ($this->numbers['companyInformation']) {
            if ($data[$column]) {
                $company = new CompanyInformation();
                // To prevent problems with latex serial letters, ampersands have to be replaced by the html entity.
                $company->setName(str_replace('&', '&amp;', $data[$column]));
                $company->setCity($data[$column + 1]);
                $company->setDescription($data[$column + 2]);
                $company->setFunctionDescription($data[$column + 3]);
                $company->setUrl($data[$column + 4]);
                $this->entityManager->persist($company);
                $member->setCompanyInformation($company);
            }
            $column += count(ExcelDataFormats::DYNAMIC_HEADERS['companyInformation']['fields']);
        }

        return $column;
    }


    /**
     * For the currently selected line of the spreadsheet array, check if there are phone number entries and if so,
     * generate the phone number objects and add them to the member we are currently creating.
     *
     * @param array       $data   Selected line from $this->spreadSheetAsArray
     * @param int         $column Column where to start reading from the array
     * @param MemberEntry $member Member object which we are creating for this line
     *
     * @return int Current column where the next data field has to be imported
     */
    private function addPhoneNumberData(array $data, int $column, MemberEntry $member): int
    {
        for ($i = 0; $i < $this->numbers['phoneNumbers']; ++$i) {
            if ($data[$column]) {
                $phoneNumber = new PhoneNumber();
                $phoneNumber->setPhoneNumberType(
                    $this->entityManager->getRepository(PhoneNumberType::class)
                                        ->findOneBy(['phoneNumberType' => $data[$column]])
                );
                $phoneNumber->setPhoneNumber($data[$column + 1]);
                $member->addPhoneNumber($phoneNumber);
            }
            $column += count(ExcelDataFormats::DYNAMIC_HEADERS['phoneNumbers']['fields']);
        }

        return $column;
    }


    /**
     * For the currently selected line of the spreadsheet array, check if there are address entries and if so, generate
     * the address objects and add them to the member we are currently creating.
     *
     * @param array       $data   Selected line from $this->spreadSheetAsArray
     * @param int         $column Column where to start reading from the array
     * @param MemberEntry $member Member object which we are creating for this line
     *
     * @return int Current column where the next data field has to be imported
     */
    private function addAddressData(array $data, int $column, MemberEntry $member): int
    {
        for ($i = 0; $i < $this->numbers['addresses']; ++$i) {
            if ($data[$column]) {
                $address = new Address();
                $address->setAddressType(
                    $this->entityManager->getRepository(AddressType::class)
                                        ->findOneBy(['addressType' => $data[$column + 1]])
                );
                $address->setAddress($data[$column + 2]);
                $address->setZip($data[$column + 3]);
                $address->setCity($data[$column + 4]);
                $address->setCountry(
                    $this->entityManager->getRepository(Country::class)
                                        ->findOneBy(['country' => $data[$column + 5]])
                );
                $this->entityManager->persist($address);
                $addressMap = new MemberAddressMap();
                $addressMap->setAddress($address);
                $addressMap->setMemberEntry($member);
                $addressMap->setIsMainAddress('yes' === $data[$column]);
            }
            $column += count(ExcelDataFormats::DYNAMIC_HEADERS['addresses']['fields']);
        }

        return $column;
    }


    /**
     * For the currently selected line of the spreadsheet array, check if there are committee function entries. If so,
     * map them to the member.
     *
     * @param array       $data   Selected line from $this->spreadSheetAsArray
     * @param int         $column Column where to start reading from the array
     * @param MemberEntry $member Member object which we are creating for this line
     *
     * @return int Current column where the next data field has to be imported
     */
    private function addCommitteeFunctionData(array $data, int $column, MemberEntry $member): int
    {
        for ($i = 0; $i < $this->numbers['committeeFunctions']; ++$i) {
            if ($data[$column]) {
                $committeeFunction = $data[$column].' - '.$data[$column + 1];
                $committeeMap = $this->entityManager->getRepository(CommitteeFunctionMap::class)
                                                    ->findOneByCommitteeFunction($committeeFunction);
                $member->addCommitteeFunctionMap($committeeMap);
            }
            $column += count(ExcelDataFormats::DYNAMIC_HEADERS['committees']['fields']);
        }

        return $column;
    }


    /**
     * For the currently selected line of the spreadsheet array, check if there are mailing list subscriptions. If so,
     * add the mailing list subscription to the member.
     *
     * @param array       $data   Selected line from $this->spreadSheetAsArray
     * @param int         $column Column where to start reading from the array
     * @param MemberEntry $member Member object which we are creating for this line
     *
     * @return int Current column where the next data field has to be imported
     */
    private function addMailingListData(array $data, int $column, MemberEntry $member): int
    {
        for ($i = 0; $i < $this->numbers['mailingLists']; ++$i) {
            if ($data[$column]) {
                /** @var MailingList $mailingList */
                $mailingList = $this->entityManager->getRepository(MailingList::class)
                                                    ->findOneBy(['listName' => $data[$column]]);
                $member->addMailingList($mailingList);
            }
            $column += count(ExcelDataFormats::DYNAMIC_HEADERS['mailingLists']['fields']);
        }

        return $column;
    }


    /**
     * For the currently selected line of the spreadsheet array, check if there are log file entries. If so,
     * add the log file entries to the member.
     *
     * @param array       $data   Selected line from $this->spreadSheetAsArray
     * @param int         $column Column where to start reading from the array
     * @param MemberEntry $member Member object which we are creating for this line
     *
     * @throws Exception
     */
    private function addLogEntryData(array $data, int $column, MemberEntry $member)
    {
        for ($i = 0; $i < $this->numbers['logFileEntries']; ++$i) {
            if ($data[$column]) {
                $editable = $data[$column + 1];
                if (is_string($editable)) {
                    $editable = !(($editable === 'false'));
                }
                $logEntry = $this->logMessageCreator->createLogEntry($editable?'member free text':'member data change', $data[$column]);
                $logEntry->setChangesOnMember($member);
                $logEntry->setEditable($editable);
                $this->entityManager->persist($logEntry);
                $this->entityManager->flush();
            }
            $column += count(ExcelDataFormats::DYNAMIC_HEADERS['logFileEntries']['fields']);
        }
    }


    /**
     * Clear all related entities (i.e. addresses, phone numbers, committee functions, group members and company
     * information.
     *
     * @param MemberEntry $member
     */
    private function clearMemberData(MemberEntry $member): void
    {
        $phoneNumbers = $member->getPhoneNumbers();
        foreach ($phoneNumbers as $phoneNumber) {
            $member->removePhoneNumber($phoneNumber);
        }

        $addresses = $member->getMemberAddressMaps();
        /** @var MemberAddressMap $address */
        foreach ($addresses as $address) {
            $member->removeMemberAddressMap($address);
        }

        $functions = $member->getCommitteeFunctionMaps();
        foreach ($functions as $function) {
            $member->removeCommitteeFunctionMap($function);
        }

        $groupMembers = $member->getGroupMembers();
        foreach ($groupMembers as $groupMember) {
            $member->removeGroupMember($groupMember);
        }

        if ($member->getCompanyInformation()) {
            $this->entityManager->remove($member->getCompanyInformation());
        }
    }
}
