<?php

/*
 * Copyright (C) 2022 by Luc Hamers <vereniging@hamers.de>
 *
 * This file is part of Vereniging.
 *
 * Vereniging is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Vereniging is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Vereniging.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Service;

use App\Entity\ChangeType;
use App\Entity\ComplexLogEntryDoubleDataField;
use App\Entity\EmailTemplate;
use App\Entity\LogfileEntry;
use App\Entity\MemberEntry;
use App\Entity\MembershipFeeTransaction;
use App\Entity\MembershipNumber;
use App\Entity\SerialLetter;
use App\Entity\Status;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\String\ByteString;
use Throwable;

/**
 * In controllers like member lists and manage finances, the user can select members from a list and do actions like
 * change the status or generate serial letters. This service provides these actions
 *
 * Class MemberListActionsForControllers
 */
class MemberListActionsForControllers
{
    private EntityManagerInterface $entityManager;
    private SerialLetterGenerator $serialLetterGenerator;
    private LogMessageCreator $logMessageCreator;
    private LogEventsDisabler $logEventsDisabler;
    private ManageMembershipNumberInterface $membershipNumberManager;
    private MailingListUpdateInterface $mailingListUpdate;
    private SerialEmailGenerator $serialEmailGenerator;


    /**
     * MemberListActionsForControllers constructor.
     *
     * @param EntityManagerInterface          $entityManager
     * @param SerialLetterGenerator           $serialLetterGenerator
     * @param LogMessageCreator               $logMessageCreator
     * @param LogEventsDisabler               $logEventsDisabler
     * @param ManageMembershipNumberInterface $membershipNumberManager
     * @param MailingListUpdateInterface      $mailingListUpdate
     * @param SerialEmailGenerator            $serialEmailGenerator
     */
    public function __construct(EntityManagerInterface $entityManager, SerialLetterGenerator $serialLetterGenerator, LogMessageCreator $logMessageCreator, LogEventsDisabler $logEventsDisabler, ManageMembershipNumberInterface $membershipNumberManager, MailingListUpdateInterface $mailingListUpdate, SerialEmailGenerator $serialEmailGenerator)
    {
        $this->entityManager = $entityManager;
        $this->serialLetterGenerator = $serialLetterGenerator;
        $this->logMessageCreator = $logMessageCreator;
        $this->logEventsDisabler = $logEventsDisabler;
        $this->membershipNumberManager = $membershipNumberManager;
        $this->mailingListUpdate = $mailingListUpdate;
        $this->serialEmailGenerator = $serialEmailGenerator;
    }


    /**
     * Change the member status of all selected members.
     *
     * @param int[] $memberIds List of member ids for which the status shall be changed
     * @param int   $statusId  Id of status to change to
     *
     * @return Response
     *
     * @throws Exception
     */
    public function changeMemberStatus(array $memberIds, int $statusId): Response
    {
        $members = $this->entityManager->getRepository(MemberEntry::class)
                                       ->findByIdOrderedByName($memberIds);

        if (0 === count($members)) {
            return new Response(null, 204);
        }

        $status = $this->entityManager->getRepository(Status::class)->find($statusId);

        foreach ($members as $member) {
            $member->setStatus($status);
        }
        $this->entityManager->flush();

        return new Response("status updated", 204);
    }


    /**
     * Generate a PDF containing a serial letter for each of the selected members.
     *
     * @param int[] $memberIds      List of member ids for which the serial letter is to be generated
     * @param int   $serialLetterId Id of the serial letter to use
     *
     * @return Response
     *
     * @throws Exception
     */
    public function createSerialLetterAction(array $memberIds, int $serialLetterId): Response
    {
        $members = $this->entityManager->getRepository(MemberEntry::class)
                                       ->findByIdOrderedByCountryAndGender($memberIds);
        if (0 === count($members)) {
            return new Response(null, 204);
        }

        /** @var SerialLetter $serialLetter */
        $serialLetter = $this->entityManager->getRepository(SerialLetter::class)
                                            ->find($serialLetterId);

        try {
            // generatePdfPreview generates an exception when the pdf creation fails.
            $filePath =  $this->serialLetterGenerator->generatePdfLetters($serialLetter, $members);
            $logContent = "Generated serial letters (%titles%) for selected members|{$serialLetter->getTitles()}";
            $this->logMessageCreator->createLogEntry('export action', $logContent);
            $this->entityManager->flush();
        } catch (Exception $e) {
            // When there was a problem reported by pdflatex, return the log file which starts with the log file line
            // which contains the first error.
            $logFile = $this->serialLetterGenerator->getLogFile();

            return new BinaryFileResponse($logFile, 200, [
                'Cache-Control' => 'max-age=0',
                'Content-Disposition' => 'attachment;filename="letter.log"',
                'Content-Length' => filesize($logFile),
            ]);
        }

        return new BinaryFileResponse($filePath, 200, [
            'Cache-Control' => 'max-age=0',
            'Content-Disposition' => 'attachment;filename="letter.pdf"',
            'Content-Length' => filesize($filePath),
        ]);
    }


    /**
     * Generate a PDF containing a serial letter for each of the selected members.
     *
     * @param int[] $memberIds       List of member ids for which the serial letter is to be generated
     * @param int   $serialLetterId  Id of the serial letter to use
     * @param int   $emailTemplateId Id of the email template to be used
     *
     * @return Response
     *
     * @throws Throwable
     */
    public function createSerialEmailAction(array $memberIds, int $serialLetterId, int $emailTemplateId): Response
    {
        $members = $this->entityManager->getRepository(MemberEntry::class)
                                       ->findByIdOrderedByCountryAndGender($memberIds);
        if (0 === count($members)) {
            return new Response(null, 204);
        }

        /** @var SerialLetter $serialLetter */
        $serialLetter = $this->entityManager->getRepository(SerialLetter::class)
                                            ->find($serialLetterId);
        /** @var EmailTemplate $emailTemplate */
        $emailTemplate = $this->entityManager->getRepository(EmailTemplate::class)
                                             ->find($emailTemplateId);

        $this->serialEmailGenerator->sendSerialEmails($serialLetter, $members, $emailTemplate);

        $logContent = "Generated serial emails (%titles%) with email template (%template%) for selected members|{$serialLetter->getTitles()},{$emailTemplate->getTemplateNames()}";
        $this->logMessageCreator->createLogEntry('export action', $logContent);
        $this->entityManager->flush();

        return new Response("emails send", 204);
    }


    /**
     * Create membership fee requests for the selected membership numbers
     *
     * @param int[] $membershipNumbers List of membership numbers for which the membership fee requests have to be
     *                                 created.
     *
     * @return Response
     *
     * @throws Exception
     */
    public function generateMembershipFeeTransactions(array $membershipNumbers): Response
    {
        if (0 === count($membershipNumbers)) {
            return new Response(null, 204);
        }

        /** @var MembershipNumber[] $membershipNumbers */
        $membershipNumbers = $this->entityManager->getRepository(MembershipNumber::class)->findBy(['membershipNumber' => $membershipNumbers]);

        foreach ($membershipNumbers as $membershipNumber) {
            $membershipType = $membershipNumber->getMemberEntries()[0]->getMembershipType();
            $fee = new MembershipFeeTransaction();
            $fee->setAmount($membershipType->getMembershipFee());
            $fee->setMembershipNumber($membershipNumber);
            $this->entityManager->persist($fee);
            $this->generateMembershipFeeTransactionLogs(null, $fee->toArray());
        }

        $this->entityManager->flush();

        return new Response("membership fee requests generated", 204);
    }


    /**
     * Close oldest membership fee requests for the selected membership numbers
     *
     * @param int[] $membershipNumbers List of membership numbers for which the membership fee requests have to be
     *                                 closed.
     *
     * @return Response
     *
     * @throws Exception
     */
    public function closeMembershipFeeTransactions(array $membershipNumbers): Response
    {
        if (0 === count($membershipNumbers)) {
            return new Response(null, 204);
        }

        /** @var MembershipNumber[] $membershipNumbers */
        $membershipNumbers = $this->entityManager->getRepository(MembershipNumber::class)->findBy(['membershipNumber' => $membershipNumbers]);

        foreach ($membershipNumbers as $membershipNumber) {
            $membershipFeeTransactions = $membershipNumber->getMembershipFeeTransactions();
            // Find the oldest unpaid transaction and set the close date, i.e. mark it as paid
            for ($i = $membershipFeeTransactions->count() - 1; $i >= 0; $i--) {
                /** @var MembershipFeeTransaction $transaction */
                $transaction = $membershipFeeTransactions->get($i);
                if (!$transaction->getCloseDate()) {
                    $before = $transaction->toArray();
                    $transaction->setCloseDate(new DateTime());
                    $this->generateMembershipFeeTransactionLogs($before, $transaction->toArray());
                    break;
                }
            }
        }

        $this->entityManager->flush();

        return new Response("membership fee requests paid", 204);
    }


    /**
     * Add use direct debit settings to selected membership numbers
     *
     * @param int[] $membershipNumbers List of membership numbers for which the use direct debit setting is added.
     * @param bool  $useDirectDebit    Value to set useDirectDebit to
     *
     * @return Response
     *
     * @throws Exception
     */
    public function setUseDirectDebit(array $membershipNumbers, bool $useDirectDebit): Response
    {
        if (0 === count($membershipNumbers)) {
            return new Response(null, 204);
        }

        /** @var MembershipNumber[] $membershipNumbers */
        $membershipNumbers = $this->entityManager->getRepository(MembershipNumber::class)->findBy(['membershipNumber' => $membershipNumbers]);

        // When the settings for using direct debit has to be changed for a membership number, then change it and
        // generate log messages for all members under this number.
        foreach ($membershipNumbers as $membershipNumber) {
            if ($useDirectDebit !== $membershipNumber->getUseDirectDebit()) {
                $membershipNumber->setUseDirectDebit($useDirectDebit);
                $this->generateDirectDebitLogMessage($membershipNumber, $useDirectDebit);
            }
        }

        $this->entityManager->flush();

        $action = $useDirectDebit ? 'use' : 'not use';

        return new Response("set memberships to $action direct debit", 204);
    }


    /**
     * Generates a log message for each member in the membership number depending on the diff of two membership fee
     * transaction object arrays (output of the toArray method), which looks like this:
     * Created membership fee request of %value% for membership number %membershipNumber%|fee,number
     *
     * @param array|null $before Object before the change, null if the object is created
     * @param array|null $after  Object after the change, null if the object is deleted
     *
     * @throws Exception
     */
    public function generateMembershipFeeTransactionLogs(?array $before, ?array $after): void
    {
        // When the content was not changed, then do not generate any log messages
        if ($before === $after) {
            return;
        }

        $baseData = $before ?? $after;
        $createDateString = str_replace(',', '\,', $baseData['createDate']);
        $fee = number_format($baseData['amount'], 2);
        $number = $baseData['membershipNumber']->getMembershipNumber();

        if (is_null($before)) {
            $content = "Created membership fee request of %value% for membership number %membershipNumber%|$fee,$number";
        } elseif (is_null($after)) {
            $content = "Deleted membership fee request from %date% of %value% for membership number %membershipNumber%|$createDateString,$fee,$number";
        } elseif (('-' === $before['closeDate']) && ('-' !== $after['closeDate'])) {
            $content  = "Closed membership fee request from %date% of %value% for membership number %membershipNumber%|$createDateString,$fee,$number";
            // The following ensures that other changes to this transaction are also logged.
            $before['closeDate'] = $after['closeDate'];
            $this->generateMembershipFeeTransactionLogs($before, $after);
        } else {
            $content = new ComplexLogEntryDoubleDataField();
            $content->setMainMessage("Updated %classname% entry %stringIdentifier%|membershipFeeTransaction,");
            $content->addDataField('Membership number', [$number, $number]);
            $content->addDataField('Amount', [$before['amount'], $after['amount']]);
            $content->addDataField('Opened', [$before['createDate'], $after['createDate']]);
            $content->addDataField('Closed', [$before['closeDate'], $after['closeDate']]);
        }

        foreach ($baseData['membershipNumber']->getMemberEntries() as $member) {
            $this->logMessageCreator->createLogEntry('member settings change', $content, $member);
        }
    }


    /**
     * This method deletes the passed user. To do this, all foreign keys to this member must be deleted. These can be
     * in:
     * LogfileEntry - changesOnMember
     * LogfileEntry - changedByMember when this member logged in or out
     *
     * @param MemberEntry $deleteUser  Member to be deleted
     * @param MemberEntry $currentUser Member to which the log messages created by the delete member will be assigned
     *                                 to.
     *
     * @throws Exception
     *
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function deleteMember(MemberEntry $deleteUser, MemberEntry $currentUser): void
    {
        // Prevent that all following changes create new log messages on the deleted member
        $this->logEventsDisabler->disableLifecycleEvents();

        // Remove the log messages on this member
        $this->deleteLogMessages(null, null, $deleteUser);

        // Remove the log messages by this member for login and logout
        $this->deleteLogMessages('login', $deleteUser, null);

        // Assign all log messages created by the delete user to the current user
        $this->updateLogMessagesChangedByUser($deleteUser, $currentUser);

        // Remove all serial letter signatures and set last updated by to the current user
        $this->updateSerialLetters('FirstSignatureMember', $deleteUser, null);
        $this->updateSerialLetters('SecondSignatureMember', $deleteUser, null);
        $this->updateSerialLetters('LastUpdatedBy', $deleteUser, $currentUser);

        // Remove or update all log messages containing the full name of the deleted user as text in group changes
        /** @var ChangeType $memberDataChangeType */
        $memberDataChangeType = $this->entityManager->getRepository(ChangeType::class)
                                                    ->findOneBy(["changeType" => 'member data change']);
        /** @var LogfileEntry[] $logMessages */
        $logMessages = $this->entityManager->getRepository(LogfileEntry::class)->findAllByParameters(10000, $memberDataChangeType, null, $deleteUser->getCompleteName(), null, null);
        $this->updateLogMessagesWithGroupChanges($deleteUser, $logMessages, 'Membership group members');

        // Remove or update all log messages containing the full name of the delete user as text in serial letter
        // changes.
        /** @var ChangeType $serialLetterChangeType */
        $serialLetterChangeType = $this->entityManager->getRepository(ChangeType::class)
                                                      ->findOneBy(["changeType" => 'serial letter change']);
        /** @var LogfileEntry[] $logMessages */
        $logMessages = $this->entityManager->getRepository(LogfileEntry::class)->findAllByParameters(10000, $serialLetterChangeType, null, $deleteUser->getCompleteName(), null, null);
        $this->updateLogMessagesWithGroupChanges($deleteUser, $logMessages, 'firstSignatureMember');
        $this->updateLogMessagesWithGroupChanges($deleteUser, $logMessages, 'secondSignatureMember');

        // When this user has a membership number and he is the only one on this number, then remove the membership fee
        // transactions and release the membership number.
        if (($deleteUser->getMembershipNumber()) && (1 === count($deleteUser->getMembershipNumber()->getMemberEntries()))) {
            foreach ($deleteUser->getMembershipNumber()->getMembershipFeeTransactions() as $transaction) {
                $this->entityManager->remove($transaction);
            }

            $this->membershipNumberManager->removeMembershipNumber($deleteUser);
        }

        foreach ($deleteUser->getMailingLists() as $mailingList) {
            $this->mailingListUpdate->removeEmailAddress($mailingList, $deleteUser);
        }

        $message = "Deleted user with database identifier %id%|".$deleteUser->getId();
        $this->logMessageCreator->createLogEntry('member data change', $message);
        $this->entityManager->remove($deleteUser);
        $this->entityManager->flush();
        $this->logEventsDisabler->enableLifecycleEvents();
    }


    /**
     * Generate a log message with the following content depending on the $userDirectDebit variable for all members
     * under this passed membership number:
     * 'Use direct debit for membership number 123'
     * 'Don't use direct debit for membership number 123'
     *
     * @param MembershipNumber $membershipNumber
     * @param bool             $useDirectDebit
     *
     * @throws Exception
     */
    private function generateDirectDebitLogMessage(MembershipNumber $membershipNumber, bool $useDirectDebit): void
    {
        foreach ($membershipNumber->getMemberEntries() as $memberEntry) {
            $message = $useDirectDebit ? 'Use ' : 'Don\'t use ';
            $message .= 'direct debit for membership number %membershipNumber%|'.$membershipNumber->getMembershipNumber();
            $this->logMessageCreator->createLogEntry('member settings change', $message, $memberEntry);
        }
    }


    /**
     * Delete log messages from the database, based on search criteria
     *
     * @param String|null      $changeType
     * @param MemberEntry|null $changedBy
     * @param MemberEntry|null $changesOn
     */
    private function deleteLogMessages(?string $changeType, ?MemberEntry $changedBy, ?MemberEntry $changesOn): void
    {
        // Replace the string change type by its database entity
        if ($changeType) {
            /** @var ChangeType $changeType */
            $changeType = $this->entityManager->getRepository(ChangeType::class)
                                              ->findOneBy(["changeType" => $changeType]);
        }

        // Remove all log messages based on the search criteria. Do this with 100 messages at a time to not overload the
        //  database.
        do {
            $logMessages = $this->entityManager->getRepository(LogfileEntry::class)->findAllByParameters(100, $changeType, null, null, $changedBy, $changesOn);
            foreach ($logMessages as $logMessage) {
                $this->entityManager->remove($logMessage);
            }
            $this->entityManager->flush();
        } while (count($logMessages) > 0);
    }


    /**
     * Update the log messages in the database to the current user where the changes were done by the delete user.
     *
     * @param MemberEntry $deleteUser  User which is to be deleted
     * @param MemberEntry $currentUser User to assign the log messages to
     */
    private function updateLogMessagesChangedByUser(MemberEntry $deleteUser, MemberEntry $currentUser): void
    {
        do {
            $logMessages = $this->entityManager->getRepository(LogfileEntry::class)->findAllByParameters(100, null, null, null, $deleteUser, null);
            foreach ($logMessages as $logMessage) {
                $logMessage->setChangedByMember($currentUser);
            }
            $this->entityManager->flush();
        } while (count($logMessages) > 0);
    }


    /**
     * Find log messages where the user to be deleted has been added or removed from a group. There are three scenarios,
     * where the log message contains the delete user name in the group message:
     * 1. No other changes or group members --> delete the log message.
     * 2. No other group members, but other changes --> remove the group part of the log message.
     * 3. A log message contains other group members --> remove the delete member from the group list.
     *
     * @param MemberEntry    $deleteUser   User which is to be deleted
     * @param LogfileEntry[] $logMessages  List of log messages to search through
     * @param string         $changeString Change string in the complex log message to check
     */
    private function updateLogMessagesWithGroupChanges(MemberEntry $deleteUser, array $logMessages, string $changeString): void
    {
        foreach ($logMessages as $logMessage) {
            if ($logMessage->isComplexLogentry()) {
                $fields = $logMessage->getComplexLogEntry()->getDataAsArray(false);
                // When the field membership group members exists, remove the members name from both fields
                if (array_key_exists($changeString, $fields)) {
                    $complexLogEntry = $logMessage->getComplexLogEntry();
                    $complexLogEntry->addDataField($changeString, [
                        $this->removeGroupMember($deleteUser, $fields[$changeString][0]),
                        $this->removeGroupMember($deleteUser, $fields[$changeString][1]),
                    ]);
                    $logMessage->setLogentry($complexLogEntry);
                    $fields = $logMessage->getComplexLogEntry()->getDataAsArray(false);
                    // When the delete member is the only member in the group list and the group list is the only field
                    // in the log message, then delete the message
                    if ([$changeString => ['-', '-']] === $fields) {
                        $this->entityManager->remove($logMessage);
                    // When there are other fields, but the group fields are empty, then remove just this field
                    } elseif (['-', '-'] === $fields[$changeString]) {
                        $complexLogEntry->removeDataField($changeString);
                    }
                    $logMessage->setLogentry($complexLogEntry);
                }
            }
        }
    }


    /**
     * Remove the member name from a string list with member names.
     *
     * @param MemberEntry $deleteMember Member to remove from the list
     * @param string      $memberList   Comma and space separated list of member names
     *
     * @return string Comma separated list without the passed member
     */
    private function removeGroupMember(MemberEntry $deleteMember, string $memberList): string
    {
        $groupMembers = explode(', ', $memberList);
        if (($key = array_search($deleteMember->getCompleteName(), $groupMembers)) !== false) {
            unset($groupMembers[$key]);
        }

        $groupMembers = implode(', ', $groupMembers);
        if ('' === $groupMembers) {
            $groupMembers = '-';
        }

        return $groupMembers;
    }


    /**
     * Search for serial letters with properties of the delete user and replace them by another user or null.
     *
     * @param string           $property      Serial letters property (i.e. FirstSignatureMember, SecondSignatureMember,
     *                                        LastUpdatedBy) to search for and to replace
     * @param MemberEntry      $deleteUser    User to search for in these properties
     * @param MemberEntry|null $replaceByUser Value to replace the user by
     */
    private function updateSerialLetters(string $property, MemberEntry $deleteUser, ?MemberEntry $replaceByUser): void
    {
        $camelCaseProperty = (new ByteString($property))->camel()->toString();
        /** @TODO  Use pagination here */
        $serialLetters = $this->entityManager->getRepository(SerialLetter::class)
                                             ->findBy([$camelCaseProperty => $deleteUser->getId()]);

        foreach ($serialLetters as $serialLetter) {
            $serialLetter->{"set$property"}($replaceByUser);
        }
    }
}
