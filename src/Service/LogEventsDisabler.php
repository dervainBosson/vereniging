<?php

/*
 * Copyright (C) 2022 by Luc Hamers <vereniging@hamers.de>
 *
 * This file is part of Vereniging.
 *
 * Vereniging is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Vereniging is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Vereniging.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Service;

/**
 * With this class (as a service) it is possible to disable doctrines lifecycle events for the entity logger. This is
 * used for inserting the fixture data.
 *
 * Class LogEventsDisabler
 */
class LogEventsDisabler
{
    private bool $disableEvents;


    /**
     * LogEventsDisabler constructor.
     */
    public function __construct()
    {
        $this->disableEvents = false;
    }


    /**
     * Call this method to disable doctrines log entity events.
     */
    public function disableLifecycleEvents(): void
    {
        $this->disableEvents = true;
    }


    /**
     * Call this method to enable doctrines log entity events.
     */
    public function enableLifecycleEvents(): void
    {
        $this->disableEvents = false;
    }


    /**
     * Returns true when the entity logger lifecycle events should be disabled.
     *
     * @return bool
     */
    public function lifeCycleEventsAreDisabled(): bool
    {
        return $this->disableEvents;
    }
}
