<?php

/*
 * Copyright (C) 2022 by Luc Hamers <vereniging@hamers.de>
 *
 * This file is part of Vereniging.
 *
 * Vereniging is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Vereniging is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Vereniging.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Service;

use App\Entity\EmailTemplate;
use App\Entity\MemberEntry;
use App\Entity\SerialLetter;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * Send serial emails based on serial letters
 *
 * Class SerialEmailGenerator
 */
class SerialEmailGenerator
{
    use SerialLetterEmailTrait;

    private EntityManagerInterface $entityManager;
    private MailerInterface $mailer;
    private TranslatorInterface $translator;
    private LogMessageCreator $logMessageCreator;


    /**
     * @param EntityManagerInterface $entityManager
     * @param MailerInterface        $mailer
     * @param TranslatorInterface    $translator
     * @param LogMessageCreator      $logMessageCreator
     */
    public function __construct(EntityManagerInterface $entityManager, MailerInterface $mailer, TranslatorInterface $translator, LogMessageCreator $logMessageCreator)
    {
        $this->entityManager = $entityManager;
        $this->mailer = $mailer;
        $this->translator = $translator;
        $this->logMessageCreator = $logMessageCreator;
    }


    /**
     *
     *
     * @param SerialLetter  $selectedLetter
     * @param MemberEntry[] $selectedMembers
     * @param EmailTemplate $emailTemplate
     *
     * @return Email[] Return the list of email objects to be able to test the content of the emails
     *
     * @throws TransportExceptionInterface
     * @throws Exception
     */
    public function sendSerialEmails(SerialLetter $selectedLetter, array $selectedMembers, EmailTemplate $emailTemplate): array
    {
        // This is to filter out group members which have not been selected
        $selectedMembersIds = [];
        foreach ($selectedMembers as $selectedMember) {
            $selectedMembersIds[] = $selectedMember->getId();
        }

        $selectedMembers = $this->removeGroupMembersWithSameAddress($selectedMembers);

        $emails = [];
        foreach ($selectedMembers as $member) {
            $emailAddress = $member->getEmail();
            if ($emailAddress) {
                $language = $selectedLetter->getContent($member->getPreferredLanguage())->getLanguage();
                $locale = strtolower($language->getLocale());
                // Select the serial letter content in the language for the current member
                $body = $selectedLetter->getContent($language)->getContent();

                // Write the content into the serial letters template
                $body = str_replace(EmailTemplate::HTML_CONTENT_VARIABLE, $body, $emailTemplate->getContent());

                // Replace all variables in the template and content
                $this->replaceVariables($member, $body, $locale, $selectedLetter, $selectedMembersIds);

                $email = (new Email())
                    ->from($emailTemplate->getSenderAddress($locale))
                    ->to($emailAddress)
                    ->subject($selectedLetter->getTitle($language))
                    ->text(strip_tags($body))
                    ->html($body);
                try {
                    $this->mailer->send($email);
                    $emails[] = $email;
                } catch (TransportExceptionInterface $e) {
                    $message = "Sending serial email failed with the following error: ".$e->getMessage();
                    // Since we have a problem sending emails, disable them for the coming log message
                    $this->logMessageCreator->disableEmailNotifications();
                    $this->logMessageCreator->createLogEntry('error', $message);
                    $this->logMessageCreator->enableEmailNotifications();
                    $this->entityManager->flush();

                    throw $e;
                }
            }
        }

        return $emails;
    }


    /**
     * Convert a php array into an html table. The first line of the array contains the headers of the table, which will
     * translated. All other rows in the array are added to the table following the header.
     *
     * @param array $dataTable
     *
     * @return string
     */
    private function createTable(array $dataTable): string
    {
        if (count($dataTable) === 0) {
            return ('-');
        }

        // Create a table with as many columns as the data table
        $table = "<table>";

        // Add the data fields
        foreach ($dataTable as $tableRow) {
            $table .= "<tr>";
            foreach ($tableRow as $tableCell) {
                $table .= "<td>$tableCell</td>";
            }
            $table .= "</tr>";
        }
        $table .= "</table>";

        return $table;
    }


    /**
     * Create and add the signature which contains the names of the people below the "kind regards".
     *
     * @param SerialLetter $letter  Serial letter object from which the settings are read.
     * @param string       $content Text in which the signature is to be written.
     * @param string       $locale  Locale to translate the committee and function into.
     */
    private function addSignature(SerialLetter $letter, string &$content, string $locale): void
    {
        $signatureData = $this->getSignatureData($letter, $locale);
        $signatureStr = $this->createTable($signatureData);

        $content = str_replace('$signature$', $signatureStr, $content);
    }


    /**
     * Group members with the same or no email address only receive one email. This method removes the group members
     * from the member list which is used to generate the serial email.
     *
     * @param MemberEntry[] $members
     *
     * @return array
     */
    private function removeGroupMembersWithSameAddress(array $members): array
    {
        // Since it is not possible to remove array members during a foreach loop, we first loop to find the ids of the
        // group members to remove.
        $groupMembersWithSameOrNoAddress = [];
        foreach ($members as $member) {
            // To prevent a recursion, first check if the current member is already on the list.
            if (!in_array($member->getId(), $groupMembersWithSameOrNoAddress)) {
                $address = $member->getEmail();
                /** @var MemberEntry $groupMember */
                foreach ($member->getGroupMembers() as $groupMember) {
                    // Group members which share the same or no email address are put on the list to be removed in
                    // step 2.
                    if (($address === $groupMember->getEmail()) || (is_null($groupMember->getEmail()))) {
                        $groupMembersWithSameOrNoAddress[] = $groupMember->getId();
                    }
                }
            }
        }

        // Second we generate a new array, in which only the members remain which are not on the previously generated
        // id list.
        $newMemberList = [];
        foreach ($members as $member) {
            if (!in_array($member->getId(), $groupMembersWithSameOrNoAddress)) {
                $newMemberList[] = $member;
            }
        }

        return ($newMemberList);
    }


    /**
     * Insert the opening line, e.g. Dear Mr. A, dear Ms. B,
     * If a group member is not in the list of selected members, then he shall not get an email. Also when he has an
     * email address (those with identical and null addresses have been removed before), then they need to be excluded
     * from the opening list.
     *
     * @param string      $content            Text in which the opening is to be written.
     * @param MemberEntry $member             Member to which the letter is to be send.
     * @param string      $locale
     * @param array       $selectedMembersIds List of members ids which are in the list for the serial letters
     */
    private function addOpening(string &$content, MemberEntry $member, string $locale, array $selectedMembersIds): void
    {
        $opening = $this->createOpening($member, $locale, true);
        $emailAddress = $member->getEmail();
        foreach ($member->getGroupMembers() as $groupMember) {
            // If the group member has its own email address, he will get his own email and is not in this one
            if (($emailAddress === $groupMember->getEmail()) || (is_null($groupMember->getEmail()))) {
                // Only have group members which have been selected in the first place
                if (in_array($groupMember->getId(), $selectedMembersIds)) {
                    // If the email for this group member is null, then add an email address here (and not save it) to
                    // prevent it to be included in following emails of this run
                    $groupMember->setEmail($emailAddress);
                    // For each added group member, the translated text starts with a small letter.
                    $opening .= $this->createOpening($groupMember, $locale, false);
                }
            }
        }

        $content = str_replace('$opening$', $opening, $content);
    }
}
