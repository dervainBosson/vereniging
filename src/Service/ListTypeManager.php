<?php

/*
 * Copyright (C) 2022 by Luc Hamers <vereniging@hamers.de>
 *
 * This file is part of Vereniging.
 *
 * Vereniging is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Vereniging is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Vereniging.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Service;

use App\Entity\EntityTranslationInterface;
use App\Entity\Language;
use App\Entity\Status;
use App\Form\FormCollection;
use App\Form\FormCollectionType;
use App\Form\ValueFormType;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\Mapping\MappingException;
use Exception;
use ReflectionException;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * This service handles the management of list types, i.e. types which can be edited by the sites administrator, like
 * academic titles and address types.
 *
 * Class ListTypeManager
 */
class ListTypeManager
{
    private FormFactoryInterface $formFactory;
    private EntityManagerInterface $entityManager;
    private string $defaultLocale;
    private array $locales;
    private string $mainFieldName;
    private string $typeWithNamespace;
    private array $extraFields;


    /**
     * Create ListTypeManager object.
     *
     * @param FormFactoryInterface   $formFactory
     * @param EntityManagerInterface $entityManager
     * @param string                 $defaultLocale
     */
    public function __construct(FormFactoryInterface $formFactory, EntityManagerInterface $entityManager, string $defaultLocale)
    {
        $this->formFactory   = $formFactory;
        $this->entityManager = $entityManager;
        $this->defaultLocale = $defaultLocale;
        $this->locales = $this->entityManager->getRepository(Language::class)->findAllLocales($this->defaultLocale);
        $this->mainFieldName = '';
        $this->typeWithNamespace = '';
        $this->extraFields = [];
    }


    /**
     * Create the headers for the type objects columns. When the main column is not to be translated, then there will be
     * only the headers for the class fields, except "id" and "slug". When the main column has to be translated (i.e.
     * when the objects uses the Translatable behaviour, then the main field is generated for each language. To be able
     * to translate the headers, they are generated as string looking like "field value|locale".
     *
     * @param string $type Entity type (without namespace) for which the headers are to be generated.
     *
     * @return array Headers for the table
     *
     * @throws MappingException
     * @throws ReflectionException
     */
    public function generateHeaders(string $type): array
    {
        $this->typeWithNamespace = 'App\Entity\\'.$type;
        // Read all the headers for the given object and remove the headers "id" and "slug"
        $headers = $this->entityManager->getMetadataFactory()
                                       ->getMetadataFor($this->typeWithNamespace)
                                       ->getFieldNames();
        unset($headers[array_search('id', $headers)]);
        unset($headers[array_search('slug', $headers)]);
        // Unset left the array with non-continuous indexes. To fix this, create a new array.
        $headers = array_values($headers);
        $this->mainFieldName = $headers[0];

        // If entity value has to be translated, change the main header to be translated with the default locale and
        // add headers for all the other locales.
        $this->extraFields = [];
        if ($this->isTranslatable($type)) {
            $headerWithLocales = [];
            foreach ($this->locales as $locale) {
                $headerWithLocales[] = $this->addMainFieldWithLocale($locale);
            }
            array_splice($headers, 0, 1, $headerWithLocales);
        }

        return $headers;
    }


    /**
     * Return the main field name for the current entity.
     *
     * @return string
     */
    public function getMainFieldName(): string
    {
        return $this->mainFieldName;
    }


    /**
     * Return a list of the names of the additional form fields which have been added for the translation of the
     * locales.
     *
     * @return array|string[]
     */
    public function getExtraFields(): array
    {
        return $this->extraFields;
    }


    /**
     * Generate a form for the passed type. This forms is a collection (FormCollection) containing the form for the
     * selected type. For this, the ValueFormType is used, which generates a form for all fields of the selected type
     * plus translation fields for each locale, if needed.
     *
     * @return FormInterface
     */
    public function generateForm(): FormInterface
    {
        $entries = $this->entityManager->getRepository($this->typeWithNamespace)->findAll();
        $listCollection = new FormCollection($entries, $this->entityManager);
        $form = $this->formFactory->create(FormCollectionType::class, $listCollection, [
            'entry_class' => ValueFormType::class,
            // The following options are passed to the ValueFormType
            'entry_class_options' => [
                'entityManager' => $this->entityManager,
                'locales' => $this->locales,
                'data_class' => $this->typeWithNamespace,
            ],
        ]);

        return $form;
    }


    /**
     * Read the locales, the default locale is the first one in the list.
     *
     * @return string[]
     */
    public function getLocales(): array
    {
        return $this->locales;
    }


    /**
     * This method handles calls from the edit controller for forms managed by this service.
     *
     * @param Request $request      Request from the form
     * @param array   $selectedType Type array containing all the settings for this type
     *
     * @return bool
     */
    public function handleForm(Request $request, array $selectedType): bool
    {
        $form = $selectedType['form'];
        $form->handleRequest($request);

        $edit = true;
        if ($form->isSubmitted() && $form->isValid()) {
            // The form data is already bound to the object, so the flush will store all its data. But when there are
            // extra fields for the locales, then these must be handles separately.
            if (count($selectedType['extraFields'])) {
                foreach ($form['members'] as $subForm) {
                    /** @var EntityTranslationInterface $entity */
                    $entity = $subForm->getData();

                    $mainGetter = 'get'.ucfirst($selectedType['mainFieldName']);
                    $isMainValueNull = $this->correctMainValueNull($mainGetter, $entity);

                    if (!$isMainValueNull) {
                        $translationFields = $selectedType['extraFields'];
                        $this->generateTranslations($translationFields, $subForm, $entity);
                    }
                }
            }

            $this->entityManager->flush();
            $edit = false;
        }

        return $edit;
    }


    /**
     * When the main value is null, then the object cannot be saved to the database, as well as the default translation.
     * To fix this, revert both objects to their original state when the main value is null.
     *
     * @param string                     $mainGetter Method name to get the main value
     * @param EntityTranslationInterface $entity
     *
     * @return bool
     */
    private function correctMainValueNull(string $mainGetter, EntityTranslationInterface $entity): bool
    {
        $isMainValueNull = false;

        // Special handler for when the main value has been cleared: reset both the entity and the
        // default translation. But do this only, when this is not a new entity (i.e. the id is not null).
        if ((is_null($entity->$mainGetter())) && (!is_null($entity->getId()))) {
            /** @var Status $entity */
            $this->entityManager->refresh($entity);
            $this->entityManager->refresh($entity->translate($entity->getDefaultLocale()));
            $isMainValueNull = true;
        }

        return $isMainValueNull;
    }


    /**
     * Add translations from the current sub form to the entity.
     *
     * @param string[]                   $translationFields
     * @param FormInterface              $subForm
     * @param EntityTranslationInterface $entity
     */
    private function generateTranslations(array $translationFields, FormInterface $subForm, EntityTranslationInterface $entity): void
    {
        try {
            foreach ($translationFields as $fieldName) {
                $translation = $subForm->get($fieldName)->getData();
                if ($translation) {
                    // The field name looks like "entityname_locale", e.g. "status_en". Use explode to get the locale.
                    $fieldParts = explode('_', $fieldName);
                    // Locale is in the last part
                    $locale = $fieldParts[count($fieldParts) - 1];
                    $entity->addNewTranslation($locale, $translation);
                }
            }
        } catch (Exception $exception) {
            // Left intentionally blank. The $subForm->get could trow an exception, when there are no
            // translation fields. If so, just do nothing
        }
    }


    /**
     * In the twig output for the header fields, there is a special handler for translating a field with several
     * locales. This methods writes the main field with the locale in this special format.
     *
     * @param string $locale
     *
     * @return string
     */
    private function addMainFieldWithLocale(string $locale): string
    {
        if ($this->defaultLocale !== $locale) {
            $this->extraFields[] = $this->mainFieldName.'_'.$locale;
        }

        return $this->mainFieldName.'|'.$locale;
    }


    /**
     * Returns true if the entity type uses the Translatable behaviour.
     *
     * @param string $type
     *
     * @return bool
     */
    private function isTranslatable(string $type): bool
    {
        // To check if entity value must be translated, check if it used the Translatable behaviour by searching for one
        // of the properties defined there.
        return property_exists('App\Entity\\'.$type, 'translations');
    }
}
