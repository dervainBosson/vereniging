<?php

/*
 * Copyright (C) 2022 by Luc Hamers <vereniging@hamers.de>
 *
 * This file is part of Vereniging.
 *
 * Vereniging is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Vereniging is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Vereniging.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Service;

use App\Entity\CommitteeFunctionMap;
use App\Entity\MemberEntry;
use App\Entity\Status;
use App\Form\MemberListsSelectionType;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Component\Form\Form;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * This provides not only the member selection form (filter by status, user role, membership type, committee and several
 * other fields, but also does the complete form handling.
 *
 * Class MemberListSelection
 */
class MemberSelectionFormHandler
{
    private EntityManagerInterface $entityManager;
    private FormFactoryInterface $formFactory;
    private ?Form $form;
    private array $settings;


    /**
     * MemberListSelection constructor.
     *
     * @param EntityManagerInterface $entityManager
     * @param FormFactoryInterface   $formFactory
     */
    public function __construct(EntityManagerInterface $entityManager, FormFactoryInterface $formFactory)
    {
        $this->entityManager = $entityManager;
        $this->formFactory = $formFactory;
        $this->form = null;
        $this->settings = [
            'currentLocale' => 'en',
            'statusWithMembershipNumberOnly' => false,
            'omitFields' => [],
        ];
    }


    /**
     * @param bool $withStatusSelected When true, the form is created with the member status selected.
     *
     * @return Form|null
     */
    public function generateForm(bool $withStatusSelected = false): ?Form
    {
        $formParameters = [];
        if ($withStatusSelected) {
            $memberStatus = $this->entityManager->getRepository(Status::class)
                                                ->findOneBy(['status' => 'member']);
            $formParameters['selectedStatus'] = $memberStatus;
        }

        $formParameters['currentLocale'] = $this->settings['currentLocale'];
        $formParameters['statusWithMembershipNumberOnly'] = $this->settings['statusWithMembershipNumberOnly'];
        $this->form = $this->formFactory->create(MemberListsSelectionType::class, null, $formParameters);
        foreach ($this->settings['omitFields'] as $removeField) {
            $this->form->remove($removeField);
        }

        return $this->form;
    }


    /**
     * @param Request $request
     *
     * @return array
     */
    public function handleFormRequest(Request $request): array
    {
        $this->form->handleRequest($request);

        $selectionData = [];
        $members = [];
        if ($this->form->isSubmitted() && $this->form->isValid()) {
            // Iterate over the data fields of the form and generate an array which contains the selected for values.
            foreach ($this->form->getData() as $fieldName => $fieldData) {
                $selectionData = $this->convertDataField($fieldName, $fieldData, $selectionData);
            }

            // Special handler for committees since these are not searchable for member entries.
            $selectionData = $this->replaceCommitteesByMemberIds($selectionData);
            $members = $this->entityManager->getRepository(MemberEntry::class)->findByCriteria($selectionData);
        }

        return $members;
    }


    /**
     * Change the options used for the member selection form
     *
     * @param string $key
     * @param $value
     *
     * @return MemberSelectionFormHandler
     *
     * @throws Exception
     */
    public function setOption(string $key, $value): MemberSelectionFormHandler
    {
        if (!array_key_exists($key, $this->settings)) {
            throw new Exception(sprintf('Trying to set key %s which does not exists!', $key));
        }

        $this->settings[$key] = $value;

        return $this;
    }


    /**
     * The findBy method for member entries cannot search for committees in which the member is a member. To be able to
     * include the committees in the member search, all members which are member of the selected committees are added to
     * the selection list by their id.
     *
     * @param array $selectionData
     *
     * @return array
     */
    private function replaceCommitteesByMemberIds(array $selectionData): array
    {
        if (array_key_exists('committee', $selectionData)) {
            $committeeFunctionMaps = $this->entityManager->getRepository(CommitteeFunctionMap::class)
                ->findBy(['committee' => $selectionData['committee']]);
            unset($selectionData['committee']);
            $ids = [];
            foreach ($committeeFunctionMaps as $committeeFunctionMap) {
                foreach ($committeeFunctionMap->getMemberEntries() as $memberEntry) {
                    $ids[] = $memberEntry->getId();
                }
            }

            // Only add the id to selection data when at least one member is found in the current committee selection.
            if (count($ids) > 0) {
                $selectionData['id'] = $ids;
            }
        }

        return $selectionData;
    }


    /**
     * This converts an entry from the request data to a format the database selector understands, e.g.:
     * string
     * bool
     * array
     * ArrayCollection
     * The data is added to the $selectionData array.
     *
     * @param string $fieldName
     * @param mixed  $fieldData
     * @param array  $selectionData
     *
     * @return array
     */
    private function convertDataField(string $fieldName, $fieldData, array $selectionData): array
    {
        if (!$fieldData) {
            return $selectionData;
        }
        // The email selection value is a string or a bool
        if ('others' === $fieldName) {
            // When the membership end request is not set, then remove it to prevent that member where this flag is set,
            // are removed from the normal request.
            if (false === $fieldData['membershipEndRequested']) {
                unset($fieldData['membershipEndRequested']);
            }
            $selectionData = array_merge($selectionData, $fieldData);
        } elseif (is_string($fieldData) || is_bool($fieldData)) {
            $selectionData[$fieldName] = $fieldData;
        // All other fields are arrays of data (i.e. array or ArrayCollection)
        } elseif (count($fieldData)) {
            $contentArray = [];
            foreach ($fieldData as $field) {
                $contentArray[] = $field;
            }
            $selectionData[$fieldName] = $contentArray;
        }

        return $selectionData;
    }
}
