<?php

/*
 * Copyright (C) 2022 by Luc Hamers <vereniging@hamers.de>
 *
 * This file is part of Vereniging.
 *
 * Vereniging is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Vereniging is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Vereniging.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * PhoneNumber
 *
 * @ORM\Entity(repositoryClass="App\Repository\PhoneNumberRepository")
 */
class PhoneNumber implements EntityLoggerInterface
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=45)
     */
    protected $phoneNumber;

    /**
     * @ORM\ManyToOne(targetEntity="PhoneNumberType", inversedBy="phoneNumbers")
     * @ORM\JoinColumn(name="phone_number_type_id", referencedColumnName="id", nullable=false)
     *
     * @var PhoneNumberType $phoneNumber
     */
    protected $phoneNumberType;

    /**
     * @ORM\ManyToOne(targetEntity="MemberEntry", inversedBy="phoneNumbers")
     * @ORM\JoinColumn(name="member_entry_id", referencedColumnName="id", nullable=false)
     */
    protected $memberEntry;

    /**
     * Get the value of id.
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set the value of phone_number.
     *
     * @param string $phoneNumber
     */
    public function setPhoneNumber($phoneNumber)
    {
        $this->phoneNumber = $phoneNumber;
    }

    /**
     * Get the value of phone_number.
     *
     * @return string
     */
    public function getPhoneNumber()
    {
        return $this->phoneNumber;
    }

    /**
     * Set PhoneNumberType entity (many to one).
     *
     * @param PhoneNumberType $phoneNumberType
     */
    public function setPhoneNumberType(PhoneNumberType $phoneNumberType)
    {
        $this->phoneNumberType = $phoneNumberType;
    }

    /**
     * Get PhoneNumberType entity (many to one).
     *
     * @return PhoneNumberType
     */
    public function getPhoneNumberType()
    {
        return $this->phoneNumberType;
    }

    /**
     * Set MemberEntry entity (many to one).
     *
     * @param MemberEntry|null $memberEntry
     */
    public function setMemberEntry($memberEntry)
    {
        $this->memberEntry = $memberEntry;
    }

    /**
     * Get MemberEntry entity (many to one).
     *
     * @return MemberEntry
     */
    public function getMemberEntry()
    {
        return $this->memberEntry;
    }


    /**
     * Copy objects data into an associative array.
     *
     * @return array Return an array containing the phone number data.
     */
    public function toArray()
    {
        $number = array();
        $number['phone_number'] = $this->phoneNumber;
        $number['type'] = $this->phoneNumberType->getPhoneNumberType();

        return ($number);
    }

    /**
     * @inheritdoc
     *
     * @return LogEntityChangeContainer
     *
     * @throws \Exception
     */
    public function getEntityContent()
    {
        $contentContainer = new LogEntityChangeContainer();
        $contentContainer->setClassName(get_class($this));
        $contentContainer->setIndex($this->id);
        $contentContainer
            ->addContentItem('phone number type', false, new LogEntryDataField($this->phoneNumberType, true))
            ->addContentItem('phone number', false, $this->phoneNumber);
        $contentContainer->setChangeType('member data change');
        $contentContainer->setChangesOnMemberId($this->memberEntry ? $this->memberEntry->getId() : null);

        return $contentContainer;
    }
}
