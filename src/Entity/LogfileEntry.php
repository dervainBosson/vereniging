<?php

/*
 * Copyright (C) 2022 by Luc Hamers <vereniging@hamers.de>
 *
 * This file is part of Vereniging.
 *
 * Vereniging is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Vereniging is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Vereniging.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Entity;

use DateTime;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Mapping as ORM;
use Exception;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * This entity contains all data for a log file entry. This entry can contain either a simple string value or a complex
 * data object derived from AbstractComplexLogEntryBase.
 *
 * @ORM\Entity(repositoryClass="App\Repository\LogfileEntryRepository")
 * @ORM\HasLifecycleCallbacks,
 * @ORM\Table(name="logfile_entry", indexes={@ORM\Index(name="fk_logfile_entry_member_entry1",
 * columns={"changed_by_member_id"}), @ORM\Index(name="fk_logfile_entry_member_entry2",
 * columns={"changes_on_member_id"}), @ORM\Index(name="fk_logfile_entry_change_type", columns={"change_type_id"})})
 */
class LogfileEntry
{
    /**
     * @ORM\Id
     * @ORM\Column(type="bigint")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(name="`date`", type="datetime")
     */
    protected $date;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $editable;

    /**
     * @ORM\Column(type="text")
     */
    protected $logentry;

    /**
     * @ORM\ManyToOne(targetEntity="ChangeType", inversedBy="logfileEntries")
     * @ORM\JoinColumn(name="change_type_id", referencedColumnName="id")
     */
    protected $changeType;

    /**
     * @ORM\ManyToOne(targetEntity="MemberEntry")
     * @ORM\JoinColumn(name="changed_by_member_id", referencedColumnName="id")
     */
    protected $changedByMember;

    /**
     * @ORM\ManyToOne(targetEntity="MemberEntry")
     * @ORM\JoinColumn(name="changes_on_member_id", referencedColumnName="id", nullable=true)
     */
    protected $changesOnMember;

    /** @var int */
    protected $complexColumnCount;
    /** @var int */
    protected $datRowCount;
    /** @var AbstractComplexLogEntryBase */
    protected $complexLogEntry;

    /** @var EntityManager */
    protected static $entityManager;
    /** @var TranslatorInterface */
    protected static $translator;
    /** @var string */
    protected static $locale;


    /**
     * Constructor.
     */
    public function __construct()
    {
        // This cannot be done in SQL using doctrine. Reason is unknown.
        $this->editable = false;
        $this->complexColumnCount = 0;
        $this->datRowCount = 0;
    }

    /**
     * Get the value of id.
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set the value of date.
     *
     * @param \DateTime $date
     */
    public function setDate($date)
    {
        $this->date = $date;
    }

    /**
     * Get the value of date.
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set the value of editable.
     *
     * @param boolean $editable
     */
    public function setEditable($editable)
    {
        $this->editable = $editable;
    }

    /**
     * True if this log entry can be edited by a user.
     *
     * @return boolean
     */
    public function isEditable()
    {
        return $this->editable;
    }

    /**
     * Set the value of logentry. When a complex log entry object is passed, this will initialise all the necessary
     * fields. When the json string of a complex log entry ia passed, then the object and all corresponding fields will
     * be initialised.
     * It is also possible to have a log message with variables. To pass values to the variables, the message must have
     * the following format: "This is a message with %variable1% and %variable2%.|value 1,value 2"
     *
     * @param string|AbstractComplexLogEntryBase $logentry
     */
    public function setLogentry($logentry)
    {
        if (is_object($logentry) && ('App\Entity\AbstractComplexLogEntryBase' === get_parent_class($logentry))) {
            $this->logentry = $logentry->serialize();
            $this->complexLogEntry = $logentry;
        } else {
            $this->logentry = $logentry;
        }
        $this->updateIsComplexLogEntry();
    }


    /**
     * Read back the complex logentry as an array. When the logentry is not complex, than null is returned.
     *
     * @return AbstractComplexLogEntryBase
     */
    public function getComplexLogEntry()
    {
        if ($this->isComplexLogentry()) {
            $this->complexLogEntry->prepareTranslation(self::$entityManager, self::$translator, self::$locale);
        }

        return $this->complexLogEntry;
    }

    /**
     * Get the value of logentry.
     *
     * @return string
     */
    public function getLogentry()
    {
        if (is_null(self::$translator)) {
            return $this->logentry;
        }

        // Special handler in case there are parameters. The message would then look like this:
        // "This is a %message% with a second %parameter%|message param,second param"
        $content = $this->logentry;
        $parts = explode('|', $content);
        $parameters = [];
        $content = $this->logentry;
        if ((!$this->isComplexLogentry()) && (count($parts) > 1)) {
            $content = $parts[0];
            preg_match_all('#.*%(.*)%#U', $parts[0], $parameterNames);
            $parameterNames = $parameterNames[1];
            // Commas in the parameter values have to be escaped. There must be a special handling of these, by
            // replacing by a special string and removing this after explode.
            $replacement = '#comma#';
            $parts[1] = str_replace('\,', $replacement, $parts[1]);
            $parameters = explode(',', $parts[1]);
            $parameters = str_replace($replacement, ',', $parameters);
            $parameters = array_combine($parameterNames, $parameters);
        }

        return self::$translator->trans($content, $parameters, null, self::$locale);
    }

    /**
     * Set ChangeType entity (many to one).
     *
     * @param ChangeType|null $changeType
     */
    public function setChangeType(ChangeType $changeType = null)
    {
        $this->changeType = $changeType;
    }

    /**
     * Get ChangeType entity (many to one).
     *
     * @return ChangeType
     */
    public function getChangeType()
    {
        return $this->changeType;
    }

    /**
     * Set MemberEntry entity related by `changed_by_member_id` (many to one).
     *
     * @param MemberEntry $memberEntry
     */
    public function setChangedByMember(MemberEntry $memberEntry = null)
    {
        $this->changedByMember = $memberEntry;
    }

    /**
     * Get MemberEntry entity related by `changed_by_member_id` (many to one).
     *
     * @return MemberEntry
     */
    public function getChangedByMember()
    {
        return $this->changedByMember;
    }

    /**
     * Set MemberEntry entity related by `changes_on_member_id` (many to one).
     *
     * @param MemberEntry $memberEntry
     */
    public function setChangesOnMember(MemberEntry $memberEntry = null)
    {
        $this->changesOnMember = $memberEntry;
    }

    /**
     * Get MemberEntry entity related by `changes_on_member_id` (many to one).
     *
     * @return MemberEntry
     */
    public function getChangesOnMember()
    {
        return $this->changesOnMember;
    }

    /**
     * Returns true when the log entry has a complex content
     *
     * @return boolean
     */
    public function isComplexLogentry()
    {
        return (!is_null($this->complexLogEntry));
    }


    /**
     * This returns the number of data rows of the current log entry. For a simple log entry, this count is 1. For a
     * complex entry, it is tne number of data fields + 1 (needed for the header)
     *
     * @return int
     */
    public function getDataRowCount()
    {
        return $this->datRowCount;
    }

    /**
     * Return the column count of the complex log file entry.
     *
     * @return int Column count of the complex log file entry, 0 if it is not complex.
     */
    public function getComplexColumnCount()
    {
        return $this->complexColumnCount;
    }

    /**
     * Triggered on insert or update
     * @ORM\PrePersist
     * @ORM\PreUpdate
     */
    public function onPrePersistOrPreUpdate()
    {
        // If the date has not been set manually, then set it to now before writing it to the database.
        if (!$this->date) {
            $this->date = new DateTime("now");
        }
    }

    /**
     * Triggered on loading the object
     *
     * @ORM\PostLoad
     */
    public function onLoad()
    {
        $this->updateIsComplexLogEntry();
    }


    /**
     * Set all prerequisites for the value to be translated
     *
     * @param EntityManagerInterface $manager
     * @param TranslatorInterface    $translator
     * @param string                 $locale
     */
    public static function prepareTranslation(EntityManagerInterface $manager, TranslatorInterface $translator, string $locale)
    {
        self::$entityManager = $manager;
        self::$translator = $translator;
        self::$locale = $locale;
    }

    /**
     * This checks if the log entry is a complex one (i.e. is an array stored as json string) and updates the
     * corresponding fields accordingly.
     */
    private function updateIsComplexLogEntry()
    {
        // When the log entry string is a valid json containing all the correct fields, then the complex log entry
        // factory will be able to generate a complex log entry object.
        try {
            $this->complexLogEntry = AbstractComplexLogEntryBase::createComplexLogEntry($this->logentry);
            $this->complexColumnCount = $this->complexLogEntry->getNumberOfDataFields();
            $this->datRowCount = 1;
            foreach ($this->complexLogEntry->getDataAsArray(false) as $logLine) {
                // Add a row for each data field
                $this->datRowCount++;
                // When the log entry contains multiple lines (could be in serial letter changes), then these lines are
                // split by \n. Count these and add to the row count.
                $this->datRowCount += substr_count($logLine[0], "\n");
            }
        } catch (Exception $e) {
            $this->complexLogEntry = null;
            $this->complexColumnCount = 0;
            $this->datRowCount = 1;
        }
    }
}
