<?php

/*
 * Copyright (C) 2022 by Luc Hamers <vereniging@hamers.de>
 *
 * This file is part of Vereniging.
 *
 * Vereniging is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Vereniging is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Vereniging.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Entity;

/**
 * Class ChangeEntry
 */
class ChangeEntry
{
    /** @var bool True when it is used for the extended comparison of objects */
    private $isExtendedContent;
    /** @var string|EntityLoggerInterface Content of the change item */
    private $content;

    /**
     * This adds an item to one of the content lists.
     *
     * @param bool                         $isExtendedContent True when the item is to be added to the extended content
     *                                                        list
     * @param string|EntityLoggerInterface $content           Either a simple text or an EntityLoggerInterface object
     */
    public function __construct(bool $isExtendedContent, $content)
    {
        $this->isExtendedContent = $isExtendedContent;
        $this->content = $content;
    }

    /**
     * @return bool
     */
    public function isExtendedContent(): bool
    {
        return $this->isExtendedContent;
    }

    /**
     * @return EntityLoggerInterface|string
     */
    public function getContent()
    {
        return $this->content;
    }
}
