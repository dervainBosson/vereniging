<?php

/*
 * Copyright (C) 2022 by Luc Hamers <vereniging@hamers.de>
 *
 * This file is part of Vereniging.
 *
 * Vereniging is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Vereniging is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Vereniging.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Mapping\Annotation as Gedmo;
use Knp\DoctrineBehaviors\Contract\Entity\TranslatableInterface;
use Knp\DoctrineBehaviors\Model\Translatable\TranslatableTrait;

/**
 * PhoneNumberType
 *
 * @ORM\Entity(repositoryClass="App\Repository\PhoneNumberTypeRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class PhoneNumberType implements ValueListInterface, EntityLoggerInterface, EntityTranslationInterface, TranslatableInterface
{
    use TranslatableTrait;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=45)
     */
    protected $phoneNumberType;

    /**
     * @Gedmo\Slug(fields={"phoneNumberType"})
     *
     * @ORM\Column(type="string", length=45, unique=true)
     */
    protected $slug;

    /**
     * @ORM\OneToMany(targetEntity="PhoneNumber", mappedBy="phoneNumberType")
     */
    protected $phoneNumbers;

    /**
     * Constructor.
     * @param string|null $phoneNumberType
     */
    public function __construct(string $phoneNumberType = null)
    {
        $this->phoneNumbers = new ArrayCollection();
        $this->setPhoneNumberType($phoneNumberType);
    }

    /**
     * Get the value of id.
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set the value of phone_number_type.
     *
     * @param string $phoneNumberType
     */
    public function setPhoneNumberType($phoneNumberType)
    {
        // When the phone number type is set for the first time or when it is changed, then update or set the default
        // translation.
        if ($this->phoneNumberType !== $phoneNumberType) {
            $this->translate($this->getDefaultLocale())->setPhoneNumberTypeTranslated($phoneNumberType);
        }

        $this->phoneNumberType = $phoneNumberType;
    }

    /**
     * Get the value of phone_number_type.
     *
     * @return string
     */
    public function getPhoneNumberType()
    {
        return $this->proxyCurrentLocaleTranslation('getPhoneNumberTypeTranslated', []);
    }

    /**
     * @return True when this object is referenced by another entry.
     */
    public function isInUse()
    {
        return ((bool) count($this->phoneNumbers));
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return (string) $this->getPhoneNumberType();
    }

    /**
     * @inheritdoc
     *
     * @return LogEntityChangeContainer
     */
    public function getEntityContent()
    {
        $contentContainer = new LogEntityChangeContainer();
        $contentContainer->setClassName(get_class($this));
        $contentContainer->setIndex($this->id);
        $contentContainer->setShortContent(['phone number type' => $this->phoneNumberType]);
        $contentContainer->setChangeType('system settings change');

        return $contentContainer;
    }

    /**
     * Use this method to add a translation for a certain locale. To store these to the database, a doctrine flush is
     * needed.
     *
     * @param string $locale
     * @param string $translation
     */
    public function addNewTranslation(string $locale, string $translation)
    {
        $this->translate($locale, false)->setPhoneNumberTypeTranslated($translation);
        $this->mergeNewTranslations();
    }

    /**
     * When the entity is created for the first time, there will be translations present, either the translation for the
     * default locale  or translations added by hand. These must be saved in the database, which is done by the merge
     * new translations command. To ensure that this is called only once regardless of how the translations are added
     * (e.g. by constructor, setter or by hand), the merge is done before persisting the object.
     *
     * @ORM\PrePersist()
     */
    public function mergeNewTranslationsOnPersist()
    {
        $this->mergeNewTranslations();
    }
}
