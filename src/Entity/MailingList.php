<?php

/*
 * Copyright (C) 2022 by Luc Hamers <vereniging@hamers.de>
 *
 * This file is part of Vereniging.
 *
 * Vereniging is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Vereniging is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Vereniging.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * MailingList
 *
 * @ORM\Entity(repositoryClass="App\Repository\MailingListRepository")
 * @ORM\Table(name="mailinglist", indexes={@ORM\Index(name="Index", columns={"id"})})
 */
class MailingList implements EntityLoggerInterface
{
    /**
     * @ORM\Id
     * @ORM\Column(type="smallint")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=45)
     */
    protected $listName;

    /**
     * @ORM\Column(type="string", length=45, nullable=true)
     */
    protected $managementEmail;

    /**
     * @ORM\Column(type="string", length=45, nullable=true)
     */
    protected $managementPassword;

    /**
     * @ORM\ManyToMany(targetEntity="MemberEntry", inversedBy="mailingLists")
     * @ORM\JoinTable(name="member_mailinglist_map",
     *     joinColumns={@ORM\JoinColumn(name="mailinglist_id", referencedColumnName="id")},
     *     inverseJoinColumns={@ORM\JoinColumn(name="member_entry_id", referencedColumnName="id")}
     * )
     */
    protected $memberEntries;

    /**
     * Constructor.
     *
     * @param string|null $listName
     */
    public function __construct(string $listName = null)
    {
        $this->listName = $listName;
        $this->memberEntries = new ArrayCollection();
    }

    /**
     * Get the value of id.
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set the value of listname.
     *
     * @param string $listName
     */
    public function setListName($listName)
    {
        $this->listName = $listName;
    }

    /**
     * Get the value of listname.
     *
     * @return string
     */
    public function getListName()
    {
        return $this->listName;
    }

    /**
     * Set the value of management_email.
     *
     * @param string $managementEmail
     */
    public function setManagementEmail($managementEmail)
    {
        $this->managementEmail = $managementEmail;
    }

    /**
     * Get the value of management_email.
     *
     * @return string
     */
    public function getManagementEmail()
    {
        return $this->managementEmail;
    }

    /**
     * Set the value of management_password.
     *
     * @param string $managementPassword
     */
    public function setManagementPassword($managementPassword)
    {
        $this->managementPassword = $managementPassword;
    }

    /**
     * Get the value of management_password.
     *
     * @return string
     */
    public function getManagementPassword()
    {
        return $this->managementPassword;
    }

    /**
     * Add MemberEntry entity to collection.
     *
     * @param MemberEntry $memberEntry
     */
    public function addMemberEntry(MemberEntry $memberEntry)
    {
        if ($this->memberEntries->contains($memberEntry)) {
            return;
        }

        $this->memberEntries[] = $memberEntry;
        $memberEntry->addMailingList($this);
    }

    /**
     * Remove MemberEntry entity from collection.
     *
     * @param MemberEntry $memberEntry
     */
    public function removeMemberEntry(MemberEntry $memberEntry)
    {
        if (!$this->memberEntries->contains($memberEntry)) {
            return;
        }

        $this->memberEntries->removeElement($memberEntry);
        $memberEntry->removeMailingList($this);
    }

    /**
     * Get MemberEntry entity collection.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMemberEntries()
    {
        return $this->memberEntries;
    }

    /**
     * @return mixed
     */
    public function __toString()
    {
        // Cast to string to also return a string when the value is null (is needed for the FormCollection strlen check)
        return ((string) $this->listName);
    }

    /**
     * @inheritdoc
     *
     * @return LogEntityChangeContainer
     */
    public function getEntityContent()
    {
        $contentContainer = new LogEntityChangeContainer();
        $contentContainer->setClassName(get_class($this));
        $contentContainer->setIndex($this->id);
        $contentContainer->setShortContent(['list name' => $this->listName]);
        $contentContainer->setExtendedContent(['management email' => $this->managementEmail]);
        $contentContainer->setChangeType('system settings change');
        $contentContainer->setStringIdentifier($this->listName);

        return $contentContainer;
    }
}
