<?php

/*
 * Copyright (C) 2022 by Luc Hamers <vereniging@hamers.de>
 *
 * This file is part of Vereniging.
 *
 * Vereniging is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Vereniging is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Vereniging.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Entity;

use App\Repository\MembershipFeeTransactionsRepository;
use DateTime;
use DateTimeInterface;
use Doctrine\ORM\Mapping as ORM;

/**
 * For tracking who has to pay and who has paid the membership fee, objects of this entity are created.
 *
 * @ORM\Entity(repositoryClass=MembershipFeeTransactionsRepository::class)
 * @ORM\HasLifecycleCallbacks,
 */
class MembershipFeeTransaction
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createDate;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $closeDate;

    /**
     * @ORM\Column(type="float")
     */
    private $amount;

    /**
     * @ORM\ManyToOne(targetEntity=MembershipNumber::class, inversedBy="membershipFeeTransactions")
     * @ORM\JoinColumn(name="membership_number_id", referencedColumnName="membership_number", nullable=false)
     */
    private $membershipNumber;


    /**
     * This is needed to initialise the create date for forms.
     *
     * MembershipFeeTransaction constructor.
     */
    public function __construct()
    {
        $this->onPrePersist();
    }

    /**
     * Get the value of id.
     *
     * @return integer
     */
    public function getId(): ?int
    {
        return $this->id;
    }


    /**
     * Set the create date. This is only needed for tests, as the date usually will be set automatically on saving the
     * object to the database.
     *
     * @param DateTimeInterface $date
     */
    public function setCreateDate(DateTimeInterface $date)
    {
        $this->createDate = $date;
    }

    /**
     * Return the date when this entity was created.
     *
     * @return DateTimeInterface
     */
    public function getCreateDate(): DateTimeInterface
    {
        return $this->createDate;
    }


    /**
     * @return DateTimeInterface|null Date on which this entity was closed (i.e. paid)
     */
    public function getCloseDate(): ?DateTimeInterface
    {
        return $this->closeDate;
    }


    /**
     * @param DateTimeInterface|null $closeDate Date at which this entity was closed (i.e. paid)
     */
    public function setCloseDate(?DateTimeInterface $closeDate): void
    {
        $this->closeDate = $closeDate;
    }


    /**
     * @return float Amount in the defined currency
     */
    public function getAmount(): float
    {
        return $this->amount;
    }


    /**
     * @param float $amount Amount in the defined currency
     */
    public function setAmount(float $amount): void
    {
        $this->amount = $amount;
    }


    /**
     * @return MembershipNumber Membership number to which this transaction is linked.
     */
    public function getMembershipNumber(): MembershipNumber
    {
        return $this->membershipNumber;
    }


    /**
     * @param MembershipNumber $membershipNumber Membership number to which this transaction has to be linked.
     */
    public function setMembershipNumber(MembershipNumber $membershipNumber): void
    {
        $this->membershipNumber = $membershipNumber;
    }


    /**
     * Triggered on insert
     *
     * @ORM\PrePersist
     */
    public function onPrePersist()
    {
        if (!$this->createDate) {
            $this->createDate = new DateTime("now");
        }
    }


    /**
     * @return string
     */
    public function __toString():string
    {
        return $this->amount."€ ".$this->createDate->format('Y-m-d').' '.($this->closeDate?$this->closeDate->format('Y-m-d'):'-');
    }


    /**
     * Return the values of this entity as array
     *
     * @return array
     */
    public function toArray(): array
    {
        $data['amount'] = number_format($this->amount, 2);
        $data['createDate'] = $this->createDate->format('M j, Y');
        $data['closeDate'] = $this->closeDate ? $this->closeDate->format('M j, Y') : '-';
        $data['membershipNumber'] = $this->membershipNumber;

        return $data;
    }
}
