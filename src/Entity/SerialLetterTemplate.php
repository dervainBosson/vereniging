<?php

/*
 * Copyright (C) 2022 by Luc Hamers <vereniging@hamers.de>
 *
 * This file is part of Vereniging.
 *
 * Vereniging is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Vereniging is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Vereniging.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * This entity contains html snippets which are shown as templates in serial letters.
 *
 * @ORM\Entity(repositoryClass="App\Repository\SerialLetterTemplateRepository")
 * @ORM\Table(name="serial_letter_template", indexes={@ORM\Index(name="fk_serial_letter_template_language_id",
 * columns={"language_id"})})
 */
class SerialLetterTemplate implements EntityLoggerInterface
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="text")
     *
     * @Assert\NotBlank
     */
    protected $templateName;


    /**
     * @ORM\Column(type="text")
     *
     * @Assert\NotBlank
     */
    protected $templateContent;


    /**
     * The inverse direction is needed to check if the language is in use by a serial letter template and therefor
     * cannot be deleted.
     *
     * @ORM\ManyToOne(targetEntity="Language", inversedBy="serialLetterTemplates")
     * @ORM\JoinColumn(name="language_id", referencedColumnName="id", nullable=false)
     */
    protected $language;


    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * @return string
     */
    public function getTemplateName(): string
    {
        return $this->templateName;
    }


    /**
     * @param string $templateName
     */
    public function setTemplateName($templateName): void
    {
        $this->templateName = $templateName;
    }


    /**
     * @return string
     */
    public function getTemplateContent(): string
    {
        return $this->templateContent;
    }


    /**
     * @param string $templateContent
     */
    public function setTemplateContent($templateContent): void
    {
        $this->templateContent = $templateContent;
    }


    /**
     * @return Language
     */
    public function getLanguage()
    {
        return $this->language;
    }


    /**
     * @param Language $language
     */
    public function setLanguage(Language $language): void
    {
        $this->language = $language;
    }


    /**
     * @inheritdoc
     *
     * @return LogEntityChangeContainer
     *
     * @throws \Exception
     */
    public function getEntityContent()
    {
        $contentContainer = new LogEntityChangeContainer();
        $contentContainer->setClassName(get_class($this));
        $contentContainer->setIndex($this->id);
        $contentContainer->setChangeType('system settings change');
        $contentContainer
            ->addContentItem('name', false, $this->templateName)
            ->addContentItem('language', true, new LogEntryDataField($this->language, true))
            ->addContentItem('content', true, $this->templateContent);

        return $contentContainer;
    }
}
