<?php

/*
 * Copyright (C) 2022 by Luc Hamers <vereniging@hamers.de>
 *
 * This file is part of Vereniging.
 *
 * Vereniging is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Vereniging is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Vereniging.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * CommitteeFunctionMap
 *
 * @ORM\Entity(repositoryClass="App\Repository\CommitteeFunctionMapRepository")
 * @ORM\Table(name="committee_function_map")
 */
class CommitteeFunctionMap
{
    /**
     * @ORM\Id
     * @ORM\Column(type="smallint")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="CommitteeFunction", inversedBy="committeeFunctionMaps")
     * @ORM\JoinColumn(name="committee_function_id", referencedColumnName="id")
     *
     * @var CommitteeFunction
     */
    protected $committeeFunction;

    /**
     * @ORM\ManyToOne(targetEntity="Committee", inversedBy="committeeFunctionMaps")
     * @ORM\JoinColumn(name="committee_id", referencedColumnName="id")
     *
     * @var Committee
     */
    protected $committee;

    /**
     * @ORM\ManyToMany(targetEntity="MemberEntry", inversedBy="committeeFunctionMaps")
     * @ORM\JoinTable(name="member_committee_map",
     *     joinColumns={@ORM\JoinColumn(name="committee_function_map_id", referencedColumnName="id")},
     *     inverseJoinColumns={@ORM\JoinColumn(name="member_entry_id", referencedColumnName="id")}
     * )
     * @ORM\OrderBy({"name" = "ASC", "firstName" = "ASC"})
     */
    protected $memberEntries;

    /**
     * Constructor.
     *
     * @param Committee|null         $committee
     * @param CommitteeFunction|null $committeeFunction
     */
    public function __construct(Committee $committee = null, CommitteeFunction $committeeFunction = null)
    {
        $this->memberEntries = new ArrayCollection();
        $this->committee = $committee;
        $this->committeeFunction = $committeeFunction;
    }

    /**
     * Get the value of id.
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set CommitteeFunction entity (many to one).
     *
     * @param CommitteeFunction $committeeFunction
     */
    public function setCommitteeFunction(CommitteeFunction $committeeFunction = null)
    {
        $this->committeeFunction = $committeeFunction;
    }

    /**
     * Get CommitteeFunction entity (many to one).
     *
     * @return CommitteeFunction
     */
    public function getCommitteeFunction()
    {
        return $this->committeeFunction;
    }

    /**
     * Set Committee entity (many to one).
     *
     * @param Committee $committee
     */
    public function setCommittee(Committee $committee = null)
    {
        $this->committee = $committee;
    }

    /**
     * Get Committee entity (many to one).
     *
     * @return Committee
     */
    public function getCommittee()
    {
        return $this->committee;
    }

    /**
     * Add MemberEntry entity to collection.
     *
     * @param MemberEntry $memberEntry
     */
    public function addMemberEntry(MemberEntry $memberEntry)
    {
        if (!$this->memberEntries->contains($memberEntry)) {
            $this->memberEntries[] = $memberEntry;
            $memberEntry->addCommitteeFunctionMap($this);
        }
    }

    /**
     * Remove MemberEntry entity from collection.
     *
     * @param MemberEntry $memberEntry
     */
    public function removeMemberEntry(MemberEntry $memberEntry)
    {
        if ($this->memberEntries->contains($memberEntry)) {
            $this->memberEntries->removeElement($memberEntry);
            $memberEntry->removeCommitteeFunctionMap($this);
        }
    }

    /**
     * Get MemberEntry entity collection.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMemberEntries()
    {
        return $this->memberEntries;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->committee->getCommitteeName()." - ".$this->committeeFunction->getCommitteeFunctionName();
    }
}
