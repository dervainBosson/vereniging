<?php

/*
 * Copyright (C) 2022 by Luc Hamers <vereniging@hamers.de>
 *
 * This file is part of Vereniging.
 *
 * Vereniging is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Vereniging is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Vereniging.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Contract\Entity\TranslatableInterface;
use Knp\DoctrineBehaviors\Model\Translatable\TranslatableTrait;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserRoleRepository")
 * @ORM\Table(name="user_role")
 * @ORM\HasLifecycleCallbacks()
 */
class UserRole implements TranslatableInterface
{
    use TranslatableTrait;

    /**
     * @ORM\Id
     * @ORM\Column(type="smallint")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     */
    private $userRole;

    /**
     * @ORM\Column(type="json_array")
     */
    private $systemRoles = array();

    /**
     * @ORM\Column(type="boolean")
     */
    private $defaultRole;

    /**
     * @ORM\OneToMany(targetEntity="MemberEntry", mappedBy="userRole")
     * @ORM\JoinColumn(name="id", referencedColumnName="user_level_id")
     */
    private $memberEntries;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getUserRole()
    {
        return $this->proxyCurrentLocaleTranslation('getUserRoleTranslated', []);
    }

    /**
     * @param string $userRole
     */
    public function setUserRole($userRole)
    {
        // When the user role is set for the first time or when it is changed, then update or set the default
        // translation.
        if ($this->userRole !== $userRole) {
            $this->translate($this->getDefaultLocale())->setUserRoleTranslated($userRole);
        }

        $this->userRole = $userRole;
    }

    /**
     * @return array
     */
    public function getSystemRoles()
    {
        $roles = $this->systemRoles;
        // give everyone ROLE_VIEW_DASHBOARD!
        if (!in_array('ROLE_VIEW_DASHBOARD', $roles)) {
            $roles[] = 'ROLE_VIEW_DASHBOARD';
        }

        return $roles;
    }

    /**
     * @param array $systemRoles
     */
    public function setSystemRoles(array $systemRoles)
    {
        $this->systemRoles = $systemRoles;
    }

    /**
     * @return string String representation of this object.
     */
    public function __toString()
    {
        return (string) $this->getUserRole();
    }

    /**
     * When the entity is created for the first time, there will be translations present, either the translation for the
     * default locale  or translations added by hand. These must be saved in the database, which is done by the merge
     * new translations command. To ensure that this is called only once regardless of how the translations are added
     * (e.g. by constructor, setter or by hand), the merge is done before persisting the object.
     *
     * @ORM\PrePersist()
     */
    public function mergeNewTranslationsOnPersist()
    {
        $this->mergeNewTranslations();
    }

    /**
     * @param BOOL $defaultRole
     */
    public function setDefaultRole($defaultRole)
    {
        $this->defaultRole = $defaultRole;
    }
}
