<?php

/*
 * Copyright (C) 2022 by Luc Hamers <vereniging@hamers.de>
 *
 * This file is part of Vereniging.
 *
 * Vereniging is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Vereniging is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Vereniging.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Contract\Entity\TranslatableInterface;
use Knp\DoctrineBehaviors\Model\Translatable\TranslatableTrait;

/**
 * MembershipType
 *
 * @ORM\Entity(repositoryClass="App\Repository\MembershipTypeRepository")
 * @ORM\Table(name="membership_type")
 * @ORM\HasLifecycleCallbacks()
 */
class MembershipType implements EntityLoggerInterface, EntityTranslationInterface, TranslatableInterface
{
    use TranslatableTrait;

    /**
     * @ORM\Id
     * @ORM\Column(type="smallint")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(name="`type`", type="string", length=45)
     */
    protected $type;

    /**
     * @ORM\Column(type="float")
     */
    protected $membershipFee;

    /**
     * @ORM\Column(type="boolean")
     */
    protected $groupMembership;

    /**
     * @ORM\Column(type="boolean")
     */
    protected $defaultGroupMembershipType;

    /**
     * @ORM\Column(type="boolean")
     */
    protected $defaultNewMembershipType;

    /**
     * @ORM\OneToMany(targetEntity="MemberEntry", mappedBy="membershipType")
     * @ORM\JoinColumn(name="id", referencedColumnName="membership_type_id")
     */
    protected $memberEntries;

    /**
     * MembershipType constructor.
     * @param string|null $type
     * @param float|null  $membershipFee
     * @param bool|null   $groupMembership
     * @param bool|null   $defaultGroupMembershipType
     * @param bool|null   $defaultNewMembershipType
     */
    public function __construct(string $type = null, float $membershipFee = null, bool $groupMembership = null, bool $defaultGroupMembershipType = null, bool $defaultNewMembershipType = null)
    {
        $this->memberEntries = new ArrayCollection();
        $this->setType($type);
        $this->membershipFee = $membershipFee;
        $this->groupMembership = $groupMembership;
        $this->defaultGroupMembershipType = $defaultGroupMembershipType;
        $this->defaultNewMembershipType = $defaultNewMembershipType;
    }

    /**
     * Get the value of id.
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set the value of type.
     *
     * @param string $type
     */
    public function setType($type)
    {
        // When the membership type is set for the first time or when it is changed, then update or set the default
        // translation. When the type is null, dpn't update the translation, because that will generate an exception
        // when calling setTypeTranslated.
        if (($this->type !== $type) && (!is_null($type))) {
            $this->translate($this->getDefaultLocale())->setTypeTranslated($type);
        }

        $this->type = $type;
    }

    /**
     * Get the value of type.
     *
     * @return string
     */
    public function getType()
    {
        return $this->proxyCurrentLocaleTranslation('getTypeTranslated', []);
    }

    /**
     * Get the value of type, as stored in this entity and not in the translation.
     *
     * @return string
     */
    public function getTypeKey()
    {
        return $this->type;
    }

    /**
     * Set the value of membership_fee.
     *
     * @param float $membershipFee
     */
    public function setMembershipFee($membershipFee)
    {
        $this->membershipFee = $membershipFee;
    }

    /**
     * Get the value of membership_fee.
     *
     * @return float
     */
    public function getMembershipFee()
    {
        return $this->membershipFee;
    }

    /**
     * Set the value of group_membership.
     *
     * @param boolean $groupMembership
     */
    public function setGroupMembership($groupMembership)
    {
        $this->groupMembership = $groupMembership;
    }

    /**
     * Get the value of group_membership.
     *
     * @return boolean
     */
    public function getGroupMembership()
    {
        return $this->groupMembership;
    }

    /**
     * @return bool
     */
    public function getDefaultGroupMembershipType()
    {
        return $this->defaultGroupMembershipType;
    }

    /**
     * @param bool $defaultGroupMembershipType
     */
    public function setDefaultGroupMembershipType(bool $defaultGroupMembershipType)
    {
        $this->defaultGroupMembershipType = $defaultGroupMembershipType;
    }

    /**
     * @return boolean
     */
    public function getDefaultNewMembershipType()
    {
        return $this->defaultNewMembershipType;
    }

    /**
     * @param bool $defaultNewMembershipType
     */
    public function setDefaultNewMembershipType(bool $defaultNewMembershipType)
    {
        $this->defaultNewMembershipType = $defaultNewMembershipType;
    }

    /**
     * Return content (membership type value) as string.
     *
     * @return mixed
     */
    public function __toString()
    {
        return (string) $this->getType();
    }

    /**
     * @return bool True when this membership type is used by at least one member entry.
     */
    public function isInUse()
    {
        return ((bool) count($this->memberEntries));
    }

    /**
     * @inheritdoc
     *
     * @return LogEntityChangeContainer
     */
    public function getEntityContent()
    {
        $contentContainer = new LogEntityChangeContainer();
        $contentContainer->setClassName(get_class($this));
        $contentContainer->setIndex($this->id);
        $contentContainer->setShortContent(['membership type' => $this->type]);
        $contentContainer->setExtendedContent(['membership fee' => $this->membershipFee,
            'group membership' => $this->groupMembership ? 'yes' : 'no',
            'default group membership' => $this->defaultGroupMembershipType ? 'yes' : 'no',
            'default new membership' => $this->defaultNewMembershipType ? 'yes' : 'no',
        ]);
        $contentContainer->setChangeType('system settings change');
        $contentContainer->setStringIdentifier($this->getType());

        return $contentContainer;
    }

    /**
     * When the entity is created for the first time, there will be translations present, either the translation for the
     * default locale  or translations added by hand. These must be saved in the database, which is done by the merge
     * new translations command. To ensure that this is called only once regardless of how the translations are added
     * (e.g. by constructor, setter or by hand), the merge is done before persisting the object.
     *
     * @ORM\PrePersist()
     */
    public function mergeNewTranslationsOnPersist()
    {
        $this->mergeNewTranslations();
    }

    /**
     * Use this method to add a translation for a certain locale. To store these to the database, a doctrine flush is
     * needed.
     *
     * @param string $locale
     * @param string $translation
     */
    public function addNewTranslation(string $locale, string $translation)
    {
        $this->translate($locale, false)->setTypeTranslated($translation);
        $this->mergeNewTranslations();
    }
}
