<?php

/*
 * Copyright (C) 2022 by Luc Hamers <vereniging@hamers.de>
 *
 * This file is part of Vereniging.
 *
 * Vereniging is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Vereniging is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Vereniging.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * SerialLetterContent
 *
 * @ORM\Entity(repositoryClass="App\Repository\SerialLetterContentRepository")
 * @ORM\Table(name="serial_letter_content", indexes={@ORM\Index(name="fk_serial_letter_content_serial_letter_id",
 * columns={"serial_letter_id"}), @ORM\Index(name="fk_serial_letter_content_language_id", columns={"language_id"})})
 */
class SerialLetterContent
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    protected $letterTitle;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    protected $content;

    /**
     * @ORM\ManyToOne(targetEntity="SerialLetter", inversedBy="serialLetterContents", cascade={"persist"})
     * @ORM\JoinColumn(name="serial_letter_id", referencedColumnName="id", nullable=false)
     */
    protected $serialLetter;

    /**
     * The inverse direction is needed to check if the language is in use by a serial letter and therefor cannot be
     * deleted.
     *
     * @ORM\ManyToOne(targetEntity="Language", inversedBy="serialLetterContents")
     * @ORM\JoinColumn(name="language_id", referencedColumnName="id", nullable=false)
     */
    protected $language;

    /**
     * Constructor.
     */
    public function __construct()
    {
    }

    /**
     * Set the value of letter_title.
     *
     * @param string $letterTitle
     */
    public function setLetterTitle($letterTitle)
    {
        $this->letterTitle = $letterTitle;
        $this->updateSerialLetterChangeDate();
    }

    /**
     * Get the value of letter_title.
     *
     * @return string
     */
    public function getLetterTitle()
    {
        return $this->letterTitle;
    }

    /**
     * Set the value of content.
     *
     * @param string $content
     */
    public function setContent($content)
    {
        $this->content = $content;
        $this->updateSerialLetterChangeDate();
    }

    /**
     * Get the value of content.
     *
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set SerialLetter entity (many to one).
     *
     * @param SerialLetter $serialLetter
     */
    public function setSerialLetter(SerialLetter $serialLetter)
    {
        $this->serialLetter = $serialLetter;
        $this->updateSerialLetterChangeDate();
    }

    /**
     * Get SerialLetter entity (many to one).
     *
     * @return SerialLetter
     */
    public function getSerialLetter()
    {
        return $this->serialLetter;
    }

    /**
     * Set Language entity (many to one).
     *
     * @param Language $language
     */
    public function setLanguage(Language $language)
    {
        $this->language = $language;
        $this->updateSerialLetterChangeDate();
    }

    /**
     * Get Language entity (many to one).
     *
     * @return Language
     */
    public function getLanguage()
    {
        return $this->language;
    }

    /**
     * Updating the serial letters change date doesn't work in a prePersist life cycle callback in this class, even
     * when recomputing the unit of work. Therefor the change date is updated in each setter.
     */
    private function updateSerialLetterChangeDate()
    {
        $this->serialLetter->setChangeDate(new \DateTime('now'));
    }
}
