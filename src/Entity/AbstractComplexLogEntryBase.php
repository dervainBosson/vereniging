<?php

/*
 * Copyright (C) 2022 by Luc Hamers <vereniging@hamers.de>
 *
 * This file is part of Vereniging.
 *
 * Vereniging is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Vereniging is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Vereniging.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Entity;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use InvalidArgumentException;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * Base class for complex log entries. Complex log entries look like this:
 * ["main message", ['name' => ['value'], 'first name' => ['value']]]
 * or
 * ["main message", ['name' => ['old name', 'new name'], 'first name' => ['old name', 'new name']]]
 *
 * Class AbstractComplexLogEntryBase
 */
abstract class AbstractComplexLogEntryBase implements \JsonSerializable
{
    /** @var string */
    protected $mainMessage = '';
    /** @var EntityManager */
    protected $entityManager;
    /** @var TranslatorInterface */
    protected $translator;
    /** @var string */
    protected $locale;
    /** @var array */
    protected $dataArray = [];


    /**
     * The object can be initialized with json data obtained from the objects serialize method.
     *
     * @param string|null $jsonData
     */
    public function __construct(string $jsonData = null)
    {
        // This is the default locale for the system. It will be overwritten when settings the current locale via
        // setLocale, which should always be done when a translator is used.
        $this->locale = 'en';

        if (!is_null($jsonData)) {
            $decoded = json_decode($jsonData, true);
            if (json_last_error() !== JSON_ERROR_NONE) {
                throw new \InvalidArgumentException('Object cannot be created, json data is corrupted!');
            }

            // key($decoded['dataArray']) returns the key of any of the entries in the data array. Since all have the
            // same number of fields, it doesn't mattes which of all keys is used.
            if (count($decoded['dataArray'])) {
                $randomKey = key($decoded['dataArray']);
                if (count($decoded['dataArray'][$randomKey]) !== $this->getNumberOfDataFields()) {
                    throw new \InvalidArgumentException(sprintf('Object cannot be created, json data contains %d fields, should contain %d!', count($decoded['dataArray'][$randomKey]), $this->getNumberOfDataFields()));
                }
            }
            $this->mainMessage = $decoded['mainMessage'];
            $this->unserialiseDataFields($decoded['dataArray']);
        }
    }


    /**
     * Set all prerequisites for the value to be translated
     *
     * @param EntityManagerInterface|null $manager
     * @param TranslatorInterface|null    $translator
     * @param string|null                 $locale
     */
    public function prepareTranslation(?EntityManagerInterface $manager, ?TranslatorInterface $translator, ?string $locale)
    {
        $this->entityManager = $manager;
        $this->translator = $translator;
        $this->locale = $locale;
    }


    /**
     * Returns the main message. When the translator is set, then the translated message is returned.
     * Besides normal text, the main message can also have a special format used by the entity changes listener, which
     * looks like this: "Created new %classname% entry %stringIdentifier%|my class name,new class object". For these
     * strings, the variables behind the pipe symbol are written to the defined variables.
     *
     * @return string
     */
    public function getMainMessage(): string
    {
        if (!is_null($this->translator)) {
            $matches = [];
            $translationVariables = [];
            $mainMessage = $this->mainMessage;

            // When the main message looks like a string with the needed variables, then do a special handling
            if (1 === preg_match('/.*(%classname%).*(%stringIdentifier%)[|](\w*),(.*)/', $this->mainMessage, $matches)) {
                // Get the variable values from the string
                $translationVariables = ['%classname%' => $matches[3], '%stringIdentifier%' => $matches[4]];
                // Now translate the class name part
                $translationVariables['%classname%'] = $this->translator->trans($translationVariables['%classname%'], [], null, $this->locale);
                // $mainMessageParts[0] contains the string up until the pipe symbol
                $mainMessageParts = explode('|', $this->mainMessage);
                $mainMessage = $mainMessageParts[0];
            }

            return $this->translator->trans($mainMessage, $translationVariables, null, $this->locale);
        }

        return $this->mainMessage;
    }


    /**
     * @param string $mainMessage
     */
    public function setMainMessage(string $mainMessage)
    {
        $this->mainMessage = $mainMessage;
    }


    /**
     * Return the json notation of this object.
     *
     * @return string
     */
    public function serialize()
    {
        return json_encode($this);
    }


    /**
     * Specify data which should be serialized to JSON
     *
     * @link https://php.net/manual/en/jsonserializable.jsonserialize.php
     *
     * @return mixed data which can be serialized by <b>json_encode</b>,
     * which is a value of any type other than a resource.
     *
     * @since 5.4.0
     */
    public function jsonSerialize()
    {
        return [
            'numberOfDataFields' => $this->getNumberOfDataFields(),
            'mainMessage' => $this->mainMessage,
            'dataArray'   => $this->dataArray,
        ];
    }


    /**
     * Returns the number of data fields
     *
     * @return int
     */
    abstract public function getNumberOfDataFields();


    /**
     * This returns the data as an associative array, which looks depending on the number of data fields like this:
     * ['name' => ['value'], 'first name' => ['value']]]
     * ['name' => ['old name', 'new name'], 'first name' => ['old name', 'new name']]
     * When the translator is set, then keys and values are translated.
     *
     * @param bool $translateContent When true, the content is translated. When false, the raw data is returned.
     *
     * @return array When returning the raw data array to be changed, read it like this: $data = &$object->getDataAsArray(false);
     */
    abstract public function &getDataAsArray(bool $translateContent = true);


    /**
     * This adds data to the data array.
     *
     * @param string $key  Array key
     * @param array  $data Array containing the data for this key
     */
    public function addDataField(string $key, array $data)
    {
        if ($this->getNumberOfDataFields() !== count($data)) {
            throw new InvalidArgumentException(sprintf("Data contains %d fields, should contain %d!", count($data), $this->getNumberOfDataFields()));
        }
        $this->dataArray[$key] = $data;
    }


    /**
     * This removes a data element from the data array.
     *
     * @param string $key Array key to remove
     */
    public function removeDataField(string $key)
    {
        if (!array_key_exists($key, $this->dataArray)) {
            throw new InvalidArgumentException(sprintf("Array key '%s' not found in data array!", $key));
        }
        unset($this->dataArray[$key]);
    }


    /**
     * Factory to generate an object containing the correct number of data fields from the json string.
     *
     * @param string $jsonString
     *
     * @return AbstractComplexLogEntryBase
     */
    public static function createComplexLogEntry(string $jsonString)
    {
        $decoded = json_decode($jsonString, true);
        if (json_last_error() !== JSON_ERROR_NONE) {
            throw new \InvalidArgumentException('Object cannot be created, json data is corrupted!');
        }

        if (is_array($decoded)) {
            $keys = array_keys($decoded);
            if (['numberOfDataFields', 'mainMessage', 'dataArray'] === $keys) {
                switch ($decoded['numberOfDataFields']) {
                    case 1:
                        return new ComplexLogEntrySingleDataField($jsonString);
                    case 2:
                        return new ComplexLogEntryDoubleDataField($jsonString);
                    default:
                        throw new \InvalidArgumentException(sprintf('Object cannot be created with number of data fields of %d!', $decoded['numberOfDataFields']));
                }
            }
        }

        throw new \InvalidArgumentException('Object cannot be created, json data doesn\'t contain the correct object!');
    }

    /**
     * The data array passed to the constructor must be deserialised for each field, which has to be done in the derived
     * classes. The derived method has to write it to the data array of the base class.
     *
     * @param array $dataArray
     */
    abstract protected function unserialiseDataFields(array $dataArray);


    /**
     * Return the data array with translated, ordered keys
     *
     * @return array
     */
    protected function translateArrayKeys()
    {
        if (!is_null($this->translator)) {
            $contentTranslated = [];
            foreach ($this->dataArray as $key => $value) {
                $contentTranslated[$this->translator->trans($key, [], null, $this->locale)] = $value;
            }
            uksort($contentTranslated, 'strnatcasecmp');

            return $contentTranslated;
        }

        return $this->dataArray;
    }


    /**
     * When the passed field is a log entry data field, then translate it to the correct string.
     *
     * @param mixed $field
     *
     * @return string|null Either string representation of the log entry data field or the original field
     */
    protected function translateLogEntryDataField($field): ?string
    {
        if (is_object($field) && ('App\Entity\LogEntryDataField' === get_class($field))) {
            /** @var LogEntryDataField $field */
            $field->prepareTranslation($this->entityManager, $this->translator, $this->locale);

            return (string) $field;
        }

        // All the htm tags other then <ins> and <del> are removed
        $field = str_replace(['<ins>', '</ins>', '<del>', '</del>'], ['#ins#', '#/ins#', '#del#', '#/del#'], $field);
        $field = strip_tags($field);
        $field = str_replace(['#ins#', '#/ins#', '#del#', '#/del#'], ['<ins>', '</ins>', '<del>', '</del>'], $field);

        return $field;
    }
}
