<?php

/*
 * Copyright (C) 2022 by Luc Hamers <vereniging@hamers.de>
 *
 * This file is part of Vereniging.
 *
 * Vereniging is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Vereniging is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Vereniging.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Mapping\Annotation as Gedmo;
use Knp\DoctrineBehaviors\Contract\Entity\TranslatableInterface;
use Knp\DoctrineBehaviors\Model\Translatable\TranslatableTrait;

/**
 * Status
 *
 * @ORM\Entity(repositoryClass="App\Repository\StatusRepository")
 * @ORM\Table(name="`status`")
 * @ORM\HasLifecycleCallbacks()
 */
class Status implements ValueListInterface, EntityLoggerInterface, EntityTranslationInterface, TranslatableInterface
{
    use TranslatableTrait;

    /**
     * @ORM\Id
     * @ORM\Column(type="smallint")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(name="`status`", type="string", length=45)
     */
    protected $status;

    /**
     * @ORM\Column(name="`has_membership_number`", type="boolean")
     */
    protected $hasMembershipNumber;

    /**
     * @Gedmo\Slug(fields={"status"})
     *
     * @ORM\Column(type="string", length=45, unique=true)
     */
    protected $slug;

    /**
     * @ORM\OneToMany(targetEntity="MemberEntry", mappedBy="status")
     * @ORM\JoinColumn(name="id", referencedColumnName="status_id")
     */
    protected $memberEntries;

    /**
     * Constructor.
     * @param string|null $status
     * @param bool|null   $hasMembershipNumber
     */
    public function __construct(string $status = null, bool $hasMembershipNumber = null)
    {
        $this->memberEntries = new ArrayCollection();
        $this->setStatus($status);
        $this->hasMembershipNumber = $hasMembershipNumber;
    }

    /**
     * Get the value of id.
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set the value of status.
     *
     * @param string $status
     */
    public function setStatus($status)
    {
        // When the status is set for the first time or when it is changed, then update or set the default
        // translation.
        if ($this->status !== $status) {
            $this->translate($this->getDefaultLocale())->setStatusTranslated($status);
        }

        $this->status = $status;
    }

    /**
     * Get the value of status.
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->proxyCurrentLocaleTranslation('getStatusTranslated', []);
    }

    /**
     * @return bool
     */
    public function getHasMembershipNumber()
    {
        return $this->hasMembershipNumber;
    }

    /**
     * @param bool $hasMembershipNumber
     */
    public function setHasMembershipNumber(bool $hasMembershipNumber)
    {
        $this->hasMembershipNumber = $hasMembershipNumber;
    }

    /**
     * @return True when this object is referenced by another entry.
     */
    public function isInUse()
    {
        return ((bool) count($this->memberEntries));
    }

    /**
     * Return content (status value) as string.
     *
     * @return mixed
     */
    public function __toString()
    {
        return (string) $this->getStatus();
    }

    /**
     * @inheritdoc
     *
     * @return LogEntityChangeContainer
     */
    public function getEntityContent()
    {
        $contentContainer = new LogEntityChangeContainer();
        $contentContainer->setClassName(get_class($this));
        $contentContainer->setIndex($this->id);
        $contentContainer->setShortContent(['status' => $this->status]);
        $contentContainer->setChangeType('system settings change');

        return $contentContainer;
    }

    /**
     * Use this method to add a translation for a certain locale. To store these to the database, a doctrine flush is
     * needed.
     *
     * @param string $locale
     * @param string $translation
     */
    public function addNewTranslation(string $locale, string $translation)
    {
        $this->translate($locale, false)->setStatusTranslated($translation);
        $this->mergeNewTranslations();
    }

    /**
     * When the entity is created for the first time, there will be translations present, either the translation for the
     * default locale  or translations added by hand. These must be saved in the database, which is done by the merge
     * new translations command. To ensure that this is called only once regardless of how the translations are added
     * (e.g. by constructor, setter or by hand), the merge is done before persisting the object.
     *
     * @ORM\PrePersist()
     */
    public function mergeNewTranslationsOnPersist()
    {
        $this->mergeNewTranslations();
    }
}
