<?php

/*
 * Copyright (C) 2022 by Luc Hamers <vereniging@hamers.de>
 *
 * This file is part of Vereniging.
 *
 * Vereniging is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Vereniging is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Vereniging.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * Title
 *
 * @ORM\Entity(repositoryClass="App\Repository\TitleRepository")
 * @ORM\Table(name="title",
 * uniqueConstraints={@ORM\UniqueConstraint(name="slug_UNIQUE", columns={"slug"})})
 */
class Title implements ValueListInterface, EntityLoggerInterface
{
    /**
     * @ORM\Id
     * @ORM\Column(type="smallint")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=45)
     */
    protected $title;

    /**
     * @ORM\Column(type="boolean")
     */
    protected $afterName;

    /**
     * @Gedmo\Slug(fields={"title"})
     *
     * @ORM\Column(type="string", length=45, unique=true)
     */
    protected $slug;

    /**
     * @ORM\OneToMany(targetEntity="MemberEntry", mappedBy="title")
     * @ORM\JoinColumn(name="id", referencedColumnName="title_id")
     */
    protected $memberEntries;

    /**
     * Constructor.
     *
     * @param string|null $title
     * @param bool|null   $afterName
     */
    public function __construct(string $title = null, bool $afterName = null)
    {
        $this->memberEntries = new ArrayCollection();
        $this->title = $title;
        $this->afterName = $afterName;
    }

    /**
     * Get the value of id.
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set the value of title.
     *
     * @param string $title
     *
     * @return Title
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get the value of title.
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set the value of after_name.
     *
     * @param boolean $afterName
     *
     * @return Title
     */
    public function setAfterName($afterName)
    {
        $this->afterName = $afterName;

        return $this;
    }

    /**
     * Get the value of after_name.
     *
     * @return boolean
     */
    public function getAfterName()
    {
        return $this->afterName;
    }

    /**
     * @return True when this object is referenced by another entry.
     */
    public function isInUse()
    {
        return ((bool) count($this->memberEntries));
    }

    /**
     * Return content (title value) as string.
     *
     * @return mixed
     */
    public function __toString()
    {
        return ($this->title);
    }

    /**
     * @inheritdoc
     *
     * @return LogEntityChangeContainer
     */
    public function getEntityContent()
    {
        $contentContainer = new LogEntityChangeContainer();
        $contentContainer->setClassName(get_class($this));
        $contentContainer->setIndex($this->id);
        $contentContainer->setShortContent(['title' => $this->title]);
        $contentContainer->setChangeType('system settings change');

        return $contentContainer;
    }
}
