<?php

/*
 * Copyright (C) 2022 by Luc Hamers <vereniging@hamers.de>
 *
 * This file is part of Vereniging.
 *
 * Vereniging is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Vereniging is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Vereniging.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Entity;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use InvalidArgumentException;
use JsonSerializable;
use ReflectionClass;
use Symfony\Contracts\Translation\TranslatorInterface;
use Exception;

/**
 * Class LogEntryDataField
 *
 * This class contains a reference to a database entity, which can be stored in a string.
 */
class LogEntryDataField implements JsonSerializable
{

    /** @var string */
    private $className;
    /** @var int */
    private $dbIndex;
    /** @var string */
    private $defaultString;
    /** @var bool */
    private $translate;
    /** @var EntityManager */
    private $entityManager;
    /** @var string */
    private $locale;
    /** @var TranslatorInterface */
    protected $translator;

    /**
     * LogEntryDataField constructor.
     *
     * @param mixed        $value     Object, string or json string. When passing a json string from a serialised
     *                                object, then $translate must be null!
     * @param boolean|null $translate When true, the object has to be translated
     *
     * @throws Exception
     */
    public function __construct($value, bool $translate = null)
    {
        $this->translate = $translate;

        // This is the default locale for the system. It will be overwritten when settings the current locale via
        // setLocale, which should always be done when a translator is used.
        $this->locale = 'en';

        if (is_object($value)) {
            // This ensures that no proxy names appear in the class names namespace, which can happed when using
            // get_class()
            $this->className = __NAMESPACE__.'\\'.(new ReflectionClass($value))->getShortName();
            $this->dbIndex = $value->getId();
            $this->defaultString = (string) $value;
        } elseif (is_string($value)) {
            // When the passed value is a correct json string for this object, then initialise it.
            if (!$this->initIfJson($value, $translate)) {
                // Value is a plain string
                $this->defaultString = $value;
            }
        } else {
            throw new InvalidArgumentException(sprintf('Value %s is neither an object nor a string!', $value));
        }
    }


    /**
     * Set all prerequisites for the value to be translated
     *
     * @param EntityManagerInterface $manager
     * @param TranslatorInterface    $translator
     * @param string                 $locale
     */
    public function prepareTranslation(?EntityManagerInterface $manager, ?TranslatorInterface $translator, ?string $locale)
    {
        $this->entityManager = $manager;
        $this->translator = $translator;
        $this->locale = $locale;
    }

    /**
     * When the entity manager is set and the referenced entity is found, then this is returned as string. If not, the
     * default value is returned.
     *
     * @return string
     */
    public function __toString()
    {
        if (!is_null($this->entityManager)) {
            try {
                // Try to read the entity from the database
                if (!is_null($this->className)) {
                    $value = $this->entityManager->getRepository($this->className)->find($this->dbIndex);
                    if (!is_null($value)) {
                        // There seems to be a bug in loading the translations: they are only loaded for the locale of the
                        // web page, not for the locale needed for the log entry. Refreshing the object also reloads the
                        // translations, which prevents saving address data from crashing.
                        $this->entityManager->refresh($value);
                        $returnValue = $this->translateEntity($value);

                        if (false === $this->translate) {
                            $returnValue = $value->__toString();
                        }

                        return $returnValue;
                    }
                }
            } catch (Exception $e) {
                // Left intentionally blank. This is reached, when the the classname is wrong
            }
        }

        // When this only contains a normal array and translation is requested, do the translation
        if ($this->translate && (!is_null($this->translator))) {
            return ($this->translator->trans($this->defaultString, [], null, $this->locale));
        }

        return $this->defaultString;
    }

    /**
     * Specify data which should be serialized to JSON
     *
     * @link https://php.net/manual/en/jsonserializable.jsonserialize.php
     *
     * @return mixed data which can be serialized by <b>json_encode</b>,
     * which is a value of any type other than a resource.
     *
     * @since 5.4.0
     */
    public function jsonSerialize()
    {
        return [
            'className'     => $this->className,
            'dbIndex'       => $this->dbIndex,
            'defaultString' => $this->defaultString,
            'translate'     => $this->translate,
        ];
    }

    /**
     * when the passed value is a json string, then initialize the object
     *
     * @param string    $value
     * @param bool|null $translate
     *
     * @return bool True, when this passed string is a valid json string and the object could be initialised
     */
    private function initIfJson(string $value, bool $translate = null): bool
    {
        // Try if the passed string is a json string
        $decoded = json_decode($value, true);
        if (json_last_error() === JSON_ERROR_NONE) {
            if (!is_null($translate)) {
                throw new InvalidArgumentException('Translate must be null when passing a json string!');
            }

            // Check if all needed keys are present by reading the keys from the json serialize method.
            foreach (array_keys($this->jsonSerialize()) as $key) {
                if (!array_key_exists($key, $decoded)) {
                    throw new InvalidArgumentException(sprintf('Parameter %s is missing in json string!', $key));
                }
            }

            $this->className = $decoded['className'];
            $this->dbIndex = $decoded['dbIndex'];
            $this->defaultString = $decoded['defaultString'];
            $this->translate = $decoded['translate'];

            return true;
        }

        return false;
    }

    /**
     * When the object to be translated is an entity using the doctrine behaviour translatable, then this method does
     * this translation.
     *
     * @param object $value
     *
     * @return object
     */
    private function translateEntity(object $value)
    {
        $returnValue = $value;
        // When this is a translatable database object, then translate it
        if ($this->translate && (method_exists($value, 'setCurrentLocale'))) {
            // Translation is done by using the __toString method. We have to save the current locale to
            // be able to switch back to it after getting the translation in the desired language. Not
            // doing this will cause string outputs on the gui in the wrong language.
            $currentLocale = $value->getCurrentLocale();
            $value->setCurrentLocale($this->locale);
            $returnValue = $value->__toString();
            $value->setCurrentLocale($currentLocale);
        }

        return $returnValue;
    }
}
