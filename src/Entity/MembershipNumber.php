<?php

/*
 * Copyright (C) 2022 by Luc Hamers <vereniging@hamers.de>
 *
 * This file is part of Vereniging.
 *
 * Vereniging is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Vereniging is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Vereniging.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Entity;

use DateTime;
use DateTimeInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * MembershipNumber
 *
 * @ORM\Entity(repositoryClass="App\Repository\MembershipNumberRepository")
 * @ORM\Table(name="membership_number")
 */
class MembershipNumber
{
    /**
     * @ORM\Id
     * @ORM\Column(type="smallint")
     */
    protected $membershipNumber;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    protected $dateRemoved;

    /**
     * @ORM\Column(type="boolean", options={"default" : 0})
     */
    protected $useDirectDebit;

    /**
     * @ORM\OneToMany(targetEntity="MemberEntry", mappedBy="membershipNumber")
     * @ORM\JoinColumn(name="membership_number", referencedColumnName="membership_number_id")
     */
    protected $memberEntries;

    /**
     * @ORM\OneToMany(targetEntity=MembershipFeeTransaction::class, mappedBy="membershipNumber", orphanRemoval=true)
     * @ORM\OrderBy({"createDate" = "DESC"})
     */
    private $membershipFeeTransactions;


    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->memberEntries = new ArrayCollection();
        $this->membershipFeeTransactions = new ArrayCollection();
    }


    /**
     * Set the value of member_number.
     *
     * @param integer $membershipNumber
     */
    public function setMembershipNumber(int $membershipNumber)
    {
        $this->membershipNumber = $membershipNumber;
    }


    /**
     * Get the value of member_number.
     *
     * @return integer
     */
    public function getMembershipNumber()
    {
        return $this->membershipNumber;
    }


    /**
     * Set the value of date_removed.
     *
     * @param DateTime|null $dateRemoved
     */
    public function setDateRemoved(DateTime $dateRemoved = null)
    {
        $this->dateRemoved = $dateRemoved;
    }


    /**
     * Get the value of date_removed.
     *
     * @return DateTime
     */
    public function getDateRemoved()
    {
        return $this->dateRemoved;
    }


    /**
     * Return the member entries which use this membership number.
     *
     * @return ArrayCollection|MemberEntry[]
     */
    public function getMemberEntries()
    {
        return $this->memberEntries;
    }


    /**
     * This method is called by the setMembershipNumber of the member entry before a new member number is set.
     *
     * @param MemberEntry $memberEntry
     */
    public function removeMemberEntry(MemberEntry $memberEntry)
    {
        if ($this->memberEntries->contains($memberEntry)) {
            $this->memberEntries->removeElement($memberEntry);
        }
    }


    /**
     * This method is called by the setMembershipNumber of the member entry after a new member number is set.
     *
     * @param MemberEntry $memberEntry
     */
    public function addMemberEntry(MemberEntry $memberEntry)
    {
        if (!$this->memberEntries->contains($memberEntry)) {
            $this->memberEntries->add($memberEntry);
        }
    }


    /**
     * @return string Membership number
     */
    public function __toString()
    {
        return (string) $this->membershipNumber;
    }


    /**
     * @return mixed
     */
    public function getUseDirectDebit()
    {
        return $this->useDirectDebit;
    }


    /**
     * @param mixed $useDirectDebit
     */
    public function setUseDirectDebit($useDirectDebit): void
    {
        $this->useDirectDebit = $useDirectDebit;
    }


    /**
     * @return Collection|MembershipFeeTransaction[]
     */
    public function getMembershipFeeTransactions(): Collection
    {
        return $this->membershipFeeTransactions;
    }

    /**
     * @param MembershipFeeTransaction $membershipFeeTransaction
     */
    public function addMembershipFeeTransaction(MembershipFeeTransaction $membershipFeeTransaction): void
    {
        if (!$this->membershipFeeTransactions->contains($membershipFeeTransaction)) {
            $this->membershipFeeTransactions[] = $membershipFeeTransaction;
            $membershipFeeTransaction->setMembershipNumber($this);
        }
    }


    /**
     * Read all membership fee transactions which are still open and the return the accumulated open
     *
     * @return float
     */
    public function getOutstandingDebt(): float
    {
        $debt = 0;

        /** @var MembershipFeeTransaction $transaction */
        foreach ($this->membershipFeeTransactions as $transaction) {
            if (!$transaction->getCloseDate()) {
                $debt += $transaction->getAmount();
            }
        }

        return $debt;
    }


    /**
     * Get the last transaction date
     *
     * @return null|DateTimeInterface
     */
    public function getLastTransactionDate(): ?DateTimeInterface
    {
        if (!count($this->membershipFeeTransactions)) {
            return null;
        }

        $lastDate = new DateTime('2020-1-1');

        /** @var MembershipFeeTransaction $transaction */
        foreach ($this->membershipFeeTransactions as $transaction) {
            if ($transaction->getCreateDate() > $lastDate) {
                $lastDate = $transaction->getCreateDate();
            }
            if (($transaction->getCloseDate()) && ($transaction->getCloseDate() > $lastDate)) {
                $lastDate = $transaction->getCloseDate();
            }
        }

        return $lastDate;
    }
}
