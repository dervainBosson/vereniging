<?php

/*
 * Copyright (C) 2022 by Luc Hamers <vereniging@hamers.de>
 *
 * This file is part of Vereniging.
 *
 * Vereniging is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Vereniging is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Vereniging.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Entity;

/**
 * Each class which changes have to generate automatic log file entries must implement this interface. The actual
 * logging is done by the LogEntityChangesListener.
 *
 * Interface EntityLoggerInterface
 */
interface EntityLoggerInterface
{
    /**
     * This method must return an associative array with the following fields:
     * className:   name of the class for which the content is saved
     * index:       index of the current entity
     * content:     array in which the fields of the entity and their values are stored.
     * change_type: string containing the change type to be logged.
     *
     * @return LogEntityChangeContainer
     */
    public function getEntityContent();
}
