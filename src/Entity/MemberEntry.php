<?php

/*
 * Copyright (C) 2022 by Luc Hamers <vereniging@hamers.de>
 *
 * This file is part of Vereniging.
 *
 * Vereniging is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Vereniging is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Vereniging.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Entity;

use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * MemberEntry
 *
 * @ORM\Entity(repositoryClass="App\Repository\MemberEntryRepository")
 */
class MemberEntry implements UserInterface, \Serializable, EntityLoggerInterface, PasswordAuthenticatedUserInterface
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=1)
     *
     * @Assert\NotBlank()
     */
    protected $gender;

    /**
     * @ORM\Column(name="`name`", type="string", length=60)
     *
     * @Assert\NotBlank()
     */
    protected $name;

    /**
     * @ORM\Column(type="string", length=60)
     *
     * @Assert\NotBlank()
     */
    protected $firstName;

    /**
     * @ORM\Column(type="date", nullable=true)
     *
     * @var \DateTime
     */
    protected $birthday;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     *
     * @Assert\Email()
     */
    protected $email;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    protected $picture;

    /**
     * @ORM\Column(type="string", length=130, nullable=true)
     */
    protected $username;

    /**
     * @ORM\Column(name="`password`", type="string", length=255, nullable=true)
     */
    protected $password;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $memberSince;

    /**
     * @ORM\Column(type="boolean")
     */
    protected $membershipEndRequested;

    /**
     * @ORM\OneToMany(targetEntity="MemberAddressMap", mappedBy="memberEntry", cascade={"persist"}, orphanRemoval=true)
     *
     * @var MemberAddressMap
     */
    protected $memberAddressMaps;

    /**
     * @ORM\OneToMany(targetEntity="PhoneNumber", mappedBy="memberEntry", cascade={"persist"}, orphanRemoval=true)
     */
    protected $phoneNumbers;

    /**
     * @ORM\ManyToOne(targetEntity="Title", inversedBy="memberEntries")
     * @ORM\JoinColumn(name="title_id", referencedColumnName="id", nullable=true)
     *
     * @var Title
     */
    protected $title;

    /**
     * @ORM\OneToOne(targetEntity="CompanyInformation", cascade={"persist"}, orphanRemoval=true, inversedBy="memberEntry")
     * @ORM\JoinColumn(name="company_information_id", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     */
    protected $companyInformation;

    /**
     * @ORM\ManyToOne(targetEntity="Language", inversedBy="memberEntries")
     * @ORM\JoinColumn(name="preferred_language_id", referencedColumnName="id")
     *
     * @Assert\NotBlank()
     *
     * @var Language
     */
    protected $preferredLanguage;

    /**
     * @ORM\ManyToOne(targetEntity="Status", inversedBy="memberEntries")
     * @ORM\JoinColumn(name="status_id", referencedColumnName="id")
     *
     * @Assert\NotBlank()
     *
     * @var Status
     */
    protected $status;

    /**
     * @ORM\ManyToOne(targetEntity="MembershipType", inversedBy="memberEntries")
     * @ORM\JoinColumn(name="membership_type_id", referencedColumnName="id")
     *
     * @var MembershipType
     */
    protected $membershipType;

    /**
     * @ORM\ManyToOne(targetEntity="MembershipNumber", inversedBy="memberEntries", cascade={"persist"})
     * @ORM\JoinColumn(name="membership_number_id", referencedColumnName="membership_number", nullable=true)
     *
     * @var MembershipNumber
     */
    protected $membershipNumber;

    /**
     * @ORM\ManyToOne(targetEntity="UserRole", inversedBy="memberEntries")
     * @ORM\JoinColumn(name="user_role_id", referencedColumnName="id")
     *
     * @var UserRole
     */
    protected $userRole;

    /**
     * @ORM\ManyToMany(targetEntity="CommitteeFunctionMap", mappedBy="memberEntries")
     *
     * @var CommitteeFunctionMap
     */
    protected $committeeFunctionMaps;

    /**
     * @ORM\ManyToMany(targetEntity="ChangeType", inversedBy="memberEntries")
     * @ORM\JoinTable(name="member_email_notification_map",
     *     joinColumns={@ORM\JoinColumn(name="member_id", referencedColumnName="id")},
     *     inverseJoinColumns={@ORM\JoinColumn(name="change_type_id", referencedColumnName="id")}
     * )
     */
    protected $emailNotifications;

    /**
     * @ORM\ManyToMany(targetEntity="MailingList", mappedBy="memberEntries")
     * @ORM\OrderBy({"listName" = "ASC"})
     */
    protected $mailingLists;

    /**
     * @ORM\ManyToMany(targetEntity="MemberEntry")
     * @ORM\JoinTable(name="member_member_group_map",
     *     joinColumns={@ORM\JoinColumn(name="member1_id", referencedColumnName="id")},
     *     inverseJoinColumns={@ORM\JoinColumn(name="member2_id", referencedColumnName="id")}
     * )
     *
     * @var Collection|MemberEntry[]
     */
    protected $groupMembers;

    private $plainPassword;

    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->memberAddressMaps = new ArrayCollection();
        $this->phoneNumbers = new ArrayCollection();
        $this->committeeFunctionMaps = new ArrayCollection();
        $this->emailNotifications = new ArrayCollection();
        $this->mailingLists = new ArrayCollection();
        $this->groupMembers = new ArrayCollection();
    }


    /**
     * Get the value of id.
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set the value of gender.
     *
     * @param string $gender
     *
     * @return MemberEntry
     */
    public function setGender($gender)
    {
        $this->gender = $gender;

        return $this;
    }

    /**
     * Get the value of gender.
     *
     * @param bool $fullText When true, it returns "Male", "Female", "Diverse" or "None" instead of 'm', 'f', 'd', null
     *
     * @return string|null
     */
    public function getGender(bool $fullText = false): ?string
    {
        if (!$fullText) {
            return $this->gender;
        }

        switch ($this->gender) {
            case 'm':
                return 'Male';
            case 'f':
                return 'Female';
            case 'd':
                return 'Diverse';
            default:
                return 'None';
        }
    }

    /**
     * Set the value of name.
     *
     * @param string $name
     *
     * @return MemberEntry
     */
    public function setName($name)
    {
        $this->name = trim($name);
        $this->setUsername();

        return $this;
    }

    /**
     * Get the value of name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set the value of first_name.
     *
     * @param string $firstName
     *
     * @return MemberEntry
     */
    public function setFirstName($firstName)
    {
        $this->firstName = trim($firstName);
        $this->setUsername();

        return $this;
    }


    /**
     * Get the value of first_name.
     *
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * Set the value of birthday.
     *
     * @param \DateTime $birthday
     *
     * @return MemberEntry
     */
    public function setBirthday($birthday)
    {
        $this->birthday = $birthday;

        return $this;
    }

    /**
     * Get the value of birthday.
     *
     * @return \DateTime
     */
    public function getBirthday()
    {
        return $this->birthday;
    }

    /**
     * Set the value of email.
     *
     * @param string $email
     *
     * @return MemberEntry
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get the value of email.
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set the value of picture.
     *
     * @param string $picture
     */
    public function setPicture($picture)
    {
        $this->picture = $picture;
    }

    /**
     * Get the value of picture.
     *
     * @return string
     */
    public function getPicture()
    {
        return $this->picture;
    }

    /**
     * Return the complete name of the member, e.g. 'Firstname Lastname'.
     *
     * @param bool $useAcademicTitle If set, the academic title is added to the name
     *
     * @return string
     */
    public function getCompleteName($useAcademicTitle = false)
    {
        if ('-' === $this->getFirstName()) {
            $completeName = $this->commaReversal($this->name);
        } else {
            $completeName = $this->commaReversal($this->firstName)." ".$this->commaReversal($this->name);
        }
        // When there are double dots in the name, replace those by a single dot. Because the replacement can generate
        // another dot pair, repeat this operation until no pairs are found anymore.
        while (strpos($completeName, "  ") !== false) {
            $completeName = str_replace("  ", " ", $completeName);
        }

        if (($useAcademicTitle) && (!is_null($this->title))) {
            if ($this->title->getAfterName()) {
                $completeName .= ' '.$this->title->getTitle();
            } else {
                $completeName = $this->title->getTitle().' '.$completeName;
            }
        }

        return ($completeName);
    }

    /**
     * Get the value of username.
     *
     * @return string
     */
    public function getUserIdentifier(): string
    {
        return $this->username;
    }

    /**
     * Get the value of username.
     *
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->username;
    }

    /**
     * return all user roles for this member.
     *
     * @return string[] List of user roles
     */
    public function getRoles()
    {
        return ($this->userRole->getSystemRoles());
    }

    /**
     * This method always returns null, since we are using bcrypt, where the salt is stored in the password.
     *
     * @return null
     */
    public function getSalt(): ?string
    {
        return (null);
    }

    /**
     *
     */
    public function eraseCredentials()
    {
        $this->plainPassword = null;
    }

    /**
     * Set the value of password.
     *
     * @param string $password
     *
     * @return MemberEntry
     */
    public function setPassword($password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Get the value of password.
     *
     * @return string
     */
    public function getPassword(): ?string
    {
        return $this->password;
    }

    /**
     * Set the value of member_since.
     *
     * @param integer $memberSince
     *
     * @return MemberEntry
     */
    public function setMemberSince($memberSince)
    {
        $this->memberSince = $memberSince;

        return $this;
    }

    /**
     * Get the value of member_since.
     *
     * @return integer
     */
    public function getMemberSince()
    {
        return $this->memberSince;
    }

    /**
     * Set the value of membership_end_requested.
     *
     * @param boolean $membershipEndRequested
     *
     * @return MemberEntry
     */
    public function setMembershipEndRequested($membershipEndRequested)
    {
        $this->membershipEndRequested = $membershipEndRequested;

        return $this;
    }

    /**
     * Get the value of membership_end_requested.
     *
     * @return boolean
     */
    public function getMembershipEndRequested()
    {
        return $this->membershipEndRequested;
    }

    /**
     * Add MemberAddressMap entity to collection (one to many).
     *
     * @param MemberAddressMap $memberAddressMap
     */
    public function addMemberAddressMap(MemberAddressMap $memberAddressMap)
    {
        // Only add address when the complete address data has been entered
        if (($memberAddressMap->getAddress()) &&
            (strlen($memberAddressMap->getAddress()->getAddress())) &&
            (strlen($memberAddressMap->getAddress()->getZip())) &&
            (strlen($memberAddressMap->getAddress()->getCity()))) {
            if ($this !== $memberAddressMap->getMemberEntry()) {
                $memberAddressMap->setMemberEntry($this);
            }

            if ($this->memberAddressMaps->contains($memberAddressMap)) {
                return;
            }

            $this->memberAddressMaps[] = $memberAddressMap;
        }
    }

    /**
     * Remove MemberAddressMap entity from collection (one to many).
     *
     * @param MemberAddressMap $memberAddressMap
     */
    public function removeMemberAddressMap(MemberAddressMap $memberAddressMap)
    {
        $this->memberAddressMaps->removeElement($memberAddressMap);
    }

    /**
     * Get MemberAddressMap entity collection (one to many).
     *
     * @return Collection|MemberAddressMap[]
     */
    public function getMemberAddressMaps()
    {
        return $this->memberAddressMaps;
    }

    /**
     * Add PhoneNumber entity to collection (one to many).
     *
     * @param PhoneNumber $phoneNumber
     */
    public function addPhoneNumber(PhoneNumber $phoneNumber)
    {
        // Only add a new phone number when the number is not empty.
        if (strlen($phoneNumber->getPhoneNumber())) {
            if ($this->phoneNumbers->contains($phoneNumber)) {
                return;
            }

            // When adding the phone number, also set the member entity in the phone number entity (owning side).
            $phoneNumber->setMemberEntry($this);
            $this->phoneNumbers[] = $phoneNumber;
        }
    }

    /**
     * Remove PhoneNumber entity from collection (one to many).
     *
     * @param PhoneNumber $phoneNumber
     */
    public function removePhoneNumber(PhoneNumber $phoneNumber)
    {
        if (!$this->phoneNumbers->contains($phoneNumber)) {
            return;
        }
        $this->phoneNumbers->removeElement($phoneNumber);
        // This is needed because phone number is the owning side
        $phoneNumber->setMemberEntry(null);
    }

    /**
     * Return all phone numbers associated with this member. This method returns an array with the following fields:
     * ['number']
     * ['type']
     * If no phone numbers are available, null is returned
     *
     * @return array All phone associated with this member entry.
     */
    public function getPhoneNumbersAsArray()
    {
        $numbers = array();
        /** @var PhoneNumber $phoneNumber */
        foreach ($this->phoneNumbers as $phoneNumber) {
            $phoneNumberData = $phoneNumber->toArray();
            $phoneNumberData['phone_number_id'] = $phoneNumber->getId();

            $numbers[$phoneNumber->getId()] = $phoneNumberData;
        }

        return ($numbers);
    }

    /**
     * Return phone number collection. The phone numbers are first sorted by type, then by phone number.
     *
     * @return ArrayCollection | PhoneNumber[] All phone number entities associated with this member entry.
     */
    public function getPhoneNumbers()
    {
        $iterator = $this->phoneNumbers->getIterator();
        $iterator->uasort(function ($a, $b) {
            /** @var PhoneNumber $a */
            /** @var PhoneNumber $b */
            if ($a->getPhoneNumberType()->getPhoneNumberType() === $b->getPhoneNumberType()->getPhoneNumberType()) {
                return $a->getPhoneNumber() < $b->getPhoneNumber() ? -1 : 1;
            }

            return ($a->getPhoneNumberType()->getPhoneNumberType() < $b->getPhoneNumberType()->getPhoneNumberType()) ? -1 : 1;
        });

        return new ArrayCollection(iterator_to_array($iterator));
    }

    /**
     * Set Title entity (many to one).
     *
     * @param Title $title
     */
    public function setTitle(Title $title = null)
    {
        $this->title = $title;
    }

    /**
     * Get Title entity (many to one).
     *
     * @return Title
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set CompanyInformation entity (many to one).
     *
     * @param CompanyInformation $companyInformation
     */
    public function setCompanyInformation(CompanyInformation $companyInformation = null)
    {
        $this->companyInformation = $companyInformation;
        if ($companyInformation) {
            $companyInformation->setMemberEntry($this);
        }
    }

    /**
     * Get CompanyInformation entity (many to one).
     *
     * @return CompanyInformation
     */
    public function getCompanyInformation()
    {
        return $this->companyInformation;
    }


    /**
     * Set Language entity (many to one).
     *
     * @param Language $preferredLanguage
     */
    public function setPreferredLanguage(Language $preferredLanguage = null)
    {
        $this->preferredLanguage = $preferredLanguage;
    }

    /**
     * Get Language entity (many to one).
     *
     * @return Language
     */
    public function getPreferredLanguage()
    {
        return $this->preferredLanguage;
    }

    /**
     * Set Status entity (many to one).
     *
     * @param Status $status
     */
    public function setStatus(Status $status = null)
    {
        $this->status = $status;
    }

    /**
     * Get Status entity (many to one).
     *
     * @return Status
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set MembershipType entity (many to one).
     *
     * @param MembershipType $membershipType
     */
    public function setMembershipType(MembershipType $membershipType = null)
    {
        $this->membershipType = $membershipType;
    }

    /**
     * Get MembershipType entity (many to one).
     *
     * @return MembershipType
     */
    public function getMembershipType()
    {
        return $this->membershipType;
    }

    /**
     * Set the membership number for this entity.
     *
     * @param MembershipNumber $membershipNumber
     */
    public function setMembershipNumber(MembershipNumber $membershipNumber = null)
    {
        // Update the member entries collection in the membership number
        if ($this->membershipNumber) {
            $this->membershipNumber->removeMemberEntry($this);
        }
        $this->membershipNumber = $membershipNumber;

        // Update the member entries collection in the membership number
        if ($this->membershipNumber) {
            $this->membershipNumber->addMemberEntry($this);
        }
    }

    /**
     * Return the current membership number.
     *
     * @return MembershipNumber|null
     */
    public function getMembershipNumber()
    {
        return $this->membershipNumber;
    }

    /**
     * Set user role entity (many to one).
     *
     * @param UserRole $userRole
     */
    public function setUserRole(UserRole $userRole = null)
    {
        $this->userRole = $userRole;
    }

    /**
     * Get user role entity (many to one).
     *
     * @return UserRole
     */
    public function getUserRole()
    {
        return $this->userRole;
    }

    /**
     * Add CommitteeFunctionMap entity to collection.
     *
     * @param CommitteeFunctionMap $committeeFunctionMap
     */
    public function addCommitteeFunctionMap(CommitteeFunctionMap $committeeFunctionMap)
    {
        if (!$this->committeeFunctionMaps->contains($committeeFunctionMap)) {
            $this->committeeFunctionMaps[] = $committeeFunctionMap;
            $committeeFunctionMap->addMemberEntry($this);
        }
    }

    /**
     * Remove CommitteeFunctionMap entity from collection.
     *
     * @param CommitteeFunctionMap $committeeFunctionMap
     */
    public function removeCommitteeFunctionMap(CommitteeFunctionMap $committeeFunctionMap)
    {
        if ($this->committeeFunctionMaps->contains($committeeFunctionMap)) {
            $this->committeeFunctionMaps->removeElement($committeeFunctionMap);
            $committeeFunctionMap->removeMemberEntry($this);
        }
    }

    /**
     * Get CommitteeFunctionMap entity collection.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCommitteeFunctionMaps()
    {
        return $this->committeeFunctionMaps;
    }

    /**
     * Add email notification change type entity to collection.
     *
     * @param ChangeType $emailNotification
     */
    public function addEmailNotification(ChangeType $emailNotification)
    {
        if (!$this->emailNotifications->contains($emailNotification)) {
            $this->emailNotifications[] = $emailNotification;
            $emailNotification->addMemberEntry($this);
        }
    }

    /**
     * Remove email notification change type entity from collection.
     *
     * @param ChangeType $emailNotification
     */
    public function removeEmailNotification(ChangeType $emailNotification)
    {
        if ($this->emailNotifications->contains($emailNotification)) {
            $this->emailNotifications->removeElement($emailNotification);
            $emailNotification->removeMemberEntry($this);
        }
    }

    /**
     * Get  email notification change type entity collection.
     *
     * @return \Doctrine\Common\Collections\Collection|ChangeType[]
     */
    public function getEmailNotifications()
    {
        return $this->emailNotifications;
    }

    /**
     * Add MailingList entity to collection.
     *
     * @param MailingList $mailingList
     */
    public function addMailingList(MailingList $mailingList)
    {
        if ($this->mailingLists->contains($mailingList)) {
            return;
        }

        $this->mailingLists[] = $mailingList;
        $mailingList->addMemberEntry($this);
    }

    /**
     * Remove MailingList entity from collection.
     *
     * @param MailingList $mailingList
     */
    public function removeMailingList(MailingList $mailingList)
    {
        if (!$this->mailingLists->contains($mailingList)) {
            return;
        }

        $this->mailingLists->removeElement($mailingList);
        $mailingList->removeMemberEntry($this);
    }

    /**
     * Get MailingList entity collection.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMailingLists()
    {
        return $this->mailingLists;
    }


    /**
     * Return the main address as array. When no address is set, null is returned. If an address is found, then the
     * following array is returned:
     * ['address']
     * ['zip']
     * ['city']
     * ['country']
     * ['company'] <-- this field is only inserted, when this is a company address
     *
     * @return array|null Array containing the main address.
     */
    public function getMainAddress()
    {
        $addresses = $this->getAddressesAsArray();
        if ([] === $addresses) {
            return (null);
        }

        // There is at least one address, and the first address is the main address
        return ($addresses[0]);
    }

    /**
     * Return all postal addresses associated with this member. The main address is returned first, then all other
     * addresses. This method returns an array with the following fields:
     * ['address']
     * ['zip']
     * ['city']
     * ['country']
     * ['type']
     * ['is_main_address']
     * ['company'] <-- this field is only inserted, when this is a company address
     * If the parameter addIndexFields is set to true, the mapping index is added as key for every address entry and the
     * following fields are added:
     * ['map_id']
     * ['address_id']
     * If no addresses are available, an empty array is returned
     *
     * @param bool $addIndexFields
     *
     * @return array All addresses associated with this member entry.
     */
    public function getAddressesAsArray(bool $addIndexFields = false)
    {
        $mainAddress = array();
        $addresses = array();
        foreach ($this->memberAddressMaps as $addressMap) {
            $address = $addressMap->getAddress()->toArray();
            if (array_key_exists('company', $address)) {
                if ($this->getCompanyInformation()) {
                    $address['company'] = $this->getCompanyInformation()->getName();
                } else {
                    unset($address['company']);
                }
            }
            $address['is_main_address'] = $addressMap->getIsMainAddress();
            if ($addIndexFields) {
                $address['map_id'] = $addressMap->getId();
                $address['address_id'] = $addressMap->getAddress()->getId();
            }
            // When this is the main address, copy it to a special array, copy to the normal addresses array if not.
            if ($addressMap->getIsMainAddress()) {
                $mainAddress = $address;
            } else {
                $addresses[] = $address;
            }
        }
        // Copy the main address as first address in the addresses array. When there are no addresses, then unshift
        // would insert mainAddress, even when this is empty. To prevent this, only copy when there is at least one
        // address.
        if (count($mainAddress)) {
            array_unshift($addresses, $mainAddress);
        }

        // Now add the mapping index.
        if ($addIndexFields) {
            $addressesWithIndex = array();
            foreach ($addresses as $address) {
                $addressesWithIndex[$address['map_id']] = $address;
            }
            $addresses = $addressesWithIndex;
        }

        return ($addresses);
    }

    /**
     * @return mixed
     */
    public function getPlainPassword()
    {
        return $this->plainPassword;
    }

    /**
     * @param mixed $plainPassword
     */
    public function setPlainPassword($plainPassword)
    {
        $this->plainPassword = $plainPassword;
        // Guarantees that the entity looks "dirty" to Doctrine when changing the plain password, so the listener
        // updates the encrypted password.
        $this->password = '';
    }


    /**
     * String representation of object
     *
     * @link http://php.net/manual/en/serializable.serialize.php
     *
     * @return string the string representation of the object or null
     *
     * @since 5.1.0
     */
    public function serialize()
    {
        return serialize(array(
            $this->id,
            $this->username,
            $this->password,
        ));
    }

    /**
     * Constructs the object
     *
     * @link http://php.net/manual/en/serializable.unserialize.php
     *
     * @param string $serialized <p>
     *                           The string representation of the object.
     *                           </p>
     * @return void
     *
     * @since 5.1.0
     */
    public function unserialize($serialized)
    {
        list (
            $this->id,
            $this->username,
            $this->password,
            ) = unserialize($serialized);
    }

    /**
     * Return the members in the currents members group.
     *
     * @return ArrayCollection|MemberEntry[] All members in this members group
     */
    public function getGroupMembers()
    {
        return $this->groupMembers;
    }

    /**
     * Add a member to the current group. This also adds all members to the new member and the new member to all
     * existing group members.
     *
     * @param MemberEntry $newGroupMember
     * @param bool        $recurse        When false, only update the current member and do not update the group list.
     */
    public function addGroupMember(MemberEntry $newGroupMember, bool $recurse = true)
    {
        if (($newGroupMember !== $this) && (!$this->groupMembers->contains($newGroupMember))) {
            if ($recurse) {
                foreach ($this->groupMembers as $groupMember) {
                    $groupMember->addGroupMember($newGroupMember, false);
                }
            }
            $this->groupMembers->add($newGroupMember);
            $newGroupMember->addGroupMember($this, false);
        }
    }

    /**
     * remove a member from the current group
     * @param MemberEntry $groupMemberToRemove
     * @param bool        $recurse             When false, only update the current member and do not update the group
     *                                         list.
     */
    public function removeGroupMember(MemberEntry $groupMemberToRemove, bool $recurse = true)
    {
        if ($this->groupMembers->contains($groupMemberToRemove)) {
            $this->groupMembers->removeElement($groupMemberToRemove);
            $groupMemberToRemove->removeGroupMember($this, false);
            if ($recurse) {
                foreach ($this->groupMembers as $groupMember) {
                    $groupMember->removeGroupMember($groupMemberToRemove, false);
                }
            }
        }
    }

    /**
     * @inheritdoc
     *
     * @return LogEntityChangeContainer
     *
     * @throws \Exception
     *
     * @SuppressWarnings("PMD.NPathComplexity")
     */
    public function getEntityContent()
    {
        $contentContainer = new LogEntityChangeContainer();
        $contentContainer->setClassName(get_class($this));
        $contentContainer->setIndex($this->id);
        $contentContainer->setChangeType('member data change');
        $contentContainer->setChangesOnMemberId($this->id);
        $groupMembers = [];
        foreach ($this->groupMembers as $groupMember) {
            $groupMembers[] = $groupMember->getCompleteName();
        }
        $groupMembers = implode(', ', $groupMembers);

        $contentContainer
            ->addContentItem('first name', false, $this->firstName)
            ->addContentItem('last name', false, $this->name)
            ->addContentItem('gender', false, ($this->gender === 'f')? new LogEntryDataField('Female', true) : new LogEntryDataField('Male', true))
            ->addContentItem('membership end requested', true, $this->getMembershipEndRequested() ? new LogEntryDataField('yes', true) : new LogEntryDataField('no', true))
            ->addContentItem('preferred language', true, new LogEntryDataField($this->preferredLanguage, true))
            ->addContentItem('status', true, new LogEntryDataField($this->status, true))
            ->addContentItem('membership type', true, new LogEntryDataField($this->membershipType, true))
            ->addContentItem('user role', true, $this->userRole ? new LogEntryDataField($this->userRole, true) : null)
            ->addContentItem('birthday', true, $this->birthday ? $this->birthday->format('Y-m-d') : null)
            ->addContentItem('title', true, $this->title ? $this->title->getTitle() : null)
            ->addContentItem('email', true, $this->email)
            ->addContentItem('membership number', false, $this->membershipNumber ? $this->membershipNumber->getMembershipNumber() : '-')
            ->addContentItem('member since', true, $this->memberSince)
            ->addContentItem('Membership group members', true, $groupMembers);

        return $contentContainer;
    }

    /**
     * Setting the user name directly is not allowed (i.e. private!). The user name is generated automatically from the
     * first name and the last name. From FirstName LastName, a user name like firstname.lastname is generated. There
     * are some special actions done when the name has commas or dots and spaces in it. For all cases, see the unit
     * test.
     *
     * @return MemberEntry
     */
    private function setUsername()
    {
        // Create first ste
        $userName = $this->commaReversal($this->firstName).".".$this->commaReversal($this->name);

        // When there are spaces in the name, replace those by dots
        $userName = str_replace(" ", ".", $userName);

        // When there are double dots in the name, replace those by a single dot. Because the replacement can generate
        // another dot pair, repeat this operation until no pairs are found anymore.
        while (strpos($userName, "..") !== false) {
            $userName = str_replace("..", ".", $userName);
        }

        $this->username = mb_strtolower($userName);

        return $this;
    }

    /**
     * In some name parts, a comma is used, e.g. when the last name is "Berg, van den". To generate a correct user name,
     * the parts after the comma have to be moved to the beginning, e.g. generating "van den Berg".
     *
     * @param string $name Pass the name in which the commas have to be removed and the part reversed.
     *
     * @return string Corrected string.
     */
    private function commaReversal($name)
    {
        $parts = explode(",", $name);
        $result = '';
        foreach (array_reverse($parts) as $part) {
            $result .= "$part ";
        }

        // Remove the last space in the string
        return (substr($result, 0, -1));
    }
}
