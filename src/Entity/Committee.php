<?php

/*
 * Copyright (C) 2022 by Luc Hamers <vereniging@hamers.de>
 *
 * This file is part of Vereniging.
 *
 * Vereniging is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Vereniging is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Vereniging.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Knp\DoctrineBehaviors\Contract\Entity\TranslatableInterface;
use Knp\DoctrineBehaviors\Model\Translatable\TranslatableTrait;

/**
 * Committee
 *
 * @ORM\Entity(repositoryClass="App\Repository\CommitteeRepository")
 * @ORM\Table(name="committee")
 * @ORM\HasLifecycleCallbacks()
 */
class Committee implements ValueListInterface, EntityLoggerInterface, EntityTranslationInterface, TranslatableInterface
{
    use TranslatableTrait;

    /**
     * @ORM\Id
     * @ORM\Column(type="smallint")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=45)
     */
    protected $committeeName;

    /**
     * @ORM\OneToMany(targetEntity="CommitteeFunctionMap", mappedBy="committee", cascade={"persist"}, orphanRemoval=true)
     * @ORM\JoinColumn(name="id", referencedColumnName="committee_id")
     */
    protected $committeeFunctionMaps;

    /**
     * Constructor.
     *
     * @param string|null $committeeName
     */
    public function __construct(string $committeeName = null)
    {
        $this->committeeFunctionMaps = new ArrayCollection();
        $this->setCommitteeName($committeeName);
    }

    /**
     * Get the value of id.
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set the value of committee.
     *
     * @param string $committee
     */
    public function setCommitteeName($committee)
    {
        // When the committee name is set for the first time or when it is changed, then update or set the default
        // translation.
        if ($this->committeeName !== $committee) {
            $this->translate($this->getDefaultLocale())->setCommitteeNameTranslated($committee);
        }

        $this->committeeName = $committee;
    }

    /**
     * Get the value of committee.
     *
     * @return string
     */
    public function getCommitteeName()
    {
        return $this->proxyCurrentLocaleTranslation('getCommitteeNameTranslated', []);
    }

    /**
     * Add CommitteeFunctionMap entity to collection (one to many).
     *
     * @param CommitteeFunctionMap $committeeFunctionMap
     *
     * @return true if the map has been changed by this action
     */
    public function addCommitteeFunctionMap(CommitteeFunctionMap $committeeFunctionMap)
    {
        $committeeFunctionMap->setCommittee($this);
        if ($this->committeeFunctionMapsContains($committeeFunctionMap)) {
            return false;
        }

        $this->committeeFunctionMaps[] = $committeeFunctionMap;

        return true;
    }

    /**
     * Remove CommitteeFunctionMap entity from collection (one to many).
     *
     * @param CommitteeFunctionMap $committeeFunctionMap
     *
     * @return true if the map has been changed by this action
     */
    public function removeCommitteeFunctionMap(CommitteeFunctionMap $committeeFunctionMap)
    {
        $committeeFunctionMap->setCommittee($this);
        // To be able to remove the map from the array collection, the completely hydrated object is needed. This is
        // returned by the committeeFunctionMapsContains method.
        if (!$committeeFunctionMap = $this->committeeFunctionMapsContains($committeeFunctionMap)) {
            return false;
        }
        $this->committeeFunctionMaps->removeElement($committeeFunctionMap);
        $committeeFunctionMap->setCommittee(null);

        return true;
    }

    /**
     * Get CommitteeFunctionMap entity collection (one to many).
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCommitteeFunctionMaps()
    {
        return $this->committeeFunctionMaps;
    }

    /**
     * Return content (committee name) as string.
     *
     * @return mixed
     */
    public function __toString()
    {
        return (string) $this->getCommitteeName();
    }

    /**
     * @inheritdoc
     *
     * @return LogEntityChangeContainer
     */
    public function getEntityContent()
    {
        $contentContainer = new LogEntityChangeContainer();
        $contentContainer->setClassName(get_class($this));
        $contentContainer->setIndex($this->id);
        $contentContainer->setShortContent(['committee name' => $this->committeeName]);
        $contentContainer->setChangeType('system settings change');

        return $contentContainer;
    }

    /**
     * When the entity is created for the first time, there will be translations present, either the translation for the
     * default locale  or translations added by hand. These must be saved in the database, which is done by the merge
     * new translations command. To ensure that this is called only once regardless of how the translations are added
     * (e.g. by constructor, setter or by hand), the merge is done before persisting the object.
     *
     * @ORM\PrePersist()
     */
    public function mergeNewTranslationsOnPersist()
    {
        $this->mergeNewTranslations();
    }

    /**
     * @return bool
     */
    public function isInUse()
    {
        return false;
    }

    /**
     * Use this method to add a translation for a certain locale. To store these to the database, a doctrine flush is
     * needed.
     *
     * @param string $locale
     * @param string $translation
     */
    public function addNewTranslation(string $locale, string $translation)
    {
        $this->translate($locale, false)->setCommitteeNameTranslated($translation);
        $this->mergeNewTranslations();
    }

    /**
     * Because the contains method of the ArrayCollection doesn't work correctly with nog fully hydrated entities, we
     * use our own contains method, which only checks the function and the committee id, but not the entities id or the
     * memberEntities.
     *
     * @param CommitteeFunctionMap $searchMap
     *
     * @return CommitteeFunctionMap Found map if the search map is contained in the committeeFunctionMaps collection,
     *                              null if not.
     */
    private function committeeFunctionMapsContains(CommitteeFunctionMap $searchMap)
    {
        foreach ($this->committeeFunctionMaps as $map) {
            if ($searchMap->getCommitteeFunction()->getId() === $map->getCommitteeFunction()->getId()) {
                if ($searchMap->getCommittee()->getId() === $map->getCommittee()->getId()) {
                    return ($map);
                }
            }
        }

        return (null);
    }
}
