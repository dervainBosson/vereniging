<?php

/*
 * Copyright (C) 2022 by Luc Hamers <vereniging@hamers.de>
 *
 * This file is part of Vereniging.
 *
 * Vereniging is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Vereniging is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Vereniging.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Entity;

use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Address
 *
 * @ORM\Entity(repositoryClass="App\Repository\AddressRepository")
 */
class Address implements EntityLoggerInterface
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=45)
     */
    protected $address;

    /**
     * @ORM\Column(type="string", length=7)
     */
    protected $zip;

    /**
     * @ORM\Column(type="string", length=45)
     */
    protected $city;

    /**
     * @ORM\OneToMany(targetEntity="MemberAddressMap", mappedBy="address", cascade={"persist"}, orphanRemoval=true)
     *
     * @var Collection|MemberAddressMap[]
     */
    protected $memberAddressMaps;

    /**
     * @ORM\ManyToOne(targetEntity="AddressType", inversedBy="addresses")
     * @ORM\JoinColumn(name="address_type_id", referencedColumnName="id")
     *
     * @var AddressType
     */
    protected $addressType;

    /**
     * @ORM\ManyToOne(targetEntity="Country", inversedBy="addresses")
     * @ORM\JoinColumn(name="country_id", referencedColumnName="id")
     *
     * @var Country
     */
    protected $country;

    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->memberAddressMaps = new ArrayCollection();
    }

    /**
     * Get the value of id.
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set the value of address.
     *
     * @param string $address
     */
    public function setAddress($address)
    {
        $this->address = $address;
    }

    /**
     * Get the value of address.
     *
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set the value of zip.
     *
     * @param string $zip
     */
    public function setZip($zip)
    {
        $this->zip = $zip;
    }

    /**
     * Get the value of zip.
     *
     * @return string
     */
    public function getZip()
    {
        return $this->zip;
    }

    /**
     * Set the value of city.
     *
     * @param string $city
     */
    public function setCity($city)
    {
        $this->city = $city;
    }

    /**
     * Get the value of city.
     *
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set AddressType entity (many to one).
     *
     * @param AddressType $addressType
     */
    public function setAddressType(AddressType $addressType = null)
    {
        $this->addressType = $addressType;
    }

    /**
     * Get AddressType entity (many to one).
     *
     * @return AddressType
     */
    public function getAddressType()
    {
        return $this->addressType;
    }

    /**
     * Set Country entity (many to one).
     *
     * @param Country $country
     */
    public function setCountry(Country $country = null)
    {
        $this->country = $country;
    }

    /**
     * Get Country entity (many to one).
     *
     * @return Country
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Add MemberAddressMap entity to collection (many to many).
     *
     * @param MemberAddressMap $memberAddressMap
     */
    public function addMemberAddressMap(MemberAddressMap $memberAddressMap)
    {
        if ($this->memberAddressMaps->contains($memberAddressMap)) {
            return;
        }

        $this->memberAddressMaps[] = $memberAddressMap;
    }

    /**
     * Copy objects data into an associative array.
     *
     * @return array Return an array containing the address data.
     */
    public function toArray()
    {
        $data = array();
        $data['address'] = $this->address;
        $data['zip'] = $this->zip;
        $data['city'] = $this->city;
        $data['country'] = $this->country->getCountry();
        $data['type'] = $this->addressType->getAddressType();
        if ($this->addressType->getUseCompanyName()) {
            $data['company'] = true;
        }

        return ($data);
    }

    /**
     * @inheritdoc
     *
     * @return LogEntityChangeContainer
     */
    public function getEntityContent()
    {
        $isMainAddress = 'no';
        $memberId = null;
        if (isset($this->memberAddressMaps[0])) {
            $memberId = $this->memberAddressMaps[0]->getMemberEntry()->getId();
            $isMainAddress = $this->memberAddressMaps[0]->getIsMainAddress() ? 'yes' : 'no';
        }
        $contentContainer = new LogEntityChangeContainer();
        $contentContainer->setClassName(get_class($this));
        $contentContainer->setIndex($this->id);
        $contentContainer->setShortContent([
            'address' => $this->getAddress(),
            'address type' => new LogEntryDataField($this->addressType, true),
            'zip' => $this->zip,
            'city' => $this->city,
            'country' => new LogEntryDataField($this->country, true),
            'is main address' => new LogEntryDataField($isMainAddress, true),
        ]);
        $contentContainer->setChangeType('member data change');
        $contentContainer->setChangesOnMemberId($memberId);

        return $contentContainer;
    }
}
