<?php

/*
 * Copyright (C) 2022 by Luc Hamers <vereniging@hamers.de>
 *
 * This file is part of Vereniging.
 *
 * Vereniging is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Vereniging is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Vereniging.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Contract\Entity\TranslatableInterface;
use Knp\DoctrineBehaviors\Model\Translatable\TranslatableTrait;

/**
 * ChangeType
 *
 * @ORM\Entity(repositoryClass="App\Repository\ChangeTypeRepository")
 * @ORM\Table(name="change_type")
 * @ORM\HasLifecycleCallbacks()
 */
class ChangeType implements TranslatableInterface
{
    use TranslatableTrait;

    /**
     * @ORM\Id
     * @ORM\Column(type="smallint")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=45)
     */
    protected $changeType;

    /**
     * @ORM\OneToMany(targetEntity="LogfileEntry", mappedBy="changeType")
     * @ORM\JoinColumn(name="id", referencedColumnName="change_type_id")
     */
    protected $logfileEntries;

    /**
     * @ORM\ManyToMany(targetEntity="MemberEntry", mappedBy="emailNotifications")
     */
    protected $memberEntries;

    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->memberEntries = new ArrayCollection();
    }

    /**
     * Get the value of id.
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set the value of change_type.
     *
     * @param string $changeType
     */
    public function setChangeType($changeType)
    {
        // When the change type is set for the first time or when it is changed, then update or set the default
        // translation.
        if ($this->changeType !== $changeType) {
            $this->translate($this->getDefaultLocale())->setChangeTypeTranslated($changeType);
        }

        $this->changeType = $changeType;
    }

    /**
     * Get the value of change_type.
     *
     * @return string
     */
    public function getChangeType()
    {
        return $this->proxyCurrentLocaleTranslation('getChangeTypeTranslated', []);
    }

    /**
     * Add MemberEntry entity to collection.
     *
     * @param MemberEntry $memberEntry
     */
    public function addMemberEntry(MemberEntry $memberEntry)
    {
        if (!$this->memberEntries->contains($memberEntry)) {
            $this->memberEntries[] = $memberEntry;
            $memberEntry->addEmailNotification($this);
        }
    }

    /**
     * Remove MemberEntry entity from collection.
     *
     * @param MemberEntry $memberEntry
     */
    public function removeMemberEntry(MemberEntry $memberEntry)
    {
        if ($this->memberEntries->contains($memberEntry)) {
            $this->memberEntries->removeElement($memberEntry);
            $memberEntry->removeEmailNotification($this);
        }
    }

    /**
     * Return all member entries which are subscribed to this change type (i.e. for log messages)
     *
     * @return ArrayCollection|MemberEntry[]
     */
    public function getEmailNotificationMembers()
    {
        return $this->memberEntries;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return (string) $this->getChangeType();
    }

    /**
     * When the entity is created for the first time, there will be translations present, either the translation for the
     * default locale  or translations added by hand. These must be saved in the database, which is done by the merge
     * new translations command. To ensure that this is called only once regardless of how the translations are added
     * (e.g. by constructor, setter or by hand), the merge is done before persisting the object.
     *
     * @ORM\PrePersist()
     */
    public function mergeNewTranslationsOnPersist()
    {
        $this->mergeNewTranslations();
    }
}
