<?php

/*
 * Copyright (C) 2022 by Luc Hamers <vereniging@hamers.de>
 *
 * This file is part of Vereniging.
 *
 * Vereniging is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Vereniging is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Vereniging.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CompanyInformation
 *
 * @ORM\Entity(repositoryClass="App\Repository\CompanyInformationRepository")
 * @ORM\Table(name="company_information")
 */
class CompanyInformation implements EntityLoggerInterface
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(name="`name`", type="string", length=100)
     */
    protected $name;

    /**
     * @ORM\Column(type="string", length=100)
     */
    protected $city;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    protected $description;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    protected $functionDescription;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    protected $url;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    protected $logo;

    /**
     * @ORM\OneToOne(targetEntity="MemberEntry", mappedBy="companyInformation")
     *
     * @var MemberEntry
     */
    protected $memberEntry;

    /**
     * Set the value of name.
     *
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * Get the value of name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set the value of city.
     *
     * @param string $city
     */
    public function setCity($city)
    {
        $this->city = $city;
    }

    /**
     * Get the value of city.
     *
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set the value of description.
     *
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * Get the value of description.
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set the value of function_description.
     *
     * @param string $functionDescription
     */
    public function setFunctionDescription($functionDescription)
    {
        $this->functionDescription = $functionDescription;
    }

    /**
     * Get the value of function_description.
     *
     * @return string
     */
    public function getFunctionDescription()
    {
        return $this->functionDescription;
    }

    /**
     * Set the value of url.
     *
     * @param string $url
     */
    public function setUrl($url)
    {
        $this->url = $url;
    }

    /**
     * Get the value of url.
     *
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }


    /**
     * Set the value of member entry. Usually doctrine takes care of this. In case of the need to read back this value
     * within one transaction, this has to be set manually
     *
     * @param MemberEntry $memberEntry
     */
    public function setMemberEntry(MemberEntry $memberEntry)
    {
        $this->memberEntry = $memberEntry;
    }

    /**
     * @inheritdoc
     *
     * @return LogEntityChangeContainer
     */
    public function getEntityContent()
    {
        $contentContainer = new LogEntityChangeContainer();
        $contentContainer->setClassName(get_class($this));
        $contentContainer->setIndex($this->id);
        $contentContainer->setShortContent(['company name' => $this->name]);
        $contentContainer->setExtendedContent([
            'city' => $this->city,
            'function' => $this->functionDescription,
            'description' => $this->description,
            'url' => $this->url,
            'logo' => $this->logo,
        ]);
        $contentContainer->setChangeType('member data change');
        $contentContainer->setChangesOnMemberId($this->memberEntry ? $this->memberEntry->getId() : null);

        return $contentContainer;
    }
}
