<?php

/*
 * Copyright (C) 2022 by Luc Hamers <vereniging@hamers.de>
 *
 * This file is part of Vereniging.
 *
 * Vereniging is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Vereniging is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Vereniging.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Repository;

use App\Entity\MembershipType;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;

/**
 * MembershipTypeRepository.
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class MembershipTypeRepository extends EntityRepository
{
    /**
     * Overwrite the default findAll method, so the type list is ordered by name.
     *
     * @return array
     */
    public function findAll(): array
    {
        return $this->findBy([], ['type' => 'ASC']);
    }


    /**
     * Overwrite the default findAll method, so the type list is ordered by name.
     *
     * @param string $currentLocale e.g. 'en' or 'de'
     *
     * @return array
     */
    public function findAllOrderedWithLocale(string $currentLocale): array
    {
        /** @var MembershipType[] $types */
        $types = $this->createFindByGroupMembershipQueryBuilder($currentLocale, false)
                      ->getQuery()->execute();

        return $types;
    }


    /**
     * Create a query builder for membership types. The types are ordered by the types in the selected locale and can be
     * filtered to only return the types for group membership types.
     *
     * @param string $currentLocale        e.g. 'en' or 'de'
     * @param bool   $onlyGroupMemberships True when only group membership types should be returned
     *
     * @return QueryBuilder
     */
    public function createFindByGroupMembershipQueryBuilder(string $currentLocale, bool $onlyGroupMemberships): QueryBuilder
    {
        $qb = $this->createQueryBuilder('membership_type')
                   ->innerJoin('membership_type.translations', 'translation')
                   ->where('translation.locale = ?1')
                   ->setParameter(1, $currentLocale)
                   ->orderBy('translation.typeTranslated', 'ASC');

        if ($onlyGroupMemberships) {
            $qb->andWhere('membership_type.groupMembership = ?2')
                ->setParameter(2, true);
        }

        return $qb;
    }


    /**
     * Find membership types and return as json array. The types are ordered by the types in the selected locale and can
     * be filtered to only return the types for group membership types.
     *
     * @param string $currentLocale        e.g. 'en' or 'de'
     * @param bool   $onlyGroupMemberships True when only group membership types should be returned
     *
     * @return string
     */
    public function findByGroupMembershipAsJson(string $currentLocale, bool $onlyGroupMemberships): string
    {
        /** @var MembershipType[] $types */
        $types = $this->createFindByGroupMembershipQueryBuilder($currentLocale, $onlyGroupMemberships)
                      ->getQuery()->execute();

        $typesArray = [];
        foreach ($types as $type) {
            $typesArray[] = [
                'id' => $type->getId(),
                'type' => $type->translate($currentLocale)->getTypeTranslated(),
                'isDefaultGroupMembershipType' => $type->getDefaultGroupMembershipType(),
                'isDefaultNewMembershipType' => $type->getDefaultNewMembershipType(),
                'isGroupMembershipType' => $type->getGroupMembership(),
            ];
        }

        return json_encode($typesArray);
    }
}
