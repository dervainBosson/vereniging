<?php

/*
 * Copyright (C) 2022 by Luc Hamers <vereniging@hamers.de>
 *
 * This file is part of Vereniging.
 *
 * Vereniging is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Vereniging is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Vereniging.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Repository;

use App\Entity\MemberEntry;
use App\Entity\MembershipFeeTransaction;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * Repository class for membership fee transactions
 */
class MembershipFeeTransactionsRepository extends ServiceEntityRepository
{
    /**
     * MembershipFeeTransactionsRepository constructor.
     *
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, MembershipFeeTransaction::class);
    }


    /**
     * Return all membership fee transactions for a member (it searches for the entries belonging to the membership
     * number).
     *
     * @param MemberEntry $memberEntry Member for which the membership fee transactions are to be found
     *
     * @return array
     */
    public function findAllByMember(MemberEntry $memberEntry): array
    {
        return $this->findBy(['membershipNumber' => $memberEntry->getMembershipNumber()], ['createDate' => 'DESC']);
    }


    /**
     * return an array with the statistics for membership transactions. The array looks like this:
     * [2020 => [expected => 123, paid => 110, outstanding => 13],
     *  2019 => [expected => 120, paid => 120, outstanding => 0]]
     *
     * @return array
     */
    public function getMembershipFeeStatistics(): array
    {
        $membershipFees = $this->findBy([], ['createDate' => 'DESC']);

        $result = [];

        foreach ($membershipFees as $membershipFee) {
            $year = $membershipFee->getCreateDate()->format('Y');
            if (!array_key_exists($year, $result)) {
                $result[$year] = ['expected' => 0, 'paid' => 0, 'outstanding' => 0];
            }

            $result[$year]['expected'] += $membershipFee->getAmount();
            if ($membershipFee->getCloseDate()) {
                $result[$year]['paid'] += $membershipFee->getAmount();
            } else {
                $result[$year]['outstanding'] += $membershipFee->getAmount();
            }
        }

        return $result;
    }
}
