<?php

/*
 * Copyright (C) 2022 by Luc Hamers <vereniging@hamers.de>
 *
 * This file is part of Vereniging.
 *
 * Vereniging is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Vereniging is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Vereniging.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Repository;

use App\Entity\UserRole;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;

/**
 * UserRoleRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class UserRoleRepository extends EntityRepository
{
    /**
     * Create query builder which finds all user roles ordered in the current locale
     *
     * @param string $currentLocale Locale of the current user for ordering the entries
     *
     * @return QueryBuilder
     */
    public function createFindAllOrderedQueryBuilder(string $currentLocale): QueryBuilder
    {
        return $this->createQueryBuilder('userRole')
            ->innerJoin('userRole.translations', 'translation')
            ->where('translation.locale = :locale')
            ->setParameter('locale', $currentLocale)
            ->orderBy('translation.userRoleTranslated', 'ASC')
            ;
    }


    /**
     * Find all user roles and order them first by the number of system roles and, if equal, by the translation of the
     * system roles.
     *
     * @param string $currentLocale
     *
     * @return UserRole[]
     */
    public function findAllOrderedByRoleCount(string $currentLocale): array
    {
        $userRoles = $this->createFindAllOrderedQueryBuilder($currentLocale)
                          ->getQuery()->getResult();
        usort($userRoles, ['App\Repository\UserRoleRepository', 'userRoleCompare']);

        return $userRoles;
    }


    /**
     * This orders user roles by the number of system roles and, if this number is identical, by the name of the user
     * role.
     *
     * @param UserRole $a
     * @param UserRole $b
     *
     * @return int
     */
    private static function userRoleCompare(UserRole $a, UserRole $b): int
    {
        if (count($a->getSystemRoles()) === count($b->getSystemRoles())) {
            return 0;
        }

        return (count($a->getSystemRoles()) > count($b->getSystemRoles())) ? +1 : -1;
    }
}
