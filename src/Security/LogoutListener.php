<?php

/*
 * Copyright (C) 2022 by Luc Hamers <vereniging@hamers.de>
 *
 * This file is part of Vereniging.
 *
 * Vereniging is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Vereniging is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Vereniging.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Security;

use App\Service\LogMessageCreator;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Component\Security\Http\Event\LogoutEvent;

/**
 * Class LogoutListener
 */
class LogoutListener
{
    private EntityManagerInterface $entityManager;
    private LogMessageCreator $logMessageCreator;

    /**
     * LogoutListener constructor.
     *
     * @param EntityManagerInterface $entityManager
     * @param LogMessageCreator      $logMessageCreator
     */
    public function __construct(EntityManagerInterface $entityManager, LogMessageCreator $logMessageCreator)
    {
        $this->entityManager = $entityManager;
        $this->logMessageCreator = $logMessageCreator;
    }


    /**
     * This method is called by the LogoutListener when a user has requested to be logged out. This is used to create a
     * log message.
     *
     * @param LogoutEvent $logoutEvent
     *
     * @throws Exception
     */
    public function onSymfonyComponentSecurityHttpEventLogoutEvent(LogoutEvent $logoutEvent): void
    {
        // When the user is logged off automatically (SessionIdleHandler), then the token has been set to null and the
        // token return a string as user.
        $token = $logoutEvent->getToken();

        if ($token) {
            $user = $token->getUser();
            $this->logMessageCreator->setUser($user);
            $this->logMessageCreator->createLogEntry('login', 'Logout succeeded', $user);
            $this->entityManager->flush();
        }
    }
}
