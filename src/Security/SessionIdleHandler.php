<?php

/*
 * Copyright (C) 2022 by Luc Hamers <vereniging@hamers.de>
 *
 * This file is part of Vereniging.
 *
 * Vereniging is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Vereniging is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Vereniging.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Security;

use App\Entity\MemberEntry;
use App\Service\LogMessageCreator;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\HttpKernelInterface;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

/**
 * Class SessionIdleHandler
 *
 * This implements a idle timeout, which closes the session when the user stops to interact with the system. This is
 * based on code found on github (https://stackoverflow.com/questions/18872721/how-to-log-users-off-automatically-after-a-period-of-inactivity/21285238,
 * Element Zero)
 */
class SessionIdleHandler
{
    private EntityManagerInterface $entityManager;
    private TokenStorageInterface $securityToken;
    private RouterInterface $router;
    private int $maxIdleTimeSeconds;
    private LogMessageCreator $logMessageCreator;

    /**
     * SessionIdleHandler constructor.
     *
     * @param EntityManagerInterface $entityManager
     * @param TokenStorageInterface  $securityToken
     * @param RouterInterface        $router
     * @param LogMessageCreator      $logMessageCreator
     * @param int                    $maxIdleTimeSeconds Defines the time after which the user is logged out.
     *
     * @throws Exception
     */
    public function __construct(EntityManagerInterface $entityManager, TokenStorageInterface $securityToken, RouterInterface $router, LogMessageCreator $logMessageCreator, int $maxIdleTimeSeconds)
    {
        if (0 >= $maxIdleTimeSeconds) {
            throw new Exception(sprintf('maxIdleTime must be greater than zero, but "%d" was passed!', $maxIdleTimeSeconds));
        }

        $this->entityManager = $entityManager;
        $this->securityToken = $securityToken;
        $this->router = $router;
        $this->maxIdleTimeSeconds = $maxIdleTimeSeconds;
        $this->logMessageCreator = $logMessageCreator;
    }


    /**
     * Destroy the security token when the user doesn't interact with the system for some time.
     *
     * @param RequestEvent $event
     *
     * @throws Exception
     */
    public function onKernelRequest(RequestEvent $event): void
    {
        if (HttpKernelInterface::MAIN_REQUEST !== $event->getRequestType()) {
            return;
        }

        $session = $event->getRequest()->getSession();
        $session->start();
        $lapse = time() - $session->getMetadataBag()->getLastUsed();


        if ($lapse > $this->maxIdleTimeSeconds) {
            // Waiting for a long time could result in a security token of null
            if ($this->securityToken->getToken()) {
                /** @var MemberEntry $currentUser */
                $currentUser = $this->securityToken->getToken()->getUser();
                $this->logMessageCreator->createLogEntry('login', 'User logged off automatically', $currentUser);
                $this->entityManager->flush();
            }
            $this->securityToken->setToken(null);

            $event->setResponse(new RedirectResponse($this->router->generate('security_logout')));
        }
    }
}
