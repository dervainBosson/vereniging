<?php

/*
 * Copyright (C) 2022 by Luc Hamers <vereniging@hamers.de>
 *
 * This file is part of Vereniging.
 *
 * Vereniging is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Vereniging is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Vereniging.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Controller\Member;

use App\Entity\LogfileEntry;
use App\Entity\MemberEntry;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * Class MemberLogFileController
 */
class MemberLogFileController extends AbstractController
{
    /**
     * @Route("/member/show_member_logs/{id}", name="member_show_logs")
     *
     * @param MemberEntry            $memberEntry
     * @param EntityManagerInterface $entityManager
     * @param TranslatorInterface    $translator
     *
     * @return Response
     */
    public function showMemberLogFileAction(MemberEntry $memberEntry, EntityManagerInterface $entityManager, TranslatorInterface $translator): Response
    {
        $logfileEntries = $entityManager->getRepository(LogfileEntry::class)
                                        ->findBy(['changesOnMember' => $memberEntry], ['date' => 'DESC', 'id' => 'DESC'], 5);

        LogfileEntry::prepareTranslation($entityManager, $translator, $this->getUser()->getPreferredLanguage()->getLocale());

        return $this->render('Member/log_entries.html.twig', [
            'logfile_entries' => $logfileEntries,
        ]);
    }
}
