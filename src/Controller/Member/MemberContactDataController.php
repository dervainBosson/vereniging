<?php

/*
 * Copyright (C) 2022 by Luc Hamers <vereniging@hamers.de>
 *
 * This file is part of Vereniging.
 *
 * Vereniging is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Vereniging is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Vereniging.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Controller\Member;

use App\Entity\MailingList;
use App\Entity\MemberEntry;
use App\Form\MemberEntryContactDataFormType;
use App\Service\LogMessageCreator;
use App\Service\MailingListUpdateInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;

/**
 * Controller for editing member entry personal data.
 *
 */
class MemberContactDataController extends AbstractController
{
    private EntityManagerInterface $entityManager;
    private LogMessageCreator $logMessageCreator;
    private MailingListUpdateInterface $mailingListUpdater;


    /**
     * MemberContactDataController constructor.
     *
     * @param EntityManagerInterface     $entityManager
     * @param LogMessageCreator          $logMessageCreator
     * @param MailingListUpdateInterface $mailingListUpdater
     * @param Security                   $security
     */
    public function __construct(EntityManagerInterface $entityManager, LogMessageCreator $logMessageCreator, MailingListUpdateInterface $mailingListUpdater, Security $security)
    {

        $this->entityManager = $entityManager;
        $this->logMessageCreator = $logMessageCreator;
        $this->mailingListUpdater = $mailingListUpdater;
        $this->logMessageCreator->setUser($security->getUser());
    }


    /**
     * This method is called when the member entry has to be edited or when it is submitted after editing.
     *
     * @Route("/member/edit_contact_data/{id}", name="member_edit_contact_data")
     *
     * @param MemberEntry $member  User for which the contact data is shown.
     * @param Request     $request
     *
     * @return Response
     *
     * @throws Exception
     */
    public function editAction(MemberEntry $member, Request $request): Response
    {
        // When the user is only allowed to edit its own data and he tries to edit other users, throw an exception.
        if (!$this->isGranted('ROLE_EDIT_OTHER_USERS') && ($this->getUser()->getUsername() !== $member->getUsername())) {
            throw $this->createAccessDeniedException('You are not allowed to edit other users data!');
        }

        $originalEmail = $member->getEmail();

        // Copy the current mailing list subscriptions into a new array collection to be able to compare with the new
        // subscription.
        $originalMailingLists = new ArrayCollection($member->getMailingLists()->toArray());

        $form = $this->createForm(MemberEntryContactDataFormType::class, $member, [
            'current_locale' => $request->getLocale(),
        ]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // Prevent code injections.
            if (!is_null($member->getEmail())) {
                $member->setEmail(htmlentities($member->getEmail()));
            }

            // Remove empty phone numbers from the member entity and from the database all together.
            foreach ($member->getPhoneNumbers() as $phoneNumber) {
                if (strlen($phoneNumber->getPhoneNumber()) === 0) {
                    $member->removePhoneNumber($phoneNumber);
                    $this->entityManager->remove($phoneNumber);
                }
            }

            $this->updateMailingLists($originalEmail, $originalMailingLists, $member);

            $this->entityManager->flush();

            return $this->render("Member/contact_data.html.twig", array(
                'member' => $member,
                'edit' => false,
            ));
        }

        return $this->render("Member/contact_data.html.twig", array(
            'member' => $member,
            'edit' => true,
            'form' => $form->createView(),
        ));
    }


    /**
     * This method is called when the member entry is switched from editing to not-editing by pressing cancel.
     *
     * @Route("/member/cancel_contact_data/{id}", name="member_cancel_contact_data")
     *
     * @param MemberEntry $member User entry which is edited.
     *
     * @return Response
     */
    public function cancelAction(MemberEntry $member): Response
    {
        return $this->render("Member/contact_data.html.twig", array(
            'member' => $member,
            'edit' => false,
        ));
    }


    /**
     * Check if the email address was changed and update the mailing lists if necessary. Also check if the subscription
     * for the mailing lists have changed. If so, also update the  mailing lists.
     *
     * @param string|null              $originalEmail        Email address for it was eventually changed in the form.
     * @param Collection|MailingList[] $originalMailingLists List with original subscribed mailing lists.
     * @param MemberEntry              $memberEntry          Current member entry
     *
     * @throws Exception
     */
    private function updateMailingLists(?string $originalEmail, Collection $originalMailingLists, MemberEntry $memberEntry): void
    {
        $this->removeEmailSubscriptions($originalEmail, $originalMailingLists, $memberEntry);
        $this->addEmailSubscriptions($originalEmail, $originalMailingLists, $memberEntry);
    }


    /**
     * Checks all mailing list subscriptions and removes them if they were deselected.
     *
     * @param string|null $originalEmail
     * @param Collection  $originalMailingLists
     * @param MemberEntry $memberEntry
     *
     * @throws Exception Exceptions when logged in user is not set or the changed type is not found.
     *
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    private function removeEmailSubscriptions(?string $originalEmail, Collection $originalMailingLists, MemberEntry $memberEntry): void
    {
        $changedEmailAddress = $originalEmail !== $memberEntry->getEmail();

        // Remove subscription is necessary
        /** @var MailingList $mailingList */
        foreach ($originalMailingLists as $mailingList) {
            // When the mailing list subscription has been removed (it is not anymore on the current mailing lists list)
            // or the email address has been changed, then remove the address from the list.
            $existClosure = function ($key, $element) use ($mailingList) {

                return $element->getId() === $mailingList->getId();
            };

            // Only remove when the original email is not empty
            if (!is_null($originalEmail)) {
                // When the email address has to be changed or the list subscription has been removed
                if ($changedEmailAddress || (!$memberEntry->getMailingLists()->exists($existClosure))) {
                    try {
                        // Remove the email address from the actual external list server
                        $this->mailingListUpdater->removeEmailAddress($mailingList, $originalEmail);
                    } catch (\Exception $exception) {
                        $this->logMessageCreator->createLogEntry('error', $exception->getMessage());
                    }

                    $message = "Unsubscribed $originalEmail from mailing list ".$mailingList->getListName();
                    $this->logMessageCreator->createLogEntry('member settings change', $message, $memberEntry);
                }
            }
        }
    }


    /**
     * Checks all mailing list subscriptions and add them if they were deselected.
     *
     * @param string|null $originalEmail
     * @param Collection  $originalMailingLists
     * @param MemberEntry $memberEntry
     *
     * @throws Exception Exceptions when logged in user is not set or the changed type is not found.
     *
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    private function addEmailSubscriptions(?string $originalEmail, Collection $originalMailingLists, MemberEntry $memberEntry): void
    {
        $changedEmailAddress = $originalEmail !== $memberEntry->getEmail();

        /** @var MailingList $mailingList */
        foreach ($memberEntry->getMailingLists() as $mailingList) {
            // When the mailing list subscription has been added (it is not on the original mailing lists list)
            // or the email address has been changed, then add the address from the list.
            $existClosure = function ($key, $element) use ($mailingList) {

                return $element->getId() === $mailingList->getId();
            };

            // Only remove when the current email is not empty
            $currentEmail = $memberEntry->getEmail();
            if (!is_null($currentEmail)) {
                // When the email address has to be changed or the list subscription has been added
                if ($changedEmailAddress || (!$originalMailingLists->exists($existClosure))) {
                    try {
                        // Add the email address from the actual external list server
                        $this->mailingListUpdater->addEmailAddress($mailingList, $currentEmail);
                    } catch (\Exception $exception) {
                        $this->logMessageCreator->createLogEntry('error', $exception->getMessage());
                    }

                    $message = "Subscribed $currentEmail to mailing list ".$mailingList->getListName();
                    $this->logMessageCreator->createLogEntry('member settings change', $message, $memberEntry);
                }
            }
        }
    }
}
