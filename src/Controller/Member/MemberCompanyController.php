<?php

/*
 * Copyright (C) 2022 by Luc Hamers <vereniging@hamers.de>
 *
 * This file is part of Vereniging.
 *
 * Vereniging is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Vereniging is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Vereniging.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Controller\Member;

use App\Entity\CompanyInformation;
use App\Entity\MemberEntry;
use App\Form\MemberEntryCompanyType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Controller for editing company data.
 *
 */
class MemberCompanyController extends AbstractController
{
    /**
     * This method is called when the member entry has to be edited or when it is submitted after editing.
     *
     * @Route("/member/edit_company_data/{id}", name="member_edit_company_data")
     *
     * @param MemberEntry            $member        Member for which the company information is to be changed.
     * @param Request                $request
     * @param EntityManagerInterface $entityManager
     *
     * @return Response
     *
     */
    public function editAction(MemberEntry $member, Request $request, EntityManagerInterface $entityManager): Response
    {
        // When the user is only allowed to edit its own data and he tries to edit other users, throw an exception.
        if (!$this->isGranted('ROLE_EDIT_OTHER_USERS') && ($this->getUser()->getUsername() !== $member->getUsername())) {
            throw $this->createAccessDeniedException('You are not allowed to edit other users data!');
        }

        if (!$member->getCompanyInformation()) {
            $companyInformation = new CompanyInformation();
            $companyInformation->setName("");
            $companyInformation->setCity("");
            $member->setCompanyInformation($companyInformation);
        }

        $form = $this->createForm(MemberEntryCompanyType::class, $member->getCompanyInformation());

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // When either the company name of the company city is removed, remove the complete company entry
            if (!$form->getData()->getName() || !$form->getData()->getCity()) {
                $entityManager->remove($member->getCompanyInformation());
                $member->setCompanyInformation(null);
            } else {
               // Prevent code injections.
                $form->getData()->setName(htmlspecialchars($form->getData()->getName()));
                $form->getData()->setCity(htmlspecialchars($form->getData()->getCity()));
                $form->getData()->setDescription(htmlspecialchars($form->getData()->getDescription()));
                $form->getData()->setFunctionDescription(htmlspecialchars($form->getData()->getFunctionDescription()));
                $form->getData()->setUrl(htmlspecialchars($form->getData()->getUrl()));
            }

            $entityManager->flush();

            return $this->render("Member/company_data.html.twig", array(
                'member' => $member,
                'edit' => false,
            ));
        }

        return $this->render("Member/company_data.html.twig", array(
            'member' => $member,
            'edit' => true,
            'form' => $form->createView(),
        ));
    }


    /**
     * This method is called when the member entry is switched from editing to not-editing by pressing cancel.
     *
     * @Route("/member/cancel_company_data/{id}", name="member_cancel_company_data")
     *
     * @param MemberEntry $member Member for which the company information form is cancelled.
     *
     * @return Response
     */
    public function cancelAction(MemberEntry $member): Response
    {
        return $this->render("Member/company_data.html.twig", [
            'member' => $member,
            'edit' => false,
        ]);
    }
}
