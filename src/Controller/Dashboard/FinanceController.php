<?php

/*
 * Copyright (C) 2022 by Luc Hamers <vereniging@hamers.de>
 *
 * This file is part of Vereniging.
 *
 * Vereniging is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Vereniging is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Vereniging.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Controller\Dashboard;

use App\Entity\MemberEntry;
use App\Entity\MembershipFeeTransaction;
use App\Entity\MembershipNumber;
use App\Entity\SerialLetter;
use App\Entity\Status;
use App\Form\FormCollection;
use App\Form\FormCollectionType;
use App\Form\MembershipFeeTransactionType;
use App\Service\MemberListActionsForControllers;
use App\Service\MemberSelectionFormHandler;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security as SecurityAnnotation;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;

/**
 * Controller for showing the club structure.
 *
 * @SecurityAnnotation("is_granted('ROLE_MANAGE_FINANCES')")
 */
class FinanceController extends AbstractController
{
    private EntityManagerInterface $entityManager;
    private MemberSelectionFormHandler $selectionFormHandler;
    private MemberListActionsForControllers $memberListActions;
    private string $locale;


    /**
     * ClubStructureController constructor.
     *
     * @param EntityManagerInterface          $entityManager
     * @param MemberSelectionFormHandler      $formHandler
     * @param Security                        $security
     * @param MemberListActionsForControllers $memberListActions
     *
     * @throws Exception
     */
    public function __construct(EntityManagerInterface $entityManager, MemberSelectionFormHandler $formHandler, Security $security, MemberListActionsForControllers $memberListActions)
    {
        $this->entityManager = $entityManager;
        $this->selectionFormHandler = $formHandler;
        /** @var MemberEntry $currentUser */
        $currentUser = $security->getUser();
        $this->locale = $currentUser->getPreferredLanguage()->getLocale();
        $this->selectionFormHandler
            ->setOption('currentLocale', $this->locale)
            ->setOption('omitFields', ['committee', 'userRole'])
            ->setOption('statusWithMembershipNumberOnly', true);
        $this->memberListActions = $memberListActions;
    }


    /**
     * Display the club structure.
     *
     * @Route("/dashboard/manage_finances", name="dashboard_manage_finances")
     *
     * @return Response
     */
    public function indexAction(): Response
    {
        // Create the member selection form
        $form = $this->selectionFormHandler->generateForm(true);

        $members = $this->entityManager->getRepository(MemberEntry::class)->findByStatus('member');
        $letters = $this->entityManager->getRepository(SerialLetter::class)->findAll();
        $statuses = $this->entityManager->getRepository(Status::class)->findAll();
        $membershipNumbers = $this->entityManager->getRepository(MembershipNumber::class)->findBy(['dateRemoved' => null], ['membershipNumber' => 'ASC']);

        return $this->render('Dashboard/manage_finances.html.twig', [
            'selectionForm' => $form->createView(),
            'members' => $members,
            'serialLetters' => $letters,
            'statuses' => $statuses,
            'membershipNumbers' => $membershipNumbers,
        ]);
    }


    /**
     * Show member list depending on the filter selection.
     *
     * @Route("/dashboard/manage_finances/show", name="dashboard_show_finances")
     *
     * @SecurityAnnotation("is_granted('ROLE_SELECT_OTHER_USER_GROUPS')")
     *
     * @param Request $request
     *
     * @return Response
     */
    public function showAction(Request $request): Response
    {
        $this->selectionFormHandler->generateForm();
        /** @var MemberEntry[] $members */
        $members = $this->selectionFormHandler->handleFormRequest($request);
        $membershipNumbers = [];
        foreach ($members as $member) {
            // There could be a membership type applied to people who are not member, i.e. they do not have a membership
            // number (yet). These have to be omitted
            if ($member->getMembershipNumber()) {
                $membershipNumbers[$member->getMembershipNumber()->getMembershipNumber()] = $member->getMembershipNumber();
            }
        }
        ksort($membershipNumbers);

        $letters = $this->entityManager->getRepository(SerialLetter::class)->findAll();
        $statuses = $this->entityManager->getRepository(Status::class)->findAll();

        return $this->render('Dashboard/finances_table.html.twig', [
            'members' => $members,
            'serialLetters' => $letters,
            'statuses' => $statuses,
            'membershipNumbers' => $membershipNumbers,
        ]);
    }


    /**
     * With a selected number of members several actions can be done. The listAction distributes these requests to the
     * appropriate function.
     *
     * @Route("/dashboard/manage_finances/action", name="dashboard_manage_finances_actions" )
     *
     * @param Request $request

     * @return Response
     *
     * @throws Exception
     */
    public function listActions(Request $request): Response
    {
        // Read the selected status id in case we will change the members status
        $statusId = $request->query->get('status_select', null);
        // Read the serial letter in case this is to be generated for the selected members
        $serialLetterId = $request->query->get('letter_select', null);
        // Read the member ids to do the action for
        $membershipNumbers = $request->query->all('membershipNumber');

        $memberIds = $this->entityManager->getRepository(MemberEntry::class)->findMemberIdsByMembershipNumbers($membershipNumbers);

        switch ($request->query->get('action_select', null)) {
            case 'serial_letter':
                $this->denyAccessUnlessGranted('ROLE_CREATE_SERIAL_LETTERS', null, 'Unable to access this page!');

                return $this->memberListActions->createSerialLetterAction($memberIds, $serialLetterId);
            case 'change_status':
                $this->denyAccessUnlessGranted('ROLE_MANAGE_MEMBER_CLUB_SETTINGS', null, 'Unable to access this page!');

                return $this->memberListActions->changeMemberStatus($memberIds, $statusId);
            case 'generate_transactions':
                $this->denyAccessUnlessGranted('ROLE_MANAGE_FINANCES', null, 'Unable to access this page!');

                return $this->memberListActions->generateMembershipFeeTransactions($membershipNumbers);
            case 'transactions_paid':
                $this->denyAccessUnlessGranted('ROLE_MANAGE_FINANCES', null, 'Unable to access this page!');

                return $this->memberListActions->closeMembershipFeeTransactions($membershipNumbers);
            case 'set_use_direct_debit':
                $this->denyAccessUnlessGranted('ROLE_MANAGE_FINANCES', null, 'Unable to access this page!');

                return $this->memberListActions->setUseDirectDebit($membershipNumbers, true);
            case 'remove_use_direct_debit':
                $this->denyAccessUnlessGranted('ROLE_MANAGE_FINANCES', null, 'Unable to access this page!');

                return $this->memberListActions->setUseDirectDebit($membershipNumbers, false);
        }

        return new Response(null, 204);
    }


    /**
     * @param Request          $request
     * @param MembershipNumber $membershipNumber
     *
     * @Route("/dashboard/manage_finances/edit_transactions/{id}", name="dashboard_manage_finances_edit_transactions")
     *
     * @return Response
     */
    public function editTransactions(Request $request, MembershipNumber $membershipNumber): Response
    {
        $originalTransactions = $membershipNumber->getMembershipFeeTransactions()->toArray();

        // $originalTransactionData contains the data of the transactions before there are any changes done by the form
        // system. This is used for the generation of the log messages.
        $originalTransactionData = [];
        /** @var MembershipFeeTransaction $transaction */
        foreach ($originalTransactions as $key => $transaction) {
            $originalTransactionData[$key] = $transaction->toArray();
        }

        // Read the membership fee from any of the members in the current mebership list
        $memberEntries = $membershipNumber->getMemberEntries();
        if (count($memberEntries)) {
            $membershipFee = $memberEntries[0]->getMembershipType()->getMembershipFee();
        } else {
            $membershipFee = 0;
        }

        // Create the form, which is a form collection of transaction forms
        $listCollection = new FormCollection($originalTransactions, $this->entityManager);
        $form = $this->createForm(FormCollectionType::class, $listCollection, [
            'entry_class' => MembershipFeeTransactionType::class,
            // The following options are passed to the ValueFormType
            'entry_class_options' => [
                'membershipFee' => $membershipFee,
                ],
        ]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // New elements do not yet contain a membership number. Add it here. This can be done for each field, since
            // it is the same for all transactions.
            /** @var MembershipFeeTransaction $transaction */
            $processedTransactions = $form->getData()->getMembers();
            foreach ($processedTransactions as $transaction) {
                $transaction->setMembershipNumber($membershipNumber);
            }

            $this->generateTransactionLogMessages($processedTransactions->toArray(), $originalTransactionData);

            $this->entityManager->flush();

            // Redirect to main finance page
            return $this->redirect($this->generateUrl('dashboard_manage_finances'));
        }

        $userList = '';
        foreach ($membershipNumber->getMemberEntries() as $member) {
            $userList .= $member->getCompleteName().', ';
        }
        $userList = substr($userList, 0, -2);

        return $this->render('Dashboard/edit_membership_fee_transactions.html.twig', [
            'userList' => $userList,
            'membershipNumber' => $membershipNumber,
            'form' => $form->createView(),
        ]);
    }


    /**
     * Generate log messages for any changes to the transactions. There are 4 different possible changes:
     * - a new transaction is created
     * - a transaction is deleted
     * - a transaction is closed
     * - data values of the the transaction are changed
     *
     * @param array $processedTransactions
     * @param array $originalTransactionData
     */
    private function generateTransactionLogMessages(array $processedTransactions, array $originalTransactionData): void
    {
        try {
            // Create an array with the data of the processed transactions. This is needed to compare with the original
            // data.
            $processedTransactionData = [];
            /** @var MembershipFeeTransaction $transaction */
            foreach ($processedTransactions as $key => $transaction) {
                $processedTransactionData[$key] = $transaction->toArray();
            }

            // Generate log messages for deleted entries
            /** @var MembershipFeeTransaction $deleted */
            foreach (array_diff_key($originalTransactionData, $processedTransactionData) as $deleted) {
                $this->memberListActions->generateMembershipFeeTransactionLogs($deleted, null);
            }
            // Generate log messages for newly created entries
            /** @var MembershipFeeTransaction $created */
            foreach (array_diff_key($processedTransactionData, $originalTransactionData) as $created) {
                $this->memberListActions->generateMembershipFeeTransactionLogs(null, $created);
            }
            // Generate log messages for changed entries (when they are not changed, no message is generate)
            /** @var MembershipFeeTransaction $changed */
            foreach (array_intersect_key($originalTransactionData, $processedTransactionData) as $key => $changed) {
                $this->memberListActions->generateMembershipFeeTransactionLogs(
                    $originalTransactionData[$key],
                    $processedTransactionData[$key]
                );
            }
        } catch (Exception $e) {
            // This is left intentionally blank. Exceptions are only thrown when the change type does not exist or when
            // no user is logged in. Both errors can not happen here.
        }
    }
}
