<?php

/*
 * Copyright (C) 2022 by Luc Hamers <vereniging@hamers.de>
 *
 * This file is part of Vereniging.
 *
 * Vereniging is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Vereniging is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Vereniging.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Controller\Dashboard;

use App\Entity\Committee;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Controller for showing the club structure.
 *
 * @Security("is_granted('ROLE_VIEW_CLUB_STRUCTURE')")
 */
class ClubStructureController extends AbstractController
{
    private bool $useAcademicTitles;


    /**
     * ClubStructureController constructor.
     *
     * @param string $useAcademicTitles
     */
    public function __construct(string $useAcademicTitles)
    {
        $this->useAcademicTitles = ($useAcademicTitles === 'true');
    }


    /**
     * Display the club structure.
     *
     * @Route("/dashboard/clubstructure", name="dashboard_club_structure")
     *
     * @param EntityManagerInterface $entityManager
     *
     * @return Response
     */
    public function indexAction(EntityManagerInterface $entityManager): Response
    {
        $committees = $entityManager->getRepository(Committee::class)
                                    ->getAllCommitteeInformation($this->useAcademicTitles);

        return $this->render('Dashboard/club_structure.html.twig', ['committees' => $committees]);
    }
}
