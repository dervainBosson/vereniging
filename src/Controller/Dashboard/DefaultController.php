<?php

/*
 * Copyright (C) 2022 by Luc Hamers <vereniging@hamers.de>
 *
 * This file is part of Vereniging.
 *
 * Vereniging is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Vereniging is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Vereniging.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Controller\Dashboard;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Controller for the dashboard.
 *
 */
class DefaultController extends AbstractController
{
    /**
     * Show dashboard page.
     *
     * @Route("/dashboard", name="dashboard_homepage")
     *
     * @return Response
     */
    public function indexAction(): Response
    {
        return $this->render('Dashboard/index.html.twig');
    }

    /**
     * Show about this software page.
     *
     * @Route("/dashboard/about", name="dashboard_about")
     *
     * @return Response
     */
    public function aboutAction(): Response
    {
        return $this->render('Dashboard/about.html.twig');
    }
}
