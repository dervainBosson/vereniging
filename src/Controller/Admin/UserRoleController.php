<?php

/*
 * Copyright (C) 2022 by Luc Hamers <vereniging@hamers.de>
 *
 * This file is part of Vereniging.
 *
 * Vereniging is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Vereniging is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Vereniging.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Controller\Admin;

use App\Entity\MemberEntry;
use App\Entity\UserRole;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class UserRightsController
 *
 * @Security("is_granted('ROLE_MANAGE_USER_ROLES')")
 */
class UserRoleController extends AbstractController
{
    /**
     * Show the user roles with their user rights.
     *
     * @Route("/administration/manage_user_roles", name="administration_manage_user_roles")
     *
     * @param EntityManagerInterface $entityManager
     *
     * @return Response
     */
    public function indexAction(EntityManagerInterface $entityManager): Response
    {
        /** @var MemberEntry $user */
        $user = $this->getUser();
        $locale = $user->getPreferredLanguage()->getLocale();
        $userRoles = $entityManager->getRepository(UserRole::class)->findAllOrderedByRoleCount($locale);

        // Make a list of roles as they are defined for the roles in the order of the roles
        $allRoles = [];
        foreach ($userRoles as $userRole) {
            $allRoles = array_unique(array_merge($allRoles, $userRole->getSystemRoles()));
        }

        return $this->render('Admin/user_roles.html.twig', [
            'userRoles' => $userRoles,
            'allRoles'  => $allRoles,
        ]);
    }
}
