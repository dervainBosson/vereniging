<?php

/*
 * Copyright (C) 2022 by Luc Hamers <vereniging@hamers.de>
 *
 * This file is part of Vereniging.
 *
 * Vereniging is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Vereniging is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Vereniging.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Controller\Admin;

use App\Entity\ChangeType;
use App\Entity\MemberEntry;
use App\Form\EmailNotificationsCollection;
use App\Form\EmailNotificationsCollectionType;
use App\Service\LogMessageCreator;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * Class EmailNotificationController
 *
 * @Security("is_granted('ROLE_MANAGE_EMAIL_NOTIFICATIONS')")
 */
class EmailNotificationController extends AbstractController
{
    private EntityManagerInterface $entityManager;
    private LogMessageCreator $logMessageCreator;
    private TranslatorInterface $translator;


    /**
     * EmailNotificationController constructor.
     *
     * @param EntityManagerInterface $entityManager
     * @param LogMessageCreator      $logMessageCreator
     * @param TranslatorInterface    $translator
     */
    public function __construct(EntityManagerInterface $entityManager, LogMessageCreator $logMessageCreator, TranslatorInterface $translator)
    {
        $this->entityManager = $entityManager;
        $this->logMessageCreator = $logMessageCreator;
        $this->translator = $translator;
    }


    /**
     * Show the users list with their email notifications settings
     *
     * @Route("/administration/manage_email_notifications/{showOnlyContentTable}",
     *              defaults={"showOnlyContentTable"=false},
     *              name="administration_manage_email_notifications")
     *
     * @param bool $showOnlyContentTable When true, the cancel button is used to call this action. The controller then
     *                                   has to select the content template instead of the complete-page template.
     *
     * @return Response
     */
    public function indexAction(bool $showOnlyContentTable): Response
    {
        $changeTypes = $this->entityManager->getRepository(ChangeType::class)->findby([], ['changeType' => 'ASC']);
        /** @var MemberEntry[] $memberEntries */
        $memberEntries = $this->entityManager->getRepository(MemberEntry::class)
                                             ->createFindAllWithEmailNotificationsQueryBuilder()
                                             ->getQuery()
                                             ->execute();
        $notificationsCollection = new EmailNotificationsCollection($memberEntries);
        $form = $this->createForm(EmailNotificationsCollectionType::class, $notificationsCollection);

        if ($showOnlyContentTable) {
            // select content template
            $template = 'Admin/email_notifications_content.html.twig';
        } else {
            // Select complete page template
            $template = 'Admin/email_notifications.html.twig';
        }

        return $this->render($template, [
            'form' => $form->createView(),
            'changeTypes' => $changeTypes,
            'edit' => false,
        ]);
    }


    /**
     * Show the email notifications edit page. This page handles the results of a form, which contains the email
     * notification settings for each selected member. Handling added members has to be handled somewhat differently
     * opposed to the usual handling of collections where members can be added. In the example on the sympony page
     * (http://symfony.com/doc/current/form/form_collections.html) new objects are added to the collection. This doesn't
     * work here, since existing members have to be added. Because of this, the list of members passed by the form
     * additionally contains the members ids, including added members. To process this correctly, the members passed to
     * the form processing are therefor taken from the post data, and added to the members already selected.
     *
     * @Route("/administration/manage_email_notifications_edit", name="administration_manage_email_notifications_edit")
     *
     * @param Request $request
     *
     * @return Response
     *
     * @throws \Exception
     */
    public function editAction(Request $request): Response
    {
        $idList = [];

        // When post data is received, use the passed members to create a list of member ids, including those of newly
        // added members.
        if ('POST' === $request->getMethod()) {
            if (array_key_exists('memberEntries', $request->request->all('email_notifications_collection'))) {
                $formMemberEntries = $request->request->all('email_notifications_collection')['memberEntries'];
                // The memberEntries array contains the ids and the email notifications. Only the ids are used here. Read
                // the ids to create an id list.
                foreach ($formMemberEntries as $entry) {
                    $idList[] = $entry['id'];
                }
            }
        }

        // When creating the form for the first time (without before posting), select all members for which already
        // email notifications have been defined.
        /** @var MemberEntry[] $memberEntries */
        $memberEntries = $this->entityManager->getRepository(MemberEntry::class)
                                             ->createFindAllWithEmailNotificationsQueryBuilder($idList)
                                             ->getQuery()
                                             ->execute();

        $previousNotificationSettings = $this->extractEmailNotificationSettings($memberEntries);

        $notificationsCollection = new EmailNotificationsCollection($memberEntries);
        $form = $this->createForm(EmailNotificationsCollectionType::class, $notificationsCollection);

        $form->handleRequest($request);

        $completeMemberList = $this->entityManager->getRepository(MemberEntry::class)->findByCriteria(['exclude' => $memberEntries]);

        $edit = true;
        if ($form->isSubmitted()) {
            $this->entityManager->flush();
            $currentNotificationSettings = $this->extractEmailNotificationSettings($memberEntries);
            $this->generateLogMessages($previousNotificationSettings, $currentNotificationSettings);
            $edit = false;
            // Read the form values from the database to update the form lines in case an empty member has been removed.
            $memberEntries = $this->entityManager->getRepository(MemberEntry::class)
                                                 ->createFindAllWithEmailNotificationsQueryBuilder()
                                                 ->getQuery()
                                                 ->execute();
             $notificationsCollection = new EmailNotificationsCollection($memberEntries);
            $form = $this->createForm(EmailNotificationsCollectionType::class, $notificationsCollection);
        }
        $changeTypes = $this->entityManager->getRepository(ChangeType::class)->findby([], ['changeType' => 'ASC']);

        return $this->render('Admin/email_notifications_content.html.twig', [
            'form' => $form->createView(),
            'changeTypes' => $changeTypes,
            'edit' => $edit,
            'completeMemberList' => $completeMemberList,
        ]);
    }


    /**
     * Write the email notification settings for a list of members to an array. Here the member entry object is the key,
     * an array of change type ids is the content.
     *
     * @param MemberEntry[] $memberEntries
     *
     * @return array
     */
    private function extractEmailNotificationSettings(array $memberEntries): array
    {
        $notificationSettings = [];
        foreach ($memberEntries as $memberEntry) {
            $emailNotifications = [];
            foreach ($memberEntry->getEmailNotifications() as $notification) {
                $emailNotifications[] = $notification->getId();
            }
            $notificationSettings[$memberEntry->getId()]['notifications'] = $emailNotifications;
            $notificationSettings[$memberEntry->getId()]['member'] = $memberEntry;
        }

        return $notificationSettings;
    }

    /**
     * @param array $previousNotificationSettings
     * @param array $currentNotificationSettings
     *
     * @throws Exception
     */
    private function generateLogMessages(array $previousNotificationSettings, array $currentNotificationSettings): void
    {
        foreach ($previousNotificationSettings as $memberId => $notificationSettings) {
            // Removed subscriptions
            $removed = array_diff($notificationSettings['notifications'], $currentNotificationSettings[$memberId]['notifications']);
            $message = ': ';
            foreach ($removed as $changeTypeId) {
                $message .= '"'.$this->entityManager->getRepository(ChangeType::class)->find($changeTypeId).'" ';
            }
            if (': ' !== $message) {
                $message = $this->translator->trans("Removed email notifications").$message;
                $this->logMessageCreator->createLogEntry('member settings change', $message, $notificationSettings['member']);
            }
            $added = array_diff($currentNotificationSettings[$memberId]['notifications'], $notificationSettings['notifications']);
            $message = ': ';
            foreach ($added as $changeTypeId) {
                $message .= '"'.$this->entityManager->getRepository(ChangeType::class)->find($changeTypeId).'" ';
            }
            if (': ' !== $message) {
                $message = $this->translator->trans("Added email notifications").$message;
                $this->logMessageCreator->createLogEntry('member settings change', $message, $notificationSettings['member']);
            }
        }
        $this->entityManager->flush();
    }
}
