<?php

/*
 * Copyright (C) 2022 by Luc Hamers <vereniging@hamers.de>
 *
 * This file is part of Vereniging.
 *
 * Vereniging is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Vereniging is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Vereniging.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Controller\Admin;

use App\Entity\Language;
use App\Entity\EmailTemplate;
use App\Form\EmailTemplateType;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * Controller for editing lists of entities like titles or languages.
 *
 * @Security("is_granted('ROLE_MANAGE_EMAIL_TEMPLATES')")
 */
class EmailTemplateController extends AbstractController
{
    private EntityManagerInterface $entityManager;
    private TranslatorInterface $translator;
    private string $defaultLocale;


    /**
     * EmailTemplateController constructor.
     * @param EntityManagerInterface $entityManager
     * @param TranslatorInterface    $translator
     * @param string                 $defaultLocale
     */
    public function __construct(EntityManagerInterface $entityManager, TranslatorInterface $translator, string $defaultLocale)
    {

        $this->entityManager = $entityManager;
        $this->translator = $translator;
        $this->defaultLocale = $defaultLocale;
    }


    /**
     * @Route("/administration/manage_email_templates", name="administration_manage_email_templates")
     *
     * @return Response
     */
    public function indexAction(): Response
    {
        $templates = $this->entityManager->getRepository(EmailTemplate::class)->findAll();
        $locales = $this->entityManager->getRepository(Language::class)->findAllLocales($this->defaultLocale);

        return $this->render('Admin/email_templates.html.twig', [
            'edit' => false,
            'locales' => $locales,
            'templates' => $templates,
        ]);
    }


    /**
     * @param int $id
     *
     * @Route("/administration/manage_email_templates/delete//{id}", name="administration_manage_email_templates_delete")
     *
     * @return Response
     */
    public function deleteAction(int $id): Response
    {
        // Because of an unknown interaction between javascript and routes, it could happen that after an empty template
        // is saved, the delete route was called with an old id. The caused a ParamConverter exception, because the id
        // was not found. As a workaround, we convert the id to a template by hand here.
        $template = $this->entityManager->getRepository(EmailTemplate::class)->find($id);
        if ($template) {
            $this->entityManager->remove($template);
            $this->entityManager->flush();
        }

        return $this->indexAction();
    }


    /**
     * @param int|null $id
     * @param Request  $request
     *
     * @return Response
     *
     * @Route("/administration/manage_email_templates/edit/{id}", name="administration_manage_email_templates_edit", defaults={"id"=null})
     */
    public function editAction(?int $id, Request $request): Response
    {
        if (is_null($id)) {
            $template = new EmailTemplate();
        } else {
            $template = $this->entityManager->getRepository(EmailTemplate::class)->find($id);
        }

        $template->setContent($this->forwardTranslationOfVariables($template->getContent()));
        $locales = $this->entityManager->getRepository(Language::class)->findAllLocales($this->defaultLocale);

        // Template name has to used from the original name in the database, because the name could be
        $templateName = $template->getTemplateName();
        $templateForm = $this->createForm(EmailTemplateType::class, null, [
            'template' => $template,
            'locales' => $locales,
        ]);

        $templateForm->handleRequest($request);
        if ($templateForm->isSubmitted() && $templateForm->isValid()) {
            $formData = $templateForm->getData();
            // Reverse translate the content variable, if we are in a different locale then the default
            $content = $this->reverseTranslationOfVariables($formData['templateContent'] ?? '');
            // Ensure that the content variable is present. If not, add it to the end of the template.
            if (false === strpos($content, EmailTemplate::HTML_CONTENT_VARIABLE)) {
                $content .= EmailTemplate::HTML_CONTENT_VARIABLE;
            }
            $template->setContent($content);

            $anyNameSet = $this->readTranslatedTemplateNames($formData, $locales, $template);
            $this->readTranslatedSenderAddresses($formData, $locales, $template);

            // Only save if at least one of the translation values has been set
            if ($anyNameSet) {
                $this->entityManager->persist($template);
                $this->entityManager->flush();
            }

            return new Response('OK');
        }

        return $this->render('Admin/email_template_edit.html.twig', [
            'form' => $templateForm->createView(),
            'templateName' => $templateName,
            'id' => $template->getId(),
            'locales' => $locales,
            'content_variable' => $this->forwardTranslationOfVariables(EmailTemplate::HTML_CONTENT_VARIABLE),
        ]);
    }


    /**
     * Each email template contains a variable (or several) where the email content will be inserted. This variable is
     * translated into the locale before it is shown on the page by this method. Before translation, it looks like this:
     * <span class="mceNonEditable" style="border: thin dotted grey;" data-variable="email content">email content</span>
     * which is translated to (e.g. into German):
     * <span class="mceNonEditable" style="border: thin dotted grey;" data-variable="email content">E-Mail-Inhalt</span>
     *
     * @param string $content
     *
     * @return string
     */
    private function forwardTranslationOfVariables(string $content): string
    {
        $contentVariable = EmailTemplate::CONTENT_VARIABLE;

         return str_replace(">$contentVariable</span>", '>'.$this->translator->trans($contentVariable).'</span>', $content);
    }


    /**
     * Each email template contains a variable (or several) where the email content will be inserted. This variable is
     * translated into the locale before it is shown on the page. Before storing it to the database, this method will
     * remove the translated value to the original one. After translation, it looks like this:
     * <span class="mceNonEditable" style="border: thin dotted grey;" data-variable="email content">E-Mail-Inhalt</span>
     * which is reverse-translated to (e.g. from German):
     * <span class="mceNonEditable" style="border: thin dotted grey;" data-variable="email content">email content</span>
     *
     * @param string $content
     *
     * @return string
     */
    private function reverseTranslationOfVariables(string $content): string
    {
        $contentVariable = EmailTemplate::CONTENT_VARIABLE;
        preg_match_all("#.*<span class=\"mceNonEditable\".+data-variable=\"$contentVariable\".*>(.+)</span>#U", $content, $matches);

        for ($i = 0; $i < count($matches[0]); $i++) {
            $translatedName = $matches[1][$i];
            $content = str_replace(">$translatedName</span>", ">$contentVariable</span>", $content);
        }

        return $content;
    }


    /**
     * @param array         $formData
     * @param array         $locales
     * @param EmailTemplate $template
     *
     * @return bool True when at least one of the template name fields contain something and the values can be saved to
     *              the database
     */
    private function readTranslatedTemplateNames(array $formData, array $locales, EmailTemplate $template): bool
    {
        $anyNameSet = false;

        // The first locale is the default one, which is written directly to the entity, but only if it is not null
        if ($formData['templateName_'.$locales[0]]) {
            $template->setTemplateName($formData['templateName_'.$locales[0]]);
            $template->addNewTranslation($locales[0], $formData['templateName_'.$locales[0]]);
            $anyNameSet = true;
        }
        // Read the translation fields and add translation, if the fields contain data
        $localesExceptDefault = array_slice($locales, 1);
        foreach ($localesExceptDefault as $locale) {
            $translation = $formData["templateName_$locale"];
            if ($translation) {
                $template->addNewTranslation($locale, $translation);
                $anyNameSet = true;
            }
        }

        return $anyNameSet;
    }


    /**
     * @param array         $formData
     * @param array         $locales
     * @param EmailTemplate $template
     */
    private function readTranslatedSenderAddresses(array $formData, array $locales, EmailTemplate $template): void
    {
        // The first locale is the default one, which is written directly to the entity, but only if it is not null
        if ($formData['senderAddress_'.$locales[0]]) {
            $template->setSenderAddress($formData['senderAddress_'.$locales[0]]);
            $template->addNewTSenderAddressTranslation($locales[0], $formData['senderAddress_'.$locales[0]]);
        }

        // Read the translation fields and add translation, if the fields contain data
        $localesExceptDefault = array_slice($locales, 1);
        foreach ($localesExceptDefault as $locale) {
            $translation = $formData["senderAddress_$locale"];
            if ($translation) {
                $template->addNewTSenderAddressTranslation($locale, $translation);
            }
        }
    }
}
