<?php

/*
 * Copyright (C) 2022 by Luc Hamers <vereniging@hamers.de>
 *
 * This file is part of Vereniging.
 *
 * Vereniging is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Vereniging is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Vereniging.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Controller\Admin;

use App\Form\FileUploadType;
use App\Service\ImportExcelDataFile;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class ImportFromExcelController
 *
 * @Security("is_granted('ROLE_IMPORT_EXCEL')")
 */
class ImportFromExcelController extends AbstractController
{
    private ImportExcelDataFile $excelImporter;
    private string $tempDirectory;


    /**
     * ImportFromExcelController constructor.
     *
     * @param ImportExcelDataFile $excelImporter
     * @param string              $tempDirectory
     */
    public function __construct(ImportExcelDataFile $excelImporter, string $tempDirectory)
    {
        $this->tempDirectory = $tempDirectory;
        $this->excelImporter = $excelImporter;
    }


    /**
     * Select data file to be imported.
     *
     * @Route("/administration/import_excel",
     *              name="administration_import_from_excel")
     *
     * @return Response
     */
    public function indexAction(): Response
    {
        $form = $this->createForm(FileUploadType::class);

        return $this->render('Admin/import_from_excel.html.twig', [
            'form' => $form->createView(),
            'logMessage' => '',
            'importError' => false,
            'resultNumbers' => [],
            'resultValues' => [],
            'fileName' => '',
        ]);
    }


    /**
     * Check if the passed excel file meets the requirements.
     *
     * @Route("/administration/import_excel_check", name="administration_import_from_excel_check")
     *
     * @param Request $request
     *
     * @return Response
     */
    public function checkAction(Request $request): Response
    {
        // Remove old uploaded files
        foreach (glob("$this->tempDirectory/vereniging_import_file.*.xlsx") as $filename) {
            // It can happen, that there are file here which have a different owner (e.g. when jenkins also stores its
            // files here). @ prevents any error message in that case.
            @unlink($filename);
        }

        $form = $this->createForm(FileUploadType::class);

        $form->handleRequest($request);

        $logMessage = '';
        $importError = false;
        $resultNumbers = [];
        $resultValues = [];
        $hashName = '';
        $originalFileName = '';
        if ($form->isSubmitted() && $form->isValid()) {
            /** @var UploadedFile $file */
            $file = $request->files->get('file_upload')['file'];

            $hashName = md5(uniqid()).'.'.$file->getClientOriginalExtension();
            // The internal file name is the actual name of the file in disk. This is to prevent passing random file
            // names to the import, which could be a security risk.
            $internalFileName = "vereniging_import_file.$hashName";
            try {
                $file->move($this->tempDirectory, $internalFileName);
                $originalFileName = $file->getClientOriginalName();
                $this->excelImporter->checkExcelFileForImport("$this->tempDirectory/$internalFileName", $originalFileName);
                $resultNumbers = $this->excelImporter->getNumbers();
                $resultValues = $this->excelImporter->getSelectValues();
            } catch (\Exception $e) {
                $logMessage = $e->getMessage();
                if (strpbrk($logMessage, 'partially uploaded') !== false) {
                    $logMessage .= " Please reload page and try again.";
                }
                $importError = true;
                if (file_exists("$this->tempDirectory/$internalFileName")) {
                    unlink("$this->tempDirectory/$internalFileName");
                }
            }
        }

        return $this->render('Admin/import_from_excel_result.html.twig', [
            'logMessage' => $logMessage,
            'importError' => $importError,
            'resultNumbers' => $resultNumbers,
            'resultValues' => $resultValues,
            'hashName' => $hashName,
            'originalFileName' => $originalFileName,
        ]);
    }


    /**
     * Called after the file is uploaded and checked. This then really imports the file into the database.
     *
     * @Route("/administration/import_excel/{hashName}/{originalFileName}", name="administration_import_from_excel_import")
     *
     * @param string $hashName         md5-hash plus extension of the uploaded file name created at the upload.
     * @param string $originalFileName Original file name
     *
     * @return Response
     */
    public function importAction(string $hashName, string $originalFileName): Response
    {
        // The internal file name is the actual name of the file in disk. This is to prevent passing random file
        // names to the import, which could be a security risk.
        $internalFileName = "vereniging_import_file.$hashName";

        $logMessage = '';
        $importError = false;
        $resultNumbers = [];
        $resultValues = [];
        $hashName = '';

        try {
            $this->excelImporter->importExcelFile("$this->tempDirectory/$internalFileName", $originalFileName);
            $resultNumbers = $this->excelImporter->getNumbers();
            $resultValues = $this->excelImporter->getSelectValues();
        } catch (\Exception $e) {
            $logMessage = $e->getMessage();
            $importError = true;
        }
        unlink("$this->tempDirectory/$internalFileName");

        return $this->render('Admin/import_from_excel_finished.html.twig', [
            'logMessage' => $logMessage,
            'importError' => $importError,
            'resultNumbers' => $resultNumbers,
            'resultValues' => $resultValues,
            'hashName' => $hashName,
        ]);
    }
}
