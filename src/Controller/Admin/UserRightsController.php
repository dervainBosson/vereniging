<?php

/*
 * Copyright (C) 2022 by Luc Hamers <vereniging@hamers.de>
 *
 * This file is part of Vereniging.
 *
 * Vereniging is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Vereniging is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Vereniging.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Controller\Admin;

use App\Entity\MemberEntry;
use App\Form\MemberEntryUserRoleType;
use App\Service\MemberSelectionFormHandler;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security as SecurityAnnotation;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;

/**
 * Class UserRightsController
 *
 * @SecurityAnnotation("is_granted('ROLE_MANAGE_USER_RIGHTS')")
 */
class UserRightsController extends AbstractController
{
    private EntityManagerInterface $entityManager;
    private MemberSelectionFormHandler $selectionFormHandler;


    /**
     * UserRightsController constructor.
     *
     * @param EntityManagerInterface     $entityManager
     * @param MemberSelectionFormHandler $selectionFormHandler
     * @param Security                   $security
     *
     * @throws Exception
     */
    public function __construct(EntityManagerInterface $entityManager, MemberSelectionFormHandler $selectionFormHandler, Security $security)
    {

        $this->entityManager = $entityManager;
        $this->selectionFormHandler = $selectionFormHandler;
        /** @var MemberEntry $currentUser */
        $currentUser = $security->getUser();
        $this->selectionFormHandler->setOption('currentLocale', $currentUser->getPreferredLanguage()->getLocale());
        $this->selectionFormHandler->setOption('omitFields', ['debt']);
    }


    /**
     * Show the users list their user rights.
     *
     * @Route("/administration/manage_user_rights", name="administration_manage_user_rights")
     *
     * @return Response
     *
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function indexAction(): Response
    {
        $selectionForm = $this->selectionFormHandler->generateForm(true);

        // Load all members with the status "member", which are shown in the default view.
        $memberEntries = $this->entityManager->getRepository(MemberEntry::class)->findByStatus('member');
        $memberForms = $this->generateMemberForms($memberEntries);

        return $this->render('Admin/user_rights.html.twig', array(
            'selectionForm' => $selectionForm->createView(),
            'memberForms' => $memberForms,
        ));
    }


    /**
     * This method is called via an ajax call whenever the user changes the member selection fields. It then returns a
     * new list of member forms.
     *
     * @Route("/administration/manage_user_rights/show", name="administration_manage_user_rights_show")
     *
     * @param Request $request
     *
     * @return Response
     *
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function showAction(Request $request): Response
    {
        $this->selectionFormHandler->generateForm(false);
        $memberEntries = $this->selectionFormHandler->handleFormRequest($request);
        $memberForms = $this->generateMemberForms($memberEntries);

        return $this->render('Admin/user_rights_member_table.html.twig', array(
            'memberForms' => $memberForms,
        ));
    }


    /**
     * This method is called when the user role drop-down of a member changes. It then saves the object to the database.
     *
     * @Route("/administration/manage_user_rights/update", name="administration_manage_user_rights_update")
     *
     * @param Request $request
     *
     * @return Response
     */
    public function updateAction(Request $request): Response
    {
        // First find out which form sent the post data
        $postData = $request->request->all();
        $memberId = substr(array_keys($postData)[0], strlen('member_form_'));

        // Read the member entry object which belongs to this id.
        $member = $this->entityManager->getRepository(MemberEntry::class)->find($memberId);

        $form = $this->createForm(MemberEntryUserRoleType::class, $member);

        // Change the request value, since here the array index of the post data contains the id of the member.
        $postData = ['member_entry_user_role' => $postData['member_form_'.$memberId]];
        $request->request->replace($postData);

        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            $this->entityManager->flush();
        }

        return new Response(null, 204);
    }


    /**
     * This method generates an array of form views. Each element is a user role selection dropdown for a single member
     * entry. To keep the forms apart, each form is assigned a name which contains the id of the member entry for which
     * the form is generated.
     *
     * @param MemberEntry[] $memberEntries
     *
     * @return array
     *
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    private function generateMemberForms(array $memberEntries): array
    {
        $memberForms = array();
        foreach ($memberEntries as $memberEntry) {
            $memberForms[] = $this->container
                ->get('form.factory')
                ->createNamedBuilder(
                    'member_form_'.$memberEntry->getId(),
                    MemberEntryUserRoleType::class,
                    $memberEntry
                )
                ->getForm()
                ->createView();
        }

        return $memberForms;
    }
}
