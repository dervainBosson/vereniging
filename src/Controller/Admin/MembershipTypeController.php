<?php

/*
 * Copyright (C) 2022 by Luc Hamers <vereniging@hamers.de>
 *
 * This file is part of Vereniging.
 *
 * Vereniging is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Vereniging is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Vereniging.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Controller\Admin;

use App\Entity\Language;
use App\Entity\MembershipType;
use App\Form\FormCollection;
use App\Form\FormCollectionType;
use App\Form\MembershipTypeType;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class MembershipTypeController
 * @Security("is_granted('ROLE_MANAGE_MEMBERSHIP_TYPES')")
 */
class MembershipTypeController extends AbstractController
{
    private EntityManagerInterface $entityManager;
    private string $defaultLocale;


    /**
     * MembershipTypeController constructor.
     *
     * @param EntityManagerInterface $entityManager
     * @param string                 $defaultLocale
     */
    public function __construct(EntityManagerInterface $entityManager, string $defaultLocale)
    {

        $this->entityManager = $entityManager;
        $this->defaultLocale = $defaultLocale;
    }


    /**
     * @Route("/administration/manage_membership_types/{showOnlyContentTable}",
     *              defaults={"showOnlyContentTable"=false},
     *              name="administration_manage_membership_types")
     *
     * @param bool $showOnlyContentTable When true, the cancel button is used to call this action. The controller then
     *                                   has to select the content template instead of the complete-page template.
     *
     * @return Response
     */
    public function indexAction(bool $showOnlyContentTable): Response
    {
        if ($showOnlyContentTable) {
            // select content template
            $template = 'Admin/membership_type_content.html.twig';
        } else {
            // Select complete page template
            $template = 'Admin/membership_type.html.twig';
        }

        $membershipTypes = $this->entityManager->getRepository(MembershipType::class)->findAllOrderedWithLocale($this->defaultLocale);
        $locales = $this->entityManager->getRepository(Language::class)->findAllLocales($this->defaultLocale);

        return $this->render($template, [
            'form'            => $this->createMembershipTypeForm()->createView(),
            'edit'            => false,
            'membershipTypes' => $membershipTypes,
            'locales'  => $locales,
        ]);
    }


    /**
     * @Route("/administration/manage_membership_types_edit", name="administration_manage_membership_types_edit")
     *
     * @param Request $request
     *
     * @return Response
     */
    public function editAction(Request $request): Response
    {
        $form = $this->createMembershipTypeForm();
        $form->handleRequest($request);

        $locales = $this->entityManager->getRepository(Language::class)->findAllLocales($this->defaultLocale);

        $edit = true;
        if ($form->isSubmitted() && $form->isValid()) {
            // Check for mandatory fields which have been cleared by the user. If so, refresh to the old value for the
            // type name or set the value to 0 for the membership fee.
            foreach ($form['members'] as $subForm) {
                /** @var MembershipType $type */
                $type = $subForm->getData();

                $this->fixEmptyData($type);

                try {
                    // Read the translation fields and add translation, if the fields contain data
                    $localesExceptDefault = array_slice($locales, 1);
                    foreach ($localesExceptDefault as $locale) {
                        $translation = $subForm->get("type_$locale")->getData();
                        if ($translation) {
                            $type->addNewTranslation($locale, $translation);
                        }
                    }
                } catch (Exception $exception) {
                    // Left intentionally blank. The $subForm->get could trow an exception, when there are no
                    // translation fields. If so, just do nothing
                }
            }
            $this->entityManager->flush();
            $edit = false;
            // Read the form values from the database to update the form lines in case an empty member has been removed.
            $form = $this->createMembershipTypeForm();
        }

        return $this->render('Admin/membership_type_content.html.twig', [
            'form'    => $form->createView(),
            'edit'    => $edit,
            'locales' => $locales,
        ]);
    }


    /**
     * @return FormInterface
     */
    private function createMembershipTypeForm(): FormInterface
    {
        $membershipTypes = $this->entityManager->getRepository(MembershipType::class)->findAllOrderedWithLocale($this->defaultLocale);
        $locales = $this->entityManager->getRepository(Language::class)->findAllLocales($this->defaultLocale);

        $types = new FormCollection($membershipTypes, $this->entityManager);
        $form = $this->createForm(FormCollectionType::class, $types, [
            'entry_class' => MembershipTypeType::class,
            // The following options are passed to the ValueFormType
            'entry_class_options' => [
                'locales' => $locales,
            ],
        ]);

        return $form;
    }


    /**
     * In the form, the membership type string could have been cleared, as well as the membership fee. This is fixed
     * here:
     * membership type string is reset to the old value.
     * membership fee is set to 0 (instead of null, which violates the database constraints for this field)
     *
     * @param MembershipType $type
     */
    private function fixEmptyData(MembershipType $type): void
    {
        // When the type is empty and this entity is not new, refresh it (discard it otherwise)
        if ((is_null($type->getTypeKey())) && (!is_null($type->getId()))) {
            $this->entityManager->refresh($type);
        }
        if (is_null($type->getMembershipFee())) {
            $type->setMembershipFee(0);
        }
    }
}
