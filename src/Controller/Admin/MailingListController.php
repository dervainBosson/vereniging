<?php

/*
 * Copyright (C) 2022 by Luc Hamers <vereniging@hamers.de>
 *
 * This file is part of Vereniging.
 *
 * Vereniging is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Vereniging is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Vereniging.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Controller\Admin;

use App\Entity\MailingList;
use App\Form\FormCollection;
use App\Form\FormCollectionType;
use App\Form\MailingListSubscriberType;
use App\Form\MailingListType;
use App\Service\LogMessageCreator;
use App\Service\MailingListUpdateInterface;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class MailingListController
 *
 * @Security("is_granted('ROLE_MANAGE_MAILINGLISTS')")
 */
class MailingListController extends AbstractController
{
    private EntityManagerInterface $entityManager;
    private LogMessageCreator $logMessageCreator;
    private MailingListUpdateInterface $mailingListUpdater;


    /**
     * MailingListController constructor.
     *
     * @param EntityManagerInterface     $entityManager
     * @param LogMessageCreator          $logMessageCreator
     * @param MailingListUpdateInterface $mailingListUpdater
     */
    public function __construct(EntityManagerInterface $entityManager, LogMessageCreator $logMessageCreator, MailingListUpdateInterface $mailingListUpdater)
    {
        $this->entityManager = $entityManager;
        $this->logMessageCreator = $logMessageCreator;
        $this->mailingListUpdater = $mailingListUpdater;
    }


    /**
     * Show the mailing lists.
     *
     * @Route("/administration/manage_mailing_lists/{showOnlyContentTable}",
     *              defaults={"showOnlyContentTable"=false},
     *              name="administration_manage_mailing_lists")
     *
     * @param bool $showOnlyContentTable When true, the cancel button is used to call this action. The controller then
     *                                   has to select the content template instead of the complete-page template.
     * @return Response
     */
    public function indexAction(bool $showOnlyContentTable): Response
    {
        $form = $this->createMailingListForm();
        $subscriberSelectForm = $this->createForm(MailingListSubscriberType::class);

        if ($showOnlyContentTable) {
            // select content template
            $template = 'Admin/mailing_list_content.html.twig';
        } else {
            // Select complete page template
            $template = 'Admin/mailing_list.html.twig';
        }

        return $this->render($template, [
            'form' => $form->createView(),
            'subscriberSelectForm' => $subscriberSelectForm->createView(),
            'edit' => false,
            'mailinglist_updater' => $this->mailingListUpdater,
        ]);
    }


    /**
     * Show the mailing lists as form.
     *
     * @Route("/administration/manage_mailing_lists_edit", name="administration_manage_mailing_lists_edit")
     *
     * @param Request $request
     *
     * @return Response
     *
     * @throws \Exception
     */
    public function editAction(Request $request): Response
    {
        $form = $this->createMailingListForm();
        /** @var MailingList[] $storedMailingLists */
        $storedMailingLists = $this->entityManager->getRepository(MailingList::class)->createFindAllQueryBuilder()->getQuery()->execute();
        $passwordArr = [];
        foreach ($storedMailingLists as $storedMailingList) {
            $passwordArr[$storedMailingList->getId()] = $storedMailingList->getManagementPassword();
        }

        $form->handleRequest($request);

        $edit = true;
        if ($form->isSubmitted()) {
            // Check for mandatory fields which have been cleared by the user. If so, refresh to the old value for the
            // type name or set the value to 0 for the membership fee.
            /** @var FormCollection $lists */
            $lists = $form->getData();
            /** @var MailingList $list */
            foreach ($lists->getMembers() as $list) {
                if (strlen($list->getListName()) === 0) {
                    $this->entityManager->refresh($list);
                }

                $this->passwordChangeHandler($list, $passwordArr);
                $this->httpAdder($list);
            }

            $this->entityManager->flush();
            $edit = false;
            // Read the form values from the database to update the form lines in case an empty member has been removed.
            $form = $this->createMailingListForm();
        }

        return $this->render('Admin/mailing_list_content.html.twig', [
            'form' => $form->createView(),
            'edit' => $edit,
            'mailinglist_updater' => $this->mailingListUpdater,
        ]);
    }


    /**
     * This action is called by the mailinglist selection form and writes the subscribers for the selected mailing list
     * to the viewer.
     *
     * @Route("/administration/manage_mailing_lists_subscribers", name="administration_manage_mailing_lists_subscribers")
     *
     * @param Request $request
     *
     * @return Response
     */
    public function listSubscribersAction(Request $request): Response
    {
        $subscriberSelectForm = $this->createForm(MailingListSubscriberType::class);
        $subscriberSelectForm->handleRequest($request);
        $mailingList = null;
        if ($subscriberSelectForm->isSubmitted()) {
            /** @var MailingList $mailingList */
            $mailingList = $subscriberSelectForm->getData()['mailinglist'];
        }

        return $this->render('Admin/mailing_subscribers.html.twig', [
            'mailingList' => $mailingList,
        ]);
    }


    /**
     * Create a form based on the mailing list collection, which contains all mailing lists in the database.
     *
     * @return FormInterface
     */
    private function createMailingListForm(): FormInterface
    {
        $listsInDb = $this->entityManager->getRepository(MailingList::class)
                                         ->createFindAllQueryBuilder()
                                         ->getQuery()
                                         ->execute();
        $listCollection = new FormCollection($listsInDb, $this->entityManager);
        $form = $this->createForm(FormCollectionType::class, $listCollection, [
            'entry_class' => MailingListType::class,
        ]);

        return $form;
    }


    /**
     * Special log message handling for passwords. The default handling with the entity changes listener
     * would show the password in the log file
     *
     * @param MailingList $list
     * @param array       $passwordArr
     *
     * @throws \Exception
     */
    private function passwordChangeHandler(MailingList $list, array $passwordArr): void
    {
        if ((array_key_exists($list->getId(), $passwordArr)) &&
            ($list->getManagementPassword() !== $passwordArr[$list->getId()])) {
            $message = "Changed management password for mailing list $list";
            $this->logMessageCreator->createLogEntry('system settings change', $message);
        }
    }


    /**
     * When the Management email field (which can also be a domain name for mailman lists) doesn't contain the add-sign,
     * it is probably a domain name. These should start with http. When this is missing, add it here.
     *
     * @param MailingList $list
     */
    private function httpAdder(MailingList $list): void
    {
        if (false === strpos($list->getManagementEmail(), '@')) {
            if ((false === strpos($list->getManagementEmail(), 'http://')) &&
                (false === strpos($list->getManagementEmail(), 'https://'))) {
                $list->setManagementEmail('http://'.$list->getManagementEmail());
            }
        }
    }
}
