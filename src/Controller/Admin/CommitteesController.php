<?php

/*
 * Copyright (C) 2022 by Luc Hamers <vereniging@hamers.de>
 *
 * This file is part of Vereniging.
 *
 * Vereniging is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Vereniging is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Vereniging.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Controller\Admin;

use App\Entity\Committee;
use App\Entity\CommitteeFunction;
use App\Entity\CommitteeFunctionMap;
use App\Service\ListTypeManager;
use App\Service\LogMessageCreator;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class CommitteesController
 *
 * @Security("is_granted('ROLE_MANAGE_COMMITTEES')")
 */
class CommitteesController extends AbstractController
{
    private EntityManagerInterface $entityManager;
    private ListTypeManager $listTypeManager;
    private LogMessageCreator $logMessageCreator;


    /**
     * ListsController constructor.
     *
     * @param EntityManagerInterface $entityManager
     * @param ListTypeManager        $listTypeManager
     * @param LogMessageCreator      $logMessageCreator
     */
    public function __construct(EntityManagerInterface $entityManager, ListTypeManager $listTypeManager, LogMessageCreator $logMessageCreator)
    {
        $this->listTypeManager = $listTypeManager;
        $this->entityManager = $entityManager;
        $this->logMessageCreator = $logMessageCreator;
    }


    /**
     * Show the committees and their functions
     *
     * @Route("/administration/manage_committees/{showOnlyContentTable}",
     *              defaults={"showOnlyContentTable"=false},
     *              name="administration_manage_committees")
     *
     * @param bool $showOnlyContentTable When true, the cancel button is used to call this action. The controller then
     *                                   has to select the content template instead of the complete-page template.
     *
     * @return Response
     *
     * @throws Exception
     */
    public function indexAction(bool $showOnlyContentTable): Response
    {
        $typeList = $this->generateTypeList();

        foreach ($typeList as &$type) {
            $type['form'] = $type['form']->createView();
        }

        $functions = $this->entityManager->getRepository(CommitteeFunction::class)->findAll();
        $committees = $this->entityManager->getRepository(Committee::class)->findAll();

        if ($showOnlyContentTable) {
            // select content template
            $template = 'Admin/committee_function_link_content.html.twig';
        } else {
            // Select complete page template
            $template = 'Admin/committee.html.twig';
        }

        return $this->render($template, [
            'functions'   => $functions,
            'committees'  => $committees,
            'edit'        => false,
            'typeList'    => $typeList,
            'post_route'  => 'administration_manage_committees_edit_values',
            'reset_route' => 'administration_manage_committees_cancel_values',
        ]);
    }


    /**
     * Edit the links between committees and their functions
     *
     * @Route("/administration/manage_committees_edit_links", name="administration_manage_committees_edit_links")
     *
     * @param Request $request
     *
     * @return Response
     *
     * @throws Exception
     */
    public function editLinksAction(Request $request): Response
    {
        $functions = $this->entityManager->getRepository(CommitteeFunction::class)->findAll();
        $committees = $this->entityManager->getRepository(Committee::class)->findAll();

        $formData = $request->request->all('committee_function_form');

        $edit = true;
        if ($request->isMethod('POST')) {
            // Special treatment when no checkbox is set. The following lines need formData to be an array.
            if (is_null($formData)) {
                $formData = [];
            }
            /** @var Committee $committee */
            foreach ($committees as $committee) {
                // Get array which contains all the values for the checked functions of this committee row
                $rowKey = 'committee_'.$committee->getId();
                // When there is at least one checkbox set for a committee (i.e. a row), then get the ids of the
                // functions as rowData. If not, get an empty array.
                $rowData = array_key_exists($rowKey, $formData) ? $formData[$rowKey] : [];
                /** @var CommitteeFunction $function */
                foreach ($functions as $function) {
                    $mapEntry = new CommitteeFunctionMap();
                    $mapEntry->setCommitteeFunction($function);
                    if (in_array($function->getId(), $rowData)) {
                        if ($committee->addCommitteeFunctionMap($mapEntry)) {
                            $message = "Added function $function to committee $committee";
                            $this->logMessageCreator->createLogEntry('system settings change', $message);
                        }
                    } else {
                        if ($committee->removeCommitteeFunctionMap($mapEntry)) {
                            $message = "Removed function $function from committee $committee";
                            $this->logMessageCreator->createLogEntry('system settings change', $message);
                        }
                    }
                }
            }
            $this->entityManager->flush();

            $edit = false;
        }

        return $this->render('Admin/committee_function_link_content.html.twig', [
            'functions' => $functions,
            'committees' => $committees,
            'edit' => $edit,
        ]);
    }


    /**
     * Edit committee and committee function values.
     *
     * @Route("/administration/manage_committees_edit_values/{type}", name="administration_manage_committees_edit_values")
     *
     * @param Request $request http request
     * @param string  $type    Name of the entity for which an object is added.
     *
     * @return Response
     */
    public function editValuesAction(Request $request, string $type): Response
    {
        $typeList = $this->generateTypeList();

        $edit = $this->listTypeManager->handleForm($request, $typeList[$type]);

        // Read the form values from the database to update the form lines in case an empty member has been removed.
        $typeList = $this->generateTypeList();

        foreach ($typeList as &$typeEntry) {
            $typeEntry['form'] = $typeEntry['form']->createView();
        }

        return $this->render('Admin/list_content.html.twig', [
            'type'        => $typeList[$type],
            'post_route'  => 'administration_manage_committees_edit_values',
            'reset_route' => 'administration_manage_committees_cancel_values',
            'edit'        => $edit,
            'entityType'  => $type,
        ]);
    }


    /**
     * Cancel editing committee and function values.
     *
     * @Route("/administration/manage_committees_cancel_values/{entityType}", name="administration_manage_committees_cancel_values")
     *
     * @param string $entityType Entity type for which the subpage has to be created.
     *
     * @return Response
     */
    public function resetAction(string  $entityType): Response
    {
        $typeList = $this->generateTypeList();

        $typeList[$entityType]['form'] = $typeList[$entityType]['form']->createView();

        return $this->render('Admin/list_content.html.twig', [
            'type'        => $typeList[$entityType],
            'post_route'  => 'administration_manage_committees_edit_values',
            'reset_route' => 'administration_manage_committees_cancel_values',
            'edit'        => false,
            'entityType'  => $entityType,
        ]);
    }


    /**
     * @return array
     */
    private function generateTypeList(): array
    {
        $typeList = [
            'committee'         => ['entity' => 'Committee', 'caption' => 'Committees'],
            'committeefunction' => ['entity' => 'CommitteeFunction', 'caption' => 'Committee functions'],
        ];

        foreach ($typeList as &$type) {
            $type['headers']       = $this->listTypeManager->generateHeaders($type['entity']);
            $type['form']          = $this->listTypeManager->generateForm();
            $type['extraFields']   = $this->listTypeManager->getExtraFields();
            $type['mainFieldName'] = $this->listTypeManager->getMainFieldName();
        }

        return $typeList;
    }
}
