<?php

/*
 * Copyright (C) 2022 by Luc Hamers <vereniging@hamers.de>
 *
 * This file is part of Vereniging.
 *
 * Vereniging is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Vereniging is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Vereniging.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Controller\Admin;

use App\Entity\EmailTemplate;
use App\Entity\MemberEntry;
use App\Service\LogMessageCreator;
use App\Service\MemberSelectionFormHandler;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security as SecurityAnnotation;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * Class UserRightsController
 *
 * @SecurityAnnotation("is_granted('ROLE_MANAGE_PASSWORDS')")
 */
class PasswordController extends AbstractController
{
    private EntityManagerInterface $entityManager;
    private MemberSelectionFormHandler $selectionFormHandler;
    private array $siteName;
    private string $baseUrl;
    private MailerInterface $mailer;
    private LogMessageCreator $logMessageCreator;
    private TranslatorInterface $translator;


    /**
     * PasswordController constructor.
     *
     * @param EntityManagerInterface     $entityManager
     * @param MemberSelectionFormHandler $selectionFormHandler
     * @param MailerInterface            $mailer
     * @param LogMessageCreator          $logMessageCreator
     * @param TranslatorInterface        $translator
     * @param Security                   $security
     * @param array                      $siteName             Site name array where with locales as key
     * @param string                     $baseUrl              URL of the site
     *
     * @throws Exception
     */
    public function __construct(EntityManagerInterface $entityManager, MemberSelectionFormHandler $selectionFormHandler, MailerInterface $mailer, LogMessageCreator $logMessageCreator, TranslatorInterface $translator, Security $security, array $siteName, string $baseUrl)
    {
        $this->entityManager = $entityManager;
        $this->selectionFormHandler = $selectionFormHandler;
        $this->siteName = $siteName;
        $this->baseUrl = $baseUrl;
        $this->mailer = $mailer;
        $this->logMessageCreator = $logMessageCreator;
        $this->translator = $translator;
        /** @var MemberEntry $currentUser */
        $currentUser = $security->getUser();
        $this->selectionFormHandler->setOption('currentLocale', $currentUser->getPreferredLanguage()->getLocale());
        $this->selectionFormHandler->setOption('omitFields', ['debt']);
    }


    /**
     * Show the users list (members) and their password settings.
     *
     * @Route("/administration/manage_passwords", name="administration_manage_passwords")
     *
     * @return Response
     */
    public function indexAction(): Response
    {
        // Create the selection form which selects user grUpdated installation docs.oups based on status and user role.
        $selectionForm = $this->selectionFormHandler->generateForm(true);
        // Load all members with the status "member", which are shown in the default view.
        $members = $this->entityManager->getRepository(MemberEntry::class)->findByStatus('member');

        return $this->render('Admin/passwords.html.twig', [
            'selectionForm' => $selectionForm->createView(),
            'members' => $members,
        ]);
    }


    /**
     * Show main member lists page.
     *
     * @Route("/administration/manage_passwords/show", name="administration_manage_passwords_show")
     *
     * @param Request $request
     *
     * @return Response
     */
    public function showAction(Request $request): Response
    {
        $this->selectionFormHandler->generateForm(false);
        $members = $this->selectionFormHandler->handleFormRequest($request);

        return $this->render('Admin/passwords_member_table.html.twig', array(
            'members' => $members,
        ));
    }


    /**
     * This is called when a button to generate a password is clicked on the password page. This method then generates a
     * new password, sends an email with the new credentials to the member and returns back to the member list page..
     *
     * @Route("/administration/manage_passwords/new_password/{id}", name="administration_manage_passwords_new_password")
     *
     * @param MemberEntry $memberEntry Member entry for which the new password has to be generated.
     *
     * @return Response
     *
     * @throws Exception
     * @throws TransportExceptionInterface
     */
    public function generateAndSendNewPasswordAction(MemberEntry $memberEntry): Response
    {
        // Generate a new password. This is stored in plainPassword and will be encrypted when memberEntry is saved to
        // the database. plainPassword is cleared when this happens. Therefor the email has to be rendered before
        // memberEntry is stored, so it can be written to the email.
        $memberEntry->setPlainPassword($this->generatePassword());
        $userLocale = $memberEntry->getPreferredLanguage()->getLocale();
        $siteName = $this->siteName[$userLocale];
        $siteString = "<a href=\"$this->baseUrl\">$siteName</a>";
        $subject = $this->translator->trans('New login credentials for', [], null, $userLocale)." $siteName";

        // Render the content of the email, including the new login credentials
        $body = $this->renderView('Admin/passwords_email.html.twig', [
            'user' => $memberEntry,
            'siteName' => $siteString,
        ]);
        // Write the body into the login email template
        $template = $this->entityManager->getRepository(EmailTemplate::class)->readLoginTemplate();
        $body = str_replace(EmailTemplate::HTML_CONTENT_VARIABLE, $body, $template->getContent());

        $email = (new Email())
            ->from($template->getSenderAddress($userLocale))
            ->to($memberEntry->getEmail())
            ->subject($subject)
            ->text(strip_tags($body))
            ->html($body);

        try {
            $this->mailer->send($email);
            $this->logMessageCreator->createLogEntry('member data change', 'Changed password', $memberEntry);
        } catch (TransportExceptionInterface $e) {
            $message = "Sending new password email failed with the following error: ".$e->getMessage();
            // Since we have a problem sending emails, disable them for the coming log message
            $this->logMessageCreator->disableEmailNotifications();
            $this->logMessageCreator->createLogEntry('error', $message);
            $this->logMessageCreator->enableEmailNotifications();
            $this->entityManager->flush();

            throw $e;
        }

        $this->entityManager->persist($memberEntry);
        $this->entityManager->flush();

        $flashMessage = $this->translator->trans('change_password_message', [
            '%name%' => $memberEntry->getCompleteName(),
            '%email%' => $memberEntry->getEmail(),
        ]);

        $this->addFlash('notice', $flashMessage);

        return $this->render('Admin/passwords_flash_messages.html.twig');
    }


    /**
     * This is called to revoke a password, i.e. access to the system.
     *
     * @Route("/administration/manage_passwords/revoke_password/{id}", name="administration_manage_passwords_revoke_password")
     *
     * @param MemberEntry $memberEntry Member entry for which the password has to be revoked.
     *
     * @return Response
     *
     * @throws Exception
     */
    public function revokePasswordAction(MemberEntry $memberEntry): Response
    {
        $memberEntry->setPassword(null);

        $this->logMessageCreator->createLogEntry('member data change', 'Revoked password', $memberEntry);
        $this->entityManager->persist($memberEntry);
        $this->entityManager->flush();

        $flashMessage = $this->translator->trans('revoke_password_message', [
            '%name%' => $memberEntry->getCompleteName(),
        ]);

        $this->addFlash('notice', $flashMessage);

        return $this->render('Admin/passwords_flash_messages.html.twig');
    }


    /**
     * Generate a password which contains upper and lower case characters and numbers. I and l are not used to improve
     * readability for the user.
     *
     * @param int $length Number of characters of the password.
     *
     * @return string Returns the generated password.
     */
    private function generatePassword(int $length = 8): string
    {
        $chars = 'abcdefghijkmnopqrstuvwxyzABCDEFGHJKLMNOPQRSTUVWXYZ0123456789';
        $count = mb_strlen($chars);

        for ($i = 0, $result = ''; $i < $length; ++$i) {
            $index = rand(0, $count - 1);
            $result .= mb_substr($chars, $index, 1);
        }

        return ($result);
    }
}
