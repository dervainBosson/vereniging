<?php

/*
 * Copyright (C) 2022 by Luc Hamers <vereniging@hamers.de>
 *
 * This file is part of Vereniging.
 *
 * Vereniging is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Vereniging is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Vereniging.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Controller\Admin;

use App\Entity\Language;
use App\Entity\SerialLetterTemplate;
use App\Form\SerialLetterTemplateType;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Controller for editing lists of entities like titles or languages.
 *
 * @Security("is_granted('ROLE_MANAGE_SERIAL_LETTER_TEMPLATES')")
 */
class SerialLetterTemplateController extends AbstractController
{
    private EntityManagerInterface $entityManager;


    /**
     * SerialLetterTemplateController constructor.
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }


    /**
     * Generate main administration page.
     *
     * @Route("/administration/manage_serial_letter_templates", name="administration_manage_serial_letter_templates")
     *
     * @return Response
     */
    public function indexAction(): Response
    {
        $templates = $this->entityManager->getRepository(SerialLetterTemplate::class)->findAllAsArray();
        $languages = $this->entityManager->getRepository(Language::class)->findAll();

        return $this->render('Admin/serial_letter_template.html.twig', [
            'templates' => $templates,
            'languages' => $languages,
        ]);
    }


    /**
     * @param SerialLetterTemplate $template
     *
     * @Route("/administration/manage_serial_letter_templates/delete//{id}", name="administration_manage_serial_letter_templates_delete")
     *
     * @return Response
     */
    public function deleteAction(SerialLetterTemplate $template): Response
    {
        $this->entityManager->remove($template);
        $this->entityManager->flush();

        return new Response();
    }


    /**
     * @param int|null $id
     * @param Request  $request
     *
     * @return Response
     *
     * @Route("/administration/manage_serial_letter_templates/edit//{id}", name="administration_manage_serial_letter_templates_edit", defaults={"id"=null})
     */
    public function editAction(?int $id, Request $request): Response
    {
        if (is_null($id)) {
            $template = new SerialLetterTemplate();
            $template->setTemplateName('');
            $template->setTemplateContent('');
        } else {
            $template = $this->entityManager->getRepository(SerialLetterTemplate::class)->find($id);
        }


        // Template name has to used from the original name in the database, because the name could be
        $templateName = $template->getTemplateName();
        $templateForm = $this->createForm(SerialLetterTemplateType::class, $template);

        $templateForm->handleRequest($request);
        if ($templateForm->isSubmitted() && $templateForm->isValid()) {
            $template = $templateForm->getData();
            $this->entityManager->persist($template);
            $this->entityManager->flush();

            return new Response('OK');
        }

        return $this->render('Admin/serial_letter_template_edit.html.twig', [
            'form' => $templateForm->createView(),
            'templateName' => $templateName,
        ]);
    }
}
