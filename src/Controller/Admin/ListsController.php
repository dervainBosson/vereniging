<?php

/*
 * Copyright (C) 2022 by Luc Hamers <vereniging@hamers.de>
 *
 * This file is part of Vereniging.
 *
 * Vereniging is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Vereniging is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Vereniging.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Controller\Admin;

use App\Service\ListTypeManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Controller for editing lists of entities like titles or languages.
 *
 * @Security("is_granted('ROLE_MANAGE_LIST_VALUES')")
 */
class ListsController extends AbstractController
{
    private ListTypeManager $listTypeManager;


    /**
     * ListsController constructor.
     *
     * @param ListTypeManager $listTypeManager
     */
    public function __construct(ListTypeManager $listTypeManager)
    {
        $this->listTypeManager = $listTypeManager;
    }


    /**
     * Show the manage list page. By passing the bool parameter, this can also be called by ajax calls wher eonly the
     * table is to be drawn.
     *
     * @Route("/administration/manage_lists", name="administration_manage_lists")
     *
     * @return Response
     *
     * @throws \Exception
     */
    public function indexAction(): Response
    {
        $typeList = $this->generateTypeList();

        foreach ($typeList as &$type) {
            $type['form'] = $type['form']->createView();
        }

        return $this->render('Admin\list.html.twig', [
            'typeList'    => $typeList,
            'post_route'  => 'administration_manage_lists_edit',
            'reset_route' => 'administration_manage_lists_reset',
            'edit'        => false,
        ]);
    }


    /**
     * Draw a sub page with the table for one type.
     *
     * @Route("/administration/manage_lists_reset/{entityType}", name="administration_manage_lists_reset")
     *
     * @param string $entityType Entity type for which the subpage has to be created.
     *
     * @return Response
     *
     * @throws \Exception
     */
    public function resetAction(string  $entityType): Response
    {
        $typeList = $this->generateTypeList();

        $typeList[$entityType]['form'] = $typeList[$entityType]['form']->createView();

        return $this->render('Admin\list_content.html.twig', [
            'type'        => $typeList[$entityType],
            'post_route'  => 'administration_manage_lists_edit',
            'reset_route' => 'administration_manage_lists_reset',
            'edit'        => false,
            'entityType'  => $entityType,
        ]);
    }


    /**
     * @Route("/administration/manage_lists_edit/{type}", name="administration_manage_lists_edit")
     *
     * @param Request $request http request
     * @param string  $type    Name of the entity for which an object is added.
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function editAction(Request $request, $type): Response
    {
        $typeList = $this->generateTypeList();

        $edit = $this->listTypeManager->handleForm($request, $typeList[$type]);

        // Read the form values from the database to update the form lines in case an empty member has been removed.
        $typeList = $this->generateTypeList();

        foreach ($typeList as &$typeEntry) {
            $typeEntry['form'] = $typeEntry['form']->createView();
        }

        return $this->render('Admin\list_content.html.twig', [
            'type'        => $typeList[$type],
            'post_route'  => 'administration_manage_lists_edit',
            'reset_route' => 'administration_manage_lists_reset',
            'edit'        => $edit,
            'entityType'  => $type,
        ]);
    }


    /**
     * @return array
     */
    private function generateTypeList(): array
    {
        $typeList = [
            'status'          => ['entity' => 'Status', 'caption' => 'Status types'],
            'title'           => ['entity' => 'Title', 'caption' => 'Academic titles'],
            'language'        => ['entity' => 'Language', 'caption' => 'Member languages'],
            'addresstype'     => ['entity' => 'AddressType', 'caption' => 'Address types'],
            'country'         => ['entity' => 'Country', 'caption' => 'Countries'],
            'phonenumbertype' => ['entity' => 'PhoneNumberType', 'caption' => 'Types of phone numbers'],
        ];

        foreach ($typeList as &$type) {
            $type['headers']       = $this->listTypeManager->generateHeaders($type['entity']);
            $type['form']          = $this->listTypeManager->generateForm();
            $type['extraFields']   = $this->listTypeManager->getExtraFields();
            $type['mainFieldName'] = $this->listTypeManager->getMainFieldName();
        }

        return $typeList;
    }
}
